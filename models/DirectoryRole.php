<?php

Yii::import('application.modules.core_models.models._base.BaseDirectoryRole');

class DirectoryRole extends BaseDirectoryRole
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
