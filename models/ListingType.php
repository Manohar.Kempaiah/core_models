<?php

Yii::import('application.modules.core_models.models._base.BaseListingType');

class ListingType extends BaseListingType
{

    // defined here so we don't depend on the internal ids all over the place
    const GOOGLE_PROFILE_ID = 87;
    const GOOGLE_PROVIDERATPRACTICE_PROFILE_ID = 146;
    const YELP_PROFILE_ID = 52;
    const YELP_WRITEREVIEW_ID = 91;
    const FACEBOOK_ID = 16;
    const YELP_PROVIDER_ID = 141;
    const THIRD_PARTY_BOOKINGURL_ID = 156;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from location if associated records exist in practice table
        $has_associated_records = ProviderPracticeListing::model()->exists(
            'listing_type_id=:listingTypeId',
            array(':listingTypeId' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError(
                "id",
                "Listing type can't be deleted because there are providers & practices linked to it."
            );
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Validate some info before save
     * @return boolean
     */
    public function beforeValidate()
    {
        if (empty($this->code)) {
            $siteObj = ListingSite::model()->findByPk($this->listing_site_id);
            if (!empty($siteObj)) {
                $siteName = $siteObj->name;
                if (!empty($siteName)) {
                    $code = StringComponent::prepareNewFriendlyUrl(trim($siteName));
                    $code = str_replace('-', '_', $code);
                    $code = strtoupper($code);
                    $this->code = $code;
                }
            }
        }
        return parent::beforeValidate();
    }

    /**
     * Retrieve a code-indexed array of listing types, and quickly
     * @return array
     */
    public static function getCodeIndexedArray()
    {
        $output = Yii::app()->cache->get("CodeListings");

        if (empty($output)) {
            $sql = 'SELECT listing_type.id AS id, listing_site.name AS name, code, per
                FROM listing_type
                INNER JOIN listing_site
                ON listing_site.id = listing_type.listing_site_id;';
            $set = Yii::app()->db->createCommand($sql)->query();

            $output = array();

            foreach ($set as $type) {
                $output[$type['code']] = array(
                    "id" => $type['id'],
                    "name" => $type['name'],
                    "code" => $type['code'],
                    "per" => $type['per']
                );
            }

            Yii::app()->cache->set("CodeListings", $output, Yii::app()->params['cache_long']);
        }

        return $output;
    }

    /**
     * Search
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('listing_site_id', $this->listing_site_id);
        $criteria->compare('level', $this->level, true);
        $criteria->compare('is_internal', $this->is_internal);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('per', $this->per, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 10)
        ));
    }

    /**
     * Rules
     * @return array
     */
    public function rules()
    {
        return array(
            array('code', 'required'),
            array('is_internal', 'numerical', 'integerOnly' => true),
            array('level', 'length', 'max' => 8),
            array('code', 'length', 'max' => 80),
            array('per', 'length', 'max' => 1),
            array('logo_path, base_url', 'length', 'max' => 255),
            array('id, level, is_internal, code, per, listing_site_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        $relations = parent::relations();
        $relations['listingSite'] = [self::BELONGS_TO, 'ListingSite', 'listing_site_id'];

        return $relations;
    }

    /**
     * Return codeType based on a per field
     * @param string $siteCode
     * @return string $codeType ( practice || provider)
     */
    public static function getCodeType($siteCode = '')
    {
        if (empty($siteCode)) {
            return '';
        }

        //
        // Determine if the ListingType code is for practice or provider
        //
        $codeType = '';
        $recListingType = ListingType::model()->cache(Yii::app()->params['cache_medium'])->find(
            'code=:code',
            array(':code' => $siteCode)
        );
        if (!empty($recListingType)) {
            if ($recListingType->per == 'T') {
                $codeType = 'practice';
            } elseif ($recListingType->per == 'V') {
                $codeType = 'provider';
            }
        }

        return $codeType;
    }
}
