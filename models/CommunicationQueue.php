<?php

Yii::import('application.modules.core_models.models._base.BaseCommunicationQueue');

class CommunicationQueue extends BaseCommunicationQueue
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'id';
    }

    /**
     * Set dates automatically if needed
     */
    public function beforeSave()
    {
        if (empty($this->date_added) || $this->date_added == '0000-00-00 00:00:00') {
            $this->date_added = date('Y-m-d H:i:s');
        }

        if (empty($this->date_updated) || $this->date_updated == '0000-00-00 00:00:00') {
            $this->date_updated = date('Y-m-d H:i:s');
        }

        return parent::beforeSave();
    }

    /**
     * Transform a JSON encoded recipient string into a human readable string
     * @param string $sentTo
     * @return string
     */
    public static function formatSentTo($sentTo)
    {
        $to = '';
        $jsonTo = json_decode($sentTo);

        if (!is_array($jsonTo)) {
            // this was likely a phone number, return as it was
            return $sentTo;
        }

        foreach ($jsonTo as $recipient) {
            $to[] = ucfirst($recipient->type) . ': ' . $recipient->email;
        }
        return implode(PHP_EOL . ', ', $to);
    }

    /**
     * CMD: Get next 100 messages in the queue to be sent
     *
     * @important We use ORDER BY id DESC because otherwise if errors piled up we'd be stuck processing the beginning of
     * the queue. This way we process LIFO which seems weird but it protects itself from accumulation of failed entries.
     * On top of that, we limit the query to the current day + yesterday so past days' stuck entries are ignored (we
     * need yesterday because otherwise messages queued right before midnight would break)
     *
     * @see CommunicationsCommand::actionProcessQueue
     * @return array
     */
    public static function getCommunicationQueue()
    {
        $sql = sprintf(
            "SELECT SQL_NO_CACHE id
            FROM communication_queue
            WHERE is_processed = 0
            AND is_sent = 0
            AND (DATE(date_added) = '%s' OR DATE(date_added) = '%s')
            ORDER BY id DESC
            LIMIT 100;",
            date('Y-m-d', strtotime('yesterday')),
            date('Y-m-d'));
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

}
