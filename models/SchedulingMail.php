<?php

Yii::import('application.modules.core_models.models._base.BaseSchedulingMail');

Yii::import('application.modules.providers.protected.components.BaseImporter');
Yii::import('application.modules.providers.protected.components.Mi7Importer');
Yii::import('application.modules.providers.protected.components.NextechImporter');
Yii::import('application.modules.providers.protected.components.DentrixG5Importer');
Yii::import('application.modules.providers.protected.components.PracticeBrainImporter');
Yii::import('application.modules.providers.protected.components.CapybaraCache');

class SchedulingMail extends BaseSchedulingMail
{

    public $partner;
    public $activeClient;
    public $organizationId;
    public $organizationName;
    public $salesforceId;
    public $organizationStatus;
    private $notificationSent = false;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function afterDelete()
    {
        CapybaraCache::removeSchedulingMail($this);

        return parent::afterDelete();
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        $labels['provider_id'] = 'Provider';
        $labels['partner_site_id'] = 'Partner Site';
        $labels['practice_id'] = 'Practice';
        $labels['first_name'] = 'Patient First Name';
        $labels['last_name'] = 'Patient Last Name';
        $labels['cell_phone'] = 'Patient Cell Phone';
        $labels['email'] = 'Patient Email';
        $labels['appointment_date'] = 'Appt. Date';
        $labels['date_time'] = 'Appt. Time';
        $labels['insurance_id'] = 'Patient Insurance';
        $labels['prescription'] = 'Patient Has Prescription?';
        $labels['reason'] = 'Appt. Reason';
        $labels['date_added'] = 'Date Added';
        $labels['date_updated'] = 'Date Updated';
        $labels['source'] = 'Source';
        $labels['partnerSite'] = 'Partner Site';
        $labels['formattedStatus'] = 'Status';
        $labels['formattedAppointmentType'] = 'Appt. Type';
        $labels['reminder_mail'] = 'Email reminder';
        $labels['reminder_sms'] = 'SMS reminder';
        $labels['reminder_autocall'] = 'Autocall reminder';
        $labels['reminder_staffcall'] = 'Staff call reminder';
        $labels['formattedWaitingFor'] = 'Waiting For';
        $labels['activeClient'] = 'Active client?';
        $labels['organizationId'] = 'Organization ID';
        $labels['organizationName'] = 'organization Name';
        $labels['salesforceId'] = 'Salesforce ID';
        $labels['organizationStatus'] = 'Organization Status';

        return $labels;
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     *
     * @return boolean
     */
    public function beforeDelete()
    {
        $mailComments = SchedulingMailComment::model()->findAll(
            'scheduling_mail_id=:scheduling_mail_id',
            array(':scheduling_mail_id' => $this->id)
        );
        // iterate the comment
        foreach ($mailComments as $mailComment) {
            // did we delete it ok?
            // no
            if (!$mailComment->delete()) {
                // return failure
                return false;
            }
        }

        $arrHistory = SchedulingMailHistory::model()->findAll(
            'scheduling_mail_id=:scheduling_mail_id',
            array(':scheduling_mail_id' => $this->id)
        );
        // iterate the history
        foreach ($arrHistory as $history) {
            // did we delete the history ok?
            // no
            if (!$history->delete()) {
                // return failure
                return false;
            }
        }
        // if we got here we made it so return success
        return parent::beforeDelete();
    }

    /**
     * Check if we have to send SQS a request to update SF data
     * This happens if we're creating an appointment request (appointment_type R) or approving or un-approving a
     * directly scheduled appointment (appointment_type S and status is now or was A)
     * @return boolean
     */
    public function beforeSave()
    {
        // do we have the practice?
        // yes
        if (!empty($this->practice)) {
            // populate ny_start_datetime
            $practiceTimeZone = $this->practice->getTimeZone();
            $dateTime = substr($this->appointment_date, 0, 10) . ' ' . $this->time_start;

            // The practice have NY time?
            if ($practiceTimeZone['phpTimezone'] == 'America/New_York') {
                // Yes
                $this->ny_start_datetime = $dateTime;
            } else {
                // No
                $nyTimeStart = new DateTime($dateTime, new DateTimeZone($practiceTimeZone['phpTimezone']));
                $nyTimeStart->setTimezone(new DateTimeZone('America/New_York'));
                $this->ny_start_datetime = $nyTimeStart->format('Y-m-d H:i:s');
            }

            // are we creating a new appointment?
            if ($this->isNewRecord) {
                // yes, look up practice details if we have a practice
                $practiceDetails = PracticeDetails::model()->find(
                    'practice_id=:practiceId',
                    [
                        ':practiceId' => $this->practice_id
                    ]
                );
                // did we find practice details?
                if (!empty($practiceDetails) && $practiceDetails->practice_type_id == 5) {
                    // yes, is the practice a virtual telemedicine practice?
                    // yes, then mark the appointment as a telemedicine appointment
                    $this->appointment_type = 'V';
                }
            }
        }

        // Try to find the partner_site_id
        if ($this->isNewRecord && empty($this->partner_site_id)) {
            // do we have the source?
            // yes
            if (!empty($this->source)) {
                $this->partner_site_id = PartnerSite::getPartnerBySource($this->source);
            } else {
                // do we have the source?
                // no
                // we dont have either parter or source, try to getting from 'referer'
                $this->source = StringComponent::getReferer();
                $this->partner_site_id = 63;
            }

            // do we have partner site 0?
            // yes
            if ($this->partner_site_id == 0) {
                // 1 = 'doctor' -> should we add a new partner site called `unknown` for this cases?
                $this->partner_site_id = 1;
            }
        }

        // query to see if this provider belongs to an organization
        $arrOrganizationAccounts = Provider::belongsToOrganization($this->provider_id);

        // did we get the organization?
        // no
        if (empty($arrOrganizationAccounts)) {
            // nope, just leave
            return parent::beforeSave();
        }

        // do we have a new request?
        // yes
        if ($this->isNewRecord && $this->appointment_type == 'R') {
            // new appointment request, just count it in
            foreach ($arrOrganizationAccounts as $organizationId) {
                SqsComponent::sendMessage('updateSFOrganizationAppointments', $organizationId);
            }
        } elseif ($this->appointment_type == 'S' || $this->appointment_type == 'V') {
            // do we have a new request?
            // no

            // check if it's a status change so it needs to be counted
            // do we have the old atributes with a new status that was previously approved?
            if (
                !empty($this->oldRecord)
                && ($this->oldRecord->status != $this->status)
                && ($this->oldRecord->status == 'A' || $this->status == 'A')
            ) {
                // yes
                // appointment status changed and it was or is now approved, so we need to count it in
                foreach ($arrOrganizationAccounts as $organizationId) {
                    SqsComponent::sendMessage('updateSFOrganizationAppointments', $organizationId);
                }
            }
        }

        return parent::beforeSave();
    }

    /**
     * Check if we have to send SQS for SMS or Email account Notifications
     * @return boolean
     */
    public function afterSave()
    {
        //is this a new record that is scheduled?
        //yes
        if ($this->isNewRecord && ($this->appointment_type == 'S' || $this->appointment_type == 'V')) {
            // new appointment request, prepare SQS command
            $queueBody = array();
            $queueBody['schedulingMailId'] = $this->id;
            $queueBody['alertType'] = 'alert_app_request';
            SqsComponent::sendMessage('alertAppRequest', $queueBody);

            // This appointment is requested for Freemium account?
            $accountData = Account::getFreeAccountByProviderPractice($this->provider_id, $this->practice_id);
            //do we have a salesforce id?
            //yes
            if (!empty($accountData['salesforce_lead_id'])) {
                // If is a Freemium, report to SF leads
                $queueBody['schedulingMailId'] = $this->id;
                $queueBody['accountData'] = $accountData;
                $queueBody['alertType'] = 'appointmentRequestForFreemiumProviders_SF';
                SqsComponent::sendMessage('appointmentRequestForFreemiumProviders_SF', $queueBody);
            }
        }

        //is this a new record?
        //yes
        if ($this->isNewRecord) {
            //grab the EHR for the provider/practice
            $arrEmrAccount = AccountEmr::accountEmrProviderPractice($this->provider_id, $this->practice_id);

            //do we have an EHR?
            //yes
            if ($arrEmrAccount) {
                //is this nextech (5)?
                //yes
                if ($arrEmrAccount['emr_type_id'] == 5) {
                    NextechImporter::matchEhrPatient($this->id, $arrEmrAccount['id'], $arrEmrAccount['emr_type_id']);
                } elseif ($arrEmrAccount['emr_type_id'] == 1) {

                    //is this nextech (5)?
                    //no - practice brain (1)

                    PracticeBrainImporter::matchEhrPatient(
                        $this->id,
                        $arrEmrAccount['id'],
                        $arrEmrAccount['emr_type_id']
                    );
                }
            }
        }

        // Saves history
        $smh = new SchedulingMailHistory;
        $smh->scheduling_mail_id = $this->id;
        $smh->localization_id = $this->localization_id;
        $smh->provider_id = $this->provider_id;
        $smh->patient_id = $this->patient_id;
        $smh->partner_site_id = $this->partner_site_id;
        $smh->appointment_type = $this->appointment_type;
        $smh->appointment_for = $this->appointment_for;
        //is this a new record?
        //yes
        if (!$this->isNewRecord) {
            //set the status to what came in
            $smh->status = $this->status;
        } else {
            //is this a new record?
            //no
            //set the status to P
            $smh->status = 'P';
        }
        $smh->first_name = $this->first_name;
        $smh->last_name = $this->last_name;
        $smh->cell_phone = $this->cell_phone;
        $smh->email = $this->email;
        $smh->appointment_date = $this->appointment_date;
        $smh->date_time = $this->date_time;
        $smh->time_start = $this->time_start;
        $smh->time_end = $this->time_end;
        $smh->insurance_id = $this->insurance_id;
        $smh->insurance_company_id = $this->insurance_company_id;
        $smh->insurance_other_plan = $this->insurance_other_plan;
        $smh->prescription = $this->prescription;
        $smh->returning_patient = $this->returning_patient;
        $smh->visit_reason_id = $this->visit_reason_id;
        $smh->reason = $this->reason;
        $smh->previous_date = $this->previous_date;
        $smh->previous_time_start = $this->previous_time_start;
        $smh->previous_time_end = $this->previous_time_end;
        $smh->review_mail = $this->review_mail;
        $smh->practice_id = $this->practice_id;
        $smh->source = $this->source;
        $smh->emr_appointment_id = $this->emr_appointment_id;
        $smh->emr_date_sent = $this->emr_date_sent;
        $smh->emr_aditional_information = $this->emr_aditional_information;
        $smh->reminder_mail = $this->reminder_mail;
        $smh->reminder_sms = $this->reminder_sms;
        $smh->reminder_autocall = $this->reminder_autocall;
        $smh->reminder_staffcall = $this->reminder_staffcall;
        $smh->insurance_company_other = $this->insurance_company_other;
        $smh->date_added = date('Y-m-d H:i:s');
        $smh->updated_by_account_id = $this->updated_by_account_id;
        $smh->save();

        //is new record?
        //yes
        if ($this->isNewRecord) {
            // Insert Notification in cpn_communication_queue
            $this->addNotification();
        }

        // Update cache according to status
        $arrSchedulingMailAttributes = (array)$this->attributes;
        if ($this->status == 'A') {
            CapybaraCache::addSchedulingMail($arrSchedulingMailAttributes);
        } else {
            CapybaraCache::removeSchedulingMail($arrSchedulingMailAttributes);
        }

        return parent::afterSave();
    }

    /**
     * Add a notification message in the CpnCommunicationQueue
     * @return void
     */
    public function addNotification()
    {
        // Add notification only for pending status
        if ($this->notificationSent || $this->status != 'P') {
            return false;
        }

        //create our SQL to grab the installation information
        $sql = sprintf(
            "SELECT
                  DISTINCT cpn_installation.id
                FROM cpn_installation
                LEFT JOIN cpn_installation_practice
                  ON cpn_installation_practice.cpn_installation_id = cpn_installation.id
                INNER JOIN cpn_installation_provider
                  ON cpn_installation_provider.cpn_installation_id = cpn_installation.id
                WHERE
                  cpn_installation.enabled_flag = 1 AND
                  cpn_installation.del_flag = 0 AND
                  cpn_installation.allow_installation_flag = 0 AND
                  cpn_installation.installation_identifier IS NOT NULL AND
                  (
                    cpn_installation.practice_id = '%s' OR
                    cpn_installation_practice.practice_id = '%s'
                  ) AND
                  cpn_installation_provider.provider_id = '%s' AND
                  cpn_installation_provider.provider_rating_notification = 1;",
            $this->practice_id,
            $this->practice_id,
            $this->provider_id
        );

        //run the query to get the installation
        $arrCpnInstallation = Yii::app()->db->createCommand($sql)->queryColumn();

        //look for our message type (1 = appt)
        $cpnMessageType = CpnMessageType::model()->find("id = :id", array(":id" => '1'));

        //do we have a message type?
        //no
        if (!$cpnMessageType) {
            //exit the method and return false
            return false;
        }

        //iterate through the installation
        foreach ($arrCpnInstallation as $cpnInstallationId) {
            //create the new database row
            $cpnCommunicationQueue = new CpnCommunicationQueue();
            $cpnCommunicationQueue->cpn_installation_id = $cpnInstallationId;
            $cpnCommunicationQueue->guid = Yii::app()->db->createCommand("SELECT uuid()")->queryScalar();
            $cpnCommunicationQueue->cpn_message_type_id = 1;
            $cpnCommunicationQueue->message = $cpnMessageType->name;
            $cpnCommunicationQueue->url = 'https://' .
                Yii::app()->params['servers']['dr_pro_hn'] .
                '/app/appointments/online-appointment?id=' . $this->id;
            $cpnCommunicationQueue->title = 'Doctor.com ' . $cpnMessageType->name;
            $cpnCommunicationQueue->seconds = '5'; //show in the companion for 5 seconds
            $cpnCommunicationQueue->date_to_publish = date('Y-m-d H:i:s');
            $cpnCommunicationQueue->date_expires = date(
                'Y-m-d H:i:s',
                strtotime($cpnCommunicationQueue->date_to_publish . ' +1 day')
            ); //expire it in 1 day
            $cpnCommunicationQueue->added_by_account_id = '1'; // Tech
            //set up our message json here for a appointment
            //{
            //  "messageIdentifier":"blah",
            //  "messageType":blah,
            //  "url":"blah",
            //  "urlTitle":"blah",
            //  "seconds":blah,
            //  "notificationDate":"blah",
            //  "expiresDate":"blah",
            //  "patient":
            //  {
            //      "patientId":blah,
            //      "firstName":"blah",
            //      "lastName":"blah",
            //      "cellPhone":"blah",
            //      "email":"blah"
            //  },
            //  "appointment":
            //  {
            //      "date":"blah",
            //      "start":"blah",
            //      "end":"blah",
            //      "reason":"blah",
            //      "source":"blah"
            //  }
            //}
            //create our message_json array
            $messageJson = array();
            $messageJson['messageIdentifier'] = $cpnCommunicationQueue->guid;
            $messageJson['messageType'] = $cpnCommunicationQueue->cpn_message_type_id;
            $messageJson['url'] = $cpnCommunicationQueue->url;
            $messageJson['urlTitle'] = $cpnCommunicationQueue->title;
            $messageJson['seconds'] = $cpnCommunicationQueue->seconds;
            $messageJson['notificationDate'] = $cpnCommunicationQueue->date_to_publish;
            $messageJson['expiresDate'] = $cpnCommunicationQueue->date_expires;

            //create our patient array
            $patient = array();
            $patient['patientId'] = $this->patient_id;
            $patient['firstName'] = $this->first_name;
            $patient['lastName'] = $this->last_name;
            //do we have a cell phone?
            //yes
            if (isset($this->cell_phone)) {
                //set the cell phone
                $patient['cellPhone'] = $this->cell_phone;
            }
            //do we have an email?
            //yes
            if (isset($this->email)) {
                //set the email
                $patient['email'] = $this->email;
            }
            //add the patient array
            $messageJson['patient'] = $patient;

            //create our appointment array
            $appt = array();
            $appt['date'] = $this->appointment_date;
            $appt['start'] = $this->time_start;
            $appt['end'] = $this->time_end;

            //do we have the visit reason id?
            //yes
            if (isset($this->visit_reason_id)) {
                //set up the sql to query against the ID
                $sqlVisitReason = sprintf(
                    "select visit_reason.name from visit_reason where visit_reason.id = '%d'",
                    $this->visit_reason_id
                );
                //run the query and get our value into the json
                $appt['reason'] = Yii::app()->db->createCommand($sqlVisitReason)->queryScalar();
            }

            //do we have a source?
            //yes
            if (isset($this->source)) {
                //set the source
                $appt['source'] = $this->source;
            }
            //add the appointment array
            $messageJson['appointment'] = $appt;

            //encode the JSON into our message_json field
            $cpnCommunicationQueue->message_json = json_encode($messageJson);

            //save it in the database
            if ($cpnCommunicationQueue->save()) {
                $this->notificationSent = true;
            }
        }
    }

    /**
     * Send to SF when Freemiun Account get an Appointment request
     * @param int $schedulingMailId
     * @param arr $accountData
     */
    public static function sendToSalesForce($schedulingMailId = 0, $accountData = '', $testingMode = false)
    {

        // Find schedule
        $schedulingMail = SchedulingMail::model()->find('id=:id', array(':id' => $schedulingMailId));
        if (empty($schedulingMail)) {
            return false;
        }

        $providerId = $schedulingMail->provider_id;
        $practiceId = $schedulingMail->practice_id;

        // Find account
        if (empty($accountData)) {
            $accountData = Account::getFreeAccountByProviderPractice($providerId, $practiceId);
        }
        if (empty($accountData)) {
            // not is a free account
            return false;
        }
        // Find provider
        $providerData = Provider::model()->findByPk($providerId);

        if (empty($providerData)) {
            return false;
        }

        // Find practice
        $practiceData = Practice::model()->findByPk($practiceId);

        if (empty($practiceData)) {
            return false;
        }

        $totalAppointmentRequested = SchedulingMail::model()->count(
            'provider_id =:provider_id',
            array(':provider_id' => $providerId)
        );

        $sfLeadId = $accountData['salesforce_lead_id'];

        // if is in testing mode doesn't push data into salesforce
        if ($testingMode) {
            return true;
        }

        $data = array();
        $data['FirstName'] = $providerData['first_name'];
        $data['MiddleName'] = $providerData['middle_name'];
        $data['LastName'] = $providerData['last_name'];
        $data['Salutation'] = $providerData['prefix'];

        $data['Email_for_Patients__c'] = $accountData['email'];
        //do we have an organization?
        //yes
        if (!empty($accountData['organization_name'])) {
            //set it to the organization
            $data['Company'] = $accountData['organization_name'];
        } else {
            //do we have an organization?
            //no
            //set it to the doctor's name
            $data['Company'] = $accountData['first_name'] . ' ' . $accountData['last_name'];
        }
        $data['Phone'] = StringComponent::formatPhone($accountData['phone_number']);
        $data['Doctor_com_Account_Name__c'] = $accountData['first_name'] . ' ' . $accountData['last_name'];

        $data['Doctor_com_Account_Created_On__c'] = TimeComponent::createSalesforceDatetime(
            $accountData['date_added']
        );
        $data['Latest_Appointment_Requested_On__c'] = TimeComponent::createSalesforceDatetime(
            $schedulingMail->date_added
        );
        $requestedFor = $schedulingMail->appointment_date . ' ' . $schedulingMail->time_start;
        $data['Latest_Appointment_Requested_For__c'] = TimeComponent::createSalesforceDatetime($requestedFor);
        $data['Total_Appointments_Requested__c'] = $totalAppointmentRequested;

        $href = 'https://' . Yii::app()->params['servers']['dr_pro_hn'];
        $href .= '/schedulingMail/admin?SchedulingMail[activeClient]=&SchedulingMail[provider_id]=' . $providerId;
        $href .= '&SchedulingMail_sort=appointment_date.desc';
        $data['Appointment_Details_At__c'] = $href;

        //send it to salesforce
        Yii::app()->salesforce->updateLead($sfLeadId, $data);
    }

    /**
     * Called on rendering the column for each row in Admin
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id . '" '
            . 'rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">'
            . $this->provider . '</a>');
    }

    /**
     * Get appointment requests to be displayed in PADM
     * @param array $arrPractices
     * @param string $filterPatient
     * @param string $filterProvider
     * @param string $filterCellPhone
     * @param string $filterEmail
     * @param string $filterDateTime
     * @param string $filterInsurance
     * @param string $filterPractice
     * @param string $filterTimeCondition
     * @param string $order
     * @param string $sort
     * @param int $accountId
     * @return array
     */
    public static function getOnlinePracticeAppointmentRequests(
        $arrPractices = array(),
        $filterPatient = '',
        $filterProvider = '',
        $filterCellPhone = '',
        $filterEmail = '',
        $filterDateTime = '',
        $filterInsurance = '',
        $filterPractice = '',
        $filterTimeCondition = '',
        $order = 'appointment_date',
        $sort = 'DESC',
        $accountId = null // used in the new PADM
    ) {

        if (empty($accountId)) {
            // for the new PADM i need to send by params
            $accountId = Yii::app()->user->id;
            $accountInfo = Account::getInfo($accountId, false, true);
            $accountId = $accountInfo['owner_account_id'];
        } else {
            $accountInfo = Account::getInfo($accountId, false, true);
        }

        if (empty($accountInfo['privileges']['appointment_request'])) {
            // The logged user has no permission to access to the appointments
            return null;
        }

        //$accountInfo['practices'] and $accountInfo['providers'] are expected to be an array of ids
        $accountInfo['providers'] = array_column($accountInfo['providers'], 'id');
        $accountInfo['practices'] = array_column($accountInfo['practices'], 'id');

        // List of the allowed providers
        $arrProviders = [];
        $lstProviders = 0;
        if (!empty($accountInfo['providers'])) {
            $arrProviders = $accountInfo['providers'];
            $lstProviders = implode(',', $arrProviders);
        }

        // List of the allowed practices
        $lstPractices = 0;
        if (!empty($accountInfo['practices'])) {
            $arrPractices = $accountInfo['practices'];
            $lstPractices = implode(',', $arrPractices);
        } else {
            // In case that the permissions only have a provider
            // limit to the ownerAccount -> practices
            $sql = sprintf(
                "SELECT GROUP_CONCAT(DISTINCT account_practice.practice_id SEPARATOR ',') as practice_ids
                FROM provider_practice
                left join account_practice
                    on account_practice.practice_id = provider_practice.practice_id
                WHERE
                    account_practice.account_id = %d
                    AND provider_practice.provider_id IN (%s)",
                $accountInfo['owner_account_id'],
                $lstProviders
            );
            $arrPractices = (array) Yii::app()->db->createCommand($sql)->queryAll();

            if (!empty($arrPractices) && !empty($arrPractices[0]['practice_ids'])) {
                $lstPractices = $arrPractices[0]['practice_ids'];
            }
        }

        // Filter for provider
        $providerCondition = '';
        if ($filterProvider > 0) {
            // selected provider is in my allowed providers?
            if (in_array($filterProvider, $arrProviders)) {
                // Applying filter, display only this provider
                $lstProviders = $filterProvider;
            } else {
                // This provider is not in my allowed providers, so setting as zero
                $lstProviders = 0;
            }
        }
        $providerCondition = sprintf("AND (sm.provider_id IN (%s) ) ", $lstProviders);

        // Filter for practice
        if ($filterPractice > 0) {
            // selected practice is in my allowed practices?
            if (in_array($filterPractice, $arrPractices)) {
                // Applying filter, display only this practice
                $lstPractices = $filterPractice;
            } else {
                // This practice is not in my allowed practices, so setting as zero
                $lstPractices = 0;
            }
        }
        $practiceCondition = sprintf(
            "AND (sm.practice_id IN (%s) OR practice_details.practice_type_id = 5) ",
            $lstPractices
        );

        $having = '';

        if (trim($filterPatient) != '' && $filterPatient != 'Search by name') {
            $having .= ($having == '' ? ' HAVING ' : ' AND ');
            $having .= 'patient LIKE "%' . $filterPatient . '%"';
        }
        if (trim($filterInsurance) != '' && $filterInsurance != 'Search by insurance') {
            $having .= ($having == '' ? ' HAVING ' : ' AND ');
            $having .= 'insurance LIKE "%' . $filterInsurance . '%"';
        }

        // filter Time Condition
        $timeCondition = sprintf(" AND ( (date_time='%s' AND ISNULL(time_start) ) ", $filterDateTime);
        if ($filterDateTime == 'M') {
            $timeCondition .= " OR (time_start BETWEEN '00:00:00' AND '12:59:59') )";
        } elseif ($filterDateTime == 'A') {
            $timeCondition .= " OR (time_start BETWEEN '13:00:00' AND '18:59:59') )";
        } elseif ($filterDateTime == 'E') {
            $timeCondition .= " OR (time_start BETWEEN '19:00:00' AND '23:59:59') )";
        } else {
            $timeCondition = '';
        }

        $ownerAccountId = $accountInfo['owner_account_id'];
        $conditionAccountId = sprintf(" AND account_provider.account_id = %d", $ownerAccountId);

        // query
        // We added LIMIT 1000 (it was 100 previously) as a way to comply with task
        // "PADM 2.0<> Surface All Appointments". However, this is not a long term solution and we have agreed with
        // the FE that we will eventually need to make a paginated call
        $sql = sprintf(
            "SELECT DISTINCT sm.id, sm.provider_id, sm.practice_id, sm.status, sm.appointment_type,
            sm.time_start, sm.time_end, sm.first_name, sm.insurance_plan_types_id, sm.self_paying,
            sm.updated_by_account_id, `account`.email AS account_email, sm.last_name,
            partner_site.display_name as source,
            CONCAT(IF(`provider`.prefix IS NOT NULL, CONCAT(`provider`.prefix, ' '), ''),
            `provider`.first_name, ' ', `provider`.last_name) AS provider_name, practice.name AS practice_name,
            practice.address AS practice_address, CONCAT(sm.first_name, ' ', sm.last_name) AS patient,
            sm.cell_phone, sm.email, appointment_date, sm.date_time, sm.reason, sm.previous_date, sm.date_added,
            insurance_company.name AS insurance, insurance.name AS plan_name, sm.insurance_id,
            sm.insurance_company_id, vr.name AS visit_reason_name, sm.insurance_other_plan,
            sm.emr_appointment_id, sm.emr_date_sent,
            insurance_plan_types.name AS plan_type_name, provider_details.no_insurance_reason,
            patient.date_of_birth AS patient_dob
            FROM scheduling_mail sm
            INNER JOIN `provider`
                ON `provider`.id = sm.provider_id
            LEFT JOIN `account`
                ON account.id = sm.updated_by_account_id
            INNER JOIN account_provider
                ON account_provider.provider_id = `provider`.id
            LEFT JOIN practice
                ON sm.practice_id = practice.id
            LEFT JOIN practice_details
                ON practice.id = practice_details.practice_id
            LEFT JOIN provider_practice
                ON (provider_practice.provider_id=sm.provider_id AND practice.id = provider_practice.practice_id)
            LEFT JOIN partner_site
                ON sm.partner_site_id = partner_site.id
            LEFT JOIN insurance
                ON sm.insurance_id = insurance.id
            LEFT JOIN insurance_company
                ON sm.insurance_company_id = insurance_company.id
            LEFT JOIN insurance_plan_types
                ON sm.insurance_plan_types_id = insurance_plan_types.id
            LEFT JOIN provider_details
                ON sm.provider_id = provider_details.provider_id
            LEFT JOIN visit_reason AS vr
                ON vr.id = sm.visit_reason_id
            LEFT JOIN patient
                ON sm.patient_id = patient.id
            WHERE sm.id > 0
                %s
                %s
                %s
                %s
                %s
                %s
            ORDER BY %s %s LIMIT 1000;",
            $conditionAccountId,
            $practiceCondition,
            $providerCondition,
            $filterTimeCondition,
            $timeCondition,
            $having,
            $order,
            $sort
        );

        $arrData = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($arrData)) {
            foreach ($arrData as $k => $v) {
                $arrData[$k]['insurance_display_name'] = self::prepareInsuranceDisplayName($v);
            }
        }

        return $arrData;
    }

    /**
     * Get the last appointments order by date_added DESC
     * @param int $ownerAccountId
     * @param int $limit
     * @return array
     */
    public static function getLastAppointments($ownerAccountId = 0, $limit = 10)
    {
        $accountId = $ownerAccountId;

        if (empty($accountId)) {
            return false;
        }

        $tmpPractices = AccountPractice::getAccountPractices($accountId);
        $arrPractices = array();
        foreach ($tmpPractices as $pra) {
            $arrPractices[$pra['id']] = $pra['id'];
        }

        $arrProviders = AccountProvider::getAccountProviders($accountId);
        if (empty($arrProviders)) {
            return false;
        }

        $lstProviders = '';
        foreach ($arrProviders as $prov) {
            $lstProviders .= $prov['id'] . ',';
        }
        $lstProviders = '(' . trim($lstProviders, ',') . ')';

        $sql = sprintf(
            "SELECT DISTINCT sm.id, sm.provider_id, sm.practice_id, sm.status, sm.appointment_type,
            sm.time_start, sm.time_end, `provider`.first_name, `provider`.last_name,
            partner_site.display_name as source,
            CONCAT(IF(`provider`.prefix IS NOT NULL, CONCAT(`provider`.prefix, ' '), ''),
            `provider`.first_name, ' ', `provider`.last_name) AS provider_name, practice.name AS practice_name,
            practice.address AS practice_address,  CONCAT(sm.first_name, ' ', sm.last_name) AS patient,
            sm.cell_phone, sm.email, appointment_date, sm.date_time, sm.reason, sm.previous_date, sm.date_added,
            insurance_company.name AS insurance, insurance.name AS plan_name, sm.insurance_id,
            sm.insurance_company_id, vr.name AS visit_reason_name, sm.insurance_other_plan,
            sm.emr_appointment_id, sm.emr_date_sent
            FROM scheduling_mail sm
            INNER JOIN `provider`
                ON `provider`.id = sm.provider_id
            INNER JOIN account_provider
                ON account_provider.provider_id = `provider`.id
            LEFT JOIN practice
                ON sm.practice_id = practice.id
            LEFT JOIN provider_practice
                ON (provider_practice.provider_id=sm.provider_id AND practice.id = provider_practice.practice_id)
            LEFT JOIN insurance
                ON sm.insurance_id = insurance.id
            LEFT JOIN insurance_company
                ON sm.insurance_company_id = insurance_company.id
            LEFT JOIN partner_site
                ON sm.partner_site_id = partner_site.id
            LEFT JOIN visit_reason AS vr
                ON vr.id = sm.visit_reason_id
            WHERE sm.id > 0
                AND sm.provider_id IN %s
                ORDER BY date_added DESC LIMIT %d;",
            $lstProviders,
            $limit
        );
        $lastAppointments = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($lastAppointments)) {
            // search and Add comments
            foreach ($lastAppointments as $key => $value) {
                if (!in_array($value['practice_id'], $arrPractices)) {
                    unset($lastAppointments[$key]);
                } else {
                    $lastAppointments[$key]['comments'] = SchedulingMailComment::getComments(
                        $value['id'],
                        $ownerAccountId
                    );
                }
            }
        }
        return $lastAppointments;
    }

    /**
     * PADM: Get details about an scheduling_mail entry
     * @param int $schedulingMailId
     * @param int $accountId
     * @return array
     */
    public static function getOnlineAppointmentRequestDetails($schedulingMailId, $accountId = 0)
    {

        // Switch to the owner, to validate that it can access this record
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);

        $sql = sprintf(
            "SELECT sm.*, insurance_company.name AS insurance, insurance.name AS plan_name,
            insurance_plan_types.name AS plan_type_name, provider_details.no_insurance_reason,
            vr.name AS visit_reason_name, sm.emr_appointment_id, sm.emr_date_sent, '' AS patient_details,
            patient.date_of_birth AS patient_dob
            FROM scheduling_mail sm
            LEFT JOIN account_provider
                ON (account_provider.provider_id = sm.provider_id AND account_provider.account_id = %d)
            LEFT JOIN account_practice
                ON (account_practice.practice_id = sm.practice_id AND account_practice.account_id = %d)
            LEFT JOIN provider_details
                ON sm.provider_id = provider_details.provider_id
            LEFT JOIN insurance
                ON sm.insurance_id = insurance.id
            LEFT JOIN insurance_company
                ON sm.insurance_company_id = insurance_company.id
            LEFT JOIN insurance_plan_types
                ON sm.insurance_plan_types_id = insurance_plan_types.id
            LEFT JOIN visit_reason AS vr
                ON sm.visit_reason_id = vr.id
            LEFT JOIN patient
                ON sm.patient_id = patient.id
            WHERE sm.id = %d",
            $ownerAccountId,
            $ownerAccountId,
            $schedulingMailId
        );

        $arrData = Yii::app()->db->createCommand($sql)->queryRow();
        if (empty($arrData)) {
            return array();
        }
        // Get the correct source based on partner site
        $partnerSite = PartnerSite::model()->cache(Yii::app()->params['cache_short'])->findByPk(
            $arrData['partner_site_id']
        );
        if (!empty($partnerSite)) {
            $arrData['source'] = !empty($partnerSite->display_name) ? $partnerSite->display_name : $arrData['source'];
        }

        if ($arrData['appointment_for'] == 'Other' && $arrData['patient_id'] > 0) {
            // find who made the appointment
            $arrData['patient_details'] = Patient::model()->getPatientDetails($arrData['patient_id']);
        }

        $arrData['visit_reason_duration'] = date(
            "i",
            strtotime("00:00:00")  + strtotime($arrData['time_end']) - strtotime($arrData['time_start'])
        );

        if (!empty($arrData) && !empty($arrData['visit_reason_id'])) {
            $sql = sprintf(
                "SELECT ppvr.appointment_visit_duration AS visit_reason_duration
                FROM provider_practice_visit_reason AS ppvr
                WHERE ppvr.visit_reason_id = %d
                    AND ppvr.provider_id = %d
                    AND ppvr.practice_id = %d",
                $arrData['visit_reason_id'],
                $arrData['provider_id'],
                $arrData['practice_id']
            );
            $arrVR = Yii::app()->db->createCommand($sql)->queryRow();
            if (!empty($arrVR['visit_reason_duration'])) {
                $arrData['visit_reason_duration'] = $arrVR['visit_reason_duration'];
            }
        }
        // get appointment comments
        $arrData['comments'] = SchedulingMailComment::getComments($schedulingMailId, $accountId);
        $arrData['insurance_display_name'] = self::prepareInsuranceDisplayName($arrData);

        return $arrData;
    }

    /**
     * Prepare Insurance Display Name
     * @param array $smRec
     * @param bool $isForPatientEmail
     * @return string $displayName
     */
    public static function prepareInsuranceDisplayName($smRec = array(), $isForPatientEmail = false)
    {
        $insuranceDisplayName = 'N/A';
        if (empty($smRec['no_insurance_reason'])) {
            if (!empty($smRec['insurance'])) {

                if (!empty($smRec['insurance_other_plan'])) {
                    // Other plan
                    $insuranceDisplayName = trim($smRec['insurance']) . ' - ' . trim($smRec['insurance_other_plan']);
                } elseif (!empty($smRec['plan_type_name'])) {
                    // Plan type
                    $insuranceDisplayName = trim($smRec['insurance']) . ' - ' . trim($smRec['plan_type_name']);
                } else {
                    // Insurance plan
                    $insuranceDisplayName = trim($smRec['insurance']) . ' - ' . trim($smRec['plan_name']);
                }
            } else {
                // No insurance_company is selected
                if (!empty($smRec['self_paying']) && $smRec['self_paying'] == 1) {
                    // Self Paying
                    $insuranceDisplayName = 'Self-paying';
                } else {
                    // Other
                    $insuranceDisplayName = 'Other';
                }
            }
        }

        if ($isForPatientEmail) {
            // Change it when is called for patient email
            if ($insuranceDisplayName == 'Other') {
                $insuranceDisplayName = "The office may reach out to you to verify your insurance. ";
                $insuranceDisplayName .= "If you have any questions, you can contact the office at %p_phone_number%.";
            } elseif ($insuranceDisplayName == 'N/A') {
                $insuranceDisplayName = $smRec['no_insurance_reason'];
            }
        }

        return $insuranceDisplayName;
    }

    /**
     * PADM: Get list of online appointment requests
     * @param int $providerId
     * @param string $filterPatient
     * @param string $filterCellPhone
     * @param string $filterEmail
     * @param string $filterAppointmentDate
     * @param string $filterDateTime
     * @param string $filterInsurance
     * @param string $filterTimeCondition
     * @param string $filterAddedCondition
     * @param string $order
     * @param string $sort
     * @return array
     */
    public static function getOnlineAppointmentRequests(
        $providerId,
        $filterPatient,
        $filterCellPhone,
        $filterEmail,
        $filterAppointmentDate,
        $filterDateTime,
        $filterInsurance,
        $filterTimeCondition,
        $filterAddedCondition,
        $order,
        $sort
    ) {

        $condition = '';
        if (trim($filterPatient) != '' && $filterPatient != 'Search by name') {
            $condition .= 'AND (patient LIKE "%' . $filterPatient . '%")';
        }
        if (trim($filterCellPhone) != '' && $filterCellPhone != 'Search by number') {
            $condition .= 'AND cell_phone LIKE "%' . $filterCellPhone . '%"';
        }
        if (trim($filterEmail) != '' && $filterEmail != 'Search by email') {
            $condition .= 'AND email LIKE "%' . $filterEmail . '%"';
        }
        if (trim($filterAppointmentDate) != '' && $filterAppointmentDate != 'Search by date') {
            $condition .= 'AND (appointment_date = "' . $filterAppointmentDate . '")';
        }
        if (trim($filterDateTime) != '' && $filterDateTime != 'Search by time') {
            $condition .= 'AND date_time = "' . $filterDateTime . '"';
        }
        if (trim($filterInsurance) != '' && $filterInsurance != 'Search by insurance') {
            $condition .= 'AND insurance LIKE "%' . $filterInsurance . '%"';
        }

        $condition .= $filterTimeCondition;
        $condition .= $filterAddedCondition;

        // query
        $sql = sprintf(
            "SELECT sm.id, CONCAT(sm.first_name, ' ', sm.last_name) AS patient, sm.cell_phone,
            sm.email, appointment_date, sm.date_time, vr.name AS visit_reason_name,
            CONCAT(insurance_company.name, ' - ', insurance.name) AS insurance,
            sm.date_added, insurance_plan_types.name AS plan_type_name, provider_details.no_insurance_reason
            FROM scheduling_mail sm
            LEFT JOIN insurance
                ON sm.insurance_id = insurance.id
            LEFT JOIN insurance_company
                ON insurance.company_id = insurance_company.id
            LEFT JOIN insurance_plan_types
                ON sm.insurance_plan_types_id = insurance_plan_types.id
            LEFT JOIN provider_details
                ON sm.provider_id = provider_details.provider_id
            LEFT JOIN visit_reason AS vr
                ON sm.visit_reason_id = vr.id
            WHERE sm.provider_id = %d
            HAVING 1=1
            %s
            ORDER BY %s %s;",
            $providerId,
            $condition,
            $order,
            $sort
        );

        $arrData = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($arrData)) {
            foreach ($arrData as $k => $v) {
                $arrData[$k]['insurance_display_name'] = self::prepareInsuranceDisplayName($v);
            }
        }

        return $arrData;
    }

    /**
     * Checks a record's ownership
     * @param int $schedulingMailId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($schedulingMailId, $accountId)
    {
        $sql = sprintf(
            "SELECT account_id
            FROM account_provider
            INNER JOIN scheduling_mail
                ON account_provider.provider_id = scheduling_mail.provider_id
            WHERE scheduling_mail.id = %d
            AND account_id = %d LIMIT 1;",
            $schedulingMailId,
            $accountId
        );
        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return !empty($result) ? (int)$result : false;
    }

    /**
     * Command: Gets the appointment requests for the performance by account report
     * @return array
     */
    public static function getAppointmentRequestsPerPartner()
    {

        $sql = sprintf(
            "SELECT account_id, partner_site_id, YEAR(scheduling_mail.date_added) AS appt_year,
            MONTH(scheduling_mail.date_added) AS appt_month, COUNT(1) AS appt_count
            FROM scheduling_mail
            INNER JOIN account_provider
                ON scheduling_mail.provider_id = account_provider.provider_id
                AND (scheduling_mail.date_added > date_sub(NOW(), INTERVAL 12 MONTH)
                AND scheduling_mail.date_added <= NOW())
            GROUP BY account_id, partner_site_id, MONTH(scheduling_mail.date_added), YEAR(scheduling_mail.date_added);"
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Find if this new appointment is free or not
     * @param $appData
     * $appData['schedulingMailId'] = Yii::app()->request->getParam('schedulingMailId', false);
     * $appData['newStatus'] = Yii::app()->request->getParam('newStatus', false);
     * $appData['newDate'] = Yii::app()->request->getParam('newDate', false);
     * $appData['startHour'] = Yii::app()->request->getParam('startHour', false);
     * $appData['startMin'] = Yii::app()->request->getParam('startMin', false);
     * $appData['startAmPm'] = Yii::app()->request->getParam('startAmPm', false);
     * $appData['endHour'] = Yii::app()->request->getParam('endHour', false);
     * $appData['endMin'] = Yii::app()->request->getParam('endMin', false);
     * $appData['endAmPm'] = Yii::app()->request->getParam('endAmPm', false);
     * $appData['overBooking'] = Yii::app()->request->getParam('overBooking', 0);
     * $appData['practiceId'] = Yii::app()->request->getParam('practiceId', 0);
     * @return bool $isFree
     */
    public static function isAppointmentFree($appData = '')
    {
        $schedulingMail = SchedulingMail::model()->find('id=:id ', array(':id' => $appData['schedulingMailId']));

        if (
            $appData['newStatus'] != 'R' && (empty($schedulingMail) || $appData['newStatus'] != $schedulingMail->status)
        ) {

            if ($appData['newStatus'] == 'A') {
                // Appointment Approve
                $newStartHour = $schedulingMail->time_start;
                $newEndHour = $schedulingMail->time_end;
                $appData['newDate'] = $schedulingMail->appointment_date;
            } elseif ($appData['newStatus'] == 'RE') {

                // Appointment Reschedule
                $newStartHour = date(
                    "H:i:s",
                    strtotime($appData['startHour'] . ':' . $appData['startMin'] . ' ' . $appData['startAmPm'])
                );
                $newEndHour = date(
                    "H:i:s",
                    strtotime($appData['endHour'] . ':' . $appData['endMin'] . ' ' . $appData['endAmPm'])
                );
            }

            // Disallow overbooking
            $allowOverbooking = false;

            // Get operatories at Provider-Practice level
            $operatories = ProviderPractice::getOperatories($schedulingMail->practice_id, $schedulingMail->provider_id);

            // Filter by ProviderId by default
            $providerSql = 'provider_id = ' . $schedulingMail->provider_id . ' AND ';

            // If there's no operatories, get them at Practice level
            if (!$operatories) {
                $operatories = Practice::getOperatories($schedulingMail->practice_id);

                // If there's operatories at Practice level, modify query to include all appointments for Practice
                if ($operatories) {
                    $providerSql = '';
                }
            }

            // Set default value in case there's no operatories
            if (!$operatories) {
                $operatories = 1;
                // Allow overbooking, since the are no operatories set
                $allowOverbooking = true;
            }

            $sql = sprintf(
                "SELECT COUNT(*)
                FROM scheduling_mail
                WHERE %s practice_id = %d AND id<>%d
                    AND appointment_date='%s'
                    AND status <> 'R'
                    AND status <> 'P'
                    AND (
                        (TIME_TO_SEC('%s') < TIME_TO_SEC(time_start) AND TIME_TO_SEC('%s') > TIME_TO_SEC(time_end))
                        OR (TIME_TO_SEC('%s') >= TIME_TO_SEC(time_start) AND TIME_TO_SEC('%s') < TIME_TO_SEC(time_end))
                        OR (TIME_TO_SEC('%s') > TIME_TO_SEC(time_start) AND TIME_TO_SEC('%s') <= TIME_TO_SEC(time_end))
                    )",
                $providerSql,
                $schedulingMail->practice_id,
                $appData['schedulingMailId'],
                $appData['newDate'],
                $newStartHour,
                $newEndHour,
                $newStartHour,
                $newStartHour,
                $newEndHour,
                $newEndHour
            );
            $appointmentsCount = Yii::app()->db->createCommand($sql)->queryScalar();

            if (empty($appointmentsCount) || $appointmentsCount < $operatories) {
                // This appointment is free for use
                return 1;
            }

            if ($allowOverbooking && $appData['overBooking'] == 1) {
                // Has overBooking
                return 1;
            }
        }

        // Appointment date-time is in use
        return 0;
    }

    /**
     * Find if this new appointment is inside practice office hours range
     * @param $appData
     * $appData['schedulingMailId'] = Yii::app()->request->getParam('schedulingMailId', false);
     * $appData['newStatus'] = Yii::app()->request->getParam('newStatus', false);
     * $appData['newDate'] = Yii::app()->request->getParam('newDate', false);
     * $appData['startHour'] = Yii::app()->request->getParam('startHour', false);
     * $appData['startMin'] = Yii::app()->request->getParam('startMin', false);
     * $appData['startAmPm'] = Yii::app()->request->getParam('startAmPm', false);
     * $appData['endHour'] = Yii::app()->request->getParam('startHour', false);
     * $appData['endMin'] = Yii::app()->request->getParam('startMin', false);
     * $appData['endAmPm'] = Yii::app()->request->getParam('startAmPm', false);
     * $appData['overBooking'] = Yii::app()->request->getParam('overBooking', 0);
     * $appData['practiceId'] = Yii::app()->request->getParam('practiceId', 0);
     * @return bool $isFree
     */
    public static function isInsidePracticeOfficeHours($appData = '')
    {
        $isFree = 1;

        if ($appData['newStatus'] == 'RE' && !empty($appData['practiceId'])) {
            $sm = SchedulingMail::model()->findByPk($appData['schedulingMailId']);
            $practiceOfficeHours = PracticeOfficeHour::getOfficeHours($appData['practiceId'], true, $sm->provider_id);

            if (!empty($practiceOfficeHours)) {

                // New date time setting for reschedule
                list($year, $month, $day) = explode('-', $appData['newDate']);
                $newHour = date(
                    "H:i",
                    strtotime($appData['startHour'] . ':' . $appData['startMin'] . ' ' . $appData['startAmPm'])
                );
                $dayOfWeek = date('w', mktime(0, 0, 0, $month, $day, $year));

                // look for the day of the week
                $recPOH = null;
                foreach ($practiceOfficeHours as $day) {
                    $dayNumber = !empty($day['day_number']) ? $day['day_number'] : $day['id'];
                    if ($dayNumber == $dayOfWeek) {
                        $recPOH = $day;
                        break;
                    }
                }

                if (!empty($recPOH)) {
                    $amPmFrom = ($recPOH['amFrom'] == 'selected') ? 'AM' : 'PM';
                    $rangeFrom = date(
                        "H:i",
                        strtotime($recPOH['hoursFrom'] . ':' . $recPOH['minsFrom'] . ' ' . $amPmFrom)
                    );
                    $amPmTo = ($recPOH['amTo'] == 'selected') ? 'AM' : 'PM';
                    $rangeTo = date("H:i", strtotime($recPOH['hoursTo'] . ':' . $recPOH['minsTo'] . ' ' . $amPmTo));
                    if ($newHour < $rangeFrom || $newHour >= $rangeTo) {
                        // New time is outside practice office range
                        $isFree = 3;
                    }
                } else {
                    // This day the practice is closed
                    $isFree = 3;
                }
            }
        }
        return $isFree;
    }

    /**
     * Get a list of appointments whose notifications need to be sent (mail, sms, autocall, staff call)
     * Conditions: one of the reminders is set in the appointment, days to appointment between 0 and 4, hours to
     * appointment match what's set by the practice in account_practice_emr
     * The "5 MINUTE" thing should be updated if the schedule for Jenkins is updated
     * @see AppointmentCommand.php::sendPatientReminders()
     * @return array
     */
    public static function getPendingPatientReminders()
    {

        $sql = sprintf(
            "SELECT *, scheduling_mail.id AS s_id, scheduling_mail.patient_id AS s_patient_id,
            scheduling_mail.practice_id AS s_practice_id, scheduling_mail.provider_id AS s_provider_id,
            IF(reminder_mail = 1 AND pre_appt_reminder_mail > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_mail HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_mail,
            IF(reminder_sms = 1 AND pre_appt_reminder_sms > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_sms HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_sms,
            IF(reminder_autocall = 1 AND pre_appt_reminder_autocall > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_autocall HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_autocall,
            IF(reminder_staffcall = 1 AND pre_appt_reminder_staffcall > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_staffcall HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_staffcall
            FROM scheduling_mail
            INNER JOIN account_practice_emr
                ON scheduling_mail.practice_id = account_practice_emr.practice_id
            INNER JOIN account_emr
                ON account_emr.account_id = account_practice_emr.account_id
            WHERE (appointment_type = 'S' OR appointment_type = 'V')
                AND account_emr.emr_type_id = 3
                AND status = 'A'
                AND (
                    (reminder_mail = 1 AND  pre_appt_reminder_mail > 0
                    AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_mail HOUR)
                    BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))

                    OR (reminder_sms = 1 AND  pre_appt_reminder_sms > 0
                    AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_sms HOUR)
                    BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))

                    OR (reminder_autocall = 1 AND  pre_appt_reminder_autocall > 0
                    AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_autocall HOUR)
                    BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))

                    OR (reminder_staffcall = 1 AND  pre_appt_reminder_staffcall > 0
                    AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_staffcall HOUR)
                    BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))
                )
            GROUP BY scheduling_mail.id;"
        );
        $arrScheduling = Yii::app()->dbRO->createCommand($sql)->queryAll();

        $sql = sprintf(
            "SELECT *, scheduling_mail.id AS s_id, scheduling_mail.patient_id AS s_patient_id,
            scheduling_mail.practice_id AS s_practice_id, scheduling_mail.provider_id AS s_provider_id,
            IF(reminder_mail = 1 AND pre_appt_reminder_mail > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_mail HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_mail,
            IF(reminder_sms = 1 AND pre_appt_reminder_sms > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_sms HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_sms,
            IF(reminder_autocall = 1 AND pre_appt_reminder_autocall > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_autocall HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_autocall,
            IF(reminder_staffcall = 1 AND pre_appt_reminder_staffcall > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_staffcall HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_staffcall
            FROM scheduling_mail
            INNER JOIN account_practice_emr
                ON scheduling_mail.practice_id = account_practice_emr.practice_id
            INNER JOIN account_emr ON account_emr.account_id = account_practice_emr.account_id
            WHERE (appointment_type = 'S' OR appointment_type = 'V')
                AND account_emr.emr_type_id != 3
                AND (
                    scheduling_mail.emr_appointment_id IS NOT NULL
                    OR
                    (account_emr.emr_type_id = 4 AND
                        (SELECT id FROM appointment
                            WHERE appointment.provider_id = scheduling_mail.provider_id AND
                            appointment.practice_id = scheduling_mail.provider_id AND
                            appointment.appointment_date = scheduling_mail.appointment_date AND
                            appointment.time_start = scheduling_mail.time_start AND
                            appointment.first_name = scheduling_mail.first_name AND
                            appointment.last_name = scheduling_mail.last_name) IS NULL
                    )
                )
                AND status = 'A'
                AND (
                    (reminder_mail = 1 AND  pre_appt_reminder_mail > 0
                    AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_mail HOUR)
                    BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))

                    OR (reminder_sms = 1 AND  pre_appt_reminder_sms > 0
                    AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_sms HOUR)
                    BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))

                    OR (reminder_autocall = 1 AND  pre_appt_reminder_autocall > 0
                    AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_autocall HOUR)
                    BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))

                    OR (reminder_staffcall = 1 AND  pre_appt_reminder_staffcall > 0
                    AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_staffcall HOUR)
                    BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))
                )
            GROUP BY scheduling_mail.id;"
        );
        $arrEHRScheduling = Yii::app()->dbRO->createCommand($sql)->queryAll();

        return array_merge($arrScheduling, $arrEHRScheduling);
    }

    /**
     * Return formatted time
     * @return string
     */
    public function getFormattedTime()
    {
        $return = '';
        if (!empty($this->time_start)) {
            $arrTime = explode(":", $this->time_start);
            $formatedDate = mktime($arrTime[0], $arrTime[1], $arrTime[2], date("m"), date("d"), date("Y"));
            $return = date("h:i a", $formatedDate);
        } else {
            if ($this->date_time == 'M') {
                $return = 'Morning';
            } elseif ($this->date_time == 'A') {
                $return = 'Afternoon';
            } else {
                $return = 'Evening';
            }
        }
        return $return;
    }

    /**
     * Return formatted time
     *
     * @return string
     */
    public function getFormattedStatus()
    {
        switch ($this->status) {
            case '':
                return 'Request only';
                break;
            case 'P':
                return 'Pending';
                break;
            case 'A':
                return 'Approved';
                break;
            case 'N':
                return 'Never confirmed';
                break;
            case 'R':
                return 'Rejected';
                break;
            case 'H':
                return 'Handling reschedule';
                break;
            default:
                return $this->status;
                break;
        }
    }

    /**
     * Find Source in partner by partner->name and return partner->display_name
     * @param string $source
     * @return string
     */
    public static function getFixedSourceName($source = 'doctor', $partnerSiteId = 0)
    {
        $source = trim(str_replace("Direct Appointment: ", "", $source));
        $displayName = $source;
        $lowerSource = strtolower($source);
        if ($lowerSource == 'https://wellness.com') {
            $lowerSource = $source = 'wellness';
        }
        if ($partnerSiteId == 63) {
            $displayName = '<a href="' . $source . '" target="dse" title="' . $source . '">Doctor.com SiteEnhance</a>';
        } else {
            $partner = PartnerSite::model()->cache(Yii::app()->params['cache_short'])->find(
                'name = :name',
                array(':name' => $lowerSource)
            );
            if (isset($partner['display_name'])) {
                $displayName = trim($partner['display_name']);
            }
        }

        return $displayName;
    }

    /**
     * Return partner_site->id from source
     * @param string $source
     * @return int or null
     */
    public static function getPartnerBySource($source = '')
    {
        $source = trim(str_replace("Direct Appointment: ", "", $source));
        $lowerSource = strtolower($source);
        if ($lowerSource == 'https://wellness.com') {
            $lowerSource = $source = 'wellness';
        }
        $partner = PartnerSite::model()->cache(Yii::app()->params['cache_long'])->find(
            'name = :name',
            array(':name' => $lowerSource)
        );

        if (!empty($partner['id'])) {
            return $partner['id'];
        }
        return null;
    }

    /**
     * Return formatted appointment type
     *
     * @return string
     */
    public function getFormattedAppointmentType()
    {
        switch ($this->appointment_type) {
            case 'R':
                return 'Appointment request';
                break;
            case 'S':
                return 'Direct Appointment';
                break;
            case 'V':
                return 'Telemedicine Appointment';
                break;
            default:
                return $this->appointment_type;
                break;
        }
    }

    /**
     * Return how long an appointment as been waiting to be approved, or nothing
     *
     * @return string
     */
    public function getFormattedWaitingFor()
    {
        if (($this->appointment_type == 'S' || $this->appointment_type == 'V') && $this->status == 'P') {
            // if the appointment is pending, show how long it's been like that
            return TimeComponent::getDateDiff(time(), $this->date_added, 1);
        }
        return '-';
    }

    /**
     * Retrieve the pending appointment requests created more than 24 hours ago.
     * @param bool $limit defaults to false, set to true for testing purposes
     * @return array
     */
    public static function getOldPendingRequests($limit = false)
    {

        $sql = sprintf(
            "SELECT a.organization_name AS account, pro.first_last AS provider, pra.name AS practice,
            pra.phone_number AS practice_phone_number,
            CONCAT(pat.first_name, ' ', pat.last_name) AS requested_by,
            IF (appointment_for = 'Self', 'Self', CONCAT(sm.first_name, ' ', sm.last_name)) AS appointment_for ,
            sm.cell_phone AS patient_cell_phone,
            date_format(sm.date_added, '%%c/%%e/%%Y') AS date_added,
            date_format(sm.appointment_date, '%%c/%%e/%%Y') AS appointment_date,
            DATE_FORMAT(time_start, '%%l:%%i %%p') AS time_start,
            DATE_FORMAT(time_end, '%%l:%%i %%p') AS time_end,
            vr.name AS visit_reason, sm.reason AS details, o.salesforce_id AS salesforce_id
            FROM scheduling_mail sm
            INNER JOIN provider pro
                ON sm.provider_id = pro.id
            INNER JOIN practice pra
                ON sm.practice_id = pra.id
            INNER JOIN account_practice apra
                ON pra.id = apra.practice_id
            INNER JOIN account a
                ON apra.account_id = a.id
            INNER JOIN patient pat
                ON sm.patient_id = pat.id
            INNER JOIN visit_reason vr
                ON sm.visit_reason_id = vr.id
            INNER JOIN organization_account oa
                ON a.id = oa.account_id
            INNER JOIN organization o
                ON oa.organization_id = o.id
            WHERE sm.status = 'P' AND sm.appointment_date < '%s'
            ORDER BY sm.id ASC
            %s;",
            date("Y-m-d", time() - 60 * 60 * 24),
            ($limit ? 'LIMIT 1' : '')
        );
        $schedulingMails = Yii::app()->db->createCommand($sql)->queryAll();

        $result = array();
        foreach ($schedulingMails as $sm) {
            $sm['account_rep'] = '';
            $sm['salesforce_link'] = '';
            $sm['account_rep_email'] = '';

            // get the salesforce account details
            $sfDetails = Account::getSalesforceDetails($sm['salesforce_id']);
            if ($sfDetails) {
                $sm['account_rep'] = $sfDetails['account_rep'];
                $sm['salesforce_link'] = $sfDetails['salesforce_link'];
                $sm['account_rep_email'] = $sfDetails['account_rep_email'];
            }

            $result[] = $sm;
        }

        // order by account_rep
        usort($result, function ($a, $b) {
            return strnatcmp($b['account_rep'], $a['account_rep']);
        });

        return $result;
    }

    /**
     * Get the list of pending appointment requests in HTML format.
     * @param array $pendingRequests
     * @return string
     */
    public function getPendingRequestsTable($pendingRequests)
    {
        $style = "style=\"padding-right: 10px;\"";
        $head = "<table>";
        $head .= "<tr>";
        $head .= "<th>Created</th>";
        $head .= "<th>Appt. Date</th>";
        $head .= "<th>Account</th>";
        $head .= "<th>Salesforce Link</th>";
        $head .= "<th>Appt. Summary</th>";
        $head .= "</tr>";
        $footer = "";
        $accountRep = false;
        $result = "";

        foreach ($pendingRequests as $request) {
            if ($accountRep !== $request['account_rep']) {
                $result .= $footer . "<br /><h2> Account Rep: "
                    . (!empty(trim($request['account_rep'])) ? $request['account_rep'] : 'Other') . "</h2>";
                $result .= $head;
                $footer = "</table>";
                $accountRep = $request['account_rep'];
            }
            $result .= "<tr>";
            $result .= "<td " . $style . ">" . $request['date_added'] . "</td>";
            $result .= "<td " . $style . ">" . $request['appointment_date'] . "</td>";
            $result .= "<td " . $style . ">" . $request['account'] . "</td>";
            $result .= "<td " . $style . ">" . ($request['salesforce_link'] ? "<a href=\"" .
                $request['salesforce_link'] . "\">" . $request['salesforce_link'] . "</a>" : "") . "</td>";
            $result .= "<td " . $style . ">" . $request['time_start'] . " - " . $request['time_end'] . " for " .
                $request['provider'] . " at " . $request['practice']
                . " (" . $request['practice_phone_number'] . ")</td>";
            $result .= "</tr>";
        }
        $result .= $footer;

        return $result;
    }

    /**
     * Get the list of pending appointment requests in plain text format.
     * @param array $pendingRequests
     * @return string
     */
    public function getPendingRequestsList($pendingRequests)
    {
        $result = "";
        $footer = "";
        $accountRep = false;
        foreach ($pendingRequests as $request) {
            if ($accountRep !== $request['account_rep']) {
                $result .= $footer . "Account Rep: "
                    . (!empty(trim($request['account_rep'])) ? $request['account_rep'] : 'Other')
                    . PHP_EOL;
                $footer = PHP_EOL . PHP_EOL;
                $accountRep = $request['account_rep'];
            }
            $result .= "Created: " . $request['date_added'] . ", ";
            $result .= "Appt. Date: " . $request['appointment_date'] . ", ";
            $result .= "Account: " . $request['account'] . ", ";
            $result .= "Salesforce Link: " . $request['salesforce_link'] . ", ";
            $result .= "Appt. Summary: " . $request['time_start'] . " - " . $request['time_end'] . " for " .
                $request['provider'] . " at " . $request['practice'] . " (" . $request['practice_phone_number'] . ")";
            $result .= PHP_EOL;
        }
        return $result;
    }

    /**
     * Get a summary of the appointment requests for paying clients in the last week.
     * @return array
     */
    public static function getLastWeekRequests()
    {
        $sql = "SELECT
            a.organization_name AS account,
            pro.first_last AS `provider`,
            pra.name AS practice, pra.phone_number AS practice_phone_number,
            CONCAT(pat.first_name, ' ', pat.last_name) AS requested_by,
            IF (appointment_for = 'Self', 'Self', CONCAT(sm.first_name, ' ', sm.last_name)) AS appointment_for ,
            sm.cell_phone AS patient_cell_phone,
            date_format(sm.date_added, '%c/%e/%Y') AS date_added,
            date_format(sm.appointment_date, '%c/%e/%Y') AS appointment_date,
            DATE_FORMAT(time_start, '%l:%i %p') AS time_start,
            DATE_FORMAT(time_end, '%l:%i %p') AS time_end,
            vr.name AS visit_reason,
            sm.reason AS details,
            sm.status,
            o.salesforce_id AS salesforce_id
            FROM scheduling_mail sm
            INNER JOIN `provider` pro
                ON sm.provider_id = pro.id
            INNER JOIN practice pra
                ON sm.practice_id = pra.id
            INNER JOIN account_practice apra
                ON pra.id = apra.practice_id
            INNER JOIN account a
                ON apra.account_id = a.id
            INNER JOIN patient pat
                ON sm.patient_id = pat.id
            INNER JOIN visit_reason vr
                ON sm.visit_reason_id = vr.id
            INNER JOIN organization_account oa
                ON a.id = oa.account_id
            INNER JOIN organization o
                ON oa.organization_id = o.id
            WHERE sm.date_added BETWEEN DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 7 DAY), '%Y-%m-%d 00:00:00')
            AND DATE_FORMAT(DATE_SUB(NOW(), INTERVAL 1 DAY), '%Y-%m-%d 23:59:59') AND o.status != 'C'
            ORDER BY sm.id ASC;";

        $schedulingMails = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->query();

        $result = array();
        foreach ($schedulingMails as $sm) {
            $sm['account_rep'] = '';
            $sm['salesforce_link'] = '';
            $sm['account_rep_email'] = '';
            if ($sm['salesforce_id']) {
                // get the salesforce account details
                $sfDetails = Account::getSalesforceDetails($sm['salesforce_id']);
                if ($sfDetails) {
                    $sm['account_rep'] = $sfDetails['account_rep'];
                    $sm['salesforce_link'] = $sfDetails['salesforce_link'];
                    $sm['account_rep_email'] = $sfDetails['account_rep_email'];
                }
            }
            $result[] = $sm;
        }

        // order by account_rep
        usort($result, function ($a, $b) {
            return strnatcmp($b['account_rep'] . $b['account'], $a['account_rep'] . $a['account']);
        });

        return $result;
    }

    /**
     * Get the list of appointment requests in HTML format.
     * @param array $requests
     * @return string
     */
    public static function getLastWeekRequestsTable($requests)
    {
        $style = "style=\"padding-right: 10px;\"";
        $head = "<table>";
        $head .= "<tr>";
        $head .= "<th>Created</th>";
        $head .= "<th>Appt. Date</th>";
        $head .= "<th>Status</th>";
        $head .= "<th>Appt. Summary</th>";
        $head .= "</tr>";
        $footer = "";
        $accountName = false;
        $arrStatus = array(
            "P" => "Pending",
            "A" => "Approved",
            "R" => "Rejected"
        );
        $result = "<br /><h2> Account Rep: "
            . (empty($requests[0])
                ? ''
                : (!empty(trim($requests[0]['account_rep'])) ? $requests[0]['account_rep'] : 'Other')) .
            "</h2>";
        $count = 0;
        foreach ($requests as $request) {

            if (!$accountName !== $request['account']) {
                if ($accountName) {
                    $result = str_replace('%%count%%', $count, $result);
                    $count = 0;
                }

                $result .= $footer;
                $result .= "<p><b>Account: "
                    . (!empty($request['salesforce_link'])
                        ? "<a href=\"" . $request['salesforce_link'] . "\">" . $request['account'] . "</a>"
                        : $request['account'])
                    . "</b></p>";
                $result .= "<p>Total appointments: %%count%% </p>";

                $result .= $head;
                $footer = "</table><br />";

                $accountName = $request['account'];
            }

            $result .= "<tr>";
            $result .= "<td " . $style . ">" . $request['date_added'] . "</td>";
            $result .= "<td " . $style . ">" . $request['appointment_date'] . "</td>";
            $result .= "<td " . $style . ">" . $arrStatus[$request['status']] . "</td>";
            $result .= "<td " . $style . ">" . $request['time_start'] . " - " . $request['time_end'] . " for "
                . $request['provider'] . " at " . $request['practice']
                . " (" . $request['practice_phone_number'] . ")</td>";
            $result .= "</tr>";
            $count++;
        }
        $result .= "</table><br />";
        $result = str_replace('%%count%%', $count, $result);

        return $result;
    }

    /**
     * Get the list of appointment requests in plain text format.
     * @param array $pendingRequests
     * @return string
     */
    public static function getLastWeekRequestsList($pendingRequests)
    {
        $result = "";
        $footer = "";
        $accountRep = false;
        $result = "";

        foreach ($pendingRequests as $request) {
            if ($accountRep !== $request['account_rep']) {
                $result .= $footer . "Account Rep: "
                    . (!empty(trim($request['account_rep']))
                        ? $request['account_rep']
                        : 'Other') . PHP_EOL;
                $footer = PHP_EOL . PHP_EOL;
                $accountRep = $request['account_rep'];
            }
            $result .= "Created: " . $request['date_added'] . ", ";
            $result .= "Appt. Date: " . $request['appointment_date'] . ", ";
            $result .= "Account: " . $request['account'] . ", ";
            $result .= "Salesforce Link: " . $request['salesforce_link'] . ", ";
            $result .= "Appt. Summary: " . $request['time_start'] . " - " . $request['time_end'] . " for " .
                $request['provider'] . " at " . $request['practice'] . " (" . $request['practice_phone_number'] . ")";
            $result .= PHP_EOL;
        }
        return $result;
    }

    /**
     * Get top 50 accounts per number of direct appointment requests in last 7 days
     * @return string
     */
    public static function getWeeklyRockstarCustomersReportDA()
    {
        return "SELECT account.id AS account_id, CONCAT(account.first_name, ' ', account.last_name) AS account_name,
            account.organization_name AS account_organization_name,
            GROUP_CONCAT(DISTINCT(provider.first_last) SEPARATOR ', ') AS provider_name, COUNT(1) AS appointments
            FROM scheduling_mail
            INNER JOIN `provider`
                ON scheduling_mail.provider_id = `provider`.id
            INNER JOIN account_provider
                ON `provider`.id = account_provider.provider_id
            INNER JOIN account
                ON account_provider.account_id = account.id
            WHERE DATEDIFF(CURDATE(), scheduling_mail.date_added) BETWEEN 1 AND 7
                AND (appointment_type = 'S' OR appointment_type = 'V')
            GROUP BY account_provider.account_id
            ORDER BY appointments DESC
            LIMIT 50;";
    }

    /**
     * Get top 50 accounts per number of "old" appointment requests in last 7 days
     * @return string
     */
    public static function getWeeklyRockstarCustomersReport()
    {
        return "SELECT account.id AS account_id, CONCAT(account.first_name, ' ', account.last_name) AS account_name,
            account.organization_name AS account_organization_name,
            GROUP_CONCAT(DISTINCT(`provider`.first_last) SEPARATOR ', ') AS provider_name, COUNT(1) AS appointments
            FROM scheduling_mail
            INNER JOIN `provider`
                ON scheduling_mail.provider_id = `provider`.id
            INNER JOIN account_provider
                ON `provider`.id = account_provider.provider_id
            INNER JOIN account
                ON account_provider.account_id = account.id
            WHERE DATEDIFF(CURDATE(), scheduling_mail.date_added) BETWEEN 1 AND 7
                AND appointment_type = 'R'
            GROUP BY account_provider.account_id
            ORDER BY appointments DESC
            LIMIT 50;";
    }

    /**
     * For all entries where appointment_date is in the past, change status to Never Confirmed
     * @return boolean
     */
    public static function processNeverConfirmed()
    {
        $set = self::model()->findAll(
            'DATEDIFF(CURDATE(), appointment_date) > 0
            AND status = "P"
            AND (appointment_type = "S" OR appointment_type = "V")',
            array()
        );
        foreach ($set as $sm) {
            echo "processing " . $sm->id . " now... " . PHP_EOL;
            $sm->status = "N";
            $sm->save();
        }
        return true;
    }

    /**
     * Get number of appointments an organization has and last appointment date, to update SalesForce:
     * DDC_Appointment_Requests__c
     * @param int $organizationId
     * @return mixed
     */
    public static function updateSFOrganizationAppointments($organizationId)
    {

        $organization = Organization::model()->findByPk($organizationId);
        if (!$organization || empty($organization->salesforce_id)) {
            return false;
        }

        // update last appointment date
        $lastAppt = self::getLastAppointmentDate($organizationId);
        if (!empty($lastAppt)) {
            $sfData = array(
                'salesforce_id' => $organization->salesforce_id,
                'field' => 'Last_Appointment_Request__c',
                'value' => date('c', strtotime($lastAppt))
            );
            SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);
        }

        // update appointment count
        $value = self::getAppointmentCount($organizationId);
        return Yii::app()->salesforce->updateAccountField(
            $organization->salesforce_id,
            'DDC_Appointment_Requests__c',
            $value
        );
    }

    /**
     * Get the last appointment date for an organization
     * @param int $organizationId
     * @return string
     */
    public static function getLastAppointmentDate($organizationId)
    {

        $sql = sprintf(
            "SELECT MAX(scheduling_mail.date_added)
            FROM organization
            INNER JOIN organization_account
                ON organization.id = organization_account.organization_id AND `connection` = 'O'
            INNER JOIN account_provider
                ON organization_account.account_id = account_provider.account_id
            INNER JOIN scheduling_mail
                ON account_provider.provider_id = scheduling_mail.provider_id
                AND (scheduling_mail.status = 'A' OR scheduling_mail.status IS NULL OR scheduling_mail.status = '')
            WHERE organization.id = %d;",
            $organizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get appointment count for an organization
     * @param int $organizationId
     */
    public static function getAppointmentCount($organizationId)
    {

        $sql = sprintf(
            "SELECT COUNT(1) AS appointment_count
            FROM organization
            INNER JOIN organization_account
                ON organization.id = organization_account.organization_id AND `connection` = 'O'
            INNER JOIN account_provider
                ON organization_account.account_id = account_provider.account_id
            INNER JOIN scheduling_mail
                ON account_provider.provider_id = scheduling_mail.provider_id
            WHERE organization.id = %d;",
            $organizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $activeClient = Yii::app()->request->getParam('SchedulingMail');
        $organizationId = Yii::app()->request->getParam('SchedulingMail');
        $organizationName = Yii::app()->request->getParam('SchedulingMail');
        $salesforceId = Yii::app()->request->getParam('SchedulingMail');
        $organizationStatus = Yii::app()->request->getParam('SchedulingMail');

        if (isset($activeClient['activeClient'])) {
            $this->activeClient = $activeClient['activeClient'];
        }

        if (isset($organizationId['organizationId'])) {
            $this->organizationId = $organizationId['organizationId'];
        }

        if (isset($organizationName['organizationName'])) {
            $this->organizationName = $organizationName['organizationName'];
        }

        if (isset($salesforceId['salesforceId'])) {
            $this->salesforceId = $salesforceId['salesforceId'];
        }

        if (isset($organizationStatus['organizationStatus'])) {
            $this->organizationStatus = $organizationStatus['organizationStatus'];
        }

        // fix dates
        $dateAdded = $appointmentDate = null;
        if (!empty($this->date_added)) {
            $date = DateTime::createFromFormat('m/d/Y', $this->date_added);
            if ($date) {
                $dateAdded = $date->format('Y-m-d');
            }
        }

        if (!empty($this->appointment_date)) {
            $date = DateTime::createFromFormat('m/d/Y', $this->appointment_date);
            if ($date) {
                $appointmentDate = $date->format('Y-m-d');
            }
        }

        $criteria = new CDbCriteria;

        $criteria->compare('t.provider_id', $this->provider_id);
        $criteria->compare('t.partner_site_id', $this->partner_site_id);
        $criteria->compare('t.appointment_type', $this->appointment_type, true);
        $criteria->compare('t.appointment_for', $this->appointment_for, true);
        $criteria->compare('t.status', $this->status, true);
        $criteria->compare('t.first_name', $this->first_name, true);
        $criteria->compare('t.last_name', $this->last_name, true);
        $criteria->compare('t.cell_phone', $this->cell_phone, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.appointment_date', $appointmentDate, true);
        $criteria->compare('t.date_time', $this->date_time, true);
        $criteria->compare('t.time_start', $this->time_start, true);
        $criteria->compare('t.time_end', $this->time_end, true);
        $criteria->compare('t.date_added', $dateAdded, true);
        $criteria->compare('t.practice_id', $this->practice_id);
        $criteria->compare('t.source', $this->source, true);
        $criteria->compare('t.updated_by_account_id', $this->updated_by_account_id, true);

        // comparison for active clients
        if ($this->activeClient == 'Yes' || $this->activeClient == 'No') {
            if ($this->activeClient == 'Yes') {
                $criteria->compare('organization.status', 'A', true);
            } else {
                $criteria->addNotInCondition('organization.status', array('A'), true);
            }
        }

        // comparison for organization status
        switch ($this->organizationStatus) {
            case 'Canceled':
                $criteria->compare('organization.status', 'C', true);
                break;
            case 'Paused':
                $criteria->compare('organization.status', 'P', true);
                break;
            case 'Active':
                $criteria->compare('organization.status', 'A', true);
                break;
            default:
                $criteria->compare('organization.status', '', true);
                break;
        }
        $criteria->compare('organization.id', $this->organizationId, true);
        $criteria->compare('organization.organization_name', $this->organizationName, true);
        $criteria->compare('organization.salesforce_id', $this->salesforceId, true);

        // fields need to be specified manually since we also need the org values
        $criteria->select = 't.id, t.provider_id, t.partner_site_id, t.appointment_type, t.appointment_for, t.status,
            t.first_name, t.last_name, t.cell_phone, t.email, t.appointment_date, t.date_time, t.time_start, t.time_end,
            t.date_added, t.date_updated, t.practice_id, t.source,
            IF(organization.status = "A", "Yes", "No") AS activeClient,
            organization.id AS organizationId,
            organization.organization_name AS organizationName,
            organization.salesforce_id AS salesforceId,
            IF(organization.id IS NULL, "",
            CASE WHEN organization.status = "C" THEN "Canceled"
                WHEN organization.status = "A"  THEN "Active"
                ELSE "Paused" END
            ) AS organizationStatus';

        /**
         * Magic numbers:
         * PHP_INT_MAX - 1 represents Showcase Page - Adwords
         * PHP_INT_MAX     represents Showcase Page - Other
         * @see core\protected\modules\providers\protected\controllers\SchedulingMailController.php actionAdmin()
         * @see core\protected\modules\providers\protected\views\administration\schedulingMail\_search.php
         */
        if ($this->partner == PHP_INT_MAX || $this->partner == PHP_INT_MAX - 1) {
            unset($this->source);
            $criteria->addCondition('source LIKE "http%"');
            if ($this->partner == PHP_INT_MAX - 1) {
                $criteria->addCondition('source LIKE "%?gclid=%"');
            }
        } else {
            $criteria->compare('t.partner_site_id', $this->partner);
        }

        // join to get to the org
        $criteria->join = 'LEFT JOIN account_provider
            ON t.provider_id = account_provider.provider_id
            LEFT JOIN organization_account
            ON account_provider.account_id = organization_account.account_id
            AND connection = "O"
            LEFT JOIN organization
            ON organization_account.organization_id = organization.id';

        $criteria->group = 't.id';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100)
        ));
    }

    /**
     * process and send SMS/Mail alert when appointment request
     * alertType = alert_app_request
     * @param int $schedulingMailId
     */
    public static function sendAlertAppRequest($schedulingMailId = 0)
    {
        if ($schedulingMailId == 0) {
            return false;
        }
        // find scheduling record
        $sql = sprintf(
            "SELECT sm.id, provider_id, practice_id, appointment_date, time_start, time_end,
            pro.first_last AS provider_name,
            pra.name AS practice_name, pra.address_full AS practice_address, pra.phone_number AS practice_phone,
            insurance_id, i.name AS insurance_name,
            insurance_company_id, ic.name AS insurance_company_name,
            visit_reason_id, vr.name AS visit_reason_name
            FROM scheduling_mail AS sm
            LEFT JOIN visit_reason AS vr
                ON sm.visit_reason_id = vr.id
            LEFT JOIN insurance AS i
                ON sm.insurance_id = i.id
            LEFT JOIN insurance_company AS ic
                ON sm.insurance_company_id = ic.id
            LEFT JOIN practice AS pra
                ON sm.practice_id = pra.id
            LEFT JOIN `provider` AS pro
                ON sm.provider_id = pro.id
            WHERE sm.id = %d LIMIT 1;",
            $schedulingMailId
        );
        $sm = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryRow();

        if (isset($sm['practice_id'])) {
            $practiceId = $sm['practice_id'];
        }

        if (isset($sm['provider_id'])) {
            $providerId = $sm['provider_id'];
        }

        $alertsTargetSMS = AccountContactSetting::model()->getAlertsTarget(
            'SMS',
            'alert_app_request',
            $practiceId,
            $providerId
        );

        if (!empty($alertsTargetSMS)) {
            // send alerts...
            $appointmentDate = date('m/d/y h:iA', strtotime($sm['appointment_date'] . ' ' . $sm['time_start']));
            foreach ($alertsTargetSMS as $alert) {
                $to = $alert['mobile_phone'];
                $body = 'Appointment Request for ' . $sm['provider_name'] . ' - ' . $sm['practice_name'];
                $body .= ' on ' . $appointmentDate . ' - Login to see details and confirm https://goo.gl/td7udu';
                $locationId = $alert['location_id'];
                SmsComponent::sendSms($to, $body, false, $locationId);
            }
            return true;
        }
    }

    /**
     * Send appropriate email for a request from Capybara
     * @param array $dataset
     * @return boolean
     */
    public static function sendNewAppointmentEmails($dataset)
    {

        if (isset($dataset['appointment'])) {
            $appointment = $dataset['appointment'];
        }

        if (isset($dataset['practice'])) {
            $practiceId = $dataset['practice'];
        }

        if (isset($dataset['provider'])) {
            $providerId = $dataset['provider'];
        }

        if (isset($dataset['patient'])) {
            $patient = $dataset['patient'];
        }

        $arrAuthorizedRecipients = Account::getEmailsFromAuthorizedRecipients(
            'appointment_request',
            $practiceId,
            $providerId
        );
        if (empty($arrAuthorizedRecipients)) {
            return false;
        }

        foreach ($arrAuthorizedRecipients as $v) {
            if ($v['connection'] == 'O') {
                // Process and Send email to the authorized recipients
                MailComponent::scheduleAppointmentToPractice($appointment, $practiceId, $v['account_id']);
                break;
            }
        }

        // Send email to patient
        MailComponent::scheduleAppointmentForPatient($appointment, $providerId, $patient, false);
        return true;
    }

    /**
     * Run the importation of data into the EHR
     * @param int $accountId
     */
    public function syncEhr($accountId)
    {
        AccountEmr::importAccountEmr($accountId);
        $this->saveOnCpnEhrCommunicationQueue();
    }

    /**
     * Approve an appointment request
     * @param int $accountId
     * @param bool $sendToEhr
     * @param bool $doNotSendEmail
     * @param int $schedulingMailEhrPatient
     */
    public function approve($accountId = 0, $sendToEhr, $doNotSendEmail, $schedulingMailEhrPatient = 0)
    {

        $sendToEhr = 1;

        if ($accountId == 0) {
            $accountId = Yii::app()->user->id;
        }

        // Getting ownerAccountId since Ehr needs this accountId to work
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);

        $previousStatus = $this->status;

        // Approve
        $this->status = 'A';

        // Add user who is updating
        $this->updated_by_account_id = $accountId;

        $this->save();

        if ($sendToEhr == 1 && empty($this->emr_date_sent)) {
            if (empty($schedulingMailEhrPatient)) {
                $schedulingMailEhrPatient = null;
            }
            if (!empty($schedulingMailEhrPatient)) {
                $this->scheduling_mail_ehr_patient_id = $schedulingMailEhrPatient;
            }
            $this->sendToEhr($ownerAccountId, true);
        }

        if (($this->appointment_type == 'S' || $this->appointment_type == 'V') && $previousStatus != 'A') {

            if (empty($doNotSendEmail)) {
                // Mail to Patient
                MailComponent::appointmentApprovedForPatient($this);
            }
            // Mail to Practice
            MailComponent::appointmentApprovedForPractice($this->id, $this->practice_id, $ownerAccountId);
        }
    }

    /**
     * Reject an appointment request
     * @param int $accountId
     * @param bool $sendToEhr
     * @param bool $doNotSendEmail
     */
    public function reject($accountId, $sendToEhr, $doNotSendEmail)
    {
        $this->status = 'R';
        // Add user who is updating
        $this->updated_by_account_id = $accountId;
        $this->save();

        // Getting ownerAccountId since Ehr needs this accountId to work
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);
        if ($sendToEhr && !empty($this->emr_aditional_information)) {
            $this->sendToEhr($ownerAccountId, true, 'R');
        }

        if (!empty($this->emr_appointment_id)) {
            $appointment = Appointment::model()->find(
                "emr_appointment_id = :emr_appointment_id",
                array(":emr_appointment_id" => $this->emr_appointment_id)
            );
            if ($appointment) {
                $appointment->status = 'R';
                $appointment->save();
            }
        }

        if (($this->appointment_type == 'S' || $this->appointment_type == 'V') && empty($doNotSendEmail)) {
            MailComponent::appointmentCancelledForPatient($this);
        }
    }

    /**
     * Reschedule the appointment request
     * @param int $accountId
     * @param bool $sendToEhr
     * @param bool $doNotSendEmail
     * @param array $newData
     * @param int $schedulingMailEhrPatient
     */
    public function reschedule($accountId, $sendToEhr, $doNotSendEmail, $newData, $schedulingMailEhrPatient = 0)
    {

        // Getting ownerAccountId since Ehr needs this accountId to work
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);

        $this->status = 'A';
        if ($schedulingMailEhrPatient == 0) {
            $schedulingMailEhrPatient = null;
        }
        if (!empty($schedulingMailEhrPatient)) {
            $this->scheduling_mail_ehr_patient_id = $schedulingMailEhrPatient;
        }

        if (empty($this->previous_date) && empty($this->previous_time_start) && empty($this->previous_time_end)) {
            $this->previous_date = $this->appointment_date;
            $this->previous_time_start = $this->time_start;
            $this->previous_time_end = $this->time_end;
        }

        // format date - $newData['newDate'] is mm-dd-yyyy
        list($month, $day, $year) = explode('-', $newData['newDate']);
        $newDate = $year . '-' . $month . '-' . $day;
        $this->appointment_date = $newDate;

        // format time
        $tempTimeStart = $newData['newHour'] . ':' . $newData['newMin'] . ' ' . $newData['newAmPm'];
        $tempTimeEnd = $newData['endHour'] . ':' . $newData['endMin'] . ' ' . $newData['endAmPm'];
        $this->time_start = date("H:i", strtotime($tempTimeStart));
        $this->time_end = date("H:i", strtotime($tempTimeEnd));

        // Add user who is updating
        $this->updated_by_account_id = $accountId;

        // save
        $this->save();

        if ($sendToEhr && !empty($this->emr_aditional_information)) {
            $this->sendToEhr($ownerAccountId, true, 'RE');
        } elseif ($sendToEhr) {
            $this->sendToEhr($ownerAccountId, true);
        }

        if ($this->appointment_type == 'S' || $this->appointment_type == 'V') {
            if (empty($doNotSendEmail)) {
                // Mail to Patient
                MailComponent::appointmentApprovedForPatient($this);
            }
            // Mail to Practice
            MailComponent::appointmentApprovedForPractice(
                $this->id,
                $this->practice_id,
                $ownerAccountId
            );
        }
    }

    /**
     * Mark an appointment as No Show
     * @param int $accountId
     */
    public function noShow($accountId)
    {
        // Checks to do in case the new status is coming as No Show (NS)
        // If the user is trying to change to No Show, the status needs to be
        // 'Never Confirmed'(N || P), 'Confirmed'(A), or 'Rejected'(R) and it's time/date had pass
        if (
            (empty($this->status) || in_array($this->status, ['P', 'N', 'A', 'R']))
            && $this->appointment_date < date('Y-m-d')
        ) {
            // Set status to No Show
            $this->status = 'S';
            // Add user who is updating
            $this->updated_by_account_id = $accountId;
            $this->save();
        }
    }

    /**
     * Send the appointment request to the ehr
     * @param int $accountId
     * @param bool $syncEhr
     * @param string $operation
     */
    public function sendToEhr($accountId, $syncEhr, $operation = 'A')
    {
        $this->emr_date_sent = null;
        $this->emr_aditional_information = $operation;

        if ($syncEhr) {
            $this->save();
            $this->syncEhr($accountId);
        }
    }

    /**
     * Get Scheduling Mail Patient
     * @param int $schedulingMailId
     * @return array
     */
    public static function getSchedulingMailPatient($schedulingMailId)
    {
        $sql = sprintf(
            "SELECT
            scheduling_mail.first_name,
            scheduling_mail.last_name,
            scheduling_mail.cell_phone,
            scheduling_mail.email,
            patient.first_name AS patient_first_name,
            patient.last_name AS patient_last_name
            FROM scheduling_mail
            INNER JOIN patient
                ON patient.id = scheduling_mail.patient_id
            WHERE scheduling_mail.id = %s;",
            $schedulingMailId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get total count of entries in the scheduling_mail table
     * @return mixed
     */
    public static function getTotalCount()
    {
        return (int)SchedulingMail::model()->count();
    }

    /**
     * Export entries from scheduling_mail table
     * @param int $limit
     * @param int $offset
     * @return mixed
     */
    public static function exportEntries($limit = 100, $offset = 0)
    {
        $sql = "SELECT
                scheduling_mail.id,
                scheduling_mail.date_added,
                scheduling_mail.email,
                scheduling_mail.first_name,
                scheduling_mail.last_name,
                scheduling_mail.appointment_date,
                scheduling_mail.date_time,
                scheduling_mail.patient_id,
                scheduling_mail.insurance_id,
                scheduling_mail.source,
                practice.name as p_name,
                organization.id AS organization_id,
                organization.organization_name AS organization_name,
                CONCAT('" . Yii::app()->salesforce->applicationUri . "', organization.salesforce_id) AS salesforce_id,
                IF(organization.id IS NULL, '',
                    CASE WHEN organization.status = 'C' THEN 'Canceled'
                        WHEN organization.status = 'A' THEN 'Active'
                        ELSE 'Paused' END
                ) AS status
                FROM scheduling_mail
                LEFT JOIN practice
                    ON scheduling_mail.practice_id = practice.id
                LEFT JOIN organization
                    ON organization.id = (
                    SELECT organization_id FROM organization_account WHERE account_id = (
                    SELECT account_id FROM account_provider WHERE provider_id = scheduling_mail.provider_id LIMIT 1
                    ) LIMIT 1
                )
                ORDER BY scheduling_mail.id ASC
                LIMIT " . $offset . ', ' . $limit . ";";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * get appointment requests for export on organization tool
     * @param int $organizationId
     * @return array
     */
    public static function getOrganizationExport($organizationId)
    {

        $sql = "SELECT account_id
            FROM organization_account
            WHERE `connection`= 'O'
            AND organization_id = :organizationId";
        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":organizationId", $organizationId, PDO::PARAM_INT);
        $ownerAccountId = $command->queryScalar();

        if ($ownerAccountId) {
            $sql = sprintf(
                "SELECT
                scheduling_mail.id AS AppointmentId,
                practice.name AS PracticeName,
                practice.address_full AS PracticeAddress,
                practice.phone_number AS PracticePhoneNumber,
                `provider`.first_middle_last AS ProviderName,
                `provider`.npi_id AS ProviderNPI,
                CONCAT(scheduling_mail.first_name,' ',scheduling_mail.last_name) AS PatientName,
                scheduling_mail.cell_phone AS PatientCellPhone,
                scheduling_mail.email AS PatientEmail,
                partner_site.name AS PartnerSiteSource,
                appointment_for AS AppointmentFor,
                IF (returning_patient = 1, 'Yes', '' ) AS ReturningPatient,
                CASE scheduling_mail.status
                    WHEN 'P' THEN 'Penging'
                    WHEN 'A' THEN 'Approved'
                    WHEN 'R' THEN 'Rejected'
                    WHEN 'N' THEN 'Never confirmed'
                    WHEN 'H' THEN 'Handling reschedule'
                    END AS AppointmentStatus,
                DATE_FORMAT(appointment_date,'%%m/%%d/%%Y') AS AppointmentDate,
                LEFT(time_start,5) AS AppointmentTimeStart,
                LEFT(time_end,5) AS AppointmentTimeEnd,
                scheduling_mail.reason AS AppointmentReason,
                DATE_FORMAT(scheduling_mail.date_added,'%%m/%%d/%%Y') AS DateAdded
                FROM scheduling_mail
                INNER JOIN practice
                    ON practice.id= scheduling_mail.practice_id
                INNER JOIN `provider`
                    ON `provider`.id = scheduling_mail.provider_id
                INNER JOIN partner_site
                    ON partner_site.id = scheduling_mail.partner_site_id
                WHERE practice.id IN (SELECT practice_id FROM account_practice WHERE account_id=%s)
                    AND provider_id IN (SELECT provider_id FROM account_provider WHERE account_id=%s)
                    AND (appointment_type='S' OR appointment_type='V')
                ORDER BY appointment_date",
                $ownerAccountId,
                $ownerAccountId
            );
            return Yii::app()->db->createCommand($sql)->queryAll();
        } else {
            return array();
        }
    }

    /**
     * Return how long an appointment as been waiting to be approved, or nothing
     * @param string $appointmentType
     * @param string $status
     * @param datetime $dateAdded
     * @return string
     */
    public static function getWaitingFor($appointmentType, $status, $dateAdded)
    {
        if ($appointmentType == 'S' && $status == 'P') {
            // if the appointment is pending, show how long it's been like that
            return TimeComponent::getDateDiff(time(), $dateAdded, 1);
        }
        return '-';
    }

    /**
     * get Firsts Appointments
     * @param string $fromDate
     * @param int $pageNumber
     * @param int $pageLimit
     * @return array
     */
    public static function getFirstsAppointments($fromDate = '', $pageNumber = 1, $pageLimit = 30)
    {

        if (empty($fromDate)) {
            $fromDate = date('Y-m-d 00:00:00', strtotime('-1 year'));
        }

        if ($pageLimit == 0) {
            //count records
            $limit = '';
        } else {
            $pageNumber = $pageNumber - 1;
            $pageNumber = $pageNumber < 1 ? 0 : $pageNumber;
            $recordOffSet = $pageNumber * $pageLimit;
            $limit = sprintf("LIMIT %d, %d", $recordOffSet, $pageLimit);
        }

        $sql = sprintf(
            "SELECT a.id AS account_id,
            COUNT(a.id) AS `counter`,
            o.salesforce_id,
            o.organization_name,
            o.status AS o_status,
            oa.organization_id,
            sm.id AS sm_id,
            sm.provider_id,
            pro.first_m_last,
            sm.practice_id,
            sm.date_added,
            sm.appointment_type,
            sm.first_name AS patient_first_name,
            sm.last_name AS patient_last_name,
            sm.email AS patient_email,
            sm.cell_phone AS patient_phone,
            CONCAT(sm.appointment_date, ' ', sm.time_start) AS appt_date,
            sm.status,
            csupport_rep_call_date,
            '' AS CSupport_Rep__c,
            partner_site.display_name AS partner_name,
            sm.internal_notes
            FROM scheduling_mail AS sm
            INNER JOIN account_provider AS apro
                ON apro.provider_id = sm.provider_id
            INNER JOIN account_practice AS apra
                ON apra.practice_id = sm.practice_id
            INNER JOIN account AS a
                ON (a.id = apro.account_id AND a.id = apra.account_id)
            INNER JOIN `provider` AS pro
                ON sm.provider_id = pro.id
            LEFT JOIN organization_account AS oa
                ON oa.account_id = a.id
            LEFT JOIN organization AS o
                ON o.id = oa.organization_id
            LEFT JOIN partner_site
                ON partner_site.id = sm.partner_site_id
            WHERE sm.date_added > '%s'
                AND ( oa.connection IS NULL OR  oa.connection = 'O' )
                AND ( o.status IS NULL OR o.status != 'C' )
            GROUP BY account_id
            HAVING `counter` < 4
            ORDER BY sm.date_added DESC %s",
            $fromDate,
            $limit
        );

        $return = Yii::app()->db->createCommand($sql)->queryAll();

        if (empty($limit)) {
            // to get total records
            return count($return);
        }
        $result = array();
        if (!empty($return)) {
            foreach ($return as $value) {

                // Looking for ['CSupport_Rep__c']
                $cSupportRepName = '';
                $value['salesforce_id'] = !empty($value['salesforce_id']) ? $value['salesforce_id'] : '';
                $value['organization_name'] = !empty($value['organization_name']) ? $value['organization_name'] : '';
                $value['organization_id'] = !empty($value['organization_id']) ? $value['organization_id'] : '';
                $value['o_status'] = !empty($value['o_status']) ? $value['o_status'] : '';
                if (!empty($value['salesforce_id'])) {
                    $sfDetails = Account::getSalesforceDetails($value['salesforce_id']);
                    $cSupportRepName = !empty($sfDetails['CSupport_Rep__c']) ? trim($sfDetails['CSupport_Rep__c']) : '';
                }

                $value['CSupport_Rep__c'] = $cSupportRepName;
                $value['status_text'] = SchedulingMail::getStatusText($value['status']);
                $value['org_status'] = ($value['o_status'] == 'A'
                    ? 'Active'
                    : ($value['o_status'] == 'P' ? 'Paused' : 'Unknown'));
                $value['apptDate'] = date("n/j/y g:i a", strtotime($value['appt_date']));
                $value['dateAdded'] = date("n/j/y", strtotime($value['date_added']));
                $value['waitingFor'] = SchedulingMail::getWaitingFor(
                    $value['appointment_type'],
                    $value['status'],
                    $value['date_added']
                );
                $value['partner_name'] = StringComponent::truncate($value['partner_name'], 30);
                $value['organization_name'] = StringComponent::truncate($value['organization_name'], 30);

                $result[] = $value;

                if ($value['counter'] > 1) {
                    // Add the others appt for the same account
                    $providerId = $value['provider_id'];
                    $practiceId = $value['practice_id'];
                    $accountId = $value['account_id'];
                    $salesForceId = $value['salesforce_id'];
                    $smId = $value['sm_id'];
                    $counter = $value['counter'];
                    $sql = sprintf(
                        "SELECT
                        a.id AS account_id,
                        o.salesforce_id,
                        o.organization_name,
                        o.status AS o_status,
                        oa.organization_id,
                        sm.id AS sm_id,
                        sm.provider_id,
                        pro.first_m_last,
                        sm.practice_id,
                        sm.date_added,
                        sm.appointment_type,
                        sm.first_name AS patient_first_name,
                        sm.last_name AS patient_last_name,
                        sm.email AS patient_email,
                        sm.cell_phone AS patient_phone,
                        CONCAT(sm.appointment_date, ' ', sm.time_start) AS appt_date,
                        sm.status,
                        csupport_rep_call_date,
                        '%s' AS CSupport_Rep__c,
                        partner_site.display_name AS partner_name,
                        sm.internal_notes
                        FROM scheduling_mail AS sm
                        INNER JOIN account_provider AS apro
                            ON apro.provider_id = sm.provider_id
                        INNER JOIN account_practice AS apra
                            ON apra.practice_id = sm.practice_id
                        INNER JOIN account AS a
                            ON a.id = %d
                        INNER JOIN provider AS pro
                            ON sm.provider_id = pro.id
                        INNER JOIN organization_account AS oa
                            ON oa.account_id = a.id
                        INNER JOIN organization AS o
                            ON o.id = oa.organization_id
                        LEFT JOIN partner_site
                            ON partner_site.id = sm.partner_site_id
                        WHERE sm.date_added > '%s'
                            AND o.salesforce_id = '%s'
                            AND oa.`connection` = 'O' AND o.salesforce_id IS NOT NULL
                            AND sm.provider_id = %d AND sm.practice_id = %d
                            AND o.date_added  <= sm.date_added AND o.status != 'C'
                            AND sm.id != %d
                        GROUP BY sm.id
                        ORDER BY sm.date_added DESC",
                        $cSupportRepName,
                        $accountId,
                        $fromDate,
                        $salesForceId,
                        $providerId,
                        $practiceId,
                        $smId
                    );
                    $eachOne = Yii::app()->db->createCommand($sql)->queryAll();

                    if (!empty($eachOne)) {
                        foreach ($eachOne as $value2) {
                            $counter--;
                            $value2['counter'] = $counter;
                            $value2['CSupport_Rep__c'] = $cSupportRepName;
                            $value2['status_text'] = SchedulingMail::getStatusText($value2['status']);
                            $value2['org_status'] = ($value2['o_status'] == 'A'
                                ? 'Active'
                                : ($value2['o_status'] == 'P' ? 'Paused' : 'Unknown'));
                            $value2['apptDate'] = date("n/j/y g:i a", strtotime($value2['appt_date']));
                            $value2['dateAdded'] = date("n/j/y", strtotime($value2['date_added']));
                            $value2['waitingFor'] = SchedulingMail::getWaitingFor(
                                $value2['appointment_type'],
                                $value2['status'],
                                $value2['date_added']
                            );
                            $value2['partner_name'] = StringComponent::truncate($value2['partner_name'], 30);
                            $value2['organization_name'] = StringComponent::truncate($value2['organization_name'], 30);
                            $result[] = $value2;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Return status text
     * @param string $status
     * @return string
     */
    public static function getStatusText($status = '')
    {
        switch ($status) {
            case 'P':
                return 'Pending';
            case 'A':
                return 'Approve';
            case 'R':
                return 'Rejected';
            case 'N':
                return 'Never Confirmed';
            case 'H':
                return 'Handling Reschedule';
            default:
                return '';
        }
    }

    /**
     * Get number of appointments for a given organization in the last month
     * @param int $organizationId
     * @return array
     */
    public static function getMRByOrganizationId($organizationId)
    {

        $base = "SELECT COUNT(DISTINCT(scheduling_mail.id)) AS total
            FROM scheduling_mail
            INNER JOIN account_practice
                ON scheduling_mail.practice_id = account_practice.practice_id
            INNER JOIN account_provider
                ON scheduling_mail.provider_id = account_provider.provider_id
                AND account_practice.account_id=account_provider.account_id
            INNER JOIN organization_account
                ON account_practice.account_id = organization_account.account_id
                AND `connection` = 'O'
            WHERE organization_id = %d
                AND DATE_FORMAT(scheduling_mail.date_added, '%%Y-%%m') = '%s';";

        $sql = sprintf($base, $organizationId, date('Y-m', strtotime('-1 month')));
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Create a new JSON Message and save the Appointment in CpnEhrCommmunicationQueue with "TO" Endpoint
     * @return void
     */
    public function saveOnCpnEhrCommunicationQueue()
    {
        $practice = Practice::model()->findByPk($this->practice_id);
        if (empty($practice)) {
            return false;
        }
        $arrTimezone = $practice->getTimeZone();

        $combinedDTStart = new DateTime(
            $this->appointment_date . ' ' . $this->time_start,
            new DateTimeZone($arrTimezone['phpTimezone'])
        );
        $combinedDTEnd = new DateTime(
            $this->appointment_date . ' ' . $this->time_end,
            new DateTimeZone($arrTimezone['phpTimezone'])
        );

        $combinedDTStart->setTimezone(new DateTimeZone('UTC'));
        $combinedDTEnd->setTimezone(new DateTimeZone('UTC'));

        $timeStart = $combinedDTStart->format('Y-m-d\TH:i:s\Z');
        $timeEnd = $combinedDTEnd->format('Y-m-d\TH:i:s\Z');

        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('America/New_York'));
        $messageDate = gmdate("Y-m-d\TH:i:s\Z");

        $visitReason = "";
        if (isset($this->visit_reason_id)) {
            $sql4 = sprintf(
                "SELECT visit_reason.name FROM visit_reason WHERE visit_reason.id = '%d'",
                $this->visit_reason_id
            );
            $visitReason = Yii::app()->db->createCommand($sql4)->queryScalar();
        }

        $patientCellPhone = '';
        if (isset($this->cell_phone)) {
            $patientCellPhone = $this->cell_phone;
        }

        $patientDateOfBirth = '';
        if (isset($this->patient->date_of_birth)) {
            $patientDateOfBirth = $this->patient->date_of_birth;
        }

        $patientFirstName = '';
        if (isset($this->first_name)) {
            $patientFirstName = $this->first_name;
        }

        $patientLastName = '';
        if (isset($this->last_name)) {
            $patientLastName = $this->last_name;
        }

        $patientEmail = '';
        if (isset($this->email)) {
            $patientEmail = $this->email;
        }

        $patientGender = '';
        if (isset($this->patient->gender)) {
            $patientGender = $this->patient->gender;
        }

        $patientNightPhone = '';
        if (isset($this->patient->night_phone)) {
            $patientNightPhone = $this->patient->night_phone;
        }

        $patientDayPhone = '';
        if (isset($this->patient->day_phone)) {
            $patientDayPhone = $this->patient->day_phone;
        }

        $returningPatient = '';
        if (isset($this->returning_patient)) {
            $returningPatient = $this->returning_patient;
        }

        $ehrPatient = null;
        if (!empty($this->scheduling_mail_ehr_patient_id)) {
            $ehrPatient = EhrPatient::model()->findByPk($this->scheduling_mail_ehr_patient_id);
        }

        //set up our query
        $sql = sprintf(
            "SELECT DISTINCT cpn_installation_provider.cpn_installation_id
            FROM cpn_installation_provider
            INNER JOIN cpn_installation
                ON cpn_installation_provider.cpn_installation_id = cpn_installation.id
            WHERE cpn_installation_provider.provider_id = %d
                AND del_flag = '0'
                AND enabled_flag = '1'
                AND provider_rating_notification = 1",
            $this->provider_id
        );

        //run the query to get the installation
        $arrCpnInstallation = Yii::app()->db->createCommand($sql)->queryColumn();

        //iterate through the installation
        foreach ($arrCpnInstallation as $cpnInstallationId) {

            $personelId = '';
            $sql = sprintf(
                "SELECT ciu.ehr_user_id, ciu.ehr_user_data, ciu.cpn_installation_id
                FROM cpn_installation_provider cip
                INNER JOIN cpn_installation_user ciu
                    ON cip.cpn_installation_user_id = ciu.id
                WHERE cip.provider_id = '%d'
                    AND cip.cpn_installation_id = '%d'; ",
                $this->provider_id,
                $cpnInstallationId
            );
            $res = Yii::app()->db->createCommand($sql)->queryRow();

            $personelId = null;
            $personelLastname = null;
            $cpnInstallationId = null;

            if (is_array($res)) {
                $personelId = $res['ehr_user_id'];
                $personelLastname = $res['ehr_user_data'];
                $cpnInstallationId = $res['cpn_installation_id'];
            }

            $sql2 = sprintf(
                "SELECT cil.ehr_location_id, cil.ehr_location_data
                FROM cpn_installation_practice cip
                INNER JOIN cpn_installation_location cil
                    ON cip.cpn_installation_location_id = cil.id
                WHERE cip.practice_id = '%d'
                    AND cip.cpn_installation_id = '%d'; ",
                $this->practice_id,
                $cpnInstallationId
            );

            $result = Yii::app()->db->createCommand($sql2)->queryRow();

            $facility = null;
            $poc = null;

            if (is_array($result)) {
                $facility = $result['ehr_location_id'];
                $poc = $result['ehr_location_data'];
            }


            $sql3 = sprintf("SELECT MAX(id) from cpn_ehr_communication_queue");
            $communicationQueueId = Yii::app()->db->createCommand($sql3)->queryScalar();

            $json = (object)[
                "messageIdentifier" => "MessageIdentifier001",
                "secret" => "",
                "sendingApplication" => "Doctor",
                "receivingApplication" => "Doctor Companion API",
                "receivingFacility" => "Doctor.com",
                "messageDate" => $messageDate,
                "messageType" => "3",
                "messageId" => "Doctor.com_" . $this->id . '_' . ($communicationQueueId + 1),
                "processingIdentifier" => $communicationQueueId + 1,   // last inserted ID+1

                'appointment' =>
                [
                    "placerId" => $this->id,

                    "scheduleId" => ["identifier" => $this->id],
                    "appointmentReason" =>
                    [
                        "identifier" => $visitReason,
                    ],
                    "appointmentType" =>
                    [
                        "identifier" => $visitReason,
                        "description" => $visitReason
                    ],
                    "timing" =>
                    [
                        "start" => $timeStart,
                        "end" => $timeEnd,
                    ],
                    "fillerStatus" =>
                    [
                        "description" => "BOOKED",
                        "identifier" => "BOOKED"
                    ],
                    "visitInfo" =>
                    [
                        "patientClass" => $returningPatient,
                        "assignedLocation" =>
                        [
                            "facility" => $facility,
                            "poc" => $poc,
                        ],

                        "attending" =>
                        [
                            "firstName" => $poc,
                        ],
                    ],
                    "personnel" =>
                    [
                        [
                            "identifier" => $personelId,
                            "identifierInternal" => $this->provider_id,
                            "lastName" => $personelLastname,
                            "status" =>
                            [
                                "description" => "BOOKED",
                                "identifier" => "BOOKED",
                            ]
                        ]
                    ],
                ],

                "patient" =>
                [
                    "identifier" => "1",
                    "identifierExternal" => empty($ehrPatient) ? $this->patient_id : $ehrPatient->ehr_patient_id,
                    "identifierInternal" => empty($ehrPatient) ? $this->patient_id : $ehrPatient->ehr_patient_id,
                    "firstName" => empty($patientFirstName) ? null : $patientFirstName,
                    "lastName" => empty($patientLastName) ? null : $patientLastName,
                    "gender" => empty($patientGender) ? null : $patientGender,
                    "email" => empty($patientEmail) ? null : $patientEmail,
                    "cellPhone" => empty($patientCellPhone) ? null : $patientCellPhone,
                    "dateOfBirth" => empty($patientDateOfBirth) ? null : $patientDateOfBirth,
                    "homePhone" => empty($patientNightPhone) ? null : $patientNightPhone,
                    "workPhone" => empty($patientDayPhone) ? null : $patientDayPhone
                ]
            ];

            $jsonEncoded = json_encode($json);

            // Check if the provider is associated to a companion installation
            $sql = sprintf(
                "SELECT cip.id
                FROM cpn_installation_provider cip
                INNER JOIN cpn_installation ci
                    ON cip.cpn_installation_id = ci.id
                WHERE ci.del_flag = '0'
                    AND ci.enabled_flag = '1'
                    AND cip.provider_id = '%d'
                    AND cip.cpn_installation_id = '%d'
                ORDER BY ci.id ASC
                LIMIT 1",
                $this->provider_id,
                $cpnInstallationId
            );
            $cpnInstallationProviderId = Yii::app()->db->createCommand($sql)->queryScalar();

            $cpnInstallationProvider = CpnInstallationProvider::model()->findByPk($cpnInstallationProviderId);

            // check if the installation has an integrated EHR
            $cpnEhrInstallation = null;
            if ($cpnInstallationProvider) {
                $cpnEhrInstallation = CpnEhrInstallation::model()->find(
                    "cpn_installation_id = :cpn_installation_id AND del_flag = 0 AND enabled_flag = 1 "
                        . "ORDER BY cpn_installation_id ASC",
                    array(":cpn_installation_id" => $cpnInstallationProvider->cpn_installation_id)
                );
            }

            // Send the appointment if an EHR is integrated
            if ($cpnEhrInstallation) {
                $message = new CpnEhrCommunicationQueue();
                $message->guid = Yii::app()->db->createCommand("SELECT uuid();")->queryScalar();
                $message->cpn_ehr_message_type_id = 3;
                $message->cpn_installation_id = $cpnEhrInstallation->cpn_installation_id;
                $message->cpn_ehr_installation_id = $cpnEhrInstallation->id;
                $message->endpoint = 'TO';
                $message->processed = 0;
                $message->success = null;
                $message->message_identifier = 'Doctor.com ' . $message->guid;
                $message->message = $jsonEncoded;
                $message->results = null;
                $message->entity_type = null;
                $message->entity_value = null;
                $message->date_added = $message->date_updated = date('Y-m-d H:i:s');
                $message->added_by_account_id = $cpnInstallationProvider->cpnInstallation->account_id;
                $message->updated_by_account_id = null;
                if ($message->save()) {
                    $this->emr_date_sent = $this->cpn_ehr_date_sent = date('Y-m-d H:i:s');
                    $this->save();
                }
            }
        }
    }
}
