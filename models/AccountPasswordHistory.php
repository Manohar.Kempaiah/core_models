<?php

Yii::import('application.modules.core_models.models._base.BaseAccountPasswordHistory');

class AccountPasswordHistory extends BaseAccountPasswordHistory
{
    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return AccountPasswordHistory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Converts a class name string from get_class() to our intended DB enum
     * @param string $classNameString
     * @return string an enum of entity_type in the table
     */
    public static function getEntityType($classNameString)
    {
        switch ($classNameString) {
            case 'Account':
                return "account";
            case 'Patient':
                return "patient";
            case 'PartnerSite':
                return "partner_site";
            case 'User':
                return "user";
            default:
                return "";
        }
    }

}
