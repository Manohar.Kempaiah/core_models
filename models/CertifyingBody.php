<?php

Yii::import('application.modules.core_models.models._base.BaseCertifyingBody');

class CertifyingBody extends BaseCertifyingBody
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'unique'),
            array('name', 'length', 'max' => 50),
            array('full_name', 'length', 'max' => 100),
            array('abbreviation', 'length', 'max' => 5),
            array('id, name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider.
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('full_name', $this->full_name, true);
        $criteria->compare('abbreviation', $this->abbreviation, true);
        return new CActiveDataProvider(
            get_class($this),
            array(
                'criteria' => $criteria,
                'pagination' => array('pageSize' => 100)
            )
        );
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from certifying_body table if associated records exist in provider_certification table
        $has_associated_records = ProviderCertification::model()->exists(
            'certifying_body_id = :certifying_body_id',
            array(
                ':certifying_body_id' => $this->id
            )
        );
        if ($has_associated_records) {
            $this->addError('id', 'This certification body cannot be deleted because certifications are linked to it.');
            return false;
        }
        return parent::beforeDelete();
    }

    public function getNameAbbreviation()
    {
        return $this->name . ' (' . $this->abbreviation . ')';
    }

}
