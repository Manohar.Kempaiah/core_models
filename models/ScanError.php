<?php

Yii::import('application.modules.core_models.models._base.BaseScanError');
Yii::import('application.components.DevOps.*');
Yii::import('application.components.DevOps.Vendors.*');
Yii::import('application.components.DevOps.Adapters.*');
Yii::import('application.modules.core_models.models._interfaces.GenericScanResult');

class ScanError extends BaseScanError
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Save a scan error, and copy the fields and previous tasks
     * to the new version.
     *
     * @param ScanError $previous
     *
     * @return void
     * @throws Exception
     */
    public function saveAndSetup($previous)
    {
        $transaction = Yii::app()->db->beginTransaction();
        try {
            $this->save(false);

            if ($previous) {
                $this->copyPreviousFieldsAndTasks($previous);
            } else {
                $this->createFieldsFromScratch();
            }

            $transaction->commit();
            return true;
        } catch (Exception $e) {
            $transaction->rollback();
            throw $e;
        }
    }

    /**
     * Copy the previous fields to the new version.
     *
     * @param ScanError $previous
     *
     * @return void
     * @throws Exception
     */
    public function createFieldsFromScratch()
    {
        $typeGroup = TypeGroup::model()->findByAttributes(['code' => 'scan_field']);
        $fieldTypes = Type::model()->findAllByAttributes(['type_group_id' => $typeGroup->id]);
        $defaultErrorType = Type::model()->getDefaultScanErrorType();

        foreach ($fieldTypes as $fieldType) {
            if ($this->shouldCreateDefaultDetail($fieldType)) {
                $scanErrorDetail = new ScanErrorDetail();
                $scanErrorDetail->field_type_id = $fieldType->id;
                $scanErrorDetail->scan_error_id = $this->id;
                $scanErrorDetail->type_id = $defaultErrorType->id;
                $scanErrorDetail->save(false);
            }
        }
    }

    public function shouldCreateDefaultDetail(Type $fieldType)
    {
        if ($fieldType->isDefaultForScanError()) {
            if ($fieldType->isDefaultForAll()) {
                return true;
            }

            if ($this->isProvider() && $fieldType->isProviderOnlyScanError()) {
                return true;
            }

            if ($this->isPractice() && $fieldType->isPracticeOnlyScanError()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Copy the previous fields to the new version.
     *
     * @param ScanError $previous
     *
     * @return void
     * @throws Exception
     */
    public function copyPreviousFieldsAndTasks(ScanError $previous)
    {
        $typesCopied = [];
        if ($previous && $previous->scanErrorDetails) {
            foreach ($previous->scanErrorDetails as $previousDetail) {
                $scanErrorDetail = new ScanErrorDetail();
                $scanErrorDetail->scan_error_id = $this->id;
                $scanErrorDetail->type_id = $previousDetail->type_id;
                $scanErrorDetail->field_type_id = $previousDetail->field_type_id;
                $scanErrorDetail->save(false);
                $typesCopied[] = $previousDetail->field_type_id;

                if ($previous->scanErrorTasks) {
                    foreach ($previous->scanErrorTasks as $task) {
                        $scanErrorDetail->addTask($task);
                    }
                }
            }
        }

        // IF new default fields were added, create those from scratch
        $defaultTypes = Type::model()->getDefaultScanErrorFieldTypes();
        if (count($typesCopied) < count($defaultTypes)) {
            Debug::cli('New default error types found, merging...');
            foreach ($defaultTypes as $defaultType) {
                if (!in_array($defaultType->id, $typesCopied)) {
                    Debug::cli('New default type to add: ' . $defaultType->code);
                    $defaultErrorType = Type::model()->getDefaultScanErrorType();

                    $scanErrorDetail = new ScanErrorDetail();
                    $scanErrorDetail->field_type_id = $defaultType->id;
                    $scanErrorDetail->scan_error_id = $this->id;
                    $scanErrorDetail->type_id = $defaultErrorType->id;
                    $scanErrorDetail->save(false);
                }
            }
        }
    }

    /**
     * Get a ScanError with all the required relationships joined
     *
     * @param integer $id
     *
     * @return ScanError
     * @throws Exception
     */
    public function getFullById($id)
    {
        $criteria = new CDbCriteria();
        $criteria->with = [
            'scanErrorReport',
            'status',
            'scanErrorDetails',
            'scanErrorDetails.scanErrorTasks',
            'scanErrorDetails.scanErrorTasks.scanTask',
            'scanErrorDetails.scanErrorLogs',
            'scanErrorDetails.fieldType',
            'scanErrorDetails.type'
        ];

        // Eager loading
        $criteria->together = true;

        return $this->findByPk($id, $criteria);
    }

    /**
     * Get a full ScanError instance.
     * Can be filtered by the $data parameter
     *
     * Returns a custom structure array
     *
     * @param integer $id
     * @param array $data
     *
     * @return array
     * @throws Exception
     */
    public function getScanErrorReportBis($data = [])
    {
        $params = [
            'scan_error_report_id' => Common::get($data, 'scan_error_report_id'),
            'scan_error_id' => Common::get($data, 'scan_error_id'),
            'site_codes' => Common::get($data, 'site_codes', []),
            'site_codes_per' => Common::get($data, 'site_codes_per', []),
            'status_ids' => Common::get($data, 'status_ids', []),
            'type_ids' => Common::get($data, 'error_type_ids', []),
            'organizations' => Common::get($data, 'organizations', []),
            'nasp_date' => Common::get($data, 'nasp_date', 'yes'),
            'verified' => Common::get($data, 'verified', 'all'),
            'partner_sites' => Common::get($data, 'partner_sites', []),
            'provider_name' => Common::get($data, 'provider_name', ''),
            'practice_name' => Common::get($data, 'practice_name', ''),
            'full_result' => Common::get($data, 'full_result', false)
        ];

        if (!$params['organizations'] && $params['partner_sites']) {
            $params['organizations'] = Organization::model()->getIdsFromPartners($params['partner_sites']);
        }

        $practiceResults = $this->getPracticeReport($params);
        $providerResults = $this->getProviderReport($params);

        return [
            'practice' => $practiceResults,
            'provider' => $providerResults
        ];
    }

    /**
     * Get all the scan errors related to a practice
     *
     * @param array $params
     *
     * @return array
     * @throws Exception
     */
    public function getPracticeReport($params)
    {
        $criteria = $this->getPracticeCriteria($params);
        $pages = null;

        if (!$params['full_result']) {
            $count = ScanError::model()->count($criteria);
            $pages = new DPagination($count);
            $pages->pageSize = 10;
            $pages->pageVar = 'tpage';
            $pages->applyLimit($criteria);
        }

        $result = ScanError::model()->findAll($criteria);

        return ['result' => $result, 'pager' => $pages];
    }

    /**
     * Get all the scan errors related to a provider
     *
     * @param array $params
     *
     * @return array
     * @throws Exception
     */
    public function getProviderReport($params)
    {
        $criteria = $this->getProviderCriteria($params);
        $pages = null;

        if (!$params['full_result']) {
            $count = ScanError::model()->count($criteria);
            $pages = new DPagination($count);
            $pages->pageSize = 10;
            $pages->applyLimit($criteria);
        }

        $result = ScanError::model()->findAll($criteria);

        return ['result' => $result, 'pager' => $pages];
    }

    /**
     * Get the base criteria to build scan error searches.
     *
     * @param array $params
     *
     * @return CDbCriteria
     * @throws Exception
     */
    protected function getBaseCriteria($params)
    {
        $criteria = new CDbCriteria;
        $criteria->with = [
            'scanErrorReport',
            'status',
            'scanErrorDetails',
            'scanErrorLogs',
            'scanErrorTasks'
        ];

        // Eager loading
        //$criteria->together = true;

        $criteria->compare('t.scan_error_report_id', $params['scan_error_report_id']);
        $criteria->compare('t.id', $params['scan_error_id']);
        $criteria->compare('t.status_id', $params['status_ids']);
        $criteria->compare('scanErrorDetails.type_id', $params['type_ids']);
        $criteria->compare('org.id', $params['organizations']);

        return $criteria;
    }

    /**
     * Get the criteria to search scan errors for a practice.
     *
     * @param array $params
     *
     * @return CDbCriteria
     * @throws Exception
     */
    protected function getPracticeCriteria($params)
    {
        $criteria = $this->getBaseCriteria($params);

        // Add specific practice conditions
        $criteria->with[] = 'scanResultPractice';
        $criteria->with[] = 'scanResultPractice.scanRequestPractice';
        $criteria->with[] = 'scanResultPractice.scanRequestPractice.scanRequest';
        $criteria->with[] = 'scanResultPractice.scanRequestPractice.primarySpecialty';
        $criteria->with['scanResultPractice.scanRequestPractice.scanRequest.organization'] = [
            'alias' => 'org'
        ];
        $criteria->with['scanResultPractice.listingType'] = [
            'alias' => 'listing'
        ];
        $criteria->with['scanResultPractice.scanRequestPractice.practice'] = [
            'alias' => 'pract'
        ];

        if ($params['nasp_date'] === 'yes') {
            $criteria->addCondition('pract.nasp_date IS NOT NULL');
        } elseif ($params['nasp_date'] === 'no') {
            $criteria->addCondition('pract.nasp_date IS NULL');
        }

        if ($params['verified'] === 'yes') {
            $criteria->compare('pract.verified', 1);
        } elseif ($params['nasp_date'] === 'no') {
            $criteria->compare('pract.verified', 0);
        }

        if (isset($params['practice_name']) && $params['practice_name']) {
            $criteria->compare('pract.name', $params['practice_name'], true);
        }

        $criteria->compare('listing.per', 'T');
        $criteria->compare('listing.code', $params['site_codes_per']['practices']);

        $criteria->order = 'listing.id ASC, pract.name ASC';

        return $criteria;
    }

    /**
     * Get the criteria to search scan errors for a practice.
     *
     * @param array $params
     *
     * @return CDbCriteria
     * @throws Exception
     */
    protected function getProviderCriteria($params)
    {
        $criteria = $this->getBaseCriteria($params);

        // Add specific provider conditions
        $criteria->with[] = 'scanResultProvider';
        $criteria->with[] = 'scanResultProvider.scanRequestProvider';
        $criteria->with[] = 'scanResultProvider.scanRequestProvider.primarySpecialty';
        $criteria->with[] = 'scanResultProvider.scanRequestProvider.scanRequestPractice';
        $criteria->with[] = 'scanResultProvider.scanRequestProvider.scanRequestPractice.scanRequest';
        $criteria->with['scanResultProvider.scanRequestProvider.scanRequestPractice.scanRequest.organization'] = [
            'alias' => 'org'
        ];
        $criteria->with['scanResultProvider.listingType'] = [
            'alias' => 'listing'
        ];
        $criteria->with['scanResultProvider.scanRequestProvider.provider'] = [
            'alias' => 'prov'
        ];

        if ($params['nasp_date'] === 'yes') {
            $criteria->addCondition('prov.nasp_date IS NOT NULL');
        } elseif ($params['nasp_date'] === 'no') {
            $criteria->addCondition('prov.nasp_date IS NULL');
        }

        if ($params['verified'] === 'yes') {
            $criteria->compare('prov.verified', 1);
        } elseif ($params['verified'] === 'no') {
            $criteria->compare('prov.verified', 0);
        }

        if (isset($params['provider_name']) && $params['provider_name']) {
            $criteria->compare('prov.first_middle_last', $params['provider_name'], true);
        }

        $criteria->compare('listing.per', 'V');
        $criteria->compare('listing.code', $params['site_codes_per']['providers']);

        $criteria->order = 'listing.id ASC, prov.first_middle_last ASC';

        return $criteria;
    }

    /**
     * Get the previous instance of this error
     *
     * @param array $scanResult
     * @param integer $currentScanErrorReportId
     *
     * @return ScanError
     */
    public function findPreviousResult($scanResult, $currentScanErrorReportId)
    {
        // Set a practice (or not practice) result or provider result?
        $practiceResult = true;
        if (!empty($scanResult['provider_id'])) {
            $practiceResult = false;
        }

        // Get the results for the previous SQL query
        $valuesToBind = [
            ":listing_type_id" => $scanResult['listing_type_id'],
            ":organization_id" => $scanResult['organization_id'],
            ":scan_error_report_id" => $currentScanErrorReportId
        ];

        // Is it a practice result?
        if ($practiceResult) {
            // Yes, get the previous Practice result
            $sql = $this->getPreviousPracticeResultSql();
            $valuesToBind[':practice_id'] = $scanResult['practice_id'];
        } else {
            // No, get the previous Provider result
            $sql = $this->getPreviousProviderResultSql();
            $valuesToBind[':provider_id'] = $scanResult['provider_id'];
            $valuesToBind[':practice_id'] = $scanResult['practice_id'];
        }

        return ScanError::model()->findBySql($sql, $valuesToBind);
    }

    /**
     * Get the SQL to find the previous instance of this practice error
     *
     * @return string
     */
    protected function getPreviousPracticeResultSql()
    {
        return "
            SELECT
               scan_error.*
            FROM scan_error
              JOIN scan_result_practice ON scan_error.scan_result_practice_id = scan_result_practice.id
              JOIN scan_request_practice ON scan_result_practice.scan_request_practice_id = scan_request_practice.id
              JOIN scan_request ON scan_request_practice.scan_request_id = scan_request.id
            WHERE
                scan_request_practice.practice_id = :practice_id
                AND scan_error.scan_result_provider_id IS NULL
                AND scan_result_practice.listing_type_id = :listing_type_id
                AND scan_request.organization_id = :organization_id
                AND scan_error.scan_error_report_id != :scan_error_report_id
            ORDER BY scan_error.id DESC LIMIT 1
        ";
    }

    /**
     * Get the SQL to find the previous instance of this provider error
     *
     * @return string
     */
    protected function getPreviousProviderResultSql()
    {
        return "
            SELECT
               scan_error.*
            FROM scan_error
                JOIN scan_result_provider ON scan_error.scan_result_provider_id = scan_result_provider.id
                JOIN scan_request_provider ON scan_result_provider.scan_request_provider_id = scan_request_provider.id
                JOIN scan_request_practice ON scan_request_provider.scan_request_practice_id = scan_request_practice.id
                JOIN scan_request ON scan_request_practice.scan_request_id = scan_request.id
            WHERE
                scan_request_provider.provider_id = :provider_id
                AND scan_error.scan_result_practice_id IS NULL
                AND scan_result_provider.listing_type_id = :listing_type_id
                AND scan_request.organization_id = :organization_id
                AND scan_request_practice.practice_id = :practice_id
                AND scan_error.scan_error_report_id != :scan_error_report_id
            ORDER BY scan_error.id DESC LIMIT 1
        ";
    }

    /**
     * Is the status id "ScanErrorStatus::RESOLVED" ?
     *
     * @return boolean
     */
    public function isResolved()
    {
        return ($this->status->code == 'scan_error_status_resolved');
    }

    /**
     * Check if the record has any error (null for no errors)
     *
     * @return boolean
     */
    public function hasError()
    {
        if (!$this->status_id || $this->status->code === 'scan_error_status_ok') {
            return false;
        }

        return true;
    }

    /**
     * Is this a new scan error?
     *
     * @return boolean
     */
    public function isNew()
    {
        return ($this->status->code == 'scan_error_status_new');
    }

    /**
     * Is the status id "ScanErrorStatus::IN_PROGRESS" ?
     *
     * @return boolean
     */
    public function isInProgress()
    {
        return ($this->status->code == 'scan_error_status_in_progress');
    }

    /**
     * Is the status id "ScanErrorStatus::ADDRESSED" ?
     *
     * @return boolean
     */
    public function isAddressed()
    {
        return ($this->status->code == 'scan_error_status_addressed');
    }

    /**
     * Is this a practice scan error?
     *
     * @return boolean
     */
    public function isPractice()
    {
        if ($this->scan_result_practice_id) {
            return true;
        }

        return false;
    }

    /**
     * Is this a practice scan error?
     *
     * @return boolean
     */
    public function hasPractice()
    {
        $practice = $this->getScanRequestPractice()->practice;
        return (bool)$practice;
    }

    /**
     * Is this a provider scan error?
     *
     * @return boolean
     */
    public function isProvider()
    {
        if ($this->scan_result_provider_id) {
            return true;
        }

        return false;
    }

    /**
     * Get the ScanRequestPractice related to this instance
     *
     * @return ScanRequestPractice
     */
    public function getScanRequestPractice()
    {
        if ($this->isPractice()) {
            return $this->scanResultPractice->scanRequestPractice;
        } else {
            return $this->scanResultProvider
                ->scanRequestProvider
                ->scanRequestPractice;
        }
    }

    /**
     * Get the ScanRequest related to this instance
     *
     * @return ScanRequest
     */
    public function getScanRequest()
    {
        return $this->getScanRequestPractice()->scanRequest;
    }

    /**
     * Get the Organization related to this instance
     *
     * @return Organization
     */
    public function getOrganization()
    {
        return $this->getScanRequest()->organization;
    }

    /**
     * Get the Practice related to this instance
     *
     * @return Practice
     */
    public function getPractice()
    {
        if ($this->isPractice()) {
            return $this->scanResultPractice->scanRequestPractice->practice;
        }

        return null;
    }

    /**
     * Get the practice name from the search form
     *
     * @return ScanRequestPractice
     */
    public function getPracticeName()
    {
        return $this->getScanRequestPractice()->practice_name;
    }

    /**
     * Get the practice full address
     *
     * @return ScanRequestPractice
     */
    public function getPracticeFullAddress()
    {
        $practice = $this->getScanRequestPractice()->practice;
        $address = '';
        if ($practice) {
            $address = $practice->getFullAddress();
        }

        return $address;
    }

    /**
     * Get the practice partial address.
     * Get all the address components except the main address.
     *
     * @return ScanRequestPractice
     */
    public function getPracticePartialAddress()
    {
        $practice = $this->getScanRequestPractice()->practice;
        $address = '';
        if ($practice) {
            $address = $practice->getPartialAddress();
        }

        return $address;
    }

    /**
     * Get the practice id
     *
     * @return integer
     */
    public function getPracticeId()
    {
        if ($this->isProvider()) {
            return $this->getScanRequestPractice()->practice_id;
        } else {
            return $this->getPractice()->id;
        }
    }

    /**
     * Get the scan log id
     *
     * @return integer
     */
    public function getScanLogId()
    {
        return $this->getScanRequest()->scan_log_id;
    }

    /**
     * Get the DDC Url for this instance
     * Can return a provider or practice url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->getGenericResult()->url;
    }

    /**
     * Get the ListingType related to this instance
     *
     * @return ListingType
     */
    public function getListingType()
    {
        return $this->getGenericResult()->listingType;
    }

    /**
     * Returns an interface to the scan result with
     * common methods for both provider and practice results.
     *
     * @return GenericScanResult
     */
    public function getGenericResult()
    {
        if ($this->isPractice()) {
            return $this->scanResultPractice;
        } else {
            return $this->scanResultProvider;
        }
    }

    /**
     * Get the Provider related to this instance
     *
     * @return Provider
     */
    public function getProvider()
    {
        if ($this->isProvider()) {
            return $this->scanResultProvider->scanRequestProvider->provider;
        }
    }

    /**
     * Get the Provider or Practice name
     *
     * @return string
     */
    public function getName()
    {
        if ($this->isPractice()) {
            return $this->getPracticeName();
        } else {
            return $this->getProvider()->getFullName();
        }
    }

    /**
     * Get a scraped value
     *
     * @param $field The field code
     *
     * @return mixed
     */
    public function getScrappedValue($field)
    {
        $attr = 'scraped_' . $field;

        return Common::get($this->getGenericResult(), $attr, '');
    }

    /**
     * Get a field value
     *
     * @param $field The field code
     *
     * @return mixed
     */
    public function getFieldValue($field)
    {
        return Common::get($this->getGenericResult(), $field, '');
    }

    /**
     * Determine if this ScanError has any scraped data
     *
     * @return boolean
     */
    public function hasScrapedData()
    {
        $result = $this->getGenericResult();

        return $result->getScrapedName() && $result->getScrapedAddress() && $result->getScrapedPhone();
    }

    /**
     * Get the listing's logo path
     *
     * @return string
     */
    public function getListingLogoPath()
    {
        return $this->getListingType()->logo_path;
    }

    /**
     * Get the base url of a listing
     *
     * @return string
     */
    public function getListingBaseUrl()
    {
        $arrUrls = explode(',', $this->getListingType()->base_url);
        if (!empty($arrUrls)) {
            return array_shift($arrUrls);
        }

        return null;
    }

    /**
     * has the name matched?
     *
     * @return boolean
     */
    public function matchName()
    {
        $result = $this->getGenericResult();
        return (bool)$result->match_name;
    }

    /**
     * has the address matched?
     *
     * @return boolean
     */
    public function matchAddress()
    {
        $result = $this->getGenericResult();
        return (bool)$result->match_address;
    }

    /**
     * has the phone matched?
     *
     * @return boolean
     */
    public function matchPhone()
    {
        $result = $this->getGenericResult();
        return (bool)$result->match_phone;
    }

    /**
     * is this a 'not found' ?
     *
     * @return boolean
     */
    public function notFound()
    {
        if (!$this->matchName() && !$this->matchAddress() && !$this->matchPhone()) {
            return true;
        }

        return false;
    }

    /**
     * Get the original scan url
     *
     * @return string
     */
    public function getHistoricalScanUrl()
    {
        return Yii::app()->params['servers']['scan_web'] . '/historical?id=' . $this->getScanLogId();
    }

    /**
     * Get a related ScanErrorDetail by it's id
     *
     * @param integer $id
     *
     * @return ScanErrorDetail
     */
    public function getDetailById($id)
    {
        if ($this->scanErrorDetails) {
            foreach ($this->scanErrorDetails as $detail) {
                if ($detail->id == $id) {
                    return $detail;
                }
            }
        }

        return null;
    }

    /**
     * Can this ScanError be saved?
     *
     * Older reports should be read-only in the UI.
     *
     * @return boolean
     */
    public function canSave()
    {
        $lastReport = ScanErrorReport::model()->getLast();

        if ($this->scanErrorReport->id === $lastReport->id) {
            return true;
        }

        return false;
    }

    /**
     * Creates a custom structure array to be sent to the UI
     * so it can update the layout and values according to
     * this ScanError
     *
     * @param integer $id
     *
     * @return array
     */
    public function getListingToolData($id)
    {
        $data = [
            'scan_error_id' => $id,
            'details' => [],
            'status' => [
                'id' => '',
                'name' => '',
                'code' => ''
            ]
        ];
        $scanError = $this->getFullById($id);

        if ($scanError && $scanError->scanErrorDetails) {
            $data['status']['id'] = $scanError->status->id;
            $data['status']['name'] = $scanError->status->name;
            $data['status']['code'] = $scanError->status->code;

            foreach ($scanError->scanErrorDetails as $detail) {
                $data['details'][$detail->id] = [];
                $data['details'][$detail->id]['latest_task'] = $detail->getLatestTask() ? $detail->getLatestTask()->url : '';
                $data['details'][$detail->id]['type'] = [
                    'id' => $detail->type->id,
                    'code' => $detail->type->code,
                    'name' => $detail->type->name
                ];
            }
        }

        return $data;
    }

    /**
     * Get the details to display in the details popup
     *
     * @return array
     */
    public function getErrorFormDetails()
    {
        $details = [];

        foreach ($this->scanErrorDetails as $detail) {
            if (!$detail->matched()) {
                if ($detail->fieldType->code === 'scan_field_photo') {
                    if ($this->isProvider()) {
                        $details[] = $detail;
                    }
                } else {
                    $details[] = $detail;
                }
            } else {
                $details[] = $detail;
            }
        }

        return $details;
    }

    /**
     * Does this ScanError have a provider and a practice related?
     *
     * @return boolean
     */
    public function hasProviderAndPractice()
    {
        if ($this->getProvider() && $this->getPractice()) {
            return true;
        }

        return false;
    }

    /**
     * Get the main table details for the UI
     *
     * @return array
     */
    public function getMainTableDetails()
    {
        $details = ['default' => [], 'extra' => []];

        foreach ($this->scanErrorDetails as $detail) {
            if ($detail->fieldType && $detail->fieldType->isDefaultForScanError()) {
                if ($detail->fieldType->code === 'scan_field_photo') {
                    if ($this->isProvider()) {
                        $details['default'][] = $detail;
                    }
                } else {
                    $details['default'][] = $detail;
                }
            } else {
                $details['extra'][] = $detail;
            }
        }

        return array_merge($details['default'], $details['extra']);
    }

    /**
     * Determine if the scan error has non-defailt fields added.
     * Eg: Office hours, website, etc.
     *
     * @return boolean
     */
    public function hasNonDefaultFields()
    {
        foreach ($this->scanErrorDetails as $detail) {
            if (!$detail->fieldType->isDefaultForScanError()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get the account related to this instance
     *
     * @return array
     */
    public function getRelatedAccount()
    {
        $account = null;
        if ($this->hasProviderAndPractice()) {
            $account = Account::getAccountBasedOnProviderAndPractice(
                $this->getProvider()->id,
                $this->getPractice()->id,
                true
            );
        }

        if (!$account && $this->isProvider()) {
            $account = AccountProvider::model()->getProviderAccount($this->getProvider()->id);
            if ($account) {
                $account = $account->id;
            }
        }

        if (!$account && $this->isPractice()) {
            $account = ProviderPractice::model()->getAccountId(
                $this->getPractice()->id
            );
        }

        return $account;
    }

    /**
     * Update the ScanErrorDetail instances due to changes
     *
     * Also updates the logs.
     *
     * @return void
     */
    public function handleDetailsChange()
    {
        $statusChanged = false;
        if ($this->isNew()) {
            $status = Status::model()->getScanErrorStatusByCode('scan_error_status_in_progress');
            $this->status = $status;
            $this->status_id = $status->id;
            $this->save(false);
            $statusChanged = true;
        }

        if ($statusChanged) {
            ScanErrorLog::model()->scanErrorStatusChange($this);
        }
    }

    public function getEntityUrl()
    {
        if ($this->isProvider()) {
            return $this->getProvider()->getUrl();
        } else {
            return $this->getPractice()->getUrl();
        }
    }

    /**
     * Get all the features related to this instance's organization
     *
     * @return Feature[]
     */
    public function getFeatures($onlyActive = true)
    {
        $features = [];
        if ($this->getOrganization()) {
            if ($this->isPractice()) {
                $attrs = [
                    'organization_id' => $this->getOrganization()->id,
                    'practice_id' => $this->getPractice()->id
                ];

                if ($onlyActive) {
                    $attrs['active'] = 1;
                }

                $orgFeatures = OrganizationFeature::model()->with('feature')->findAllByAttributes($attrs);
            } else {
                $attrs = [
                    'organization_id' => $this->getOrganization()->id,
                    'provider_id' => $this->getProvider()->id
                ];

                if ($onlyActive) {
                    $attrs['active'] = 1;
                }

                $orgFeatures = OrganizationFeature::model()->with('feature')->findAllByAttributes($attrs);
            }

            if ($orgFeatures) {
                foreach ($orgFeatures as $orgFeature) {
                    $addFeature = false;
                    if (
                        ($this->isHealthgrades() && $this->isHealthgradesFeature($orgFeature->feature))
                        ||
                        ($this->isVitals() && $this->isVitalsFeature($orgFeature->feature))
                        ||
                        $this->isGlobalFeature($orgFeature->feature)
                    ) {
                        $addFeature = true;
                    }

                    if ($addFeature) {
                        $features[] = $orgFeature->feature;
                    }
                }
            }
        }

        return $features;
    }

    /**
     * Get all the Healthgrades features related to this instance's organization
     *
     * @return Feature[]
     */
    public function getHealthgradesFeatures()
    {
        $features = $this->getFeatures();
        $hgFeatures = [];

        if ($features) {
            foreach ($features as $feature) {
                if ($this->isHealthgradesFeature($feature)) {
                    $hgFeatures[] = $feature;
                }
            }
        }

        return $hgFeatures;
    }

    /**
     * Get all the Healthgrades profile feature (if any)
     *
     * @return Feature
     */
    public function getHealthgradesProfileFeature()
    {
        $features = $this->getHealthgradesFeatures();
        $profileFeature = null;
        if ($features) {
            foreach ($features as $feature) {
                if ($feature->id === 14 || (!$profileFeature && $feature->id === 15)) {
                    $profileFeature = $feature;
                }
            }
        }

        return $profileFeature;
    }

    /**
     * Get the Salesforce name of the Healthgrades plan
     *
     * @return string
     */
    public function getHealthgradesProfileForSalesforce()
    {
        $picklistValue = 'Basic';
        $feature = $this->getHealthgradesProfileFeature();
        if ($feature) {
            if ($feature->id === 14) {
                $picklistValue = 'Premium';
            } elseif ($feature->id === 15) {
                $picklistValue = 'Enhanced';
            }
        }

        return $picklistValue;
    }

    /**
     * Determine if the feature belongs to Healthgrades
     *
     * @param Feature $feature
     *
     * @return boolean
     */
    protected function isHealthgradesFeature(Feature $feature)
    {
        return in_array($feature->id, $this->healthgradesFeatures());
    }

    /**
     * Determine if the feature belongs to Vitals
     *
     * @param Feature $feature
     *
     * @return boolean
     */
    protected function isVitalsFeature(Feature $feature)
    {
        return in_array($feature->id, $this->vitalsFeatures());
    }

    /**
     * Determine if the feature applies to any listing
     *
     * @param Feature $feature
     *
     * @return boolean
     */
    protected function isGlobalFeature(Feature $feature)
    {
        return in_array($feature->id, $this->globalFeatures());
    }

    /**
     * Get the ids of Healthgrades features
     *
     * @return array
     */
    protected function healthgradesFeatures()
    {
        return ['14', '15', '18'];
    }

    /**
     * Get the ids of Vitals features
     *
     * @return array
     */
    protected function vitalsFeatures()
    {
        return ['16'];
    }

    /**
     * Get the ids of Global features
     *
     * @return array
     */
    protected function globalFeatures()
    {
        return ['4'];
    }

    /**
     * Is the related ListingType from Healthgrades?
     *
     * @return boolean
     */
    public function isHealthgrades()
    {
        if ($this->getListingType()->code == 'healthgrades') {
            return true;
        }

        return false;
    }

    /**
     * Is the related ListingType from Vitals?
     *
     * @return boolean
     */
    public function isVitals()
    {
        if ($this->getListingType()->code == 'vitals') {
            return true;
        }

        return false;
    }

    /**
     * Is the related ListingType from Google?
     *
     * @return boolean
     */
    public function isGoogle()
    {
        $gValues = ['google', 'googleProvider'];
        if (in_array($this->getListingType()->code, $gValues)) {
            return true;
        }

        return false;
    }

    /**
     * Is the related ListingType claimed?
     *
     * @return boolean
     */
    public function isClaimed()
    {
        if ($this->isProvider()) {
            $claimStatus = ProviderPracticeGmb::model()->getClaimStatus($this->getScanRequestPractice()->practice_id, $this->getProvider()->id);
            return $claimStatus['provider_practice_claimed'];
        } else {
            $claimStatus = ProviderPracticeGmb::model()->getClaimStatus($this->getPractice()->id);
            return $claimStatus['practice_claimed'];
        }
    }

    /**
     * Is the GMB listing read only?
     *
     * @return boolean
     */
    public function isGmbReadonly()
    {
        if ($this->isProvider()) {
            $gmb = ProviderPracticeGmb::model()->getByPracticeAndProvider($this->getScanRequestPractice()->practice_id, $this->getProvider()->id);
        } else {
            $gmb = ProviderPracticeGmb::model()->getByPracticeAndProvider($this->getPractice()->id);
        }

        return ($gmb && $gmb->is_read_only);
    }

    /**
     * Is the listing read only?
     *
     * @return boolean
     */
    public function isReadonly()
    {
        if ($this->isGoogle() && $this->isGmbReadonly()) {
            return true;
        }

        return false;
    }

    /**
     * Get the name ScanErrorDetail
     *
     * @return ScanErrorDetail
     */
    public function getNameDetail()
    {
        return $this->getDetailByCode('scan_field_name');
    }

    /**
     * Get the address ScanErrorDetail
     *
     * @return ScanErrorDetail
     */
    public function getAddressDetail()
    {
        return $this->getDetailByCode('scan_field_address');
    }

    /**
     * Get the phone ScanErrorDetail
     *
     * @return ScanErrorDetail
     */
    public function getPhoneDetail()
    {
        return $this->getDetailByCode('scan_field_phone');
    }

    /**
     * Get a related ScanErrorDetail by it's field type code
     *
     * @return ScanErrorDetail
     */
    public function getDetailByCode($code)
    {
        if ($this->scanErrorDetails) {
            foreach ($this->scanErrorDetails as $detail) {
                if ($detail->fieldType->code === $code) {
                    return $detail;
                }
            }
        }

        return null;
    }

    /**
     * Get the total amount of issues from this error
     *
     * @return integer
     */
    public function getIssuesCount()
    {
        $count = 0;
        if ($this->scanErrorDetails) {
            foreach ($this->scanErrorDetails as $detail) {
                if ($detail->isError()) {
                    $count++;
                }
            }
        }

        return $count;
    }

    /**
     * Get the total amount of issues from this error
     *
     * @return integer
     */
    public function getIssueFields()
    {
        $fields = [];
        if ($this->scanErrorDetails) {
            foreach ($this->scanErrorDetails as $detail) {
                if ($detail->isError()) {
                    $fields[] = $detail->fieldType->name;
                }
            }
        }

        return implode(', ', $fields);
    }

    /**
     * Get the scraped photo from this error's provider.
     * Can be empty.
     *
     * @return string
     */
    public function getProviderScrapedPhoto()
    {
        return Common::get($this, 'scanResultProvider->photo', '');
    }

    /**
     * Get the scraped appointments url.
     * Can be empty.
     *
     * @return string
     */
    public function getScrapedAppointmentsUrl()
    {
        return Common::get($this, 'scanResultProvider->appointments_url', '');
    }

    /**
     * Utility function to determine if the photo is present,
     * but it's a listing's default image (which is the same as not having one)
     *
     * @return boolean
     */
    public function isDefaultImage()
    {
        $result = (int) Common::get($this, 'scanResultProvider->default_photo');
        return (bool)($result === 1);
    }

    /**
     * Utility function to determine if this listing has appointments enabled
     *
     * @return boolean
     */
    public function hasAppointments()
    {
        $result = (int) Common::get($this, 'scanResultProvider->has_appointments');
        return (bool)($result === 1);
    }

    /**
     * Utility function to determine if this listing has DDC appointments.
     * DDC Appointments follow this pattern:
     *    -> https://patients.doctor.com/directAppointment?provider=DDC_PROVIDER_ID&partner=LISTING_NAME
     *
     * @return boolean
     */
    public function hasDdcAppointments()
    {
        $result = (int) Common::get($this, 'scanResultProvider->ddc_appointments');
        return (bool)($result === 1);
    }
}
