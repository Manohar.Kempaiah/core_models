<?php

Yii::import('application.modules.core_models.models._base.BaseScanTemplate');

class ScanTemplate extends BaseScanTemplate
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Scan Template|Scan Templates', $n);
    }

    /**
     * Find a template its listing types related
     * @param string $type type of template (I.E: Internal, Sales)
     * @param string $name name of template (I.E: Medical Scan, Chiropractic Scan, Dental Scan, Podiatric Scan)
     * @param string $per practice (T), provider (V)
     * @param boolean $associated indicates whether is looking for primary or associated providers
     *
     * @return array
     */
    public static function getTemplate($typeId, $name, $per, $associated = false)
    {
        // Apply base condition, filter by practice (T) or provider (V)
        $condition = "listing_type.per = '" . $per . "' ";

        // Add extra condition, filter by primary or associated provider
        if ($per == 'V') {
            if ($associated) {
                $condition .= " AND scan_template_listing_type.provider_secondary_only ";
            } else {
                $condition .= " AND scan_template_listing_type.provider_primary_only ";
            }
        }

        // Build the query
        $sql = sprintf(
            "SELECT
                scan_template.id AS `scan_template_id`, scan_template.version AS `scan_template_version`,
                listing_type.name AS `title`, listing_type.code AS `code`,
                scan_template_listing_type.section AS `section`, listing_type.`engine` AS `engine`,
                scan_template_listing_type.weight AS `weight`, scan_template_listing_type.max_age AS `max_age`,
                scan_template_listing_type.type AS `type`
            FROM
                listing_site
                INNER JOIN listing_type ON listing_type.listing_site_id = listing_site.id
                INNER JOIN scan_template_listing_type ON scan_template_listing_type.listing_type_id = listing_type.id
                INNER JOIN scan_template ON scan_template.id = scan_template_listing_type.scan_template_id
            WHERE
                scan_template.scan_template_type_id = %d AND scan_template.internal_name = '%s' AND scan_template.active
                AND %s
            ORDER BY
                scan_template_listing_type.section, isnull(listing_type.sort_order), listing_type.sort_order,
                scan_template_listing_type.weight DESC, listing_type.name",
            $typeId,
            $name,
            $condition
        );

        // Return the result of the given query
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from scan template table if associated records exist in scan request table
        $hasAssociatedRecords = ScanRequest::model()->exists(
            'scan_template_id=:scan_template_id',
            array(':scan_template_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError(
                'id',
                "The Scan Template cannot be removed because it's linked to Scan Requests. "
                    . "You can disable it from the Modify screen."
            );
            return false;
        }

        // Delete Scan Template Listing Type Version
        $scanTLTV = ScanTemplateListingTypeVersion::model()->findAll(
            'scan_template_id=:scan_template_id',
            array(':scan_template_id' => $this->id)
        );
        if ($scanTLTV) {
            foreach ($scanTLTV as $eachScan) {
                $eachScan->delete();
            }
        }

        // Delete Scan Template Listing Type
        $scanTLT = ScanTemplateListingType::model()->findAll(
            'scan_template_id=:scan_template_id',
            array(':scan_template_id' => $this->id)
        );
        if ($scanTLT) {
            foreach ($scanTLT as $eachScan) {
                $eachScan->delete();
            }
        }

        // get account info
        if (php_sapi_name() != 'cli') {
            $accountId = Yii::app()->user->id;
        } else {
            $accountId = 1;
        }

        // Insert Audit
        if (!empty($accountId)) {
            AuditLog::create(
                'D',
                'Scan Template by account ' . $accountId,
                'scan_template_listing_type_version',
                null,
                false
            );
        }

        return parent::beforeDelete();
    }

    /**
     * Template Listing
     * @param int $scanTemplateId
     * @return array
     */
    public static function templateListing($scanTemplateId)
    {
        // List of Listings sites configured for a specific template
        $sql = sprintf(
            "SELECT
            listing_type.per,
            IF(listing_type.per='V','Provider', IF(per='T','Practice','Provider at Practice')) as listing_type_per,
            scan_template_listing_type.id AS id,
            scan_template_listing_type.type AS type,
            listing_site.name AS listing_site_name,
            listing_site.id AS listing_site_id,
            scan_template_listing_type.listing_type_id AS listing_type_id,
            scan_template_listing_type.max_age AS max_age,
            scan_template_listing_type.weight AS weight,
            scan_template_listing_type.section AS section,
            IF(listing_type.per<>'T', scan_template_listing_type.provider_primary_only, 0) AS provider_primary_only,
            IF(listing_type.per<>'T', scan_template_listing_type.provider_secondary_only, 0) AS provider_secondary_only,
            scan_template_listing_type.date_added,
            CONCAT(account.first_name, ' ',account.last_name) AS account_name
            FROM scan_template_listing_type
            INNER JOIN listing_type ON scan_template_listing_type.listing_type_id = listing_type.id
            INNER JOIN listing_site ON listing_type.listing_site_id = listing_site.id
            INNER JOIN account ON account.id = scan_template_listing_type.added_by_account_id
            WHERE scan_template_id = %d
            ORDER BY listing_type.per,
                scan_template_listing_type.section, isnull(listing_type.sort_order), listing_type.sort_order,
                scan_template_listing_type.weight DESC, listing_type.name;",
            $scanTemplateId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Template Listing Version
     * @param int $scanTemplateId
     * @return array
     */
    public static function templateListingVersion($scanTemplateId)
    {
        // List of Listings version
        $sql = sprintf(
            "SELECT
            scan_template_listing_type_version.version_date_added AS datetime_log,
            scan_template_listing_type_version.scan_template_version AS scan_template_version,
            scan_template_listing_type_version.id AS scan_template_listing_type_id,
            scan_template_listing_type_version.listing_type_id AS listing_type_id,
            scan_template_listing_type_version.scan_template_id AS scan_template_id,
            scan_template_listing_type_version.max_age AS max_age,
            scan_template_listing_type_version.weight AS weight,
            scan_template_listing_type_version.section As section,
            scan_template_listing_type_version.provider_primary_only AS provider_primary_only,
            scan_template_listing_type_version.provider_secondary_only AS provider_secondary_only,
            DATE_FORMAT(scan_template_listing_type_version.date_added, '%%m-%%d-%%Y %%H:%%i') AS date_added,
            scan_template_listing_type_version.date_updated AS date_updated,
            IF(listing_type.per='V','Provider', IF(per='T','Practice','Provider at Practice')) as listing_type_per,
            listing_site.name AS listing_site_name,
            CONCAT(account.first_name, ' ',account.last_name) AS account_name
            FROM scan_template_listing_type_version
            INNER JOIN scan_template
            ON scan_template.id = scan_template_listing_type_version.scan_template_id
            INNER JOIN listing_type
            ON scan_template_listing_type_version.listing_type_id = listing_type.id
            INNER JOIN listing_site
            ON listing_type.listing_site_id = listing_site.id
            INNER JOIN account
            ON account.id = scan_template_listing_type_version.added_by_account_id
            WHERE scan_template.id = %d
            ORDER BY
                scan_template_listing_type_version.scan_template_version DESC,
                scan_template_listing_type_version.version_date_added,
                listing_type.per,
                scan_template_listing_type_version.section, isnull(listing_type.sort_order), listing_type.sort_order,
                scan_template_listing_type_version.weight DESC, listing_type.name;",
            $scanTemplateId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Template Listing
     * @return array
     */
    public static function templateListingType()
    {
        // List of Listings sites
        $sql = "SELECT
            listing_type.id AS listing_type_id,
            listing_type.name AS listing_site_name,
            listing_type.per
            FROM listing_type
            INNER JOIN listing_site
                ON listing_type.listing_site_id = listing_site.id
            WHERE url_type='P'
                AND listing_site.active='1'
                AND listing_type.active='1'
                AND engine IS NOT NULL
                AND engine <> ''
                AND per IN ('V','T')
            ORDER BY listing_type.per, listing_site.name; ";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the specialties to be shown on PDT
     * @param int $scanTemplateTypeId
     * @param bool $showInPdt
     * @return array
     */
    public static function getSpecialties($scanTemplateTypeId, $showInPdt = true)
    {
        // List of specialties
        $sql = "SELECT internal_name FROM scan_template " .
            " WHERE active = 1 AND scan_template_type_id = :scanTemplateTypeId";

        if ($showInPdt) {
            $sql .= " AND shown_in_pdt = 1";
        }

        return Yii::app()->db->createCommand($sql)
            ->bindValue(':scanTemplateTypeId', $scanTemplateTypeId, PDO::PARAM_INT)
            ->queryAll();
    }

    /**
     * Template Listing Update
     *
     * @param int $id
     * @param int $templateIndex
     * @param array $arrTemplate
     * @return array
     */
    public static function templateListingUpdate($id, $templateIndex, $arrTemplate)
    {

        $out = array();
        $out['success'] = true;

        // Insert into Scan Template Listing Type Version
        $sql = sprintf(
            "INSERT INTO scan_template_listing_type_version
            (scan_template_version, scan_template_listing_type_id, listing_type_id, scan_template_id,
            max_age, weight, section, provider_primary_only, provider_secondary_only, date_added, date_updated,
            added_by_account_id)
            SELECT
            scan_template.version AS scan_template_version,
            scan_template_listing_type.id AS scan_template_listing_type_id,
            scan_template_listing_type.listing_type_id AS listing_type_id,
            scan_template_listing_type.scan_template_id AS scan_template_id,
            scan_template_listing_type.max_age AS max_age,
            scan_template_listing_type.weight AS weight,
            scan_template_listing_type.section As section,
            scan_template_listing_type.provider_primary_only AS provider_primary_only,
            scan_template_listing_type.provider_secondary_only AS provider_secondary_only,
            scan_template_listing_type.date_added AS date_added,
            scan_template_listing_type.date_updated AS date_updated,
            scan_template_listing_type.added_by_account_id AS added_by_account_id
            FROM scan_template_listing_type
            INNER JOIN scan_template
            ON scan_template.id = scan_template_listing_type.scan_template_id
            INNER JOIN listing_type
            ON  listing_type.id =  scan_template_listing_type.listing_type_id
            INNER JOIN listing_site
            ON  listing_site.id =  listing_type.listing_site_id
            WHERE scan_template.id = %d
            ORDER BY listing_type.per, listing_site.name; ",
            $id
        );
        Yii::app()->db->createCommand($sql)->execute();

        // Delete Scan Template Listing Type
        $scanTLT = ScanTemplateListingType::model()->findAll(
            'scan_template_id=:scan_template_id',
            array(':scan_template_id' => $id)
        );
        if ($scanTLT) {
            foreach ($scanTLT as $eachScan) {
                if (!$eachScan->delete()) {
                    $out['success'] = false;
                    $out['error'] = json_encode($eachScan->getErrors());
                    break;
                }
            }
        }

        // Insert into Scan Template Listing Type
        for ($i = 0; $i <= $templateIndex; $i++) {

            // If not was deleted
            if ($arrTemplate[$i]['per'] && $arrTemplate[$i]['delete'] == 0) {

                $sTLT = new ScanTemplateListingType();
                $sTLT->listing_type_id = $arrTemplate[$i]['type'];
                $sTLT->type = isset($arrTemplate[$i]['visibility_type']) ? $arrTemplate[$i]['visibility_type']: '';
                $sTLT->scan_template_id = $id;
                $sTLT->max_age = $arrTemplate[$i]['age'];
                $sTLT->weight = $arrTemplate[$i]['weight'];
                $sTLT->section = $arrTemplate[$i]['section'];
                $sTLT->provider_primary_only = $arrTemplate[$i]['primary'];
                $sTLT->provider_secondary_only = $arrTemplate[$i]['secondary'];
                if (php_sapi_name() != 'cli') {
                    $sTLT->added_by_account_id = Yii::app()->user->id;
                } else {
                    $sTLT->added_by_account_id = 1;
                }
                $sTLT->date_added = date("Y-m-d H:i:s");
                if (!$sTLT->save()) {
                    $out['success'] = false;
                    $out['error'] = json_encode($sTLT->getErrors());
                }
            }
        }

        // Update Scan Template
        $scanT = ScanTemplate::model()->findByPk($id);
        if ($scanT) {
            $scanT->version = ($scanT->version + 1);
            if (!$scanT->save()) {
                $out['success'] = false;
                $out['error'] = json_encode($scanT->getErrors());
            }
        }

        // get account info
        if (php_sapi_name() != 'cli') {
            $accountId = Yii::app()->user->id;
        } else {
            $accountId = 1;
        }

        // Insert Audit
        AuditLog::create(
            'U',
            'Scan Template Version #' . $scanT->version .' by account ' . $accountId,
            'scan_template_listing_type_version',
            null,
            false
        );

        return $out;
    }

    /**
     * Search
     * @return \CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('version', $this->version);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('active', $this->active);
        $criteria->compare('shown_in_pdt', $this->shown_in_pdt);
        $criteria->compare('scan_template_type_id', $this->scan_template_type_id);
        $criteria->join = 'JOIN scan_template_type ON t.scan_template_type_id = scan_template_type.id';
        $criteria->order = 'scan_template_type.name, name';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100),
        ));
    }

}
