<?php

Yii::import('application.modules.core_models.models._base.BaseDatabaseConsistencyChecks');

class DatabaseConsistencyChecks extends BaseDatabaseConsistencyChecks
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
