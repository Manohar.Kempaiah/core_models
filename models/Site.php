<?php

Yii::import('application.modules.core_models.models._base.BaseSite');

class Site extends BaseSite
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * get scanned site codes
     * @param string $codeType -> T:Practice | V:Provider
     * @return array
     */
    public static function getScannedSiteCodes($codeType = 'T')
    {
        $sql = sprintf(
            "SELECT DISTINCT lt.code, s.name FROM site AS s
            INNER JOIN listing_type as lt ON  s.id = lt.site_id
            INNER JOIN scan_template_listing_type_version as stltv ON stltv.listing_type_id = lt.id
            WHERE s.active = 1
                AND lt.active = 1
                AND lt.url_type = 'P'
                AND lt.per='%s'
            ORDER BY s.name ASC;",
            $codeType
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

}
