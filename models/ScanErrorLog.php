<?php

Yii::import('application.modules.core_models.models._base.BaseScanErrorLog');

class ScanErrorLog extends BaseScanErrorLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Return log for a givven scan_error->id
     * @param int $scanErrorId
     * @return array
     */
    public static function getByScanErrorId($scanErrorId = 0)
    {
        if (empty($scanErrorId)) {
            return [];
        }

        $sql = sprintf(
            "SELECT scan_error_status_id, scan_error_status.name AS status_name,
            related_task_url, object_type,
            scan_error_type_id, scan_error_type.name AS `type_name`,
            comment, scan_error_log.date_added, added_by_account_id,
            CONCAT(account.first_name, ' ', account.last_name) as account_name
            FROM scan_error_log
            LEFT JOIN scan_error_status
                ON scan_error_status_id=scan_error_status.id
            LEFT JOIN scan_error_type
                ON scan_error_type_id=scan_error_type.id
            LEFT JOIN account
                ON account.id = added_by_account_id
            WHERE scan_error_id = %d
            ORDER BY date_added DESC",
            $scanErrorId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get related_task_Url
     * @param int $scanErrorId
     * @param string $objectType
     *
     * @return mixed
     */
    public static function getRelatedTaskUrl($scanErrorId, $objectType)
    {
        $sql = sprintf(
            "SELECT related_task_url
            FROM scan_error_log
            WHERE scan_error_id = %d
                AND object_type = 'N'
            ORDER BY id DESC
            LIMIT 1;",
            $scanErrorId,
            $objectType
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Handle a change in a ScanErrorDetail instance
     *
     * Creates a new ScanErrorLog based on the changes detected.
     *
     * @param ScanErrorDetail $scanErrorDetail
     *
     * @return boolean
     * @throws Exception
     */
    public function scanErrorDetailChange(ScanErrorDetail $scanErrorDetail)
    {
        $comment = '';
        $who = $this->getAddedBy();

        if ($scanErrorDetail->type_id !== $scanErrorDetail->oldRecord->type_id) {
            $comment = "Error type changed from " . $scanErrorDetail->oldRecord->type->name . " to " .
                $scanErrorDetail->type->name;
        }

        $scanErrorLog = new ScanErrorLog();
        $scanErrorLog->status_id = $scanErrorDetail->scanError->status_id;
        $scanErrorLog->type_id = $scanErrorDetail->oldRecord->type_id;
        $scanErrorLog->comment = $comment;
        $scanErrorLog->scan_error_id = $scanErrorDetail->scan_error_id;
        $scanErrorLog->scan_error_detail_id = $scanErrorDetail->id;
        $scanErrorLog->added_by_account_id = $who;

        return $scanErrorLog->save(false);
    }

    /**
     * Get the id of the current user to set the 'added_by' field
     *
     * @return integer|null
     */
    public function getAddedBy()
    {
        $who = null;
        if (Yii::app()->user) {
            $who = Yii::app()->user->getId();
        }

        return $who;
    }

    /**
     * Handle a change in the ScanErrorStatus Type
     *
     * Saves the corresponding changes in the log
     *
     * @return boolean
     */
    public function scanErrorStatusChange(ScanError $scanError)
    {
        $comment = '';
        $who = $this->getAddedBy();

        if ($scanError->status_id !== $scanError->oldRecord->status_id) {
            $comment = "Status changed from " . $scanError->oldRecord->status->name . " to " . $scanError->status->name;
        }

        $scanErrorLog = new ScanErrorLog();
        $scanErrorLog->status_id = $scanError->oldRecord->status_id;
        $scanErrorLog->comment = $comment;
        $scanErrorLog->scan_error_id = $scanError->id;
        $scanErrorLog->added_by_account_id = $who;

        return $scanErrorLog->save(false);
    }

    /**
     * Get the field name to which this log applies.
     * It can return 'Scan error level' if it applies to
     * the scan error instead of a field.
     *
     * @return string
     */
    public function relatedFieldName()
    {
        if ($this->hasDetail()) {
            return $this->scanErrorDetail->fieldType->name;
        } else {
            return 'Scan error level';
        }
    }

    /**
     * Determine if the log has a scan error detail.
     *
     * @return boolean
     */
    public function hasDetail()
    {
        if ($this->scan_error_detail_id && $this->scanErrorDetail) {
            return true;
        }

        return false;
    }

    /**
     * Get the name of the account that generated this log.
     * Can return N/A if none is available.
     *
     * @return string
     */
    public function getUserName()
    {
        if ($this->hasAccount()) {
            return $this->addedByAccount->getFullName();
        } else {
            return 'N/A';
        }
    }

    /**
     * Determine if the log has a related account.
     *
     * @return boolean
     */
    public function hasAccount()
    {
        if ($this->added_by_account_id && $this->addedByAccount) {
            return true;
        }

        return false;
    }
}
