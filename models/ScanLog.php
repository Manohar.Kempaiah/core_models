<?php

Yii::import('application.modules.core_models.models._base.BaseScanLog');

class ScanLog extends BaseScanLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Override the default method to prevent PDI sync
     * @return boolean
     */
    public function afterSave()
    {
        return true;
    }

    /**
     * Get the Year-Month value for the given record and provider
     * @return string
     */
    public function getYearMonthByProviderId($providerId)
    {
        $sql = sprintf(
            "SELECT `year_month`
                FROM scan
                WHERE provider_id = '%d' AND
                        DATE(date_added) = '%s'
                ORDER BY abs(timediff('%s', date_added)) DESC
                LIMIT 1;",
            $providerId,
            substr($this->date_added, 0, 10),
            $this->date_added
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get the provider section from a scan log data instance
     * @param int $providerId
     * @param array $data
     * @return array
     */
    public static function getProviderData($providerId, $data)
    {
        // look for the provider by id
        for ($i = 0; $i < count($data); $i++) {
            if (isset ($data[$i]['results'][0]['providerID']) &&
                $data[$i]['results'][0]['providerID'] == $providerId) {
                for ($j = 0; $j < count($data[$i]['results']); $j++) {
                    if (!empty($data[$i]['results'][$j]["phone"])) {
                        $data[$i]['results'][$j]["phone"] =
                        StringComponent::normalizePhoneNumber($data[$i]['results'][$j]["phone"], true);
                    }
                    // look for empty dates and replace them by valid ISO 8601 standard dates
                    if (
                        isset($data[$i]['results'][$j]["reviewSet"])
                        &&
                        count($data[$i]['results'][$j]["reviewSet"]) > 0
                    ) {
                        for ($k = 0; $k < count($data[$i]['results'][$j]["reviewSet"]); $k++) {
                            if (
                                $data[$i]['results'][$j]["reviewSet"][$k]['date'] == "0000-00-00 00:00:00"
                                ||
                                empty($data[$i]['results'][$j]["reviewSet"][$k]['date'])
                            ) {
                                $data[$i]['results'][$j]["reviewSet"][$k]['date'] = "0001-01-01 00:00:00";
                            }
                        }
                    }

                    // look for empty dates and replace them by valid ISO 8601 standard dates
                    for ($j = 0; $j < count($data[$i]['stats']['reviewSnippets']); $j++) {
                        if (
                            $data[$i]['stats']["reviewSnippets"][$j]['date'] == "0000-00-00 00:00:00"
                            ||
                            empty($data[$i]['stats']["reviewSet"][$j]['date'])
                        ) {
                            $data[$i]['stats']["reviewSnippets"][$j]['date'] = "0001-01-01 00:00:00";
                        }
                    }
                }
                return $data[$i];
            }
        }

        // if the provider is not found by id, look for it by name
        $provider = Provider::model()->cache(Yii::app()->params['cache_short'])->findByPk($providerId);
        for ($i = 0; $i < count($data); $i++) {
            if ($data[$i]['provider'] == $provider->first_last) {
                for ($j = 0; $j < count($data[$i]['results']); $j++) {
                    if (!empty($data[$i]['results'][$j]["phone"])) {
                        $data[$i]['results'][$j]["phone"] =
                        StringComponent::normalizePhoneNumber($data[$i]['results'][$j]["phone"], true);
                    }
                    // look for empty dates and replace them by valid ISO 8601 standard dates
                    if (
                        isset($data[$i]['results'][$j]["reviewSet"])
                        &&
                        count($data[$i]['results'][$j]["reviewSet"]) > 0
                    ) {
                        for ($k = 0; $k < count($data[$i]['results'][$j]["reviewSet"]); $k++) {
                            if (
                                $data[$i]['results'][$j]["reviewSet"][$k]['date'] == "0000-00-00 00:00:00"
                                ||
                                empty($data[$i]['results'][$j]["reviewSet"][$k]['date'])
                            ) {
                                $data[$i]['results'][$j]["reviewSet"][$k]['date'] = "0001-01-01 00:00:00";
                            }
                        }
                    }
                }

                // look for empty dates and replace them by valid ISO 8601 standard dates
                for ($j = 0; $j < count($data[$i]['stats']['reviewSnippets']); $j++) {
                    if (
                        $data[$i]['stats']["reviewSnippets"][$j]['date'] == "0000-00-00 00:00:00"
                        ||
                        empty($data[$i]['stats']["reviewSet"][$j]['date'])
                    ) {
                        $data[$i]['stats']["reviewSnippets"][$j]['date'] = "0001-01-01 00:00:00";
                    }
                }
                return $data[$i];
            }
        }

        return array();
    }

    /**
     * Get scan logs related to a practice
     *
     * @param string $date
     * @param string $practiceName
     * @return array
     */
    public function getScanHistoryLogs($date, $practiceName)
    {
        $sql = "SELECT
              scan_log.id AS scan_log_id, scan_log.data
            FROM
              scan_log
            WHERE
              scan_log.date_added BETWEEN DATE_SUB(:date1, INTERVAL 1 SECOND)
              AND DATE_ADD(:date2, INTERVAL 1 SECOND)
              AND scan_log.data LIKE :practice_name ";

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValue(':date1', $date, PDO::PARAM_STR)
            ->bindValue(':date2', $date, PDO::PARAM_STR)
            ->bindValue(':practice_name', "%" . $practiceName . "%", PDO::PARAM_STR)
            ->queryAll();
    }

    /**
     * Get full practice history
     *
     * @param integer $practiceId
     * @return array
     */
    public function getHistory($practiceId)
    {
        $scanPractice = ScanPractice::model()->getHistoryForScan($practiceId);

        $return = [];
        if ($scanPractice) {
            $scanLogs = ScanLog::model()->getScanHistoryLogs(
                $scanPractice['date_added'],
                $scanPractice['practice_name']
            );

            if ($scanLogs) {
                foreach ($scanLogs as $scanLog) {
                    $pastScan = $scanLog->data;
                    $providers = $pastScan->scan->providers;
                    $practiceResults = $pastScan->scan->practice->results;

                    if ($practiceResults) {
                        foreach ($practiceResults as $practiceResult) {
                            $return[$practiceResult->code] = $practiceResult;
                        }
                    }

                    if ($providers) {
                        foreach ($providers as $provider) {
                            $providerId = $provider->results[0]->providerID;

                            $providerResultSet = [];

                            if ($provider->results) {

                                foreach ($provider->results as $pr) {
                                    $providerResultSet[$pr->code] = $pr;
                                }
                            }

                            $return[$providerId] = $providerResultSet;
                        }
                    }
                }
            }
        }

        return $return;
    }

    /**
     * Get a scan result object
     *
     * @return ScanResultData
     */
    public function getScanDataObject()
    {
        return new ScanResultData($this->data);
    }
}
