<?php

Yii::import('application.modules.core_models.models._base.BaseCredential');

class Credential extends BaseCredential
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from credential if associated records exist in provider table
        $has_associated_records = Provider::model()->exists(
            'credential_id=:credential_id',
            array(':credential_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError('id', 'Credential cannot be deleted because there are providers using it.');
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Call fn_credential_clean_up before make any validations
     * @return void
     */
    public function beforeValidate()
    {
        // clean the title before make any validations
        $sql = 'SELECT fn_credential_clean_up(:text)';

        try {
            $this->title = Yii::app()->db->createCommand($sql)->queryScalar([':text' => $this->title]);
        } catch (Exception $ex) {
            // do nothing, not trow any issue if the function called trhow an error
        }

        return parent::beforeValidate();
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = array('title', 'unique');

        return $rules;
    }

}
