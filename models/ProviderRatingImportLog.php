<?php

Yii::import('application.modules.core_models.models._base.BaseProviderRatingImportLog');

class ProviderRatingImportLog extends BaseProviderRatingImportLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Save the sourceData into the log table.
     *
     * @param array $sourceData
     * @param string $internalNotes
     * @param int $id
     */
    public static function saveLog($sourceData, $internalNotes, $id = null)
    {
        $log = new ProviderRatingImportLog();
        $log->provider_rating_id = $id;
        $log->partner_site_id = $sourceData['partner_site_id'];
        $log->provider_id = empty($sourceData['provider_id']) ? null : $sourceData['provider_id'];
        $log->practice_id = empty($sourceData['practice_id']) ? null : $sourceData['practice_id'];
        $log->origin = $sourceData['origin'];
        $log->partner_site_review_id = $sourceData['review_id'];
        $log->source_data = json_encode($sourceData);
        $log->internal_notes = $internalNotes;
        $log->save();
    }
}
