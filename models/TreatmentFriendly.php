<?php

Yii::import('application.modules.core_models.models._base.BaseTreatmentFriendly');

class TreatmentFriendly extends BaseTreatmentFriendly
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Autocomplete for treatments based on name and code
     * @param string $q
     * @param int $limit
     * @return array
     */
    public static function autocomplete($q, $limit, $subSpecialty = '')
    {

        $q = trim(addslashes($q));

        if ($q === '') {
            $limit = 999;
        }

        $sql = "
            SELECT tf.id, tf.name, tf.code, tf.provider_level_search
            FROM
            (
                SELECT DISTINCT CONCAT('T-', treatment.id) AS id, IF(treatment_friendly.name != '', treatment_friendly.name, treatment.name)  AS name, treatment.code AS code, treatment.provider_level_search as provider_level_search
                FROM treatment
                INNER JOIN treatment_friendly_treatment
                ON treatment_friendly_treatment.treatment_id = treatment.id
                INNER JOIN treatment_friendly
                ON treatment_friendly_treatment.treatment_friendly_id = treatment_friendly.id
                " .
                ($subSpecialty != "" ? "INNER JOIN specialty_treatment ON treatment.id = specialty_treatment.treatment_id
                 INNER JOIN specialty ON specialty_treatment.sub_specialty_id = specialty.sub_specialty_id AND specialty.sub_specialty_name = '" . $subSpecialty . "'" : "")
                . "
                WHERE treatment.name LIKE '%" . $q . "%' OR treatment.code LIKE '%" . $q . "%' OR treatment_friendly.name LIKE '%" . $q . "%' OR treatment_friendly.alt_name_array LIKE '%" . $q . "%'

                UNION

                SELECT CONCAT('QP-', quality_area.id) as id, quality_area.quality_path_description, '' AS code, quality_area.provider_level_search as provider_level_search
                FROM quality_area
                WHERE quality_path_description IS NOT NULL AND
                quality_area.quality_path_description LIKE '%" . $q . "%'
            ) as tf

            ORDER BY tf.name LIKE '%" . $q . "%' DESC, tf.code LIKE '%" . $q . "%' DESC, name ASC
            LIMIT " . $limit . "

                ;";

        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }

}
