<?php

Yii::import('application.modules.core_models.models._base.BaseProviderRatingWidget');

class ProviderRatingWidget extends BaseProviderRatingWidget
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Return the widgets in which a review is promoted for an account
     * @param int $reviewId
     * @param int $accountId
     * @return array
     */
    public static function getDisplayReviewWidgets($reviewId, $accountId)
    {

        $sql = sprintf(
            "SELECT DISTINCT w.*,
             (
                SELECT count(*)
                FROM provider_rating_widget prw
                WHERE
                prw.provider_rating_id = %d AND
                prw.account_id = %d AND
                prw.widget_id = w.id
             ) AS promoted
             FROM widget w
             INNER JOIN provider_practice_widget ppw
             ON w.id = ppw.widget_id
             INNER JOIN provider_rating pr
             ON pr.provider_id = ppw.provider_id AND
             (pr.practice_id = ppw.practice_id OR pr.practice_id IS NULL)
             WHERE w.account_id = %d AND
             (w.widget_type = 'DISPLAY_REVIEWS' OR w.widget_type = 'MOBILE_ENHANCE') AND
             hidden = 0 AND draft = 0;
            ",
            $reviewId,
            $accountId,
            $accountId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Clear the cache for the associated widget.
     * @return bool
     */
    public function afterSave()
    {
        Widget::clear($this->widget->key_code);

        return parent::afterSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        Widget::clear($this->widget->key_code);

        return parent::beforeDelete();
    }

}
