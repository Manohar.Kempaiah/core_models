<?php

Yii::import('application.modules.core_models.models._base.BaseWaitingRoomSetting');

class WaitingRoomSetting extends BaseWaitingRoomSetting
{
    public $uploadPath;
    public $appointmentRequest;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Initializes the model with the correct basePath according to the active module.
     * @internal Avoid using __construct. Use init() instead.
     *  if you still need to use it, call parent::__construct($scenario). Read yii documentation.
     */
    public function init()
    {
        parent::init();
        $basePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'providers';
        $this->uploadPath = $basePath . Yii::app()->params['upload_offices'];
    }

    /**
     * Save event to audit log
     *
     * @return void
     */
    public function beforeSave()
    {

        if ($this->isNewRecord) {
            AuditLog::create(
                'C',
                'VV WR created',
                'waiting_room_setting',
                'Provider #' . $this->provider_id,
                false,
                $this->account_id,
                true
            );
        } else {
            AuditLog::create(
                'U',
                'VV WR updated',
                'waiting_room_setting',
                'Provider #' . $this->provider_id,
                false,
                $this->account_id,
                true
            );
        }

        return parent::beforeSave();
    }

    /**
     * Automatically enable telemedicine after creating a Waiting Room
     */
    public function afterSave()
    {

        // if we're updating leave
        if (!($this->isNewRecord)) {
            return false;
        }

        // automatically enable the "Telemedicine" option for this provider since they now have VV (#38927)
        // but only if they haven't had it on before
        $pd = ProviderDetails::model()->find('provider_id=:providerId', [':providerId' => $this->provider_id]);

        if (!empty($pd) && !empty($pd->telemedicine_disabled_by_account_id)) {
            // this means they already had it turned on but manually turned it off, so we don't want to
            // automatically enable it again
            return parent::afterSave();
        }

        // check if appointment request is set
        $params = array("accept_new_patients" => 0);
        if (isset($this->appointmentRequest)) {
            $params['appointment_request'] = $this->appointmentRequest;
        }
        // enable telemedicine automatically
        $telemedicineCreated = ProviderPractice::getOrCreateTelemedicinePractice(
            $this->provider_id,
            $this->account_id,
            $params
        );
        // create the default hours
        if (!empty($telemedicineCreated)) {
            $providerPracticeId = $telemedicineCreated['id'];

            // get the schedule of this provider
            $pps = ProviderPracticeSchedule::getPrimaryPracticeSchedule(
                $this->provider_id,
                $this->account_id,
            );
            if ($pps['success']) {
                $primaryPracticeSchedule = isset($pps['data']) ? $pps['data'] : 0 ;
                if (!empty($primaryPracticeSchedule)) {
                    ProviderPracticeSchedule::saveTelemedicineOfficeHours(
                        $providerPracticeId,
                        $primaryPracticeSchedule
                    );
                }
            }

        }
        return parent::afterSave();
    }

    /**
     * Get a friendly URL for a waiting room
     *
     * @return string $result
     */
    public function getFriendlyUrl()
    {
        $url = '';

        $provider = Provider::model()->cache(Yii::app()->params['cache_medium'])->findByPk($this->provider_id);
        if ($provider) {
            $url = $provider->friendly_url;
        }

        return StringComponent::getNewFriendlyUrl($url, $this->id, 'waitingRoomSetting');
    }

    /**
     * Return a ReviewRequest available
     *
     * @return string $result
     */
    public static function getReviewRequestAvailable($url = '')
    {
        // Get the waiting room settings and
        // find waiting room
        $waitingRoomSetting = WaitingRoomSetting::model()
            ->cache(Yii::app()->params['cache_medium'])
            ->find("friendly_url = :friendly_url", array(":friendly_url" => $url));

        // quit in case the waiting room couldn't be found
        if (!$waitingRoomSetting) {
            return array(
                'success' => false,
                'error' => true,
                'data' => 'Invalid waiting room'
            );
        }

        $wrsId = $waitingRoomSetting->id;
        $firstDay = TimeComponent::getFirstDay();

        $sql = sprintf(
            "SELECT
                IF (wri.review_request_id IS NOT NULL, wri.review_request_id, wrg.review_request_id)
                    AS review_request_id_number
            FROM waiting_room_invite AS wri
            LEFT JOIN waiting_room_guest AS wrg
                ON wrg.waiting_room_invite_id = wri.id AND wrg.`type` = 'P'
            where waiting_room_setting_id = %d
                AND wri.date_added > '%s'
            GROUP BY wri.id;",
            $wrsId,
            $firstDay
        );

        $resultQuery = Yii::app()->db->createCommand($sql)->queryAll();

        $rrSent = 0;
        if (!empty($resultQuery)) {
            foreach ($resultQuery as $v) {
                if (!empty($v['review_request_id_number'])) {
                    $rrSent++;
                }
            }
        }

        $remain = 5 - $rrSent;
        if ($remain < 0) {
            $remain = 0;
        }
        return $remain;
    }

    /**
     * Return the waiting room list for the logged account
     *
     * @param array $accountInfo
     * @param array $params
     *  $params['waiting_room_friendly_url']
     *  $params['add_listings']
     *  $params['guid']
     *  $params['cache']
     * @return array $result
     */
    public static function getList($accountId, $ownerAccountId, $params = array())
    {
        $useCache = Common::get($params, 'cache', true);
        $guid = Common::get($params, 'guid', '');
        $waitingRoomFriendlyUrl = Common::get($params, 'waiting_room_friendly_url', '');
        $queueId = "waiting_room_get_list_" . $accountId . '_' . $waitingRoomFriendlyUrl;

        $order = Common::get($params, 'order', 'name ASC');

        // Validate order is letters and/or spaces
        if (!preg_match("/^[a-zA-Z\s]+$/", $order)) {
            $order = 'name ASC';
        }

        if (Common::isTrue($useCache)) {
            // get queue from cache
            $jsonQueue = Yii::app()->cache->get($queueId);
            $arrQueue = json_decode($jsonQueue, true);
            if (!empty($arrQueue)) {
                return $arrQueue;
            }
        }

        $isFreemium = Common::isTrue(Account::isFreemiumAccount($accountId));

        $addListings = Common::get($params, 'add_listings', 1);

        // Search for waiting rooms
        $waitingRoomCondition = '';
        if (!empty($waitingRoomFriendlyUrl)) {
            $waitingRoomCondition = " AND waiting_room_setting.friendly_url = :waiting_room_friendly_url ";
        }

        $waitingRooms = array();
        $waitingRoomCount = 0;

        $lstProviders = AccountProvider::getList($ownerAccountId);
        if (!empty($lstProviders)) {

            $sql = sprintf(
                "SELECT
                    waiting_room_setting.id, waiting_room_setting.enabled, provider_id, name,
                    CONCAT('https://%s/' , waiting_room_setting.friendly_url) AS url,
                    waiting_room_setting.friendly_url as wrs_friendly_url, logo_path, welcome_message,
                    rate_review_title, sms_template, email_template, waiting_room_setting.send_review_request,
                    custom_url, provider_practice_listing_id, account_device_id,
                    CONCAT(provider.prefix, ' ' ,provider.first_m_last) as provider_name,
                    waiting_room_invite.patient_email, waiting_room_invite.patient_phone_number,
                    patient_point_enabled
                FROM
                    waiting_room_setting
                LEFT JOIN provider ON waiting_room_setting.provider_id = provider.id
                LEFT JOIN waiting_room_invite ON waiting_room_invite.waiting_room_setting_id = waiting_room_setting.id
                    AND waiting_room_invite.guid = :guid
                WHERE
                    account_id = :owner_account_id %s AND provider_id IN (%s)
                ORDER BY %s",
                Yii::app()->params->servers['dr_off_hn'],
                $waitingRoomCondition,
                $lstProviders,
                $order
            );

            $command = Yii::app()->db->createCommand($sql);
            $command->bindValue(':guid', $guid);
            $command->bindValue(':owner_account_id', $ownerAccountId);
            if ($waitingRoomCondition) {
                $command->bindValue(':waiting_room_friendly_url', $waitingRoomFriendlyUrl);
            }
            $waitingRooms = $command->queryAll();

            // How many WaitingRooms are created under the owner?
            $accountList = OrganizationAccount::getAllAccountId($ownerAccountId);
            $accountList = implode(',', $accountList);
            if (!empty($accountList)) {
                $sql = sprintf(
                    "SELECT count(id) as total
                    FROM waiting_room_setting
                    WHERE
                        account_id IN (%s)",
                    $accountList
                );
                $waitingRoomCount = Yii::app()->db
                    ->cache(Yii::app()->params['cache_short'])
                    ->createCommand($sql)->queryScalar();
            }
        }

        $providerId = 0;
        if (!empty($waitingRooms)) {
            foreach ($waitingRooms as $key => $value) {

                if (!empty($waitingRoomFriendlyUrl)) {
                    // only 1 waiting room, get the provider_id
                    $providerId = $value['provider_id'];
                }

                $waitingRooms[$key]['logo_path'] = AssetsComponent::generateImgSrc(
                    $value['logo_path'],
                    'offices',
                    'XL',
                    true
                );
                $waitingRooms[$key]['friendly_url'] = $waitingRooms[$key]['wrs_friendly_url'];

                if (
                    !empty($value['custom_url'])
                    || !empty($value['provider_practice_listing_id'])
                    || !empty($value['account_device_id'])
                ) {
                    $waitingRooms[$key]['review_request_allowed'] = 1;
                } else {
                    $waitingRooms[$key]['review_request_allowed'] = 0;
                }

                // if no patient email and phone number is provided, turn down review_request
                if (!empty($guid) && (empty($value['patient_phone_number']) && empty($value['patient_email']))) {
                    $waitingRooms[$key]['review_request_allowed'] = 0;
                }

                // make sure we have review requests left
                if (
                    $waitingRooms[$key]['review_request_allowed'] == 1
                    &&
                    Account::isFreemiumAccount($ownerAccountId)
                ) {
                    if (WaitingRoomSetting::getReviewRequestAvailable($waitingRooms[$key]['friendly_url'])) {
                        $waitingRooms[$key]['review_request_allowed'] = 1;
                    } else {
                        $waitingRooms[$key]['review_request_allowed'] = 0;
                    }
                }

                // by default, patientpoint checkbox is not visible and can't be toggled
                $waitingRooms[$key]['patient_point_elegible'] = 0;
                $waitingRooms[$key]['patient_point_enabled'] = 0;
                // if we have the provider and it's a paying client, check if the provider's specialty is supported
                if (!empty($providerId) && !Account::isFreemiumAccount($ownerAccountId)) {
                    $supportedSpecialy = ProviderSpecialty::hasPatientPointSupportedSpecialty($providerId);
                    if (!empty($supportedSpecialy)) {
                        // the provider's specialty is supported: make the checkbox visible
                        $waitingRooms[$key]['patient_point_elegible'] = 1;
                        // use the checked status from the DB
                        $waitingRooms[$key]['patient_point_enabled'] = $value['patient_point_enabled'];
                    }
                }

                unset($waitingRooms[$key]['wrs_friendly_url']);
            }
        }

        $providerPracticeListing = array();
        if (Common::isTrue($addListings) && !$isFreemium) {

            $arrPractices = array();
            // Get the RR settings for the associated practices

            if ($providerId > 0) {
                // limit to the practices that provider is working on
                $practices = ProviderPractice::getProvidersPractices($providerId, $accountId, 'workingOn');

                $arrPractices = array();
                foreach ($practices as $p) {
                    $arrPractices[$p['practice_id']] = array(
                        'name' => $p['name'],
                        'address_full' => trim($p['address'] . ' ' . $p['address_2']),
                        'city_state' => trim($p['city_state']),
                    );
                }
            } else {
                $practiceParams = array(
                    'account_id' => $accountId,
                    'use_cache' => 1
                );
                $arrPractices = AccountPractice::getPractices($practiceParams);
            }

            // Cycle through the practices
            foreach ($arrPractices as $practiceId => $practice) {

                $messagingSettings = PracticeDetails::getMessagingSetting(
                    $practiceId,
                    $ownerAccountId,
                    $providerId
                );

                if (empty($messagingSettings)) {
                    continue;
                }

                // DDC links
                $reviewLinks['ddc'] = '';
                $reviewLinks['ddc_id'] = '';
                foreach ($messagingSettings['practice_devices'] as $device) {
                    if ($device['device_model_id'] == DeviceModel::REVIEW_REQUEST_ID) {
                        // It is a review_request
                        $reviewLinks['ddc'] = $device['ddc_link'];
                        $reviewLinks['ddc_id'] = $device['id'];
                        break;
                    }
                }
                if (!empty($reviewLinks['ddc'])) {
                    $providerPracticeListing[] = array(
                        'practice_id' => $practiceId,
                        'type' => 'ddc',
                        'name' => $practice['name'],
                        'practice_address' => $practice['address_full'],
                        'city_state' => $practice['city_state'],
                        'provider_practice_listing_id' => 0,
                        'provider_id' => null,
                        'provider_name' => null,
                        'account_device_id' => $reviewLinks['ddc_id'],
                    );
                }

                $reviewLinks = $messagingSettings['review_links'];

                // Yelp links
                if (!empty($reviewLinks['yelp'])) {
                    $providerPracticeListing[] = array(
                        'practice_id' => $practiceId,
                        'type' => 'yelp',
                        'name' => $practice['name'],
                        'practice_address' => $practice['address_full'],
                        'city_state' => $practice['city_state'],
                        'provider_practice_listing_id' => $reviewLinks['yelp_id'],
                        'provider_id' => null,
                        'provider_name' => null,
                        'account_device_id' => 0,
                    );
                }

                // Google links
                if (!empty($reviewLinks['google+'])) {
                    $providerPracticeListing[] = array(
                        'practice_id' => $practiceId,
                        'type' => 'google',
                        'name' => $practice['name'],
                        'practice_address' => $practice['address_full'],
                        'city_state' => $practice['city_state'],
                        'provider_practice_listing_id' => $reviewLinks['google+_id'],
                        'provider_id' => null,
                        'provider_name' => null,
                        'account_device_id' => 0,
                    );
                }

                // Google links p@p
                if (
                    !empty($reviewLinks['google_pp'])
                    && !empty($reviewLinks['provider_id'])
                    && $reviewLinks['provider_id'] == $providerId
                ) {
                    // Is p@p but not for this provider

                    $providerPracticeListing[] = array(
                        'practice_id' => $practiceId,
                        'type' => 'google',
                        'name' => $practice['name'] . ' [' . $reviewLinks['provider_name'] . ']',
                        'practice_address' => $practice['address_full'],
                        'city_state' => $practice['city_state'],
                        'provider_practice_listing_id' => $reviewLinks['google_id_pp'],
                        'provider_id' => $reviewLinks['provider_id'],
                        'provider_name' => $reviewLinks['provider_name'],
                        'account_device_id' => 0,
                    );
                }
            }
        }

        $arrQueue = [
            'waiting_rooms' => $waitingRooms,
            'listings' => $providerPracticeListing,
            'waiting_room_count' => $waitingRoomCount
        ];
        // update the entry in the cache
        $jsonQueue = json_encode($arrQueue);
        Yii::app()->cache->set($queueId, $jsonQueue, Yii::app()->params['cache_short']);

        return $arrQueue;
    }

    /**
     * Saves a Waiting room
     *
     * @param array $data
     *  $data = array(
     *     "id" => 2259,
     *     "name" =>"Dr  Mariano Gonzalez",
     *     "url" => "https://office-b.doctor.com/Dr-Mariano-Gonzalez-1",
     *     "welcome_message" => "Welcome",
     *     "logo_path" => null,
     *     "sms_template" => "Please join your call with Dr. Mariano Gonzalez at:",
     *     "email_template" => "Please click the button below to begin your telemedicine call with Dr. Mariano
     * Gonzalez.",
     *     "enabled" => 1,
     *     "provider_id" => 4031402,
     *     'friendly_url' => Common::getUrlPath($data['url']);
     *     'owner_account_id' => $this->user()->getOwnerAccountId(),
     *     'account_id' => $this->user()->getAccountId(),
     * );
     * @return mixed $result
     */
    public static function saveData($data)
    {

        $providerId = Common::get($data, 'provider_id');

        $customUrl = Common::get($data, 'custom_url', null);
        if (!empty($customUrl)) {

            // Do we get a json or url ?
            $temp = json_decode($customUrl);
            if (json_last_error() === 0) {
                // JSON is valid, so is a Google place in a custom_url
            } else {
                // should be a valid url
                $customUrl = StringComponent::validateUrl($customUrl, true, true);
                // This url exists ??
                $exists = @get_headers($customUrl);
                if (empty($exists)) {
                    return "Invalid custom_url format";
                }
            }
        }

        $waitingRoomId = Common::get($data, 'id', null);
        if (empty($waitingRoomId)) {
            // Trying to create a new waitngRoom
            // Does this provider have a waiting room created??
            $wrs = WaitingRoomSetting::model()->find(
                "provider_id =:provider_id",
                array(":provider_id" => $providerId)
            );
            if (!empty($wrs->id)) {
                // yes, he have one waitingRoom
                return "This provider already has a waiting room created.";
            }
        }

        // id is empty, it's a create action
        if (empty($data['friendly_url'])) {
            // Create the object and set the added fields
            $waitingRoom = new WaitingRoomSetting();
            $waitingRoom->account_id = Common::get($data, 'owner_account_id');
            $waitingRoom->added_by_account_id = Common::get($data, 'account_id');
            $waitingRoom->provider_id = $providerId;
            $waitingRoom->friendly_url = $waitingRoom->getFriendlyUrl();
            $waitingRoom->date_added = date('Y-m-d H:i:s');
        } else {
            // Get the existing object and set the updated fields
            $waitingRoom = WaitingRoomSetting::model()->find(
                "friendly_url=:friendly_url",
                array(':friendly_url' => Common::get($data, 'friendly_url'))
            );
            if (!$waitingRoom) {
                return "The object does not exist.";
            }
        }

        // Set the fields
        $waitingRoom->name = Common::get($data, 'name', '');
        $waitingRoom->updated_by_account_id = Common::get($data, 'account_id');
        $waitingRoom->date_updated = date('Y-m-d H:i:s');

        if (!empty($customUrl)) {
            // Check if this a Google url in a custom field
            // doctor.com?json
            $exist = strpos($customUrl, 'doctor.com?json=');
            if ($exist !== false) {
                $customUrl = ltrim($customUrl, 'http://doctor.com?json=');
                $customUrl = ltrim($customUrl, 'https://doctor.com?json=');
            }
            $customUrl = urldecode($customUrl);
        }
        $waitingRoom->custom_url = $customUrl;

        // This fields should be updated only in case that we send it in the array
        if (isset($data['email_template'])) {
            $waitingRoom->email_template = $data['email_template'];
        }
        if (isset($data['sms_template'])) {
            $waitingRoom->sms_template = $data['sms_template'];
        }
        if (isset($data['rate_review_title'])) {
            $waitingRoom->rate_review_title = $data['rate_review_title'];
        }
        if (isset($data['welcome_message'])) {
            $waitingRoom->welcome_message = $data['welcome_message'];
        }
        if (isset($data['send_review_request'])) {
            $waitingRoom->send_review_request = $data['send_review_request'];
        }
        if (isset($data['enabled'])) {
            $waitingRoom->enabled = $data['enabled'];
        }
        if (isset($data['provider_practice_listing_id'])) {
            $waitingRoom->provider_practice_listing_id = $data['provider_practice_listing_id'];
        } elseif (!empty($waitingRoom->custom_url)) {
            $waitingRoom->provider_practice_listing_id = null;
        }
        if (isset($data['account_device_id'])) {
            $waitingRoom->account_device_id = $data['account_device_id'];
        } elseif (!empty($waitingRoom->custom_url)) {
            $waitingRoom->account_device_id = null;
        }
        if (isset($data['patient_point_enabled'])) {
            $waitingRoom->patient_point_enabled = $data['patient_point_enabled'];
        }

        if (!empty($waitingRoom->provider_practice_listing_id)) {
            // if we choose for a yelp/google
            $waitingRoom->account_device_id = null;
            $waitingRoom->custom_url = null;
        } elseif (!empty($waitingRoom->account_device_id)) {
            // we choose for feedback
            $waitingRoom->provider_practice_listing_id = null;
            $waitingRoom->custom_url = null;
        } elseif (!empty($waitingRoom->custom_url)) {
            // custom url
            $waitingRoom->provider_practice_listing_id = null;
            $waitingRoom->account_device_id = null;
        } else {
            $waitingRoom->send_review_request = 0;
        }

        // Try to save
        try {
            if (!$waitingRoom->save()) {
                return json_encode($waitingRoom->getErrors());
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }

        // Upload the file and update the logo_path
        if (isset($_FILES['WaitingRoomSetting']['name']['logo_path'])) {
            $target = 'logo_path';
            $fileUploaded = File::upload($waitingRoom, $target);
            $waitingRoom->logo_path = $fileUploaded['file_name'];
            $waitingRoom->save();
        }

        return (array) $waitingRoom->attributes;
    }

    /**
     * Return the waiting room list for the logged account
     * @param int $accountId
     * @return array $result
     */
    public static function getLog(
        $accountId,
        $ownerAccountId,
        $friendlyUrl = '',
        $limit = 25,
        $offset = 0,
        $filters = array(),
        $sort = null,
        $order = 'desc'
    ) {
        $additionalConditions = '';
        if (!empty($filters['patient_nickname'])) {
            $additionalConditions = 'AND i.patient_nickname LIKE '
                . Yii::app()->db->quoteValue('%' . $filters['patient_nickname'] . '%');
        }

        $limitQuery = '';
        // If limit is not null, use it
        if ($limit) {
            $limitQuery = 'LIMIT ' . $limit . ' OFFSET ' . $offset;
        }

        switch ($sort) {
            case 'provider':
                $orderQuery = 'ORDER BY p.first_last';
                break;
            case 'patient':
                $orderQuery = 'ORDER BY i.patient_nickname';
                break;
            case 'email':
                if ($order != 'desc') {
                    $orderQuery = 'ORDER BY i.patient_email ASC,i.patient_phone_number';
                } else {
                    $orderQuery = 'ORDER BY i.patient_email DESC,i.patient_phone_number';
                }
                break;
            case 'invite_date':
                $orderQuery = 'ORDER BY i.date_added';
                break;
            case 'duration':
                $orderQuery = 'ORDER BY MAX(q.room_duration)';
                break;
            case 'start_time':
            default:
                $orderQuery = 'ORDER BY room_created';
                break;
        }

        if ($order != 'desc') {
            $orderQuery .= ' ASC';
        } else {
            $orderQuery .= ' DESC';
        }

        /**
         * provider name
         * patient name
         * patient email,phone
         * invite date
         * start time
         * duration
         */

        $sql = sprintf(
            "SELECT
                s.enabled, s.name,
                p.first_last AS provider,
                i.patient_nickname, i.patient_email,
                i.patient_phone_number, i.date_added AS invite_date,
                i.is_static,
                MAX(q.room_created) as room_created,
                MAX(q.room_ended) as room_ended,
                MIN(q.provider_connected) AS provider_connected,
                IF (
                    MAX(q.provider_disconnected) > MIN(q.provider_connected),
                    MAX(q.provider_disconnected) ,
                    MAX(q.room_ended)
                ) AS provider_disconnected,
                MIN(q.patient_connected) AS patient_connected,
                MAX(q.patient_disconnected) AS patient_disconnected,
                TIME_TO_SEC(
                    TIMEDIFF(
                        IF (
                            MAX(q.provider_disconnected) > MIN(q.provider_connected),
                            MAX(q.provider_disconnected),
                            MAX(q.room_ended)
                        ), provider_connected
                    )
                ) room_duration,
                SUM(q.room_duration) as twilio_room_duration,

                MAX(q.room_duration) as room_duration,
                rr.id, rr.account_id, rr.practice_id, rr.provider_id, rr.patient_id,
                rr.google_link, rr.yelp_link, rr.ddc_link, rr.custom_link,
                rr.google_clicked, rr.yelp_clicked, rr.ddc_clicked, rr.custom_clicked,
                rr.hidden, rr.status, rr.send_via, rr.source,
                DATE_FORMAT(rr.date_added, '%%m/%%d/%%y') AS date_added_f,
                DATE_FORMAT(rr.date_added, '%%h:%%i %%p') AS time_added_f,
                rr.send_link, pr.name AS practice_name, p.first_last AS provider_name,
                pa.first_name, pa.last_name, pa.mobile_phone, pa.email ,
                cd.communication_id, cd.communication_table, cl.body AS body_log, cq.body AS body_queue,
                IF(rr.google_clicked =1 AND yelp_clicked = 1, 'Both',
                    IF(rr.google_clicked =1 , 'Google',
                        IF(rr.yelp_clicked =1 , 'Yelp',
                            IF(rr.ddc_clicked =1 , 'Doctor', 'n/a')
                        )
                    )
                ) AS clicked,
                i.guid, i.id AS waiting_room_invite_id,
                wrch.messages_count AS messages_count,
                count(wrg.id) as participants
            FROM waiting_room_setting s
                INNER JOIN provider p ON s.provider_id = p.id
                INNER JOIN waiting_room_invite i ON i.waiting_room_setting_id =  s.id
                INNER JOIN waiting_room_queue q ON q.waiting_room_invite_id = i.id
                LEFT JOIN review_request rr ON i.review_request_id = rr.id
                LEFT JOIN practice pr ON rr.practice_id = pr.id
                LEFT JOIN patient AS pa ON rr.patient_id = pa.id
                LEFT JOIN communication_description AS cd ON reference_id = rr.id
                    AND reference_table = 'review_request'
                LEFT JOIN communication_log AS cl ON cl.id = cd.communication_id
                LEFT JOIN communication_queue AS cq ON cq.id = cd.communication_id
                LEFT JOIN waiting_room_chat_history AS wrch
                    ON i.id = wrch.waiting_room_invite_id AND wrch.chat IS NOT NULL
                LEFT JOIN waiting_room_guest AS wrg ON i.id = wrg.waiting_room_invite_id
            WHERE
                s.account_id = %d
                AND ('' = '%s' OR s.friendly_url = '%s')
                AND provider_connected IS NOT NULL AND (patient_connected IS NOT NULL OR room_sid IS NOT NULL)
                AND s.provider_id IN
                    (SELECT provider_id FROM account_provider WHERE account_id = %d)
                %s
            GROUP BY i.id
            %s
            %s",
            $ownerAccountId,
            $friendlyUrl,
            $friendlyUrl,
            $accountId,
            $additionalConditions,
            $orderQuery,
            $limitQuery
        );

        $results = Yii::app()->db
            ->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->queryAll();

        for ($i = 0; $i < count($results); $i++) {
            // get the last call elapsed time if they were both patient and
            // provider connected in the same room, then disconnected and rejoined
            $totalElapsed = (int) Yii::app()->cache->get(
                "telemedicine_wriid_" . $results[$i]['waiting_room_invite_id'] . '_total_elapsed_time'
            );

            // add the attribute to the result item
            $results[$i]['sum_calls_time'] = $totalElapsed;
        }

        return $results;
    }

    /**
     * Get total records for the logged account
     * @param $accountId
     * @param $ownerAccountId
     * @param string $friendly_url
     * @return mixed
     */
    public static function getLogCount($accountId, $ownerAccountId, $friendlyUrl = '', $filters = array())
    {
        $additionalConditions = '';
        if (!empty($filters['patient_nickname'])) {
            $additionalConditions = 'AND i.patient_nickname LIKE '
                . Yii::app()->db->quoteValue('%' . $filters['patient_nickname'] . '%');
        }

        $sql = sprintf(
            "SELECT
                COUNT(DISTINCT i.id)
            FROM waiting_room_invite i
                INNER JOIN waiting_room_setting s ON i.waiting_room_setting_id = s.id
                INNER JOIN provider p ON s.provider_id = p.id
                INNER JOIN waiting_room_queue q ON q.waiting_room_invite_id = i.id
            WHERE
                s.account_id = %d
                AND ('' = '%s' OR s.friendly_url = '%s')
                AND q.provider_connected IS NOT NULL AND q.patient_connected IS NOT NULL
                AND s.provider_id IN
                    (SELECT provider_id FROM account_provider WHERE account_id = %d)
                %s;",
            $ownerAccountId,
            $friendlyUrl,
            $friendlyUrl,
            $accountId,
            $additionalConditions
        );

        return Yii::app()->db
            ->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->queryScalar();
    }

    /**
     * Return the upcoming list for the logged account
     * @param int $accountId
     * @return array $result
     */
    public static function getUpcoming(
        $accountId,
        $ownerAccountId,
        $friendlyUrl = '',
        $limit = 25,
        $offset = 0,
        $filters = array()
    ) {
        $additionalConditions = '';
        if (!empty($filters['patient_nickname'])) {
            $additionalConditions = 'AND i.patient_nickname LIKE '
                . Yii::app()->db->quoteValue('%' . $filters['patient_nickname'] . '%');
        }

        $sql = sprintf(
            "SELECT
                s.enabled,
                s.name,
                p.first_last AS provider,
                i.patient_nickname,
                i.patient_email,
                i.patient_phone_number,
                i.date_added AS invite_date,
                i.guid,
                i.id AS waiting_room_invite_id,
                (
                    SELECT COUNT(*) FROM waiting_room_guest wrg
                    WHERE i.id = wrg.waiting_room_invite_id
                ) as participants
            FROM waiting_room_invite i
                INNER JOIN waiting_room_setting s ON i.waiting_room_setting_id =  s.id
                INNER JOIN provider p ON s.provider_id = p.id
                LEFT JOIN waiting_room_queue q ON q.waiting_room_invite_id = i.id
            WHERE
                q.id IS NULL
                AND s.account_id = %d
                AND ('' = '%s' OR s.friendly_url = '%s')
                AND s.provider_id IN
                    (SELECT provider_id FROM account_provider WHERE account_id = %d)
                %s
            ORDER BY i.date_added ASC
            LIMIT %d OFFSET %d",
            $ownerAccountId,
            $friendlyUrl,
            $friendlyUrl,
            $accountId,
            $additionalConditions,
            $limit,
            $offset
        );

        return Yii::app()->db
            ->createCommand($sql)
            ->queryAll();
    }

    /**
     * Return the upcoming count for the logged account
     * @param int $accountId
     * @return array $result
     */
    public static function getUpcomingCount($accountId, $ownerAccountId, $friendlyUrl = '', $filters = array())
    {
        $additionalConditions = '';
        if (!empty($filters['patient_nickname'])) {
            $additionalConditions = 'AND i.patient_nickname LIKE '
                . Yii::app()->db->quoteValue('%' . $filters['patient_nickname'] . '%');
        }

        $sql = sprintf(
            "SELECT
                COUNT(DISTINCT i.id)
            FROM waiting_room_invite i
                INNER JOIN waiting_room_setting s ON i.waiting_room_setting_id =  s.id
                INNER JOIN provider p ON s.provider_id = p.id
                LEFT JOIN waiting_room_queue q ON q.waiting_room_invite_id = i.id
            WHERE
                q.id IS NULL
                AND s.account_id = %d
                AND ('' = '%s' OR s.friendly_url = '%s')
                AND s.provider_id IN
                    (SELECT provider_id FROM account_provider WHERE account_id = %d)
                %s
            ORDER BY i.date_added DESC",
            $ownerAccountId,
            $friendlyUrl,
            $friendlyUrl,
            $accountId,
            $additionalConditions
        );

        return Yii::app()->db
            ->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->queryScalar();
    }

    /**
     * Get total count of upcoming visits with MP (guests)
     * @param $accountId
     * @param $ownerAccountId
     * @param string $friendly_url
     * @return mixed
     */
    public static function getUpcomingCountWithMP($accountId, $ownerAccountId, $friendlyUrl = '')
    {
        $sql = sprintf(
            "SELECT
                COUNT(DISTINCT i.id)
            FROM waiting_room_invite i
                INNER JOIN waiting_room_setting s ON i.waiting_room_setting_id =  s.id
                INNER JOIN waiting_room_guest AS wrg ON i.id = wrg.waiting_room_invite_id
                INNER JOIN provider p ON s.provider_id = p.id
                LEFT JOIN waiting_room_queue q ON q.waiting_room_invite_id = i.id
            WHERE
                q.room_sid IS NULL
                AND s.account_id = %d
                AND ('' = '%s' OR s.friendly_url = '%s')
                AND s.provider_id IN
                    (SELECT provider_id FROM account_provider WHERE account_id = %d)",
            $ownerAccountId,
            $friendlyUrl,
            $friendlyUrl,
            $accountId
        );

        return Yii::app()->db
            ->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->queryScalar();
    }

    /**
     * Indicates if the queue is online or not
     * @param bool online Sets the queue as online
     * @return boolean
     */
    public function isOnline($online = false)
    {
        $queueId = "telemedicine_" . $this->id;
        if ($online) {
            Yii::app()->cache->set($queueId . "_online", 1, 600);
        }
        $cachedRoom = Yii::app()->cache->get($queueId . "_online");
        return !empty($cachedRoom);
    }

    /**
     * Retrieve the list of patients waiting for the provider's queue
     * @return array
     */
    public function getQueue()
    {
        $now = strtotime(date('Y-m-d H:i:s'));
        // Is there any heartBeat for this waiting_room_setting ?
        $heartbeatWRS = Yii::app()->cache->get('heartbeat_wrs_' . $this->id);
        $lastPulse = TimeComponent::getDiff($now, $heartbeatWRS);

        if (empty($heartbeatWRS) || $lastPulse > 5) {
            return array();
        }

        // get queue from cache
        $queueId = "telemedicine_" . $this->id;

        // Recreate the query
        $sql = sprintf(
            "SELECT DISTINCT i.id,
            i.is_static,
            count(DISTINCT q.id) AS distinct_queue,
            MAX(q.room_created) AS room_created,
            q.room_ended,
            NULL AS heartbeat,
            MIN(q.room_created) AS first_room_created,
            ( -- Adding room_ended for the last record in wrq --
                SELECT room_ended FROM waiting_room_queue AS q1
                WHERE q1.waiting_room_invite_id = i.id
                ORDER BY q1.id DESC LIMIT 1
                ) AS last_room_ended,
            MAX(q.patient_connected) AS date_added,
            q.provider_connected,
            q.provider_disconnected,
            NOW() AS `time`,
            IF(q.room_created IS NOT NULL, 1, 0) AS joined,
            MAX(q.id) AS last_queue,
            SUM(q.room_duration) AS duration,
            i.guid,
            i.waiting_room_setting_id,
            i.patient_nickname,
            i.patient_email,
            i.patient_phone_number,

            g.nickname AS guest_nickname,
            q.patient_connected,
            q.patient_disconnected,
            q.room_sid,
            ( -- Adding GUEST connected
                SELECT COUNT(*)
                FROM waiting_room_guest_queue AS wrgq
                LEFT JOIN waiting_room_guest wrg ON wrg.id = wrgq.waiting_room_guest_id
                WHERE q.id = wrgq.waiting_room_queue_id
                AND `guest_connected` is not null
                AND `guest_disconnected` is null
                ) AS guest_joined,
            ( -- Adding participants connected
                SELECT COUNT(*)
                FROM waiting_room_guest_queue AS wrgq
                LEFT JOIN waiting_room_guest wrg ON wrg.id = wrgq.waiting_room_guest_id
                WHERE q.id = wrgq.waiting_room_queue_id
                AND `type`='P'
                AND `guest_connected` is not null
                AND `guest_disconnected` is null
                ) + (IF (q.patient_disconnected IS NULL AND q.patient_connected IS NOT NULL, 1, 0)) AS
                participants_connected,

            ( -- Adding total participants + mainPatient
                SELECT COUNT(*) FROM waiting_room_guest wrg
                WHERE i.id = wrg.waiting_room_invite_id
                AND `type`='P'
                ) + 1 as total_participants,
            ( -- Adding colleagues connected
                SELECT COUNT(*) FROM waiting_room_guest_queue AS wrgq
                LEFT JOIN waiting_room_guest wrg ON wrg.id = wrgq.waiting_room_guest_id
                WHERE q.id = wrgq.waiting_room_queue_id
                AND `type`='C'
                AND `guest_connected` is not null
                AND `guest_disconnected` is null
                ) as colleagues_connected,
            ( -- Adding total colleagues
                SELECT COUNT(*) FROM waiting_room_guest wrg
                WHERE i.id = wrg.waiting_room_invite_id
                AND `type`='C'
                ) as total_colleagues
            FROM waiting_room_queue AS q
            INNER JOIN waiting_room_invite AS i ON i.id = q.waiting_room_invite_id
            LEFT JOIN waiting_room_guest AS g ON i.id = g.waiting_room_invite_id
            WHERE
                i.enabled = 1
                AND i.waiting_room_setting_id = %d
                AND (DATE(q.room_created) = DATE(NOW())) -- only show the today call's
                GROUP BY q.waiting_room_invite_id
                HAVING
                (guest_joined > 0 OR participants_connected > 0 OR colleagues_connected > 0 OR joined > 0)

                OR ( -- Include if the provider hang_up but the patient still connected
                    last_room_ended IS NULL -- if is null, the call is in course
                    AND q.patient_disconnected IS NULL -- the pratient still connected
                    AND q.room_sid IS NOT NULL -- must be connected sometime
                    )
                OR ( -- Include if the patient is the only connected
                    last_room_ended IS NULL -- if is null, the call is in course
                    AND q.patient_connected IS NOT NULL -- the pratient still connected
                    )
                OR ( -- Include if only the guest is connected
                    last_room_ended IS NULL -- if is null, the call is in course
                    AND q.patient_disconnected IS NULL -- the pratient still connected
                    AND q.patient_connected IS NULL -- the pratient still connected
                    AND q.provider_disconnected IS NULL -- the pratient still connected
                    AND q.provider_connected IS NULL -- the pratient still connected
                    AND q.room_sid IS NULL -- must be connected sometime
                    )
            ORDER BY q.id DESC",
            $this->id
        );

        $arrQueue = Yii::app()->db->createCommand($sql)->queryAll();

        if (!empty($arrQueue)) {
            // Action to take on special cases
            foreach ($arrQueue as $k => $v) {
                $arrQueue[$k]['utc_time_start'] = TimeComponent::convertToUTC($v['room_created']);
                $heartbeatWRI =  Yii::app()->cache->get('heartbeat_wri_' . $v['id']);
                $arrQueue[$k]['heartbeat'] = date("Y-m-d H:i:s", $heartbeatWRI);

                if (empty($heartbeatWRI)) {
                    // never connected
                    unset($arrQueue[$k]);
                } else {
                    $lastPulse = TimeComponent::getDiff($now, $heartbeatWRI);
                    if ($lastPulse > 3) {
                        WaitingRoomQueue::removeUnused($v['last_queue']);
                        // last participant was connected more than 5 secons ago
                        Yii::app()->cache->set('heartbeat_wri_' . $v['id'], null);
                        unset($arrQueue[$k]);
                    }
                }
            }
            $arrQueue = array_values($arrQueue);
        }

        // update the entry in the cache
        $data = array(
            'created' => date('Y-m-d H:i:s'),
            'data' => $arrQueue,
        );
        // Save the cache
        Yii::app()->cache->set($queueId, json_encode($data), 3 * Yii::app()->params['cache_short']);

        return $arrQueue;
    }

    /**
     * return the URL
     */
    public static function getUrl($type = 'visit')
    {
        $url = "https://" . Yii::app()->params['servers']['dr_off_hn'];

        if ($type == 'visit') {
            $url = "https://" . Yii::app()->params['servers']['dr_vis_hn'];
        }

        return $url;
    }


    /**
     * Get names of accounts own $providerId
     * @param int $providerId
     * @param int $accountId
     * @return string
     */
    public static function getProviderAccountWRS($providerId, $accountId)
    {

        $sql = sprintf(
            "SELECT account.id, CONCAT(account.first_name, ' ', account.last_name, ' - ', account.email) AS name
            FROM account_provider
            INNER JOIN account
                ON account_provider.account_id = account.id
            LEFT JOIN organization_account
                ON account.id = organization_account.account_id
            WHERE provider_id = %d
                AND account.enabled = 1
                AND ( organization_account.id IS NULL OR organization_account.connection = 'O' );",
            $providerId
        );
        $res = Yii::app()->db->createCommand($sql)->query();

        // build the account ids string
        $str = "";
        $selected = false;
        foreach ($res as $value) {

            $str .= "<option value='" . $value['id'] . "'";

            // if accountId are setting
            if (!is_null($accountId) && $accountId == $value['id']) {
                $selected = true;
                $str .= " selected ";
            }

            $str .=  ">" . $value['name'] . "</option>";
        }

        if (!$selected && !empty($accountId)) {

            $account = Account::model()->findByPk($accountId);
            if ($account) {
                $str .= "<option value='" . $account->id . "' selected>" . $account->first_name . ' '
                    . $account->last_name . ' - ' . $account->email . "</option>";
            }
        }

        return $str;
    }
}
