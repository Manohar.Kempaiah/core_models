<?php

Yii::import('application.modules.core_models.models._base.BaseInsurancePlanTypes');

class InsurancePlanTypes extends BaseInsurancePlanTypes
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Return array with all plan types
     *
     * @return array
     */
    public static function getList()
    {
        $sql = "SELECT id, `name` FROM insurance_plan_types ORDER BY `id` ASC";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

}
