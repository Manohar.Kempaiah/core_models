<?php

Yii::import('application.modules.core_models.models._base.BaseCpnErrorLog');

class CpnErrorLog extends BaseCpnErrorLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Error Log
     *
     * @param obj $request
     * @param string $ipAddress
     * @return bool
     */
    public function errorLog($request, $ipAddress)
    {
        // Create new CpnErrorLog
        $cel = new CpnErrorLog();

        //do we have the software identifier?
        //yes
        if (isset($request->softwareIdentifier)) {
            //set the software identifier
            $cel->software_identifier = $request->softwareIdentifier;
        }

        //did we get an installation identifier?
        //yes
        if (isset($request->installationIdentifier)) {
            //set the installation identifier
            $cel->installation_identifier = $request->installationIdentifier;
        }

        //set the IP address
        $cel->ip_address = $ipAddress;

        //set our error
        $cel->error_text = $request->error;

        //set date_added
        $cel->date_added = date('Y-m-d H:i:s');

        //save it to the database
        $resultSave = $cel->save();

        //did it save ok?
        //yes
        if ($resultSave) {
            //return success
            return true;
        }

        //return failure if we got here
        return false;
    }
}
