<?php

Yii::import('application.modules.core_models.models._base.BaseReviewRequestExternal');

class ReviewRequestExternal extends BaseReviewRequestExternal
{
    // defined here so we don't have to redefine all over the place
    const MAX_GENERATE_RR_LINKS = 1000;
    const MAX_SEND_RR = 100;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Send review request to a patient
     * @param array $data
     * @param string $partnerSiteId
     * @param string $appTitle
     * @return json
     */
    public static function sendReviewRequest($data = array(), $partnerSiteId, $appTitle)
    {

        $results['success'] = false;
        $results['error'] = '';
        $results['data'] = '';

        // Generate only one link for this record
        $data['count'] = 1;
        $data['return_account_device_id'] = true;

        // Set up the send_via field
        if (!empty($data['patient_email']) && !empty($data['patient_phone'])) {
            $sendVia = 'BOTH';
        } elseif (!empty($data['patient_email'])) {
            $sendVia = 'EMAIL';
        } elseif (!empty($data['patient_phone'])) {
            $sendVia = 'SMS';
        } else {
            $sendVia = 'API'; // by default
        }
        $data['send_via'] = $sendVia;

        // Generate the review request links for the given parameters
        $rrLink = AccountDevice::generateReviewRequestLinks($data, $partnerSiteId, $appTitle);

        // IS there an error?
        if (empty($rrLink['key'][0])) {
            // Yes
            $results['success'] = false;
            $results['error'] = 'Review request links not found for the given parameters.';
            $results['data'] = $data;
            return $results;
        }

        $ddcLink = $rrLink['base_url'] . $rrLink['key'][0];
        $data['ddc_link'] = $ddcLink;
        $accountDeviceId = $rrLink['account_device_id'];
        $reviewRequestExternalId = $rrLink['review_request_external_id'];

        $practiceId = Common::get($data, 'practice_id', null);
        $providerId = Common::get($data, 'provider_id', null);
        $sendTo = Common::get($data, 'send_to', 'DDC');
        $source = 'review_request_external';
        $data['coming_from'] = 'review_request_external';

        // Look for the practice
        $practiceObj = Practice::model()->cache(Yii::app()->params['cache_medium'])->findByPk($practiceId);
        // Looking for the practice_details
        $pd = PracticeDetails::model()->cache(Yii::app()->params['cache_medium'])->find(
            'practice_id=:practiceId',
            array(':practiceId' => $practiceId)
        );
        if (!$pd) {
            // practice details missing, generate them
            $pd = new PracticeDetails;
            $pd->practice_id = $practiceId;
            $pd->save();
        }

        // prepare patient data
        $patientData = new stdClass();
        $patientData->email  = StringComponent::validateEmail($data['patient_email']);
        $patientData->firstName  = Common::get($data, 'patient_first_name', null);
        $patientData->lastName  = Common::get($data, 'patient_last_name', null);
        $patientData->lang_id = 'en';
        $patientData->account_device_id = $accountDeviceId;
        $patientData->location_id = !empty($practiceObj->location_id) ? $practiceObj->location_id : 999999;
        $patientData->phone = Common::get($data, 'patient_phone', null);
        $patientData->partner_site_id = $partnerSiteId;

        // create or update a patient
        $patientData = Patient::createOrUpdatePatient($patientData);

        // Do we have a valid patient?
        $patient = $patientData['arrPatient'];
        if (empty($patient->id)) {
            // No
            $results['success'] = false;
            $results['error'] = 'PATIENT_NOT_EXISTS';
            $results['data'] = 'Patient not exists.';
            return $results;
        }

        // Add patient_id to the review_request_external
        $rre = ReviewRequestExternal::model()->findByPk($reviewRequestExternalId);
        $rre->patient_id = $patient->id;
        $rre->save();

        // Look for the practice<->patient relation
        $practicePatient = PracticePatient::model()->exists(
            'patient_id=:patientId AND practice_id=:practiceId',
            array(':patientId' => $patient->id, ':practiceId' => $practiceId)
        );
        if (!$practicePatient) {
            // link patient to practice if not already linked
            $practicePatient = new PracticePatient;
            $practicePatient->patient_id = $patient->id;
            $practicePatient->practice_id = $practiceId;
            $practicePatient->primary_practice = 1;
            $practicePatient->save();
        }

        $customFields = array();
        $customFields['send_to'] = $sendTo; // DDC
        $customFields['template'] = !empty($data['email_template']) ? $data['email_template'] :
            'SEND_REVIEW_REQUEST_EXTERNAL';
        $customFields['source'] = !empty($data['source']) ? $data['source'] : '';
        $rrLink = '';

        // If there's custom fields for DDC Review Request
        if (!empty($data['reviewRequestEmailTitle'])) {
            $customFields['emailTitle'] = $data['reviewRequestEmailTitle'];
        }
        if (!empty($data['reviewRequestEmailBody'])) {
            $customFields['emailBody'] = $data['reviewRequestEmailBody'];
        }

        if (!empty($data['reviewRequestSmsBody'])) {
            $customFields['smsBody'] = $data['reviewRequestSmsBody'];
        }

        //
        // Prepare $rr params
        //
        $rr = new stdClass();
        $rr->patient_id = $patient->id;
        $rr->practice_id = $practiceId;
        $rr->google_link = '';
        $rr->yelp_link = '';
        $rr->ddc_link = $ddcLink;
        $rr->custom_link = '';
        $rr->id = $reviewRequestExternalId;
        $rr->provider_id = $providerId;
        $rr->source = $source; //'review_request_external'
        $rr->send_link = $sendTo;

        // do we have something to send? otherwise fail
        $mailSent = $smsSent = false;

        //
        // Send ReviewRequest via EMAIL
        //
        if ($data['send_via'] == 'BOTH' || $data['send_via'] == 'EMAIL') {
            $mailSent = (bool) MailComponent::sendReviewRequest($rr, $customFields);
        }

        //
        // Send ReviewRequest via SMS
        //
        if ($data['send_via'] == 'BOTH' || $data['send_via'] == 'SMS') {
            // send sms
            $smsSent = (bool) SmsComponent::sendReviewRequest($rr, $customFields);
        }

        // Create the entry in the audit_log
        $auditSection = 'Review request External sent to patient #' . $patient->id .
            ' (' . $patient->first_name . ' ' . $patient->last_name . ') (' . $sendTo . ')';
        AuditLog::create('C', $auditSection, 'review_request_external', null, false);

        if (Common::isTrue($mailSent) || Common::isTrue($smsSent)) {
            return true;
        }

        return false;
    }

}
