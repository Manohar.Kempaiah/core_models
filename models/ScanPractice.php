<?php

Yii::import('application.modules.core_models.models._base.BaseScanPractice');

class ScanPractice extends BaseScanPractice
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Find the most recent ScanPractice and related information: For Google and Yelp: number, average, whether there
     * were reviews in the last 3 months
     * @param int $practiceId
     * @return ScanPractice
     */
    public static function getLatestByPractice($practiceId = 0)
    {

        // find latest scan for that practice
        $sql = sprintf(
            "SELECT google_reviews, yelp_reviews
            FROM scan_practice
            WHERE practice_id = %d
            ORDER BY date_added DESC
            LIMIT 1",
            $practiceId
        );
        $latest = ScanPractice::model()->cache(Yii::app()->params['cache_medium'])->findBySql($sql);

        // did we find a scanpractice object?
        if (empty($latest)) {
            // no, just leave
            return false;
        }
        $arrScan = $latest->attributes;

        // find the latest scan.id
        $sql = sprintf(
            "SELECT id
            FROM scan
            WHERE practice_id = %d
            AND provider_id IS NULL
            ORDER BY date_added DESC
            LIMIT 1",
            $practiceId
        );
        $scanId = ScanPractice::model()->cache(Yii::app()->params['cache_medium'])->findBySql($sql);
        if (!empty($scanId)) {
            $scanId = $scanId['id'];
        }

        // find latest google average
        $googleReviewAvg = ScanResult::model()->cache(Yii::app()->params['cache_medium'])->find(
            'scan_id = :scanId AND code = "google"',
            array(':scanId' => $scanId)
        );

        // do we have a google average?
        if (!empty($googleReviewAvg->review_average)) {
            // yes, use it
            $googleReviewAvg = $googleReviewAvg->review_average;
        } else {
            // no, use 0
            $googleReviewAvg = 0;
        }

        // find latest yelp average
        $yelpReviewAvg = ScanResult::model()->cache(Yii::app()->params['cache_medium'])->find(
            'scan_id = :scanId AND code = "YELP"',
            array(':scanId' => $scanId)
        );

        // do we have a yelp average?
        if (!empty($yelpReviewAvg->review_average)) {
            // yes, use it
            $yelpReviewAvg = $yelpReviewAvg->review_average;
        } else {
            // no, use 0
            $yelpReviewAvg = 0;
        }

        // find latest google review
        $google3m = false;
        $providerRating = ProviderRating::model()->cache(Yii::app()->params['cache_medium'])->find(
            'practice_id = :practiceId AND partner_site_id = :partnerSiteId',
            array(':practiceId' => $practiceId, ':partnerSiteId' => PartnerSite::GOOGLE_PARTNER_SITE_ID)
        );
        // did we find a google review?
        if (!empty($providerRating)) {
            // yes, is it from the last 3 months?
            if (strtotime($providerRating->date_added) > strtotime('-3 months')) {
                // yes
                $google3m = true;
            }
        }

        // find latest yelp review
        $yelp3m = false;
        $providerRating = ProviderRating::model()->cache(Yii::app()->params['cache_medium'])->find(
            'practice_id = :practiceId AND yext_site_id = "yelp"',
            array(':practiceId' => $practiceId)
        );
        // did we find a recent yelp review?
        if (!empty($providerRating)) {
            // yes, is it from the last 3 months?
            if (strtotime($providerRating->date_added) > strtotime('-3 months')) {
                // yes
                $yelp3m = true;
            }
        }

        // merge everything and return
        return array_merge(
            $arrScan,
            ['googleAvg' => $googleReviewAvg],
            ['yelpAvg' => $yelpReviewAvg],
            ['google3m' => $google3m],
            ['yelp3m' => $yelp3m]
        );
    }

    /**
     * Get a ScanPractice object by practice and date_added
     * @param int $practiceId
     * @param string $dateAdded
     * @return ScanPractice
     */
    public static function getByPracticeIdAndDate($practiceId, $dateAdded)
    {

        return ScanPractice::model()->cache(Yii::app()->params['cache_long'])->find(
            'practice_id = :practice_id AND '
            . ' DATE(date_added) = :date_added '
            . ' ORDER BY ABS(TIMEDIFF(:datetime_added, date_added)) DESC',
            array(
                'practice_id' => $practiceId,
                ':date_added' => substr($dateAdded, 0, 10), ':datetime_added' => $dateAdded)
            );
    }

    /**
     * Get The scan log for the current scan_practice record
     * @return int
     */
    public function getScanLogId()
    {
        // the record has a defined scan_log_id
        // yes
        if (!empty($this->scan_log_id)) {
            // return scan_log_id
            return $this->scan_log_id;
        } else {
            // if the value is unset, recover it in the old way
            $sql = sprintf(
                "SELECT scan_log.id " .
                " FROM scan_practice  " .
                " INNER JOIN scan_log ON " .
                " scan_log.date_added BETWEEN DATE_SUB(scan_practice.date_added, INTERVAL 1 SECOND) " .
                " AND DATE_ADD(scan_practice.date_added, INTERVAL 1 SECOND) " .
                " WHERE scan_practice.date_added <= '%s' AND " .
                " DATE_FORMAT(scan_practice.date_added, '%%Y-%%m') = '%s' AND scan_practice.practice_id = '%s' " .
                " AND data LIKE CONCAT('%%', scan_practice.practice_name, '%%') " .
                " ORDER BY scan_log.id DESC " .
                " LIMIT 1;",
                $this->date_added,
                substr($this->date_added, 0, 7),
                $this->practice_id
            );

            // return scan_log_id
            return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryScalar();
        }
    }

    /**
     * Get the history of a practice
     *
     * @param integer $practiceId
     * @return array
     */
    public function getHistoryForScan($practiceId)
    {
        $sql = "SELECT
            scan_practice.id AS id,
            scan_practice.practice_id,
            scan_practice.practice_name,
            scan_practice.address,
            scan_practice.phone,
            scan_practice.date_added
        FROM
          scan_practice
        INNER JOIN practice ON scan_practice.practice_id = practice.id
        WHERE
            YEAR(scan_practice.date_added) <= YEAR(CURRENT_DATE - INTERVAL 1 MONTH)
            AND MONTH(scan_practice.date_added) <= MONTH(CURRENT_DATE - INTERVAL 1 MONTH)
            AND scan_practice.practice_id = :practice_id
            AND practice.name = scan_practice.practice_name
            AND practice.address = scan_practice.address
            AND practice.phone_number = scan_practice.phone
        ORDER BY scan_practice.id ASC
        LIMIT 1";

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValue(':practice_id', $practiceId, PDO::PARAM_INT)
            ->queryRow();
    }

}
