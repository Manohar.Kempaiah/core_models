<?php

Yii::import('application.modules.core_models.models._base.BaseScanStatsPractice');

class ScanStatsPractice extends BaseScanStatsPractice
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('scan_input_practice_id', 'required'),
            array('id, scan_input_practice_id, op_score, listing_accuracy, missing_listings, total_reviews, google_reviews, yelp_reviews, profile_completion_score', 'numerical', 'integerOnly' => true),
            array('reviews_average', 'length', 'max' => 10),
            array('reviews_grade', 'length', 'max' => 5),
            array('profile_completion_missing_data, missing_listing_names', 'safe'),
            array('op_score, reviews_average, listing_accuracy, missing_listings, total_reviews, google_reviews, yelp_reviews, profile_completion_score, profile_completion_missing_data, missing_listing_names, reviews_grade', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, scan_input_practice_id, op_score, reviews_average, listing_accuracy, missing_listings, total_reviews, google_reviews, yelp_reviews, profile_completion_score, profile_completion_missing_data, missing_listing_names, reviews_grade', 'safe', 'on' => 'search'),
        );
    }

    public function getLatestByPractice($practiceId)
    {
        $criteria = new CDbCriteria();

        $criteria->with[] = 'scanInputPractice';
        $criteria->with[] = 'scanInputPractice.scanRequest';

        $criteria->condition = 'scanInputPractice.practice_id = :practice_id';
        $criteria->params = [
            ':practice_id' => $practiceId
        ];

        $criteria->order = 'scanInputPractice.id DESC';

        return $this->find($criteria);
    }

    public function getLatestByPracticeForCompetitors($practiceId)
    {
        $criteria = new CDbCriteria();

        $criteria->with[] = 'scanInputPractice';
        $criteria->with[] = 'scanInputPractice.scanRequest';
        $criteria->with[] = 'scanInputPractice.scanRequest.scanTemplate';
        $criteria->with['scanInputPractice.scanRequest.scanTemplate.scanTemplateType'] = [
            'alias' => 'stt'
        ];

        $criteria->addCondition('scanInputPractice.practice_id = :practice_id');

        // Ignore initial competitors scan wich is reduced in listings
        $criteria->addCondition('stt.name != "Competitors"');

        $criteria->params = [
            ':practice_id' => $practiceId
        ];

        $criteria->order = 'scanInputPractice.id DESC';

        return $this->find($criteria);
    }

    public function averageForState($stateId)
    {
        $date = new DateTime("-2 months");
        $values = [
            ":state_id" => $stateId,
            ":date" => $date->format("Y-m-d H:i:s")
        ];

        $sql = "
        SELECT
            AVG(scan_stats_practice.listing_accuracy) AS accuracy,
            AVG(scan_stats_practice.total_reviews) AS total_reviews,
            AVG(scan_stats_practice.reviews_average) AS reviews_average,
            AVG(scan_stats_practice.missing_listings) AS missing_listings,
            AVG(scan_stats_practice.op_score) AS score
        FROM scan_stats_practice
            JOIN scan_request_practice ON scan_request_practice.id = scan_stats_practice.scan_input_practice_id
            JOIN scan_request ON scan_request.id = scan_request_practice.scan_request_id
            JOIN practice ON practice.id = scan_request_practice.practice_id
            JOIN location ON location.id = practice.location_id
            JOIN city ON city.id = location.city_id
            JOIN state ON state.id = city.state_id
        WHERE
            state.id = :state_id
            AND scan_request.date_added > :date
            AND scan_request.organization_id IS NOT NULL
        ";

        return Yii::app()->db
            ->createCommand($sql)
            ->bindValues($values)
            ->queryRow();
    }
}
