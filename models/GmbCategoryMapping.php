<?php

Yii::import('application.modules.core_models.models._base.BaseGmbCategoryMapping');

class GmbCategoryMapping extends BaseGmbCategoryMapping
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * This is invoked before the record is saved.
     * @return boolean whether the record should be saved.
     */
    public function beforeSave()
    {

        if ($this->isNewRecord) {
            $this->date_added = date('Y-m-d H:i:s');
            $sql = "SELECT count(id) AS count FROM gmb_category_mapping WHERE  sub_specialty_id = '" . $this->sub_specialty_id . "'";
        } else {
            $sql = "SELECT count(id) AS count FROM gmb_category_mapping WHERE id != " . $this->id . "  AND sub_specialty_id = '" . $this->sub_specialty_id . "'";
        }

        $res = Yii::app()->db->createCommand($sql)->queryRow();
        if (isset($res['count']) && $res['count'] > 0) {
            $this->addError(
                "level",
                "This sub specialty id (" . $this->sub_specialty_id . ") is already available in database"
            );
            return false;
        }

        $this->date_updated = date('Y-m-d H:i:s');
        return parent::beforeSave();
    }

}
