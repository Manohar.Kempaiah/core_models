<?php

Yii::import('application.modules.core_models.models._base.BaseProviderMedication');

class ProviderMedication extends BaseProviderMedication
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Before Validate
     * @return boolean
     */
    public function beforeValidate()
    {
        if ($this->medication_id == "") {
            $this->medication_id = "0";
        }

        return parent::beforeValidate();
    }

    /**
     * Before Save
     * @return boolean
     */
    public function beforeSave()
    {

        $returnValue = true;
        if ($this->medication_id == "0") {

            $medication_id_save = CHttpRequest::getPost('medication_id_save');

            if ($medication_id_save == '') {
                $medication_id_save = CHttpRequest::getPost('medication_id_lookup');
            }

            if ($medication_id_save != "") {

                // check if already existed
                $medication_id = Medication::model()->find('name=:name', array(':name' => $medication_id_save))->id;
                if ($medication_id > 0) {
                    $this->medication_id = $medication_id;
                    $returnValue = true;
                }
            }
            $returnValue = false;
        }

        if ($returnValue) {

            // check for provider_details, create it if necessary
            $providerDetails = ProviderDetails::model()->find(
                'provider_id=:provider_id',
                array(':provider_id' => $this->provider_id)
            );
            if (!$providerDetails) {
                $providerDetails = new ProviderDetails();
                $providerDetails->provider_id = $this->provider_id;
            }
            $providerDetails->has_medications = 1;
            $providerDetails->save();

            return parent::beforeSave();
        }
        $this->addError("id", "Couldn't delete medication.");
        return false;
    }

    /**
     * called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id
            . '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider
            . '">' . $this->provider . '</a>');
    }

    /**
     * Get details about the medications a provider prescribes
     * @param int $providerId
     * @param bool $limit
     * @return array
     */
    public function getDetails($providerId, $limit = false)
    {

        $sql = sprintf(
            "SELECT SQL_CALC_FOUND_ROWS
                DISTINCT(name) AS medication_name
            FROM provider_medication
            INNER JOIN medication
                ON provider_medication.medication_id = medication.id
            WHERE provider_medication.provider_id = '%d'
            ORDER BY medication_name
            %s;",
            $providerId,
            ($limit ? 'LIMIT 50' : '')
        );
        $arrMedications = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();

        $sql = sprintf("SELECT FOUND_ROWS();");
        $countMedications = Yii::app()->db
            ->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)->queryScalar();

        return array($arrMedications, $countMedications);
    }

    /**
     * PADM: Get a provider's medications
     * @param int $providerId
     * @param int $limit
     * @param int $pageSize
     * @return array
     */
    public static function getProviderMedications($providerId, $limit = 0, $pageSize = 10, $cache = true)
    {

        $sql = sprintf(
            'SELECT pm.id AS id, pm.medication_id, pm.provider_id,
                IF(ndc IS NOT NULL, CONCAT(`name`, " (", ndc,")"), `name`) AS `name`
            FROM provider_medication AS pm
            INNER JOIN medication
                ON pm.medication_id = medication.id
            WHERE pm.provider_id = %d
            ORDER BY `name`
            LIMIT %d, %d;',
            $providerId,
            $limit,
            $pageSize
        );

        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_provider
            INNER JOIN provider_medication
                ON account_provider.provider_id = provider_medication.provider_id
            WHERE provider_medication.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        foreach ($postData as $data) {
            $auditAction = '';
            $model = null;

            if (isset($data['backend_action']) && $data['backend_action'] == 'delete' && $data['id'] > 0) {
                $model = ProviderMedication::model()->findByPk($data['id']);
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_MEDICATION";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }
                    $auditAction = 'D';
                }
            } else {
                if (!empty($data['id']) && $data['id'] > 0) {
                    $auditAction = 'U';
                    $model = ProviderMedication::model()->findByPk($data['id']);
                }
                if (empty($model)) {
                    $auditAction = 'C';
                    $model = new ProviderMedication;
                }
                $model->provider_id = $providerId;
                $model->medication_id = $data['medication_id'];

                if (!$model->save()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_MEDICATION";
                    $results['data'] = $model->getErrors();
                    return $results;
                } else {
                    if ($auditAction == 'C') {
                        // return the new item
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                }
            }
            if (!empty($auditAction)) {
                $auditSection = 'ProviderMedication #' . $providerId . ' (#' . $model->id . ')';
                AuditLog::create($auditAction, $auditSection, 'provider_medication', null, false);
            }
        }
        return $results;
    }
}
