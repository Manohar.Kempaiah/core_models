<?php

Yii::import('application.modules.core_models.models._base.BaseCpnInstrumentationLog');

class CpnInstrumentationLog extends BaseCpnInstrumentationLog
{

    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * Creates a record into cpn_instrumentation_log with base on a JSON string received from SQS
     * @param string $json
     * @return boolean
     */
    public function createFromJson($json)
    {
        // decode the document into an object
        $objLog = json_decode($json);

        // check timestamp format and generate an event timestamp
        if (StringComponent::validDate($objLog->timestamp, "Y-m-d\TH:i:s\Z")) {
            // convert UTC timestamp into EST
            $timestamp = DateTime::createFromFormat("Y-m-d\TH:i:s\Z", $objLog->timestamp, new DateTimeZone('UTC'));
            $timestamp->setTimeZone(new DateTimeZone('America/New_York'));
        } else {
            // Invalid timestamp, default to now
            $timestamp = new DateTime();
        }

        // process event_type
        $arrType = explode('.', $objLog->type);
        $arrUi = explode('_', $arrType[0]);

        // set table attributes
        $this->application_name = $objLog->applicationName;
        $this->session_identifier = $objLog->sessionIdentifier;
        $this->event_type = $objLog->type;
        $this->event_category = $objLog->category;
        $this->event_timestamp = $timestamp->format('Y-m-d H:i:s');
        $this->ui_element_type = $arrUi[0];
        $this->ui_event_type = $arrUi[1];
        $this->ui_target_object = $arrType[1];
        $this->ui_event_result = $arrType[2];
        $this->app_identifier = $objLog->appSource->identifier;
        $this->app_version = $objLog->appSource->version;
        $this->app_installation_identifier = $objLog->appSource->installationIdentifier;
        $this->app_secret = $objLog->appSource->secret;
        $this->app_method = $objLog->appSource->method;
        $this->source_message = $json;
        $this->date_added = date('Y-m-d H:i:s');

        // save
        return $this->save();
    }

    /**
     * Get entries from cpn_instrumentation_log as a partner_data_feed insert entry
     * @param int $sinceId
     * @param int $page
     * @param int $size
     * return array
     */
    public static function getLogEntries($sinceId, $page, $size)
    {
        // get next entries from the log
        $sql = sprintf(
            "SELECT cil.id, cil.application_name, cil.session_identifier, cil.event_type, cil.event_category,
                cil.event_timestamp, cil.ui_element_type, cil.ui_event_type, cil.ui_target_object,
                cil.ui_event_result, cil.app_identifier, cil.app_version, ci.id as cpn_installation_id,
                ci.practice_id, ci.account_id, o.id as organization_id, o.salesforce_id, cil.app_method,
                cil.date_added
            FROM cpn_instrumentation_log cil
            LEFT JOIN cpn_installation ci ON cil.app_installation_identifier = ci.installation_identifier
                AND cil.app_secret = ci.secret
            LEFT JOIN organization_account oa ON oa.account_id = ci.account_id AND connection = 'O'
            LEFT JOIN organization o ON o.id = oa.organization_id
            WHERE cil.id > '%d'
            ORDER BY cil.id ASC
            LIMIT %d, %d;",
            $sinceId,
            $page,
            $size
        );
        $result = Yii::app()->db->createCommand($sql)->queryAll();

        // format entries as partner_data_feed
        $messages = array();
        foreach ($result as $r) {
            $messages[] = array(
                'id' => $r['id'],
                'timestamp' => $r['date_added'],
                'entity_type' => 'cpn_instrumentation_log',
                'data_object' => '{"values":{' . substr(json_encode($r), 1, -1) . '}}',
                'operation' => 'INSERT',
                'partner_site_id' => '1'
            );
        }

        return $messages;
    }
}
