<?php

Yii::import('application.modules.core_models.models._base.BaseProviderStats');

class ProviderStats extends BaseProviderStats
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Track a provider profile view by trying to update the table and otherwise insert into it
     * @param int $providerId
     * @return boolean
     */
    public static function trackProviderProfileView($providerId)
    {

        if (intval($providerId) < 0) {
            return false;
        }

        if (UserAgentComponent::isBot()) {
            return false;
        }

        //update stats
        $sql = sprintf(
            "UPDATE provider_stats
            SET profile_views = profile_views + 1
            WHERE provider_id = '%s'
            LIMIT 1;",
            $providerId
        );
        $affectedRows = Yii::app()->db->createCommand($sql)->execute();

        if ($affectedRows == 0) {
            //there was no record for this provider: insert stats
            $sql = sprintf(
                "INSERT INTO provider_stats
                (provider_id, profile_views)
                VALUES
                ('%s', '1');",
                $providerId
            );
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    /**
     * Track a provider search view by trying to update the table and otherwise insert into it
     * @param int $providerId
     * @return boolean
     */
    public static function trackProviderSearchView($providerId)
    {

        if (intval($providerId) < 0) {
            return false;
        }

        if (UserAgentComponent::isBot()) {
            return false;
        }

        //update stats
        $sql = sprintf(
            "UPDATE provider_stats
            SET search_views = search_views + 1
            WHERE provider_id = '%s'
            LIMIT 1;",
            $providerId
        );
        $affectedRows = Yii::app()->db->createCommand($sql)->execute();

        if ($affectedRows == 0) {
            //there was no record for this provider: insert stats
            $sql = sprintf(
                "INSERT INTO provider_stats
                (provider_id, search_views)
                VALUES
                ('%s', '1');",
                $providerId
            );
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    /**
     * Track an element view for a practice by upserting data for its providers
     * @param array $arrData
     */
    public static function trackPracticeElementView($arrData)
    {

        // grab practice id
        $practiceId = $arrData['practiceId'];
        unset($arrData['practiceId']);

        // inserts stats for ALL providers in the practice
        $providersAtPractice = ProviderPractice::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $practiceId)
        );

        foreach ($providersAtPractice as $providerAtPractice) {
            $arrData['providerId'] = $providerAtPractice->provider_id;
            // insert stats or update for this provider-at-practice using sqs
            SqsComponent::sendMessage('trackProviderElementView', ($arrData));
        }
    }

    /**
     * Track a provider element view by trying to update the table and otherwise insert into DB or, if
     * $elementViewedPdiFormat provided, use PDI API to submit it via a web request.
     * @param array $arrData
     * @return boolean
     */
    public static function trackProviderElementView($arrData)
    {

        $siteId = $arrData['siteId'];
        $providerId = $arrData['providerId'];
        $elementViewed = $arrData['elementViewed'];
        $elementViewedPdiFormat = (!empty($arrData['elementViewedPdiFormat']) ? $arrData['elementViewedPdiFormat'] :
            null);

        // are we missing a provider?
        if (intval($providerId) < 0) {
            // yes, leave
            return false;
        }

        // is it a bot?
        if (UserAgentComponent::isBot()) {
            // yes, leave
            return false;
        }

        // should we send the update to pdi?
        if ($elementViewedPdiFormat) {

            // yes
            $arrParams = array(
                'TrackingActivity[name]' => $elementViewedPdiFormat,
                'TrackingActivity[parameters]' => json_encode(array(
                    'provider_id' => $providerId,
                )),
                'TrackingActivity[site_id]' => $siteId
            );

            // send to PDI
            $curlUrl = "https://" . Yii::app()->params->servers['dr_pdi_hn'] . '/tracking/track';
            $curlRes = curl_init($curlUrl);
            curl_setopt($curlRes, CURLOPT_HEADER, 0);
            curl_setopt($curlRes, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curlRes, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt(
                $curlRes,
                CURLOPT_USERPWD,
                Yii::app()->params['pdi_admin_user'] . ":" . Yii::app()->params['pdi_admin_pass']
            );
            curl_setopt($curlRes, CURLOPT_POST, true);
            curl_setopt($curlRes, CURLOPT_POSTFIELDS, $arrParams);
            curl_exec($curlRes);
            curl_close($curlRes);
        } else {
            // no, update stats in DB directly
            $sql = sprintf(
                "UPDATE provider_stats
                SET %s = IF (%s IS NULL, 1, %s + 1) + 1
                WHERE provider_id = %d
                LIMIT 1;",
                $elementViewed,
                $elementViewed,
                $elementViewed,
                $providerId
            );
            $affectedRows = Yii::app()->db->createCommand($sql)->execute();

            // did we affect no records?
            if ($affectedRows == 0) {
                // yes: there was no record for this provider: insert stats
                $sql = sprintf(
                    "INSERT INTO provider_stats
                    (provider_id, %s)
                    VALUES
                    (%d, '1');",
                    $elementViewed,
                    $providerId
                );
                Yii::app()->db->createCommand($sql)->execute();
            }

            // update provider stats report database
            ProviderStatsReport::model()->trackProviderElementView($providerId, $elementViewed);
        }
    }

}
