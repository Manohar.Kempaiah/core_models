<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeGp');

class ProviderPracticeGp extends BaseProviderPracticeGp
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function afterSave()
    {
        if ($this->isNewRecord) {
            $this->createGmbRecord();
        }

        return parent::afterSave();
    }

    /**
     * Creates or updates a GMB record (if place_id is missing)
     */
    public function createGmbRecord()
    {
        $response = [
            'created' => false,
            'message' => null
        ];

        // Refresh place id before doing anything
        $this->refreshPlaceId();

        // Looks for a provider_practice_gmb record
        $hasPpGmb = ProviderPracticeGmb::model()
            ->exists('place_id = :place_id', ['place_id' => $this->place_id]);

        // If there's no matching record
        if (!$hasPpGmb) {

            // Try looking for a record matching provider/practice
            if ($this->provider_id) {
                $ppGmb = ProviderPracticeGmb::model()->find(
                    'provider_id = :provider_id AND practice_id = :practice_id AND link_verified = 0',
                    ['provider_id' => $this->provider, 'practice_id' => $this->practice_id]
                );
            } else {
                $ppGmb = ProviderPracticeGmb::model()->find(
                    'provider_id IS NULL AND practice_id = :practice_id AND link_verified = 0',
                    ['practice_id' => $this->practice_id]
                );
            }

            // If a record exists, associate place_id
            if ($ppGmb) {

                $ppGmb->place_id = $this->place_id;
                $ppGmb->link_verified = 0;
                $ppGmb->save();

                $response['created'] = true;

            } else {

                // Look for organization feature
                $gmbFeature = OrganizationFeature::model()->find(
                    'feature_id = :feature_id AND active = 1
                        AND (practice_id = :practice_id OR provider_id = :provider_id)',
                    [
                        'feature_id' => Feature::GMB,
                        'provider_id' => $this->provider_id,
                        'practice_id' => $this->practice_id
                    ]
                );

                // If GMB feature is enabled for the provider/practice
                if ($gmbFeature) {

                    // Looks for an owner account
                    $organizationAccount = OrganizationAccount::model()
                        ->findOwnerAccount($gmbFeature->organization_id);

                    // Add new entry
                    if ($organizationAccount) {

                        $canCreate = true;
                        // Does not the practice linked to the account?
                        if (!AccountPractice::model()->exists(
                            'account_id = :account_id AND practice_id = :practice_id',
                            [
                                ':account_id' => $organizationAccount->account_id,
                                ':practice_id' => $this->practice_id
                            ]
                        )) {
                            $response['message'] = 'Practice is not associated to the account';
                            $canCreate = false;
                        }

                        // Does not the provider linked to the account?
                        if ($this->provider_id && !AccountProvider::model()->exists(
                            'account_id = :account_id AND provider_id = :provider_id',
                            [
                                ':account_id' => $organizationAccount->account_id,
                                ':provider_id' => $this->provider_id
                            ]
                        )) {
                            $response['message'] = 'Provider is not associated to the account';
                            $canCreate = false;
                        }

                        if (!empty($this->provider_id) && !empty($this->practice_id)) {
                            if (!ProviderPractice::model()->exists(
                                'provider_id = :provider_id AND practice_id = :practice_id',
                                ['practice_id' => $this->practice_id, 'provider_id' => $this->provider_id]
                            )) {
                                $response['message'] = 'Provider-Practice is not associated';
                                $canCreate = false;
                            }
                        }
                        if (!empty($this->provider_id)) {
                            $providerPracticeExists = ProviderPracticeGmb::model()->exists(
                                'practice_id = :practice_id
                                AND account_id = :account_id
                                AND provider_id = :provider_id',
                                [
                                    'practice_id' => $this->practice_id,
                                    'account_id' => $organizationAccount->account_id,
                                    'provider_id' => $this->provider_id
                                ]
                            );
                        } else {
                            $providerPracticeExists = ProviderPracticeGmb::model()->exists(
                                'practice_id = :practice_id AND provider_id IS NULL AND account_id = :account_id',
                                ['practice_id' => $this->practice_id, 'account_id' => $organizationAccount->account_id]
                            );
                        }

                        if (!$providerPracticeExists && $canCreate) {
                            $ppGmb = new ProviderPracticeGmb();
                            $ppGmb->account_id = $organizationAccount->account_id;
                            $ppGmb->provider_id = $this->provider_id;
                            $ppGmb->practice_id = $this->practice_id;
                            $ppGmb->place_id = $this->place_id;
                            $ppGmb->date_added = date('Y-m-d H:i:s');
                            $ppGmb->date_updated = date('Y-m-d H:i:s');
                            $ppGmb->added_by_account_id = 1;
                            $ppGmb->is_read_only = 1;
                            $ppGmb->save();

                            $response['created'] = true;
                        } else {
                            $response['created'] = false;
                        }
                    } else {
                        $response['message'] = 'No organization owner account';
                    }
                } else {
                    $response['message'] = 'No GMB feature';
                }
            }

        } else {
            $response['message'] = 'Already exists';
        }

        return $response;
    }

    public function refreshPlaceId()
    {
        // ask Google to refresh the place ID so it's not stale (this is a free api call)
        $jsonResponse = @file_get_contents(
            'https://maps.googleapis.com/maps/api/place/details/json?placeid='
            . $this->place_id . '&fields=place_id&key=' . Yii::app()->params['ddc_google_places_apikey']
        );

        if (!empty($jsonResponse)) {
            // decode the response
            $responseObj = json_decode($jsonResponse);

            // did it succeed?
            if (!empty($responseObj) && $responseObj->status == 'OK') {

                // yes, did the place id change?
                if (!empty($responseObj->result->place_id) && $responseObj->result->place_id != $this->place_id) {
                    $this->isNewRecord = false;
                    $this->place_id = $responseObj->result->place_id;
                    $this->save();
                }
            }
        }
    }

    /**
     * Searches for a record by practice and provider ids
     *
     * @param integer $practiceId
     * @param integer $providerId
     * @return ProviderPracticeGp
     */
    public function searchByPracticeAndProvider($practiceId, $providerId = null)
    {
        $values = array(
            ':practice_id' => $practiceId
        );

        $condition = "practice_id = :practice_id";

        // Add provider id parameter if it's not null
        if ($providerId) {
            $values[":provider_id"] = $providerId;
            $condition .= " AND provider_id = :provider_id";
        } else {
            $condition .= " AND provider_id IS NULL";
        }

        return ProviderPracticeGp::model()->find($condition, $values);
    }

    /**
     * Searches for a place id from provider and practice input
     *
     * @param integer $practiceId
     * @param integer $providerId
     * @return string
     */
    public function getPlaceId($practiceId, $providerId = null)
    {
        $result = $this->searchByPracticeAndProvider($practiceId, $providerId);

        if ($result) {
            // Days from last update
            $date1 = new DateTime(date("Y-m-d", strtotime($result->date_updated)));
            $date2 = new DateTime(date("Y-m-d"));
            $daysPassed = $date1->diff($date2)->days;
            // This record was updated less than 90 days ago?
            if ($daysPassed < 90) {
                // Yes
                return $result->place_id;
            }
        }
        return null;
    }

    /**
     * Update/insert a ProviderPracticeGp entry
     *
     * @param integer $practiceId
     * @param string $placeId
     * @param integer $providerId
     * @return boolean
     */
    public function upsert($practiceId, $placeId, $providerId = null)
    {
        $ppgp = $this->searchByPracticeAndProvider($practiceId, $providerId);

        if (!$ppgp) {
            $ppgp = new ProviderPracticeGp();
            $ppgp->provider_id = $providerId;
            $ppgp->practice_id = $practiceId;
            $ppgp->date_added = date("Y-m-d H:i:s");
            $ppgp->place_id = $placeId;
            return $ppgp->save();
        } else {

            // Days from last update
            $date1 = new DateTime(date("Y-m-d", strtotime($ppgp->date_updated)));
            $date2 = new DateTime(date("Y-m-d"));
            $daysPassed = $date1->diff($date2)->days;

            // If not exists or has more than 90 days, we need to update it
            if ($ppgp->place_id <> $placeId  || ($ppgp->place_id == $placeId && $daysPassed > 90)) {
                $ppgp->place_id = $placeId;
                $ppgp->date_updated = date("Y-m-d H:i:s");
                return $ppgp->save();
            }
        }

        return true;
    }
}
