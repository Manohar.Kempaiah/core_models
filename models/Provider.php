<?php

Yii::import('application.modules.core_models.models._base.BaseProvider');

class Provider extends BaseProvider
{

    public $address;
    public $zipcode;
    public $city;
    public $state;
    public $phone_number;
    public $email;
    public $website;
    public $suspended;
    public $credential_name;
    public $vanity_specialty;
    public $impersonate_id;
    public $multiple_accounts;
    public $partner;
    public $uploadPath;
    public $organization;
    public $featured_partner_site;
    public $adenhance_url;
    public $verified;
    public $nasp_date;

    /**
     * When a new provider is created, enable automatically the appointment booking in provider_details table.
     *
     * @var boolean
     */
    public $enableAppointmentBooking = true;

    /**
     * Indicates the Provider isn't linked to any DDC Account.
     */
    const CLAIMED_NO_ACCOUNT = 0;

    /**
     * Indicates the Provider is linked to a Freemium Account.
     * That means account that isn't related to an Organization or Account that are related to a
     * Canceled Organization (Churned Client).
     */
    const CLAIMED_FREEMIUM_ACCOUNT = 1;

    /**
     * Indicates the Provider is linked to a Paying Client (Active/ Paused Organization).
     */
    const CLAIMED_PAYING_ACCOUNT = 2;

    /**
     * Init
     */
    public function init()
    {
        parent::init();
        $basePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'providers';
        $this->uploadPath = $basePath . Yii::app()->params['upload_providers'];
    }

    /**
     * Columns representing the schema
     *
     * @return array
     */
    public static function representingColumn()
    {
        return array('prefix', 'first_name', 'last_name');
    }

    /**
     * Model
     */
    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Rules
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = array('fees', 'length', 'max' => 500);
        $rules[] = array('verified, is_test', 'default', 'setOnEmpty' => true, 'value' => 0);
        $rules[] = array('verified, is_test', 'numerical', 'integerOnly' => true);
        $rules[] = array('verified, is_test', 'safe', 'on' => 'search');
        $rules[] = array('nasp_date', 'safe', 'on' => 'search');

        return $rules;
    }

    /**
     * Additional relations
     *
     * @return array
     */
    public function relations()
    {
        $relations = parent::relations();

        $relations['providerInsurances'] = array(self::HAS_MANY, 'ProviderInsurance', 'provider_id');
        $relations['providerMedications'] = array(self::HAS_MANY, 'ProviderMedication', 'provider_id');
        $relations['providerMedicationBackups'] = array(self::HAS_MANY, 'ProviderMedicationBackup', 'provider_id');

        $relations['practices'] = array(self::MANY_MANY, 'Practice', 'provider_practice(practice_id, provider_id)');

        return $relations;
    }


    public function notSuspended()
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->getTableAlias(false) . '.suspended = 0',
        ]);
        return $this;
    }


    /**
     * Specify the display labels for attributes (name=>label)
     *
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['npi_id'] = Yii::t('app', 'NPI ID');
        $labels['credential_id'] = Yii::t('app', 'Credential');
        $labels['accepts_new_patients'] = Yii::t('app', 'Accepting New Patients?');
        $labels['brief_bio'] = Yii::t('app', 'Short Biography');
        $labels['bio'] = Yii::t('app', 'Long Biography');
        $labels['fees'] = Yii::t('app', 'Fees Information');
        $labels['docscore'] = Yii::t('app', 'DocPoints (tm)');
        $labels['avg_rating'] = Yii::t('app', 'Avg. Rating');
        $labels['fullProvider'] = 'Name';
        $labels['phone_number'] = 'Phone';
        $labels['zipcode'] = 'Zip';
        $labels['website'] = 'Site';
        $labels['vanity_specialty'] = 'Vanity Specialty';
        $labels['specialty'] = 'Spec.';
        $labels['state'] = 'ST Abbr.';
        $labels['impersonate_id'] = 'Acct.';
        $labels['verified'] = Yii::t('app', 'Verified');
        $labels['nasp_date'] = Yii::t('app', 'NASP Date');

        return $labels;
    }

    /**
     * Extend base search
     *
     * @return \CActiveDataProvider|boolean
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        //don't enable eager loading here!
        $arrProvider = Yii::app()->request->getParam('Provider');

        // has selected partner site id?
        $partnerSiteId = (!empty($arrProvider['partner']) ? $arrProvider['partner'] : '');

        if (isset($arrProvider['fullProvider']) && !empty($arrProvider['fullProvider'])) {
            $criteria->compare(
                'CONCAT(t.first_name, " ", t.last_name)',
                trim($_REQUEST['Provider']['fullProvider']),
                true
            );
            $this->fullProvider = $_REQUEST['Provider']['fullProvider'];
        } else {
            $this->fullProvider = '';
        }

        $criteria->compare('t.npi_id', $this->npi_id, false);
        $criteria->compare('gender', $this->gender, false);
        $criteria->compare('prefix', $this->prefix, true);
        $criteria->compare('t.first_name', $this->first_name, true);
        $criteria->compare('t.middle_name', $this->middle_name, true);
        $criteria->compare('t.last_name', $this->last_name, true);
        $criteria->compare('accepts_new_patients', $this->accepts_new_patients, false);
        $criteria->compare('year_began_practicing', $this->year_began_practicing, false);
        $criteria->compare('vanity_specialty', $this->vanity_specialty, false);
        $criteria->compare('docscore', $this->docscore, false);
        $criteria->compare('avg_rating', $this->avg_rating, false);
        $criteria->compare('is_test', $this->is_test);
        $criteria->compare('t.friendly_url', $this->friendly_url, true);
        $criteria->compare('featured', $this->featured, false);
        $criteria->compare('claimed', $this->claimed);
        $criteria->compare('t.suspended', $this->suspended);
        $criteria->compare('verified', $this->verified, false);
        $criteria->compare('nasp_date', $this->nasp_date, false);

        $criteria->compare('credential.title', $this->credential_id, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('zipcode', $this->zipcode, false);
        $criteria->compare('city.name', $this->city, true);
        $criteria->compare('state.abbreviation', $this->state, false);
        $criteria->compare('provider_practice.phone_number', $this->phone_number, true);
        $criteria->compare('provider_practice.email', $this->email, true);
        $criteria->compare('website', $this->website, true);
        if (!empty($partnerSiteId)) {
            $criteria->compare('account.partner_site_id', $partnerSiteId, true);
        }

        $dateFields = array('date_added', 'date_updated');

        foreach ($dateFields as $dateField) {

            switch ($this->$dateField) {

                case '1':
                    //today
                    $criteria->compare('DATE_FORMAT(t.' . $dateField . ', "%Y-%m-%d")', '=' . date('Y-m-d'), false);
                    break;
                case '2':
                    //yesterday
                    $criteria->compare(
                        'DATE_FORMAT(t.' . $dateField . ', "%Y-%m-%d")',
                        '=' . date('Y-m-d', strtotime('yesterday')),
                        false
                    );
                    break;
                case '3':
                    //this week
                    $criteria->compare('t.' . $dateField, '>=' . date('Y-m-d', strtotime('last monday')), false);
                    break;
                case '4':
                    //last week
                    $criteria->compare('t.' . $dateField, '>=' . date('Y-m-d', strtotime('monday last week')), false);
                    $criteria->compare('t.' . $dateField, '<=' . date('Y-m-d', strtotime('sunday last week')), false);
                    break;
                case '5':
                    //this month
                    $criteria->compare(
                        't.' . $dateField,
                        '>=' . date('Y-m-d', mktime(0, 0, 0, date("m"), 1, date("Y"))),
                        false
                    );
                    break;
                case '6':
                    //last month
                    $criteria->compare(
                        't.' . $dateField,
                        '>=' . date("Y-m-d", mktime(0, 0, 0, date("m") - 1, 1, date("Y"))),
                        false
                    );
                    $criteria->compare(
                        't.' . $dateField,
                        '<=' . date("Y-m-d", mktime(0, 0, 0, date("m"), -1, date("Y"))),
                        false
                    );
                    break;
                case '7':
                    //this quarter
                    $this_quarter = TimeComponent::getThisQuarter();
                    $criteria->compare('t.' . $dateField, '>=' . $this_quarter['start'], false);
                    break;
                case '8':
                    //last quarter
                    $last_quarter = TimeComponent::getLastQuarter();
                    $criteria->compare('t.' . $dateField, '>=' . date('Y-m-d', $last_quarter['start']), false);
                    $criteria->compare('t.' . $dateField, '<=' . date('Y-m-d', $last_quarter['end']), false);
                    break;
                case '9':
                    //this year
                    $criteria->compare('t.' . $dateField, '>=' . date('Y') . '-01-01', false);
                    break;
                case '10':
                    //last year
                    $dateF = (date('Y') - 1) . '-01-01';
                    $dateT = (date('Y') - 1) . '-12-31';
                    $criteria->compare('t.' . $dateField, '>=' . $dateF, false);
                    $criteria->compare('t.' . $dateField, '<=' . $dateT, false);
                    break;
                case '11':
                    //custom
                    if ($_REQUEST[$dateField . '_f'] != '' && $_REQUEST[$dateField . '_t'] != '') {
                        $dateF = explode('/', $_REQUEST[$dateField . '_f']);
                        $dateT = explode('/', $_REQUEST[$dateField . '_t']);
                        $dateF = $dateF[2] . '-' . $dateF[0] . '-' . $dateF[1];
                        $dateT = $dateT[2] . '-' . $dateT[0] . '-' . $dateT[1];
                        $criteria->compare('t.' . $dateField, '>=' . $dateF, false);
                        $criteria->compare('t.' . $dateField, '<=' . $dateT, false);
                    }
                    break;
                default:
                    break;
            }
        }

        if (
            $this->friendly_url == '' && $this->npi_id == '' && $this->gender == '' && $this->prefix == ''
            && $this->first_name == '' && $this->last_name == '' && $this->accepts_new_patients == ''
            && $this->year_began_practicing == '' && $this->docscore == '' && $this->avg_rating == ''
            && $this->featured == '' && $this->credential_id == '' && $this->address == '' && $this->zipcode == ''
            && $this->city == '' && $this->state == '' && $this->phone_number == '' && $this->email == ''
            && $this->website == '' && $this->date_added == '' && $this->date_updated == ''
            && trim($this->fullProvider) == '' && $this->suspended == ''
        ) {
            return false;
        }

        $criteria->select = 't.id, t.npi_id as npi_id, gender, credential_id, prefix, t.first_name, t.middle_name,
            t.last_name, accepts_new_patients, year_began_practicing, docscore, avg_rating, t.friendly_url,
            t.date_added AS date_added, t.date_updated AS date_updated, account.date_signed_in, featured,
            credential.title AS credential_name, practice.website,
            IF(practice.address_full IS NOT NULL, practice.address_full,
                IF(practice.address_2 IS NOT NULL, CONCAT(practice.address, " ",practice.address_2), practice.address) )
                AS address,
            location.zipcode, city.name AS city, state.abbreviation AS state,
            IF(provider_practice.phone_number != "", provider_practice.phone_number, practice.phone_number)
            AS phone_number,
            IF(provider_practice.email != "", provider_practice.email, practice.email) AS email,
            IF(provider_suspended.provider_id IS NOT NULL, true, false) AS suspended,
            IF(COUNT(DISTINCT(account_provider.account_id)) = 1, account_provider.account_id, 0) AS impersonate_id,
            IF(COUNT(DISTINCT(account_provider.account_id)) > 1, true, false) AS multiple_accounts';

        // join to account because need to filter by partner_site_id
        $joinAccount = ' LEFT JOIN account ON account.id = account_provider.account_id';

        $criteria->join = '
            LEFT JOIN provider_practice
                ON t.id = provider_practice.provider_id AND primary_location
            LEFT JOIN practice
                ON provider_practice.practice_id = practice.id
            LEFT JOIN location
                ON practice.location_id = location.id
            LEFT JOIN city
                ON location.city_id = city.id
            LEFT JOIN state
                ON city.state_id = state.id
            LEFT JOIN credential
                ON t.credential_id = credential.id
            LEFT JOIN provider_suspended
                ON t.id = provider_suspended.provider_id
            LEFT JOIN account_provider
                ON t.id = account_provider.provider_id' . $joinAccount;

        // uncomment this line because generate duplicate record on view
        $criteria->group = 't.id';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100),
            'sort' => array(
                'attributes' => array(
                    'fullProvider' => array(
                        'asc' => 't.first_name ASC, t.last_name ASC',
                        'desc' => 't.first_name DESC, t.last_name DESC',
                    ),
                    'zipcode' => array(
                        'asc' => 'location.zipcode ASC',
                        'desc' => 'location.zipcode DESC',
                    ),
                    'city' => array(
                        'asc' => 'city.name ASC',
                        'desc' => 'city.name DESC',
                    ),
                    'state' => array(
                        'asc' => 'state.abbreviation ASC',
                        'desc' => 'state.abbreviation DESC',
                    ),
                    '*',
                )
            )
        ));
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     *
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from provider if associated records exist in listing_site_provider_stats table
        $hasAssociatedRecords = ListingSiteProviderStats::model()->exists(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError("id", "Provider can't be deleted because it has listing site.");
            return false;
        }

        // avoid deleting from provider if it has been manually set as featured for a partner site
        $hasAssociatedRecords = PartnerSiteProviderFeatured::model()->exists(
            'provider_id=:providerId',
            array(':providerId' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError(
                'id',
                'This provider cannot be deleted because it has been manually set as featured for a partner site'
            );
            return false;
        }

        // avoid deleting from provider if it has been manually set as featured for a partner site
        $hasAssociatedRecords = ProviderPracticeGmb::model()->exists(
            'provider_id=:providerId',
            array(':providerId' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError(
                'id',
                'This provider cannot be deleted because it has GMB records'
            );
            return false;
        }

        // avoid deleting from provider if associated records exist in account_device_provider table
        $hasAssociatedRecords = AccountDeviceProvider::model()->exists(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError('id', 'This provider is linked to a ReviewHub');
            return false;
        }

        // avoid deleting from provider if associated records exist in organization_feature table
        $hasAssociatedRecords = OrganizationFeature::model()->exists(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError('id', 'This provider has features in an organization linked to him/her');
            return false;
        }

        // avoid deleting from provider if associated records exist in organization_history table
        $hasAssociatedRecords = OrganizationHistory::model()->exists(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError("id", "This provider is linked to an Organization's history");
            return false;
        }

        // avoid deleting from provider if associated records exist in organization_import_log table
        $hasAssociatedRecords = OrganizationImportLog::model()->exists(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError("id", "This provider is linked to an OrganizationImportLog");
            return false;
        }

        // avoid deleting from provider if associated records exist in scan table
        $hasAssociatedRecords = Scan::model()->exists(
            'provider_id = :provider_id',
            array(':provider_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError("id", "This provider have records in the scan table");
            return false;
        }

        // avoid deleting from provider if associated records exist in scan_provider table
        $hasAssociatedRecords = ScanProvider::model()->exists(
            'provider_id = :provider_id',
            array(':provider_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError("id", "This provider have records in the scan_provider table");
            return false;
        }

        // delete records from account_provider before deleting this provider
        $arrAccountProvider = AccountProvider::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrAccountProvider as $accountProvider) {
            if (!$accountProvider->delete()) {
                $this->addError("id", "Couldn't unlink account from providers.");
                return false;
            }
        }

        // delete records from account_provider_emr before deleting this provider
        $data = AccountProviderEmr::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($data as $d) {
            if (!$d->delete()) {
                $this->addError("id", "Couldn't delete account_provider_emr.");
                return false;
            }
        }

        // delete records from cpn_provider_installation before deleting this provider
        $data = CpnInstallationProvider::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($data as $d) {
            if (!$d->delete()) {
                $this->addError("id", "Couldn't delete cpn_provider_installation.");
                return false;
            }
        }

        // delete records from provider_award before deleting this provider
        $arrProviderAward = ProviderAward::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderAward as $providerAward) {
            if (!$providerAward->delete()) {
                $this->addError("id", "Couldn't delete provider's awards.");
                return false;
            }
        }

        // delete records from provider_certification before deleting this provider
        $arrProviderCertification = ProviderCertification::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderCertification as $providerCertification) {
            if (!$providerCertification->delete()) {
                $this->addError("id", "Couldn't delete certification");
                return false;
            }
        }

        // delete records from provider_details before deleting this provider
        $arrProviderDetails = ProviderDetails::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderDetails as $providerDetails) {
            if (!$providerDetails->delete()) {
                $this->addError("id", "Couldn't delete details.");
                return false;
            }
        }

        // delete records from provider_education before deleting this provider
        $arrProviderEducation = ProviderEducation::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderEducation as $providerEducation) {
            if (!$providerEducation->delete()) {
                $this->addError("id", "Couldn't delete education.");
                return false;
            }
        }

        // delete records from provider_experience before deleting this provider
        $arrProviderExperience = ProviderExperience::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderExperience as $providerExperience) {
            if (!$providerExperience->delete()) {
                $this->addError("id", "Couldn't delete experience.");
                return false;
            }
        }

        // delete records from provider_form before deleting this provider
        $arrProviderForm = ProviderForm::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderForm as $providerForm) {
            if (!$providerForm->delete()) {
                $this->addError("id", "Couldn't delete files.");
                return false;
            }
        }

        // delete records from provider_hospital before deleting this provider
        $arrProviderHospital = ProviderHospital::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderHospital as $providerHospital) {
            if (!$providerHospital->delete()) {
                $this->addError("id", "Couldn't delete hospitals.");
                return false;
            }
        }

        // delete records from provider_language before deleting this provider
        $arrProviderLanguage = ProviderLanguage::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderLanguage as $providerLanguage) {
            if (!$providerLanguage->delete()) {
                $this->addError("id", "Couldn't delete language.");
                return false;
            }
        }

        // delete records from provider_menbership before deleting this provider
        $arrProviderMembership = ProviderMembership::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderMembership as $providerMembership) {
            if (!$providerMembership->delete()) {
                $this->addError("id", "Couldn't delete memberships.");
                return false;
            }
        }

        // delete records from provider_payment_type before deleting this provider
        $arrProviderPaymentType = ProviderPaymentType::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderPaymentType as $providerPaymentType) {
            if (!$providerPaymentType->delete()) {
                $this->addError("id", "Couldn't delete payment types.");
                return false;
            }
        }

        // delete records from provider_photo before deleting this provider
        $arrProviderPhoto = ProviderPhoto::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderPhoto as $providerPhoto) {
            if (!$providerPhoto->delete()) {
                $this->addError("id", "Couldn't delete photos.");
                return false;
            }
        }

        // delete records from provider_population before deleting this provider
        $arrProviderPopulation = ProviderPopulation::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderPopulation as $providerPopulation) {
            if (!$providerPopulation->delete()) {
                $this->addError("id", "Couldn't delete treated populations.");
                return false;
            }
        }

        // delete records from provider_practice_insurance before deleting this provider
        $arrProviderPracticeInsurance = ProviderPracticeInsurance::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderPracticeInsurance as $providerPracticeInsurance) {
            if (!$providerPracticeInsurance->delete()) {
                $this->addError("id", "Couldn't delete Provider Practice Insurances.");
                return false;
            }
        }

        // delete records from provider_insurance before deleting this provider
        $arrProviderInsurance = ProviderInsurance::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderInsurance as $providerInsurance) {
            if (!$providerInsurance->delete()) {
                $this->addError("id", "Couldn't delete Provider Insurances.");
                return false;
            }
        }

        // delete records from provider_practice before deleting this provider
        $arrProviderPractice = ProviderPractice::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderPractice as $providerPractice) {
            if (!$providerPractice->delete()) {
                $this->addError("id", "Couldn't unlink practices.");
                return false;
            }
        }

        // delete records from provider_practice_gp before deleting this provider
        $arrProviderPractice = ProviderPracticeGp::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderPractice as $providerPractice) {
            if (!$providerPractice->delete()) {
                $this->addError("id", "Couldn't remove provider_practice_gp record.");
                return false;
            }
        }

        // delete records from provider_publication before deleting this provider
        $arrProviderPublication = ProviderPublication::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrProviderPublication as $providerPublication) {
            if (!$providerPublication->delete()) {
                $this->addError("id", "Couldn't delete publications.");
                return false;
            }
        }

        // delete records from provider_rating before deleting this provider
        $arrProviderRating = ProviderRating::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderRating as $providerRating) {
            if (!$providerRating->delete()) {
                $this->addError("id", "Couldn't delete reviews");
                return false;
            }
        }

        // delete records from provider_residency before deleting this provider
        $arrProviderResidency = ProviderResidency::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderResidency as $providerResidency) {
            if (!$providerResidency->delete()) {
                $this->addError("id", "Couldn't delete residencies.");
                return false;
            }
        }

        // delete records from provider_schedule before deleting this provider
        $arrProviderSchedule = ProviderSchedule::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderSchedule as $providerSchedule) {
            if (!$providerSchedule->delete()) {
                $this->addError("id", "Couldn't delete schedule.");
                return false;
            }
        }

        // delete records from provider_specialty before deleting this provider
        $arrProviderSpecialty = ProviderSpecialty::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderSpecialty as $providerSpecialty) {
            if (!$providerSpecialty->delete()) {
                $this->addError("id", "Couldn't delete specialty.");
                return false;
            }
        }

        // delete records from provider_stats before deleting this provider
        $arrProviderStats = ProviderStats::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderStats as $providerStats) {
            if (!$providerStats->delete()) {
                $this->addError("id", "Couldn't delete stats.");
                return false;
            }
        }

        // delete records from provider_stats_report before deleting this provider
        $arrProviderStatsReport = ProviderStatsReport::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderStatsReport as $providerStatsReport) {
            if (!$providerStatsReport->delete()) {
                $this->addError("id", "Couldn't delete stats");
                return false;
            }
        }

        // delete records from provider_suspended before deleting this provider
        $arrProviderSuspended = ProviderSuspended::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderSuspended as $providerSuspended) {
            if (!$providerSuspended->delete()) {
                $this->addError("id", "Couldn't un-suspend");
                return false;
            }
        }

        // delete records from provider_treatment before deleting this provider
        $arrProviderTreatment = ProviderTreatment::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderTreatment as $providerTreatment) {
            if (!$providerTreatment->delete()) {
                $this->addError("id", "Couldn't delete treatments");
                return false;
            }
        }

        // delete records from provider_treatment_performed before deleting this provider
        $arrProviderTreatmentPerformed = ProviderTreatmentPerformed::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderTreatmentPerformed as $providerTreatmentPerformed) {
            if (!$providerTreatmentPerformed->delete()) {
                $this->addError("id", "Couldn't delete treatments.");
                return false;
            }
        }

        // delete records from provider_url_history before deleting this provider
        $arrProviderUrlHistory = ProviderUrlHistory::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderUrlHistory as $providerUrlHistory) {
            if (!$providerUrlHistory->delete()) {
                $this->addError("id", "Couldn't delete URL history.");
                return false;
            }
        }

        // delete records from provider_video before deleting this provider
        $arrProviderVideo = ProviderVideo::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrProviderVideo as $providerVideo) {
            if (!$providerVideo->delete()) {
                $this->addError("id", "Couldn't delete videos.");
                return false;
            }
        }

        // delete records from provider_scheduling_mail before deleting this provider
        $arrSchedulingMail = SchedulingMail::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($arrSchedulingMail as $schedulingMail) {
            if (!$schedulingMail->delete()) {
                $this->addError("id", "Couldn't delete appointments.");
                return false;
            }
        }

        // delete records from provider_scheduling_mail before deleting this provider
        $data = Appointment::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->id)
        );
        foreach ($data as $d) {
            if (!$d->delete()) {
                $this->addError("id", "Couldn't delete appointments.");
                return false;
            }
        }

        $arrTwilioNumbers = TwilioNumber::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $this->id)
        );
        foreach ($arrTwilioNumbers as $twilioNumber) {
            if (!$twilioNumber->delete()) {
                $this->addError("id", "Couldn't delete phone number.");
                return false;
            }
        }

        // delete records from provider_patient before deleting this provider
        $arrProviderPatient = ProviderPatient::model()->findAll(
            'provider_id=:providerId',
            array(':providerId' => $this->id)
        );
        foreach ($arrProviderPatient as $providerPatient) {
            if (!$providerPatient->delete()) {
                $this->addError("id", "Couldn't delete patients.");
                return false;
            }
        }

        // delete records from provider_practice_listing before deleting this provider
        $arrProviderPracticeListing = ProviderPracticeListing::model()->findAll(
            'provider_id = :providerId',
            array(":providerId" => $this->id)
        );
        foreach ($arrProviderPracticeListing as $providerPracticeListing) {
            if (!$providerPracticeListing->delete()) {
                $this->addError("id", "Couldn't delete links");
                return false;
            }
        }

        // delete records from partner_site_params before deleting this provider
        $arrPartnerSiteParams = PartnerSiteParams::model()->findAll(
            'provider_id=:providerId',
            array(':providerId' => $this->id)
        );
        foreach ($arrPartnerSiteParams as $partnerSiteParam) {
            if (!$partnerSiteParam->delete()) {
                $this->addError("id", "Couldn't delete from Partner Site Param.");
                return false;
            }
        }

        // delete records from provider_practice_post before deleting this provider
        $arrPosts = ProviderPracticePost::model()->findAll(
            'provider_id=:providerId',
            array(':providerId' => $this->id)
        );
        foreach ($arrPosts as $post) {
            if (!$post->delete()) {
                $this->addError('id', 'This provider cannot be deleted because posts are attached to it.');
                return false;
            }
        }

        // delete records from account_details before deleting this provider
        $arrDetails = AccountDetails::model()->findAll(
            'claimed_provider_id=:providerId',
            array(':providerId' => $this->id)
        );
        foreach ($arrDetails as $detail) {
            $detail->claimed_provider_id = null;
            if (!$detail->save()) {
                $this->addError('id', 'This provider cannot be deleted because accounts are attached to it.');
                return false;
            }
        }

        // delete records from provider_medication before deleting this provider
        $arrDetails = ProviderMedication::model()->findAll(
            'provider_id=:providerId',
            array(':providerId' => $this->id)
        );
        foreach ($arrDetails as $detail) {
            if (!$detail->delete()) {
                $this->addError('id', 'This provider cannot be deleted because medications are attached to it.');
                return false;
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Return the provider address and country when it is no US
     *
     * @param array $location
     * @return string
     */
    public static function formatProviderLocation($location)
    {
        $cityName = str_replace(' ', '&nbsp;', trim($location['city_name']));
        $abbreviation = str_replace(' ', '&nbsp;', trim($location['abbreviation']));
        $zipCode = str_replace(' ', '&nbsp;', trim($location['zipcode']));
        $countryName = '';

        if (strpos($abbreviation, '_')) {
            $sql = sprintf(
                "SELECT SQL_NO_CACHE country.name
                FROM state
                INNER JOIN country ON state.country_id=country.id
                WHERE abbreviation = '%s';",
                $abbreviation
            );
            $countryName = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
            if (!empty($countryName)) {
                $countryName = ',&nbsp;' . $countryName;
            }
            $abbreviation = explode('_', $abbreviation);
            $abbreviation = $abbreviation[1];
        }

        return $cityName . ',&nbsp;' . $abbreviation . ',&nbsp;' . $zipCode . $countryName;
    }

    /**
     * Claim process: check by npi
     *
     * @param int $npiId
     * @param string $lastName
     * @return json
     */
    public static function claimCheckByNpi($npiId, $lastName)
    {
        if (!Provider::isNPIFormatValid($npiId)) {
            return array(
                'success' => false,
                'error' => true,
                'data' => 'Please enter a valid NPI number.',
            );
        }
        if (empty($lastName)) {
            return array(
                'success' => false,
                'error' => true,
                'data' => 'Please enter the provider last name.',
            );
        }

        $provider = Provider::model()->findByNPI($npiId, $lastName);

        if ($provider) {
            $accountId = 0;
            if (php_sapi_name() != 'cli') {
                $accountId = Yii::app()->user->id;
            }
            $providerPractices = ProviderPractice::createComboPracticesClaimProvider($provider->id, $accountId);

            $output = array(
                'success' => true,
                'found' => true,
                'provider' => array(
                    'id' => $provider->id,
                    'gender' => $provider->gender,
                ),
                'practices' => $providerPractices,
            );
        } else {
            $output = array(
                'success' => true,
                'found' => false,
            );
        }
        return $output;
    }

    /**
     * Save the claim process
     *
     * @param array $data
     * @return array
     */
    public static function claimSave($data = null)
    {

        $results['success'] = true;
        $results['error'] = "";
        $results['data'] = array();
        $results['data']['validateWith'] = '';

        if (empty($data)) {
            $results['success'] = false;
            $results['http_response_code'] = 200;
            $results['error'] = "EMPTY_DATA";
            $results['data'] = "You are sending no data.";
            return $results;
        }

        $accountId = $data['accountId'];
        $status = 'ok';
        $arrMsgs = $arrPractice = array();
        $providerId = null;

        // validate data for both methods
        $data['email'] = StringComponent::validateEmail($data['email']);
        if (empty($data['email'])) {
            $status = 'error';
            $arrMsgs[] = 'Please enter an e-mail address.';
        }

        //
        // Do we ha to Adding provider by npi
        //
        if ($data['validateWith'] == 'npi') {
            // Yes
            // validate data input for NPI methods
            if (!Provider::isNPIFormatValid($data['npiId'])) {
                $status = 'error';
                $arrMsgs[] = 'Please enter a valid NPI number.';
            }
            if (empty($data['lastName'])) {
                $status = 'error';
                $arrMsgs[] = 'Please enter the provider last name.';
            }

            if ($status != 'ok') {
                $results['success'] = false;
                $results['error'] = "PROVIDER_CLAIM_ERRORS";
                $results['data'] = $arrMsgs;
                return $results;
            }

            // find provider
            $arrProvider = Provider::model()->findByNPI($data['npiId'], $data['lastName'], $data['countryId']);

            if (empty($arrProvider->id)) {
                // profile was not found or the npi didn't match
                // Should we switch to ManualValidation way
                $data['validateWith'] = 'mv';
            }

            // Still Adding by npi?
            if ($data['validateWith'] == 'npi') {
                $providerId = $arrProvider->id;
                $data['providerId'] = $providerId;
                // Yes

                if ($data['practiceId'] > 0) {
                    // Find selected practice
                    $practice = Practice::model()->findByPk($data['practiceId']);
                    if (!$practice) {
                        $status = 'error';
                        $arrMsgs[] = 'The Practice does not exists.';
                    }
                } else {
                    // Does this provider have a practice linked as primaryPractice?
                    $providerPractice = ProviderPractice::model()->find(
                        "provider_id=:provider_id AND primary_location=1",
                        array(
                            ":provider_id" => $data['providerId']
                        )
                    );
                    if (!empty($providerPractice->practice_id)) {
                        // Yes, is the primaryPractice for this provider

                        // This practice is unclaimed?
                        $accountPractice = AccountPractice::model()->find(
                            "practice_id = :practice_id",
                            array(":practice_id" => $providerPractice->practice_id)
                        );
                        if (empty($accountPractice)) {
                            // Yes is unclaimed, so I can added to my account
                            $data['practiceId'] = $providerPractice->practice_id;
                        }
                    }
                }


                if ($status != 'ok') {
                    $results['success'] = false;
                    $results['error'] = "PROVIDER_CLAIM_ERRORS";
                    $results['data'] = $arrMsgs;
                    return $results;
                }

                // OK let´s go to save claim
                if ($data['selectedGender'] != $arrProvider['gender']) {
                    // if select different gender update provider gender
                    $thisProvider = $arrProvider;
                    $thisProvider->gender = $data['selectedGender'];
                    $thisProvider->save();
                }

                // if the account belongs to a partner site
                $account = Account::model()->findByPk($accountId);
                if (!empty($account->partner_site_id)) {
                    $providerPartner = Provider::model()->findByPk($providerId);
                    $providerPartner->partner_site_id = $account->partner_site_id;
                    $providerPartner->save();
                }

                // create new account<->provider
                AccountProvider::associateAccountProvider($accountId, $providerId);

                // Update providerDetails
                $providerDetails = ProviderDetails::model()->find(
                    'provider_id = :provider_id',
                    array(':provider_id' => $data['providerId'])
                );
                if (!$providerDetails) {
                    $providerDetails = new ProviderDetails;
                    $providerDetails->provider_id = $data['providerId'];
                    $providerDetails->schedule_mail_enabled = 1;
                    $needToSaveEmr = 1;

                    // check if enableAppointmentrequest value is sent
                    if (isset($data['enableAppointmentRequest'])) {

                        $providerDetails->schedule_mail_enabled =$data['enableAppointmentRequest'];
                        $needToSaveEmr = $data['enableAppointmentRequest'];

                    }

                    $providerDetails->email = $data['email'];
                    $providerDetails->save();
                } else {
                    $providerDetails->schedule_mail_enabled = 1;
                    $needToSaveEmr = 1;

                    // check if enableAppointmentrequest value is sent
                        if (isset($data['enableAppointmentRequest'])) {

                            $providerDetails->schedule_mail_enabled =$data['enableAppointmentRequest'];
                            $needToSaveEmr = $data['enableAppointmentRequest'];
                        }

                        $providerDetails->email = $data['email'];
                        $providerDetails->update();
                    }
                // by default enable scheduling & creating account_emr

                if ($data['practiceId'] > 0) {

                    // My account is linked with this practice ?
                    $haveThisPractice = AccountPractice::model()->find(
                        'practice_id=:practiceId AND account_id=:accountId',
                        array(':practiceId' => $data['practiceId'], ':accountId' => $accountId)
                    );
                    if (!$haveThisPractice) {
                        $addAccountPractice = new AccountPractice;
                        $addAccountPractice->practice_id = $data['practiceId'];
                        $addAccountPractice->account_id = $accountId;
                        $addAccountPractice->save();
                    }

                    // provider account is linked to this practice?
                    $providerPracticeExists = ProviderPractice::model()->find(
                        'provider_id=:provider_id AND practice_id=:practice_id',
                        array(':provider_id' => $data['providerId'], ':practice_id' => $data['practiceId'])
                    );
                    if (!$providerPracticeExists) {
                        $addProviderPractice = new ProviderPractice;
                        $addProviderPractice->provider_id = $data['providerId'];
                        $addProviderPractice->practice_id = $data['practiceId'];
                        $addProviderPractice->primary_location = 1;
                        $addProviderPractice->save();
                    }
                }

                $payingClient = Account::belongsToOrganization($accountId);

                if (!$payingClient) {
                    $arrAccount = Account::model()->findByPk($accountId);
                    if ($arrAccount->email != '') {
                        // automatic claim confirmation
                        MailComponent::automaticSignupApproved($accountId, $data['providerId']);
                    }
                }

                // The provider claimed have to inheritPracticeSchedules?
                $practiceId = !empty($data['practiceId']) ? $data['practiceId'] : null;

                if ($needToSaveEmr) {
                    // Sync EMR data, to allow booking
                    ProviderPracticeSchedule::inheritPracticeSchedules($providerId, $practiceId);
                    Yii::app()->params['emr_importation_disabled'] = null;
                    AccountEmr::importAccountEmr($accountId);
                } else {
                    Yii::app()->params['emr_importation_disabled'] = null;
                    AccountEmr::accountEmrData($accountId, $needToSaveEmr);
                }

                $results['success'] = true;
                $results['http_response_code'] = 200;
                $results['error'] = "";
                $results['data'] = $arrProvider->attributes;
            }
        }

        //
        // Do we have to added by Manual Validation methods
        //
        if ($data['validateWith'] == 'mv') {
            // Yes

            if (!empty($data['npiId']) && !Provider::isNPIFormatValid($data['npiId'])) {
                // validate data input for NPI methods
                $status = 'error';
                $arrMsgs[] = 'Please enter a valid NPI number.';
            }

            if (empty($data['lastName'])) {
                $status = 'error';
                $arrMsgs[] = "Please enter the provider&#39;s last name.";
            }
            if (empty($data['firstName'])) {
                $status = 'error';
                $arrMsgs[] = "Please enter the provider&#39;s first name.";
            }
            if ($status != 'ok') {
                $results['success'] = false;
                $results['error'] = "PROVIDER_CLAIM_ERRORS";
                $results['data'] = $arrMsgs;
                return $results;
            }

            // location
            if (!isset($data['location_id'])) {
                $arrAccount = Account::model()->findByPk($accountId);
                $data['location_id'] = $arrAccount->location_id;
            }

            // Manual Validation
            $manualValidation = new ManualValidation();
            $manualValidation->account_id = $accountId;
            $manualValidation->provider_id = $data['providerId'];

            // Practice and location
            $practiceName = '';
            if ($data['practiceId']) {
                $manualValidation->practice_id = $data['practiceId'];
                $arrPractice = Practice::model()->findByPk($data['practiceId']);
                $practiceName = $arrPractice->name;
            }

            $manualValidation->first_name = $data['firstName'];
            $manualValidation->npi_number = Common::get($data, 'npiId');
            $manualValidation->last_name = $data['lastName'];
            $manualValidation->gender = $data['selectedGender'];
            $manualValidation->specialty_id = Common::get($data, 'specialty_id');
            $manualValidation->sub_specialty_id = Common::get($data, 'sub_specialty_id');
            $manualValidation->location_id = $data['location_id'];
            $manualValidation->email = $data['email'];
            $manualValidation->status = "P";
            $manualValidation->validation_type = "Provider";
            $manualValidation->status_comments = ($data['providerId'] > 0
                ? 'Provider requested NPI validation which failed'
                : 'Provider requested manual review');
            $manualValidation->practice_name = $practiceName;
            $manualValidation->date_added = date('Y-m-d H:i:s');
            $manualValidation->date_updated = date('Y-m-d H:i:s');

            $manualValidation->save();
            $belongsToOrganization = Account::belongsToOrganization($accountId, true, true);
            //manual validation pending email
            if ($manualValidation->email != '' && !$belongsToOrganization) {
                MailComponent::manualValidationPending(
                    $manualValidation->id,
                    $manualValidation->provider_id,
                    $manualValidation->account_id
                );
            }
            $results['success'] = true;
            $results['error'] = "";
            $results['data'] = (array)$manualValidation->attributes;
        }
        $results['data']['validateWith'] = $data['validateWith'];
        return $results;
    }

    /**
     * Returns the primary practice of the givven provider
     *
     * @param integer $providerId
     * @param bool $firstAsPrimary
     * @param integer $ownerAccountId
     *
     * @return integer $practiceId
     */
    public static function getPrimaryPractice($providerId = 0, $firstAsPrimary = true, $ownerAccountId = null)
    {
        if ($providerId == 0) {
            return null;
        }

        // Get all practices related to this provider
        if (empty($ownerAccountId)) {
            $sql = sprintf(
                "SELECT provider_practice.practice_id, primary_location, rank_order
                FROM provider_practice
                WHERE provider_practice.provider_id = %d
                GROUP BY provider_practice.practice_id
                ORDER BY primary_location DESC, rank_order ASC;",
                $providerId
            );
        } else {
            $sql = sprintf(
                "SELECT provider_practice.practice_id, primary_location, rank_order
                FROM provider_practice
                INNER JOIN account_provider ON provider_practice.provider_id = account_provider.provider_id
                INNER JOIN account_practice ON provider_practice.practice_id = account_practice.practice_id
                WHERE account_practice.account_id = %d
                AND provider_practice.provider_id = %d
                GROUP BY provider_practice.practice_id
                ORDER BY primary_location DESC, rank_order ASC;",
                $ownerAccountId,
                $providerId
            );
        }

        $data = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();

        if (empty($data)) {
            return null;
        }

        $practiceId = 0;

        foreach ($data as $v) {

            if ($practiceId == 0 && $firstAsPrimary) {
                $practiceId = $v['practice_id'];
            }
            if ($v['primary_location'] == 1) {
                $practiceId = $v['practice_id'];
                break;
            }
        }

        return $practiceId;
    }

    /**
     * Get the primary practice of the current provider
     *
     * @return Practice
     */
    public function primaryPractice()
    {
        return Practice::model()
            ->with('providerPractices')
            ->with('twilioNumbers')
            ->find(
                'providerPractices.provider_id = :provider_id AND providerPractices.primary_location = 1',
                [':provider_id' => $this->id]
            );
    }

    /**
     * Returns true if the given NPI appears to be of a valid format
     * (this just checks format, not whether we have that NPI or not)
     *
     * @param string $npi
     * @return boolean
     */
    public static function isNPIFormatValid($npi)
    {
        return preg_match('/\d{10}/', $npi) ? true : false;
    }

    /**
     * Validate format and prevent duplicate
     */
    public function validateNPI()
    {
        $this->npi_id = trim($this->npi_id);

        if (empty($this->npi_id)) {
            $this->npi_id = null;
            return true;
        }

        $isValidFormat = $this->isNPIFormatValid($this->npi_id);

        if ($isValidFormat) {

            if (strlen($this->npi_id) > 10) {
                $this->addError('npi_id', 'NPI is too long (maximum is 10 characters).');
                return false;
            } elseif ($this->isNewRecord || (!$this->id > 0)) {
                // check if npi is unique
                if (Provider::model()->exists('npi_id=:npiId', array(':npiId' => $this->npi_id))) {
                    $this->addError('npi_id', 'This NPI number is already used.');
                    return false;
                }
            } else {
                // check that npi_id doesnt exist for other provider
                if (Provider::model()->exists(
                    'npi_id=:npiId AND id!=:id',
                    array(':npiId' => $this->npi_id, ':id' => $this->id)
                )) {
                    $this->addError('npi_id', 'This NPI number is already used.');
                    return false;
                }
            }
        } else {
            $this->addError('npi_id', 'Incorrect format of NPI.');
            return false;
        }

        return true;
    }

    /**
     * Sanitize user input and generate friendly_url before saving, create credential if necessary
     *
     * @return boolean
     */
    public function beforeSave()
    {

        $postProvider = Yii::app()->request->getPost('Provider', null);
        if (!empty($postProvider)) {
            $this->attributes = $postProvider;
        }

        $this->normalizeAttributes();

        // validate NPI
        if ($this->scenario != 'optionalCredentials' && !$this->validateNPI()) {
            return false;
        }

        // if no credential set, set based on credential_id_save POST param
        $validCredential = ($this->credential_id != "0" && $this->credential_id)
            || $this->scenario == 'optionalCredentials';

        if (!$validCredential) {
            $credential_id_save = Yii::app()->request->getParam('credential_id_save');

            if (empty($credential_id_save)) {
                $credential_id_save = Yii::app()->request->getParam('credential_id_lookup');
            }

            if (empty($credential_id_save)) {
                //no credentials yet -- check credential_id (used on provider admin)
                $credential_id_save = Yii::app()->request->getParam('Provider');
                if (isset($credential_id_save['credential_id'])) {
                    $credential_id_save = $credential_id_save['credential_id'];
                }
            }

            if (!empty($credential_id_save)) {

                //check if already existed
                $credential = Credential::model()->cache(Yii::app()->params['cache_medium'])->find(
                    'title=:title',
                    array(':title' => $credential_id_save)
                );
                if (!empty($credential)) {
                    $this->credential_id = $credential->id;
                    $validCredential = true;
                } else {
                    $credential = new Credential;
                    $credential->title = $credential_id_save;
                    $credential->refered_as = $credential_id_save;
                    $credential->definition = $credential_id_save;
                    $credential->save();
                    $this->credential_id = $credential->id;
                    if ($this->credential_id > 0) {
                        $validCredential = true;
                    }
                }
            }
        }

        if (!$validCredential && $this->scenario != 'optionalCredentials') {
            $this->addError('credential_id', 'No credentials provided');
            return false;
        }


        if ($this->isNewRecord) {

            $this->suspended = 0;

            // set default docscore (it will get recomputed in afterSave()
            $this->docscore = Docscore::model()->getBaseDocScore(1);
            $this->date_added = date('Y-m-d H:i:s');
        } else {

            // URL changed; save URL history so redirects work properly
            $curr = self::findByPk($this->id);

            if ($curr->friendly_url != $this->friendly_url && !empty($curr->friendly_url)) {

                if (!ProviderUrlHistory::model()->exists(
                    'friendly_url=:friendly_url',
                    array(':friendly_url' => $curr->friendly_url)
                )) {
                    $puh = new ProviderUrlHistory();
                    $puh->provider_id = $this->id;
                    $puh->friendly_url = $curr->friendly_url;

                    if (!$puh->save()) {
                        Yii::log(
                            "Error saving URL history after friendly URL change: " . serialize($puh->getErrors(), true),
                            'error',
                            __METHOD__
                        );
                    }
                }
            }
            // update docscore (don't save since that will happen after this current save anyway)
            // $this->updateScore(true);
        }

        // set verified flag to default value
        if ($this->verified === null) {
            $this->verified = 0;
        }

        $this->date_updated = date('Y-m-d H:i:s');

        // set the display search order, weighedRating should be 0 for new providers
        $weighedRating = empty($this->id) ? 0 : self::getWeighedRating($this->id);
        $this->display_search_order = $this->calculateSearchOrder(
            $this->featured,
            $this->docscore,
            $weighedRating,
            $this->photo_path
        );
        return parent::beforeSave();
    }

    /**
     * Validate Friendly URL format and prevent duplicate
     *
     * @return bool
     */
    public function validateFriendlyURL()
    {
        $this->friendly_url = trim($this->friendly_url);

        // check friendly url is a valid string
        if (!preg_match('/^[\w-]+$/', $this->friendly_url)) {
            $this->addError('friendly_url', 'Only characters from A to Z, numbers and dashes (-) are accepted');
            return false;
        }

        //if we're modifying (not creating) a provider
        if ($this->id > 0) {

            // check if we've entered a friendly url
            if ($this->friendly_url == '') {
                $this->addError('friendly_url', 'Friendly URL cannot be blank');
                return false;
            }

            // check that friendly_url doesnt exist for other provider
            if (Provider::model()->exists(
                'friendly_url=:friendly_url AND id!=:id',
                array(':friendly_url' => $this->friendly_url, ':id' => $this->id)
            )) {
                $this->addError('friendly_url', 'This friendly URL is already used.');
                return false;
            }
        } else {
            // check that friendly_url doesnt exist
            if (Provider::model()->exists(
                'friendly_url=:friendly_url',
                array(':friendly_url' => $this->friendly_url)
            )) {
                $this->addError('friendly_url', 'This friendly URL is already used.');
                return false;
            }
        }

        return true;
    }

    /**
     * Sanitize input before validating
     *
     * @return mixed
     */
    public function beforeValidate()
    {

        // normalize various attributes that may be unset/null
        $this->normalizeAttributes();

        // validate NPI
        if ($this->scenario != 'optionalCredentials' && !$this->validateNPI()) {
            return false;
        }

        // validate FriendlyURL
        if (!$this->validateFriendlyURL()) {
            return false;
        }

        return parent::beforeValidate();
    }

    /**
     * For attributes set to null values, set them to how they should be set before saving (for use in validation and
     * before save; note that it's in both in the case of beforeSave(false) being called, meaning no validation is done)
     */
    public function normalizeAttributes()
    {

        // no credential should instead of credential '0' set
        if (!$this->credential_id) {
            $this->credential_id = "0";
        }

        // no prefix should mean prefix is blank
        if (!$this->prefix) {
            $this->prefix = "";
        }

        if (!$this->date_umd_updated || $this->date_umd_updated == '0000-00-00 00:00:00') {
            $this->date_umd_updated = null;
        }

        if (!$this->year_began_practicing) {
            $this->year_began_practicing = null;
        }

        // trim extra spaces
        $this->first_name = StringComponent::cleanseString($this->first_name);
        $this->middle_name = StringComponent::cleanseString($this->middle_name);
        $this->last_name = StringComponent::cleanseString($this->last_name);
        $this->suffix = StringComponent::cleanseString($this->suffix);

        $initialMiddleName = !empty($this->middle_name) ? substr($this->middle_name, 0, 1) . ' ' : '';
        $middleName = !empty($this->middle_name) ? $this->middle_name . ' ' : '';

        // set name-related denormalized fields based on normalized values
        $this->first_last = trim($this->first_name . ' ' . $this->last_name . ' ' . $this->suffix);
        $this->first_m_last = trim($this->first_name . ' ' . $initialMiddleName
            . $this->last_name . ' ' . $this->suffix);

        $this->first_middle_last = $this->first_name . ' ' . $middleName . $this->last_name . ' ' . $this->suffix;
        $this->first_middle_last = trim($this->first_middle_last);

        // create friendly url based on other fields set if not already set
        if (!$this->friendly_url) {

            $suffix = $this->prefix;
            $firstName = $this->first_name;
            $lastName = $this->last_name;

            $this->friendly_url = $this->getFriendlyUrl($suffix, $firstName, $lastName);
        }

        $this->additional_specialty_information = StringComponent::cleanseString(
            $this->additional_specialty_information
        );
        $this->vanity_specialty = StringComponent::cleanseString($this->vanity_specialty);
        $this->brief_bio = StringComponent::cleanseString($this->brief_bio);
        $this->bio = StringComponent::cleanseString($this->bio);
        $this->fees = StringComponent::cleanseString($this->fees);

        if (empty($this->npi_id) || $this->npi_id == ' ') {
            $this->npi_id = null;
        }
    }

    /**
     * Save provider Data
     *
     * @param array $postData
     * @param int $accountId
     * @return array
     */
    public static function saveProvider($postData = array(), $accountId = 0)
    {

        $results['success'] = false;
        $results['error'] = "EMPTY_POST";
        $results['data'] = "";

        if (empty($postData) || $accountId == 0) {
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        $providerId = !empty($postData['id']) ? $postData['id'] : 0;
        if ($providerId == 0) {
            $results['error'] = "PROVIDER_ID_ZERO";
            return $results;
        }

        // validate access
        $allowed = self::checkPermissions($providerId, $accountId);
        if (!$allowed) {
            $results['http_response_code'] = 403;
            $results['error'] = "ACCESS_DENIED";
            return $results;
        }

        $provider = Provider::model()->findByPk($providerId);
        if (!$provider) {
            $results['http_response_code'] = 422;
            $results['error'] = "PROVIDER_NOT_EXISTS";
            return $results;
        }

        // this fields must be ommited from post
        $prohibitedFields = array(
            'id',
            'first_last',
            'first_m_last',
            'first_middle_last',
            'npi_id',
            'docscore',
            'avg_rating',
            'friendly_url',
            'date_added',
            'date_updated',
            'umd_id',
            'date_umd_updated',
            'featured',
            'verified',
            'claimed',
            'partner_site_id',
            'is_test',
            'suspended'
        );

        $schema = Provider::model()->getSchema();
        foreach ($schema as $fieldName => $fieldType) {
            // if not prohibitted field
            if (!in_array($fieldName, $prohibitedFields) && isset($postData[$fieldName])) {
                // is posted?
                if ($fieldType == 'string') {
                    $value = trim($postData[$fieldName]);
                } elseif ($fieldType == 'email') {
                    $value = StringComponent::validateEmail($postData[$fieldName]);
                } elseif ($fieldType == 'integer') {
                    $value = (int)$postData[$fieldName];
                } else {
                    $value = $postData[$fieldName];
                }

                $provider->$fieldName = $value;
            }
        }

        if ($provider->year_began_practicing == 'yyyy' || $provider->year_began_practicing == 0) {
            unset($provider->year_began_practicing);
        }
        if (
            !empty($provider->year_began_practicing)
            &&
            ($provider->year_began_practicing < 1900 || $provider->year_began_practicing > date("Y"))
        ) {
            unset($provider->year_began_practicing);
        }

        if ($provider->validate()) {

            // update provider
            $provider->date_updated = date('Y-m-d H:i:s');

            // // upload photo ?
            // $resultUpload = File::upload($provider, "photo_path");
            // if (!$resultUpload['success']) {
            //      $results['error'] = "ERROR_UPLOADING_PHOTO";
            //      $results['data'] = $resultUpload;
            //      return $results;
            //  }
            //
            if ($provider->save()) {

                $auditSection = 'Provider #' . $provider->id . ' (' . $provider->first_middle_last . ')';
                AuditLog::create('U', $auditSection, 'provider', null, false);

                $results['success'] = true;
                $results['error'] = "";
                $results['data'] = (array)$provider->attributes;
                return $results;
            }

            $results['error'] = "ERROR_SAVING_PROVIDER";
            $results['data'] = $provider->getErrors();
            return $results;
        }

        $results['error'] = "ERROR_VALIDATING_PROVIDER";
        $results['data'] = $provider->getErrors();
        return $results;
    }

    /**
     * get date signed in
     *
     * @param int $providerId
     * @return string
     */
    public static function getDateSignedIn($providerId = 0)
    {
        if ($providerId > 0) {
            $sql = sprintf(
                "SELECT account.date_signed_in
                FROM account_provider
                INNER JOIN account
                    ON account.id = account_provider.account_id
                WHERE account_provider.provider_id =  %d
                ORDER BY account_provider.id;",
                $providerId
            );
            $arrTemp = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();

            if (!empty($arrTemp)) {
                return $arrTemp['date_signed_in'];
            }
        }
        return '';
    }

    /**
     * Build provider name for admin section
     *
     * @return string
     */
    public function getFullProvider()
    {

        if ($this->id <= 0) {
            return '';
        }
        return ($this->prefix != '' ? $this->prefix . ' ' : '')
            . addslashes($this->first_name . ' ' . $this->last_name);
    }

    /**
     * This is just so that we can remember fullProvider when filtering by it
     * in the provider admin view - see search() in:
     *
     * @param string $value
     * @return boolean
     * @see protected/models/_base/BaseProvider.php
     * and
     * @see protected/views/administration/provider/admin.php
     */
    public function setFullProvider($value)
    {
        $this->fullProvider = $value;
        return true;
    }

    /**
     * Format provider website if necessary
     *
     * @return string
     */
    public function getFormattedWebsite()
    {

        $website = $this->website;
        if ($website != '') {
            if (stristr($website, 'http') === false) {
                $website = 'http://' . $website;
            }
            return '<a href="' . $website . '" target="_blank"><span class="ui-icon ui-icon-newwin"></span></a>';
        }
        return '';
    }

    /**
     * returns specialty name and sub specialty name concatenated
     *
     * @param string $provider_id provider id
     * @return string
     *
     */
    public function getSpecialtySubspecialty($provider_id = null)
    {

        $trim = false;
        if ($this->id) {
            $trim = true;
            $provider_id = $this->id;
        }

        $sql = sprintf(
            "SELECT CONCAT(specialty.name, ' - ', sub_specialty.name)
            FROM provider_specialty
            INNER JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
            INNER JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
            WHERE provider_id = %d
            ORDER BY provider_specialty.primary_specialty
            LIMIT 1;",
            $provider_id
        );
        $specSubspec = Yii::app()->db->createCommand($sql)->queryScalar();
        if ($trim && strlen($specSubspec) > 20) {
            $specSubspec = '<span title="' . $specSubspec . '">' . substr($specSubspec, 0, 17) . '..</span>';
        }
        return $specSubspec;
    }

    /**
     * Get a concatenated list of specialty ids and names given a provider
     *
     * @param int $providerId
     * @return array
     */
    public static function getProviderSpecialties($providerId)
    {

        $cacheTime = Yii::app()->params['cache_short'];
        //check if this is one of the pdi clients that only has one specialty,
        //if that's the case we want to show the sub-specialty in the profile instead
        $sqlCountSpecs = sprintf("SELECT COUNT(1) FROM specialty;");
        $countSpecs = Yii::app()->db->cache($cacheTime)->createCommand($sqlCountSpecs)->queryScalar();

        $sqlSpec = sprintf(
            "SELECT SQL_NO_CACHE
            GROUP_CONCAT(id SEPARATOR \", \") AS specialties_ids,
            GROUP_CONCAT(name SEPARATOR \", \") AS specialties
            FROM(
                SELECT DISTINCT(specialty.id) AS id, %s AS name
                FROM provider_specialty

                INNER JOIN sub_specialty
                    ON provider_specialty.sub_specialty_id = sub_specialty.id

                INNER JOIN specialty
                    ON sub_specialty.specialty_id = specialty.id

                WHERE provider_specialty.provider_id = %d
                ORDER BY provider_specialty.primary_specialty
                LIMIT 5
            ) specialtiesSQ;",
            ($countSpecs == 1 ? "CONCAT(specialty.name, ' - ', sub_specialty.name)" : "specialty.name"),
            $providerId
        );
        return Yii::app()->db->cache($cacheTime)->createCommand($sqlSpec)->queryRow();
    }

    /**
     * Get a concatenated list of subspecialty ids and names given a provider
     *
     * @param int $providerId
     * @return array
     */
    public static function getProviderSubSpecialties($providerId)
    {

        $sqlSpec = sprintf(
            "SELECT SQL_NO_CACHE
            GROUP_CONCAT(id SEPARATOR \", \") AS sub_specialty_ids,
            GROUP_CONCAT(name SEPARATOR \", \") AS sub_specialties
            FROM(
                SELECT DISTINCT(sub_specialty.id), name
                FROM provider_specialty
                INNER JOIN sub_specialty
                    ON provider_specialty.sub_specialty_id = sub_specialty.id
                WHERE provider_specialty.provider_id = %d
                ORDER BY provider_specialty.primary_specialty
                LIMIT 5
            ) specialtiesSQ;",
            $providerId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sqlSpec)->queryRow();
    }

    /**
     * Show provider suggestions in autocomplete using last name and first name if present
     *
     * @param string $q
     * @param int $limit
     * @param string $firstName
     * @return array
     */
    public static function autocompleteByLastName($q = '', $limit = 10, $firstName = '')
    {

        $q = addslashes(trim($q));
        $includeFirstName = '';
        if (!empty($firstName) && $firstName != 'First Name') {
            $firstName = addslashes(trim($firstName));
            $includeFirstName = " AND provider.first_name LIKE '" . $firstName . "%'";
        }

        $sqlBase = "SELECT provider.id, first_name, last_name,
            CONCAT(IF(prefix = '', '', CONCAT(prefix, ' ')), first_name, ' ', last_name) AS provider_name,
            specialty.name AS specialty, city.name AS city, abbreviation AS state, location.zipcode,
            provider.friendly_url
            FROM provider
            INNER JOIN provider_specialty
                ON provider.id = provider_specialty.provider_id
            INNER JOIN sub_specialty
                ON sub_specialty.id = provider_specialty.sub_specialty_id
            INNER JOIN specialty
                ON specialty.id = sub_specialty.specialty_id
            INNER JOIN provider_practice
                ON provider.id = provider_practice.provider_id
            INNER JOIN practice
                ON provider_practice.practice_id = practice.id
            INNER JOIN location
                ON practice.location_id = location.id
            INNER JOIN city
                ON location.city_id = city.id
            INNER JOIN state
                ON city.state_id = state.id
            WHERE state.country_id = 1
            AND provider.localization_id = 1
            AND provider_practice.primary_location = 1
            AND provider.suspended = 0
            %s
            %s
            GROUP BY provider.id
            ORDER BY display_search_order, provider.id
            LIMIT %d;";
        $searchLastName = sprintf("AND provider.last_name LIKE '%%%s'", $q);
        $sql = sprintf($sqlBase, $searchLastName, $includeFirstName, $limit);

        $arrResult = Yii::app()->db->createCommand($sql)->queryAll();

        if (empty($arrResult)) {
            $searchLastName = sprintf("AND provider.last_name LIKE '%s%%'", $q);
            $sql = sprintf($sqlBase, $searchLastName, $includeFirstName, $limit);
            $arrResult = Yii::app()->db->createCommand($sql)->queryAll();
        }
        if (!empty($arrResult)) {
            return $arrResult;
        }
        return false;
    }

    /**
     * Returns the provider ids from a given first and last name, using just
     * exact matches or, if none found, prefix matches, else fuzzier matches
     *
     * @param string $q string with name (could be first, last, or both)
     * @return array
     */
    public static function findByQuery($q)
    {

        if (trim($q) != '') {
            // assume first name is all before last space, last name is all
            // after; if that's not the case, prefix match later will be used
            // to match (e.g. for cases like last name "Van Buren")

            $first = $last = '';
            $split = preg_split("/\s+(?=\S*+$)/", $q);
            $first = $split[0];
            if (count($split) > 1) {
                $last = $split[1];
            }

            // do look up by name, acknowledging that this function is smart
            // about trying different combinations for cases where a given word
            // is ambiguous about whether it's a first or last name
            return Provider::findByName($first, $last);
        }
        return false;
    }

    /**
     * Returns the provider ids from a given first and last name, using just
     * exact matches or, if none found, prefix matches, else fuzzier matches;
     * note: first + last will be tried together to account for cases
     * where names have spaces within them, e.g. "David Van Buren" or
     * "Mary Lee Kordes"
     *
     * @param string $sFirst first name
     * @param string $sLast last name
     * @return array
     */
    public static function findByName($sFirst = '', $sLast = '')
    {

        $sFirst = addslashes($sFirst);
        $sLast = addslashes($sLast);

        // get exact matches first (NOTE: we also get matches
        // with the given full word and an additional word
        // to account for middle name cases as well as
        // suffix cases like "Thomas Bender III" where the
        // last name is actually "Bender III" in the database
        if ($sFirst && $sLast) {
            $searchType = 'BOTH';
            $q = $sFirst . '% ' . $sLast;
            $condition = "(first_name = '" . $sFirst .
                "' OR first_name LIKE '" . $sFirst .
                " %') AND (last_name = '" .
                $sLast . "' OR last_name LIKE '" .
                $sLast . " %')";
        } elseif ($sFirst) {
            $searchType = 'FIRST';
            $q = $sFirst;
            $condition = "(first_name = '" . $sFirst .
                "' OR first_name LIKE '" . $sFirst . " %')";
        } else {
            $searchType = 'LAST';
            $q = $sLast;
            $condition = "(last_name = '" . $sLast .
                "' OR last_name LIKE '" . $sLast . " %')";
        }

        // look up exact matches
        $sql = sprintf(
            "SELECT id
            FROM provider
            WHERE %s;",
            $condition
        );

        $assocArray = Yii::app()->dbRO->createCommand($sql)->queryAll();

        $array = array();
        foreach ($assocArray as $v) {
            array_push($array, $v['id']);
        }

        // if any exact matches, just return those
        if (!empty($array) && is_array($array)) {
            return $array;
        }

        // no exact matches found, so look up prefix matches
        $condition = "first_name LIKE '" . $q . "%' OR last_name LIKE '" . $q
            . "%' OR CONCAT(first_name, ' ', last_name) LIKE '" . $q . "%'";
        if ($searchType == 'FIRST') {
            // first name only
            $condition = "first_name LIKE '" . $sFirst . "%'";
        } elseif ($searchType == 'LAST') {
            // last name only
            $condition = "last_name LIKE '" . $sLast . "%'";
        }

        $sql = sprintf(
            "SELECT id
            FROM provider
            WHERE %s;",
            $condition
        );

        $assocArray = Yii::app()->dbRO->createCommand($sql)->queryAll();

        $array = array();
        foreach ($assocArray as $v) {
            array_push($array, $v['id']);
        }

        if (count($array) > 0 && is_array($array)) {
            return $array;
        }

        // no results found, try a more fuzzy search
        $sql = "SELECT id
            FROM provider
            WHERE first_name LIKE '" . $q . "%'
            OR last_name LIKE '" . $q . "%'
            OR CONCAT(first_name, ' ', last_name) LIKE '" . $q . "%';";
        $assocArray = Yii::app()->dbRO->createCommand($sql)->queryAll();

        $array = array();
        foreach ($assocArray as $v) {
            array_push($array, $v['id']);
        }
        return $array;
    }

    /**
     * Get most of the details for a provider given his/her ID
     *
     * @param int $providerId
     * @param string $searchedLocationIds
     * @return array
     */
    public static function getDetails($providerId, $searchedLocationIds = '')
    {

        $practiceCondition = '';
        $order = '';
        if (trim($searchedLocationIds) != '' && trim($searchedLocationIds) != ',') {
            $practiceCondition = sprintf('AND practice.location_id IN (%s)', $searchedLocationIds);
            $order = 'ORDER BY primary_location DESC';
        }

        $sql = sprintf(
            "SELECT DISTINCT
            provider.*, CONCAT(IF(provider.prefix = '', '' , CONCAT(provider.prefix, '.')), ' ',
            provider.first_middle_last) AS provider, provider.first_middle_last AS provider_name,
            city.name AS city, state.abbreviation AS state, location.zipcode,
            practice.address, practice.phone_number, specialty_profile.id AS specialty_id,
            practice.id AS practice_id, credential.title AS credential_title,
            provider.nasp_date AS nasp_date,
            (
                SELECT GROUP_CONCAT(name_esp_sub SEPARATOR ',  ')
                FROM
                (
                    SELECT
                    DISTINCT(
                        CONCAT(specialty_profile.name, ' - ',sub_specialty_profile.name)
                    ) AS name_esp_sub
                    FROM provider_specialty AS provider_specialty_profile

                    LEFT JOIN sub_specialty AS sub_specialty_profile
                    ON sub_specialty_profile.id = provider_specialty_profile.sub_specialty_id

                    LEFT JOIN specialty AS specialty_profile
                    ON sub_specialty_profile.specialty_id = specialty_profile.id
                    WHERE provider_id = %d
                    ORDER BY provider_specialty_profile.primary_specialty
                )
                specialtiesSQ
            ) AS specialties
            FROM provider
            LEFT JOIN provider_practice
                ON provider_practice.provider_id = provider.id
                AND primary_location
            LEFT JOIN practice
                ON practice.id = provider_practice.practice_id
            LEFT JOIN location
                ON practice.location_id = location.id
            LEFT JOIN city
                ON location.city_id = city.id
            LEFT JOIN state
                ON city.state_id = state.id

            LEFT JOIN provider_specialty AS provider_specialty_profile
                ON provider_specialty_profile.provider_id = provider.id

            LEFT JOIN sub_specialty AS sub_specialty_profile
                ON sub_specialty_profile.id = provider_specialty_profile.sub_specialty_id

            LEFT JOIN specialty AS specialty_profile
                ON sub_specialty_profile.specialty_id = specialty_profile.id

            LEFT JOIN credential
                ON provider.credential_id = credential.id
            WHERE provider.id = '%s'
            GROUP BY provider.id
            %s
            %s
            LIMIT 1;",
            $providerId,
            $providerId,
            $practiceCondition,
            $order
        );

        return Yii::app()->dbRO->createCommand($sql)->queryAll();
    }

    /**
     * Get additional details for a provider from provider_details
     *
     * @param int $providerId
     * @return array
     */
    public function getExtraDetails($providerId)
    {
        $sql = sprintf(
            "SELECT philosophy.name AS philosophy, IF(has_financing_plans IS NULL, 'Unknown', (IF
            (has_financing_plans = 0, 'No', 'Yes'))) AS has_financing_plans, IF(has_sliding_billing IS NULL, 'Unknown',
            (IF (has_sliding_billing = 0, 'No', 'Yes'))) AS has_sliding_billing,
            IF(has_medications IS NULL, 'Unknown', (IF (has_medications = 0, 'No', 'Yes'))) AS has_medications,
            no_insurance_reason, schedule_mail_enabled
            FROM provider_details
            LEFT JOIN philosophy
                ON provider_details.philosophy_id = philosophy.id
            WHERE provider_id = %d
            LIMIT 1;",
            $providerId
        );
        return Yii::app()->dbRO->createCommand($sql)->queryRow();
    }

    /**
     * Get populations a provider attends
     *
     * @param int $providerId
     * @return array
     */
    public function getPopulations($providerId)
    {
        $sql = sprintf(
            "SELECT name
            FROM provider_population
            INNER JOIN population
                ON provider_population.population_id = population.id
            WHERE provider_id = %d
            ORDER BY name;",
            $providerId
        );
        return Yii::app()->dbRO->createCommand($sql)->queryAll();
    }

    /**
     * Get the types of payment that a provider accepts
     *
     * @param int $providerId
     * @return array
     */
    public function getPaymentTypes($providerId)
    {
        $sql = sprintf(
            "SELECT name, class_name
            FROM provider_payment_type
            INNER JOIN payment_type
                ON provider_payment_type.payment_type_id = payment_type.id
            WHERE provider_id = %d;",
            $providerId
        );
        $paymentTypes = Yii::app()->dbRO->createCommand($sql)->queryAll();
        foreach ($paymentTypes as $ind => $value) {
            if ($value['name'] == 'Other') {
                array_push($paymentTypes, $paymentTypes[$ind]);
                unset($paymentTypes[$ind]);
            }
        }
        return $paymentTypes;
    }

    /**
     * Get provider residencies
     *
     * @param int $providerId
     * @return array
     */
    public function getResidency($providerId)
    {
        $sql = sprintf(
            "SELECT residency_type.name AS residency_type, hospital_name, area_of_focus,
            year_completed
            FROM provider_residency
            LEFT JOIN residency_type
                ON provider_residency.residency_type_id = residency_type.id
            WHERE provider_id = %d;",
            $providerId
        );
        return Yii::app()->dbRO->createCommand($sql)->queryAll();
    }

    /**
     * Get a list of the publications made by a provider
     *
     * @param int $providerId
     * @return array
     */
    public function getPublications($providerId)
    {
        $sql = sprintf(
            'SELECT
            CASE publication_type_id
            WHEN 1 THEN "Book"
            WHEN 2 THEN "Dissertation"
            WHEN 3 THEN "Magazine Article"
            WHEN 4 THEN "Medical/Scholarly Journal Article"
            WHEN 5 THEN "Online Article"
            WHEN 6 THEN "Text Book"
            WHEN 7 THEN "Other Publication"
            END AS publication_type,
            publisher_name, title, published_month, published_year
            FROM provider_publication
            WHERE provider_id = "%s"
            ORDER BY published_year DESC, published_month DESC;',
            $providerId
        );
        return Yii::app()->dbRO->createCommand($sql)->queryAll();
    }

    /**
     * Get a provider object given a NPI ID and a last name
     *
     * @param int $npiId
     * @param string $lastName
     * @param int $countryId
     * @return Provider
     */
    public function findByNPI($npiId, $lastName, $countryId = 1)
    {
        return Provider::model()->find(
            'npi_id=:npi_id AND last_name=:last_name AND country_id = :country_id',
            array(
                ':npi_id' => $npiId,
                ':last_name' => $lastName,
                ':country_id' => $countryId
            )
        );
    }

    /**
     * Get a provider object given a NPI ID and a last name
     *
     * @param int $npiId
     * @param string $lastName
     * @param int $countryId
     * @return Provider
     */
    public function findByNPIOnly($npiId, $countryId = 1)
    {
        return Provider::model()->find(
            'npi_id=:npi_id AND country_id = :country_id',
            array(
                ':npi_id' => $npiId,
                ':country_id' => $countryId
            )
        );
    }

    /**
     * Get a list of the social links added by a provider
     *
     * @param int $providerId
     * @return array
     */
    public function getSocialLinks($providerId)
    {
        $sql = sprintf(
            'SELECT listing_site.name, url AS link
            FROM provider_practice_listing
            INNER JOIN listing_type
                ON provider_practice_listing.listing_type_id = listing_type.id
            INNER JOIN listing_site
                ON listing_type.listing_site_id = listing_site.id
            WHERE provider_id = %d
                AND practice_id IS NULL
                AND is_internal = FALSE
            ORDER BY listing_site.name;',
            $providerId
        );
        return Yii::app()->dbRO->createCommand($sql)->queryAll();
    }

    /**
     * Return credential->title for a given provider
     *
     * @param integer $providerId
     * @return string $credentialTitle
     */
    public static function getCredential($providerId = 0)
    {
        if ($providerId == 0) {
            return '';
        }

        $sql = sprintf(
            "SELECT credential.title AS credential_title
            FROM provider
            LEFT JOIN credential
                ON provider.credential_id = credential.id
            WHERE provider.id = %d",
            $providerId
        );
        $result = Yii::app()->dbRO->createCommand($sql)->queryRow();
        if (empty($result)) {
            return '';
        }
        return $result['credential_title'];
    }

    /**
     * Return the current instance's credentials
     *
     * @return string
     */
    public function credentials()
    {
        if ($this->credential) {
            return $this->credential->title;
        }

        return '';
    }


    /**
     * Find provider by ID in Read Only DB
     *
     * @param int $providerId
     * @param bool $useCache
     * @return array
     */
    public static function getByIdRO($providerId = 0, $useCache = true)
    {
        if ($providerId == 0) {
            return null;
        }

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;

        $sql = sprintf(
            "SELECT id, localization_id, country_id, first_last, first_m_last, first_middle_last,
            npi_id, gender, credential_id, prefix, first_name, middle_name,    last_name,
            suffix, docscore, accepts_new_patients, year_began_practicing, vanity_specialty,
            additional_specialty_information, brief_bio, bio, fees, avg_rating,
            friendly_url, photo_path, date_added, date_updated, umd_id, date_umd_updated,
            featured, verified,    claimed, suspended,    partner_site_id, is_test, nasp_date
            FROM provider
            WHERE id = %d; ",
            $providerId
        );
        return Yii::app()->dbRO->cache($cacheTime)->createCommand($sql)->queryRow();
    }

    /**
     * Track a provider search view by trying to update the table and otherwise insert into it
     *
     * @param int $providerId
     * @return boolean
     */
    public static function trackProviderSearchViewStats($providerId)
    {

        if (intval($providerId) <= 0) {
            return false;
        }

        if (UserAgentComponent::isBot()) {
            return false;
        }

        //update stats
        $sql = sprintf(
            "UPDATE provider_stats
            SET search_views = search_views + 1
            WHERE provider_id = %d
            LIMIT 1;",
            $providerId
        );
        $affected_rows = Yii::app()->db->createCommand($sql)->execute();

        if ($affected_rows == 0) {
            //there was no record for this provider: insert stats
            $sql = sprintf(
                "INSERT INTO provider_stats
                (provider_id, search_views)
                VALUES
                ('%s', '1');",
                $providerId
            );
            $affected_rows = Yii::app()->db->createCommand($sql)->execute();
        }

        return $affected_rows;
    }

    /**
     * Track a provider search view by trying to update the table and otherwise insert into it
     *
     * @param int $providerId
     * @return boolean
     * @todo because we have provider_stats_report that keeps daily/monthly data,
     * we shouldn't need to have provider_stats too, since its data could be
     * derived from the former. I suggest removing all references to it and
     * updating the code that queries it
     *
     */
    public static function trackProviderSearchViewStatsReport($providerId)
    {

        if (intval($providerId) <= 0) {
            return false;
        }

        if (UserAgentComponent::isBot()) {
            return false;
        }

        //update stats
        $sql = sprintf(
            "UPDATE provider_stats_report
            SET search_views = search_views + 1
            WHERE provider_id = %d
            AND `date` = CURDATE()
            LIMIT 1;",
            $providerId
        );
        $affected_rows = Yii::app()->db->createCommand($sql)->execute();

        if ($affected_rows == 0) {
            //there was no record for this provider: insert stats
            $sql = sprintf(
                "INSERT INTO provider_stats_report
                (provider_id, `date`, search_views)
                VALUES
                ('%s', CURDATE(), '1');",
                $providerId
            );
            $affected_rows = Yii::app()->db->createCommand($sql)->execute();
        }

        return $affected_rows;
    }

    /**
     * Count providers in a given city
     *
     * @param int $cityId
     * @return int
     */
    public function countProvidersByCity($cityId)
    {

        $criteria['practice.city_id'] = $cityId;
        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Count providers accepting a given insurance in a given city
     *
     * @param int $cityId
     * @param int $insuranceId
     * @return int
     */
    public function countProvidersByCityInsurance($cityId, $insuranceId)
    {
        $criteria['practice.city_id'] = $cityId;
        $criteria['insurance.company_id'] = $insuranceId;
        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Count providers practicing a given specialty in a given city
     *
     * @param int $cityId
     * @param int $specialtyId
     * @return int
     */
    public function countProvidersByCitySpecialty($cityId, $specialtyId)
    {

        $criteria['practice.city_id'] = $cityId;
        $criteria['specialty.specialty_id'] = $specialtyId;
        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Count providers practicing a specialty and accepting an insurance
     *
     * @param int $specialtyId
     * @param int $insuranceId
     * @return int
     */
    public function countProvidersBySpecialtyInsurance($specialtyId, $insuranceId)
    {
        $criteria['specialty.specialty_id'] = $specialtyId;
        $criteria['insurance.company_id'] = $insuranceId;
        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Returns the amount of providers in a given state
     *
     * @param int $stateId
     * @return int
     */
    public function countProvidersByState($stateId)
    {
        $criteria['practice.state_id'] = $stateId;
        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Count providers in a state accepting an insurance
     *
     * @param int $stateId
     * @param int $insuranceId
     * @return array
     */
    public function countProvidersByStateInsurance($stateId, $insuranceId)
    {
        $criteria['practice.state_id'] = $stateId;
        $criteria['insurance.company_id'] = $insuranceId;
        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Count providers in a state practicing a specialty
     *
     * @param int $stateId
     * @param int $specialtyId
     * @return array
     */
    public function countProvidersByStateSpecialty($stateId, $specialtyId)
    {

        $criteria['practice.state_id'] = $stateId;
        $criteria['specialty.specialty_id'] = $specialtyId;
        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Count providers in a state practicing a specialty accepting an insurance
     *
     * @param int $stateId
     * @param int $specialtyId
     * @param int $insuranceId
     * @return array
     */
    public function countProvidersByStateSpecialtyInsurance($stateId, $specialtyId, $insuranceId)
    {
        $criteria['practice.state_id'] = $stateId;
        $criteria['specialty.specialty_id'] = $specialtyId;
        $criteria['insurance.company_id'] = $insuranceId;
        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Generate the friendly_url for a provider
     * If the provider's new, we pass parameters up to $title only
     * Currently nothing's done on update
     *
     * @param string $prefix
     * @param string $firstName
     * @param string $lastName
     * @param int $providerId
     * @return string
     */
    public static function getFriendlyUrl($prefix = '', $firstName = '', $lastName = '', $providerId = 0)
    {
        $prefix = StringComponent::prepareNewFriendlyUrl($prefix);
        $firstName = StringComponent::prepareNewFriendlyUrl($firstName);
        $lastName = StringComponent::prepareNewFriendlyUrl($lastName);

        $url = ($prefix != '' ? $prefix . "-" : '') . $firstName . "-" . $lastName;

        return StringComponent::getNewFriendlyUrl($url, $providerId, 'provider');
    }

    /**
     * Update the docscore for provider $this
     * Must be called in object context!
     *
     * @param boolean $saveAttributesOnly
     * @return void
     */
    public function updateScore($saveAttributesOnly = false)
    {

        if ($this->isNewRecord) {
            return;
        }

        $docscoreTotal = 0;
        $arrMissingData = array();
        // go through possible scores adding up
        for ($i = 1; $i <= 11; $i++) {
            list($thisDocScore, $missingData) = $this->getDocScore($i);

            $docscoreTotal += $thisDocScore;
            if (!empty($missingData)) {
                foreach ($missingData as $missingDataPoint) {
                    $arrMissingData[] = $missingDataPoint;
                }
            }
        }

        // The docScore was changed?
        if ($this->docscore != $docscoreTotal) {

            $this->docscore = $docscoreTotal;

            // so we don't call afterSave() twice in a row
            $this->saveAttributes(array('docscore' => $this->docscore));

            // find this provider's organizations
            $arrOrganizations = Provider::belongsToOrganization($this->id);
            if (!empty($arrOrganizations)) {
                foreach ($arrOrganizations as $organizationId) {
                    // send a request to update docpoints in SalesForce
                    SqsComponent::sendMessage('updateSFOrganizationDocScore', $organizationId);
                }
            }
        }

        // look up field that indicates missing fields
        $providerDetails = ProviderDetails::model()->find('provider_id=:providerId', array(':providerId' => $this->id));
        if (!$providerDetails) {
            $providerDetails = new ProviderDetails;
            $providerDetails->provider_id = $this->id;
            $providerDetails->save();
        }
        // build a string with what's missing
        if (!empty($arrMissingData)) {
            $strMissingData = implode(', ', $arrMissingData);
        } else {
            $strMissingData = '';
        }
        // save it if necessary
        if ($providerDetails->score_missing_data != $strMissingData) {
            $providerDetails->score_missing_data = $strMissingData;
            $providerDetails->save();
        }

        return $this->docscore;
    }

    /**
     * Get DocPoints for given ID ($docscoreId) for given provider ($this)
     * Must be called in object context!
     *
     * @param int $docscoreId
     * @return array
     */
    public function getDocScore($docscoreId)
    {
        $connection = Yii::app()->db;
        $docscoreEarned = false;
        $missingData = [];

        switch ($docscoreId) {
            case 1:
                // "Baseline"
                $docscoreEarned = true;
                break;
            case 2:
                // "Basic Information"
                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider
                    INNER JOIN credential
                        ON provider.credential_id = credential.id
                    INNER JOIN provider_details
                        ON provider.id = provider_details.provider_id
                    INNER JOIN provider_specialty
                        ON provider.id = provider_specialty.provider_id
                    INNER JOIN provider_practice
                        ON provider.id = provider_practice.provider_id
                    WHERE provider.id = %d
                        AND first_name != ''
                        AND last_name != '';",
                    $this->id
                );

                $docscoreEarned = (bool)$connection->createCommand($sql)->queryScalar();

                // save missing data always
                if (!$docscoreEarned) {
                    if (empty($this->credential_id)) {
                        $missingData[] = 'Credential';
                    }
                    if (!ProviderSpecialty::model()->exists(
                        'provider_id=:providerId',
                        array(':providerId' => $this->id)
                    )) {
                        $missingData[] = 'Specialty';
                    }
                    if (!ProviderPractice::model()->exists(
                        'provider_id=:providerId',
                        array(':providerId' => $this->id)
                    )) {
                        $missingData[] = 'Practices';
                    }
                    if (empty($this->first_name)) {
                        $missingData[] = 'First Name';
                    }
                    if (empty($this->last_name)) {
                        $missingData[] = 'Last Name';
                    }
                }

                if (!$docscoreEarned) {
                    // old claimed providers get points anyway
                    $sql = sprintf(
                        "SELECT SQL_NO_CACHE IF(YEAR(date_added) <= 2011, 1, 0) AS year_added
                        FROM account_provider
                        WHERE provider_id = %d
                        ORDER BY account_provider.id ASC
                        LIMIT 1;",
                        $this->id
                    );
                    $docscoreEarned = (bool)$connection->createCommand($sql)->queryScalar();
                }

                if (!$docscoreEarned) {
                    // old UN-claimed providers get points anyway too!
                    $sql = sprintf(
                        "SELECT SQL_NO_CACHE IF(account_provider.id IS NULL, 1, 0) AS old_provider
                        FROM provider
                        LEFT JOIN account_provider
                            ON provider.id = account_provider.provider_id
                        WHERE provider.id = %d
                        ORDER BY account_provider.id ASC
                        LIMIT 1;",
                        $this->id
                    );
                    $docscoreEarned = (bool)$connection->createCommand($sql)->queryScalar();
                }

                break;
            case 3:
                // "Biography"
                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider
                    WHERE id = %d
                    AND brief_bio != '' ;",
                    $this->id
                );
                $docscoreEarned = (bool)$connection->createCommand($sql)->queryScalar();
                if (!$docscoreEarned) {
                    $missingData[] = StringComponent::providerScoreSectionName(3);
                }
                break;
            case 4:
                // "Add Insurance"
                // check provider_details for a no-insurance reason
                $sql = sprintf(
                    "SELECT no_insurance_reason
                    FROM provider_details
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned = (bool)($connection->createCommand($sql)->queryScalar() != '');
                if (!$docscoreEarned) {
                    // try provider_insurance first
                    $sql = sprintf(
                        "SELECT SQL_NO_CACHE COUNT(1) AS required
                        FROM provider_insurance
                        WHERE provider_id = %d;",
                        $this->id
                    );
                    $docscoreEarned = (bool)($connection->createCommand($sql)->queryScalar() > 0);
                    if (!$docscoreEarned) {
                        // if we had no luck, try provider_-_practice_insurance
                        $sql = sprintf(
                            "SELECT SQL_NO_CACHE COUNT(1) AS required
                            FROM provider_practice_insurance
                            WHERE provider_id = %d;",
                            $this->id
                        );
                        $docscoreEarned = (bool)($connection->createCommand($sql)->queryScalar() > 0);
                    }
                }
                if (!$docscoreEarned) {
                    $missingData[] = StringComponent::providerScoreSectionName(4);
                }
                break;
            case 5:
                // "Add Fees and Payment Options"
                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider
                    INNER JOIN provider_payment_type
                        ON provider.id = provider_payment_type.provider_id
                    WHERE provider.id = %d ;",
                    $this->id
                );
                $docscoreEarned = (bool)($connection->createCommand($sql)->queryScalar() > 0);
                if (!$docscoreEarned) {
                    $missingData[] = StringComponent::providerScoreSectionName(5);
                }
                break;
            case 6:
                // "Add Education"
                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_certification
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned_1 = (bool)($connection->createCommand($sql)->queryScalar() > 0);

                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_education
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned_2 = (bool)($connection->createCommand($sql)->queryScalar() > 0);

                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_residency
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned_3 = (bool)($connection->createCommand($sql)->queryScalar() > 0);
                $docscoreEarned = ($docscoreEarned_1 || $docscoreEarned_2 || $docscoreEarned_3);
                if (!$docscoreEarned) {
                    $missingData[] = StringComponent::providerScoreSectionName(6);
                }
                break;
            case 7:
                // "Add Experience"
                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_award
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned_1 = (bool)($connection->createCommand($sql)->queryScalar() > 0);

                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_experience
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned_2 = (bool)($connection->createCommand($sql)->queryScalar() > 0);

                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_membership
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned_3 = (bool)($connection->createCommand($sql)->queryScalar() > 0);

                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_publication
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned_4 = (bool)($connection->createCommand($sql)->queryScalar() > 0);
                $docscoreEarned = ($docscoreEarned_1 || $docscoreEarned_2 || $docscoreEarned_3
                    || $docscoreEarned_4);
                if (!$docscoreEarned) {
                    $missingData[] = StringComponent::providerScoreSectionName(7);
                }
                break;
            case 8:
                // "Add Procedures"
                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_treatment
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned = (bool)($connection->createCommand($sql)->queryScalar() > 0);
                if (!$docscoreEarned) {
                    $missingData[] = StringComponent::providerScoreSectionName(8);
                }
                break;
            case 9:
                // "Add Medications"
                $sql = sprintf(
                    "SELECT SQL_NO_CACHE
                    IF((COUNT(medication_id) > 0 OR provider_details.has_medications = 0), 1, 0) AS required
                    FROM provider
                    LEFT JOIN provider_details
                    ON provider.id = provider_details.provider_id
                    LEFT JOIN provider_medication
                    ON provider_medication.provider_id = provider.id
                    WHERE provider.id = %d;",
                    $this->id
                );
                $docscoreEarned = (bool)($connection->createCommand($sql)->queryScalar() > 0);
                if (!$docscoreEarned) {
                    $missingData[] = StringComponent::providerScoreSectionName(9);
                }
                break;
            case 10:
                // "Add Photos"
                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_photo
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned_1 = (bool)($connection->createCommand($sql)->queryScalar() > 0);

                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_video
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned_2 = (bool)($connection->createCommand($sql)->queryScalar() > 0);

                $docscoreEarned = ($docscoreEarned_1 || $docscoreEarned_2);
                if (!$docscoreEarned) {
                    $missingData[] = StringComponent::providerScoreSectionName(10);
                }
                break;
            case 11:
                // "Add Forms"
                $sql = sprintf(
                    "SELECT SQL_NO_CACHE COUNT(1) AS required
                    FROM provider_form
                    WHERE provider_id = %d;",
                    $this->id
                );
                $docscoreEarned = (bool)($connection->createCommand($sql)->queryScalar() > 0);
                if (!$docscoreEarned) {
                    $missingData[] = StringComponent::providerScoreSectionName(11);
                }
                break;
            default:
        }

        if ($docscoreEarned) {
            $score = Docscore::model()->getBaseDocScore($docscoreId);
            return array($score, $missingData);
        }
        return array(false, $missingData);
    }

    /**
     * Returns whether schedule_mail_enabled is true or pb_user is bigger than 0
     *
     * @return boolean
     */
    public function schedulingEnabled()
    {
        $providerId = $this->id;
        $pd = ProviderDetails::model()->find('provider_id=:provider_id', array(':provider_id' => $providerId));
        if ($pd->schedule_mail_enabled == 1 || $pd->pb_user > 0) {
            return true;
        }
        return false;
    }

    /**
     * Gets a provider's weighed rating (upper if more recent)
     *
     * @see http://dev.doctor.com/T6636 for details
     * @param int $providerId
     * @return int
     */
    public static function getWeighedRating($providerId)
    {

        $sql = sprintf(
            "SELECT ROUND((SUM(rating * (1/(1+(POW(2, (0.17*(DATEDIFF(NOW(), date_added)-90))))))) /
            COUNT(1)) * 1000)
            FROM provider_rating
            WHERE provider_id = %d
            AND status = 'A';",
            $providerId
        );
        return Yii::app()->dbRO->createCommand($sql)->queryScalar();
    }

    /**
     * Calculate a provider's search order for provider.display_search_order
     * MAX: F=1, DP=80, R=5000, P=1 => 108050001 =>  91900000
     * MIN: F=0, DP=20, R=0000, P=0 => 002000000 => 197950001
     *
     * @param int $featured
     * @param int $docpoints
     * @param int $weighedRating
     * @param string $photoPath
     * @return int
     */
    public function calculateSearchOrder($featured, $docpoints, $weighedRating, $photoPath)
    {

        $base = 199950001;

        if ($photoPath == '') {
            $photoPath = 0;
        } else {
            $photoPath = 1;
        }

        $weighedRating = str_pad($weighedRating, 4, '0', STR_PAD_LEFT);

        return $base - (($featured * 100000000) + ($docpoints * 100000) + ($weighedRating * 10) + $photoPath);
    }

    /**
     * Insert/update provider data in provider_details and automatically turn on online appointments for new providers
     *
     * @return boolean
     */
    public function afterSave()
    {

        if ($this->isNewRecord) {

            // automatically turn on/off online appointments for new providers
            $pd = ProviderDetails::model()->find('provider_id = :provider_id', [':provider_id' => $this->id]);
            if (!$pd) {
                $pd = new ProviderDetails();
                $pd->provider_id = $this->id;
            }

            if ($this->enableAppointmentBooking) {
                $pd->schedule_mail_enabled = 1;
            } else {
                $pd->schedule_mail_enabled = 0;
            }

            $pd->save();
        }

        $this->updateScore();
        return parent::afterSave();
    }

    /**
     * Given a ProviderPhoto id, update the photo_path of provider when it changes
     *
     * @param ProviderPhoto $providerPhoto
     * @return boolean
     */
    public static function afterProviderPhotoUpdate($providerPhoto)
    {
        $sql = sprintf(
            "UPDATE provider AS p
            INNER JOIN provider_photo AS pp
                ON p.id = pp.provider_id
            SET p.photo_path = pp.photo_path
            WHERE p.id = '%s'
                AND pp.cover = 1;",
            $providerPhoto->provider_id
        );
        return Yii::app()->db->createCommand($sql)->execute();
    }

    /**
     * Set to NULL the photo_path of provider table
     *
     * @param ProviderPhoto $providerPhoto
     * @return boolean
     */
    public static function setPhotoPathNull($providerPhoto)
    {
        $sql = sprintf(
            "UPDATE provider AS p
            SET p.photo_path = NULL
            WHERE p.id = %d;",
            $providerPhoto->provider_id
        );
        return Yii::app()->db->createCommand($sql)->execute();
    }

    /**
     * Returns providers that are associated to other account but works on the same practice.
     *
     * @param int $account_id
     * @param array $arrPractice (optional parameter)
     * @param boolean $dashBoard (optional parameter)
     * @return array
     */
    public static function getProvidersAssociatedToOtherAccounts($account_id, $arrPractice = null, $dashBoard = null)
    {
        $practiceList = $limit = '';
        $comma_pra = "";
        $providers = array();
        $id = 'id';

        if (!$arrPractice) {
            $arrPractice = AccountPractice::model()->findAll(
                'account_id=:account_id',
                array(':account_id' => $account_id)
            );
            $id = 'practice_id';
        }
        if (!empty($arrPractice)) {
            foreach ($arrPractice as $practice) {
                $practiceList .= $comma_pra . $practice[$id];
                $comma_pra = ",";
            }
        }

        // Limit the number of results if the user is looking his dashboard.
        if ($dashBoard) {
            $limit = 'LIMIT 10';
        }

        if (!empty($practiceList)) {
            $sql = sprintf(
                "SELECT SQL_CALC_FOUND_ROWS DISTINCT(p.id)
                FROM provider AS p
                INNER JOIN provider_practice
                    ON p.id = provider_practice.provider_id
                INNER JOIN account_practice
                    ON account_practice.practice_id=provider_practice.practice_id
                LEFT JOIN account_provider
                    ON account_provider.provider_id = provider_practice.provider_id
                WHERE provider_practice.practice_id IN (%s)
                AND account_practice.account_id = %d
                AND p.id NOT IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                %s;",
                $practiceList,
                $account_id,
                $account_id,
                $limit
            );
            $arrProviders = Yii::app()->db->createCommand($sql)->queryAll();

            if ($dashBoard) {
                // get the total number of those providers
                $sql = sprintf("SELECT FOUND_ROWS();");
                $providers[] = Yii::app()->db->createCommand($sql)->queryScalar();
            }
            foreach ($arrProviders as $provider) {
                $providers[$provider['id']] = Provider::model()->findByPk($provider['id']);
            }
        }
        return $providers;
    }

    /* copied */

    /**
     * Returns the average of years practicing
     *
     * @param int $cityId
     * @return float
     */
    public function getYearsPracticingByCity($cityId)
    {
        $criteria['practice.city_id'] = $cityId;
        return $this->getAverageYearBeganPracticing($criteria);
    }

    /**
     * Average time practicing of providers in a city (optionally specialty, optionally insurance)
     *
     * @param int $cityId
     * @param int $specialtyId
     * @param int $insuranceId
     * @return int
     */
    public function getYearsPracticingByCitySpecialtyInsurance($cityId, $specialtyId = null, $insuranceId = null)
    {
        if ($insuranceId) {
            if ($specialtyId) {
                $criteria['insurance.company_id'] = $insuranceId;
                $criteria['specialty.specialty_id'] = $specialtyId;
                return $this->getAverageYearBeganPracticing($criteria);
            } else {
                $criteria['insurance.company_id'] = $insuranceId;
                return $this->getAverageYearBeganPracticing($criteria);
            }
        } else {
            $criteria['specialty.specialty_id'] = $specialtyId;
            return $this->getAverageYearBeganPracticing($criteria);
        }
    }

    /**
     * Average time practicing of providers in a (optionally specialty, optionally insurance)
     *
     * @param int $specialtyId
     * @param int $insuranceId
     * @return float
     */
    public function getYearsPracticingBySpecialtyInsurance($specialtyId = null, $insuranceId = null)
    {

        if ($specialtyId) {
            $criteria['specialty.specialty_id'] = $specialtyId;
        }
        if ($insuranceId) {
            $criteria['insurance.company_id'] = $insuranceId;
        }
        return $this->getAverageYearBeganPracticing($criteria);
    }

    /**
     * Returns the average number of years providers have been practicing in a given state
     *
     * @param int $stateId
     * @return float
     */
    public function getYearsPracticingByState($stateId)
    {
        if ($stateId == 0) {
            return false;
        }

        $criteria['practice.state_id'] = $stateId;
        return $this->getAverageYearBeganPracticing($criteria);
    }

    /**
     * Average time practicing of providers in a state (optionally specialty, optionally insurance)
     *
     * @param int $stateId
     * @param int $specialtyId
     * @param int $insuranceId
     * @return float
     */
    public function getYearsPracticingByStateSpecialtyInsurance($stateId, $specialtyId = null, $insuranceId = null)
    {
        $criteria['practice.state_id'] = $stateId;
        if ($specialtyId) {
            $criteria['specialty.specialty_id'] = $specialtyId;
        }
        if ($insuranceId) {
            $criteria['insurance.company_id'] = $insuranceId;
        }
        return $this->getAverageYearBeganPracticing($criteria);
    }

    /**
     * Returns the avg docscore of the providers in a given city
     *
     * @param int $cityId
     * @return float
     */
    public function getDocPointsByCity($cityId)
    {
        $criteria['practice.city_id'] = $cityId;
        return $this->getAverageDocscore($criteria);
    }

    /**
     * Returns docscore avg for providers in a city (optionally specialty, optionally insurance)
     *
     * @param int $cityId
     * @param int $specialtyId
     * @param int $insuranceId
     * @return int
     */
    public function getDocPointsByCitySpecialtyInsurance($cityId = 0, $specialtyId = null, $insuranceId = null)
    {
        if ($cityId == 0) {
            return false;
        }

        $criteria['practice.city_id'] = $cityId;
        if ($specialtyId) {
            $criteria['specialty.specialty_id'] = $specialtyId;
        }
        if ($insuranceId) {
            $criteria['insurance.company_id'] = $insuranceId;
        }
        return $this->getAverageDocscore($criteria);
    }

    /**
     * Returns docscore avg for providers in a state (optionally specialty, optionally insurance)
     *
     * @param int $specialtyId
     * @param int $insuranceId
     * @return float
     */
    public function getDocPointsBySpecialtyInsurance($specialtyId = null, $insuranceId = null)
    {
        if ($specialtyId) {
            $criteria['specialty.specialty_id'] = $specialtyId;
        }
        if ($insuranceId) {
            $criteria['insurance.company_id'] = $insuranceId;
        }
        return $this->getAverageDocscore($criteria);
    }

    /**
     * Returns the avg docscore of the providers in a given state
     *
     * @param int $stateId
     * @return float
     */
    public function getDocPointsByState($stateId)
    {
        $criteria['practice.state_id'] = $stateId;
        return $this->getAverageDocscore($criteria);
    }

    /**
     * Returns docscore avg for providers in a state (optionally specialty, optionally insurance)
     *
     * @param int $stateId
     * @param int $specialtyId
     * @param int $insuranceId
     * @return float
     */
    public function getDocPointsByStateSpecialtyInsurance($stateId, $specialtyId = null, $insuranceId = null)
    {
        $criteria['practice.state_id'] = $stateId;
        if ($specialtyId) {
            $criteria['specialty.specialty_id'] = $specialtyId;
        }
        if ($insuranceId) {
            $criteria['insurance.company_id'] = $insuranceId;
        }
        return $this->getAverageDocscore($criteria);
    }

    /**
     * Check whether a credential allows a provider to use the Dr prefix
     * $drValidCredentials is optional and used to avoid looking up in the DB
     * For use in import processes, etc.
     *
     * @param string $specifiedCredentialTitle
     * @param string $drValidCredentials
     * @return boolean
     */
    public static function isValidDrCredential($specifiedCredentialTitle = '', $drValidCredentials = '')
    {

        if (!empty($specifiedCredentialTitle)) {

            if (!empty($drValidCredentials)) {
                $drValidCredentials = GlobalParameter::model()->find('`key`="dr_valid_credentials"')->value;
            }
            $drValidCredentials = explode(', ', $drValidCredentials);

            foreach ($drValidCredentials as $drValidCredential) {

                $drValidCredential = strtolower(trim(str_replace('.', '', $drValidCredential)));
                $specifiedCredentialTitle = strtolower(trim(str_replace('.', '', $specifiedCredentialTitle)));

                $pattern = "(^" . $drValidCredential . "$|";

                $pattern .= "^" . $drValidCredential . ",|";
                $pattern .= "," . $drValidCredential . "$|";
                $pattern .= "," . $drValidCredential . ",|";
                $pattern .= ",\s" . $drValidCredential . ",|";
                $pattern .= ",\s" . $drValidCredential . "$|";
                $pattern .= "\s" . $drValidCredential . ",|";
                $pattern .= "," . $drValidCredential . "\s|";

                $pattern .= "^" . $drValidCredential . ";|";
                $pattern .= ";" . $drValidCredential . "$|";
                $pattern .= ";" . $drValidCredential . ";|";
                $pattern .= ";\s" . $drValidCredential . ";|";
                $pattern .= ";\s" . $drValidCredential . "$|";
                $pattern .= "\s" . $drValidCredential . ";|";
                $pattern .= ";" . $drValidCredential . "\s|";

                $pattern .= "^" . $drValidCredential . "-|";
                $pattern .= "-" . $drValidCredential . "$|";
                $pattern .= "-" . $drValidCredential . "-|";
                $pattern .= "-\s" . $drValidCredential . "-|";
                $pattern .= "-\s" . $drValidCredential . "$|";
                $pattern .= "\s" . $drValidCredential . "-|";
                $pattern .= "-" . $drValidCredential . "\s|";

                $pattern .= "^" . $drValidCredential . "\s|";
                $pattern .= "\s" . $drValidCredential . "\s|";
                $pattern .= "^" . $drValidCredential . "\s|";
                $pattern .= "\s" . $drValidCredential . "$)";

                if (preg_match($pattern, $specifiedCredentialTitle)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     *
     * Takes the provided parameters to either look up an existing
     * provider or create a new one. Returns the provider (if there
     * was an error, check the return object's getErrors() result
     *
     * @param object $providerArgs
     * @param array $newProviders this array is added to if practice is new
     * @return \Provider
     */
    public static function createOrFetchProvider($providerArgs, &$newProviders)
    {

        // ensure name, address, phone_number, and zipcode are all provided
        $provider = new Provider;
        $provider->attributes = (array)$providerArgs;

        // ensure info about practice provided is valid
        if (empty($providerArgs->first_name)) {
            $provider->addError('first_name', 'First name cannot be blank');
        }
        if (empty($providerArgs->last_name)) {
            $provider->addError('last_name', 'Address cannot be blank');
        }
        if (empty($providerArgs->npi_id)) {
            $provider->addError('npi_id', 'NPI id cannot be blank');
        }

        // exit early, showing errors
        if ($provider->getErrors()) {
            return $provider;
        }

        // try to look up existing provider instead of creating new one
        $provider = Provider::model()->find("npi_id = :npi_id", array(":npi_id" => $providerArgs->npi_id));

        if ($provider) {
            // just return first match
            return $provider;
        }

        $provider = new Provider;
        $provider->attributes = (array)$providerArgs;
        $provider->friendly_url = $provider->getFriendlyUrl('', $provider->first_name, $provider->last_name, '');

        // try saving; note: if this fails, $provider->getErrors()
        // will have contents
        if (!$provider->save()) {
            return $provider;
        }

        // this is a new practice, so add to $newPractices list
        $newProviders[] = $provider;

        return $provider;
    }

    /**
     * Return whether a provider belongs to an account that belongs to an organization that's active
     *
     * @param int $providerId
     * @return boolean
     */
    public static function isClient($providerId)
    {
        $accountProviders = AccountProvider::model()->cache(Yii::app()->params['cache_short'])->
            findAll("provider_id=:provider_id", array(":provider_id" => $providerId));

        foreach ($accountProviders as $accountProvider) {
            $organizationAccount = OrganizationAccount::model()
                ->cache(Yii::app()->params['cache_short'])
                ->find(
                    'account_id = :account_id AND connection = :connection',
                    array(':account_id' => $accountProvider->account_id, ':connection' => "O")
                );
            if ($organizationAccount) {
                $organization = Organization::model()
                    ->cache(Yii::app()->params['cache_short'])
                    ->findByPk($organizationAccount->organization_id);
                if ($organization->status == 'A') {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks a record's ownership
     *
     * @param int $providerId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($providerId = 0, $accountId = 0)
    {
        if ($providerId < 1 || $accountId < 1) {
            return false;
        }

        $sql = sprintf(
            "SELECT account_id
            FROM account_provider
            WHERE account_provider.provider_id = %d
                AND account_id = %d
            LIMIT 1;",
            $providerId,
            $accountId
        );

        $result = Yii::app()->db->createCommand($sql)->queryScalar();
        return ($result) ? (int)$result : false;
    }

    /**
     * Kiosk: Gets one or more provider's details to be used in the ReviewHub in the order they were asked for
     *
     * @param string $providerId
     * @return array
     */
    public static function getProviderDetailsForKiosk($providerId = '')
    {
        $order = '';
        $providers = explode(',', $providerId);
        if (!empty($providers)) {
            $order = 'ORDER BY';
            foreach ($providers as $provider) {
                $order .= ' provider_id = ' . $provider . ' DESC, ';
            }
            $order = trim($order);
            $order = rtrim($order, ',');
        }
        $order = rtrim($order, ',');

        $sql = sprintf(
            "SELECT provider.prefix, provider.id, provider.first_name, provider.middle_name,
            provider.last_name, provider.gender, provider.photo_path, provider_specialty.id AS
            primary_specialty, vanity_specialty, npi_id AS provider_npi,
            specialty.name AS specialty, sub_specialty.name AS sub_specialty, credential.title AS credential
            FROM provider
            INNER JOIN credential
                ON provider.credential_id = credential.id
            LEFT JOIN provider_specialty
                ON provider_specialty.provider_id = provider.id
                AND provider_specialty.primary_specialty = 1
            LEFT JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
            LEFT JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
            WHERE provider.id IN (%s)
            GROUP BY provider.id %s;",
            $providerId,
            $order
        );
        $result = Yii::app()->dbRO->createCommand($sql)->queryAll();
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                if (!empty($value['photo_path'])) {
                    $result[$key]['photo_path'] = 'https://' . Yii::app()->params->servers['dr_ast_hn'] .
                        '/img?p=providers/' . $value['photo_path'] . '&w=150&h=150';
                }
            }
        }
        return $result;
    }

    /**
     * get details from list of providers
     *
     * @param string $providerList
     * @param bool $useCache
     * @return array
     */
    public static function getProviderDetailsFromList($providerList = '', $useCache = true)
    {

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;
        $sql = sprintf(
            "SELECT provider.prefix, provider.id, provider.last_name, provider.first_m_last,
            provider.gender, provider.photo_path, docscore, provider.friendly_url, featured, claimed, suspended,
            provider_specialty.id AS primary_specialty, vanity_specialty,
            specialty.name AS specialty, sub_specialty.name AS sub_specialty,
            credential.title AS credential, CONCAT(specialty.name, ' - ', sub_specialty.name) AS specialty_name
            FROM provider
            INNER JOIN credential
                ON provider.credential_id = credential.id
            LEFT JOIN provider_specialty
                ON provider_specialty.provider_id = provider.id
                AND provider_specialty.primary_specialty = 1
            LEFT JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
            LEFT JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
            WHERE provider.id IN (%s)
            ORDER BY provider.first_m_last ASC;",
            $providerList
        );
        $result = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                if (!empty($value['photo_path'])) {
                    $result[$key]['photo_path'] = 'https://' . Yii::app()->params->servers['dr_ast_hn'] .
                        '/img?p=providers/' . $value['photo_path'] . '&w=150&h=150';
                }
            }
        }
        return $result;
    }

    /**
     * GADM: Data provider for provider autocomplete
     * Searches by NPI if 10 digits, else for active clients first, then all providers
     *
     * @param string $q
     * @return array
     */
    public static function autocompleteGADM($q = '')
    {
        $q = StringComponent::sanitize($q);

        // remove invalid characters to be searched
        $find = array(
            '&',
            '\r\n',
            '\n',
            '+',
            '&',
            '<',
            '>',
            '*',
            '+',
            '/',
            '-',
            '\\',
            '%',
            '$',
            '#',
            '.',
            '"',
            "'",
            '!',
            '(',
            ')',
            '[',
            ']',
            '^',
            ';',
            ',',
            ':',
            '|',
            '?',
            '=',
            'Â¿',
            'Â¡',
            '.',
            '_'
        );
        $q = str_replace($find, ' ', $q);

        $condition = $clientJoin = $order = '';

        // the base sql we'll use
        $sqlBase = "SELECT DISTINCT provider.id
            FROM provider
            %s
            %s
            %s
            LIMIT 20;";

        if (strlen($q) == 10 && floatval($q) == $q && $q > 0) {
            // it was a 10 digit number: search by NPI (don't use %d here)
            $condition = sprintf("WHERE provider.npi_id = '%s' ", $q);
        } else {
            // search for active clients by first and last name
            $clientJoin = 'INNER JOIN account_provider
                ON provider.id = account_provider.provider_id
                INNER JOIN organization_account
                ON organization_account.account_id = account_provider.account_id AND `connection` = "O"
                INNER JOIN organization ON organization.id = organization_account.organization_id AND
                `status` = "A" ';

            // search by their combination
            $condition = sprintf('WHERE first_last LIKE "%%%s%%"', $q);
            if (stristr($q, 'dr') !== false) {
                // if the search string had dr in it try removing it
                $condition .= sprintf(' OR first_last LIKE "%%%s%%"', trim(str_replace('dr', '', $q)));
            }

            $order = 'ORDER BY last_name ';
        }

        // try clients first
        $sql = sprintf($sqlBase, $clientJoin, $condition, $order);
        $providerIds = Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->queryAll();

        if (!$providerIds && $clientJoin && $order) {
            // we tried looking for clients by name and didn't find any, look for all of them (by name)
            $sql = sprintf($sqlBase, '', $condition, '');
            $providerIds = Yii::app()->db->cache(Yii::app()->params['cache_short'])
                ->createCommand($sql)
                ->queryAll();
        }

        // take the array of arrays and build an array we can implode
        $strProviders = [];
        if (!empty($providerIds)) {
            foreach ($providerIds as $providerId) {
                $strProviders[] = $providerId['id'];
            }
        }

        if (empty($strProviders)) {
            // nothing to get details about
            return array();
        }

        $strProviders = implode(', ', $strProviders);

        if (!empty($strProviders)) {
            // get details from the ids
            $sql = sprintf(
                "SELECT SQL_NO_CACHE DISTINCT provider.id, provider.npi_id, prefix,
                first_name, last_name, credential.title AS title, city.name AS city_name,
                state.abbreviation AS abbreviation, location.zipcode AS zipcode
                FROM provider
                INNER JOIN credential
                    ON credential_id = credential.id
                LEFT JOIN provider_practice
                    ON provider.id = provider_practice.provider_id
                AND primary_location = 1
                LEFT JOIN practice
                    ON provider_practice.practice_id = practice.id
                LEFT JOIN location
                    ON practice.location_id = location.id
                LEFT JOIN city
                    ON location.city_id = city.id
                LEFT JOIN state
                    ON city.state_id = state.id
                WHERE provider.id IN (%s)
                ORDER BY last_name;",
                $strProviders
            );
            return Yii::app()->dbRO->createCommand($sql)->queryAll();
        }

        // couldn't implode (!)
        return array();
    }

    /**
     * Get an array representing the information of the provider in the
     * format specified by NPPES
     *
     * @return array
     */
    public function getNppes()
    {
        $sql = sprintf(
            "SELECT DISTINCT
            '13' AS `CMS27410200_BHT02_TransactionSetPurposeCode`,
            'U5' AS `CMS27410200_BHT06_TransactionTypeCode`,
            '111431' AS `CMS27420300_2100AA_NM109_SubmitterIdentifier`,
            '' AS `CMS27420100_2000C_TRN02_ReferenceIdentification`,
            '1' AS `CMS27420300_2100CA_NM102_EntityTypeQualifier`,
            provider.last_name AS `CMS27420300_2100CA_NM103_ProviderLastOrOrganizationName`,
            provider.first_name AS `CMS27420300_2100CA_NM104_ProviderFirstName`,
            provider.middle_name AS `CMS27420300_2100CA_NM105_ProviderMiddleName`,
            provider.prefix AS `CMS27420300_2100CA_NM106_ProviderNamePrefix`,
            '' AS `CMS27420300_2100CA_NM107_ProviderNameSuffix`,
            'XX' AS `CMS27420300_2100CA_NM108_IdentificationCodeQualifier`,
            provider.npi_id AS `CMS27420300_2100CA_NM109_ProviderIdentifier`,
            '' AS `CMS27420300_2100CA_NM110_ProviderNewOrganizationName`,
            '' AS `CMS27420400_2100CA_N202_ProviderDoingBusinessAsOrTrade Name`,
            'N' AS `CMS27421500_2100CA_PSI01_IsSubpart`,
            '' AS `CMS27421500_2100CA_PSI02_LegalBusinessName`,
            '' AS `CMS27421500_2100CA_PSI03_ParentTaxIdentificationNumber`,
            'EM' AS `CMS27420500_2100CA_PER03_CommunicationNumberQualifier`,
            IF(organization_id IS NOT NULL AND (system_email IS NULL OR system_email=''),account.email,system_email)
            AS `CMS27420500_2100CA_PER04_CommunicationNumber`,
            'TE' AS `CMS27420500_2100CA_PER05_CommunicationNumberQualifier`,
            REPLACE(REPLACE(REPLACE(REPLACE(practice.phone_number,'(',''),')',''),'-',''),' ','')
            AS `CMS27420500_2100CA_PER06_CommunicationNumber`,
            '' AS `CMS27420500_2100CA_PER07_CommunicationNumberQualifier`,
            '' AS `CMS27420500_2100CA_PER08_CommunicationNumber`,
            IF(provider_details.date_birth IS NOT NULL AND provider_details.date_birth<>'0000-00-00',
            DATE_FORMAT(provider_details.date_birth,'%%m/%%d/%%Y'),'') AS `CMS27420600_2100CA_DMG02_ProviderBirthDate`,
            provider.gender AS `CMS27420600_2100CA_DMG03_ProviderGenderCode`,
            '' AS `CMS27420700_2100CA_API02_ActionCode`,
            '' AS `CMS27420700_2100CA_API04_StatusReasonCode`,
            credential.title AS `CMS27420900_2100CA_DEG04_ProviderProfessionalDesignation`,
            '' AS `CMS27421000_2100CA_IND01_CountryOfBirthCode`,
            '' AS `CMS27421000_2100CA_IND02_StateOrProvinceOfBirthCode`,
            '' AS `CMS27421200_2100CA_DTP03_DateTimePeriod`,
            '' AS `CMS27421400_2100CA_CRC03_ConditionIndicator`,
            '' AS `CMS27421400_2100CA_CRC05_ConditionDetail`,
            '' AS `CMS27421300_2100CA_MTX02_TextualData`,
            '31' AS `CMS27422100_2110CA_NX101_AddressTypeCode`,
            practice.address AS `CMS27422300_2110CA_N301_ProviderAddressLine`,
            practice.address_2 AS `CMS27422300_2110CA_N302_ProviderAddressLine`,
            city.name AS `CMS27422400_2110CA_N401_ProviderCityName`,
            state.abbreviation AS `CMS27422400_2110CA_N402_ProviderStateCode`,
            location.zipcode AS `CMS27422400_2110CA_N403_ProviderPostalZoneOrZipCode`,
            'US' AS `CMS27422400_2110CA_N404_ProviderCountryCode`,
            LEFT(sub_specialty.taxonomy_code,2) AS `CMS27422600_2120CA_LQ01_CodeListQualifierCode`,
            'Y' AS `CMS27422600_2120CA_LQ03_IsPrimaryTaxonomy`,
            sub_specialty.taxonomy_code AS `CMS27422600_2120CA_LQ02_ProviderSpecialityCode`,
            SUBSTRING(licence_number, LOCATE('-',licence_number) + 1, LENGTH(licence_number))
            AS `CMS27423200_2130CA_HPL02_ProviderIdentificationNumber`,
            IF(LOCATE('-',licence_number)<>'0',
            SUBSTRING(licence_number, 1,LOCATE('-',licence_number) - 1),
            '') AS `CMS27423200_2130CA_HPL04_IssuingStateCode`,
            'SY' AS `CMS27423400_2140CA_REF01_ReferenceIdentificationQualifier`,
            '' AS `CMS27423400_2140CA_REF02_ProviderIdentifier`,
            '' AS `CMS27423400_2140CA_REF03_Description`,
            '' AS `CMS27423400_2140CA_REF05_State`,
            'VI' AS `CMS27420300_2100CD_NM101_EntityIdentifierCode`,
            account.last_name AS `CMS27420300_2100CD_NM103_AffiliatedEntityLastOrOrganizationName`,
            account.first_name AS `CMS27420300_2100CD_NM104_AffiliatedEntityFirstName`,
            '' AS `CMS27420300_2100CD_NM105_AffiliatedEntityMiddleName`,
            '' AS `CMS27420300_2100CD_NM106_AffiliatedEntityNamePrefix`,
            '' AS `CMS27420300_2100CD_NM107_AffiliatedEntityNameSuffix`,
            'TE' AS `CMS27420500_2100CD_PER03_CommunicationNumberQualifier`,
                REPLACE(REPLACE(REPLACE(REPLACE(account.phone_number,'(',''),')',''),'-',''),' ','')
                AS `CMS27420500_2100CD_PER04_CommunicationNumber`,
            IF(account.fax_number IS NULL,'','FX') AS `CMS27420500_2100CD_PER05_CommunicationNumberQualifier`,
            IF(account.fax_number IS NULL,'',
            REPLACE(REPLACE(REPLACE(REPLACE(account.fax_number,'(',''),')',''),'-',''),' ',''))
            AS `CMS27420500_2100CD_PER06_CommunicationNumber`,
            '' AS `CMS27420500_2100CD_PER07_CommunicationNumberQualifier`,
            '' AS `CMS27420500_2100CD_PER08_CommunicationNumber`,
            '' AS `CMS27420600_2100CD_DEG04_Description`,
            'CONTACT' AS `CMS27421300_2100CD_MTX02_TextualData`,
            'TE' AS `CMS27420500_2100DA_PER03_CommunicationNumberQualifier`,
            REPLACE(REPLACE(REPLACE(REPLACE(practice.phone_number,'(',''),')',''),'-',''),' ','')
            AS `CMS27420500_2100DA_PER04_CommunicationNumber`,
            IF(practice.fax_number IS NULL,'','FX') AS `CMS27420500_2100DA_PER05_CommunicationNumberQualifier`,
            IF(practice.fax_number IS NULL,'',
                REPLACE(REPLACE(REPLACE(REPLACE(practice.fax_number,'(',''),')',''),'-',''),' ',''))
                AS `CMS27420500_2100DA_PER06_CommunicationNumber`,
            '' AS `CMS27420500_2100DA_PER07_CommunicationNumberQualifier`,
            '' AS `CMS27420500_2100DA_PER08_CommunicationNumber`,
            practice.address AS `CMS27422300_2110DA_N301_SiteOrLocationAddressLine`,
            practice.address_2 AS `CMS27422300_2110DA_N302_SiteOrLocationAddressLine`,
            city.name AS `CMS27422400_2110DA_N401_SiteOrLocationCityName`,
            state.abbreviation AS `CMS27422400_2110DA_N402_SiteOrLocationStateCode`,
            location.zipcode AS `CMS27422400_2110DA_N403_SiteOrLocationPostalZoneOrZipCode`,
            'US' AS `CMS27422400_2110DA_N404_SiteOrLocationCountryCode`,
            '' AS `CMS27422600_2120EA_LQ01_CodeListQualifierCode`,
            '' AS `CMS27422600_2120EA_LQ02_ProviderGroupSpecialtyCode`
            FROM provider
            INNER JOIN credential ON credential.id = provider.credential_id
            INNER JOIN provider_specialty ON provider_specialty.id =
                (SELECT id FROM provider_specialty WHERE provider_id= %d ORDER BY primary_specialty LIMIT 1)
            INNER JOIN sub_specialty ON sub_specialty.id= provider_specialty.sub_specialty_id
            INNER JOIN provider_practice ON provider_practice.provider_id = provider.id
            INNER JOIN practice ON practice.id = provider_practice.practice_id
            LEFT JOIN location ON location.id = practice.location_id
            LEFT JOIN city ON city.id=location.city_id
            LEFT JOIN state ON state.id = city.state_id
            LEFT JOIN provider_details ON provider.id = provider_details.provider_id
            LEFT JOIN account_provider ON account_provider.provider_id = provider.id
            LEFT JOIN account ON (account.id = account_provider.account_id AND account.enabled='1')
            LEFT JOIN organization_account
            ON (organization_account.account_id = account.id AND organization_account.connection='O')
            LEFT JOIN organization
            ON (organization.id = organization_account.organization_id AND organization.status='A')
            WHERE provider.id= %d
            AND provider_practice.primary_location='1'
            ORDER BY organization_id DESC
            LIMIT 1;",
            $this->id,
            $this->id
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Create or match a provider with base on an array based on the Marketplace PUF
     *
     * @param array $provider
     * @param int $sourceCompanyId
     * @return Provider
     */
    public static function createOrMatchMarketplace($provider, $sourceCompanyId)
    {
        $sourceDate = date_create_from_format('Y-m-d', $provider['last_updated_on']);

        // try to look up existing practices instead of creating new one
        $model = null;
        if ($provider['npi']) {
            $model = Provider::model()->find("npi_id = :npi_id", array(
                ":npi_id" => $provider['npi']
            ));
        } else {
            // only process records with npi
            return false;
        }

        if (!$model) {
            // import insurances only for matching providers
            return false;
        }

        if ($model) {
            $log = InsuranceMarketplaceLog::model()->find(
                "company_id = :company_id AND provider_id = :provider_id ORDER BY date_updated DESC",
                array(":company_id" => $sourceCompanyId, ":provider_id" => $model->id)
            );

            if ($log) {
                $logDate = date_create_from_format('Y-m-d', substr($log->date_updated, 0, 10));
                if ($sourceDate <= $logDate) {
                    return $model;
                }
            } else {
                if (!$provider['npi']) {
                    $model = new Provider();
                }
            }
        } else {
            $model = new Provider();
        }

        $isNewProvider = $model->isNewRecord;
        if ($isNewProvider) {
            // map provider data into the DB
            $model->country_id = 1;
            $model->localization_id = 1;
            $model->first_last = $provider['name']['first'] . ' ' . $provider['name']['last'];
            $model->first_m_last = $provider['name']['first'] . ' ' .
                isset($provider['name']['middle']) && !empty($provider['name']['middle']) ?
                strtoupper(substr($provider['name']['middle'], 1)) . '.' : '' . $provider['name']['last'];
            $model->first_middle_last = $provider['name']['first'] . ' ' .
                isset($provider['name']['middle']) && !empty($provider['name']['middle']) ?
                $provider['name']['middle'] : '' . $provider['name']['last'];
            $model->npi_id = $provider['npi'];
            $model->gender = isset($provider['gender']) ? substr($provider['gender'], 0, 1) : 'M';
            // keep the provider's credential or set the blank credential by default.
            $model->credential_id = $model->credential_id ? $model->credential_id : 10213;
            $model->first_name = $provider['name']['first'];
            $model->middle_name = isset($provider['name']['middle']) ? $provider['name']['middle'] : '';
            $model->last_name = $provider['name']['last'];
            $model->npi_id = $provider['npi'];
            $model->prefix = '';
            $model->suffix = isset($provider['name']['suffix']) ? $provider['name']['suffix'] : '';
            $model->accepts_new_patients = isset($provider['name']['accepting']) &&
                $provider['name']['accepting'] == 'accepting' ? 1 : 0;

            // calculate friendly_url
            $model->friendly_url = $model->friendly_url ? $model->friendly_url : $model->getFriendlyUrl(
                $model->prefix,
                $model->first_name,
                $model->last_name
            );

            // import specialty information
            $model->additional_specialty_information = implode(
                ',',
                (isset($provider['specialty'])
                    ? $provider['specialty']
                    : (isset($provider['speciality']) ? $provider['speciality'] : array()))
            );
            $model->save();

            $log = new InsuranceMarketplaceLog();
            $log->company_id = $sourceCompanyId;
            $log->provider_id = $model->id;
            $log->npi_id = $model->npi_id ? $model->npi_id : null;
            $log->date_updated = $provider['last_updated_on'];
            $log->date_processed = date("Y-m-d H:i:s");
            $log->json = json_encode($provider);
            $log->save();
        }

        if (!$model->hasErrors()) {

            if ($isNewProvider) {
                // import provider_practice
                foreach ($provider['addresses'] as $address) {
                    $practice = Practice::model()->find(
                        "phone_number = :phone_number AND address = :address",
                        array(":phone_number" => $address['phone'], ":address" => $address["address"])
                    );

                    if (!$practice && $address['phone']) {
                        $practice = Practice::model()->find(
                            "phone_number = :phone_number",
                            array(":phone_number" => $address['phone'])
                        );
                    }

                    if ($practice && $practice->location->zipcode != $address['zip']) {
                        $practice = null;
                    }

                    if (!$practice) {
                        $arrPractice = array();
                        $practice = Practice::model()->createOrFetchPractice(
                            (object)array(
                                "name" => "Practice at " . $address['address'],
                                "address" => $address['address'],
                                "zipcode" => $address['zip'],
                                "phone_number" => $address['phone'] != '0000000000' ? $address['phone'] : ''
                            ),
                            $arrPractice,
                            false
                        );
                    }

                    if (!$practice->id) {
                        $practice->save();
                    }

                    $pp = ProviderPractice::model()->find(
                        "provider_id = :provider_id AND practice_id = :practice_id",
                        array(":provider_id" => $model->id, ":practice_id" => $practice->id)
                    );
                    if (!$pp) {
                        $pp = new ProviderPractice();
                        $pp->provider_id = $model->id;
                        $pp->practice_id = $practice->id;
                        $pp->save();
                    }
                }
            }

            $log = new InsuranceMarketplaceLog();
            $log->company_id = $sourceCompanyId;
            $log->provider_id = $model->id;
            $log->practice_id = null;
            $log->npi_id = $model->npi_id ? $model->npi_id : null;
            $log->date_updated = $provider['last_updated_on'];
            $log->date_processed = date("Y-m-d H:i:s");
            $log->json = json_encode($provider);
            $log->save();

            // import provider_insurance
            if (isset($provider['plans'])) {
                foreach ($provider['plans'] as $plan) {
                    if (!isset($plan['plan_id'])) {
                        continue;
                    }

                    $insurance = Insurance::model()->cache(Yii::app()->params['cache_long'])->find(
                        "marketplace_id = :marketplace_id",
                        array(":marketplace_id" => $plan['plan_id'])
                    );
                    if ($insurance) {
                        $sql = sprintf(
                            "INSERT IGNORE INTO provider_insurance
                            (provider_id, company_id, insurance_id, date_added)
                            VALUES ('%d', '%d', '%d', '%s');",
                            $model->id,
                            $insurance->company_id,
                            $insurance->id,
                            date('Y-m-d H:i:s')
                        );
                        Yii::app()->db->createCommand($sql)->execute();
                    }
                }
            }
        }

        return $model;
    }

    /**
     * Determinate whether a provider belongs to an organization that's active and has AdEnhance enabled
     *
     * @param int $providerId
     * @return bool
     */
    public static function hasAdEnhance($providerId)
    {

        $sql = sprintf(
            "SELECT DISTINCT provider.id AS provider_id
            FROM organization
            INNER JOIN organization_feature
                ON organization.id = organization_feature.organization_id
                AND feature_id = 11
                AND active = 1
            INNER JOIN provider
                ON provider.id = organization_feature.provider_id
            INNER JOIN provider_practice
                ON provider.id = provider_practice.practice_id
            INNER JOIN practice
                ON provider_practice.practice_id = practice.id
            WHERE provider.id = %d
                AND organization.status = 'A'
                AND practice.country_id = 1
                AND provider.country_id = 1;",
            $providerId
        );
        return (bool)Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Find out whether a provider belongs to an organization
     *
     * @param int $providerId
     * @return mixed
     */
    public static function belongsToOrganizationNotCanceled($providerId)
    {

        $arrAccountProvider = AccountProvider::model()->findAll(
            'provider_id=:providerId',
            array(':providerId' => $providerId)
        );

        if (!$arrAccountProvider) {
            // not linked to an account so can't be linked to an organization
            return false;
        }

        // check each account
        foreach ($arrAccountProvider as $accountProvider) {
            $organizationAccount = OrganizationAccount::model()->find(
                'account_id=:accountId AND `connection` = "O"',
                array(':accountId' => $accountProvider->account_id)
            );

            if ($organizationAccount) {

                // this account was linked to an organization, so the provider belongs to an organization
                $organization = Organization::model()->findByPk($organizationAccount->organization_id);

                if ($organization && $organization->status != 'C') {
                    return $organization;
                }
            }
        }

        // no account was linked to an organization
        return false;
    }

    /**
     * Find out whether a provider belongs to an organization, active or not
     *
     * @param int $providerId
     * @return mixed
     */
    public static function belongsToOrganization($providerId)
    {

        // is this provider linked to at least an account?
        $arrAccountProvider = AccountProvider::model()->cache(Yii::app()->params['cache_medium'])->findAll(
            'provider_id=:providerId',
            array(':providerId' => $providerId)
        );

        if (!$arrAccountProvider) {
            // not linked to an account so can't be linked to an organization
            return false;
        }

        // check each account
        $arrOrganizations = array();
        foreach ($arrAccountProvider as $accountProvider) {
            // check for owner organizations of this account
            $arrOrganizationAccounts = OrganizationAccount::model()->cache(Yii::app()->params['cache_medium'])->findAll(
                'account_id=:accountId AND `connection` = "O"',
                array(':accountId' => $accountProvider->account_id)
            );
            if ($arrOrganizationAccounts) {
                foreach ($arrOrganizationAccounts as $organizationAccount) {
                    // build an array with them
                    $arrOrganizations[] = $organizationAccount->organization_id;
                }
            }
        }

        if (!empty($arrOrganizations)) {
            // return an array with all the organization ids that were linked
            return $arrOrganizations;
        }

        // no account was linked to an organization
        return false;
    }

    /**
     * Get average docpoints an organization has, to update SalesForce:
     * DDC_Docpoints__c
     *
     * @param int $organizationId
     * @return array
     */
    public static function updateSFOrganizationDocScore($organizationId)
    {

        $organization = Organization::model()->findByPk($organizationId);
        if (!$organization || empty($organization->salesforce_id)) {
            return false;
        }

        $value = self::getOrganizationDocScore($organizationId);

        return Yii::app()->salesforce->updateAccountField($organization->salesforce_id, 'DDC_Docpoints__c', $value);
    }

    /**
     * Find providers associated with 'O'-level Account
     *
     * @param int $ownerAccountId
     * @param bool $order
     * @param bool $cached
     * @param array $limit
     * @return array
     */
    public static function getOwnerProviders($ownerAccountId, $order = false, $cached = true, $limit = null)
    {

        // do we want to order the results?
        if ($order) {
            // yes
            $order = 'ORDER BY first_name, last_name';
        } else {
            // no
            $order = '';
        }

        if ($limit) {
            $pager = 'LIMIT ' . (($limit['page'] - 1) * $limit['limit']) . ', ' . $limit['limit'];
        } else {
            $pager = '';
        }

        $sql = sprintf(
            "SELECT provider.id, first_name, middle_name,
            last_name, gender, npi_id,
            friendly_url, nasp_date
            FROM provider
            INNER JOIN account_provider
            ON account_provider.provider_id = provider.id
            WHERE account_provider.account_id = %d
            %s
            %s;",
            $ownerAccountId,
            $order,
            $pager
        );

        // cached?
        // yes
        if ($cached) {
            // cache
            return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
        } else {
            // no cache
            return Yii::app()->db->createCommand($sql)->queryAll();
        }
    }

    /**
     * Get the average docscore for all providers in an organization
     *
     * @param int $organizationId
     * @return int
     */
    public static function getOrganizationDocScore($organizationId)
    {

        $sql = sprintf(
            "SELECT
                AVG(provider.docscore) AS docscore_average
            FROM
                organization
                INNER JOIN organization_account
                    ON organization.id = organization_account.organization_id AND `connection` = 'O'
                INNER JOIN account_provider
                    ON organization_account.account_id = account_provider.account_id
                INNER JOIN provider
                    ON account_provider.provider_id = provider.id
            WHERE
                organization.id = %d;",
            $organizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get data of the chosen provider for a request an appointment
     *
     * @param string $providerString
     * @return array
     */
    public static function getProviderDetailsForAppointment($providerString)
    {
        $sql = sprintf(
            'SELECT DISTINCT provider.id AS provID,
            CONCAT(first_name, " ", last_name) AS name,
            provider.photo_path, provider_details.no_insurance_reason, provider.gender,
            GROUP_CONCAT(
                CONCAT( specialty.name, " - ", sub_specialty.name )
                ORDER BY primary_specialty SEPARATOR ", "
                ) AS specialty,
            localization.code, provider_details.schedule_mail_enabled, 0 as is_venus_concept
            FROM provider
            LEFT JOIN provider_specialty
                ON provider.id = provider_specialty.provider_id
            LEFT JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
            LEFT JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
            INNER JOIN provider_details
                ON provider_details.provider_id = provider.id
            INNER JOIN localization
                ON localization.id = provider.localization_id
            WHERE provider.id IN (%s)
            AND provider.suspended != 1
            GROUP BY provider.id;',
            $providerString
        );

        $result = Yii::app()->db->createCommand($sql)->queryAll();

        if (!empty($result)) {
            foreach ($result as $key => $value) {
                $result[$key]['is_venus_concept'] = Provider::isVenusConcept($value['provID']);
            }
        }

        return $result;
    }

    /**
     * Return 1(true) if the provider is belongs to a Venus Concept,
     * otherwise, return 0(false)
     *
     * @param int $providerId
     * @return bool
     */
    public static function isVenusConcept($providerId = 0)
    {

        $venusConceptPartnerSiteId = 75;
        if ($providerId == 0) {
            return 0;
        }

        $sql = sprintf(
            "SELECT provider.id AS provider_id, provider.first_m_last
            FROM provider
            INNER JOIN account_provider
                ON account_provider.provider_id = provider.id
            INNER JOIN organization_account
                ON organization_account.account_id = account_provider.account_id
            INNER JOIN organization
                ON organization.id = organization_account.organization_id
            WHERE organization.partner_site_id = %d
            AND organization_account.connection = 'O'
            AND provider.id = %d;",
            $venusConceptPartnerSiteId,
            $providerId
        );
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        if (!empty($result['provider_id'])) {
            return 1;
        }
        return 0;
    }

    /**
     * Get data of the chosen provider for DirectReview
     *
     * @param string $providerString
     * @return array
     */
    public static function getProviderDetailsForDirectReview($providerString)
    {
        $providerIds = explode(',', $providerString);
        $placeHolders = [];
        if ($providerIds) {
            foreach ($providerIds as $key => $pid) {
                $placeHolders[] = ':provider_id_' . $key;
            }
        }
        $providerIdsCondition = implode(', ', $placeHolders);

        $sql = sprintf(
            'SELECT DISTINCT provider.id AS provID,
            CONCAT(first_name, " ", last_name) AS name,
            CONCAT(prefix, ".", first_m_last) AS full_name,
            provider.photo_path, provider_details.no_insurance_reason, provider.gender,
            GROUP_CONCAT(DISTINCT(specialty.name) SEPARATOR ", ") AS specialty,
            localization.code
            FROM provider
            LEFT JOIN provider_specialty
                ON provider.id = provider_specialty.provider_id
            LEFT JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
            LEFT JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
            LEFT JOIN provider_details
                ON provider_details.provider_id = provider.id
            INNER JOIN localization
                ON localization.id = provider.localization_id
            WHERE provider.id IN (%s)
                AND provider.suspended != 1
            GROUP BY provider.id;',
            $providerIdsCondition
        );

        $query = Yii::app()->db->createCommand($sql);
        foreach ($providerIds as $key => $pid) {
            $query->bindValue($placeHolders[$key], $pid);
        }

        return $query->queryAll();
    }

    /**
     * Return average docscore of all providers based on criteria
     *
     * @param array $criteria
     * @return float
     */
    private function getAverageDocscore($criteria)
    {
        return DoctorIndex::calculateAverage($criteria, 'docscore');
    }

    /**
     * Return average year all providers began practicing based on criteria
     *
     * @param array $criteria
     * @return float
     */
    private function getAverageYearBeganPracticing($criteria)
    {
        return DoctorIndex::calculateAverage($criteria, 'year_began_practicing');
    }

    /**
     * Return Twilio for a provider in a certain partner
     *
     * @param int $providerId
     * @param int $partnerId
     * @return string
     */
    public static function getProviderTwilio($providerId, $partnerId)
    {
        $sql = sprintf(
            'SELECT practice_id
            FROM provider_practice
            WHERE provider_id = %d
            AND primary_location = 1',
            $providerId
        );
        $providerPractice = Yii::app()->db->createCommand($sql)->queryRow();

        $sql = sprintf(
            'SELECT twilio_number
            FROM twilio_number
            WHERE practice_id = %d
            AND twilio_partner_id = %d',
            $providerPractice['practice_id'],
            $partnerId
        );
        $result = Yii::app()->db->createCommand($sql)->queryRow();

        return $result['twilio_number'];
    }

    /**
     * Return provider's phone number in a predefine cascade order
     *
     * @param int $providerId
     * @return string
     */
    public static function getProviderPhone($providerId)
    {
        $sql = sprintf(
            'SELECT phone_number
            FROM provider_practice
            WHERE provider_id = %d
            AND primary_location = 1',
            $providerId
        );
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        if ($result['phone_number'] && $result['phone_number'] != '') {
            return $result['phone_number'];
        }

        $sql = sprintf(
            'SELECT phone_number
            FROM practice
            WHERE id = %d',
            $result['practice_id']
        );
        $result = Yii::app()->db->createCommand($sql)->queryRow();

        return $result['phone_number'];
    }

    /**
     * Get all providers with an incorrect verified flag, $verified can be 1 to get the verified providers that must be
     * not verified or 0 for get the providers that are not verified but must be verified
     *
     * @param int $verified
     * @return array
     */
    public static function getProvidersWithIncorrectVerifiedFlag($verified)
    {

        if ($verified == 1) {
            $sql = 'SELECT DISTINCT provider.id
                    FROM provider
                    LEFT JOIN account_provider ON account_provider.provider_id = provider.id
                    LEFT JOIN account ON account.id = account_provider.account_id
                    LEFT JOIN organization_account ON organization_account.account_id = account.id
                        AND organization_account.`connection` = "O"
                    LEFT JOIN organization ON organization.id = organization_account.organization_id
                    WHERE provider.verified = 1
                    AND (((account.date_added < DATE_SUB(NOW(), INTERVAL 90 DAY)
                        OR account.date_signed_in < DATE_SUB(NOW(), INTERVAL 90 DAY))
                        AND organization.`id` IS NULL)
                           OR organization.`status` NOT IN ("A","P")
                           OR account.id IS NULL
                           OR account.enabled = 0)
                    AND NOT EXISTS (SELECT 1
                            FROM account_provider
                            INNER JOIN account ON (account.id = account_provider.account_id AND account.enabled = 1)
                            LEFT JOIN organization_account ON (organization_account.account_id = account.id
                                AND organization_account.`connection` = "O")
                            LEFT JOIN organization ON organization.id = organization_account.organization_id
                            WHERE account_provider.provider_id = provider.id
                            AND (organization.`status` IN ("A","P")
                            OR
                                (account.date_added >= DATE_SUB(NOW(), INTERVAL 90 DAY)
                                OR  account.date_signed_in >= DATE_SUB(NOW(), INTERVAL 90 DAY)))
                            )';
        } else {
            $sql = 'SELECT DISTINCT p.id
                    FROM provider p
                    JOIN account_provider ap ON p.id = ap.provider_id
                    JOIN account a ON ap.account_id = a.id and a.enabled = 1
                    LEFT JOIN organization_account oa ON a.id = oa.account_id AND oa.`connection` = "O"
                    LEFT JOIN organization o ON oa.organization_id = o.id AND o.status in ("A","P")
                    WHERE (p.verified = 0 OR p.verified IS NULL)
                    AND  ((a.date_added >= DATE_SUB(NOW(), INTERVAL 90 DAY)
                            OR a.date_signed_in >= DATE_SUB(NOW(), INTERVAL 90 DAY))
                          OR o.id is not null)';
        }

        return Yii::app()->db->createCommand($sql)->queryColumn();
    }

    /**
     * Return an array with needed data/format for the dashboard
     *
     * @param array $params
     * @param bool $useCache
     * @return  array
     */
    public static function prepareDataForDashboard($params = array(), $useCache = true)
    {

        $pageSize = $params['pageSize'];
        $providerPageFrom = $params['actualPage'];
        $accountInfo = $params['accountInfo'];
        $accountId = $params['accountId'];

        $arrProviders = array();
        $providerList = array();
        $lstProviders = '';
        if (!empty($accountInfo['providers'])) {
            $internalPage = $providerPageFrom - 1;
            $from = $internalPage * $pageSize;
            $to = $from + $pageSize;
            $i = 0;
            foreach ($accountInfo['providers'] as $k => $v) {
                if ($i >= $from && $i < $to) {
                    $lstProviders .= $k . ',';
                    $providerList[] = $v;
                }
                $i++;
                if ($i > $to) {
                    break;
                }
            }
            $lstProviders = rtrim($lstProviders, ',');
            // get providers details
            $arrProviders = Provider::getProviderDetailsFromList($lstProviders, $useCache);

            foreach ($arrProviders as $k => $v) {
                $providerId = $v['id'];
                // get other practices for this provider
                $practices = ProviderPractice::model()->findAll(
                    'provider_id=:provider_id',
                    array(':provider_id' => $providerId)
                );
                $practicesList = '';
                if (!empty($practices)) {
                    foreach ($practices as $v2) {
                        $practicesList .= $v2->practice_id . ',';
                    }
                }

                // prepare providers links
                $arrProviders[$k]['href']['profile'] = Yii::app()->createUrl(
                    'myaccount/provider',
                    array('id' => $providerId)
                );
                $arrProviders[$k]['href']['ratings'] = Yii::app()->createUrl(
                    'myaccount/reviewReports',
                    array('provider_id' => $providerId)
                );
                $arrProviders[$k]['href']['dashboard'] = Yii::app()->createUrl(
                    'myaccount/dashboard',
                    array('provider_id' => $providerId)
                );
                $arrProviders[$k]['href']['appointment'] = Yii::app()->createUrl(
                    '/myaccount/onlinePracticeAppointmentRequests',
                    array('filterProvider' => $providerId)
                );
                $arrProviders[$k]['href']['unlink'] = Yii::app()->createUrl(
                    '/myaccount/unlinkProvider',
                    array('id' => $providerId)
                );

                $arrProviders[$k]['practices'] = trim($practicesList, ',');

                // get other owners
                $arrProviders[$k]['otherOwners'] = AccountProvider::model()->getOtherOwners($providerId, $accountId);
            }
        }
        return $arrProviders;
    }

    /**
     * Get providers without specialty
     *
     * @param integer $accountId
     * @return array
     */
    public static function getProvidersWithoutSpecialty($accountId)
    {
        $sql = sprintf(
            'SELECT p.id, p.first_m_last, p.friendly_url
            FROM provider p
            JOIN account_provider ap
                ON ap.provider_id = p.id
            LEFT JOIN provider_specialty ps
                ON ps.provider_id = p.id
            WHERE ap.account_id = %d
                AND ps.id IS NULL',
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get provider ids from an account filtering by external
     * provider id.
     *
     * @param integer $accountId
     * @param array $arrProviderExternalIds
     * @return array
     */
    public static function getNonExternalProvidersFromAccount($accountId, $arrProviderExternalIds)
    {
        $inCondition = "'" . join("','", $arrProviderExternalIds) . "'";
        $sql = sprintf(
            "SELECT DISTINCT p.id
            FROM provider p
            JOIN account_provider ap
                ON ap.provider_id = p.id
            LEFT JOIN organization_import_log oil
                ON oil.provider_id = p.id
            WHERE ap.account_id = %d
                AND (oil.external_provider_id NOT IN (%s) OR oil.external_provider_id IS NULL)",
            $accountId,
            $inCondition
        );
        return Yii::app()->db->createCommand($sql)->queryColumn();
    }

    /**
     * Return a JSONP callback to inject the given provider's bio into the specified container.
     * It retrieves the brief_bio if the full bio is empty.
     *
     * @param int $providerId
     * @param string $container
     * @return string
     */
    public static function getBioCallback($providerId, $container)
    {
        // get provider
        $provider = Provider::model()->findByPK((int)$providerId);

        if ($provider) {
            // return formatted JSONP string
            return "DoctorProvider.bioCallback('" .
                urlencode(empty($provider->bio) ? $provider->brief_bio : $provider->bio) .
                "', '" . $container . "');";
        } else {
            // if no provider was found, return empty callback.
            return "DoctorProvider.bioCallback('', '" . $container . "');";
        }
    }

    /**
     * Fetch the last 1000 providers that were updated (after a given date)
     *
     * @param string $dateUpdated
     * @return array
     */
    public static function getProvidersForRSSFeed($dateUpdated = null)
    {

        // should we fetch only entries updated after certain time?
        $dateUpdatedCondition = '';
        if (!empty($dateUpdated)) {
            // yes, build condition
            $dateUpdatedCondition = sprintf("WHERE provider.date_updated > '%s'", $dateUpdated);
        }

        // build actual query
        $sql = sprintf(
            "SELECT provider.prefix, provider.first_name, provider.last_name,
            credential.title, provider.friendly_url, provider.bio,
            provider.brief_bio, CONCAT(specialty.name, ' - ', sub_specialty.name) AS specialty,
            provider.date_updated
            FROM provider
            INNER JOIN provider_specialty
            ON provider.id = provider_specialty.provider_id
            INNER JOIN sub_specialty
            ON provider_specialty.sub_specialty_id = sub_specialty.id
            INNER JOIN specialty
            ON sub_specialty.specialty_id = specialty.id
            LEFT JOIN credential
            ON provider.credential_id = credential.id
            %s
            GROUP BY provider.id
            ORDER BY provider.date_updated DESC, provider_specialty.primary_specialty
            LIMIT 1000;",
            $dateUpdatedCondition
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Searches for a provider by non-id parameters.
     *
     * @param array $params Possible params: full_name, name, address, phone, zip
     *
     * @return array
     */
    public function searchByData($params)
    {
        // Add more fields to select if needed
        $sql = 'SELECT DISTINCT
                  provider.id,
                  provider.npi_id,
                  provider.first_name,
                  provider.middle_name,
                  provider.last_name,
                  provider.first_m_last,
                  provider.prefix,
                  provider.suffix,
                  organization_feature.feature_id,
                  credential.title AS credential_title,
                  practice.phone_number AS practice_phone,
                  provider_practice.phone_number AS provider_practice_phone,
                  practice.address AS practice_address,
                  location.zipcode,
                  location.latitude,
                  location.longitude,
                  practice.latitude AS practice_latitude,
                  practice.longitude AS practice_longitude,
                  city.name AS city_name,
                  state.name AS state_name,
                  state.abbreviation AS state_abbreviation,
                  sub_specialty.name AS primary_specialty,
                  sub_specialty.generic_category AS primary_specialty_category,
                  sub_specialty.nucc_grouping AS primary_specialty_nucc_grouping,
                  sub_specialty.nucc_classification AS primary_specialty_nucc_classification,
                  sub_specialty.nucc_specialization AS primary_specialty_nucc_specialization
                FROM practice
                  INNER JOIN provider_practice ON practice.id=provider_practice.practice_id
                  LEFT JOIN provider ON provider_practice.provider_id=provider.id
                  LEFT JOIN location ON practice.location_id=location.id
                  LEFT JOIN city ON location.city_id = city.id
                  LEFT JOIN state ON city.state_id = state.id
                  LEFT JOIN credential ON provider.credential_id = credential.id
                  LEFT JOIN organization_feature ON (
                    organization_feature.provider_id = provider.id
                    AND organization_feature.feature_id = 18
                    AND organization_feature.active = 1
                  )
                  LEFT JOIN provider_specialty ON (
                      provider_specialty.provider_id = provider.id
                      AND provider_specialty.primary_specialty = 1
                  )
                  LEFT JOIN sub_specialty ON provider_specialty.sub_specialty_id = sub_specialty.id
                WHERE
                  (
                    provider.first_last = :first_last
                    OR provider.first_m_last = :first_m_last
                    OR provider.first_middle_last = :first_middle_last
                    OR provider.first_m_last = :first_m_last_alpha
                    OR provider.first_middle_last = :first_middle_last_alpha
                  )
                  AND provider.suspended="0"
                GROUP BY practice.id, provider.id
                ORDER BY
                    practice.name LIKE :name DESC,
                    practice.address LIKE :address DESC,
                    location.zipcode = :zip DESC,
                    practice.phone_number_clean = :phone DESC';

        $alphanumName = preg_replace("/[^0-9a-zA-Z ]/m", "", $params['full_name']);

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValue(':first_last', $params['full_name'], PDO::PARAM_STR)
            ->bindValue(':first_m_last', $params['full_name'], PDO::PARAM_STR)
            ->bindValue(':first_m_last_alpha', $alphanumName, PDO::PARAM_STR)
            ->bindValue(':first_middle_last', $params['full_name'], PDO::PARAM_STR)
            ->bindValue(':first_middle_last_alpha', $alphanumName, PDO::PARAM_STR)
            ->bindValue(':name', $params['name'], PDO::PARAM_STR)
            ->bindValue(':address', $params['address'], PDO::PARAM_STR)
            ->bindValue(':phone', $params['phone'], PDO::PARAM_STR)
            ->bindValue(':zip', $params['zip'], PDO::PARAM_STR)
            ->queryAll();
    }

    public function getDataForCompetitors($npi)
    {
        // Add more fields to select if needed
        $sql = 'SELECT DISTINCT
                  provider.id,
                  practice.id AS practice_id,
                  provider.npi_id,
                  provider.first_name,
                  provider.middle_name,
                  provider.last_name,
                  provider.first_m_last,
                  provider.prefix,
                  provider.suffix,
                  credential.title AS credential_title,
                  practice.phone_number AS practice_phone,
                  provider_practice.phone_number AS provider_practice_phone,
                  practice.address AS practice_address,
                  location.zipcode,
                  location.latitude,
                  location.longitude,
                  practice.latitude AS practice_latitude,
                  practice.longitude AS practice_longitude,
                  city.name AS city_name,
                  state.name AS state_name,
                  state.abbreviation AS state_abbreviation,
                  sub_specialty.name AS primary_specialty,
                  sub_specialty.generic_category AS primary_specialty_category,
                  sub_specialty.nucc_grouping AS primary_specialty_nucc_grouping,
                  sub_specialty.nucc_classification AS primary_specialty_nucc_classification,
                  sub_specialty.nucc_specialization AS primary_specialty_nucc_specialization
                FROM practice
                  INNER JOIN provider_practice ON practice.id=provider_practice.practice_id
                  LEFT JOIN provider ON provider_practice.provider_id=provider.id
                  LEFT JOIN location ON practice.location_id=location.id
                  LEFT JOIN city ON location.city_id = city.id
                  LEFT JOIN state ON city.state_id = state.id
                  LEFT JOIN credential ON provider.credential_id = credential.id
                  LEFT JOIN provider_specialty ON (
                      provider_specialty.provider_id = provider.id
                      AND provider_specialty.primary_specialty = 1
                  )
                  LEFT JOIN sub_specialty ON provider_specialty.sub_specialty_id = sub_specialty.id
                WHERE provider.npi_id = :npi
                GROUP BY practice.id, provider.id
                ORDER BY provider_practice.primary_location DESC';

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValue(':npi', $npi, PDO::PARAM_STR)
            ->queryRow();
    }

    /**
     * Searches for a provider by npi for PDT
     *
     * @param mixed $id
     *
     * @return array
     */
    public function searchByIdForScan($id)
    {
        // Add more fields to select if needed
        $sql = 'SELECT
                  provider.id,
                  provider.npi_id,
                  provider.first_name,
                  provider.middle_name,
                  provider.last_name,
                  provider.prefix,
                  provider.suffix,
                  organization_feature.feature_id,
                  credential.title AS credential_title
                FROM provider
                  LEFT JOIN credential ON provider.credential_id = credential.id
                  LEFT JOIN organization_feature ON (
                    organization_feature.provider_id = provider.id
                    AND organization_feature.feature_id = 18
                    AND organization_feature.active = 1
                  )
                WHERE provider.id = :id
                      LIMIT 1';

        return Yii::app()->db
            ->createCommand($sql)
            ->bindValue(':id', $id, PDO::PARAM_STR)
            ->queryRow();
    }

    /**
     * Get the scanned data for a provider/practice pair.
     *
     * @param integer $providerId
     * @param integer $practiceId
     * @return array
     */
    public function getScanData($providerId, $practiceId)
    {
        $valuesToBind = array(
            ':provider_id' => $providerId
        );
        $order = " ORDER BY provider_practice.primary_location DESC ";
        if ($practiceId) {
            $order = " ORDER BY practice.id = :practice_id IS TRUE DESC ";
            $valuesToBind[":practice_id"] = $practiceId;
        }

        $sql = "SELECT
                  provider.first_last AS name, practice.address, practice.phone_number,
                  provider.friendly_url, provider.featured,provider_photo.photo_path,
                  provider.id AS provID, COUNT(provider_rating.id) as review_number,
                  AVG(provider_rating.rating) as review_average,
                  practice.latitude,
                  practice.longitude,
                  location.zipcode,
                  city.name AS city,
                  state.name AS state_full,
                  state.abbreviation AS state,
                  sub_specialty.name AS sub_specialty,
                  sub_specialty.generic_category AS sub_specialty_category,
                  specialty.name AS specialty
                FROM practice
                  INNER JOIN provider_practice ON practice.id = provider_practice.practice_id
                  LEFT JOIN provider ON provider_practice.provider_id = provider.id
                  LEFT JOIN location ON practice.location_id = location.id
                  LEFT JOIN city ON location.city_id = city.id
                  LEFT JOIN state ON city.state_id = state.id
                  LEFT JOIN provider_photo ON provider.id = provider_photo.provider_id
                  LEFT JOIN provider_rating
                    ON (
                      provider_rating.provider_id = provider_practice.provider_id
                      AND provider_rating.status = 'A' AND provider_rating.origin = 'D'
                    )
                  LEFT JOIN provider_specialty ON (
                      provider_specialty.provider_id = provider.id
                      AND provider_specialty.primary_specialty = 1
                  )
                  LEFT JOIN sub_specialty ON provider_specialty.sub_specialty_id = sub_specialty.id
                  LEFT JOIN specialty ON sub_specialty.specialty_id = specialty.id
                WHERE
                    provider.id = :provider_id
                    AND provider.suspended='0'
                GROUP BY practice.id, provider.id, provider_photo.photo_path
                %s
                LIMIT 1";
        $sql = sprintf($sql, $order);

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValues($valuesToBind)
            ->queryRow();
    }

    /**
     * Get a provider object given a NPI
     *
     * @param int $npi
     *
     * @return Provider
     */
    public function fetchByNPI($npi)
    {
        return Provider::model()->find(
            'npi_id = :npi',
            [':npi' => $npi]
        );
    }

    /**
     * Get a provider object given a NPI
     *
     * @param int $npi
     * @param float $lat
     * @param float $lng
     *
     * @return Provider
     */
    public function getCompetitors($specialties, $lat = null, $lng = null)
    {
        $specialtiesCondition = "";
        if ($specialties) {
            $specialtiesCondition = "AND (";
            foreach ($specialties as $key => $specialty) {
                if ($key !== 0) {
                    $specialtiesCondition .= " OR ";
                }
                $specialtiesCondition .= "sub_specialty.name LIKE '%$specialty%'";
            }
            $specialtiesCondition .= ")";
        }

        $sql = "SELECT
                practice.id,
                practice.name,
                provider.first_middle_last,
                provider.npi_id,
                ( 3959 * acos( cos( radians(:lat) ) * cos( radians( latitude ) )
                * cos( radians( longitude ) - radians(:lng) ) + sin( radians(:lat2) ) * sin(radians(latitude)) ) )
                AS distance
            FROM practice
                JOIN provider_practice ON provider_practice.practice_id = practice.id
                JOIN provider ON provider_practice.provider_id = provider.id
                JOIN account_practice ON account_practice.practice_id = practice.id
                JOIN account ON account_practice.account_id = account.id
                JOIN organization_account ON organization_account.account_id = account.id
                JOIN organization ON organization_account.organization_id = organization.id
                JOIN scan_request ON scan_request.organization_id = organization.id
                JOIN provider_specialty ON provider_specialty.provider_id = provider.id
                JOIN sub_specialty ON provider_specialty.sub_specialty_id = sub_specialty.id
            WHERE
                practice.name NOT LIKE 'Practice At%'
                AND provider.verified = 1
                AND organization.status = 'A'
                $specialtiesCondition
            GROUP BY provider.id
            HAVING distance < 25
            ORDER BY distance LIMIT 30";

        $valuesToBind = [
            ':lat' => $lat,
            ':lat2' => $lat,
            ':lng' => $lng
        ];

        try {
            $results = Yii::app()->db->createCommand($sql)
                ->bindValues($valuesToBind)
                ->queryAll();
        } catch (Exception $e) {
            print_r($e);
            exit;
        }


        return $results;
    }

    /**
     * Get a provider from Medicare given a NPI
     *
     * @param int $npi
     *
     * @return array
     */
    public function getFromMedicareByNPI($npi)
    {
        $sql = "SELECT * FROM physician_medicare_data WHERE NPI = :npi";

        return Yii::app()->db->createCommand($sql)
            ->bindValue(':npi', $npi)
            ->queryAll();
    }

    /**
     * Get a provider from Medicare given a NPI
     *
     * @param int $npi
     *
     * @return array
     */
    public function searchMedicare($params)
    {
        $sql = "SELECT * FROM physician_medicare_data pmd WHERE
                pmd.zip LIKE :zip
                AND pmd.lst_nm = :last_name
                AND pmd.frst_nm LIKE :first_name_like
                ORDER BY
                    pmd.adr_ln_1 LIKE :address_like DESC,
                    pmd.phn_numbr = :phone DESC
                LIMIT 1";

        return Yii::app()->db->createCommand($sql)
            ->bindValues([
                ':zip' => $params['zip'] . "%",
                ':last_name' => $params['last_name'],
                ':first_name_like' => "%" . $params['name'] . '%',
                ':address_like' => "%" . $params['address'] . "%",
                ':phone' => (int)$params['phone'],
            ])
            ->queryAll();
    }

    /**
     * Return a normalized bio prepared to be saved in brief bio.
     *
     * @param string $bio
     * @param int $maxLength
     * @return string
     */
    public static function normalizeBriefBio($bio, $maxLength = 170)
    {
        // remove html tags
        $briefBio = strip_tags($bio);

        // remove line breaks
        $briefBio = str_replace('{line break}', ' ', $briefBio);

        // replace specific abbreviations
        $briefBio = str_replace(['Dr.', 'M.D.', '"'], ['Dr', 'MD', ''], $briefBio);

        // remove dot from all single letter abbreviation (middle names)
        $briefBio = preg_replace('/ ([A-Z])\. /', ' $1 ', $briefBio);
        $briefBio = preg_replace('/^([A-Z])\. /', ' $1 ', $briefBio);

        // convert multiple spaces into only one space
        $briefBio = preg_replace('/([ ]+)/', ' ', $briefBio);

        // truncate bio
        $briefBio = StringComponent::truncateParagraph($briefBio, $maxLength);

        // return the brief bio
        return $briefBio;
    }

    /**
     * Return array with the summary of a provider
     *
     * @param int $providerId
     * @return array
     */
    public static function getProviderSummary($providerId)
    {
        $sql = sprintf(
            "SELECT p.npi_id, p.gender, p.prefix, p.first_name, p.middle_name, p.last_name, p.suffix,
                p.first_m_last, d.title, p.photo_path, ss.name AS sub_specialty, s.name AS specialty, q.address,
                c.name AS city, l.zipcode, t.abbreviation AS state
            FROM provider p
            INNER JOIN credential d ON p.credential_id = d.id
            INNER JOIN provider_practice pp ON p.id = pp.provider_id AND pp.primary_location = 1
            INNER JOIN practice q ON q.id = pp.practice_id
            INNER JOIN provider_specialty ps ON p.id = ps.provider_id
            INNER JOIN sub_specialty ss ON ps.sub_specialty_id = ss.id
            INNER JOIN specialty s ON ss.specialty_id = s.id
            INNER JOIN location l ON q.location_id = l.id
            INNER JOIN city c ON l.city_id = c.id
            INNER JOIN state t ON c.state_id = t.id
            WHERE p.id = %d
            AND ps.primary_specialty = 1;",
            (int)$providerId
        );
        $result = Yii::app()->db->createCommand($sql)->queryRow();


        $result['real_photo_path'] = !empty($result['photo_path']) ? 'https://' .
            Yii::app()->params->servers['dr_ast_hn'] . '/img?p=providers/' . $result['photo_path'] . '&w=141' : null;
        return $result;
    }

    public function getFullName()
    {
        return $this->first_m_last;
    }

    public function getPrefixedFullName()
    {
        $name = $this->getFullName();
        if ($this->prefix) {
            if (substr($this->prefix, -1) === '.') {
                $name = $this->prefix . " " . $name;
            } else {
                $name = $this->prefix . ". " . $name;
            }
        }

        return $name;
    }

    public function getPhotoUrl($params = ['w' => 141])
    {
        if (empty($this->photo_path)) {
            return null;
        }

        $url = 'https://' . Yii::app()->params->servers['dr_ast_hn'] . '/img?p=providers/' .
            urlencode($this->photo_path);

        if ($params) {
            $querystring = http_build_query($params);
            $url .= '&' . $querystring;
        }

        return $url;
    }

    public function getUrl()
    {
        $url = '';
        if ($this->friendly_url) {
            $url = Yii::app()->params['site_url'] . '/' . $this->friendly_url;
        }

        return $url;
    }

    public function getPrimarySpecialty()
    {
        return ProviderSpecialty::model()
            ->with('subSpecialty')
            ->with('subSpecialty.specialty')
            ->findByAttributes([
                'provider_id' => $this->id,
                'primary_specialty' => '1'
            ]);
    }
}
