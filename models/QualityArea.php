<?php

Yii::import('application.modules.core_models.models._base.BaseQualityArea');

class QualityArea extends BaseQualityArea
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // delete records from provider_treatment_performed before delete this quality_area id
        $arrProviderTreatmentPerformed = ProviderTreatmentPerformed::model()->findAll(
            "quality_area_id = :quality_area_id",
            array(
                ":quality_area_id" => $this->id
            )
        );
        foreach ($arrProviderTreatmentPerformed as $providerTreatmentPerformed) {
            if (!$providerTreatmentPerformed->delete()) {
                $this->addError("id", "Couldn't delete treatments.");
                return false;
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Find quality area IDs by matching against name
     * @param string $q
     * @return array
     */
    public static function findByName($q)
    {
        $qa = QualityArea::model()->cache(Yii::app()->params['cache_medium'])->find(
            "name = :q AND quality_path_description IS NULL",
            array(":q" => $q)
        );
        if ($qa) {
            return $qa->id;
        }
        return null;
    }

    /**
     * Find quality path area IDs by matching against name
     * @param string $q
     * @return array
     */
    public static function findQualityPathAreaByName($q)
    {
        $qa = QualityArea::model()->cache(Yii::app()->params['cache_medium'])->find(
            "quality_path_description = :q",
            array(":q" => $q)
        );
        if ($qa) {
            return $qa->id;
        }
        return null;
    }

    /**
     * Autocomplete for treatments based on name and code
     * @param string $q
     * @param int $limit
     * @return array
     */
    public function autocomplete($q, $limit)
    {

        $q = trim($q);

        $sql = "SELECT SQL_NO_CACHE DISTINCT CONCAT('Q-', quality_area.id) AS id, quality_area.name AS name
            FROM quality_area
            INNER JOIN cost_quality ON cost_quality.quality_area_id = quality_area.id
            WHERE quality_path_description IS NULL quality_area.name LIKE '" . $q . "%'
            ORDER BY name
            LIMIT " . $limit . ";";

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

}
