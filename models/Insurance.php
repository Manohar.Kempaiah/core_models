<?php

Yii::import('application.modules.core_models.models._base.BaseInsurance');

/**
 * Insurance model
 * @property ProviderInsurance[] $providerInsurances
 */
class Insurance extends BaseInsurance
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get the complete name of the insurance
     * @return string
     */
    public function getFullInsurance()
    {

        $insurance = Insurance::model()->findByPk($this->id);
        return addslashes($insurance->name);
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        $relations = parent::relations();
        $relations['providerInsurances'] = array(self::HAS_MANY, 'ProviderInsurance', 'insurance_id');

        return $relations;
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['enabled'] = Yii::t('app', 'Enabled (No implemented)');

        return $labels;
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        //this is to enable eager loading
        $criteria->with = array('company');

        $criteria->compare('company_id', $this->company_id);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('name_plural', $this->name_plural, true);
        $criteria->compare('name_ads', $this->name_ads, true);
        $criteria->compare('api_abbreviature', $this->api_abbreviature, true);
        $criteria->compare('enabled', $this->enabled);

        return new CActiveDataProvider(
            get_class($this),
            array(
                'criteria' => $criteria,
                'pagination' => array('pageSize' => 100)
            )
        );
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from insurance if associated records exist in provider_practice_insurance table
        $has_associated_records = ProviderPracticeInsurance::model()->exists(
            'insurance_id=:insurance_id',
            array(':insurance_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Insurance can't be deleted because there are provider & practices linked to it.");
            return false;
        }

        // delete records from scheduling_mail before deleting this insurance
        $arrSchedulingMail = SchedulingMail::model()->findAll(
            "insurance_id = :insurance_id",
            array(":insurance_id" => $this->id)
        );
        foreach ($arrSchedulingMail as $schedulingMail) {
            if (!$schedulingMail->delete()) {
                $this->addError('id', 'Could not delete linked appointments');
                return false;
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Add friendly_url if necessary
     * @return bool
     */
    public function beforeSave()
    {

        if ($this->id > 0) {
            return parent::beforeSave();
        } elseif ($this->friendly_url == '') {
            $this->friendly_url = $this->getFriendlyUrl($this->name);
        }
        return parent::beforeSave();
    }

    /**
     * Show insurance suggestions in autocomplete
     * Used by AutocompleteController.php
     * @param string $q
     * @param int $limit
     * @return array
     */
    public function autocomplete($q, $limit)
    {

        $connection = Yii::app()->db;

        $q = trim($q);

        $sql = "SELECT 0 AS all_in, insurance.id AS id, insurance.name AS name,
            CONCAT(insurance_company.name, ' - ', insurance.name) AS name_display
            FROM insurance
            INNER JOIN insurance_company
            ON insurance.company_id = insurance_company.id
            WHERE (insurance.name LIKE '" . $q . "%' OR insurance_company.name LIKE '" . $q . "%')
            GROUP BY insurance.id
            ORDER BY insurance_company.name, insurance.name
            LIMIT " . $limit . ";";

        $array = $connection->createCommand($sql)->queryAll();

        $sqlCompany = sprintf(
            "SELECT 1 AS all_in, name AS company, 0 AS id
            FROM insurance_company
            WHERE name LIKE '%s%%'",
            $q
        );

        $arrCompany = $connection->createCommand($sqlCompany)->queryAll();
        if (sizeof($arrCompany) > 0) {
            $array = array_merge($arrCompany, $array);
        }

        return $array;
    }

    /**
     * Find insurance's IDs looking up their names or their parent companies'
     *
     * @param string $q
     * @return array
     */
    public static function findByName($q)
    {

        $q = addslashes($q);

        $sql = sprintf(
            "SELECT insurance.id
            FROM insurance
            INNER JOIN insurance_company
            ON insurance.company_id = insurance_company.id
            WHERE insurance.name LIKE '%%%s%%'
            OR insurance_company.name LIKE '%%%s%%';",
            $q,
            $q
        );

        $assocArray = Yii::app()->db->createCommand($sql)->queryAll();
        $array = array();
        foreach ($assocArray as $v) {
            array_push($array, $v['id']);
        }
        return $array;
    }

    /**
     * Find insurance's IDs looking up their parent companies' id
     *
     * @param mixed $q
     * @return array
     */
    public static function findByCompanyId($q)
    {

        $q = addslashes($q);

        $sql = '';
        if (trim($q) == '') {
            return array();
        } elseif (strpos($q, ',') === false) {
            $sql = sprintf(
                "SELECT insurance.id
                FROM insurance
                WHERE company_id = '%s';",
                $q
            );
        } else {
            $sql = sprintf(
                "SELECT insurance.id
                FROM insurance
                WHERE company_id IN (%s);",
                $q
            );
        }

        $connection = Yii::app()->db;
        $command = $connection->cache(Yii::app()->params['cache_medium'])->createCommand($sql);
        $assocArray = $command->queryAll();
        $array = array();
        foreach ($assocArray as $v) {
            array_push($array, $v['id']);
        }
        return $array;
    }

    /**
     * Generate a Friendly URL given an Insurance name
     *
     * @param string $insuranceName
     * @param int $insuranceId
     * @return string
     */
    public static function getFriendlyUrl($insuranceName, $insuranceId = 0)
    {
        $url = StringComponent::prepareNewFriendlyUrl($insuranceName);
        return StringComponent::getNewFriendlyUrl($url, $insuranceId, 'insurance');
    }

    /**
     * Once an insurance is deleted, delete all references to it in the provider_practice_insurance and
     * provider_insurance tables
     * @return bool
     */
    public function afterDelete()
    {

        $providerPracticeInsurances = ProviderPracticeInsurance::model()->findAll(
            "insurance_id = :insurance_id",
            array(":insurance_id" => $this->id)
        );
        foreach ($providerPracticeInsurances as $providerPracticeInsurance) {
            $providerPracticeInsurance->delete();
        }

        $providerInsurances = ProviderInsurance::model()->findAll(
            "insurance_id = :insurance_id",
            array(":insurance_id" => $this->id)
        );
        foreach ($providerInsurances as $providerInsurance) {
            $providerInsurance->delete();
        }

        return parent::afterDelete();
    }

    /**
     * Get list of insurance plans for displaying to user, e.g. in dropdown list on the appointment booking modal.
     * "Don't have one" is an option instead of "I don't have insurance" because in some cases, e.g. Dental Plans, it's
     * a Dental Savings Plan and the word "insurance" is a sensitive word they prefer to avoid
     *
     * @param int $companyId
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public function insurancePlanList($companyId = 0, $providerId = 0, $practiceId = 0)
    {
        $plans = array();
        if ($companyId > 0 && $providerId > 0 && $practiceId > 0) {
            $sql = sprintf(
                "SELECT ppi.insurance_id as id, i.name as text
                FROM provider_practice_insurance AS ppi
                LEFT JOIN insurance as i ON ppi.insurance_id = i.id
                WHERE ppi.company_id = %d
                AND ppi.provider_id = %d
                AND ppi.practice_id = %d
                ORDER BY text;",
                $companyId,
                $providerId,
                $practiceId
            );
            $plans = Yii::app()->db->createCommand($sql)->queryAll();
        }
        if (empty($plans)) {
            $plans[0]['id'] = null;
            $plans[0]['text'] = 'Don\'t have one';
        }

        return $plans;
    }

    /**
     * Get plans for this company
     * @param int $companyId
     * @return arr $plaList
     */
    public function getInsurances($companyId = 0)
    {
        $returnList = true;
        $lstCompanyIds = InsuranceCompany::getSibblingCompanyIds($companyId, $returnList);
        if ($lstCompanyIds) {
            $sql = sprintf(
                "SELECT id, name
                FROM insurance
                WHERE company_id IN (%s)
                ORDER BY name ASC;",
                $lstCompanyIds
            );
            return Yii::app()->db->cache(7 * Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
        } else {
            return array();
        }
    }

    /**
     * Returns the top 10 insurances in a city
     * @param int $cityId
     * @return array
     */
    public function getTopInsurancesByCity($cityId)
    {
        $criteria['practice.city_id'] = $cityId;
        return $this->getTopInsurances($criteria, 10);
    }

    /**
     * Get most accepted insurances by providers practicing a specialty
     * @param int $specialtyId
     * @return array
     */
    public function getTopInsurancesBySpecialty($specialtyId)
    {
        $criteria['specialty.specialty_id'] = $specialtyId;
        return $this->getTopInsurances($criteria);
    }

    /**
     * Return top 10 insurance companies accepted by providers in a given state
     * @param int $stateId
     * @return array
     */
    public function getTopInsurancesByState($stateId)
    {
        $criteria['practice.state_id'] = $stateId;
        return $this->getTopInsurances($criteria, 10);
    }

    /**
     * Return top insurances by number of providers based on criteria
     * @param array $criteria
     * @param int $maxResultSize
     * @return array
     */
    private function getTopInsurances($criteria, $maxResultSize = 9999)
    {
        $topHits = DoctorIndex::topHitsAggregate($criteria, 'insurance.company_id', $maxResultSize);
        $insuranceIds = array_column($topHits, 'key');
        $providerCounts = array_column($topHits, 'doc_count');

        if (empty($insuranceIds)) {
            return array();
        }

        $sql = sprintf(
            "SELECT SQL_NO_CACHE id AS company_id, internal_name AS company_name, friendly_url
            FROM insurance_company
            WHERE id IN (%s)
            ORDER BY FIELD(id, %s);",
            implode(',', $insuranceIds),
            implode(',', $insuranceIds)
        );

        $results = Yii::app()->dbRO->cache(86400)->createCommand($sql)->queryAll();

        foreach ($results as $i => $result) {
            $results[$i]['providers'] = $providerCounts[$i];
        }

        return $results;
    }

    /**
     * PADM: Get insurance plans given a provider, practice and company
     * @param int $practiceId
     * @param string $practiceName
     * @param int $companyId
     * @return array
     */
    public static function getPracticeCompanyPlans($practiceId, $practiceName, $companyId)
    {

        $sql = sprintf(
            "SELECT %s AS practice_id, '%s' AS practice_name, insurance.id as insurance_id,
            insurance.name AS insurance_name, '' AS details, 1 AS checked
            FROM insurance
            WHERE company_id = %d
            ORDER BY insurance_name;",
            $practiceId,
            addslashes($practiceName),
            $companyId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * PADM: Gets a provider's insurances
     * @param int $providerId
     * @return array
     */
    public static function getProviderInsurances($providerId)
    {

        $sql = sprintf(
            "SELECT pi.*, ic.name AS company_name
            FROM provider_insurance AS pi
            LEFT JOIN insurance_company AS ic
            ON pi.company_id = ic.id
            WHERE provider_id = %d
            ORDER BY company_id ASC, insurance_id ASC;",
            $providerId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get a complete list of insurance plans
     * @return array
     */
    public static function allInsurances()
    {

        $sql = "SELECT SQL_NO_CACHE DISTINCT insurance_company.name AS company_id, insurance.id AS insurance_id,
            insurance.name AS insurance_name
            FROM insurance
            INNER JOIN insurance_company
            ON insurance.company_id = insurance_company.id
            ORDER BY company_id, insurance_name;";
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Creates an insurance from a plan array as provided by the Marketplace PUF
     * @param int $companyId
     * @param string $plan
     * @return Insurance
     */
    public static function createOrMatchMarketplace($companyId, $plan)
    {

        $model = Insurance::model()->find(
            "marketplace_id = :marketplace_id",
            array(":marketplace_id" => $plan['plan_id'])
        );
        if (!$model) {
            $model = new Insurance();
        }

        $model->localization_id = 1;
        $model->company_id = $companyId;
        $model->name = $plan['marketing_name'];
        $model->name_plural = $plan['marketing_name'];
        $model->api_abbreviature = substr(trim(strtoupper($plan['marketing_name'])), 0, 4);
        $model->friendly_url = $model->getFriendlyUrl($model->name);
        $model->marketplace_id = $plan['plan_id'];

        if ($model->save()) {
            return $model;
        }
        return null;
    }

    /**
     * Insert the new plans from the insurance DB into the core DB for a given company
     * @param int $companyId
     */
    public static function insertNewMarketplacePlans($companyId)
    {
        $plans = Insurance::model()->findAll(
            "company_id = :company_id",
            array(":company_id" => $companyId)
        );

        foreach ($plans as $plan) {
            $sql = sprintf("SELECT COUNT(id) FROM insurance WHERE marketplace_id = '%s';", $plan->marketplace_id);
            $count = Yii::app()->dbCore->createCommand($sql)->queryScalar();

            if ($count == 0) {
                $sql = sprintf(
                    "INSERT INTO insurance
                    (localization_id, company_id, name,
                    name_plural, name_ads, api_abbreviature,
                    description, friendly_url, visits_number,
                    enabled, date_added, marketplace_id)
                    VALUES
                    ('%d', '%d', '%s',
                    '%s', NULL, '%s',
                    NULL, '%s', '0',
                    '0', '%s', '%s')",
                    $plan->localization_id,
                    $plan->company_id,
                    addslashes($plan->name),
                    addslashes($plan->name_plural),
                    addslashes($plan->api_abbreviature),
                    $plan->friendly_url,
                    $plan->date_added,
                    $plan->marketplace_id
                );
                Yii::app()->dbCore->createCommand($sql)->execute();
            }
        }
    }

    /**
     * Delete insurance plans which are no longer in the marketplace
     * @param int $companyId
     */
    public static function deleteOldMarketplacePlans($companyId)
    {
        $sql = sprintf("SELECT id, marketplace_id FROM insurance WHERE company_id = '%d'", $companyId);
        $result = Yii::app()->dbCore->createCommand($sql)->query();

        foreach ($result as $plan) {
            $sql = sprintf("SELECT COUNT(id) FROM insurance WHERE marketplace_id = '%s';", $plan['marketplace_id']);
            $count = Yii::app()->db->createCommand($sql)->queryScalar();

            if ($count == 0 || empty($plan['marketplace_id'])) {
                $sql = sprintf("DELETE FROM insurance WHERE id = '%d'", $plan['id']);
                Yii::app()->dbCore->createCommand($sql)->execute();
            }
        }
    }

    /**
     * Get all the inssurances and its associated companies
     * @param bool $popularForChiropractorsOnly
     * @return array
     */
    public static function getInsurancesCompanies($popularForChiropractorsOnly = false)
    {
        // cache for 1 week
        $daysToCache = 7;
        $secondsToCache = 86400 * $daysToCache;

        if (!$popularForChiropractorsOnly) {
            $sql = "SELECT SQL_NO_CACHE DISTINCT GROUP_CONCAT(DISTINCT insurance_company.id SEPARATOR ',') AS id,
                insurance_company.name as company_id,
                insurance_company_alias.alias_name AS name
                FROM insurance_company
                LEFT JOIN insurance_company_alias ON insurance_company_alias.company_name = insurance_company.name
                INNER JOIN insurance ON insurance.company_id = insurance_company.id
                WHERE insurance_company_alias.alias_name IS NOT NULL
                GROUP BY name
                ORDER BY is_popular DESC, name ASC;";
        } else {
            $sql = "SELECT SQL_NO_CACHE DISTINCT GROUP_CONCAT(DISTINCT insurance_company.id SEPARATOR ',') AS id,
                insurance_company.name as company_id,
                insurance_company_alias.alias_name AS name
                FROM insurance_company
                LEFT JOIN insurance_company_alias ON insurance_company_alias.company_name = insurance_company.name
                INNER JOIN insurance ON insurance.company_id = insurance_company.id
                WHERE insurance_company_alias.alias_name IS NOT NULL
                AND insurance_company.name IN (
                    SELECT name FROM insurance_company WHERE id IN
                    (143,121,101,432,151,123,165,160,458,382,385,422,260,124,210,109,131,148,149,164,369,455,248,347,
                    266,461,464,462,463,163,327,465,282,134,153,147,349,189,348,257,156,233,152,466,146,122,239,161,141,
                    162,150,178,155,159,278,358,265,183,370,253,386,275,168,137,173,204,301,329,383,292,279,342,238,376,
                    379,251,328,286,310,300,311,316,424,115,336,315,237,191,384,388,296)
                )
                GROUP BY name
                ORDER BY is_popular DESC, name ASC;";
        }
        $companies = Yii::app()->db->cache($secondsToCache)->createCommand($sql)->queryAll();

        if (!$popularForChiropractorsOnly) {
            $sql = "SELECT SQL_NO_CACHE company_id, id AS id, name AS name
                FROM insurance
                ORDER BY name;";
        } else {
            $sql = "SELECT SQL_NO_CACHE company_id, id AS id, name AS name
                FROM insurance
                WHERE id IN (
                    704,705,433,706,707,206,207,208,209,210,211,212,213,214,215,216,217,218,219,2843,221,220,222,223,
                    2844,224,2845,2846,2847,2848,2850,2849,225,531,817,1023,943,4368,2531,2560,1024,1025,1026,434,2762,
                    2763,2764,2765,1674,1675,1676,561,2766,1325,1322,1323,1324,4369,4411,254,257,255,258,256,259,260,
                    261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,607,282,283,284,
                    285,286,287,4413,288,289,290,291,292,293,294,295,296,297,298,4412,299,300,301,302,303,304,305,306,
                    307,308,309,310,311,312,313,314,997,786,798,562,2449,2450,2451,4352,1560,2348,1771,4405,4399,4402,
                    4400,4401,435,436,437,438,439,440,441,532,533,534,535,442,443,563,536,981,2271,4403,1772,608,609,
                    1326,2532,1928,1929,1930,650,651,652,653,654,655,656,657,658,867,444,1027,1028,763,2380,2349,2533,
                    787,1180,2373,445,315,1650,1651,316,317,1773,1677,4353,446,886,944,4354,709,708,887,888,447,818,
                    1461,788,4355,841,842,4404,448,564,565,566,449,450,567,745,746,945,747,748,749,710,537,508,889,890,
                    891,892,789,451,452,453,750,751,893,894,1505
                )
                ORDER BY name;";
        }
        $insurances = Yii::app()->db->cache($secondsToCache)->createCommand($sql)->queryAll();

        return array(
            'companies' => $companies,
            'insurances' => $insurances,
        );
    }

    /**
     * Get all the inssurances and its associated companies
     * @param integer $partnerId
     * @return array
     */
    public static function getInsurancesCompaniesByPartnerId($partnerId)
    {
        // cache for 1 week
        $daysToCache = 7;
        $secondsToCache = 86400 * $daysToCache;

        $sql = sprintf(
            "SELECT DISTINCT GROUP_CONCAT(DISTINCT ic.id SEPARATOR ',') AS id,
                ic.name as company_id, ica.alias_name AS name
            FROM prd01.insurance i
            INNER JOIN prd01.insurance_company ic ON i.company_id = ic.id
            INNER JOIN prd01.insurance_company_alias ica ON ica.company_name = ic.name
            INNER JOIN prd01.provider_insurance pi ON pi.insurance_id = i.id
            INNER JOIN prd01.account_provider ap ON ap.provider_id = pi.provider_id
            INNER JOIN prd01.organization_account oa ON oa.account_id = ap.account_id
            INNER JOIN prd01.organization o ON o.id = oa.organization_id
            WHERE o.partner_site_id = %u AND ica.alias_name IS NOT NULL
            GROUP BY name
            ORDER BY ic.name;",
            $partnerId
        );

        $companies = Yii::app()->db->cache($secondsToCache)->createCommand($sql)->queryAll();

        $sql = sprintf(
            "SELECT DISTINCT i.company_id, i.id, i.name
            FROM prd01.insurance i
            INNER JOIN prd01.provider_insurance pi ON pi.insurance_id = i.id
            INNER JOIN prd01.account_provider ap ON ap.provider_id = pi.provider_id
            INNER JOIN prd01.organization_account oa ON oa.account_id = ap.account_id
            INNER JOIN prd01.organization o ON o.id = oa.organization_id
            WHERE o.partner_site_id = %u
            ORDER BY i.name;",
            $partnerId
        );

        $insurances = Yii::app()->db->cache($secondsToCache)->createCommand($sql)->queryAll();

        return array(
            'companies' => $companies,
            'insurances' => $insurances,
        );
    }
}
