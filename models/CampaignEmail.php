<?php

Yii::import('application.modules.core_models.models._base.BaseCampaignEmail');

class CampaignEmail extends BaseCampaignEmail
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Set dates automatically if needed
     */
    public function beforeSave()
    {
        if (empty($this->date_created) || $this->date_created == '0000-00-00 00:00:00') {
            $this->date_created = date('Y-m-d H:i:s');
        }

        if (empty($this->date_sent) || $this->date_sent == '0000-00-00 00:00:00') {
            $this->date_sent = date('Y-m-d H:i:s');
        }

        return parent::beforeSave();
    }

    /**
     * Creates a new record in campaign_email with base on the template and content provided
     * @param int $campaignId
     * @param Template $template
     * @param array $content
     * @return boolean
     */
    public static function createOfficeHoursConfirmEmail($campaignId, $template, $content)
    {
        $subject = $template->subject;
        $subject = str_replace('%a_organization_name%', $content['account_organization_name'], $subject);

        $plainSchedule = "";
        $richSchedule = "";

        $plainContent = $template->content_plain;
        $richContent = $template->content;


        $campaignEmail = new CampaignEmail();
        $campaignEmail->campaign_id = $campaignId;
        $campaignEmail->account_id = $content['account_id'];
        $campaignEmail->to = $content['to'];
        $campaignEmail->subject = $subject;
        $campaignEmail->content_plain = $plainContent;
        $campaignEmail->content = $richContent;
        $campaignEmail->date_created = date('Y-m-d H:i:s');

        $result = $campaignEmail->save();

        foreach ($content['providers'] as $provider) {
            $links = CampaignEmailLink::generateLinks($campaignEmail->id, $content['account_id'], $provider['id']);
            $plainSchedule .= self::getProviderSchedulesPlain($provider, $content['account_id'], $links);
            $richSchedule .= self::getProviderSchedules($provider, $content['account_id'], $links);
        }
        $plainContent = str_replace("%p_provider_schedules%", $plainSchedule, $plainContent);
        $richContent = str_replace("%p_provider_schedules%", $richSchedule, $richContent);

        $campaignEmail->content_plain = $plainContent;
        $campaignEmail->content = $richContent;

        return $campaignEmail->save();

    }

    /**
     * Get the schedule information for the given provider
     * @param array $provider
     * @param int $accountId
     * @params array $links
     * @return string
     */
    public static function getProviderSchedules($provider, $accountId, $links)
    {
        $result = "<p>Please check if the office hours for ".$provider['name']." are correct:</p>";
        foreach ($provider['practices'] as $practice) {
            $result .= "<p>Practice: ".$practice['name'] . "</p>";
            $result .= "<table style=\"color:#465259;\">";
            $result .= "<tr style\"color: #28B7B7;\">";
            $result .= "<th style=\"padding-right: 20px; text-align: left;\">Day</th>";
            $result .= "<th style=\"padding-right: 20px; text-align: left;\">From</th>";
            $result .= "<th style=\"padding-right: 20px; text-align: left;\">To</th>";
            $result .= "</tr>";

            if (count($practice['schedule']) > 0) {
                foreach ($practice['schedule'] as $day) {
                    $result .= "<tr>";
                    $result .= "<td style=\"padding-right: 20px; text-align: left;\">" . $day['name'] . "</td>";
                    $result .= "<td style=\"padding-right: 20px; text-align: right;\">" . $day['from'] . "</td>";
                    $result .= "<td style=\"padding-right: 10px; text-align: right;\">" . $day['to'] . "</td>";
                    $result .= "</tr>";
                }
            } else {
                $result .= '<tr><td colspan="3">No office hours have been specified</td></tr>';
            }

            $result .= "</table>";
        }

        $result .= "<p><a href=\"" . $links['change'] .
            "\">Click here</a> to update the office hours for this provider</p>";
        $result .= "<p><a href=\"" . $links['visitReasons'] .
            "\">Click here</a> to update the visit reasons for this provider</p>";
        $result .= "<p><a href=\"" . $links['vacations'] .
            "\">Click here</a> to inform about vacations</p>";
        $result .= "<p><a href=\"" . $links['disable'] .
            "\">Click here</a> to turn off this feature for this provider</p>";
        $result .= "<br />";

        return $result;
    }

    /**
     * Get schedule information in plain format for the given provider
     * @param array $provider
     * @param array $links
     * @return string
     */
    public static function getProviderSchedulesPlain($provider, $accountId, $links)
    {

        $result = "Please check if the office hours for ".$provider['name']." are correct:\n\n";
        foreach ($provider['practices'] as $practice) {
            $result .= "Practice: " . $practice['name'] . "\n";
            if (count($practice['schedule']) > 0) {
                foreach ($practice['schedule'] as $day) {
                    $result .= $day['name'] . ": " . $day['from'] . " - " . $day['to'] . "\n";
                }
            } else {
                $result .= 'No office hours have been specified \n';
            }

            $result .= "\n";
        }

        $result .= "- Update the office hours for this provider at: " . $links['change'] . "\n";
        $result .= "- Update the visit reasons for this provider at: " . $links['visitReasons'] . "\n";
        $result .= "- Inform about vacations at: " . $links['vacations'] . "\n";
        $result .= "- Turn off this feature for this provider at: " . $links['disable'] . "\n";
        $result .= "\n";

        return $result;
    }

    /**
     * Get non-sent emalis of a given campaing
     * @param int $campaignId
     * @return array
     */
    public static function getPendingCampaignEmails($campaignId)
    {
        $sql = sprintf("SELECT DISTINCT id FROM campaign_email WHERE campaign_id = %d AND processed = 0", $campaignId);
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // delete records from provider_previous_practice before deleting this account
        $arrCEL = CampaignEmailLink::model()->findAll(
            'campaign_email_id=:campaignEmailId',
            array(':campaignEmailId' => $this->id)
        );
        foreach ($arrCEL as $cel) {
            if (!$cel->delete()) {
                $this->addError(
                    "id",
                    "This campaign cannot be deleted because a campaign_email_link is attached to it."
                );
                return false;
            }
        }

        return parent::beforeDelete();
    }

}
