<?php

Yii::import('application.modules.core_models.models._base.BaseAccountDevice');

class AccountDevice extends BaseAccountDevice
{

    public $uploadPath;
    public $device_nickname;
    public $account_name;
    public $practice_name;

    /**
     * Initializes the model with the correct basePath according to the active module.
     * @internal Avoid using __construct. Use init() instead.
     *  if you still need to use it, call parent::__construct($scenario). Read yii documentation.
     */
    public function init()
    {
        parent::init();
        $basePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'providers';
        $this->uploadPath = $basePath . Yii::app()->params['upload_devices'];
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'program_title';
    }

    /**
     * Declares the validation rules
     * @return array
     */
    public function rules()
    {

        $rules = parent::rules();

        foreach ($rules as $key => $value) {
            // remove required fields from the base rules since we'll redefine them below - otherwise it doesn't work
            if ($value[1] == 'required') {
                unset($rules[$key]);
            }
        }

        $rules[] = array(
            'device_id, account_id, practice_id, date_shipped, date_enrolled, localization_id',
            'required'
        );
        $rules[] = array(
            'logo_path', 'file', 'types' => 'jpg, jpeg, gif, png', 'maxSize' => 1024 * 1024 * 15,
            'allowEmpty' => true, 'on' => 'hasImage'
        );
        $rules[] = array(
            'logo_hero_path', 'file', 'types' => 'jpg, jpeg, gif, png', 'maxSize' => 1024 * 1024 * 15,
            'allowEmpty' => true, 'on' => 'hasImage'
        );
        $rules[] = array('logo_office_staff_path', 'file', 'types' => 'jpg, jpeg, gif, png', 'maxSize' =>
        1024 * 1024 * 15, 'allowEmpty' => true, 'on' => 'hasImage');
        $rules[] = array(
            'logo_path, logo_hero_path, logo_office_staff_path', 'ext.CustomValidators.CheckDirectory',
            'uploadPath' => $this->uploadPath
        );

        return $rules;
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'device_id' => null,
            'account_id' => null,
            'practice_id' => null,
            'status' => Yii::t('app', 'Status'),
            'is_replacement' => Yii::t('app', 'Is Replacement'),
            'date_shipped' => Yii::t('app', 'Date Shipped'),
            'shipment_tracking_number' => Yii::t('app', 'Shipment Tracking #'),
            'logo_path' => Yii::t('app', 'Logo Path'),
            'logo_hero_path' => Yii::t('app', 'Logo Hero Path'),
            'logo_office_staff_path' => Yii::t('app', 'Office & Staff Photo'),
            'date_enrolled' => Yii::t('app', 'Date Enrolled'),
            'date_last_accessed' => Yii::t('app', 'Last Accessed'),
            'date_last_review_received' => Yii::t('app', 'Last Review Received'),
            'date_delivered' => Yii::t('app', 'Date Delivered'),
            'status_reason' => Yii::t('app', 'Status Reason'),
            'notes' => Yii::t('app', 'Notes'),
            'initial_cta' => Yii::t('app', 'Initial CTA'),
            'account' => null,
            'device' => null,
            'practice' => null,
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->with = array('account', 'device', 'practice');

        if ($this->account_id > 0) {
            $criteria->compare('account_id', $this->account_id);
        } else {
            $criteria->addSearchCondition(
                'CONCAT(account.first_name," ",account.last_name)',
                "%" . trim($this->account_name) . "%",
                false,
                'OR',
                'LIKE'
            );
        }

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.device_id', $this->device_id);
        $criteria->compare('device.nickname', $this->device_nickname, true);
        $criteria->compare('t.practice_id', $this->practice_id);
        $criteria->compare('practice.name', $this->practice_name, true);
        $criteria->compare('t.account_id', $this->account_id);
        $criteria->compare('t.status', $this->status, true);
        $criteria->compare('t.is_replacement', $this->is_replacement);
        $criteria->compare('t.date_shipped', $this->date_shipped, true);

        $criteria->compare('t.shipment_tracking_number', $this->shipment_tracking_number, true);
        $criteria->compare('t.logo_path', $this->logo_path, true);
        $criteria->compare('t.logo_hero_path', $this->logo_hero_path, true);
        $criteria->compare('t.logo_office_staff_path', $this->logo_office_staff_path, true);
        $criteria->compare('t.date_enrolled', $this->date_enrolled, true);
        $criteria->compare('t.date_last_accessed', $this->date_last_accessed, true);
        $criteria->compare('t.date_last_review_received', $this->date_last_review_received, true);
        $criteria->compare('t.date_delivered', $this->date_delivered, true);
        $criteria->compare('t.status_reason', $this->status_reason, true);
        $criteria->compare('t.localization_id', $this->localization_id, true);
        $criteria->compare('t.notes', $this->notes, true);

        $sort = new CSort();
        $sort->attributes = array(
            'status' => array(
                'asc' => 't.status',
                'desc' => 't.status desc',
            ),
            'device' => array(
                'asc' => 'device.nickname',
                'desc' => 'device.nickname desc',
            ),
            'account' => array(
                'asc' => 'account.first_name',
                'desc' => 'account.first_name desc',
            ),
            'practice' => array(
                'asc' => 'practice.name',
                'desc' => 'practice.name desc',
            ),
            'date_shipped' => array(
                'asc' => 't.date_shipped',
                'desc' => 't.date_shipped desc',
            ),
            'date_delivered' => array(
                'asc' => 't.date_delivered',
                'desc' => 't.date_delivered desc',
            ),
        );
        $sort->defaultOrder = 'account.first_name';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100),
            'sort' => $sort,
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Needed for the extension ERememberFiltersBehavior, which saves the filters inputted after refreshing the page.
     * @return array
     */
    public function behaviors()
    {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'ERememberFiltersBehavior',
            ),
        );
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // delete records from account_device_history before deleting this account_device
        $arrAccountDeviceHistory = AccountDeviceHistory::model()->findAll(
            'account_device_id = :account_device_id',
            array(':account_device_id' => $this->id)
        );
        foreach ($arrAccountDeviceHistory as $accountDeviceHistory) {
            if (!$accountDeviceHistory->delete()) {
                $this->addError("id", "Couldn't unlink history from account device.");
                return false;
            }
        }

        // delete records from account_device_provider before deleting this account_device
        $arrAccountDeviceProvider = AccountDeviceProvider::model()->findAll(
            'account_device_id = :account_device_id',
            array(':account_device_id' => $this->id)
        );
        foreach ($arrAccountDeviceProvider as $accountDeviceProvider) {
            if (!$accountDeviceProvider->delete()) {
                $this->addError("id", "Couldn't unlink providers from account device history.");
                return false;
            }
        }

        // update the RH count in SF
        SqsComponent::sendMessage('updateSFAccountDevices', $this->account_id);

        return parent::beforeDelete();
    }

    /**
     * validate all required field are filled
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->isNewRecord && ($this->status == '' || !$this->status)) {
            // Pre-delivery (Ready, waiting to have label generated and shipped) -
            // manual when account_device created in GADM
            $this->status = 'P';
        }

        $valid = true;
        $errorBeforeSave = '';
        $this->email_required = 0;

        if (empty($this->date_shipped)) {
            $valid = false;
            $errorBeforeSave = 'Date shipped is empty';
        }
        if (empty($this->date_enrolled)) {
            $valid = false;
            $errorBeforeSave = 'Date enrolled is empty';
        }
        if (empty($this->practice_id)) {
            $valid = false;
            $errorBeforeSave = 'Practice is empty';
        }
        if (empty($this->device_id)) {
            $valid = false;
            $errorBeforeSave = 'Device is empty';
        }
        if ($valid) {

            if ($this->date_enrolled == '0000-00-00') {
                $this->date_enrolled = date("Y-m-d");
            }

            // do we need to update the RH count in SF?
            if ($this->isNewRecord) {
                // yes, because this is a new record
                SqsComponent::sendMessage('updateSFAccountDevices', $this->account_id);
            } elseif ($this->account_id != $this->oldRecord->account_id) {
                // yes, because we switched the account. also update the old account
                SqsComponent::sendMessage('updateSFAccountDevices', $this->account_id);
                SqsComponent::sendMessage('updateSFAccountDevices', $this->oldRecord->account_id);
            } elseif ($this->device_id != $this->oldRecord->device_id) {
                // yes because the device changed (just in case something weird is going on, eg non-digital to digital)
                SqsComponent::sendMessage('updateSFAccountDevices', $this->account_id);
            }

            return parent::beforeSave();
        }
        $this->addError('id', $errorBeforeSave);
        return false;
    }

    /**
     * If associating a device to an account, mark the device as Unavailable
     * If marking an account-device as Returned, mark the device as Available
     * @return mixed
     */
    public function afterSave()
    {
        if ($this->isNewRecord) {
            $device = Device::model()->findByPk($this->device_id);
            if ($device) {
                // device became Unavailable
                $device->status = 'U';
                $device->save();
            }
        } elseif ($this->status == 'R') {
            $device = Device::model()->findByPk($this->device_id);
            if ($device && $device->device_model_id != 7) {
                // device became Available if is not a DigitalDevice (model_id=7)
                $device->status = 'A';
                $device->save();
            }
        } elseif ($this->status == 'D') {
            // warning: Stranded is status D in account-device, but status S in device
            $device = Device::model()->findByPk($this->device_id);
            if ($device) {
                // device became stranded
                $device->status = 'S';
                $device->save();
            }
        }
        return parent::afterSave();
    }

    /**
     * - Validate that the chosen device is not being used by another account
     * - If the status reason is changed to active, validate that no other account is using that device
     * @return boolean
     */
    public function beforeValidate()
    {
        if (!$this->isNewRecord) {
            // verifies that the device is not active
            if ($this->status == 'A') {
                $hasActiveRecord = AccountDevice::model()->find(
                    'device_id=:device_id AND id!=:id AND status="A"',
                    array(':device_id' => $this->device_id, ':id' => $this->id)
                );

                if ($hasActiveRecord) {
                    $this->addError('status', 'This device is already linked to an account.');
                    return false;
                }
            }
            // Update: Find if the chosen device is available
            $deviceCurrentId = AccountDevice::model()->findByPk($this->id)->device_id;
            if ($deviceCurrentId > 0 && ($deviceCurrentId != $this->device_id)) {
                $device = Device::model()->findByPk($this->device_id);
                if ($device->status != 'A') {
                    $this->addError('device_id', 'Device is not available.');
                    return false;
                }
            }

            // Check that no other account is using the device if the status is changed
            $currentStatus = $this->oldRecord->status;
            if (($currentStatus != $this->status) && ($this->status == 'A')) {
                $this->countDevicesAssociated($this->device_id);
            }
        } else {
            // Find if the chosen device is not being used by another account (create action)
            $this->countDevicesAssociated($this->device_id);
        }

        // safeguard for null dates
        if ($this->date_last_accessed == '0000-00-00 00:00:00') {
            $this->date_last_accessed = null;
        }
        if ($this->date_last_review_received == '0000-00-00 00:00:00') {
            $this->date_last_review_received = null;
        }

        return parent::beforeValidate();
    }

    /**
     * Create a new Digital Device and link to this practice for this organization
     * @param object $currentOrgFeature
     *
     * @return array
     */
    public static function assignDigitalDevice($currentOrgFeature)
    {
        $result['success'] = true;
        $result['error'] = '';
        if (empty($currentOrgFeature) || $currentOrgFeature->feature_id != 20) {
            // is empty or feature_id not is reviewRequest (id:20)
            $result['success'] = false;
            $result['error'] = 'organization-feature is empty or is not a reviewrequest feature';
        }

        $practiceId = $currentOrgFeature->practice_id;
        $organizationId = $currentOrgFeature->organization_id;
        $organizationAccount = Organization::model()->findByPk($organizationId)->getOwnerAccount();
        $ownerAccountId = $organizationAccount->id;

        // Is the account enrolled in the Kiosk Program?
        $account = Account::model()->findByPk($ownerAccountId);
        if ($account->has_review_kiosk_program != 1) {
            // No, so we need to enrolled
            $account->has_review_kiosk_program = 1;
            $account->save();
        }

        // Create the device
        $newDevice = Device::createNewDigitalDevice();

        // Create account_device
        $accountDevice = new AccountDevice();
        $accountDevice->localization = 1;
        $accountDevice->device_id = $newDevice->id;
        $accountDevice->account_id = $ownerAccountId;
        $accountDevice->practice_id = $practiceId;
        $accountDevice->status = 'A';
        $accountDevice->date_shipped = date('Y-m-d');
        $accountDevice->date_enrolled = date('Y-m-d');
        if (!$accountDevice->save()) {
            $result['success'] = false;
            $result['error'] = 'Could not save account_device';
            $result['data'] = $accountDevice->getErrors();
        } else {
            $result['data']['account_device'] = (array) $accountDevice->attributes;

            // Associate ReviewRequest account device to all providers associated to the practice
            $arrProviders = ProviderPractice::getPracticeProviders($practiceId, $ownerAccountId);
            // Do we have providers in this practice?
            if (!empty($arrProviders)) {
                // Yes, we have
                $lstProviders = '';
                foreach ($arrProviders as $v) {
                    if (Common::isTrue($v['checked'])) {
                        $lstProviders .= $v['provider_id'] . '=true,';
                    }
                }
                $lstProviders = rtrim($lstProviders, ',');

                // Add providers to this account-device
                $data['accountDeviceId'] = $accountDevice->id;
                $data['action'] = 5; // Add/remove provider
                $data['providerStr'] = $lstProviders;
                $data['followUpEmail'] = 0;
                $data['sendEmailReview'] = 0;
                $data['accountId'] = $ownerAccountId;

                $result['data']['account_device_providers'] = ReviewHubDashboard::saveServices($data);
            }
        }
        return $result;
    }

    /**
     * Create a new Digital Device and link to this practice for this organization
     * @param object $currentOrgFeature
     *
     * @return array
     */
    public static function removeDigitalDevice($currentOrgFeature)
    {
        $result['success'] = true;
        $result['error'] = '';
        if (empty($currentOrgFeature) || $currentOrgFeature->feature_id != 20) {
            // is empty or feature_id not is reviewRequest (id:20)
            $result['success'] = false;
            $result['error'] = 'organization-feature is empty or is not a reviewrequest feature';
        }

        $practiceId = $currentOrgFeature->practice_id;
        $organizationId = $currentOrgFeature->organization_id;
        $organizationAccount = Organization::model()->findByPk($organizationId)->getOwnerAccount();
        $ownerAccountId = $organizationAccount->id;

        // Find digitalDevices related to this practice/account
        $sql = sprintf(
            "SELECT account_device.id
            FROM account_device
            LEFT JOIN device ON device.id = device_id
            WHERE device.device_model_id = 7
                AND account_device.practice_id = %d
                AND account_device.account_id = %d
                AND account_device.STATUS = 'A'",
            $practiceId,
            $ownerAccountId
        );
        $accountDevices = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($accountDevices)) {
            foreach ($accountDevices as $ad) {
                $accountDevice = AccountDevice::model()->findByPk($ad['id']);
                $accountDevice->status = 'R';
                if (!$accountDevice->save()) {
                    $result['success'] = false;
                    $result['error'] = 'Could not save account_device';
                    $result['data'] = $accountDevice->getErrors();
                    return $result;
                }
            }
        }
        return $result;
    }

    /**
     * Update FollowUp Email
     * @param int $accountDeviceId
     * @param int $followUpEmail
     * @param int $accountId
     * @return int
     */
    public static function updateFollowUpEmail($accountDeviceId = 0, $followUpEmail = null, $accountId = 0)
    {

        // only check if we're an admin if we're not in cli
        if (php_sapi_name() != 'cli' && !empty(Yii::app()->session['admin_account_id'])) {
            // we're an admin inside PADM, so just use the account_device_id to look up
            $accountDevice = AccountDevice::model()->findByPk($accountDeviceId);
        } else {
            // validate the user
            $accountId = OrganizationAccount::switchOwnerAccountID($accountId);
            $accountDevice = AccountDevice::model()->find(
                'id=:accountDeviceId AND account_id=:accountId',
                array(':accountDeviceId' => $accountDeviceId, ':accountId' => $accountId)
            );
        }

        if (!empty($accountDevice)) {
            $accountDevice->send_followup_email = $followUpEmail;
            $accountDevice->send_followup_sms = $followUpEmail;
            $accountDevice->save();
        }
        $results['success'] = true;
        $results['http_response_code'] = 200;
        $results['error'] = "";
        $results['data'] = (array) $accountDevice->attributes;
        return $results;
    }

    /**
     * Query EasyPost for shipping rates given a set of addresses and a parcel
     * @param array $data
     * $data Eg:
     *   {
     *       "account_id": 16490,
     *       "practice_id": 3659772,
     *       "device_id": 415,
     *       "label_format": "std",
     *       "shipping": {
     *           "reference": "",
     *           "address_1_name": "Bufford api updated 123",
     *           "address_2_name": "ATTN: Doctor.com",           ------------> all _2_ referer to Doctor.com office
     *           "address_1_phone": "2035189378",
     *           "address_2_phone": "(888) 666-8161",
     *           "address_1_address": "2 Sammons Lane",
     *           "address_2_address": "85 5th Ave",
     *           "address_1_address_2": "suite 900123",
     *           "address_2_address_2": "7th Floor",
     *           "address_1_zip": "10001",
     *           "address_2_zip": "10003",
     *           "address_1_city": "New York",
     *           "address_2_city": "New York",
     *           "address_1_state": "New York",
     *           "address_2_state": "NY",
     *           "address_1_country": "US",
     *           "address_2_country": "US",
     *           "parcel_length": "11",
     *           "parcel_width": "13",
     *           "parcel_height": "4",
     *           "parcel_weight": "80",
     *           "label_format": "std",
     *           "international_shipping": "no",
     *           "int": {
     *               "description": "Chromebook laptop",
     *               "quantity": "1",
     *               "weight": "80",
     *               "value": "200",
     *               "origin_country": "US",
     *               "customs_certify": "true",
     *               "customs_signer": "Liz Tomazic",
     *               "contents_type": "merchandise",
     *               "contents_explanation": "",
     *               "restriction_type": "none",
     *               "restriction_comments": ""
     *           }
     *       },
     *       "international_shipping": {
     *           "international_shipping": "no",
     *           "int": {
     *               "description": "Chromebook laptop",
     *               "quantity": "1",
     *               "weight": "80",
     *               "value": "200",
     *               "origin_country": "US",
     *               "customs_certify": "true",
     *               "customs_signer": "Liz Tomazic",
     *               "contents_type": "merchandise",
     *               "contents_explanation": "",
     *               "restriction_type": "none",
     *               "restriction_comments": ""
     *           }
     *       }
     *   }
     * @return json
     */
    public static function getShippingRates($data = array())
    {

        $response['success'] = false;
        $results['http_response_code'] = 200;
        $response['error'] = '';
        $response['data'] = [];

        if (empty($data)) {
            $response['success'] = false;
            $response['error'] = 'EMPTY_DATA.';
            $response['data'] = [];
            return $response;
        }

        $labelFormat = !empty($data['labelFormat']) ? $data['label_format'] : '';

        // toggle this flag to true in order to test without contacting easypost
        $debug = !empty($data['debug']) ? $data['debug'] : false;

        $arrShipping = !empty($data['shipping']) ? $data['shipping'] : array();
        $arrInternationalShipping = !empty($data['international_shipping']) ? $data['international_shipping'] : array();


        // check all fields are filled in
        foreach ($arrShipping as $k => $v) {
            if ($k != 'reference' && $k != 'international_shipping' && $k != 'int' && trim($v) == '') {
                echo 'Please fill in all of the fields.';
                Yii::app()->end();
            }
        }


        if ((!isset($arrShipping['address_1_phone']) || strlen($arrShipping['address_1_phone']) < 10)
            || (!isset($arrShipping['address_2_phone']) || strlen($arrShipping['address_2_phone']) < 10)) {
            echo 'Phone numbers must be at least 10 alphanumeric characters long.';
            Yii::app()->end();
        }

        // init library
        require_once(Yii::app()->basePath . "/vendors/EasyPost/lib/easypost.php");

        \EasyPost\EasyPost::setApiKey(Yii::app()->params['easypost_api_key']);

        if (!$debug) {
            // from address
            $fromAddress = \EasyPost\Address::create(array(
                "company" => $arrShipping['address_1_name'],
                "street1" => $arrShipping['address_1_address'],
                "street2" => $arrShipping['address_1_address_2'],
                "city" => $arrShipping['address_1_city'],
                "state" => $arrShipping['address_1_state'],
                "country" => $arrShipping['address_1_country'],
                "zip" => $arrShipping['address_1_zip'],
                "phone" => $arrShipping['address_1_phone']
            ));

            // to address
            $toAddress = \EasyPost\Address::create(array(
                "name" => $arrShipping['address_2_name'],
                "street1" => $arrShipping['address_2_address'],
                "street2" => $arrShipping['address_2_address_2'],
                "city" => $arrShipping['address_2_city'],
                "state" => $arrShipping['address_2_state'],
                "country" => $arrShipping['address_2_country'],
                "zip" => $arrShipping['address_2_zip'],
                "phone" => $arrShipping['address_2_phone']
            ));

            // parcel
            $parcel = \EasyPost\Parcel::create(array(
                "length" => $arrShipping['parcel_length'], // in
                "width" => $arrShipping['parcel_width'], // in
                "height" => $arrShipping['parcel_height'], // in
                "weight" => $arrShipping['parcel_weight'] // 3 lb = 48 oz
            ));

            // add reference
            $reference = Yii::app()->request->getQuery('reference', '');
            if (trim($reference) == '') {
                $reference = '-';
            }

            // check for label type
            $options = array('print_custom_1' => $reference);
            if ($labelFormat == 'zpl') {
                $options = array('print_custom_1' => $reference, 'label_format' => 'zpl');
            }

            // check for international shipping
            if ($arrInternationalShipping['international_shipping'] === 'yes') {
                $arrInternationalShipping = $arrInternationalShipping['int'];

                // create Custom Item
                $customsItem = \EasyPost\CustomsItem::create(array(
                    "description" => $arrInternationalShipping['description'],
                    "quantity" => (int) $arrInternationalShipping['quantity'],
                    "weight" => (int) $arrInternationalShipping['weight'],
                    "value" => $arrInternationalShipping['value'],
                    "hs_tariff_number" => 847130,
                    "origin_country" => $arrInternationalShipping['origin_country'],
                ));

                $customsInfo = \EasyPost\CustomsInfo::create(array(
                    "eel_pfc" => 'NOEEI 30.37(a)',
                    "customs_certify" => (bool) $arrInternationalShipping['customs_certify'],
                    "customs_signer" => $arrInternationalShipping['customs_signer'],
                    "contents_type" => $arrInternationalShipping['contents_type'],
                    "contents_explanation" => $arrInternationalShipping['contents_explanation'],
                    "restriction_type" => $arrInternationalShipping['restriction_type'],
                    "restriction_comments" => $arrInternationalShipping['restriction_comments'],
                    "customs_items" => array($customsItem)
                ));
            }

            // create shipment
            $shipment = \EasyPost\Shipment::create(array(
                "to_address" => $toAddress,
                "from_address" => $fromAddress,
                "parcel" => $parcel,
                "options" => $options,
                "customs_info" => (!empty($customsInfo) ? $customsInfo : null)
            ));
        }

        $response = [];
        if (!empty($shipment->rates) > 0 || $debug) {
            $i = 0;
            $rates = [];

            if (!$debug) {
                foreach ($shipment->rates as $rate) {
                    $rates[$i]['carrier'] = $rate['carrier'];
                    $rates[$i]['service'] = $rate['service'];
                    $rates[$i]['currency'] = $rate['currency'];
                    $rates[$i]['rate'] = $rate['rate'];
                    $rates[$i]['est_delivery_days'] = $rate['est_delivery_days'];
                    $rates[$i]['id'] = $rate['id'];
                    $rates[$i]['shipment_id'] = $rate['shipment_id'];
                    $i++;
                }
            } else {
                // use some sample data
                $rates = array(
                    0 => array(
                        'carrier' => 'USPS', 'service' => 'Priority', 'currency' => 'USD', 'rate' => '13.52',
                        'est_delivery_days' => null, 'id' => 'rate_oVUtbpWb', 'shipment_id' => 'shp_7etKBU0O'
                    ),
                    1 => array(
                        'carrier' => 'USPS', 'service' => 'ParcelSelect', 'currency' => 'USD', 'rate' => '12.73',
                        'est_delivery_days' => null, 'id' => 'rate_yMwSSdwy', 'shipment_id' => 'shp_7etKBU0O'
                    ),
                    2 => array(
                        'carrier' => 'USPS', 'service' => 'Express', 'currency' => 'USD', 'rate' => '35.42',
                        'est_delivery_days' => 1, 'id' => 'rate_WW3cvzg4', 'shipment_id' => 'shp_7etKBU0O'
                    ),
                );
            }
            $response['success'] = true;
            $response['error'] = '';
            $response['data'] = $rates;
        } else {
            $response['success'] = false;
            $response['error'] = 'NO_RATES_WERE_FOUND';
            $response['data'] = 'No rates were found. Please check the provided addresses.';
        }

        return $response;
    }

    /**
     * Buy the label with index $index from available rates for this shipment
     * @param int $accountId
     * @param int $shipmentId
     * @param int $rateId
     * @param int $deviceId
     * @param int $practiceId
     * @param bool $debug
     * @param bool $createCase
     * @return array
     */
    public static function buyShippingLabel(
        $accountId = null,
        $shipmentId = null,
        $rateId = null,
        $deviceId = 0,
        $practiceId = 0,
        $debug = false,
        $createCase = false
    ) {
        $response['success'] = false;
        $results['http_response_code'] = 200;
        $response['error'] = '';
        $response['data'] = '';

        if (empty($accountId) || empty($shipmentId) || empty($rateId)) {
            $response['success'] = false;
            $response['error'] = 'EMPTY_DATA.';
            $response['data'] = '';
            return $response;
        }

        if ($debug) {
            $response['success'] = true;
            $response['data'] = 'TRACKING_CODE|http://assets.geteasypost.com/postage_labels/labels/vm3rQop.png';
            return $response;
        }

        try {
            // init library
            require_once(Yii::app()->basePath . "/vendors/EasyPost/lib/easypost.php");

            \EasyPost\EasyPost::setApiKey(Yii::app()->params['easypost_api_key']);

            $shipment = \EasyPost\Shipment::retrieve(array('id' => $shipmentId));

            $i = 0;
            foreach ($shipment->rates as $rate) {
                if ($rate['id'] == $rateId) {
                    break;
                }
                $i++;
            }

            if ($shipment && $shipment->rates && $shipment->buy($shipment->rates[$i])) {
                // output label
                $results = $shipment->tracking_code . '|' . $shipment->postage_label->label_url;

                // save record in audit log
                $auditLog = new AuditLog();
                $auditLog->account_id = $accountId;
                $auditLog->action = 'C';
                $auditLog->table = 'account_device';
                $auditLog->section = 'GADM: Account Devices';
                $auditLog->date_added = date('Y-m-d H:i:s');
                // default to tech@corp.doctor.com for command-line processes that don't have a user id
                $auditLog->admin_account_id = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
                $auditLog->notes = json_encode([$shipment->tracking_code, $shipment->postage_label->label_url]);
                $auditLog->hidden = 1;
                $auditLog->save();
                $response['success'] = true;
                $response['data'] = $results;

                // We should not create SF case
                $createCase = false;

                if ($createCase) {
                    // create SF case - this happens when requesting a return label from PADM or GADM

                    // find practice info
                    $practiceObj = Practice::model()->findByPk($practiceId);

                    // find provider info
                    $pp = ProviderPractice::model()->find(
                        array(
                            'order' => 'id',
                            'condition' => 'practice_id=:practiceId AND primary_location = 1',
                            'params' => array(':practiceId' => $practiceObj->id)
                        )
                    );
                    if (empty($pp)) {
                        $pp = ProviderPractice::model()->find(
                            array(
                                'order' => 'id',
                                'condition' => 'practice_id=:practiceId',
                                'params' => array(':practiceId' => $practiceObj->id)
                            )
                        );
                    }
                    if (empty($pp)) {
                        $mainDoctorName = $practiceObj->name;
                    } else {
                        $providerObj = Provider::model()->findByPk($pp->provider_id);
                        $mainDoctorName = $providerObj->first_m_last;
                    }

                    // find salesforce info
                    $sfId = '';
                    $accountObj = Account::model()->findByPk($accountId);
                    if ($accountObj) {
                        $organizationObj = $accountObj->getOwnedOrganization();
                        $sfId = $organizationObj->salesforce_id;
                    }

                    $data = array(
                        'RecordTypeId' => '0121J0000016V7A',
                        'Type' => 'ReviewHub',
                        'AccountId' => $sfId,
                        'Doctor_Name__c' => $mainDoctorName,
                        'OwnerId' => '005o0000003d8Vw', // andrea korakis
                        'NPI__c' => (!empty($providerObj) ? $providerObj->npi_id : ''),
                        'Status' => 'New',
                        'Priority' => 'Normal',
                        'RH_Replacement__c' => 0,
                        'Reason' => 'Other',
                        'Case_Reason__c' => 'Other',
                        'Origin' => 'Phone',
                        'ReviewHub_ID__c' => $deviceId,
                        'Practice_ID__c' => $practiceObj->id,
                        'Practice_Address__c' => $practiceObj->address . ' ' . $practiceObj->address_2,
                        'Review_Hub_Replacement_Notes__c' => '',
                        'Subject' => 'Return label',
                        'Description' => 'Tracking code: ' . (!empty($shipment) ? $shipment->tracking_code : 'N/A')
                            . ' -  Return label: ' . (!empty($shipment) ? $shipment->postage_label->label_url : 'N/A'),
                        'Root_Cause__c' => '',
                        'Internal_Notes__c' => '',
                        'Post_Mortem__c' => ''
                    );

                    SqsComponent::sendMessage('salesforceCreateCase', $data);
                }
            } else {
                $response['success'] = false;
                $response['error'] = 'ERROR';
                $response['data'] = 'The operation did not complete successfully.';
            }

            return $response;
        } catch (Exception $e) {
            $response['success'] = false;
            $response['error'] = 'ERROR_BUY_SHIPPING_LABEL';
            $response['data'] = $e->getMessage();
            return $response;
        }
    }

    /**
     * get the account_id associated to the AccountDevice
     * @param int $accountDeviceId
     * @return int
     */
    public static function getAccountId($accountDeviceId = null)
    {
        if (!$accountDeviceId) {
            return false;
        }

        $ad = AccountDevice::model()->findByPk($accountDeviceId);
        if ($ad) {
            return $ad->account_id;
        }
        return false;
    }

    /**
     * Get a descriptive status name instead of letters in the grid.
     * @return string
     */
    public function getStatusName()
    {
        switch ($this->status) {
            case 'P':
                return "Pre-delivery";
            case 'T':
                return "In transit to client";
            case 'A':
                return "Active";
            case 'B':
                return "Return requested";
            case 'S':
                return "Suspended";
            case 'D':
                return "Stranded";
            case 'C':
                return "In transit from client";
            case 'R':
                return "Returned";
            default:
        }
        return 'Unknown';
    }

    /**
     * Get the name of the account and the nickname of the device given an accountDevice_id
     * @param  int $accountDeviceId
     * @return string
     */
    public function getAccountDeviceName($accountDeviceId)
    {
        if (!$accountDeviceId) {
            return false;
        }

        $accountDevice = AccountDevice::model()->findByPk($accountDeviceId);
        if ($accountDevice) {
            $account = Account::model()->findByPk($accountDevice->account_id);
            if ($account) {
                $device = Device::model()->findByPk($accountDevice->device_id);
                if ($device) {
                    return addslashes($account->first_name . ' ' . $account->last_name . ' - ' . $device->nickname);
                }
            }
        }

        return false;
    }

    /**
     * Get the nickname of the device given an $accountDeviceId
     * @param int $accountDeviceId
     * @return string
     */
    public function getDeviceName($accountDeviceId)
    {
        $accountDevice = AccountDevice::model()->findByPk($accountDeviceId);
        if ($accountDevice) {
            $device = Device::model()->findByPk($accountDevice->device_id);
            if ($device) {
                return addslashes($device->nickname);
            }
        }
        return false;
    }

    /**
     * Get the name of the account given an accountDeviceId
     * @param int accountDeviceId
     * @return string
     */
    public static function getAccountName($accountDeviceId)
    {

        if (intval($accountDeviceId) <= 0) {
            return false;
        }

        $accountDevice = AccountDevice::model()
            ->cache(Yii::app()->params['cache_medium'])
            ->findByPk($accountDeviceId);

        if ($accountDevice) {
            $account = Account::model()
                ->cache(Yii::app()->params['cache_medium'])
                ->findByPk($accountDevice->account_id);
            if ($account) {
                return addslashes($account->first_name . ' ' . $account->last_name);
            }
        }

        return false;
    }

    /**
     * Get all the Review Request account devices related to the given account/practice
     *
     * @param int accountId
     * @param int practiceId
     * @param boolean providers
     *
     * @return array
     */
    public static function getAccountDevices($accountId, $practiceId = 0, $providers = false)
    {
        // Get all devices related to the account-practice
        $criteria = new CDbCriteria();
        $criteria->with = array('device');
        if ($practiceId > 0) {
            $criteria->condition = "t.practice_id = :practice_id AND t.account_id = :account_id AND t.status = 'A'";
            $criteria->params =  array(':practice_id' => $practiceId, ':account_id' => $accountId);
        } else {
            $criteria->condition = "t.account_id = :account_id AND t.status = 'A'";
            $criteria->params =  array(':account_id' => $accountId);
        }
        $criteria->order = "t.id DESC";
        $accountDevices = AccountDevice::model()->findAll($criteria);

        // Initialize the empty array for returning the review requests devices
        $practiceDevices = [];

        // Go through all related account devices
        foreach ($accountDevices as $accountDevice) {

            // Add the account device item to the result array
            $practiceDevice = [
                'id' => $accountDevice->id,
                'practice_id' => $accountDevice->practice_id,
                'name' => $accountDevice->device->nickname,
                'device_model_id' => $accountDevice->device->device_model_id,
                'ddc_link' => $accountDevice->getReviewHubUrl()
            ];

            // Is the provider flag set?
            // Yes, then add the related providers to Account Device
            if ($providers) {
                $practiceDevice['providers'] = $accountDevice->getAccountDeviceProviders();
            }

            // Add the practice device item to the result array
            $practiceDevices[] = $practiceDevice;
        }

        return $practiceDevices;
    }


    /**
     * Get the providers associated to the account device
     */
    protected function getAccountDeviceProviders()
    {
        // Default result
        $arrAccountDevideProviders = [];

        // Does the account device have associated providers?
        if (!empty($this->accountDeviceProviders)) {

            // Go through all related account devices providers
            foreach ($this->accountDeviceProviders as $accountDeviceProvider) {
                // Build the params array
                $params = [
                    'provider_id' => $accountDeviceProvider->id
                ];
                // Add the provider to the result array
                $arrAccountDevideProviders[] = [
                    'provider_id' => $accountDeviceProvider->provider_id,
                    'ddc_link' => $this->getReviewHubUrl($params)
                ];
            }
        }

        return $arrAccountDevideProviders;
    }

    /**
     * Counts the number of devices associated with an account given a condition.
     * If there are 1 or more devices associated, it'll add an error to device_id property.
     */
    public function countDevicesAssociated()
    {
        if ($this->device_id > 0 && $this->account_id > 0) {
            $condition = sprintf(
                "device_id = %s AND account_id != %s AND status = 'A'",
                $this->device_id,
                $this->account_id
            );

            $numberOfDevices = AccountDevice::model()->count($condition);
            if ($numberOfDevices) {
                $this->addError('device_id', 'Device already in use by another account.');
            }
        }
    }

    /**
     * find the account devices base on practice and provider
     */
    public static function findAccountDevice($data)
    {
        // condition for getting account devices
        $condition = ['t.status' => 'A'];

        // Check parameters
        if (!empty($data['practice_id'])) {
            $condition['t.practice_id'] = $data['practice_id'];
        }
        if (!empty($data['provider_id'])) {
            $condition['provider.id'] = $data['provider_id'];
        } elseif (!empty($data['provider_npi'])) {
            $provider = Provider::model()->findByAttributes(array('npi_id' => $data['provider_npi']));
            $data['provider_id'] = !empty($provider) ? $provider->id : 0;
            $condition['provider.id'] = $data['provider_id'];
            unset($data['provider_npi']);
        }

        // Build the criteria object
        $criteria = new CDbCriteria;
        $criteria->distinct = true;
        $criteria->join = 'INNER JOIN account_device_provider ON t.id = account_device_provider.account_device_id ' .
            'INNER JOIN provider ON account_device_provider.provider_id = provider.id';
        $criteria->order = 't.id DESC';
        $criteria->limit = 1;
        $criteria->addColumnCondition($condition);

        // Get all active account devices for the given criteria
        return AccountDevice::model()->cache(Yii::app()->params['cache_medium'])->find($criteria);
    }

    /**
     * Generate review request links for the given parameters
     *
     * @param array data
     * @return array
     */
    public static function generateReviewRequestLinks($data, $partnerSiteId, $appTitle)
    {

        if (empty($data['count']) || $data['count'] > ReviewRequestExternal::MAX_GENERATE_RR_LINKS) {
            $data['count'] = ReviewRequestExternal::MAX_GENERATE_RR_LINKS;
        }

        $result = ['count' => 0];
        $count = $data['count'];

        // // Get all active account devices for the given criteria
        $accountDevice = AccountDevice::findAccountDevice($data);

        if (!empty($accountDevice)) {
            $result['count'] = $count;

            // Preparing the parameters
            $params = [];
            if (!empty($data['provider_id'])) {
                $params['provider_id'] = $data['provider_id'];
            }
            if (!empty($data['patient_first_name'])) {
                $params['patient_first_name'] = $data['patient_first_name'];
            }
            if (!empty($data['patient_last_name'])) {
                $params['patient_last_name'] = $data['patient_last_name'];
            }
            if (!empty($data['patient_email'])) {
                $params['patient_email'] = $data['patient_email'];
            }
            if (!empty($data['patient_phone'])) {
                $params['patient_phone'] = $data['patient_phone'];
            }

            $result['base_url'] = "https://url.doctor.com/";

            $baseUrl = 'https://' . Yii::app()->params->servers['dr_fee_hn'] . '?sid=' . $accountDevice->getSID();

            if (!empty($params)) {
                $baseUrl .= '&params=' . base64_encode(json_encode($params));
            }

            $sendVia = !empty($data['send_via']) ? $data['send_via'] : 'API';

            for ($i = 0; $i < $count; $i++) {
                $options['uid'] =  StringComponent::generateGUID();
                if (!empty($partnerSiteId)) {
                    $options['partner_site_id'] =  $partnerSiteId;
                }
                $key = base64_encode(json_encode($options));

                $url = $baseUrl . "&key=" . $key;

                // Store the entry in review_request_external table
                $rre = new ReviewRequestExternal();
                $rre->account_device_id = $accountDevice->id;
                $rre->practice_id =  $accountDevice->practice_id;
                if (!empty($data['provider_id'])) {
                    $rre->provider_id = $data['provider_id'];
                }
                $rre->partner_site_id = $partnerSiteId;
                $rre->app_title = $appTitle;
                $rre->date_added = TimeComponent::getNow();
                $rre->rr_guid = $options['uid'];
                $rre->send_via = $sendVia;
                $rre->save();

                $rre->ddc_link = StringComponent::shortenURL(
                    'https://' . Yii::app()->params->servers['dr_pro_hn'] . '/site/reviewRequestExternalRedirect'
                        . '?id=' . base64_encode($rre->id)
                        . '&type=' . base64_encode('D')
                        . '&to=' . base64_encode(urlencode($url))
                );
                $rre->save();

                // get last word from URL after a slash
                preg_match("/[^\/]+$/", $rre->ddc_link, $matches);
                $result['key'][] = $matches[0];

                // Do we need to return an extra info?
                if ($data['count'] == 1
                    && !empty($data['return_account_device_id'])
                    && Common::isTrue($data['return_account_device_id'])
                ) {
                    // Yes, needed by ReviewController::sendReviewRequest
                    $result['account_device_id'] = $accountDevice->id;
                    $result['review_request_external_id'] = $rre->id;
                }

                unset($rre);

            }

        }

        return $result;
    }

    /**
     * Checks if the account associated with this device is eligible for a
     * reward after this review being approved. If so, send them a Starbucks
     * gift card using the Tango Card API.
     *
     * Tango Card documentation is here: https://github.com/tangocarddev/RaaS
     *
     * @param int $providerId
     * @param boolean $forcePurchaseFunds force the purchase of funds
     * @param int $practiceId
     * @return boolean True if reward sent, else false
     */
    public function sendRewardIfEligible($providerId, $forcePurchaseFunds = false, $practiceId = 0)
    {

        $accountId = $this->getAccountId($this->id);

        if (!$this->isEligibleForReward($providerId)) {
            return false;
        }

        $account = Account::model()->findByPk($accountId);

        if (!$account) {
            throw new Exception('Cannot send starbucks gift card: account ' .
                $accountId . ' does not exist');
        }

        return $account->sendReward($forcePurchaseFunds, $practiceId);
    }

    /**
     * Checks if the given provider is affiliated with the current owner of
     * this account_device and, if so, checks for 3 approved reviews from
     * 3 unique patients for this account on ReviewHubs. If there have been
     * 3 exactly (no more or less), returns true, else false.
     *
     * @param int $providerId
     * @return boolean True if eligible, else false
     */
    public function isEligibleForReward($providerId)
    {

        $accountId = $this->getAccountId($this->id);

        // if account has already received an award, they aren't eligible
        $account = Account::model()->findByPk($accountId);
        if ($account->date_reward_sent) {
            return false;
        }

        // don't proceed if this provider isn't affiliated with the account
        $ap = AccountProvider::model()->find(
            'account_id=:account_id AND provider_id=:provider_id',
            array(':account_id' => $accountId, ':provider_id' => $providerId)
        );
        if (!$ap) {
            return false;
        }

        // ensure there are 3 approved reviews from 3 unique patients across
        // all of the providers associated with this account; return false if not
        // @TODO: this doesn't check if patients are unique - need to add that
        $sql = sprintf(
            "SELECT count(DISTINCT provider_rating.patient_id) FROM provider_rating, account_provider, account
            WHERE (provider_rating.status = 'A')
            AND provider_rating.provider_id = account_provider.provider_id
            AND account_provider.account_id = account.id
            AND account.id = %d
            AND provider_rating.account_device_id > 0;",
            $accountId
        );

        $count = Yii::app()->db->createCommand($sql)->queryScalar();

        // must have EXACTLY 3, not more or less (more = legacy person or already has received the reward)
        if ($count != 3) {
            return false;
        }

        return true;
    }

    /**
     * Get the number of approved reviews an account-device has
     * @return string
     */
    public function getApprovedReviews()
    {

        $sql = sprintf(
            "SELECT IF(COUNT(1) > 0, COUNT(1), '')
            FROM provider_rating
            WHERE account_device_id = %d
            AND provider_rating.status = 'A';",
            $this->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get devices from a given account
     * @param int $accountId
     * @return array
     */
    public function getDevices($accountId = 0)
    {

        $sql = sprintf(
            "SELECT ad.id AS account_device_id, ad.account_id, ad.device_id, ad.status,
            ad.practice_id, p.name AS practice_name, p.address_full AS practice_address, p.phone_number,
            d.nickname, dm.name AS device_model_name, l.name AS localization_name,
            GROUP_CONCAT(DISTINCT(CONCAT(pro.first_name, ' ', pro.last_name)) SEPARATOR ', ') AS providers,
            GROUP_CONCAT(DISTINCT(CONCAT(pro.id)) SEPARATOR ', ') AS provider_ids,
            ad.send_email_review_to_account, ad.send_followup_email, ad.send_followup_sms
            FROM account_device AS ad
                INNER JOIN account_practice as ap
                    ON ad.practice_id = ap.practice_id AND ap.account_id = %d
                LEFT JOIN account_device_provider AS adp
                    ON ad.id = adp.account_device_id
                LEFT JOIN account_provider as apro
                    ON adp.provider_id = apro.provider_id AND ap.account_id = %d
                LEFT JOIN provider AS pro
                    ON adp.provider_id = pro.id
                LEFT JOIN practice AS p
                    ON ad.practice_id = p.id
                LEFT JOIN provider_practice AS pp
                    ON pp.practice_id = ad.practice_id AND pp.provider_id = adp.provider_id
                LEFT JOIN device AS d
                    ON ad.device_id = d.id
                LEFT JOIN device_model AS dm
                    ON d.device_model_id = dm.id
                LEFT JOIN localization AS l
                    ON ad.localization_id = l.id
            WHERE ad.account_id = %d
                AND ad.status='A'
            GROUP BY ad.id;",
            $accountId,
            $accountId,
            $accountId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     *
     * Returns a unique, encrypted token used on the client-side so that we know
     * which account_device_id is associated with a given request in a secure
     * way.
     * @param int $accountDeviceId
     * @return string
     */
    public function getCSRFToken($accountDeviceId = 0)
    {
        if ($accountDeviceId == 0) {
            $accountDeviceId = $this->id;
        }
        return bin2hex(Yii::app()->getSecurityManager()->encrypt($accountDeviceId . '-' . time()));
    }

    /**
     * Decrypts the given CSRF token and, based on its structure, determines
     * which AccountDevice is associated with it and fetches the
     * corresponding AccountDevice
     *
     * @param string $token
     * @return string
     */
    public static function getAccountDeviceFromCSRFToken($token)
    {

        if (!$token) {
            return null;
        }

        try {
            // convert string to binary string
            $decryptedString = Yii::app()->getSecurityManager()->decrypt(hex2bin($token));
            if (!$decryptedString) {
                return null;
            }

            // what is prior to the - sign is the account_device id
            $id = explode('-', $decryptedString)[0];
            if (!$id) {
                return null;
            }

            return AccountDevice::model()->findbyPk($id);
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Kiosk: Sets a device's last date accessed to the current dateTime
     * if lastAccessed > 1 hour
     * @param int $accountDeviceId
     * @param datetime $dateLastAccessed (
     * @return bool
     */
    public static function setAccountDeviceAccessed($accountDeviceId = 0, $dateLastAccessed = 0)
    {
        $tmpLastAccessed = strtotime($dateLastAccessed);
        $now = strtotime('now');
        $diff = ($now - $tmpLastAccessed);

        if ($accountDeviceId > 0 && $diff > 3600) {

            $accountDevice = AccountDevice::model()->findByPk($accountDeviceId);
            if ($accountDevice) {
                $accountDevice->date_last_accessed = date('Y-m-d H:i:s');
                // try to update the account/device
                if (!$accountDevice->save()) {
                    // we couldn't, log to file
                    Yii::log('Error while updating account/device: ' . json_encode($accountDevice->getErrors()));
                    return false;
                } else {
                    // success
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Kiosk: Sets a device's last date review received
     * @param int $accountDeviceId
     * @return bool
     */
    public static function setAccountDeviceLastReview($accountDeviceId = 0)
    {
        $accountDevice = AccountDevice::model()->findByPk($accountDeviceId);
        if (!empty($accountDevice)) {
            $accountDevice->date_last_review_received = date('Y-m-d H:i:s');
            if ($accountDevice->save()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Checks for existing shipping logs of a ReviewHub for a given account
     * @param int $accountId
     * @return bool
     */
    public static function hasReceivedReviewHub($accountId)
    {
        $sql = sprintf(
            "SELECT COUNT(1) AS has_shipped
            FROM shipping_log
            INNER JOIN account_device
            ON shipping_log.tracking_number = account_device.shipment_tracking_number
            WHERE account_id = %d;",
            $accountId
        );

        return (Yii::app()->db->createCommand($sql)->queryScalar() > 1);
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_device
            WHERE account_device.id = %d and account_device.account_id = %d;",
            $recordId,
            $accountId
        );
    }

    /**
     * Find by deviceId
     * if $statusList not empty() add condition to find with this status
     * @param int $deviceId
     * @param string $statusList
     * @return bool
     */
    public static function findByDeviceId($deviceId = 0, $statusList = '')
    {

        if ($deviceId == 0) {
            return false;
        }

        $statusCondition = '';
        if (!empty($statusList)) {
            $statusCondition = ' AND status IN (' . $statusList . ') ';
        }

        $sql = sprintf(
            "SELECT id, account_id
            FROM account_device
            WHERE device_id = %d
            %s ;",
            $deviceId,
            $statusCondition
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get all the account_device_practice records for this account - device
     * @param int $accountId
     * @param int $practiceId
     * @return array
     */
    public static function getDeviceByAccountPractice($accountId = 0, $practiceId = 0)
    {
        $sql = sprintf(
            "SELECT ad.id,
            d.nickname AS name,
            d.device_model_id
            FROM account_device AS ad
            LEFT JOIN device AS d
            ON d.id = ad.device_id
            WHERE ad.practice_id = %d
            AND ad.account_id = %d
            AND ad.status IN ('P', 'T', 'A');",
            $practiceId,
            $accountId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Prepare and return json data required by ReviewHub
     * @param int $accountDeviceId
     * @param int $providerId
     */
    public function getJsonForKiosk($accountDeviceId = 0, $providerId = 0)
    {

        $accountDevice = AccountDevice::model()->find(
            "id = :id AND (`status` = 'A' OR `status` = 'P' OR `status` = 'T')",
            array(':id' => $accountDeviceId)
        );

        if (!empty($accountDevice->id)) {

            $arrProviders = false;
            $arrToJson = array();
            $accountId = $accountDevice->account_id;

            // Organization linked to this account
            $accountOrganization = Organization::getByAccountId($accountId, false);

            // get csfr token
            $arrToJson['accountDevice']['id'] = $accountDevice->id;
            $arrToJson['accountDevice']['enabled'] = 1;
            $arrToJson['accountDevice']['csfrToken'] = '';
            if (Yii::app()->params["environment"] != 'docker') {
                $arrToJson['accountDevice']['csfrToken'] = $this->getCSRFToken($accountDevice->id);
            }
            $arrToJson['accountDevice']['email_required'] = $accountDevice->email_required;
            $arrToJson['accountDevice']['isReviewRequest'] = (int) $accountDevice->isReviewRequest();

            // If the account belongs to an organization
            if ($accountOrganization) {
                // Check if device is ReviewRequest or ReviewHub
                if ($accountDevice->isReviewRequest()) {
                    // Disable only if the organization is canceled
                    if ($accountOrganization['status'] == Organization::STATUS_CANCELED) {
                        $arrToJson['accountDevice']['enabled'] = 0;
                    }
                } else {
                    // Disable if the organization is not active
                    if ($accountOrganization['status'] != Organization::STATUS_ACTIVE) {
                        $arrToJson['accountDevice']['enabled'] = 0;
                    }
                }

                // Adding SF product specialist (if it was disabled)
                $arrToJson['product_specialist'] = null;
                if ($arrToJson['accountDevice']['enabled'] == 0 && !empty($accountOrganization['salesforce_id'])) {
                    // look up organization in salesforce
                    $sfAccount = Yii::app()->salesforce->getObject("Account", $accountOrganization['salesforce_id']);

                    // The Product Specialist field in Salesforce is Onboard_Rep__c
                    if (!empty($sfAccount['Onboard_Rep__c'])) {
                        $sfUser = Yii::app()->salesforce->getObject("User", $sfAccount['Onboard_Rep__c']);
                        if (!empty($sfUser)) {
                            $arrToJson['product_specialist'] = array(
                                'name' =>Common::getAttribute('Name', $sfUser, ''),
                                'email' => Common::getAttribute('Email', $sfUser, ''),
                                'phone' => Common::getAttribute('Phone', $sfUser, '')
                            );
                        }
                    }
                }
            }

            // get default for all available languages
            $arrToJson['available_languages'] = Common::getAvailableLanguages();
            $arrToJson['show_provider_specialties'] = $accountDevice->show_provider_specialties;

            $languageId = Yii::app()->language;

            // get All Personalized translations, if haven´t use default translations
            $arrToJson['language']['default'] = Localization::getLocalizationCode($accountDevice->localization_id);
            // Load customized translations
            $arrTranslations = AccountDeviceTranslations::getTranslations($accountDeviceId);

            // exclude denied or inexistent languages
            foreach ($arrToJson['available_languages'] as $key => $value) {
                if (strpos($accountDevice['denied_languages'], $key) !== false || !isset($arrTranslations[$key])) {
                    // it´s on denied languages, so remove
                    unset($arrToJson['available_languages'][$key]);
                    if ($languageId == $key) {
                        // the user try to select denied_laguage, so force to en
                        $languageId = 'en';
                        Yii::app()->language = $languageId;
                    }
                }
            }

            // Load default translations from files
            $arrTranslationsKeywords = Language::loadKeyword($arrToJson['available_languages']);

            $arrToJson['language']['translation'] = '';
            if (!empty($arrTranslations)) {
                foreach ($arrTranslations as $key => $value) {
                    if (!empty($arrTranslationsKeywords[$key])) {
                        $arrTranslations[$key] = array_merge($arrTranslations[$key], $arrTranslationsKeywords[$key]);
                        if (empty($arrTranslations[$key]['initial_cta'])) {
                            $arrTranslations[$key]['initial_cta'] = $arrTranslationsKeywords[$key]['def_initial_cta'];
                        }
                    }
                }
                $arrToJson['language']['translation'] = $arrTranslations;
            }

            $arrToJson['practice']['logo_path'] = '';
            if (!empty($accountDevice['logo_path'])) {
                $arrToJson['practice']['logo_path'] = 'https://' . Yii::app()->params->servers['dr_ast_hn'] .
                    '/devices/' . $accountDevice['logo_path'];
            }

            $arrToJson['practice']['logo_hero_path'] = '/images/default-reviewhub-cover.jpg';
            if (!empty($accountDevice['logo_hero_path'])) {
                $arrToJson['practice']['logo_hero_path'] = 'https://' . Yii::app()->params->servers['dr_ast_hn'] .
                    '/devices/' . $accountDevice['logo_hero_path'];
            }

            $arrToJson['practice']['program_title'] = '';
            if (!empty($accountDevice['program_title'])) {
                $arrToJson['practice']['program_title'] = $accountDevice['program_title'];
            }

            // Get practice details
            $practiceId = $accountDevice['practice_id'];
            $practiceData = Practice::model()->getDetails($practiceId);

            $arrToJson['practice']['logo_office_staff_path'] = '';
            if (empty($accountDevice['logo_office_staff_path'])) {
                if (!empty($practiceData[0]['logo_path'])) {
                    $arrToJson['practice']['logo_office_staff_path'] = 'https://' .
                        Yii::app()->params->servers['dr_ast_hn'] .
                        '/img?p=practices/' . $practiceData[0]['logo_path'] . '&h=260';
                }
            } else {
                $arrToJson['practice']['logo_office_staff_path'] = 'https://' .
                    Yii::app()->params->servers['dr_ast_hn'] . '/devices/' .
                    $accountDevice['logo_office_staff_path'];
            }

            if (!empty($practiceData[0])) {
                $arrToJson['practice']['id'] = $practiceData[0]['id'];
                $arrToJson['practice']['name'] = $practiceData[0]['name'];
                $arrToJson['practice']['full_address'] = $practiceData[0]['address_full'];
                $arrToJson['practice']['country_id'] = $practiceData[0]['country_id'];
                $arrToJson['practice']['color'] = null;

                $practiceLocationCountry['location_id'] = $practiceData[0]['location_id'];
                $practiceLocationCountry['country_id'] = $practiceData[0]['country_id'];
                $practiceLocationCountry['zipcode'] = $practiceData[0]['zipcode'];
            }

            // Get providers
            $accountDeviceProviders = AccountDeviceProvider::model()->getAccountDeviceProviders(
                $accountDeviceId,
                $practiceId
            );
            $accountDeviceProviders = trim($accountDeviceProviders);
            // Get details for the providers
            if (!empty($accountDeviceProviders) && $accountDeviceProviders != ',') {
                $arrProviders = Provider::model()->getProviderDetailsForKiosk($accountDeviceProviders);
                $arrToJson['practice']['providers'] = $arrProviders;
            }

            // get practice liks for shared buttons
            $useCache = false;
            $useInternalCondition = false;
            $arrToJson['practiceSocialLinks'] = ProviderPracticeListing::getPracticeSocialLinks(
                $practiceId,
                $useCache,
                $useInternalCondition,
                $providerId
            );

            if (!empty($arrToJson['practiceSocialLinks']) && $providerId > 0) {
                // if we have a providerId, remove the google link related only to a practice profile
                $providerAtPracticeListing = false;
                foreach ($arrToJson['practiceSocialLinks'] as $psl) {
                    if ($psl['listing_type_id'] == ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID) {
                        $providerAtPracticeListing = true;
                        break;
                    }
                }
                // We have at p@p level?
                if (Common::isTrue($providerAtPracticeListing)) {
                    // Yes, remove at practiceLevel
                    foreach ($arrToJson['practiceSocialLinks'] as $k => $psl) {
                        if ($psl['name'] == 'Google'
                            && $psl['listing_type_id'] != ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID
                        ) {
                            unset($arrToJson['practiceSocialLinks'][$k]);
                        }
                    }
                    $arrToJson['practiceSocialLinks'] = array_values($arrToJson['practiceSocialLinks']);
                }
            }

            $return['created'] = date("Y-m-d H:i:s");
            if (empty($accountDevice) || empty($practiceData) || empty($arrProviders)) {
                $return['error'] = true;

                if (empty($arrProviders)) {
                    $return['message'] = "EMPTY_PROVIDERS";
                } else {
                    $return['message'] = "There was a problem retrieving data.";
                }

            } else {
                $return['success'] = true;
                $return['data'] = $arrToJson;
            }
        } else {
            $return['created'] = date("Y-m-d H:i:s");
            $return['error'] = true;
            $return['message'] = "accountDeviceId not exists.";
        }
        return json_encode($return, JSON_NUMERIC_CHECK);
    }

    /**
     * Get total number of devices
     * @return int
     */
    public static function getTotalDevices()
    {
        $sql = "SELECT COUNT(1) AS total
            FROM account_device
            GROUP BY account_id
            ORDER BY total DESC
            LIMIT 1;";
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Delete reviewHub before unlink practice
     * @param int $accountId
     * @param int $valueId -> could be provider_id or practice_id
     * @param bool $isProvider -> if is true, $valueId is a provider_id
     * @return bool
     */
    public static function deleteReviewHub($accountId, $valueId, $isProvider)
    {
        // Delete Device related
        if ($isProvider) {
            $arrReviewHubWidget = AccountDeviceProvider::getDeviceByAccountProvider($accountId, $valueId);
        } else {
            $arrReviewHubWidget = AccountDevice::getDeviceByAccountPractice($accountId, $valueId);
        }
        if (count($arrReviewHubWidget) > 0) {
            foreach ($arrReviewHubWidget as $reviewHubWidget) {
                if ($isProvider) {
                    if (AccountDeviceProvider::model()->findByPk($reviewHubWidget['id'])->delete()) {
                        AuditLog::create('U', 'Unlink ACCOUNT_DEVICE_PROVIDER #' . $reviewHubWidget['id']);
                    } else {
                        return false;
                    }
                } else {
                    // change each reviewhub status to suspended
                    $accountDevice = AccountDevice::model()->findByPk($reviewHubWidget['id']);
                    $previousStat = $accountDevice->status;
                    $accountDevice->status = 'S';
                    $accountDevice->save();
                    AuditLog::create('U', 'Unlink ACCOUNT_DEVICE #' . $reviewHubWidget['id']);
                    // save a log of this change
                    AccountDeviceHistory::log($reviewHubWidget['id'], $previousStat, 'S', 'M');
                }
            }
        }

        return true;
    }

    /**
     * Get ReviewHub URL from device
     * @param array $params array (I.E: provider_id or provider_npi)
     * @param array $key array object (I.E: uid, partner_site_id)
     *
     * @return string
     */
    public function getReviewHubUrl($params = null, $key = null)
    {
        if ($this->device) {
            $args = '';
            if (!empty($params)) {
                $args .= '&params=' . base64_encode(json_encode($params));
            }

            return $this->device->getReviewHubUrl($key) . $args;
        }

        return null;
    }

    /**
     * Get SID from device
     *
     * @return string
     */
    public function getSID()
    {
        if ($this->device) {
            return $this->device->getSID();
        }

        return null;
    }

    /**
     * Is it a Review Request account device?
     *
     * @return string
     */
    public function isReviewRequest()
    {
        if ($this->device && $this->device->device_model_id == DeviceModel::REVIEW_REQUEST_ID) {
            return true;
        }

        return false;
    }

    /**
     * Update the IDs of the RHs linked to an account (ReviewHub_Device_IDs__c  and ReviewRequest_Link_IDs__c)
     * @see SalesForceCommand.php
     * @param int $accountId
     * @return boolean
     */
    public static function updateSFAccountDevices($accountId)
    {

        // look up account
        $accountObj = Account::model()->findByPk($accountId);
        // do we have it?
        if (!$accountObj) {
            // no, leave
            return false;
        }
        // look up parent org
        $organizationObj = $accountObj->getOwnedOrganization();
        // do we have it?
        if (!$organizationObj) {
            // no, leave
            return false;
        }
        $sfId = $organizationObj->salesforce_id;
        // do we have a sfid for the org?
        if (empty($sfId)) {
            // no, leave
            return false;
        }

        // query non-digital rhs (model != 7)
        $sql = sprintf(
            "SELECT GROUP_CONCAT(device.id) AS total
            FROM account_device
            INNER JOIN device
            ON account_device.device_id = device.id
            WHERE account_id = %d
            AND device.device_model_id != 7
            AND account_device.status != 'R'
            AND account_device.status != 'D'
            ORDER BY account_device.id;",
            $accountId
        );
        $newValue = Yii::app()->db->createCommand($sql)->queryScalar();

        // update sf
        Yii::app()->salesforce->updateAccountField(
            $sfId,
            'ReviewHub_Device_IDs__c',
            $newValue
        );

        // query digital rhs (model 7)
        $sql = sprintf(
            "SELECT GROUP_CONCAT(device.id)
            FROM account_device
            INNER JOIN device
            ON account_device.device_id = device.id
            WHERE account_id = %d
            AND device.device_model_id = 7
            AND account_device.status != 'R'
            AND account_device.status != 'D'
            ORDER BY account_device.id;",
            $accountId
        );
        $newValue = Yii::app()->db->createCommand($sql)->queryScalar();
        // update sf
        return Yii::app()->salesforce->updateAccountField(
            $sfId,
            'ReviewRequest_Link_IDs__c',
            $newValue
        );
    }
}
