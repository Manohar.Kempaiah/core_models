<?php

Yii::import('application.modules.core_models.models._base.BaseProviderStatsReport');

class ProviderStatsReport extends BaseProviderStatsReport
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Track a provider profile view by trying to update the table and otherwise insert into it
     * @param int $providerId
     * @return boolean
     */
    public static function trackProviderProfileView($providerId)
    {

        if (intval($providerId) < 0) {
            return false;
        }

        if (UserAgentComponent::isBot()) {
            return false;
        }

        //update stats
        $sql = sprintf(
            "UPDATE provider_stats_report
            SET profile_views = profile_views + 1
            WHERE provider_id = '%s'
            AND `date` = CURDATE()
            LIMIT 1;",
            $providerId
        );
        $affectedRows = Yii::app()->db->createCommand($sql)->execute();

        if ($affectedRows == 0) {
            //there was no record for this provider: insert stats
            $sql = sprintf(
                "INSERT INTO provider_stats_report
                (provider_id, `date`, profile_views)
                VALUES
                ('%s', CURDATE(), '1');",
                $providerId
            );
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    /**
     * Track a provider search view by trying to update the table and otherwise insert into it
     * @param int $providerId
     * @return boolean
     */
    public static function trackProviderSearchView($providerId)
    {

        if (intval($providerId) < 0) {
            return false;
        }

        if (UserAgentComponent::isBot()) {
            return false;
        }

        //update stats
        $sql = sprintf(
            "UPDATE provider_stats_report
            SET search_views = search_views + 1
            WHERE provider_id = '%s'
            AND `date` = CURDATE()
            LIMIT 1;",
            $providerId
        );
        $affectedRows = Yii::app()->db->createCommand($sql)->execute();

        if ($affectedRows == 0) {
            //there was no record for this provider: insert stats
            $sql = sprintf(
                "INSERT INTO provider_stats_report
                (provider_id, `date`, search_views)
                VALUES
                ('%s', CURDATE(), '1');",
                $providerId
            );
            Yii::app()->db->createCommand($sql)->execute();
        }
    }

    /**
     * Track a provider element view by trying to update the table and
     * otherwise insert into DB.
     * @param int $providerId
     * @param string $elementViewed
     * @return mixed
     */
    public static function trackProviderElementView($providerId, $elementViewed)
    {

        if (intval($providerId) < 0) {
            return false;
        }

        if (UserAgentComponent::isBot()) {
            return false;
        }

        // update stats report in DB
        $sql = sprintf(
            "UPDATE provider_stats_report
            SET %s = IF (%s IS NULL, 1, %s + 1) + 1
            WHERE provider_id = '%s'
            AND `date` = CURDATE()
            LIMIT 1;",
            $elementViewed,
            $elementViewed,
            $elementViewed,
            $providerId
        );
        $affectedRows = Yii::app()->db->createCommand($sql)->execute();

        // did we affect no records?
        if ($affectedRows == 0) {
            // yes: there was no record for this provider: insert stats
            $sql = sprintf(
                "INSERT INTO provider_stats_report
                (provider_id, `date`, %s)
                VALUES
                ('%s', CURDATE(), '1');",
                $elementViewed,
                $providerId
            );
            $affectedRows = Yii::app()->db->createCommand($sql)->execute();
        }
        return $affectedRows;
    }

}
