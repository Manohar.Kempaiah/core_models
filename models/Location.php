<?php

Yii::import('application.modules.core_models.models._base.BaseLocation');

class Location extends BaseLocation
{

    public $country_id;
    public $state_id;

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Rules
     */
    public function rules()
    {
        return array(
            array('city_name, zipcode, city_id, dma_id', 'required'),
            array('city_id, is_pending_approval', 'numerical', 'integerOnly' => true),
            array('latitude, longitude', 'numerical'),
            array('city_name', 'length', 'max' => 50),
            array('zipcode', 'length', 'max' => 10),
            array('is_pending_approval, latitude, longitude', 'default', 'setOnEmpty' => true, 'value' => null),
            array(
                'id, city_id, is_pending_approval, city_name, zipcode, latitude, longitude, country_id, state_id, '
                . 'dma_id', 'safe', 'on' => 'search'
            ),
        );
    }

    /**
     * Search
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('t.is_pending_approval', $this->is_pending_approval, true);
        $criteria->compare('city_name', $this->city_name, true);
        $criteria->compare('zipcode', $this->zipcode, true);
        $criteria->compare('latitude', $this->latitude);
        $criteria->compare('longitude', $this->longitude);
        $criteria->compare('city.id', $this->city_id, true);
        $criteria->compare('state.id', $this->state_id, true);
        $criteria->compare('country.id', $this->country_id, true);
        $criteria->compare('zipcode_type', $this->zipcode_type, true);
        $criteria->compare('dma_id', $this->dma_id, true);

        $criteria->join = '
            LEFT JOIN city
            ON t.city_id = city.id
            LEFT JOIN state
            ON city.state_id = state.id
            LEFT JOIN country
            ON state.country_id = country.id';

        return new CActiveDataProvider(
            get_class($this),
            array('criteria' => $criteria, 'pagination' => array('pageSize' => 10))
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'city_id' => Yii::t('app', 'City'),
            'is_pending_approval' => Yii::t('app', 'Is Pending Approval'),
            'city_name' => Yii::t('app', 'Location'),
            'zipcode' => Yii::t('app', 'Zipcode'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'state_id' => Yii::t('app', 'State'),
            'country_id' => Yii::t('app', 'Country'),
            'zipcode_type' => Yii::t('app', 'Zipcode Type'),
            'dma_id' => Yii::t('app', 'Dma'),
            'accounts' => null,
            'city' => null,
            'manualValidations' => null,
            'patients' => null,
            'practices' => null,
            'practiceDetails' => null,
            'dma' => null,
        );
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        $relations = parent::relations();
        $relations['city'] = array(self::BELONGS_TO, 'City', 'city_id');
        $relations['hospitals'] = array(self::HAS_MANY, 'Hospital', 'location_id');
        $relations['cities'] = array(self::BELONGS_TO, 'City', 'id');
        $relations['providerExperiences'] = array(self::BELONGS_TO, 'ProviderExperience', 'location_id');

        return $relations;
    }

    /**
     * Returns location with format: zip_code (city_name, State_abbreviation)
     * @param bool $full
     * @return string
     */
    public function getFullLocation($full = true)
    {
        $city = City::model()->cache(Yii::app()->params['cache_medium'])->findByPk($this->city_id);
        if ($city->name !== 'Unknown') {
            $state = State::model()->cache(Yii::app()->params['cache_medium'])->findByPk($city->state_id);
            if ($full) {
                return $this->zipcode . ' (' . $city->name . ', ' . $state->abbreviation . ')';
            }
            return $city->name . ', ' . $state->abbreviation;
        }
        return 'Unknown';
    }

    /**
     * Returns city_name, zipcode, state.name as name, state.abbreviation from a given location
     * @param int $locationId id of the location
     * @param bool $useCache
     * @return array returns an array with the results of the query.
     */
    public static function getLocationDetails($locationId = 0, $useCache = true)
    {
        if ($locationId == 0) {
            return false;
        }

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_medium'] : 0;

        $sql = "SELECT city_name, zipcode, `state`.`name` AS `name`, `state`.abbreviation, latitude, longitude,
            country.`name` AS country_name, country.id AS country_id, city.friendly_url AS city_friendly_url,
            `state`.friendly_url AS state_friendly_url, country.code country_code
            FROM location
            INNER JOIN city ON location.city_id = city.id
            INNER JOIN `state` ON city.state_id = `state`.id
            INNER JOIN country ON `state`.country_id = country.id
            WHERE location.id = :location_id";

        $location = Yii::app()->db->cache($cacheTime)
            ->createCommand($sql)
            ->bindValues([':location_id' => $locationId])
            ->queryAll();
        if (!isset($location[0])) {
            $location[0] = false;
        }
        return $location;
    }

    /**
     * Return latitude & longitude from address + location_id
     * @param string $address
     * @param integer $locationId
     * @param string $returnType array | json
     * @param array $arrLocationDetails
     * @return array || json
    */
    public static function getLatitudeLongitude(
        $address = '',
        $locationId = 0,
        $returnType = 'json',
        $arrLocationDetails = null
    )
    {

        $latLong['latitude'] = '';
        $latLong['longitude'] = '';
        $latLong['google_href'] = '#';
        $latLong['cityName'] = '';
        $latLong['error'] = 'xx';
        if (($locationId > 0 && $locationId != '999999') || (!empty($arrLocationDetails))) {

            $address = !empty($address) ? trim($address) . ', ' : '';

            if ($arrLocationDetails) {
                $locationDetails = $arrLocationDetails;
            } else {
                $locationDetails = Location::getLocationDetails($locationId);
            }

            if (!empty($locationDetails)) {
                $cityName = trim($locationDetails[0]['city_name']);
                $abbreviation = trim($locationDetails[0]['abbreviation']);
                $zipCode = trim($locationDetails[0]['zipcode']);
                $countryName = trim($locationDetails[0]['country_name']);
                $countryCode = !empty($locationDetails[0]['country_code']) ?
                        trim($locationDetails[0]['country_code']) : '';
                $stateName = !empty($locationDetails[0]['name']) ? trim($locationDetails[0]['name']) : '';

                $strLocation = $cityName . ", " . $abbreviation . " " . $zipCode . ', ' . $countryName;

                // Search for lat/long using OpenStreetMap
                $geoResult = GeoLocationComponent::geocode($address, '', $cityName, $stateName, $zipCode, $countryCode);

                // set receive data
                $latLong['latitude'] = !empty($geoResult['lat']) ? $geoResult['lat'] : null;
                $latLong['longitude'] = !empty($geoResult['lon']) ? $geoResult['lon'] : null;
                $latLong['payload'] = !empty($geoResult['payload']) ? $geoResult['payload'] : null;
                $latLong['request'] = !empty($geoResult['request']) ? $geoResult['request'] : null;
                $latLong['source'] = !empty($geoResult['source']) ? $geoResult['source'] : null;

                // if empty assign location data
                if (empty($latLong['latitude']) && !empty($locationDetails[0]['latitude'])) {
                    $latLong['latitude'] = $locationDetails[0]['latitude'];
                }
                if (empty($latLong['longitude']) && !empty($locationDetails[0]['longitude'])) {
                    $latLong['longitude'] = $locationDetails[0]['longitude'];
                }

                $latLong['cityName'] = $strLocation;
            } else {
                $latLong['error'] = 'Invalid ZipCode: ' . $locationDetails[0]['zipcode'] . ' - LocationId: ' .
                    $locationId;
            }
        } else {
            $latLong['error'] = 'Invalid LocationId: ' . $locationId;
        }

        if ($returnType == 'json') {
            $return = json_encode($latLong);
        } elseif ($returnType == 'array') {
            $return = $latLong;
        }
        return $return;
    }

    /**
     * Returns location data given a query string
     * @internal we should have only one place where we process different styles of inputting location data and analyze
     * it there: currently it's spread everywhere
     *
     * @param string $q string for querying the db
     * @param int $limit limit for rows obtained in the query
     * @return array returns an array with the results of the query.
     */
    public static function autocomplete($q, $limit)
    {
        $condition = '';
        $valuesToBind = [];

        if (strpos($q, ',') !== false) {

            $arrQ = explode(',', $q);

            if (count($arrQ) == 2) {

                $arrQ[0] = trim($arrQ[0]);
                $arrQ[1] = trim($arrQ[1]);

                $condition = "WHERE country_id = 1 AND (city.name LIKE :city_name_like
                    AND (state.name LIKE :state_name_like OR state.abbreviation LIKE :state_abbr_like))";
                $valuesToBind[':city_name_like'] = $arrQ[0] . "%";
                $valuesToBind[':state_name_like'] = $arrQ[1] . "%";
                $valuesToBind[':state_abbr_like'] = $arrQ[1] . "%";
            } elseif (count($arrQ) == 3) {

                $arrQ[0] = trim($arrQ[0]);
                $arrQ[1] = trim($arrQ[1]);
                $arrQ[2] = trim($arrQ[2]);

                $condition = "WHERE country_id = 1 AND (city.name LIKE :city_name_like
                    AND (state.name LIKE :state_name_like OR state.abbreviation LIKE :state_abbr_like))";

                $valuesToBind[':city_name_like'] = $arrQ[0] . "%";
                $valuesToBind[':state_name_like'] = $arrQ[1] . "%";
                $valuesToBind[':state_abbr_like'] = $arrQ[2] . "%";
            }
        }

        if ($condition == '') {
            $condition = "WHERE
                country_id = 1 AND
                (location.zipcode LIKE :zip_like
                OR city.name LIKE :city_name_like
                OR state.name LIKE :state_name_like)";

            $valuesToBind[':zip_like'] = $q . "%";
            $valuesToBind[':city_name_like'] = $q . "%";
            $valuesToBind[':state_name_like'] = $q . "%";
        }

        $sql = sprintf(
            "SELECT MIN(location.id) AS id, zipcode, city.name AS city, `state`.`name` AS state,
            `state`.abbreviation AS abbreviation
            FROM location
            INNER JOIN city ON location.city_id = city.id
            INNER JOIN `state` ON city.state_id = `state`.id
            %s
            GROUP BY zipcode
            ORDER BY zipcode, city_name, `state`.`name`
            LIMIT %d;",
            $condition,
            $limit
        );

        return Yii::app()->db->createCommand($sql)
            ->bindValues($valuesToBind)
            ->queryAll();
    }

    /**
     * Show location suggestions in autocomplete
     * Used by AutocompleteController.php
     *
     * @param string $q
     * @param int $limit
     * @param string $countriesId
     * @return array
     */
    public static function lazyAutocomplete($q, $limit, $countriesId = "1")
    {
        $connection = Yii::app()->db;
        $valuesToBind = [];
        $countriesId = "(".$countriesId.")";

        $orderByAlliance = "";
        if (Yii::app()->theme->getName() == "alliance") {
            $orderByAlliance = "state.abbreviation = 'WI' DESC, state.abbreviation = 'IL' DESC, "
                . "state.abbreviation = 'IA' DESC, state.abbreviation = 'MN' DESC,";
        }

        $sql = null;

        $q = addslashes(trim($q));
        if (substr($q, 0, 1) == ',' || substr($q, 0, 1) == ';') {
            // start with comma ?
            $q = substr($q, 1);
        }
        if (substr($q, -1, 1) == ',' || substr($q, -1, 1) == ';') {
            // end with comma ?
            $q = substr($q, 0, -1);
        }

        $strZip5 = (string)substr($q, 0, 5);
        $intZip5 = (string)trim($strZip5);
        $strZip7 = substr($q, 0, 7);
        // test with: `canadian-postal-code`
        $expression = '/^([a-zA-Z]\d[a-zA-Z])\ {0,1}(\d[a-zA-Z]\d)$/';

        if ((intval($intZip5) > 0 && strlen($intZip5) <= 5) || preg_match($expression, $strZip7)) {
            // zipcode!
            $sql = sprintf(
                "SELECT 0 AS all_in, `location`.id AS id, zipcode,
                city.name AS city, `state`.`name` AS state,
                `state`.abbreviation AS abbreviation
                FROM `location`
                    INNER JOIN city
                        ON location.city_id = city.id
                    INNER JOIN `state`
                        ON city.state_id = `state`.id
                WHERE country_id IN " . $countriesId . "
                    AND (zipcode LIKE :zip_code_like)
                GROUP BY zipcode
                ORDER BY zipcode, city_name, `state`.`name`
                LIMIT %d",
                $limit
            );

            $valuesToBind[':zip_code_like'] = "%" . $intZip5 . "%";
        } else {

            if (!strpos($q, ',') && strpos($q, ' ')) {

                $arrQ = explode(' ', $q);
                $state = $arrQ[count($arrQ) - 1];

                $sqlStateAb = "SELECT id
                    FROM state
                    WHERE country_id IN " . $countriesId . "
                    AND abbreviation = :abbreviation
                    AND state.id != 999999";

                $arrStateAb = $connection->cache(Yii::app()->params['cache_medium'])
                    ->createCommand($sqlStateAb)
                    ->bindValues([':abbreviation' => $state])
                    ->queryRow();

                if ((isset($arrStateAb['id']) && ($arrStateAb['id'])) && $arrStateAb['id'] > 0) {
                    $arrQ[count($arrQ) - 1] = ', ' . $arrQ[count($arrQ) - 1];
                    $q = implode(" ", $arrQ);
                }
            }

            // no zipcode
            if (strpos($q, ',') !== false) {

                $arrQ = explode(',', $q);
                $arrQ = array_filter($arrQ);
                $arrQ[0] = isset($arrQ[0]) ? trim($arrQ[0]) : '';
                $arrQ[1] = isset($arrQ[1]) ? trim($arrQ[1]) : '';

                if (sizeof($arrQ) == 2) {

                    // we have 1) city, state 2) city, state abbreviation
                    $sql = sprintf(
                        "SELECT 0 AS all_in, MIN(location.id) AS id, city.name AS city,
                        `state`.`name` AS `state`, `state`.abbreviation AS abbreviation
                        FROM `location`
                            INNER JOIN city
                                ON location.city_id = city.id
                            INNER JOIN `state`
                                ON city.state_id = `state`.id
                        WHERE country_id IN " . $countriesId . "
                            AND (city.name LIKE :city_name_like OR replace(city.name , ' ','') LIKE :city_name_like_bis)
                            AND (`state`.`name` LIKE :state_name_like OR state.abbreviation LIKE :state_abbr_like)
                            AND (city.id != 999999 AND `state`.id != 999999)
                        GROUP BY city.name, `state`.`name`
                        ORDER BY %s city.name, `state`.`name`
                        LIMIT %d",
                        $orderByAlliance,
                        $limit
                    );

                    $valuesToBind[':city_name_like'] = "%" . $arrQ[0] . "%";
                    $valuesToBind[':city_name_like_bis'] = "%" . $arrQ[0] . "%";
                    $valuesToBind[':state_name_like'] = "%" . $arrQ[1] . "%";
                    $valuesToBind[':state_abbr_like'] = "%" . $arrQ[1] . "%";
                } elseif (sizeof($arrQ) == 3) {

                    // we have 1) city, state 2) city, state abbreviation
                    $sql = sprintf(
                        "SELECT 0 AS all_in, MIN(`location`.id) AS id, city.name AS city,
                        `state`.`name` AS `state`, `state`.abbreviation AS abbreviation
                        FROM `location`
                            INNER JOIN city
                                ON `location`.city_id = city.id
                            INNER JOIN `state`
                                ON city.state_id = `state`.id
                        WHERE country_id IN " . $countriesId . "
                            AND (city.name LIKE :city_name_like OR replace(city.name , ' ','') LIKE :city_name_like_bis)
                            AND (`state`.`name` LIKE :state_name_like OR `state`.abbreviation LIKE :state_abbr_like)
                            AND (city.id != 999999 AND `state`.id != 999999)
                        GROUP BY city.name, `state`.`name`
                        ORDER BY %s city.name, `state`.`name`
                        LIMIT %d ;",
                        $orderByAlliance,
                        $limit
                    );

                    $valuesToBind[':city_name_like'] = "%" . $arrQ[0] . "%";
                    $valuesToBind[':city_name_like_bis'] = "%" . $arrQ[0] . "%";
                    $valuesToBind[':state_name_like'] = "%" . $arrQ[2] . "%";
                    $valuesToBind[':state_abbr_like'] = "%" . $arrQ[2] . "%";
                }
            } else {
                // we have 1) city 2) state
                if (strlen($q) <= 2) {
                    $sql = sprintf(
                        "SELECT 0 AS all_in, MIN(location.id) AS id, city.name AS city,
                        state.name AS state, state.abbreviation AS abbreviation
                        FROM location
                        INNER JOIN city
                        ON location.city_id = city.id
                        INNER JOIN state
                        ON city.state_id = state.id
                        WHERE country_id IN " . $countriesId . "
                        AND (state.abbreviation LIKE :state_abbr_like)
                        AND (city.id != 999999 AND state.id != 999999)
                        GROUP BY city.name, state.name
                        ORDER BY %s city.name, state.name
                        LIMIT %d",
                        $orderByAlliance,
                        $limit
                    );

                    $valuesToBind[':state_abbr_like'] = "%" . $q . "%";
                } else {

                    $sql = sprintf(
                        "SELECT 0 AS all_in, MIN(location.id) AS id, city.name AS city,
                        state.name AS state, state.abbreviation AS abbreviation
                        FROM location
                        INNER JOIN city
                        ON location.city_id = city.id
                        INNER JOIN state
                        ON city.state_id = state.id
                        WHERE country_id IN ".$countriesId."
                        AND (
                            city.name LIKE :city_name_like
                            OR
                            replace(city.name , ' ','') LIKE :city_name_like_bis
                            OR
                            state.name LIKE :state_name_like
                        )
                        AND (city.id != 999999 AND state.id != 999999)
                        GROUP BY city.name, state.name
                        ORDER BY %s city.name, state.name
                        LIMIT %d",
                        $orderByAlliance,
                        $limit
                    );

                    $valuesToBind[':city_name_like'] = "%" . $q . "%";
                    $valuesToBind[':city_name_like_bis'] = "%" . $q . "%";
                    $valuesToBind[':state_name_like'] = "%" . $q . "%";
                }
            }
        }

        $array = array();
        if (isset($sql)) {
            $array = $connection->cache(Yii::app()->params['cache_medium'])
                ->createCommand($sql)
                ->bindValues($valuesToBind)
                ->queryAll();
        }

        $array = array_filter($array);

        if (strlen($q) == 2) {
            //find state by abbreviation
            $sqlState = "SELECT 1 AS all_in, name AS state, abbreviation
                FROM state
                WHERE country_id IN " . $countriesId . "
                AND abbreviation = :abbreviation
                AND state.id != 999999
                ORDER BY name ASC
                LIMIT 1";

            $arrState = $connection->cache(Yii::app()->params['cache_medium'])
                ->createCommand($sqlState)
                ->bindValues([':abbreviation' => $q])
                ->queryAll();

            if (sizeof($arrState) > 0) {
                $arrState = array_filter($arrState);
                $array = array_merge($arrState, $array);
            }
        } else {
            //find state by name
            $sqlState = "SELECT 1 AS all_in, name AS state, abbreviation
                FROM state
                WHERE country_id IN " . $countriesId . "
                AND name LIKE :state_name_like
                AND state.id != 999999
                ORDER BY name ASC
                LIMIT 1";

            $arrState = $connection->cache(Yii::app()->params['cache_medium'])
                ->createCommand($sqlState)
                ->bindValues([':state_name_like' => "%" . $q . "%"])
                ->queryAll();
            if (sizeof($arrState) > 0) {
                $arrState = array_filter($arrState);
                $array = array_merge($arrState, $array);
            }
        }

        $array = array_filter($array);

        return array($strZip5 == $intZip5, $array);
    }

    /**
     * Function used to search for locations matching the input in the main query
     * @internal we should have only one place where we process different styles of inputting location data and analyze
     * it there
     * @internal currently it's spread everywhere
     *
     * @param string $q string for the query
     * @return array with the results of the query
     */
    public static function findByName($q, $countriesId = '1')
    {
        $valuesToBind = [];
        $q = trim($q);
        $array = array();
        $connection = Yii::app()->db;
        $countriesId = "(".$countriesId.")";

        // to check for zipcode
        $strZip5 = (string)substr($q, 0, 5);
        $intZip5 = (string)intval($strZip5, 10);

        // to check for autocompleted non-zipcode
        $arrQ = explode(',', $q);
        $arrQ[0] = isset($arrQ[0]) ? trim($arrQ[0]) : '';
        $arrQ[1] = isset($arrQ[1]) ? trim($arrQ[1]) : '';
        $arrQ[2] = isset($arrQ[2]) ? trim($arrQ[2]) : '';
        $sql = '';

        if (strlen($q) == 5 && ($strZip5 == $intZip5 || $strZip5 == '0' . $intZip5)) {
            //zipcode only
            $sql = "SELECT SQL_NO_CACHE id
                FROM location
                WHERE zipcode = :zip";

            $valuesToBind[':zip'] = $q;
        } elseif ($strZip5 == $intZip5 && strpos($q, '-') != 0) {
            // zipcode - city, abbreviation
            $arrQ = explode('-', $q);
            $arrQ[0] = trim($arrQ[0]);
            $zCityState = !empty($zCityState) ? trim($zCityState) : '';
            $arrQ[2] = trim(substr($arrQ[1], strpos($arrQ[1], ',') + 2));
            $arrQ[1] = trim(substr($arrQ[1], 0, strpos($arrQ[1], ',')));

            $sql = "SELECT SQL_NO_CACHE location.id
                FROM location
                    INNER JOIN city
                        ON city.id = location.city_id
                    INNER JOIN state
                        ON state.id = city.state_id
                WHERE (location.zipcode = :zip
                    AND location.city_name = :city
                    AND state.abbreviation = :state_abbr
                    )";

            $valuesToBind[':zip'] = $arrQ[0];
            $valuesToBind[':city'] = $arrQ[1];
            $valuesToBind[':state_abbr'] = $arrQ[2];
        } elseif ($arrQ[0] != '' && $arrQ[1] != '' && $arrQ[2] != '') {
            // city, abbreviation
            $sql = "SELECT SQL_NO_CACHE location.id
                FROM location
                    INNER JOIN city
                        ON city.id = location.city_id
                    INNER JOIN state
                        ON state.id = city.state_id
                    WHERE (city.name = :city AND state.abbreviation = :state_abbr)";

            $valuesToBind[':city'] = $arrQ[0];
            $valuesToBind[':state_abbr'] = $arrQ[2];
        } elseif ($arrQ[0] != '' && $arrQ[1] != '') {
            // city, abbreviation
            $sql = "SELECT SQL_NO_CACHE location.id
                FROM location
                    INNER JOIN city
                        ON location.city_id = city.id
                    INNER JOIN state
                        ON city.state_id = state.id
                WHERE (city.name = :city
                    AND state.abbreviation = :state_abbr)";

            $valuesToBind[':city'] = $arrQ[0];
            $valuesToBind[':state_abbr'] = $arrQ[1];
        } elseif ($arrQ[0] != '') {
                    // city, abbreviation
            $sql = "SELECT SQL_NO_CACHE location.id
                    FROM location
                        INNER JOIN city
                            ON location.city_id = city.id
                        INNER JOIN state
                            ON city.state_id = state.id
                    WHERE state.country_id IN " .$countriesId. " AND location.city_name = :city" ;

            $valuesToBind[':city'] = $arrQ[0];
        }

        if ($sql != '') {
            $command = $connection->cache(7 * Yii::app()->params['cache_long'])
                ->createCommand($sql)
                ->bindValues($valuesToBind);
            $assocArray = $command->queryAll();
            foreach ($assocArray as $v) {
                array_push($array, $v['id']);
            }
        }

        if (sizeof($array) == 0) {
            $valuesToBind = [];

            // either no results or no autocomplete was used

            if ($arrQ[0] != '' && $arrQ[1] != '') {
                // check for manual 1) city 2) city, state 3) city, state abbreviation
                $sql = "SELECT SQL_NO_CACHE location.id
                    FROM location
                        INNER JOIN city ON city.id = location.city_id
                        INNER JOIN state ON state.id = city.state_id
                    WHERE location.city_name = :city
                        AND (state.name = :state
                        OR state.abbreviation = :state_abbr)";

                $valuesToBind[':city'] = $arrQ[1];
                $valuesToBind[':state'] = $arrQ[0];
                $valuesToBind[':state_abbr'] = $arrQ[2];
            } else {
                // 1) city 2) state
                if (strlen($q) <= 2) {
                    //city state abbreviation
                    $sql = "SELECT SQL_NO_CACHE location.id
                        FROM location
                            INNER JOIN city
                                ON city.id = location.city_id
                            INNER JOIN state
                                ON state.id = city.state_id
                        WHERE state.abbreviation LIKE :state_abbr_like";

                    $valuesToBind[':state_abbr_like'] = '%' . $q . '%';
                } else {
                    //city state
                    $sql = "SELECT SQL_NO_CACHE location.id
                        FROM location
                            INNER JOIN city
                                ON city.id = location.city_id
                            INNER JOIN state
                                ON state.id = city.state_id
                        WHERE location.zipcode = :zip
                            OR location.city_name LIKE :city_name_like
                            OR state.name LIKE :state_name_like";

                    $valuesToBind[':zip'] = $q;
                    $valuesToBind[':city_name_like'] = '%' . $q . '%';
                    $valuesToBind[':state_name_like'] = '%' . $q . '%';
                }
            }

            $assocArray = $connection->cache(7 * Yii::app()->params['cache_long'])
                ->createCommand($sql)
                ->bindValues($valuesToBind)
                ->queryAll();

            $array = array();
            foreach ($assocArray as $v) {
                array_push($array, $v['id']);
            }
        }

        return $array;
    }

    /**
     * Find locations near any of the given locations, by looking up their average latitude and longitude
     *
     * @param array $sLocationId id of the location to work with
     * @param int   $miles number of miles to determine closeness
     * @return array
     */
    public static function findNearby($sLocationId, $miles)
    {
        $valuesToBind = [];

        if (is_array($sLocationId) && !empty($sLocationId)) {
            $sLocation = implode(',', $sLocationId);
        } elseif (intval($sLocationId) > 0) {
            $sLocation = $sLocationId;
            unset($sLocationId);
            $sLocationId[0] = $sLocation; //treat as array
        } else {
            //just quit
            return false;
        }

        // find avg lat/long
        $sql = "SELECT SQL_NO_CACHE AVG(latitude) AS avg_latitude, AVG(longitude) AS avg_longitude
            FROM location
                WHERE ABS(latitude) > 0
                    AND ABS(longitude) > 0
                    AND id IN (:ids)";

        $valuesToBind[':ids'] = $sLocation;

        $avg = Yii::app()->db->cache(Yii::app()->params['cache_medium'])
            ->createCommand($sql)
            ->bindValues($valuesToBind)
            ->queryAll();

        if ($avg[0]['avg_latitude'] && $avg[0]['avg_longitude']) {
            return self::findNearLatLon($avg[0]['avg_latitude'], $avg[0]['avg_longitude'], $miles);
        }
        return false;
    }

    /**
     * Find locations near a given latitude and longitude
     *
     * @param string $latitude
     * @param string $longitude
     * @param int   $miles number of miles to determine closeness
     * @return array
     */
    public static function findNearLatLon($latitude, $longitude, $miles)
    {
        $valuesToBind = [];

        // find locations nearby (this had a limit 2000, changed it to 1000 b/c it seemed too large at 20130722)
        // restored to 2000 on 20130731 b/c otherwise searching 100m from ny = searching 80m from ny
        $sql = "SELECT SQL_NO_CACHE location.id AS id,
            3956 *
            2 *
            ASIN(
                SQRT(
                    POWER(SIN((:latitude - ABS(location.latitude)) * PI() / 180 / 2), 2)
                    +
                    COS(:latitude_bis * PI() / 180)
                    *
                    COS(ABS(location.latitude) * PI() / 180)
                    *
                    POWER(SIN((:longitude - location.longitude) * PI() / 180 / 2), 2))
                ) AS distance
            FROM location
            WHERE ABS(latitude) > 0
            AND ABS(longitude) > 0
            HAVING distance < :distance
            ORDER BY distance
            LIMIT 2000";

        $valuesToBind[':latitude'] = $latitude;
        $valuesToBind[':latitude_bis'] = $latitude;
        $valuesToBind[':longitude'] = $longitude;
        $valuesToBind[':distance'] = $miles;

        $res = Yii::app()->db->cache(Yii::app()->params['cache_medium'])
            ->createCommand($sql)
            ->bindValues($valuesToBind)
            ->queryAll();

        foreach ($res as $v) {
            $sLocationId[] = $v['id'];
        }

        // return an array without duplicated
        if (!empty($sLocationId)) {
            return array_values(array_unique($sLocationId));
        }
        return false;
    }

    /**
     * Find cityName beforeSave
     * @return bool
     */
    public function beforeSave()
    {

        // do we have a latitude?
        if ($this->latitude == "" && $this->longitude != '') {
            $this->addError('latitude', 'Latitude cannot be blank.');
            return false;
        }

        // do we have a longitude?
        if ($this->longitude == "" && $this->latitude != '') {
            $this->addError('longitude', 'Longitude cannot be blank.');
            return false;
        }

        // sanitize zipcode
        $this->zipcode = StringComponent::sanitize($this->zipcode);

        // check if this zipcode exists for this city
        $sql = "SELECT id
            FROM location
            WHERE city_id = :city_id
                AND zipcode = :zip
                AND id != :id
            LIMIT 1";

        $exists = Yii::app()->db
            ->createCommand($sql)
            ->bindValues([
                ':city_id' => $this->city_id,
                ':zip' => $this->zipcode,
                ':id' => $this->id
            ])
            ->queryRow();

        if (!empty($exists)) {
            $this->addError('zipcode', 'Location already exists in this city.');
            return false;
        }

        // is this an existing record?
        if (!$this->isNewRecord) {
            // yes, check for duplicate names
            $duplicateName = Location::model()->exists(
                'city_name=:city_name AND zipcode=:zipcode AND id !=:id',
                array(':city_name' => $this->city_name, ':zipcode' => $this->zipcode, ':id' => $this->id)
            );
        } else {
            // no, check for duplicate names
            $duplicateName = Location::model()->exists(
                'city_name=:city_name AND zipcode=:zipcode',
                array(':city_name' => $this->city_name, ':zipcode' => $this->zipcode)
            );
        }

        // was the name a duplicate?
        if ($duplicateName) {
            // yes, exit
            $this->addError('city_name', 'Duplicate Name.');
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Update location data in practice
     * @return bool
     */
    public function afterSave()
    {

        if (!$this->isNewRecord) {

            $old = $this->oldRecord;

            // update in practice if something changed
            $attributesChanged = ($old->zipcode != $this->zipcode || $old->city_id != $this->city_id || $old->latitude
                != $this->latitude || $old->longitude != $this->longitude);

            if ($attributesChanged && (!empty($this->latitude) && !empty($this->longitude))) {

                // update lat/long in practice
                $arrPractices = Practice::model()->findAll('location_id=:location_id', array(':location_id' =>
                $this->id));
                foreach ($arrPractices as $practice) {
                    if (empty($practice->latitude) || $practice->latitude == $old->latitude) {
                        $practice->latitude = $this->latitude;
                    }
                    if (empty($practice->longitude) || $practice->longitude == $old->longitude) {
                        $practice->longitude = $this->longitude;
                    }
                }
            }
        }

        return parent::afterSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from location if associated records exist in practice table
        $hasAssociatedRecords = Practice::model()->exists(
            'location_id=:location_id',
            array(':location_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError("id", "Location can't be deleted because there are practices linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Get top zipcodes for a given insurance company in a given city
     * @param int $cityId
     * @param int $insuranceId
     * @return array
     */
    public function getTopZipcodesByCityInsurance($cityId, $insuranceId)
    {
        $criteria['insurance.company_id'] = $insuranceId;
        return $this->getTopZipcodesByCity($cityId, $criteria);
    }

    /**
     * Get top zipcodes for a given specialty in a given city
     * @param int $cityId
     * @param int $specialtyId
     * @return array
     */
    public function getTopZipcodesByCitySpecialty($cityId, $specialtyId)
    {
        $criteria['specialty.specialty_id'] = $specialtyId;
        return $this->getTopZipcodesByCity($cityId, $criteria);
    }

    /**
     * Returns the location and the country in a given practice
     * @param int $practiceId
     * @return arr
     */
    public function getPracticeLocationId($practiceId = 0)
    {

        $return['location_id'] = 999999; // unknown location
        $return['country_id'] = 1;
        $return['zipcode'] = 99999; // unknown zipcode

        if ($practiceId > 0) {
            $sql = "SELECT practice.location_id, state.country_id, location.zipcode
                FROM practice
                LEFT JOIN location on location.id = practice.location_id
                LEFT JOIN city on city.id = location.city_id
                LEFT JOIN state on state.id = city.state_id
                WHERE practice.id = :practice_id LIMIT 1";

            $return = Yii::app()->db->cache(Yii::app()->params['cache_short'])
                ->createCommand($sql)
                ->bindValues([':practice_id' => $practiceId])
                ->queryRow();
        }
        return $return;
    }

    /**
     * Returns the location and the country in a given zipcode
     * @param int $zipCode
     * @return arr
     */
    public static function getCityStatedByZipCode($zipCode)
    {
        $sql = "SELECT
                state.abbreviation AS state,
                state.name AS state_name,
                location.city_name AS city,
                location.latitude AS latitude,
                location.longitude AS longitude
            FROM location
            LEFT JOIN city
                ON city.id = location.city_id
            LEFT JOIN state
                ON state.id = city.state_id
            WHERE location.zipcode = :zip
            LIMIT 1";

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValues([':zip' => $zipCode])
            ->queryRow();
    }

    /**
     * Returns the location and the country in a given zipcode
     * @param int $practiceId
     * @return arr
     */
    public function getLocationIdByZipCode($zipCode = '')
    {

        $return['location_id'] = 999999; // unknown location
        $return['country_id'] = 1;
        $return['city_name'] = 'Unknown';
        $return['zipcode'] = 99999; // unknown zipcode
        $return['success'] = 0;

        $zipCode = trim($zipCode);
        if (!empty($zipCode)) {
            $sql = "SELECT location.id as location_id, state.country_id, location.zipcode,
                location.city_name, 1 as success, city.friendly_url
                FROM location
                LEFT JOIN city
                    ON city.id = location.city_id
                LEFT JOIN state
                    ON state.id = city.state_id
                WHERE location.zipcode = :zip LIMIT 1";

            $return = Yii::app()->db->cache(Yii::app()->params['cache_short'])
                ->createCommand($sql)
                ->bindValues([':zip' => $zipCode])
                ->queryRow();
        }
        return $return;
    }

    /**
     * Checks if notifications should be sent to partners for the current record.
     * @return boolean
     */
    public function pdiDisabled()
    {
        if (!empty($this->city)) {
            return $this->city->pdiDisabled();
        }
        return false;
    }

    /**
     * Return top zipcodes in a particular city by number of providers based on criteria
     * @param int $cityId
     * @param array $criteria
     * @param int $maxResultSize
     * @return array
     */
    private function getTopZipcodesByCity($cityId, $criteria, $maxResultSize = 9999)
    {
        $criteria['practice.city_id'] = $cityId;
        $indexResults = DoctorIndex::topHits($criteria, 'practice.zipcode', $maxResultSize);

        $sql = "SELECT SQL_NO_CACHE zipcode
            FROM location
            WHERE city_id = :city_id";

        $sqlResults = Yii::app()->db->cache(86400)
            ->createCommand($sql)
            ->bindValues([':city_id' => $cityId])
            ->queryAll();

        return array_intersect(array_column($indexResults, 'key'), array_column($sqlResults, 'zipcode'));
    }

    /**
     * Get the timezone of the location
     *
     * @deprecated Not used anymore
     *
     * @return array
     */
    public function getTimeZone()
    {
        $phpTimeZone = array(
            "EST" => "America/New_York",
            "CST" => "America/Chicago",
            "MST" => "America/Denver",
            "PST" => "America/Los_Angeles",
            "AKST" => "America/Anchorage",
            "HI" => "America/Adak",
            "AST" => "America/Puerto_Rico"
        );

        $sql = "SELECT DISTINCT t.time_zone
            FROM location l
                INNER JOIN city c ON l.city_id = c.id
                INNER JOIN `state` s ON c.state_id = s.id
                INNER JOIN timezone t ON s.abbreviation = t.state_code AND t.area_code =
                (SELECT LEFT(p.phone_number, 3) as ac
            FROM practice p
            WHERE p.location_id = l.id
            GROUP BY ac
            ORDER BY COUNT(p.id) DESC LIMIT 1)
            WHERE l.id = :id";

        $timeZone = Yii::app()->db
            ->createCommand($sql)
            ->bindValues([':id' => $this->id])
            ->queryScalar();

        return array("abbreviation" => $timeZone, "phpTimezone" => $phpTimeZone[$timeZone]);
    }

    /**
     * Clean Up US ZIP Codes
     * @param string $zipCode
     * @return string Sanitized ZipCode
     */
    public static function cleanUpUsZipCodes($zipCode)
    {
        if (empty($zipCode)) {
            return null;
        }
        $zipCode = trim($zipCode);
        $explodedZipCode = explode('-', $zipCode);

        if (empty($explodedZipCode[0])) {
            return null;
        }
        $zipCode = (string)$explodedZipCode[0];
        return (string)trim($zipCode);
    }

    public static function getByZipCode($zipCode, $limit = 10, $country = 1)
    {
        $whereCountry = "";
        if ($country !== "all") {
            $whereCountry = "AND country_id = ". intval($country);
        }

        $sql = "SELECT location.id, AVG(latitude) AS latitude, AVG(longitude) AS longitude, zipcode,
                CONCAT(city.name, ', ', state.abbreviation) AS city
                FROM location
                INNER JOIN city
                ON location.city_id = city.id
                INNER JOIN state
                ON city.state_id = state.id
                WHERE zipcode LIKE :zip
                AND latitude IS NOT NULL
                AND longitude IS NOT NULL
                    " . $whereCountry . "
                GROUP By location.id, zipcode, city
                ORDER BY zipcode
                LIMIT " . $limit . ";";

        $q = Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValues([':zip' => $zipCode."%"]);

        if ($limit == 1) {
            return $q->queryRow();
        }

        return $q->queryAll();
    }
}
