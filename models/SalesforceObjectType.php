<?php

Yii::import('application.modules.core_models.models._base.BaseSalesforceObjectType');

class SalesforceObjectType extends BaseSalesforceObjectType
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
