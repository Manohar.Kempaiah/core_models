<?php

Yii::import('application.modules.core_models.models._base.BaseCpnSoftwareService');

class CpnSoftwareService extends BaseCpnSoftwareService
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get Services
     *
     * @param int $installationId
     * @return array
     */
    public function getServices($installationId)
    {
        // Query
        $sql = sprintf(
            "SELECT
            ss.guid AS identifier,
            ss.service_name AS name,
            s.installation_folder AS installationFolder,
            s.file_name AS exeName
            FROM cpn_software_installation si
                INNER JOIN cpn_installation i on si.cpn_installation_id = i.id
                INNER JOIN cpn_software s on si.cpn_software_id = s.id
                INNER JOIN cpn_software_service ss on s.id = ss.cpn_software_id
            WHERE i.id = '%s'
                AND i.enabled_flag = 1
                AND i.del_flag = 0
                AND s.del_flag = 0
                AND s.enabled_flag = 1
                AND si.del_flag = 0
                AND si.enabled_flag = 1;",
            $installationId
        );

        // Return all the result
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

}
