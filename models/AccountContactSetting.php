<?php

Yii::import('application.modules.core_models.models._base.BaseAccountContactSetting');

class AccountContactSetting extends BaseAccountContactSetting
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * get account_contact_settings details
     * @param int $accountContactId
     * @return arr
     */
    public static function getContactSetting($accountContactId = 0)
    {

        if ($accountContactId == 0) {
            return false;
        }

        $sql = sprintf(
            "SELECT id, account_contact_id, practice_id, provider_id
            FROM account_contact_setting
            WHERE account_contact_id = %d
            ORDER BY practice_id ASC, provider_id ASC;",
            $accountContactId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get account_contact_settings details
     * @param str $alertType : SMS || Email
     * @param str $alertField
     * @param int $practiceId
     * @param int $providerId
     * @param int $accountId
     * @return mixed
     */
    public static function getAlertsTarget(
        $alertType = '',
        $alertField = '',
        $practiceId = 0,
        $providerId = 0,
        $accountId = 0
    )
    {

        if ($alertField == 'alert_billing' || $alertField == 'alert_report') {

            if ($alertType == 'SMS') {
                $alertTypeCondition = " AND ac.mobile_phone != ''";
            } else {
                $alertTypeCondition = " AND ac.email != ''";
            }
            $fieldName = 'ac.' . $alertField;
            $alertFieldCondition = sprintf("AND ( %s = '%s' OR %s = 'BOTH')", $fieldName, $alertType, $fieldName);

            $sql = sprintf(
                "SELECT acs.account_contact_id, ac.name, ac.mobile_phone,
                a.location_id
                FROM account_contact_setting AS acs
                LEFT JOIN account_contact AS ac
                ON ac.id = acs.account_contact_id
                LEFT JOIN account AS a
                ON a.id = ac.account_id
                WHERE ac.account_id = %d AND ac.active = 1
                %s
                %s ;",
                $accountId,
                $alertTypeCondition,
                $alertFieldCondition
            );
            return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
        }

        if (empty($alertType) || empty($alertField) || empty($practiceId)) {
            return false;
        }

        $accountPractices = $accountProviders = $targetAccounts = array();

        // find accounts linked to this practice
        $accountPractices = AccountPractice::model()->getAccountsByPractice($practiceId);

        $providerCondition = '';
        if ($providerId > 0) {
            // find accounts linked to this provider
            $accountProviders = AccountProvider::model()->getAccountsByProvider($providerId);
            $providerCondition = sprintf("AND (acs.provider_id = %d OR acs.provider_id IS NULL )", $providerId);
        }

        // make list of accounts related to this practices or provider
        foreach ($accountPractices as $ap) {
            $targetAccounts[$ap['account_id']] = $ap['account_id'];
        }
        foreach ($accountProviders as $ap) {
            $targetAccounts[$ap['account_id']] = $ap['account_id'];
        }
        $listAccount = implode(',', $targetAccounts);

        $fieldCondition = '';
        if ($alertField == 'alert_app_request') {
            $fieldCondition = sprintf(" AND (ac.alert_app_request = '%s' OR ac.alert_app_request = 'BOTH')", $alertType);

        } elseif ($alertField == 'alert_review') {
            $fieldCondition = sprintf(" AND (ac.alert_review = '%s' OR ac.alert_review = 'BOTH')", $alertType);

        } elseif ($alertField == 'alert_tracked_call') {
            $fieldCondition = sprintf(" AND (ac.alert_tracked_call = '%s' OR ac.alert_tracked_call = 'BOTH')", $alertType);
        }

        if (empty($listAccount) || empty($fieldCondition)) {
            return false;
        }

        if ($alertType == 'SMS') {
            $alertTypeCondition = " AND ac.mobile_phone != ''";
        } else {
            $alertTypeCondition = " AND ac.email != ''";
        }

        $sql = sprintf(
            "SELECT acs.account_contact_id, ac.name, ac.mobile_phone, a.location_id
            FROM account_contact_setting AS acs
            LEFT JOIN account_contact AS ac ON ac.id = acs.account_contact_id
            LEFT JOIN account AS a ON a.id = ac.account_id
            WHERE ac.account_id IN (%s) AND ac.active = 1
            %s
            AND ( (acs.practice_id = %d %s )
                OR ( acs.provider_id IS NULL AND acs.practice_id IS NULL )
            )
            %s;",
            $listAccount,
            $alertTypeCondition,
            $practiceId,
            $providerCondition,
            $fieldCondition
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

}
