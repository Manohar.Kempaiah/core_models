<?php

Yii::import('application.modules.core_models.models._base.BaseOrganization');

// autoload vendors class
Common::autoloadVendors();
class Organization extends BaseOrganization
{
    public $partner_name;

    const STATUS_ACTIVE = 'A';
    const STATUS_PAUSED = 'P';
    const STATUS_CANCELED = 'C';

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'organization_name';
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('organization_group_id, partner_site_id', 'numerical', 'integerOnly' => true),
            array('organization_name', 'length', 'max' => 100),
            array('status, onboarding', 'length', 'max' => 1),
            array('onboarding_task_id', 'length', 'max' => 6),
            array('salesforce_id', 'length', 'max' => 18),
            array('salesforce_id', 'unique'),
            array('type', 'length', 'max' => 22),
            array('yext_customer_id', 'length', 'max' => 15),
            array('date_added, date_updated', 'safe'),
            array('pg_client_id, provconn_url', 'length', 'max'=>255),
            array('pg_client_id, provconn_url, provconn_last_update', 'default', 'setOnEmpty' => true, 'value' => null),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array(
                'id, organization_group_id, organization_name, status, onboarding, onboarding_task_id,
                salesforce_id, yext_customer_id, partner_site_id, date_added, date_updated, pg_client_id',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'organization_group_id' => 'Organization Group',
            'organization_name' => 'Organization Name',
            'status' => 'Status',
            'onboarding' => 'Onboarding',
            'onboarding_task_id' => 'Onboarding Task',
            'salesforce_id' => 'Salesforce',
            'type' => Yii::t('app', 'Type'),
            'yext_customer_id' => 'Yext Customer',
            'partner_site_id' => 'Partner Site',
            'date_added' => 'Date Added',
            'date_updated' => 'Date Updated',
            'pg_client_id' => 'PG Client ID Mapping',
            'bf_brand_id' => 'BF Brand ID',
            'dm_id' => 'Data Manager ID',
        );
    }

    /**
     * Actions to take before delete the organization
     */
    public function beforeDelete()
    {

        // Search for the organizationDetails record
        $organizationDetails = OrganizationDetails::model()->find(
            'organization_id=:organization_id',
            array(':organization_id' => $this->id)
        );

        // Orga.Details record exists?
        if (!empty($organizationDetails)) {
            $organizationDetails->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * Update SalesForce if a SalesForce ID was entered and this account didn't use to have one
     * @return boolean
     */
    public function beforeSave()
    {
        $oldRecord = $this->oldRecord;
        // get previous research_mode status
        if (!$this->isNewRecord && Yii::app()->params['environment'] == 'prod') {
            if (
                $oldRecord->salesforce_id == ''
                && $this->salesforce_id != ''
                && strlen($this->salesforce_id) == 18
            ) {
                // a salesforce id was entered, add a note
                $data = array(
                    'salesforce_id' => $this->salesforce_id,
                    'subject' => "Linked to Doctor.com",
                    'description' => "This account was not previously connected to Doctor.com. This has been remedied."
                );
                SqsComponent::sendMessage('salesforceSaveAccountNote', $data);

                // update the actual data
                $sfData = array();
                $sfData["Doctor_com_Org_Id__c"] = $this->id;

                $ownerAccount = $this->getOwnerAccount();

                $sfData["Doctor_com_Account_Id__c"] = $ownerAccount->id;
                $sfData["Phab_Task_URL__c"] = Yii::app()->params->servers['dr_dev_hn'] . '/'
                    . $this->onboarding_task_id;

                SqsComponent::sendMessage('salesforceUpdateAccount', $this->salesforce_id, $sfData);
            }
        }

        // is the SF ID empty?
        if (empty($this->salesforce_id)) {
            // mark it as null so we don't have duplicate key errors (it's unique but nullable)
            $this->salesforce_id = null;
        }

        // did the organization status change?
        if (
            !$this->isNewRecord
            && is_object($oldRecord)
            && $oldRecord->status != $this->status
        ) {
            // yes, look up the organization's accounts
            $orgAccounts = OrganizationAccount::model()->findAll(
                "organization_id = :organization_id",
                array(":organization_id" => $this->id)
            );

            // loop through them
            foreach ($orgAccounts as $orgAccount) {
                // find all their widgets
                $widgets = Widget::model()->findAll(
                    "account_id = :account_id",
                    array(":account_id" => $orgAccount->account_id)
                );

                // loop through them
                foreach ($widgets as $widget) {
                    // clear their cache
                    Widget::clear($widget->key_code);
                }
            }
        }

        $this->survey_syndication_enabled_date=date('Y-m-d', strtotime($this->survey_syndication_enabled_date));

        return parent::beforeSave();
    }

    /**
     * Make some updates and validation after save the Organization record
     * @return boolean
     */
    public function afterSave()
    {

        $wasChanged = false;
        // Search for the organizationDetails record
        $organizationDetails = OrganizationDetails::model()->find(
            'organization_id=:organization_id',
            array(':organization_id' => $this->id)
        );

        // Orga.Details record exists?
        if (empty($organizationDetails)) {
            // Nop, so create it
            $organizationDetails = new OrganizationDetails();
            $organizationDetails->organization_id = $this->id;
            $organizationDetails->date_added = TimeComponent::getNow();
            $organizationDetails->scan_template_type_id = 2; // internal
            $wasChanged = true;
        }

        // The partner_site_id was changed ?
        if (empty($this->oldRecord)
            || (!empty($this->oldRecord) && $this->oldRecord->partner_site_id != $this->partner_site_id)
        ) {

            // yes, was changed.
            // update partner_site_id for all account of this organization
            $orgAccount = OrganizationAccount::model()->findAll(
                "organization_id=:organization_id",
                array(":organization_id" => $this->id)
            );
            if (!empty($orgAccount)) {
                foreach ($orgAccount as $oa) {
                    $account = Account::model()->find("id=:id", array(":id" => $oa->account_id));
                    if (!empty($account)) {
                        $account->partner_site_id = $this->partner_site_id;
                        $account->save();
                    }
                }
            }
        }

        // Have an scan_template_type_id ?
        if (
            empty($organizationDetails->scan_template_type_id)
            || $organizationDetails->scan_template_type_id == ScanTemplateType::TYPE_INTERNAL_ID
        ) {
            // Nop, check for the partner setting

            $partnerSiteId = $this->partner_site_id;

            // IS the org. related to a partner?
            if (!empty($partnerSiteId)) {
                // Yes

                $partnerSite = PartnerSite::model()->findByPk($partnerSiteId);
                if (!empty($partnerSite)) {
                    $partnerSiteScanTemplateTypeId = $partnerSite->scan_template_type_id;
                }

                // The partner have an scan_template_type_id ?
                if (!empty($partnerSiteScanTemplateTypeId)) {
                    // Yes

                    // Assign the partner scan_template_type_id to the organization
                    $organizationDetails->scan_template_type_id = $partnerSiteScanTemplateTypeId;
                    $wasChanged = true;
                }
            }
        }

        // Now, we have an scan_template_type_id for the organization?
        if (
            empty($organizationDetails->scan_template_type_id)
            || $organizationDetails->scan_template_type_id == ScanTemplateType::TYPE_INTERNAL_ID
        ) {
            // Nop

            // Is a Healthcare Institution?
            if ($this->type == 'Healthcare Institution') {
                // Yes, is a Healthcare Institution

                // Set scan_template_type_id=5 (ENT) as default
                $organizationDetails->scan_template_type_id = 5;
                $wasChanged = true;
            }
        }

        // We made some changes to the OrganizationDetails record?
        if ($wasChanged) {
            // Yes

            $organizationDetails->save();
        }

        return parent::afterSave();
    }

    /**
     * Change an Organization's status and record this in the organization_history table
     * @param string $mode
     * @return boolean
     */
    public function changeStatus($mode)
    {

        // check if organization already has the requested status
        if ($mode == $this->status) {
            // nothing to do, this may happen because sqs sends the same message several times

            if (!empty($this->salesforce_id)) {
                // save a note in salesforce
                $data = array(
                    'salesforce_id' => $this->salesforce_id,
                    'subject' => "Status unchanged",
                    'description' => "Nothing to do: new status " . $mode
                        . " is the same as current status " . $this->status
                );
                SqsComponent::sendMessage('salesforceSaveAccountNote', $data);
                return false;
            }
        }

        $this->status = $mode;

        if ($this->save(false)) {
            $historyEntry = new OrganizationHistory();
            $historyEntry->organization_id = $this->id;
            $historyEntry->event = $mode;
            // magic account_id = 1:
            // default to tech@corp.doctor.com for command-line processes that don't have a user id
            if (php_sapi_name() === 'cli') {
                $historyEntry->account_id = 1;
            } else {
                $historyEntry->account_id = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
            }

            // If the organization change the status then clear the cache
            $arrAccounts = OrganizationAccount::getAllAccountId(0, $this->id);
            if (!empty($arrAccounts)) {
                // Search for all widgets related to the accounts
                $lstAccounts = implode(',', $arrAccounts);
                $sql = sprintf("SELECT * FROM widget WHERE account_id IN (%s)", $lstAccounts);
                $arrW = Yii::app()->dbRO->createCommand($sql)->queryRow();
                if (!empty($arrW)) {
                    foreach ($arrW as $w) {
                        // For each widget, clear the cache
                        Widget::clear($w['key_code']);
                    }
                }
            }

            return $historyEntry->save();
        }
        return false;
    }

    /**
     * remove a single OrganizationFeature
     *
     * @param $featID int - the ID of the Feature being removed
     * @param $providerID int - the ID of the Provider that has the Feature attached
     * @param $practiceID int - the ID of the Practice that has the Feature attached
     */
    public function removeFeature($featID, $providerID, $practiceID)
    {

        if ($providerID) {
            $currentOrgFeature = OrganizationFeature::model()->find(
                'organization_id = :organization_id AND feature_id = :feature_id AND provider_id = :provider_id',
                array(
                    ':organization_id' => $this->id,
                    ':feature_id' => $featID,
                    ':provider_id' => $providerID
                )
            );
        } elseif ($practiceID) {
            $currentOrgFeature = OrganizationFeature::model()->find(
                'organization_id = :organization_id AND feature_id = :feature_id AND practice_id = :practice_id',
                array(
                    ':organization_id' => $this->id,
                    ':feature_id' => $featID,
                    ':practice_id' => $practiceID
                )
            );
        }

        // Does the feature exist?
        if (!$currentOrgFeature) {
            // No, so it is already removed
            return true;
        }

        // Deactivate
        if ($currentOrgFeature->deactivate()) {

            $deleteFeatured = true;
            // Enable HG PatientPursuit #
            if ($featID == 18) {

                // Find by Provider or Practice
                if ($providerID) {

                    $featuredHealthgrades = OrganizationFeature::model()->find(
                        'organization_id = :organization_id '
                            . 'AND feature_id = :feature_id '
                            . 'AND provider_id = :provider_id',
                        array(
                            ':organization_id' => $this->id,
                            ':feature_id' => 14,
                            ':provider_id' => $providerID
                        )
                    );

                    $enhancedHealthgrades = OrganizationFeature::model()->find(
                        'organization_id = :organization_id '
                            . 'AND feature_id = :feature_id '
                            . 'AND provider_id = :provider_id',
                        array(
                            ':organization_id' => $this->id,
                            ':feature_id' => 15,
                            ':provider_id' => $providerID
                        )
                    );
                    // Find by Practice
                } elseif ($practiceID) {

                    $featuredHealthgrades = OrganizationFeature::model()->find(
                        'organization_id = :organization_id '
                            . 'AND feature_id = :feature_id '
                            . 'AND practice_id = :practice_id',
                        array(
                            ':organization_id' => $this->id,
                            ':feature_id' => 14,
                            ':practice_id' => $practiceID
                        )
                    );

                    $enhancedHealthgrades = OrganizationFeature::model()->find(
                        'organization_id = :organization_id '
                            . 'AND feature_id = :feature_id '
                            . 'AND practice_id = :practice_id',
                        array(
                            ':organization_id' => $this->id,
                            ':feature_id' => 15,
                            ':practice_id' => $practiceID
                        )
                    );
                }

                // If Featured Healthgrades or Enhanced Healthgrades exisit can't delete
                if ($featuredHealthgrades || $enhancedHealthgrades) {
                    $deleteFeatured = false;
                }
            }

            // featID 14: Featured on HG || featID 15: Enhanced on HG
            if ($featID == 14 || $featID == 15) {
                // delete feature 18: Enable HG PatientPursuit
                $this->removeFeature(18, $providerID, 0);
            }

            $insertHistory = true;
            // If delete featured is true
            if ($deleteFeatured && !$currentOrgFeature->delete()) {
                // If can't delete OrgFeature can't insert history
                $insertHistory = false;
            }

            // If insert history is true
            if ($insertHistory) {
                $historyEntry = new OrganizationHistory;
                $historyEntry->organization_id = $this->id;
                $historyEntry->event = "R";
                $historyEntry->feature_id = $featID;

                if ($providerID) {
                    $historyEntry->provider_id = $providerID;
                } elseif ($practiceID) {
                    $historyEntry->practice_id = $practiceID;
                }

                // magic account_id = 1:
                // default to tech@corp.doctor.com for command-line processes that don't have a user id
                if (php_sapi_name() === 'cli') {
                    $historyEntry->account_id = 1;
                } else {
                    $historyEntry->account_id = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
                }

                if ($historyEntry->save()) {
                    return true;
                }
            }
            return true;
        }
        $this->addError("id", "Couldn't remove feature #" . $this->id);
        return false;
    }

    /**
     * add a single OrganizationFeature
     *
     * @param $featID int - the ID of the Feature being added
     * @param $providerID int - the ID of the Provider that should have the Feature attached
     * @param $practiceID int - the ID of the Practice that should have the Feature attached
     * @param $active int
     */
    public function addFeature($featID, $providerID, $practiceID, $active = 1)
    {
        if ($providerID > 0 || $practiceID > 0) {
            // Cancelled organizations can't have active features
            if ($this->status == 'C') {
                $this->addError('addFeature', 'Could not add features to a canceled organization');
                return false;
            }

            // Find Organization Feature
            if ($providerID > 0) {
                $currentOrgFeature = OrganizationFeature::model()->find(
                    'feature_id = :feature_id AND organization_id = :organization_id AND provider_id = :provider_id',
                    array(
                        ':feature_id' => $featID,
                        ':organization_id' => $this->id,
                        ':provider_id' => $providerID
                    )
                );
                // Find by Practice
            } else {
                $currentOrgFeature = OrganizationFeature::model()->find(
                    'feature_id = :feature_id AND organization_id = :organization_id AND practice_id = :practice_id',
                    array(
                        ':feature_id' => $featID,
                        ':organization_id' => $this->id,
                        ':practice_id' => $practiceID
                    )
                );
            }

            // If not exist Insert
            if (!$currentOrgFeature || $featID == 2) {
                $currentOrgFeature = new OrganizationFeature;
                $currentOrgFeature->organization_id = $this->id;
                $currentOrgFeature->feature_id = $featID;
                // if Provider Id exist
                if ($providerID > 0) {
                    $currentOrgFeature->provider_id = $providerID;

                    // else if practice Id exist
                } elseif ($practiceID > 0) {
                    $currentOrgFeature->practice_id = $practiceID;
                }

                // save
                if (!$currentOrgFeature->save()) {
                    $this->addError("addFeature", "Couldn't save feature.");
                    return false;
                }
            }

            $activation = true;
            // Active Organization Feature
            if ($active == 1) {
                $activation = $currentOrgFeature->activate();
            }

            // featID 14: Featured on HG || featID 15: Enhanced on HG
            if ($featID == 14 || $featID == 15) {
                // insert feature 18: Enable HG PatientPursuit
                $this->addFeature(18, $providerID, 0, 0);
            }

            // Insert Organization History
            if ($activation) {
                $historyEntry = new OrganizationHistory;
                $historyEntry->organization_id = $this->id;
                $historyEntry->event = "N";
                $historyEntry->feature_id = $featID;

                if ($providerID) {
                    $historyEntry->provider_id = $providerID;
                } elseif ($practiceID) {
                    $historyEntry->practice_id = $practiceID;
                }

                // magic account_id = 1:
                // default to tech@corp.doctor.com for command-line processes that don't have a user id
                if (php_sapi_name() === 'cli') {
                    $historyEntry->account_id = 1;
                } else {
                    $historyEntry->account_id = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
                }

                if ($historyEntry->save()) {
                    return $activation;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    public function addDefaultFeature($featID, $providerID, $practiceID, $active = 1)
    {

        if ($providerID > 0 || $practiceID > 0) {

            // Cancelled organizations can't have active features
            if ($this->status == 'C') {
                $this->addError('addFeature', 'Could not add features to a canceled organization');
                return false;
            }

            // If not exist Insert

                $currentOrgFeature = new OrganizationFeature;
                $currentOrgFeature->organization_id = $this->id;
                $currentOrgFeature->feature_id = $featID;
                $currentOrgFeature->active = $active;
                // if Provider Id exist
                if ($providerID > 0) {
                    $currentOrgFeature->provider_id = $providerID;

                    // else if practice Id exist
                } elseif ($practiceID > 0) {
                    $currentOrgFeature->practice_id = $practiceID;
                }

                // save
                if (!$currentOrgFeature->save()) {
                    echo "NOT SAVED";
                    $this->addError("addFeature", "Couldn't save feature.");
                    return false;
                } else {
                    echo "SAVED";
                }


            $activation = true;
            // Active Organization Feature
            if ($active == 1) {
                $activation = $currentOrgFeature->activate();
            }

            // featID 14: Featured on HG || featID 15: Enhanced on HG
            if ($featID == 14 || $featID == 15) {
                // insert feature 18: Enable HG PatientPursuit
                $this->addFeature(18, $providerID, 0, 0);
            }

            // Insert Organization History
            if ($activation) {
                $historyEntry = new OrganizationHistory;
                $historyEntry->organization_id = $this->id;
                $historyEntry->event = "N";
                $historyEntry->feature_id = $featID;

                if ($providerID) {
                    $historyEntry->provider_id = $providerID;
                } elseif ($practiceID) {
                    $historyEntry->practice_id = $practiceID;
                }

                // magic account_id = 1:
                // default to tech@corp.doctor.com for command-line processes that don't have a user id
                if (php_sapi_name() === 'cli') {
                    $historyEntry->account_id = 1;
                } else {
                    $historyEntry->account_id = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
                }

                if ($historyEntry->save()) {
                    return $activation;
                } else {
                    return false;
                }
            }
        }
        return false;
    }

    /**
     * retrieve the 'O'-level Account associated with the Organization
     *
     * @return Account
     */
    public function getOwnerAccount()
    {
        $orgAccount = OrganizationAccount::model()->find(
            'organization_id = :organization_id AND connection = :connection',
            array(':organization_id' => $this->id, ':connection' => "O")
        );

        if (empty($orgAccount->account_id)) {
            return false;
        }

        return Account::model()->findByPK($orgAccount->account_id);
    }

    /**
     * get the organization linked to an accountId
     * @params int $accountId
     * @params bool $useCache
     * @return array
     */
    public static function getByAccountId($accountId = 0, $useCache = true)
    {
        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;

        $sql = sprintf(
            'SELECT o.id, o.organization_group_id, o.organization_name, o.status, o.onboarding,
            o.onboarding_task_id, o.salesforce_id, o.yext_customer_id, o.date_added, o.adwords_cid,
            o.date_updated, oa.connection, oa.account_id, o.package, o.type, od.baa_accepted, 0 as has_premium
            FROM organization_account AS oa
            LEFT JOIN organization AS o
                ON oa.organization_id = o.id
            LEFT JOIN organization_details AS od
                ON od.organization_id = o.id
            WHERE oa.account_id = %d',
            $accountId
        );

        $result = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryRow();

        // Organization.package: Practice Growth | Basic | Premium | Core | Enterprise
        if (!empty($result) && trim($result['package']) == 'Premium') {
            $result['has_premium'] = 1;
        }

        return $result;
    }

    /**
     * return salesforce_id from specific provider and practice
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function getSalesforceID($providerId, $practiceId)
    {
        $sql = sprintf(
            "SELECT
            organization.salesforce_id
            FROM organization
            INNER JOIN organization_account
                ON organization_account.organization_id = organization.id
                AND organization_account.`connection` = 'O'
            INNER JOIN account_provider
                ON account_provider.account_id = organization_account.account_id
            INNER JOIN account_practice
                ON account_practice.account_id = organization_account.account_id
            WHERE account_provider.provider_id = %d
                AND account_practice.practice_id = %d
                AND organization.salesforce_id IS NOT NULL
                AND account_provider.account_id = account_practice.account_id",
            $providerId,
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryColumn();
    }

    /**
     * return Scanned organizations for the current month
     *
     * @param {optional} string $yearMonth
     *
     * @return array
     */
    public static function getScannedOrganizations($orgIds = [], $start = null)
    {
        // Assign start and end dates
        if (empty($start)) {
            $start = date("Y-m");
        }
        $start = date("Y-m-01", strtotime($start)) . ' 00:00:00';
        $end = date("Y-m-t", strtotime($start)) . ' 23:59:59';

        // Build find criteria
        $criteria = new CDbCriteria;
        $criteria->distinct = true;
        $criteria->join = 'INNER JOIN scan_request ON t.id = scan_request.organization_id';
        $criteria->order = 'organization_id ASC';
        $criteria->addColumnCondition(array('status' => "A"));
        $criteria->addBetweenCondition('scan_request.date_added', $start, $end);

        if ($orgIds) {
            $criteria->addInCondition('scan_request.organization_id', $orgIds);
        }

        // Get all active organizations with scans the for current month
        return Organization::model()->findAll($criteria);
    }

    /**
     * change Organization's status to Canceled and deactivate its OrganizationFeatures
     * @return string
     */
    public function cancel()
    {

        // mark as no pause requested
        $this->date_pause_requested = null;
        $this->save();

        $resultSet = array();

        if ($this->changeStatus("C")) {

            $flawless = true;

            // loop through OrganizationFeature and de-activate
            foreach ($this->organizationFeatures as $feature) {
                if (!$feature->deactivate()) {
                    $resultSet['featureError'] = "Error deactivating features.";
                    $flawless = false;
                }
            }

            $update = $this->updateYext();
            if (isset($update['err'])) {
                $resultSet['err'] = $resultSet['yextError'] = "Error updating Yext: " . $update['err'];
                $flawless = false;
            }

            // resolve master task
            if (!empty($this->onboarding_task_id)) {
                $vendorSystem = ''; // Default vendor
                if (substr(trim($this->onboarding_task_id), 0, 1) == 'T') {
                    // we should use phabricator
                    $vendorSystem = 'phabricator';
                    $intID = ltrim($this->onboarding_task_id, "T");
                } else {
                    $intID = $this->onboarding_task_id;
                }

                // Get devops manager vendor
                $vendor = DevOps::get()->vendor($vendorSystem);
                $ticket = DevOpsTicket::build([
                    'id' => $intID,
                    'status' => 'wontfix'
                ]);

                try {
                    $vendor->updateTicket($ticket);
                } catch (Exception $e) {
                    Yii::log(
                        'Could not resolve task ' . $intID . ': ' . $e->getMessage(),
                        'error',
                        'gadm.organization.cancel'
                    );
                }
            }

            // mark other open tasks as wontfix
            if (!empty($this->salesforce_id) && $vendor != null) {

                if (isset($vendor)) {
                    $result = $vendor->searchTickets([
                        'states' => ['New'],
                        'tags' => ['Client Services'],
                        'query' => 'sf-account-' . $this->salesforce_id
                    ]);
                }

                if (isset($result) && $result->hasData()) {
                    foreach ($result->data() as $ticket) {
                        $salesforceId = $ticket->getSalesforceId();
                        if ($salesforceId == $this->salesforce_id) {
                            $ticket->setStatus('Wontfix');
                            if (isset($vendor)) {
                                $vendor->updateTicket($ticket);
                            }
                        }
                    }
                }
            }

            // unfeature all providers in partner sites
            $ownerAccount = $this->getOwnerAccount();
            foreach ($ownerAccount->accountProviders as $ownedProvider) {
                $partnerSiteProviderFeatured = PartnerSiteProviderFeatured::model()->findAll(
                    "provider_id = :provider_id",
                    array(":provider_id" => $ownedProvider->provider_id)
                );

                foreach ($partnerSiteProviderFeatured as $item) {
                    if (self::providerHasOtherActiveOrganization($item->provider_id, $ownerAccount->id)) {
                        continue;
                    }

                    $item->delete();
                }
            }

            // make sure we stop paying for their Yelp-Capybara
            foreach ($ownerAccount->accountPractices as $ownedPractice) {
                $currentPractice = Practice::model()->findByPK($ownedPractice->practice_id);
                if ($currentPractice) {
                    $currentPractice->deactivateYelp();
                }
            }

            // set claimed status for all practices and providers related to the organization
            $this->updateClaimedValue();

            // remove nasp date in all providers and practices related to the account
            $this->removeNaspDate();

            // remove all manual validation from the owner account
            if ($ownerAccount) {
                $manualValidation = ManualValidation::model()->findAll(
                    'account_id = :account_id',
                    [':account_id' => $ownerAccount->id]
                );

                if ($manualValidation) {
                    foreach ($manualValidation as $mv) {
                        $mv->delete();
                    }
                }
            }

            if ($flawless) {
                $resultSet['success'] = "Done!";
            }
        } else {
            $resultSet['err'] = "Could not alter Organization status.";
        }

        return $resultSet;
    }

    /**
     * change Organization's status to Active and reactivate its OrganizationFeatures
     * @return boolean
     */
    public function reactivate()
    {

        // mark as no pause requested
        $this->date_pause_requested = null;
        $this->save();

        if ($this->changeStatus("A")) {

            $flawless = true;

            // loop through OrganizationFeature and re-activate
            foreach ($this->organizationFeatures as $feature) {
                if (!$feature->activate()) {
                    $flawless = false;
                }
            }

            $update = $this->updateYext();
            if (isset($update['err'])) {
                $flawless = false;
            }

            // set claimed status for all practices and providers related to the organization
            $this->updateClaimedValue();

            return $flawless;
        }
        return false;
    }

    /**
     * Change Organization's status to Paused
     * Also find all ReviewHubs that belong to this account and mark them as "Suspended"
     * @see OrganizationsCommand::actionPausePending
     * @return boolean
     */
    public function pause()
    {
        if ($this->changeStatus("P")) {
            $flawless = true;

            $account = $this->getOwnerAccount();

            if ($account) {

                // find the account's active reviewhubs
                $accountDevices = AccountDevice::model()->findAll(
                    'account_id=:accountId AND status="A"',
                    array(':accountId' => $account->id)
                );
                foreach ($accountDevices as $accountDevice) {

                    // change each reviewhub status to suspended
                    $accountDevice->status = 'S';
                    $flawless = $flawless && $accountDevice->save();

                    // save a log of this change
                    AccountDeviceHistory::log($accountDevice->id, 'A', 'S', 'A');
                }
            }
            return $flawless;
        }
        return false;
    }

    /**
     * Mark the organization as needing to be paused. once 31 days go by it's paused
     * @see OrganizationsCommand::actionPausePending
     * @return boolean
     */
    public function pauseRequest()
    {
        // check if organization is already paused
        if ($this->status == 'P' || $this->status == 'C') {
            // nothing to do
            if (!empty($this->salesforce_id)) {
                // save a note in salesforce
                $data = array(
                    'salesforce_id' => $this->salesforce_id,
                    'subject' => "Status unchanged",
                    'description' => "Organization is already paused or canceled"
                );
                SqsComponent::sendMessage('salesforceSaveAccountNote', $data);
            }
            return false;
        }

        $this->date_pause_requested = date('Y-m-d H:i:s');
        if (!$this->save()) {
            return false;
        }

        $historyEntry = new OrganizationHistory();
        $historyEntry->organization_id = $this->id;
        $historyEntry->event = 'PR';
        // magic account_id = 1:
        // default to tech@corp.doctor.com for command-line processes that don't have a user id
        if (php_sapi_name() === 'cli') {
            $historyEntry->account_id = 1;
        } else {
            $historyEntry->account_id = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
        }

        if (!$historyEntry->save()) {
            return false;
        }

        return true;
    }

    /**
     * Change Organization's status to Un-paused (active)
     * Also find all ReviewHubs that belong to this account
     * and mark them as "Active" if they were marked as "Suspended"
     *
     * @see OrganizationsCommand::actionPausePending
     * @return boolean
     */
    public function unpause()
    {
        // mark as no pause requested
        $this->date_pause_requested = null;
        $this->save();

        if ($this->changeStatus("A")) {
            $account = $this->getOwnerAccount();
            if ($account) {
                $accountDevices = AccountDevice::model()->findAll(
                    'status="S" AND account_id=:accountId',
                    array(':accountId' => $account->id)
                );
                foreach ($accountDevices as $accountDevice) {

                    // change the reviewhub status back to active
                    $accountDevice->status = 'A';
                    $accountDevice->save();

                    // save a log of this change
                    AccountDeviceHistory::log($accountDevice->id, 'S', 'A', 'A');
                }
            }
            return true;
        }

        if ($this->status != 'A') {
            return false;
        }

        $historyEntry = new OrganizationHistory();
        $historyEntry->organization_id = $this->id;
        $historyEntry->event = 'UP';
        $historyEntry->account_id = php_sapi_name() != 'cli' && !empty(Yii::app()->user->id) ? Yii::app()->user->id : 1;

        if (!$historyEntry->save()) {
            return false;
        }

        return true;
    }

    /**
     * from Modify: alter Organization's Practice's Features
     * @param array $practices
     * @param array $changes
     * @param bool $updateYext always true except for when running tests (since it will cause issues by sending dupes)
     * @return boolean
     */
    public function modifyPractices($practices, $changes, $updateYext = true)
    {
        $flawless = true;
        $yextFlawless = false;
        $returnMsg = 'done!';

        foreach ($practices as $practice) {
            // save practice unpublished status
            $practicePublished = Practice::model()->findByPK($practice['ID']);
            if ($practicePublished) {
                $practicePublished->unpublished = Common::isTrue($practice['unpublished']) ? 1 : 0;
                $practicePublished->save();
            }

            // save practice features
            foreach ($changes[$practice['ID']] as $modification => $value) {
                switch ($modification) {
                    case 'rhubs':
                        $modID = 2;
                        break;
                    case 'yext':
                        $modID = 3;
                        $yextFlawless = true;
                        break;
                    case 'reviewrequest':
                        $modID = 20;
                        break;
                    case 'google_yelp':
                        $modID = 13;
                        break;
                    case 'bing':
                        $modID = 22;
                        break;
                    case 'gmb':
                        $modID = 17;
                        break;
                    case 'mse':
                        $modID = 8;
                        break;
                    case 'showcase':
                        $modID = 9;
                        break;
                    case 'rhubsNumber':
                        $modID = 2;
                        break;
                    case 'booking_url':
                        $modID = 27;
                        break;
                    default:
                        $modID = 1;
                        break;
                }

                if ($modification == 'rhubsNumber') {
                    // changing the amount of RHubs for the practice

                    $oldNumber = $value;
                    $newNumber = $practice['features']['rhubsNumber'];

                    if ($oldNumber > $newNumber) {
                        $difference = $oldNumber - $newNumber;
                        for ($i = 0; $i < $difference; $i++) {

                            // create entry in audit log
                            AuditLog::create(
                                'D',
                                'Org. Tool',
                                false,
                                'Decrease ' . $modification . ' to ' . $newNumber
                                    . ' (#' . $modID . ') for Pract. ID #' . $practice['ID'],
                                false
                            );

                            // removal
                            if (!$this->removeFeature($modID, 0, $practice['ID'])) {
                                $returnMsg = 'Error decreasing ' . $modification . ' to ' . $newNumber
                                    . ' for Pract. ID #';
                                $flawless = false;
                            }
                        }
                    } elseif ($oldNumber < $newNumber) {
                        $difference = $newNumber - $oldNumber;
                        for ($i = 0; $i < $difference; $i++) {

                            // create entry in audit log
                            AuditLog::create(
                                'C',
                                'Org. Tool',
                                false,
                                'Increase ' . $modification . ' to ' . $newNumber
                                    . ' (#' . $modID . ') for Pract. ID #' . $practice['ID'],
                                false
                            );

                            // addition
                            if (!$this->addFeature($modID, 0, $practice['ID'], 1)) {
                                $returnMsg = 'Error increasing ' . $modification . ' to ' . $newNumber
                                    . ' for Pract. ID #' . $practice['ID'];
                                $flawless = false;
                            }
                        }
                    }
                } elseif ($modification == 'rhubs' && $value) {
                    // when removing RHubs, make sure we remove all of them

                    $rhubs = OrganizationFeature::model()->findAll(
                        'feature_id = :feature_id AND practice_id = :practice_id',
                        array(
                            ':feature_id' => $modID,
                            ':practice_id' => $practice['ID']
                        )
                    );
                    $rhubsNumber = count($rhubs);

                    for ($i = 0; $i < $rhubsNumber; $i++) {

                        // create entry in audit log
                        AuditLog::create(
                            'D',
                            'Org. Tool',
                            false,
                            'Remove ' . $modification . ' (#' . $modID . ') from Pract. ID #' . $practice['ID'],
                            false
                        );

                        if (!$this->removeFeature($modID, 0, $practice['ID'])) {
                            $returnMsg = 'Error removing ' . $modification . ' from Pract. ID #' . $practice['ID'];
                            $flawless = false;
                        }
                    }
                } else {
                    // regular feature alteration

                    if ($value) {

                        // create entry in audit log
                        AuditLog::create(
                            'D',
                            'Org. Tool',
                            false,
                            'Remove ' . $modification . ' (#' . $modID . ') from Pract. ID #' . $practice['ID'],
                            false
                        );

                        // removal
                        if (!$this->removeFeature($modID, 0, $practice['ID'])) {
                            $returnMsg = 'Error removing ' . $modification . ' from Pract. ID #' . $practice['ID'];
                            $flawless = false;
                        }
                    } else {

                        // create entry in audit log
                        AuditLog::create(
                            'C',
                            'Org. Tool',
                            false,
                            'Add ' . $modification . ' (#' . $modID . ') to Pract. ID #' . $practice['ID'],
                            false
                        );

                        // addition
                        if ($modID == 2 && $practices[$practice['ID']]['features']['rhubsNumber'] > 1) {
                            // we need to add a reviewhub but it may be more than one
                            for ($i = 1; $i <= $practices[$practice['ID']]['features']['rhubsNumber']; $i++) {
                                if (!$this->addFeature($modID, 0, $practice['ID'], 1)) {
                                    $returnMsg = 'Error adding ' . $modification . ' to Pract. ID #' . $practice['ID'];
                                    $flawless = false;
                                }
                            }
                        } else {
                            // add normally
                            if (!$this->addFeature($modID, 0, $practice['ID'], 1)) {
                                $returnMsg = 'Error adding ' . $modification . ' to Pract. ID #' . $practice['ID'];
                                $flawless = false;
                            }
                        }
                    }
                }
            }
        }

        if ($updateYext && $yextFlawless) {
            $update = $this->updateYext();
            if (isset($update['err'])) {
                $returnMsg = $update['err'];
                $flawless = false;
            }
        }

        Organization::updateSFOrganizationFeatures($this->id);

        return array(
            'status' => $flawless,
            'message' => $returnMsg,
        );
    }

    /**
     * from Modify: alter Organization's Provider's Features
     * @param array $providers
     * @param array $changes
     * @return boolean
     */
    public function modifyProviders($providers, $changes)
    {

        $flawless = true;
        // All providers
        foreach ($providers as $provider) {

            // If Enable HG PatientPursuit
            if (
                isset($changes[$provider['ID']]['enable_patientpursuit_1'])
                || isset($changes[$provider['ID']]['enable_patientpursuit_2'])
            ) {

                $value = false;
                // If isset Enable HG PatientPursuit
                if (
                    isset($changes[$provider['ID']]['enable_patientpursuit_1'])
                    && isset($changes[$provider['ID']]['enable_patientpursuit_2'])
                ) {

                    $value = false;

                    // Else if isset Enable HG PatientPursuit 1
                } elseif (isset($changes[$provider['ID']]['enable_patientpursuit_1'])) {

                    $value = $changes[$provider['ID']]['enable_patientpursuit_1'];

                    // Else if isset Enable HG PatientPursuit 2
                } elseif (isset($changes[$provider['ID']]['enable_patientpursuit_2'])) {

                    $value = $changes[$provider['ID']]['enable_patientpursuit_2'];
                }

                $changes[$provider['ID']]['enable_patientpursuit'] = $value;
                // unset Enable HG PatientPursuit
                unset($changes[$provider['ID']]['enable_patientpursuit_1']);
                unset($changes[$provider['ID']]['enable_patientpursuit_2']);
            }

            // All providers
            foreach ($changes[$provider['ID']] as $modification => $value) {
                switch ($modification) {
                    case 'featured_provider':
                        $modID = 4;
                        break;
                    case 'profilesync':
                        $modID = 10;
                        break;
                    case 'featured_healthgrades':
                        $modID = 14;
                        break;
                    case 'enhanced_healthgrades':
                        $modID = 15;
                        break;
                    case 'featured_vitals':
                        $modID = 16;
                        break;
                    case 'enable_patientpursuit':
                        $modID = 18;
                        break;
                    case 'gmb':
                        $modID = 17;
                        break;
                    case 'bing':
                        $modID = 24;
                        break;
                    case 'booking_url':
                        $modID = 26;
                        break;
                    default:
                        $modID = 1;
                        break;
                }

                if ($value) {

                    // create entry in audit log
                    AuditLog::create(
                        'D',
                        'Org. Tool',
                        false,
                        'Remove ' . $modification . ' (#' . $modID . ') from Prov. ID #' . $provider['ID'],
                        false
                    );

                    // removal
                    if ($this->removeFeature($modID, $provider['ID'], 0)) {
                        if ($modID == 14 || $modID == 15) {
                            // remove HG custom specialty if was defined for HG partner_site_id (46)
                            // but if the provider was changed from featured to enhanced, this will not apply
                            if (
                                !(isset($changes[$provider['ID']]['featured_healthgrades']) &&
                                    isset($changes[$provider['ID']]['enhanced_healthgrades']))
                            ) {
                                $customSpecialty = PartnerSiteParams::model()->find(
                                    'provider_id = :provider_id AND partner_site_id = 46 AND ' .
                                        'param_name = "provider_hg_primary_specialty"',
                                    [':provider_id' => $provider['ID']]
                                );

                                if ($customSpecialty) {
                                    $customSpecialty->delete();
                                }
                            }
                        }
                    } else {
                        $flawless = false;
                    }
                } else {

                    // create entry in audit log
                    AuditLog::create(
                        'C',
                        'Org. Tool',
                        false,
                        'Add ' . $modification . ' (#' . $modID . ') to Prov. ID #' . $provider['ID'],
                        false
                    );

                    // addition
                    if ($this->addFeature($modID, $provider['ID'], 0, 1)) {
                        if ($modID == 14 || $modID == 15) {
                            // remove HG custom specialty if was defined for HG partner_site_id (46)
                            // but if the provider was changed from featured to enhanced, this will not apply
                            if (
                                !(isset($changes[$provider['ID']]['featured_healthgrades']) &&
                                    isset($changes[$provider['ID']]['enhanced_healthgrades']))
                            ) {
                                $customSpecialty = PartnerSiteParams::model()->find(
                                    'provider_id = :provider_id AND partner_site_id = 46 AND ' .
                                        'param_name = "provider_hg_primary_specialty"',
                                    [':provider_id' => $provider['ID']]
                                );

                                if ($customSpecialty) {
                                    $customSpecialty->delete();
                                }
                            }
                        }
                    } else {
                        $flawless = false;
                    }
                }
            }
        }

        Organization::updateSFOrganizationFeatures($this->id);

        return $flawless;
    }

    /**
     * from Modify or PADM: create new Manager-level Account
     * @param array $details
     * @param array $providers
     * @param array $practices
     * @param enum(M,S) $connection
     * @param bool $isTesting
     * @return boolean
     */
    private function createSubAccount($details, $providers, $practices, $connection, $isTesting = false)
    {
        $flawless = true;

        $ownerAccount = $this->getOwnerAccount();
        if (!$ownerAccount) {
            return false;
        }

        $newAccount = new Account();
        $newAccount->account_type_id = AccountType::FREE_ACCOUNT;
        $newAccount->enabled = 1; // must be integer
        $newAccount->partner_site_id = 1;
        // required fields
        $newAccount->email = $details['email'];
        $newAccount->first_name = $details['first_name'];
        $newAccount->last_name = $details['last_name'];
        $newAccount->organization_name = $this->organization_name;
        if (!empty($details['phone'])) {
            $newAccount->phone_number = $details['phone'];
        }
        $newAccount->location_id = $ownerAccount->location_id;
        $newAccount->date_signed_in = '0000-00-00 00:00:00';

        // "Administrative Staff"
        $newAccount->position_id = 4;

        // create randomly generated password
        $newAccount->assignRandomPassword();

        // do not send this Account as a lead to Salesforce
        $newAccount->setScenario("noLeadRequired");

        if ($newAccount->save()) {

            if (!$isTesting) {
                // Send welcome mail and password create mail
                MailComponent::accountSignupConfirmation($newAccount->id);

                // generate hash
                $encoded_link = MailComponent::createEncodedLink($newAccount);

                // notify ourselves that an account was created via email
                $mailFrom = array(
                    'From' => "notifications@corp.doctor.com",
                    'From_Name' => "Doctor.com Account Notification"
                );
                $mailTo = 'notifications@corp.doctor.com';
                $mailSubject = 'Doctor.com - Account Signup Notification';
                $mailAltBody = "A new account has been created on doctor.com for " . $newAccount->first_name . " "
                    . $newAccount->last_name . " (" . $newAccount->email . ") on " . $newAccount->date_added . " "
                    . " To create your new password, please visit this link: $encoded_link ";

                $mailBody = nl2br($mailAltBody);
                MailComponent::sendEmail(
                    $mailFrom,
                    $mailTo,
                    false,
                    false,
                    $mailSubject,
                    $mailBody,
                    $mailAltBody,
                    false,
                    false,
                    $newAccount->email
                );
            }

            // Associate practices to account
            if (!empty($practices)) {
                foreach ($practices as $practice) {
                    $accountPractice = new AccountPractice();
                    $accountPractice->practice_id = $practice;
                    $accountPractice->account_id = $newAccount->id;
                    $accountPractice->all_providers = $details['all'];
                    if (!$accountPractice->save()) {
                        $flawless = false;
                    }
                }
            }

            // Associate providers to account
            if (!empty($providers)) {
                foreach ($providers as $provider) {
                    $accountProvider = new AccountProvider();
                    $accountProvider->provider_id = $provider;
                    $accountProvider->account_id = $newAccount->id;
                    $accountProvider->all_practices = $details['all'];
                    if (!$accountProvider->save()) {
                        $flawless = false;
                    }
                }
            }

            // Create privileges to access staffer account
            if ($connection == 'S') {
                if (!empty($details['privileges'])) {
                    $accountPrivileges = new AccountPrivileges();
                    $accountPrivileges->account_id = $newAccount->id;
                    $accountPrivileges->reviews = $details['privileges']['reviews'];
                    $accountPrivileges->appointment_request = $details['privileges']['appointment_request'];
                    $accountPrivileges->tracked_calls = $details['privileges']['tracked_calls'];
                    $accountPrivileges->products_services = $details['privileges']['products_services'];
                    $accountPrivileges->directory = !empty($details['privileges']['directory'])
                        ? $details['privileges']['directory']
                        : array();
                    if (!$accountPrivileges->save()) {
                        $flawless = false;
                    }
                }
            }

            // Save Organization Account
            $organizationAccount = new OrganizationAccount();
            $organizationAccount->account_id = $newAccount->id;
            $organizationAccount->organization_id = $this->id;
            $organizationAccount->connection = $connection;

            if (!$organizationAccount->save()) {
                $flawless = false;
            }
        } else {
            $flawless = false;
        }

        return $flawless;
    }

    /**
     * from Modify or PADM: create new Manager-level Account
     * @param array $details
     * @return boolean
     */
    public function createManagerAccount($details, $isTesting = false)
    {

        $ownerAccount = $this->getOwnerAccount();

        $connectionType = (!empty($details['connection_type'])) ? $details['connection_type'] : 'M';

        if ($ownerAccount) {
            $practices = array();
            foreach ($ownerAccount->accountPractices as $ownerPractice) {
                $practices[] = $ownerPractice->practice_id;
            }
            $providers = array();
            foreach ($ownerAccount->accountProviders as $ownerProvider) {
                $providers[] = $ownerProvider->provider_id;
            }
            $flawless = $this->createSubAccount($details, $providers, $practices, $connectionType, $isTesting);
        } else {
            $flawless = false;
        }
        return $flawless;
    }

    /**
     * from PADM: create new Staffer-level Account
     * @param array $details
     * @param bool $isTesting
     * @return boolean
     */
    public function createStafferAccount($details, $isTesting = false)
    {
        return $this->createSubAccount($details, $details["providers"], $details["practices"], "S", $isTesting);
    }

    /**
     * Construct an Organization along with its full O-level Account, and OrganizationFeatures
     *
     * - create Account with random password
     * - edit or create Providers
     * - edit or create Practices
     * - attach Providers and Practices to Account
     * - attach Practices to Providers
     *
     * - create OrganizationFeatures and activate() them
     * - create Phabricator task tree list
     *
     * @param array $accountDetails
     * @param array $organizationDetails
     * @param array $submittedPractices
     * @param array $submittedProviders
     * @return \Organization
     */
    public static function construct($accountDetails, $organizationDetails, $submittedPractices, $submittedProviders)
    {
        ini_set('max_execution_time', 3600);
        $devOpsVendor = DevOps::get()->vendor();

        // fallback data array for Error Tasks
        $received = array();
        $received['organization'] = $organizationDetails;
        $received['account'] = $accountDetails;
        $received['providers'] = $submittedProviders;
        $received['practices'] = $submittedPractices;

        // UPSELL CHECK - we could be coming from the Upsell Tool!
        $upsell = false;
        if (!empty($organizationDetails['id']) && !empty($organizationDetails['exists'])) {
            $upsell = true;
        }

        // ORGANIZATION CREATION/RETRIEVAL
        if (!$upsell) {
            $newOrganization = new Organization;
            $newOrganization->organization_name = $organizationDetails['name'];
            $newOrganization->package = $organizationDetails['package'];

            // If partner site is set
            if (isset($organizationDetails['partner_site_id'])) {
                $newOrganization->partner_site_id = $organizationDetails['partner_site_id'];
            }

            if ($organizationDetails['salesforce_id']) {
                $newOrganization->salesforce_id = $organizationDetails['salesforce_id'];
            }

            if (!$newOrganization->save()) {
                $devOpsVendor->forgeClosingToolErrorTask(
                    print_r($received, true),
                    null,
                    "**Organization was not created!**\n" . print_r($newOrganization->getErrors(), true)
                );
            }
        } else {
            $newOrganization = Organization::model()->findByPK($organizationDetails['id']);

            if (!$newOrganization) {
                $devOpsVendor->forgeClosingToolErrorTask(
                    print_r($received, true),
                    null,
                    "**Organization was not found!**\n" . print_r($newOrganization->getErrors(), true)
                );
            }
        }

        // ORGANIZATION DETAILS
        if (isset($organizationDetails['client_support_tier'])) {

            $orgD = OrganizationDetails::model()->find(
                'organization_id=:organization_id',
                array(':organization_id' => $newOrganization->id)
            );
            if (!$orgD) {
                // not found, create it
                $orgD = new OrganizationDetails;
                $orgD->organization_id = $newOrganization->id;
                $orgD->client_support_tier = $organizationDetails['client_support_tier'];
                $orgD->date_added = date("Y-m-d H:i:s");
                $orgD->save();
            } else {
                // found, modify it
                $orgD->client_support_tier = $organizationDetails['client_support_tier'];
                $orgD->save();
            }
        }

        // ACCOUNT CREATION/RETRIEVAL
        if (!$upsell) {

            $zipcodeCheck = false;

            // if there's no zipcode it's because we have to create the location
            if (
                empty($organizationDetails['zip']) && !empty($accountDetails['billing_city'])
                && !empty($accountDetails['billing_state']) && !empty($accountDetails['billing_country'])
                && !empty($accountDetails['practice_zip']) && !empty($accountDetails['latitude'])
                && !empty($accountDetails['longitude'])
                && !empty($accountDetails['first_name'])
                && !empty($accountDetails['last_name'])
            ) {

                // maybe they asked us to create it but it already exists, make it seem they entered it correctly
                $zipcodeCheck = Location::model()->find(
                    'zipcode=:zipcode',
                    array(':zipcode' => $accountDetails['practice_zip'])
                );

                if (!$zipcodeCheck) {
                    // it didn't exist, create it

                    // look up state
                    $state = State::model()->find(
                        'name=:name AND country_id=:countryId',
                        array(
                            ':name' => $accountDetails['billing_state'],
                            ':countryId' => $accountDetails['billing_country']
                        )
                    );
                    if (!$state) {
                        // not found, create it
                        $state = new State;
                        $state->name = $accountDetails['billing_state'];
                        $state->friendly_url = State::getFriendlyUrl($accountDetails['billing_state']);
                        $state->country_id = $accountDetails['billing_country'];
                        $state->is_pending_approval = 1; // needs to be approved
                        $state->save();
                    }

                    // look up city
                    $city = City::model()->find(
                        'name=:name AND state_id=:stateId',
                        array(':name' => $accountDetails['billing_city'], ':stateId' => $state->id)
                    );
                    if (!$city) {
                        // not found, create it
                        $city = new City;
                        $city->name = $accountDetails['billing_city'];
                        $city->friendly_url = City::getFriendlyUrl($accountDetails['billing_city']);
                        $city->state_id = $state->id;
                        $city->is_pending_approval = 1; // needs to be approved
                        $city->save();
                    }

                    // we already established the zipcode didn't exist, create it directly
                    $location = new Location;
                    $location->zipcode = $accountDetails['practice_zip'];
                    $location->city_id = $city->id;
                    $location->city_name = $accountDetails['billing_city'];
                    $location->latitude = $accountDetails['latitude'];
                    $location->longitude = $accountDetails['longitude'];
                    $location->is_pending_approval = 1; // needs to be approved
                    $location->save();

                    // make it seem they entered an already-existing zipcode
                    $zipcodeCheck = $location;
                }
            }

            if (!$zipcodeCheck) {
                // normal zipcode check (it should already exist)
                $zipcodeCheck = Location::model()->find(
                    'zipcode=:zipcode',
                    array(':zipcode' => $organizationDetails['zip'])
                );
            }

            // ensure zipcode is valid or return that error along with others
            if (!$zipcodeCheck) {
                $newAccount = new Account();
                $password = $newAccount->assignRandomPassword();

                $newAccount->first_name = $accountDetails['first_name'];
                $newAccount->last_name = $accountDetails['last_name'];
                $newAccount->id = 0;
                $devOpsVendor->forgeClosingToolErrorTask(
                    print_r($received, true),
                    $newAccount,
                    "**CRITICAL ACCOUNT CREATION ERROR**\ninvalid zipcode?!"
                );
            }

            // still no zipcode? assign unknown
            $location_id = (!empty($zipcodeCheck->id) ? $zipcodeCheck->id : 999999); // 999999 = Unknown

            if (!empty($accountDetails['id'])) {
                // Doctor Account ID already associated with SF Account
                $newAccount = Account::model()->findByPK($accountDetails['id']);
                if (empty($newAccount)) {
                    // Does an Account already exist that is associated with the email?
                    $newAccount = Account::model()->find('email=:email', array(':email' => $accountDetails['email']));
                    if (empty($newAccount)) {
                        $devOpsVendor->forgeClosingToolErrorTask(
                            print_r($received, true),
                            null,
                            "**CRITICAL ACCOUNT CREATION ERROR**\nAccount_Id does not exist?!"
                        );
                    } else {
                        $exists = OrganizationAccount::model()->exists(
                            'account_id = :account_id AND `connection` = :connection',
                            array(':account_id' => $newAccount->id, ':connection' => "O")
                        );
                        if ($exists) {
                            $devOpsVendor->forgeClosingToolErrorTask(
                                print_r($received, true),
                                $newAccount,
                                "**CRITICAL ACCOUNT CREATION ERROR**\nAccount is linked to another organization?!"
                            );
                        }
                    }
                } else {

                    $exists = OrganizationAccount::model()->exists(
                        'account_id = :account_id AND connection = :connection',
                        array(':account_id' => $accountDetails['id'], ':connection' => "O")
                    );
                    if ($exists) {
                        $devOpsVendor->forgeClosingToolErrorTask(
                            print_r($received, true),
                            $newAccount,
                            "**CRITICAL ACCOUNT CREATION ERROR**\nAccount is related with another organization?!"
                        );
                    }
                }
            }

            // Does an Account already exist that is associated with the email?
            if (empty($newAccount)) {
                $newAccount = Account::model()->find('email=:email', array(':email' => $accountDetails['email']));
            }
            if (empty($newAccount)) {
                $newAccount = new Account();
                // create randomly generated password
                $password = $newAccount->assignRandomPassword();
            }

            // If partner site is not defined
            if (!isset($accountDetails['partner_site_id'])) {
                $accountDetails['partner_site_id'] = 1;
            }
            $newAccount->account_type_id = AccountType::FREE_ACCOUNT;
            $newAccount->enabled = 1; // must be integer
            $newAccount->partner_site_id = $accountDetails['partner_site_id'];
            if (isset($accountDetails['is_beta_account'])) {
                $newAccount->is_beta_account = $accountDetails['is_beta_account'];
            }
            // required fields
            $newAccount->email = $accountDetails['email'];
            $newAccount->first_name = $accountDetails['first_name'];
            $newAccount->last_name = $accountDetails['last_name'];
            $newAccount->organization_name = $organizationDetails['name'];
            $newAccount->phone_number = $accountDetails['phone'];
            $newAccount->location_id = $location_id ? $location_id : 999999; // 999999 = Unknown
            // optional fields
            $newAccount->position_id = 5;
            $newAccount->address = $organizationDetails['address'];
            if (!empty($organizationDetails['address_2'])) {
                $newAccount->address_2 = $organizationDetails['address_2'];
            }

            // do not send this Account as a lead to Salesforce
            $newAccount->setScenario("noLeadRequired");

            $newAccount->date_signed_in = '0000-00-00 00:00:00';

            if (!$newAccount->save()) {
                $devOpsVendor->forgeClosingToolErrorTask(
                    print_r($received, true),
                    $newAccount,
                    "**Account was not created!**\n" . print_r($newAccount->getErrors(), true)
                );
            }
            $newAccount = Account::model()->find('email=:email', array(':email' => $newAccount->email));

            // ORGANIZATIONACCOUNT CREATION
            $organizationAccount = new OrganizationAccount();
            $organizationAccount->account_id = $newAccount->id;
            $organizationAccount->organization_id = $newOrganization->id;
            $organizationAccount->connection = "O";

            if (!$organizationAccount->save()) {
                $devOpsVendor->forgeClosingToolErrorTask(
                    print_r($received, true),
                    $newAccount,
                    "**OrganizationAccount was not created!**\n" . print_r($organizationAccount->getErrors(), true)
                );
            }
        } else {
            $newAccount = $newOrganization->getOwnerAccount();

            if (!$newAccount) {
                $devOpsVendor->forgeClosingToolErrorTask(
                    print_r($received, true),
                    $newAccount,
                    "**Owner Account not found!**\n" . print_r($newAccount->getErrors(), true)
                );
            }
        }

        // Phabricator integration elements
        $taskBody = "";

        // MSE && SHOW CASE PAGE && TELEMEDICINE
        if (!$upsell) {
            if ($organizationDetails['has_mse'] || $organizationDetails['has_showcase']) {
                $orgFeatureShowCase = new OrganizationFeature();
                $orgFeatureShowCase->organization_id = $newOrganization->id;
                $orgFeatureShowCase->feature_id = $organizationDetails['has_mse'] ? 8 : 9;
                $orgFeatureShowCase->provider_id = null;
                $orgFeatureShowCase->practice_id = null;
                $orgFeatureShowCase->active = 1;
                if (!$orgFeatureShowCase->save()) {

                    $errorMsgCreation = $organizationDetails['has_mse'] ? "**MSE " : "**Show Case Page ";
                    $errorMsgCreation .= " was not created!**\n";

                    $devOpsVendor->forgeClosingToolErrorTask(
                        print_r($received, true),
                        $orgFeatureShowCase,
                        $errorMsgCreation . print_r($orgFeatureShowCase->getErrors(), true)
                    );
                } else {

                    $activation = $orgFeatureShowCase->activate();

                    if ($activation) {
                        $historyEntry = new OrganizationHistory;
                        $historyEntry->organization_id = $newOrganization->id;
                        $historyEntry->event = "N";
                        $historyEntry->feature_id = $organizationDetails['has_mse'] ? 8 : 9;
                        // magic account_id = 1:
                        // default to tech@corp.doctor.com for command-line processes that don't have a user id
                        if (php_sapi_name() != 'cli') {
                            $historyEntry->account_id = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
                        } else {
                            $historyEntry->account_id = 1;
                        }

                        if (!$historyEntry->save()) {
                            $devOpsVendor->forgeClosingToolErrorTask(
                                print_r($received, true),
                                $historyEntry,
                                "**Organization History not found!**\n" . print_r($historyEntry->getErrors(), true)
                            );
                        }
                        if ($organizationDetails['has_mse']) {
                            $taskBody = $taskBody . " - " . $activation . " - **MOBILE SITEENHANCE**\n";
                        } else {
                            $taskBody = $taskBody . " - " . $activation . " - **SHOWCASEPAGE**\n";
                        }
                    }
                }
            }

            if ($organizationDetails['has_telemedicine']) {
                $orgFeatureTelemedicine = new OrganizationFeature();
                $orgFeatureTelemedicine->organization_id = $newOrganization->id;
                $orgFeatureTelemedicine->feature_id = 23;
                $orgFeatureTelemedicine->provider_id = null;
                $orgFeatureTelemedicine->practice_id = null;
                $orgFeatureTelemedicine->active = 1;
                if (!$orgFeatureTelemedicine->save()) {

                    $errorMsgCreation = "**Telemedicine was not created!**\n";

                    $devOpsVendor->forgeClosingToolErrorTask(
                        print_r($received, true),
                        $orgFeatureTelemedicine,
                        $errorMsgCreation . print_r($orgFeatureTelemedicine->getErrors(), true)
                    );
                } else {

                    $activation = $orgFeatureTelemedicine->activate();

                    if ($activation) {
                        $historyEntry = new OrganizationHistory;
                        $historyEntry->organization_id = $newOrganization->id;
                        $historyEntry->event = "N";
                        $historyEntry->feature_id = 23;
                        // magic account_id = 1:
                        // default to tech@corp.doctor.com for command-line processes that don't have a user id
                        if (php_sapi_name() != 'cli') {
                            $historyEntry->account_id = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
                        } else {
                            $historyEntry->account_id = 1;
                        }

                        if (!$historyEntry->save()) {
                            $devOpsVendor->forgeClosingToolErrorTask(
                                print_r($received, true),
                                $historyEntry,
                                "**Organization History not found!**\n" . print_r($historyEntry->getErrors(), true)
                            );
                        }
                        $taskBody = $taskBody . " - " . $activation . " - **TELEMEDICINE**\n";
                    }
                }
            }
        }

        // PRACTICES

        // this array holds newly-created practices and their newly-assigned IDs
        // the IDs are required for provider-practice relations later
        $newlyCreatedPractices = array();
        $practiceFeatures = array();
        foreach ($submittedPractices as $practice) {
            if (isset($practice['create'])) {
                $currentPractice = new Practice;
                $currentPractice->score = 0;
            } else {
                $currentPractice = Practice::model()->findByPK($practice['ID']);
            }

            $currentPractice->name = $practice['name'];
            $currentPractice->address = $practice['address_1'];
            if (!empty($practice['address_2'])) {
                $currentPractice->address_2 = $practice['address_2'];
            }
            $currentPractice->phone_number = $practice['phone'];
            if (!empty($practice['fax'])) {
                $currentPractice->fax_number = $practice['fax'];
            }
            $currentPractice->email = $practice['email'];

            $zipcodeCheck = Location::model()->find('zipcode=:zipcode', array(':zipcode' => $practice['zip']));
            $location_id = (!empty($zipcodeCheck->id) ? $zipcodeCheck->id : 999999); // 999999 = Unknown
            $currentPractice->location_id = $location_id;
            if (!empty($practice['unpublished'])) {
                $currentPractice->unpublished = $practice['unpublished'] ? 1 : 0;
            }
            if ($currentPractice->save()) {
                // practice_details
                $practiceDetailsExists = PracticeDetails::model()->exists(
                    'practice_id=:practiceId',
                    array(':practiceId' => $currentPractice->id)
                );
                if (!$practiceDetailsExists) {
                    $practiceDetails = new PracticeDetails();
                    $practiceDetails->practice_id = $currentPractice->id;
                    // update practiceDetails
                    $practiceDetails->emr_id = 0;
                    $practiceDetails->practice_size = 0;
                    $practiceDetails->practice_type_id = 1;
                    $practiceDetails->coverage_state_id = null;
                    $practiceDetails->coverage_location_id = null;
                    $practiceDetails->emr_id = 1;
                    $practiceDetails->coverage_radius = null;
                    $practiceDetails->yext_special_offer = null;
                    $practiceDetails->review_request_email_title = null;
                    if (!$practiceDetails->save()) {
                        $devOpsVendor->forgeClosingToolErrorTask(
                            print_r($received, true),
                            $newAccount,
                            "Error saving practice-details:" . print_r($practiceDetails->getErrors(), true)
                        );
                    }
                }

                // Associate account-practice to the owner and to all managers
                AccountPractice::associateAccountPractice($newAccount->id, $currentPractice->id);

                if (isset($practice['create'])) {
                    $newPractice = array();
                    $newPractice['ID'] = $currentPractice->id;
                    $newPractice['address'] = $practice['address_1'];
                    $newPractice['email'] = $practice['email'];
                    $newlyCreatedPractices[] = $newPractice;
                }

                // PRACTICE FEATURES
                if (!empty($practice['features'])) {
                    foreach ($practice['features'] as $feature => $value) {
                        $taskName = false;
                        $featureValue = Common::isTrue($value) ? 1 : 0;
                        switch ($feature) {
                            case 'rhubs':
                                $featID = 2;
                                $taskName = "REVIEWHUB";
                                $featureValue = $practice['features']['rhubsNumber'];
                                break;
                            case 'yext':
                                $featID = 3;
                                break;
                            case 'mse':
                                $featID = 8;
                                $taskName = "MOBILE SITEENHANCE";
                                break;
                            case 'showcase':
                                $featID = 9;
                                $taskName = "SHOWCASEPAGE";
                                break;
                            case 'uberall':
                                $featID = 12;
                                break;
                            case 'google_yelp':
                                $featID = 13;
                                $featureValue = Common::isTrue($value) ? 1 : 0;
                                break;
                            case 'bing':
                                $featID = 22;
                                $featureValue = Common::isTrue($value) ? 1 : 0;
                                break;
                            case 'gmb':
                                $featID = 17;
                                $featureValue = Common::isTrue($value) ? 1 : 0;
                                break;
                            case 'reviewrequest':
                                $featID = 20;
                                break;
                            case 'rhubsNumber':
                                $featID = 0;
                                break;
                            default:
                                $featID = 1;
                                break;
                        }
                        if ($featID < 2) {
                            continue;
                        }

                        // This features must be activated later
                        $practiceFeatures[$currentPractice->id][$featID] = array(
                            'name' => $feature,
                            'value' => $featureValue
                        );
                    }
                }
            } else {
                $devOpsVendor->forgeClosingToolErrorTask(
                    print_r($received, true),
                    $newAccount,
                    "Error saving practice: " . print_r($currentPractice->getErrors(), true)
                );
            }
        }

        // PROVIDERS

        // prepared for Salesforce
        $profileSyncProviders = array();
        $featuredProviders = array();

        foreach ($submittedProviders as $provider) {
            if (isset($provider['create'])) {
                $currentProvider = new Provider;

                // when creating a new provider, we're using very basic logic to determine their credentials
                // we default to 1 (MD) since CS research will correct it anyway

                switch ($provider['specialty_id']) {
                    case 23:
                        $currentProvider->credential_id = 3;
                        break;
                    case 83:
                        $currentProvider->credential_id = 9;
                        break;
                    case 94:
                        $currentProvider->credential_id = 10;
                        break;
                    case 72:
                        $currentProvider->credential_id = 12;
                        break;
                    case 87:
                        $currentProvider->credential_id = 13;
                        break;
                    case 14:
                        $currentProvider->credential_id = 14;
                        break;
                    default:
                        $currentProvider->credential_id = 1;
                        break;
                }
            } else {
                $currentProvider = Provider::model()->findByPK($provider['ID']);
            }

            $currentProvider->first_name = $provider['first_name'];
            if (!empty($provider['middle_name'])) {
                $currentProvider->middle_name = $provider['middle_name'];
            } else {
                $currentProvider->middle_name = null;
            }
            $currentProvider->last_name = $provider['last_name'];

            $currentProvider->gender = $provider['gender'];

            if (!empty($provider['npi'])) {
                $currentProvider->npi_id = $provider['npi'];
            }

            $currentProvider->claimed = 1;
            $providerSaved = $currentProvider->save();
            if (!$providerSaved) {
                // if we couldn't save it, maybe it's because the NPI is a duplicate. clear it and try again.
                $providerErrors = $currentProvider->getErrors();
                if (!empty($providerErrors['npi_id'])) {
                    $currentProvider->npi_id = null;
                    $providerSaved = $currentProvider->save();
                }
            }

            if ($providerSaved) {

                $providerDetails = ProviderDetails::model()->find(
                    'provider_id = :providerId',
                    array(':providerId' => $currentProvider->id)
                );
                if ($providerDetails == null) {
                    $providerDetails = new ProviderDetails;
                    $providerDetails->provider_id = $currentProvider->id;
                }
                // automatically turn on online appointment requests
                $providerDetails->schedule_mail_enabled = 1;
                $providerDetails->save();

                // ACCOUNT-PROVIDER LINK
                // Associate Owner & Managers to the new provider
                AccountProvider::associateAccountProvider($newAccount->id, $currentProvider->id);

                // PROVIDER FEATURES
                if (!empty($provider['features'])) {

                    foreach ($provider['features'] as $feature => $value) {

                        if ($value === false) {
                            // if we check and then uncheck something we don't want it
                            continue;
                        }

                        $taskName = false;
                        switch ($feature) {
                            case 'featured_provider':
                                $featID = 4;
                                $featuredProviders[] = $provider['first_name'] . " " . $provider['last_name'];
                                break;
                            case 'profilesync':
                                $featID = 10;
                                $profileSyncProviders[] = $provider['first_name'] . " " . $provider['last_name'];
                                $taskName = "CLAIM HEALTH LISTINGS";
                                break;
                            case 'featured_healthgrades':
                                $featID = 14;
                                break;
                            case 'enable_patientpursuit_1':
                                $featID = 18;
                                break;
                            case 'enhanced_healthgrades':
                                $featID = 15;
                                break;
                            case 'enable_patientpursuit_2':
                                $featID = 18;
                                break;
                            case 'featured_vitals':
                                $featID = 16;
                                break;
                            default:
                                $featID = 1;
                                break;
                        }
                        if ($featID < 2) {
                            continue;
                        }

                        // addition
                        $activationResult = $newOrganization->addFeature($featID, $currentProvider->id, 0, 1);
                        if (!$activationResult) {
                            $devOpsVendor->forgeClosingToolErrorTask(
                                print_r($received, true),
                                $newAccount,
                                "Error saving feature: " . $feature . " for " . $currentPractice->id
                            );
                        }
                    }
                }

                // PROVIDER SPECIALTIES
                $specialties = ProviderSpecialty::model()->findAll(
                    'provider_id = :provider_id',
                    array(':provider_id' => $currentProvider->id)
                );

                $primarySpecialtyCorrect = false;
                $secondarySpecialtyCorrect = false;

                if (!empty($specialties)) {

                    foreach ($specialties as $specialty) {
                        if ($specialty->sub_specialty_id == $provider['sub_specialty_id']) {
                            $primarySpecialtyCorrect = true;
                        } elseif (
                            !empty($provider['secondary_sub_specialty_id'])
                            && $specialty->sub_specialty_id == $provider['secondary_sub_specialty_id']
                        ) {
                            $secondarySpecialtyCorrect = true;
                        } else {
                            try {
                                if (!$specialty->delete()) {
                                    $devOpsVendor->forgeClosingToolErrorTask(
                                        print_r($received, true),
                                        $newAccount,
                                        "Error deleting incorrect provider specialty: " . print_r(
                                            $specialty->getErrors(),
                                            true
                                        )
                                    );
                                    $primarySpecialtyCorrect = true;
                                    $secondarySpecialtyCorrect = true;
                                }
                            } catch (CDbException $e) {
                                Yii::log(
                                    "Error deleting incorrect provider specialty: " . get_class($e) . ' - '
                                        . $e->getMessage(),
                                    'error',
                                    'system.models.organization'
                                );
                            }
                        }
                    }
                }

                if (!$primarySpecialtyCorrect) {
                    $newPrimarySpecialty = new ProviderSpecialty;
                    $newPrimarySpecialty->provider_id = $currentProvider->id;
                    $newPrimarySpecialty->sub_specialty_id = $provider['sub_specialty_id'];

                    if (!$newPrimarySpecialty->save()) {
                        $devOpsVendor->forgeClosingToolErrorTask(
                            print_r($received, true),
                            $newAccount,
                            "Error saving provider specialty: " . print_r($newPrimarySpecialty->getErrors(), true)
                        );
                    }
                }
                if (!empty($provider['secondary_sub_specialty_id']) && !$secondarySpecialtyCorrect) {
                    $newSecondarySpecialty = new ProviderSpecialty;
                    $newSecondarySpecialty->provider_id = $currentProvider->id;
                    $newSecondarySpecialty->sub_specialty_id = $provider['secondary_sub_specialty_id'];

                    if (!$newSecondarySpecialty->save()) {
                        $devOpsVendor->forgeClosingToolErrorTask(
                            print_r($received, true),
                            $newAccount,
                            "Error saving provider specialty: " . print_r($newSecondarySpecialty->getErrors(), true)
                        );
                    }
                }
            } else {
                $devOpsVendor->forgeClosingToolErrorTask(
                    print_r($received, true),
                    $newAccount,
                    "Error saving provider: " . print_r($currentProvider->getErrors(), true)
                );
            }

            // PROVIDER PRACTICE

            $intPractices = array();

            foreach ($provider['practices'] as $currentPractice) {
                $providerPractice = ProviderPractice::model()->find(
                    'provider_id = :provider_id AND practice_id = :practice_id',
                    array(
                        ':provider_id' => $currentProvider->id,
                        ':practice_id' => $currentPractice['ID']
                    )
                );
                if (empty($providerPractice)) {
                    // new practices don't have IDs in this data set, we need to find it
                    $providerPracticePracticeId = $currentPractice['ID'];
                    if (isset($currentPractice['create'])) {
                        foreach ($newlyCreatedPractices as $newPractice) {
                            if (($currentPractice['address_1'] == $newPractice['address'])
                                && ($currentPractice['email'] == $newPractice['email'])
                            ) {
                                $providerPracticePracticeId = $newPractice['ID'];
                            }
                        }
                    }
                    // insert new association
                    $providerPractice = ProviderPractice::model()->find(
                        'provider_id = :provider_id AND practice_id = :practice_id',
                        array(
                            ':provider_id' => $currentProvider->id,
                            ':practice_id' => $providerPracticePracticeId
                        )
                    );
                    if (empty($providerPractice)) {
                        $providerPractice = new ProviderPractice();
                        $providerPractice->provider_id = $currentProvider->id;
                        $providerPractice->practice_id = $providerPracticePracticeId;
                    }
                }

                if (isset($provider['primary_practice'])) {
                    if ($provider['primary_practice'] == $providerPractice->practice_id) {
                        $providerPractice->primary_location = 1;
                    } else {
                        $providerPractice->primary_location = 0;
                    }
                } else {
                    // if primary_practice is not set, we know there is only one pract for this provider
                    $providerPractice->primary_location = 1;
                }

                // make sure the practices we don't touch are not marked primary
                if ($providerPractice->primary_location == 1) {
                    $providerPractices = ProviderPractice::model()->findAll(
                        'practice_id != :practice_id AND provider_id = :provider_id',
                        array(
                            ':practice_id' => $providerPractice->practice_id,
                            ':provider_id' => $currentProvider->id
                        )
                    );
                    if ($providerPractices) {
                        foreach ($providerPractices as $foundRelation) {
                            $foundRelation->primary_location = 0;
                            try {
                                if (!$foundRelation->save()) {
                                    $devOpsVendor->forgeClosingToolErrorTask(
                                        print_r($received, true),
                                        $newAccount,
                                        "Error cleaning other practices for provider: " .
                                            print_r($foundRelation->getErrors(), true)
                                    );
                                }
                            } catch (CDbException $e) {
                                Yii::log(
                                    "Error cleaning other practices for provider: " . get_class($e) . ' - ' .
                                        $e->getMessage(),
                                    'error',
                                    'system.models.organization'
                                );
                            }
                        }
                    }
                }

                // ensure int values using coercion
                $intPractices[] = 0 + $providerPractice->practice_id;

                if (!$providerPractice->save()) {
                    $devOpsVendor->forgeClosingToolErrorTask(
                        print_r($received, true),
                        $newAccount,
                        "Error saving provider-practice relation: " . print_r($providerPractice->getErrors(), true)
                    );
                }

                // add new provider to the staff manager if manage the location that doctor is associated
                if ($upsell) {
                    $organizationAccount = OrganizationAccount::model()->findAll(
                        "organization_id = :organization_id AND `connection` = 'S'",
                        array(':organization_id' => $newOrganization->id)
                    );
                    if (!empty($organizationAccount)) {
                        foreach ($organizationAccount as $account) {

                            $existStaffAccountPractice = AccountPractice::model()->exists(
                                'account_id = :accountId AND practice_id = :practiceId',
                                array(
                                    ':accountId' => $account->account_id,
                                    ':practiceId' => $providerPractice->practice_id
                                )
                            );

                            if ($existStaffAccountPractice) {
                                $existStaffAccountProvider = AccountProvider::model()->exists(
                                    'account_id = :accountId AND provider_id = :providerId',
                                    array(
                                        ':accountId' => $account->account_id,
                                        ':providerId' => $providerPractice->provider_id
                                    )
                                );

                                if (!$existStaffAccountProvider) {
                                    $accountProviderStaff = new AccountProvider;
                                    $accountProviderStaff->account_id = $account->account_id;
                                    $accountProviderStaff->provider_id = $providerPractice->provider_id;
                                    if (!$accountProviderStaff->save()) {
                                        $devOpsVendor->forgeClosingToolErrorTask(
                                            print_r($received, true),
                                            $account,
                                            "Error saving staff account-provider relation: "
                                                . print_r($accountProviderStaff->getErrors(), true)
                                        );
                                    }
                                }
                            }
                        }
                    }
                }
            }

            // delete unwanted ProviderPractices
            if (!empty($intPractices)) {
                $providerPractices = ProviderPractice::model()->findAll(
                    'provider_id = :provider_id',
                    array(':provider_id' => $currentProvider->id)
                );
                if ($providerPractices) {
                    foreach ($providerPractices as $foundRelation) {
                        if (!in_array($foundRelation->practice_id, $intPractices)) {
                            if (
                                $foundRelation  && self::unwantedProviderPractices(
                                    $newOrganization->id,
                                    $newAccount->id,
                                    $foundRelation->practice_id
                                )
                            ) {
                                try {
                                    if (!$foundRelation->delete()) {
                                        $devOpsVendor->forgeClosingToolErrorTask(
                                            print_r($received, true),
                                            $newAccount,
                                            "Error deleting defunct provider-practice relation: " .
                                                print_r($foundRelation->getErrors(), true)
                                        );
                                    }
                                } catch (CDbException $e) {
                                    Yii::log(
                                        "Error deleting defunct provider-practice relation: " . get_class($e)
                                            . ' - ' . $e->getMessage(),
                                        'error',
                                        'system.models.organization'
                                    );
                                }
                            }
                        }
                    }
                }
            }
        }
//var_dump($practiceFeatures);die();
        // ADD/UPDATE PRACTICE FEATURES
        if (!empty($practiceFeatures)) {

            foreach ($practiceFeatures as $practiceId => $features) {

                foreach ($features as $featureId => $feature) {
                    if ($featureId == 2 && $feature['value'] > 1) {
                        // ReviewHubs
                        for ($i = 0; $i < $feature['value']; $i++) {
                            $activationResult = $newOrganization->addFeature($featureId, 0, $practiceId, 1);

                            if (!$activationResult) {
                                $devOpsVendor->forgeClosingToolErrorTask(
                                    print_r($received, true),
                                    $newAccount,
                                    "Error saving feature: " . $feature['name'] . " for " . $practiceId
                                );
                            } else {
                                $currentNumber = $i + 1;
                                $taskBody = $taskBody . " - " . $activationResult . " - **REVIEWHUB " .
                                    $currentNumber . "** - " . $practiceId . "\n";
                            }
                        }
                    } elseif (Common::isTrue($feature['value'])) {

                        $activationResult = $newOrganization->addFeature($featureId, 0, $practiceId, 1);
                        if (!$activationResult) {
                            $devOpsVendor->forgeClosingToolErrorTask(
                                print_r($received, true),
                                $newAccount,
                                "Error saving feature: " . $feature['name'] . " for " . $practiceId
                            );
                        } elseif ($taskName) {
                            $taskBody = $taskBody . " - " . $activationResult . " - **" . $taskName . "** - " .
                                $practiceId . "\n";
                        }
                    }
                }
            }
        }


        if (!empty($newOrganization->salesforce_id)) {

            $sfData = array();

            if (!$upsell) {
                $sfData["Doctor_com_Org_Id__c"] = $newOrganization->id;
                $sfData["Doctor_com_Account_Id__c"] = $newAccount->id;
                $sfData["Closing_Tool_Completed__c"] = TimeComponent::createSalesforceDatetime();
            }

            $sfData["ProfileSync_Provider_Names__c"] = count($profileSyncProviders) > 10
                ? '10+ providers (see Doctor.com admin for details)'
                : join("\n", $profileSyncProviders);

            $sfData["Promoted_Provider_Names__c"] = count($featuredProviders) > 10
                ? '10+ providers (see Doctor.com admin for details)'
                : join("\n", $featuredProviders);

            $saveSF = Yii::app()->salesforce->updateAccount($newOrganization->salesforce_id, $sfData);

            Organization::updateSFOrganizationFeatures($newOrganization->id);

            if (isset($saveSF['error'])) {
                $devOpsVendor->forgeClosingToolErrorTask(
                    print_r($received, true),
                    $newAccount,
                    "ERROR SUBMITTING TO SALESFORCE: " . print_r($saveSF['error'], true)
                );
            }
        }

        return $newOrganization;
    }

    /**
     * change an Organization's onboarding status and record this in the organization_history table
     *
     * @param $mode string/enum {"N","I","C"}
     * @return boolean
     */
    public function changeOnboarding($mode)
    {
        $this->onboarding = $mode;
        if ($this->save()) {
            if ($mode == "C") {
                // if we're completing onboarding, record this in OrganizationHistory
                $historyEntry = new OrganizationHistory;
                $historyEntry->organization_id = $this->id;
                $historyEntry->event = "B";
                // magic account_id = 1:
                // default to tech@corp.doctor.com for command-line processes that don't have a user id
                if (php_sapi_name() != 'cli') {
                    $historyEntry->account_id = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
                } else {
                    $historyEntry->account_id = 1;
                }
                return $historyEntry->save();
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    /**
     * Check if the provider has other active organization
     * @param int $providerId Provider id
     * @param int $organizationId Organization id
     * @return boolean
     */
    public static function providerHasOtherActiveOrganization($providerId, $organizationId)
    {
        $sql = sprintf(
            "SELECT COUNT(1)
            FROM account_provider ap
            INNER JOIN account a
                ON a.id = ap.account_id
            INNER JOIN organization_account oa
                ON oa.account_id = a.id
            INNER JOIN organization o
                ON o.id = oa.organization_id
            WHERE ap.provider_id = %d
                AND oa.account_id != %d
                AND o.status = 'A'
                AND oa.`connection` = 'O';",
            $providerId,
            $organizationId
        );
        $cnt = Yii::app()->db->createCommand($sql)->queryScalar();
        return $cnt > 0;
    }

    //////////////////////
    //       YEXT       //
    //////////////////////

    /**
     * the Yext customer IDs we use are Organization ID + offset to not overlap existing IDs
     * @return int
     */
    public function yextID()
    {
        if (!empty($this->yext_customer_id)) {
            return $this->yext_customer_id;
        } else {
            $this->yext_customer_id = $this->id + 10000;
            $this->save();
            return $this->yext_customer_id;
        }
    }

    /**
     * check whether a Yext customer was created or if is possible created
     */
    public function checkStatusYext()
    {
        $resultSet = array();

        $yextPracticeCount = 0;
        foreach ($this->organizationFeatures as $feature) {
            if (!empty($feature->practice_id) && $feature->feature_id == 3 && $feature->active) {
                $yextPracticeCount++;
            }
        }

        if ($yextPracticeCount < 1) {
            $resultSet['success'] = "**This Organization does not require Yext.**";
            return $resultSet;
        }

        if ($this->yext_customer_id) {
            $resultSet['success'] = "**Location data successfully updated.**";
            return $resultSet;
        }

        $resultSet['success'] = '';
        return $resultSet;
    }

    /**
     * Get Onboard Queue
     * @param int $page
     * @param int $approval
     * @return array
     */
    public static function sqlOnboardQueue($page, $approval)
    {

        $onboardQueueStr = "";
        if ($approval == 0) {
            $onboardQueueStr = "
                AND (
                    account_practice.research_status IN ('Pending', 'Rejected')
                    OR
                    account_provider.research_status IN ('Pending', 'Rejected')
                )
            ";
        }

        $sql = sprintf(
            "SELECT DISTINCT organization.id AS orgID, organization.organization_name,
            organization.onboarding, organization.onboarding_task_id,
            organization.salesforce_id, UNIX_TIMESTAMP(organization.date_added) AS closeDate,
            account.first_name, account.last_name, account.id AS accID,
            account.email, account.research_mode,
            account.research_date_initialization AS date_sent_to_profile_population
            FROM account
            INNER JOIN organization_account
                ON account.id = organization_account.account_id
            INNER JOIN organization
                ON organization_account.organization_id = organization.id
            INNER JOIN account_practice
                ON account.id = account_practice.account_id
            INNER JOIN account_provider
                ON account.id = account_provider.account_id
            WHERE organization_account.connection = 'O'
                AND account.research_mode <> 0
                %s
            ORDER BY organization.date_added ASC
            LIMIT 200;",
            $onboardQueueStr
        );

        $data = Yii::app()->db->createCommand($sql)->queryAll();

        usort($data, function ($a, $b) {
            return strcmp($a["closeDate"], $b["closeDate"]);
        });

        return $data;
    }

    /**
     * (mostly Onboarding)
     * check whether a Yext customer was created
     * if not, create!
     * @return array
     */
    public function checkYext()
    {
        $dataSet = array('customer_id' => $this->yextID());

        $resultSet = array();

        $yextPracticeCount = 0;

        foreach ($this->organizationFeatures as $feature) {
            if (!empty($feature->practice_id) && $feature->feature_id == 3 && $feature->active) {
                $yextPracticeCount++;
            }
        }

        if ($yextPracticeCount < 1) {
            $this->yext_customer_id = null;
            $this->save();
            $resultSet['success'] = "This Organization does not require Yext.";
            return $resultSet;
        }

        try {
            YextComponent::getCustomer($dataSet);
            $resultSet['success'] = "Yext has them!";
        } catch (HTTPFutureHTTPResponseStatus $e) {

            if (strpos($e, "Customer not found") === true) {

                $ownerAccount = $this->getOwnerAccount();

                $yextLocations = array();
                $locationIds = array();

                foreach ($this->organizationFeatures as $feature) {
                    if (!empty($feature->practice_id) && $feature->feature_id == 3) {
                        $currentPractice = Practice::model()->findByPK($feature->practice_id);
                        if (empty($currentPractice)) {
                            // practice doesn't exist anymore
                            return false;
                        }
                        $yextLocations[] = $currentPractice->yextConversion();
                        $locationIds[] = (string) $feature->practice_id;
                    }
                }

                $dataSet = [
                    'id' => $this->yextID(),
                    'businessName' => $this->organization_name,
                    'contactFirstName' => $ownerAccount->first_name,
                    'contactLastName' => $ownerAccount->last_name,
                    'contactPhone' => $ownerAccount->phone_number,
                    'contactEmail' => $ownerAccount->email,
                    'locations' => $yextLocations,
                    'subscriptions' => array(
                        'offerId' => Yii::app()->params['YEXT_MONTHLY_STANDARD_OFFER_ID'],
                        'locationIds' => $locationIds
                    )
                ];

                try {
                    YextComponent::postCustomer($dataSet);
                    $resultSet['success'] = "We have pushed them to Yext!";

                    if (!empty($this->salesforce_id)) {
                        $data = array(
                            'salesforce_id' => $this->salesforce_id,
                            'subject' => "Linked to Doctor.com",
                            'description' =>
                            "This account was not previously connected to Doctor.com. This has been remedied."
                        );
                        SqsComponent::sendMessage('salesforceSaveAccountNote', $data);
                    }
                } catch (HTTPFutureHTTPResponseStatus $f) {
                    if (strpos($f, "There is already a pending order for a business")) {
                        $resultSet['success'] = "Yext is currently processing them!";
                    } else {
                        $this->yext_customer_id = null;
                        $this->save();
                        $resultSet['err'] = "secondary request hit a " . $f->getStatusCode() . ", error was " .
                            $f->getMessage();
                    }
                }
            } else {
                $this->yext_customer_id = null;
                $this->save();
                $resultSet['err'] = "primary request hit a " . $e->getStatusCode() . ", error was " . $e->getMessage();
            }
        }
        return $resultSet;
    }

    /**
     * run an intelligent Yext update sequence
     * cancels subscription if no practices have the Yext feature attached
     * @return success/err associative array
     */
    public function updateYext()
    {
        if (empty($this->yext_customer_id)) {
            $resultSet['success'] = "This Org never had Yext.";
            return $resultSet;
        }

        $locationIds = array();

        $resultSet = array();

        $yextPracticeCount = 0;

        $dataSet = array('customer_id' => $this->yextID());

        $customerLocationIds = array();

        try {
            $result = YextComponent::getLocations($dataSet);
            $customerLocations = $result['locations'];

            foreach ($customerLocations as $custLoc) {
                $customerLocationIds[] = $custLoc['id'];
            }
        } catch (HTTPFutureHTTPResponseStatus $e) {
            $errorCode = $e->getStatusCode();
            if ($errorCode == 400) {
                // this person never had Yext in the first place
                // we need to check to make sure, as OrgFeatures might have been deleted
                $resultSet['success'] = "This Org never had Yext.";
            } else {
                $resultSet['err'] = "Locations retrieval hit an error " . $errorCode . ", error was " .
                    $e->getMessage();
                if ($errorCode == 500) {
                    $resultSet['err'] .= ' - please try again later.';
                }
            }
            return $resultSet;
        }

        try {
            $result = YextComponent::getSubscription($dataSet);

            // assume that highest-index subscription is current
            foreach ($result['subscriptions'] as $sub) {
                $subscriptionLocationIds = $sub['locationIds'];
                $subscriptionId = $sub['id'];
                $subscriptionStatus = $sub['status'];
            }
        } catch (HTTPFutureHTTPResponseStatus $e) {
            $errorCode = $e->getStatusCode();
            $resultSet['err'] = "Subscription retrieval hit an error " . $errorCode . ", error was " .
                $e->getMessage();
            if ($errorCode == 500) {
                $resultSet['err'] .= ' - please try again later.';
            }
            return $resultSet;
        }

        foreach ($this->organizationFeatures as $feature) {
            if (!empty($feature->practice_id) && $feature->feature_id == 3 && $feature->active) {
                $currentPractice = Practice::model()->findByPK($feature->practice_id);
                if (empty($currentPractice)) {
                    // practice doesn't exist anymore, continue
                    continue;
                }
                $yextLocation = $currentPractice->yextConversion();
                $yextLocation['customerId'] = $this->yextID();

                // safeguard
                if (empty($yextLocation['specialOffer'])) {
                    $yextLocation['specialOffer'] = 'Call Now to Schedule!';
                }

                if (in_array($yextLocation['id'], $customerLocationIds)) {
                    // Location already exists - update it
                    try {
                        $result = YextComponent::putLocation($yextLocation);
                    } catch (HTTPFutureHTTPResponseStatus $e) {
                        $resultSet['err'] = "location update hit a " . $e->getStatusCode() . ", error was " .
                            $e->getMessage();
                        return $resultSet;
                    }
                } else {
                    // create the Location
                    try {
                        $result = YextComponent::postLocation($yextLocation);
                    } catch (HTTPFutureHTTPResponseStatus $e) {
                        $resultSet['err'] = "location creation hit a " . $e->getStatusCode() . ", error was " .
                            $e->getMessage();
                        return $resultSet;
                    }
                }

                // silently synchronize all data to yext
                ob_start();
                Practice::yextSync(
                    array(
                        'practice_id' => $currentPractice->id,
                        'yext_customer_id' => $yextLocation['customerId'],
                        'yext_location_id' => $currentPractice->yext_location_id
                    )
                );
                ob_end_clean();

                $yextPracticeCount++;

                $locationIds[] = $feature->practice_id;
            }
        }

        if (!isset($resultSet['err'])) {
            if ($yextPracticeCount > 0) {
                // check if there is an active subscription
                if (!empty($subscriptionStatus) && $subscriptionStatus == "ACTIVE") {
                    // do we need to update the locations for the sub?

                    $needToUpdate = false;

                    foreach ($locationIds as $currLoc) {
                        if (!in_array($currLoc, $subscriptionLocationIds)) {
                            $needToUpdate = true;
                        }
                    }
                    foreach ($subscriptionLocationIds as $currLoc) {
                        if (!in_array($currLoc, $locationIds)) {
                            $needToUpdate = true;
                        }
                    }

                    if ($needToUpdate) {
                        $dataSet['subscription_id'] = $subscriptionId;
                        $dataSet['locationIds'] = $locationIds;

                        foreach ($locationIds as $locationIdValue) {
                            $dataSet['locationIds'] = $locationIdValue;
                            try {
                                $result = YextComponent::putSubscription($dataSet);
                            } catch (HTTPFutureHTTPResponseStatus $e) {
                                $resultSet['err'] = "subscription update hit a " . $e->getStatusCode()
                                    . ": error was " . $e->getMessage() . " for " . serialize($dataSet);
                                return $resultSet;
                            }
                        }
                    }

                    $resultSet['success'] = "Location data updated!";
                } else {
                    // create new subscription

                    $dataSet['locationIds'] = $locationIds;
                    $dataSet['offerId'] = Yii::app()->params['YEXT_MONTHLY_STANDARD_OFFER_ID'];

                    try {
                        $result = YextComponent::postSubscription($dataSet);
                    } catch (HTTPFutureHTTPResponseStatus $e) {
                        $resultSet['err'] = "Subscription creation hit a " . $e->getStatusCode()
                            . ", error was " . $e->getMessage();
                        return $resultSet;
                    }

                    $resultSet['success'] = "New subscription has been created.";
                }
            } else {
                //cancel subscription

                if (!empty($subscriptionStatus) && $subscriptionStatus == "ACTIVE") {
                    $dataSet['subscription_id'] = $subscriptionId;
                    $dataSet['status'] = "CANCELED";

                    try {
                        $result = YextComponent::cancelSubscription($dataSet);
                    } catch (HTTPFutureHTTPResponseStatus $e) {
                        $resultSet['err'] = "subscription cancelation hit a " . $e->getStatusCode()
                            . ", error was " . $e->getMessage();
                        return $resultSet;
                    }

                    $resultSet['success'] = "Subscription has been canceled.";
                } else {
                    $resultSet['success'] = "Nothing has changed.";
                }
            }
        }

        return $resultSet;
    }

    /**
     * Delete cancelled organization
     * @param int $id
     */
    public static function deleteCancelled($id)
    {

        ETLComponent::startTransaction();

        /**
         * Delete History
         */
        $organizationH = OrganizationHistory::model()->findAll(
            'organization_id = :organization_id',
            array(':organization_id' => $id)
        );
        foreach ($organizationH as $organization) {
            $organization->delete();
        }

        /**
         * Update Practice
         */
        $sql = sprintf(
            "SELECT DISTINCT practice_id
            FROM organization_feature
            WHERE feature_id=3
            AND organization_id = %d
            AND practice_id NOT IN (
                SELECT practice_id
                FROM organization_feature
                WHERE feature_id=3
                AND organization_id <> %d
            ); ",
            $id,
            $id
        );
        $practices = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($practices as $practice) {

            $prc = Practice::model()->findByPK($practice['practice_id']);
            $prc->yext_location_id = null;
            $prc->update();
        }

        /**
         * Delete Featured
         */
        $organizationF = OrganizationFeature::model()->findALL(
            'organization_id = :organization_id',
            array(':organization_id' => $id)
        );
        foreach ($organizationF as $organization) {
            $organization->delete();
        }

        /**
         * Delete Account
         */
        $organizationA = OrganizationAccount::model()->findALL(
            'organization_id = :organization_id',
            array(':organization_id' => $id)
        );
        foreach ($organizationA as $organization) {
            $organization->delete();
        }

        /**
         * Delete Organization
         */
        $organization = Organization::model()->findByPK($id);
        if (!$organization->delete()) {
            return false;
        } else {
            ETLComponent::commitTransaction();
        }

        return true;
    }

    /**
     * Update organization-related data points in SalesForce:
     * - Number_of_Locations__c
     * - Number_of_Providers__c
     * - Number_of_ReviewHubs__c
     * - Number_of_Review_Requests__c
     * - Number_of_ProfilePromotes__c (this is the number of featured providers)
     * - Number_of_SiteEnhance_Mobiles__c (# that click "Mobile only" radio button)
     * - Number_of_Showcase_Pages__c (# that click "ShowcasePage" radio button)
     * - Number_of_ProfileSyncs__c (number of providers with the profilesync feature -id 10-)
     * - Number_of_ProfilePromotes_Healthgrades__c
     * - Number_of_Enhanced_Healthgrades__c
     * - Number_of_ProfilePromotes_Vitals__c
     * - Promoted_Healthgrades_Provider_Names__c
     * - Enhanced_Healthgrades_Provider_Names__c
     * - Promoted_Vitals_Provider_Names__c
     * - VirtualVisit_Enabled__c
     *
     * If the organization is cancelled, it counts both the active and inactive features
     * Otherwise (if active or paused) it only counts the active features,
     * @param int $organizationId
     * @return boolean
     */
    public static function updateSFOrganizationFeatures($organizationId)
    {

        $organization = Organization::model()->findByPk($organizationId);
        if (!$organization) {
            return false;
        }

        $ownerAccount = $organization->getOwnerAccount();
        if (!$ownerAccount) {
            return false;
        }

        // count locations
        $sql = sprintf(
            'SELECT COUNT(account_id)
            FROM account_practice
            INNER JOIN practice on practice.id = account_practice.practice_id
            INNER JOIN practice_details on practice_details.practice_id = practice.id
            WHERE account_id = %d
            AND practice_type_id != 5;',
            $ownerAccount->id
        );
        $accountPractices = (int) Yii::app()->db->createCommand($sql)->queryScalar();

        // count providers
        $accountProviders = AccountProvider::model()->count(
            'account_id=:accountId',
            array(':accountId' => $ownerAccount->id)
        );

        // count reviewhubs
        if ($organization->status == 'C') {
            // for cancelled organizations, count both active and inactive features
            $accountDevices = OrganizationFeature::model()->count(
                'organization_id=:organizationId AND feature_id = 2',
                array(':organizationId' => $organizationId)
            );
        } else {
            // only count active features
            $accountDevices = OrganizationFeature::model()->count(
                'organization_id=:organizationId AND feature_id = 2 AND active = 1',
                array(':organizationId' => $organizationId)
            );
        }

        // count reviewRequest
        if ($organization->status == 'C') {
            // for cancelled organizations, count both active and inactive features
            $accountRR = OrganizationFeature::model()->count(
                'organization_id=:organizationId AND feature_id = 20',
                array(':organizationId' => $organizationId)
            );
        } else {
            // only count active features
            $accountRR = OrganizationFeature::model()->count(
                'organization_id=:organizationId AND feature_id = 20 AND active = 1',
                array(':organizationId' => $organizationId)
            );
        }

        // count featured providers
        if ($organization->status == 'C') {
            // for cancelled organizations, count both active and inactive features
            $profilePromotes = OrganizationFeature::model()->count(
                array(
                    'condition' => 'organization_id=:organizationId AND feature_id IN (4, 14, 16)',
                    'select' => 'COUNT(DISTINCT(provider_id))',
                    'params' => array(":organizationId" => $organizationId)
                )
            );
        } else {
            // only count active features
            $profilePromotes = OrganizationFeature::model()->count(
                array(
                    'condition' => 'organization_id=:organizationId AND feature_id IN (4, 14, 16) AND active = 1',
                    'select' => 'COUNT(DISTINCT(provider_id))',
                    'params' => array(":organizationId" => $organizationId)
                )
            );
        }

        // count siteenhance mobile
        if ($organization->status == 'C') {
            // for cancelled organizations, count both active and inactive features
            $siteenhanceMobiles = OrganizationFeature::model()->count(
                'organization_id=:organizationId AND feature_id = 8',
                array(':organizationId' => $organizationId)
            );
        } else {
            // only count active features
            $siteenhanceMobiles = OrganizationFeature::model()->count(
                'organization_id=:organizationId AND feature_id = 8 AND active = 1',
                array(':organizationId' => $organizationId)
            );
        }

        // count showcase pages
        if ($organization->status == 'C') {
            // for cancelled organizations, count both active and inactive features
            $showcasePages = OrganizationFeature::model()->count(
                'organization_id=:organizationId AND feature_id = 9',
                array(':organizationId' => $organizationId)
            );
        } else {
            // only count active features
            $showcasePages = OrganizationFeature::model()->count(
                'organization_id=:organizationId AND feature_id = 9 AND active = 1',
                array(':organizationId' => $organizationId)
            );
        }

        // count profilesyncs
        if ($organization->status == 'C') {
            // for cancelled organizations, count both active and inactive features
            $profileSyncs = OrganizationFeature::model()->count(
                'organization_id=:organizationId AND feature_id = 10',
                array(':organizationId' => $organizationId)
            );
        } else {
            // only count active features
            $profileSyncs = OrganizationFeature::model()->count(
                'organization_id=:organizationId AND feature_id = 10 AND active = 1',
                array(':organizationId' => $organizationId)
            );
        }

        // find featured providers
        $featuredProviders = array();
        $arrProfilePromotes = OrganizationFeature::model()->findAll(
            'organization_id=:organizationId AND feature_id IN (4, 14, 16) AND active = 1',
            array(':organizationId' => $organizationId)
        );
        foreach ($arrProfilePromotes as $arrProfilePromote) {
            $featuredProviders[] = $arrProfilePromote->provider->first_name . ' ' .
                $arrProfilePromote->provider->last_name;
        }
        if (!empty($featuredProviders)) {
            // remove duplicates
            $featuredProviders = array_values(array_unique($featuredProviders));
        }

        // find profilesyncs
        $profileSyncProviders = array();
        $arrProfileSyncs = OrganizationFeature::model()->findAll(
            'organization_id=:organizationId AND feature_id = 10 AND active = 1',
            array(':organizationId' => $organizationId)
        );
        foreach ($arrProfileSyncs as $arrProfileSync) {
            $profileSyncProviders[] = $arrProfileSync->provider->first_name . ' ' .
                $arrProfileSync->provider->last_name;
        }
        if (!empty($profileSyncProviders)) {
            // remove duplicates
            $profileSyncProviders = array_values(array_unique($profileSyncProviders));
        }

        // find providers featured on healthgrades
        $featuredHealthgrades = array();
        $arrFeaturedHealthgrades = OrganizationFeature::model()->findAll(
            'organization_id=:organizationId AND feature_id = 14 AND active = 1',
            array(':organizationId' => $organizationId)
        );
        foreach ($arrFeaturedHealthgrades as $featuredProvider) {
            $featuredHealthgrades[] = $featuredProvider->provider->first_name . ' ' .
                $featuredProvider->provider->last_name;
        }
        if (!empty($featuredHealthgrades)) {
            // remove duplicates
            $featuredHealthgrades = array_values(array_unique($featuredHealthgrades));
        }

        // find providers enhanced on healthgrades
        $enhancedHealthgrades = array();
        $arrEnhancedHealthgrades = OrganizationFeature::model()->findAll(
            'organization_id=:organizationId AND feature_id = 15 AND active = 1',
            array(':organizationId' => $organizationId)
        );
        foreach ($arrEnhancedHealthgrades as $enhanced) {
            $enhancedHealthgrades[] = $enhanced->provider->first_name . ' ' .
                $enhanced->provider->last_name;
        }
        if (!empty($enhancedHealthgrades)) {
            // remove duplicates
            $enhancedHealthgrades = array_values(array_unique($enhancedHealthgrades));
        }

        // find providers featured on vitals
        $featuredVitals = array();
        $arrFeaturedVitals = OrganizationFeature::model()->findAll(
            'organization_id=:organizationId AND feature_id = 16 AND active = 1',
            array(':organizationId' => $organizationId)
        );
        foreach ($arrFeaturedVitals as $featuredProvider) {
            $featuredVitals[] = $featuredProvider->provider->first_name . ' ' .
                $featuredProvider->provider->last_name;
        }
        if (!empty($featuredVitals)) {
            // remove duplicates
            $featuredVitals = array_values(array_unique($featuredVitals));
        }

        // build array to send to sf
        $sfData = array();

        // counts
        $sfData['Number_of_Locations__c'] = $accountPractices;
        $sfData['Number_of_Providers__c'] = $accountProviders;
        $sfData['Number_of_ReviewHubs__c'] = $accountDevices; /* feature 2 */
        $sfData['Number_of_Review_Requests__c'] = $accountRR; /* feature 20 */
        $sfData['Number_of_SiteEnhance_Mobiles__c'] = $siteenhanceMobiles; /* feature 8 */
        $sfData['Number_of_Showcase_Pages__c'] = $showcasePages; /* feature 9 */

        $sfData['Number_of_ProfilePromotes__c'] = $profilePromotes; /* feature 4, 14, 16 */
        $sfData['Number_of_ProfileSyncs__c'] = $profileSyncs; /* feature 10 */

        $sfData['Number_of_ProfilePromotes_Healthgrades__c'] = sizeof($featuredHealthgrades); /* feature 14 */
        $sfData['Number_of_Enhanced_Healthgrades__c'] = sizeof($enhancedHealthgrades); /* feature 15 */
        $sfData['Number_of_ProfilePromotes_Vitals__c'] = sizeof($featuredVitals); /* feature 16 */

        // names
        $sfData["Promoted_Provider_Names__c"] = count($featuredProviders) > 10 ?
            '10+ providers (see Doctor.com admin for details)' : join("\n", $featuredProviders); /* feature 4 */
        $sfData["ProfileSync_Provider_Names__c"] = count($profileSyncProviders) > 10 ?
            '10+ providers (see Doctor.com admin for details)' : join("\n", $profileSyncProviders); /* feature 10 */

        $sfData["Promoted_Healthgrades_Provider_Names__c"] = count($featuredHealthgrades) > 10 ?
            '10+ providers (see Doctor.com admin for details)' : join("\n", $featuredHealthgrades); /* feature 14 */
        $sfData["Enhanced_Healthgrades_Provider_Names__c"] = count($enhancedHealthgrades) > 10 ?
            '10+ providers (see Doctor.com admin for details)' : join("\n", $enhancedHealthgrades); /* feature 15 */
        $sfData["Promoted_Vitals_Provider_Names__c"] = count($featuredVitals) > 10 ?
            '10+ providers (see Doctor.com admin for details)' : join("\n", $featuredVitals); /* feature 16 */

        return Yii::app()->salesforce->updateAccount($organization->salesforce_id, $sfData);
    }

    /**
     * get paused organization
     * @param $dayOfWeek 0->all, 1->Mon, 2->Tue,.... 7->Sun
     * @return arr
     */
    public function getPausedOrganizations($dayOfWeek = 0)
    {
        $sql = sprintf(
            "SELECT o.id, o.organization_name , o.date_updated, oa.account_id,
            MAX(oh.date_added) AS last_event_P, (WEEKDAY(MAX(oh.date_added)) + 1) AS day_of_week
            FROM organization AS o
            INNER JOIN organization_account AS oa
                ON oa.organization_id = o.id
            INNER JOIN organization_history AS oh
                ON oh.organization_id = o.id
            WHERE  oa.connection = 'O'
                AND  o.`status`  = 'P'
                AND oh.event='P'
            GROUP BY o.id;"
        );
        $arrResults = Yii::app()->db->createCommand($sql)->queryAll();

        if (empty($arrResults)) {
            return '';
        }

        if ($dayOfWeek == 0) {
            // return all data
            $pausedOrganization = $arrResults;
        } else {
            // return only if day_of_week = $dayOfWeek
            $pausedOrganization = array();
            foreach ($arrResults as $result) {
                if ($result['day_of_week'] == $dayOfWeek) {
                    $pausedOrganization[] = $result;
                }
            }
        }
        return $pausedOrganization;
    }

    /**
     * When an account is updated from PADM,
     * create or update tasks in Phab so that Vitals and Healthgrades are manually updated
     * @param int $organizationId
     * @param string $message
     * @return mixed
     */
    public static function createPhabricatorUpdateTasks($organizationId, $message)
    {
        $taskId = $ticketDescription = false;
        $message = date('h:ia') . ': ' . $message;

        // look up the organization
        $organization = Organization::model()->findByPk($organizationId);
        if (empty($organization)) {
            return false;
        }

        // try to find existing task
        if (empty($organization->salesforce_id)) {
            return false;
        }

        // look up organization in salesforce
        $sfAccount = Yii::app()->salesforce->getObject("Account", $organization->salesforce_id);

        if (empty($sfAccount)) {
            // account wasn't found, leave
            return false;
        }

        if (empty($sfAccount['Onboard_Held_Date__c']) && empty($sfAccount['Onboard_Skipped_Date__c'])) {
            // onboard not held, leave
            return false;
        }

        // take onboarding skipped date or onboarding held date to simulate Onboard_Held_or_Skipped__c
        if (empty($sfAccount['Onboard_Skipped_Date__c'])) {
            $heldOrSkippedDate = $sfAccount['Onboard_Held_Date__c'];
        } else {
            $heldOrSkippedDate = $sfAccount['Onboard_Skipped_Date__c'];
        }

        if (!(time() - strtotime($heldOrSkippedDate) > 28 * 86400)) {
            // only process entries when it's been more than 4 weeks past onboarding held date
            return false;
        }

        // create task title
        $taskTitle = 'UPDATE VITALS & HEALTHGRADES - ' . $organization->organization_name . ' - ' . date('m/d/Y');

        // // look up cs project
        // $arrProjects = PhabricatorComponent::returnProjects();
        // $csProject = $arrProjects['Client Services'];
        // // search for matching tasks
        // $arrTasks = PhabricatorComponent::searchTasks(null, array('fullText' => 'sf-account-' .
        //     $organization->salesforce_id, 'status' => 'status-open', 'projectPHIDs' => array($csProject)));

        // Get devops manager vendor
        $vendor = DevOps::get()->vendor('phabricator');

        $result = $vendor->searchTickets([
            'states' => ['New'],
            'tags' => ['Client Services'],
            'query' => 'sf-account-' . $organization->salesforce_id
        ]);

        if ($result->hasData()) {
            foreach ($result->data() as $ticket) {
                $salesforceId = $ticket->getSalesforceId();
                $ticketTitle = $ticket->getTitle();
                if ($salesforceId == $organization->salesforce_id && $ticketTitle == $taskTitle) {
                    $ticketDescription = $ticket->getBody();
                    $taskId = $ticket->getId();
                    break (1);
                }
            }
            if (!empty($taskId)) {
                // update existing task
                $newDescription = $ticketDescription . "\r\n\r\n" . $message;
                $ticket->setBody($newDescription);
                return $vendor->updateTicket($ticket);
            } else {

                // create new task
                $ticket = DevOpsTicket::build([
                    'title' => $taskTitle,
                    'body' => $message,
                    'body_format' => 'markdown',
                    'salesforce_id' => $organization->salesforce_id,
                    'assignedTo' => 'raquelg',
                    'extra_data' => ["auxiliary" => ["type" => 'LISTING_UPDATE']]
                ]);
                return $vendor->createTicket($ticket);
            }
        }
    }

    /**
     * Build the 45-day report, sent to organizations 45 days after their onboarding held date or onboard skipped date
     * This is as long as the org has 1 or more 4-5 star reviews
     *
     * @param Organization $organization
     * @param string $accountRep
     * @param array $reviews
     * @param array $goodReviews
     * @return string
     */
    public static function get45DaysReport($organization, $accountRep, $reviews, $goodReviews)
    {

        $body = '<style>td{width:auto !important;}</style>';
        $body .= '<br />';
        $body .= 'Hello,<br />';
        $body .= '<br />';
        $body .= 'Great reviews are coming your way! It\'s still early on, but I want to keep you posted on your
            progress using the ReviewHub&trade;.';
        $body .= '<br />';
        $body .= '<br />';

        // start table
        $body .= '<table align="center" style="background: #fff; -moz-border-radius: 5px; -webkit-border-radius: 5px;
            border-radius: 5px; border: 1px solid #b7c4c8 !important;" valign="top" cellpadding="0" cellspacing="0"
            width="100%">';
        $body .= '<tr>';
        $body .= '<td>';

        // headings
        $body .= '<table cellspacing="0" width="100%" cellpadding="10">';
        $body .= '<tr style="background-color: #e4eaed; vertical-align:middle;">';
        $body .= '<td style="padding-left:20px;" width="25%">';
        $body .= '<strong style="font-size:18px;">ReviewHub&trade; Update</strong>';
        $body .= '<br />';
        $body .= '<small style="color:gray">Review Highlights</small>';
        $body .= '</td>';
        $body .= '<td width="25%">';
        $body .= '<strong style="font-size:12px">TOTAL REVIEWS COLLECTED</strong>';

        // total reviewhub reviews
        $body .= '<br />' . sizeof($reviews);
        $body .= '</td>';
        $body .= '<td width="25%">';
        $body .= '<strong style="font-size:12px">';
        $body .= 'AVERAGE RATING';
        $body .= '</strong>';
        $body .= '<br />';

        // reviewhub average for whole account
        $reviewAverage = 0;
        foreach ($reviews as $review) {
            $reviewAverage += $review->rating;
        }
        $reviewAverage = $reviewAverage / sizeof($reviews);
        $body .= AssetsComponent::showStars($reviewAverage);
        $body .= '</td>';
        $body .= '<td width="25%">';
        $body .= '</td>';
        $body .= '</tr>';

        // first review
        $body .= '<tr style="border-top: 1px solid #b7c4c8 !important;">';
        $body .= '<td style="padding-left:20px;">';
        $body .= '<strong style="font-size:12px">RATING</strong>';
        $body .= '<br />';
        $body .= AssetsComponent::showStars($goodReviews[0]->rating);
        $body .= '<br />';
        $body .= '<br />';
        $body .= '<strong style="font-size:12px">AUTHOR</strong>';
        $body .= '<br />';
        $body .= StringComponent::anonymizeReviewerName(
            $goodReviews[0]->reviewer_name,
            $goodReviews[0]->reviewer_name_anon
        );
        $body .= '</td>';
        $body .= '<td colspan="3">';
        $body .= '<strong style="font-size:18px; color:#469497">';
        $body .= $goodReviews[0]->title;
        $body .= '</strong>';
        $body .= '<br />';
        $body .= '<hr style="border: 1px solid #e4eaed !important;" />';
        $body .= '<br />';
        $body .= $goodReviews[0]->description;
        $body .= '</td>';
        $body .= '</tr>';

        // second review (if we have it)
        if (!empty($goodReviews[1])) {
            $body .= '<tr>';
            $body .= '<td style="padding-left:20px;">';
            $body .= '<strong style="font-size:12px">RATING</strong>';
            $body .= '<br />';
            $body .= AssetsComponent::showStars($goodReviews[1]->rating);
            $body .= '<br />';
            $body .= '<br />';
            $body .= '<strong style="font-size:12px">AUTHOR</strong>';
            $body .= '<br />';
            $body .= StringComponent::anonymizeReviewerName(
                $goodReviews[1]->reviewer_name,
                $goodReviews[1]->reviewer_name_anon
            );
            $body .= '</td>';
            $body .= '<td colspan="3">';
            $body .= '<strong style="font-size:18px; color:#469497">';
            $body .= $goodReviews[1]->title;
            $body .= '</strong>';
            $body .= '<br />';
            $body .= '<hr style="border: 1px solid #e4eaed !important;" />';
            $body .= '<br />';
            $body .= $goodReviews[1]->description;
            $body .= '</td>';
            $body .= '</tr>';
        }

        // end second review, start footer
        $body .= '<tr>';
        $body .= '<td colspan="4" style="border-top: 1px solid #b7c4c8 !important; text-align:center;" align="center">';
        $body .= '<br />';
        $body .= 'To read all your reviews, simply log in to your Doctor.com portal <a
            href="https://providers.doctor.com">here</a>.';
        $body .= '<br />';
        $body .= '&nbsp;';
        $body .= '<a href="https://www.doctor.com/faq">';
        $body .= 'Where Do My Reviews Go?';
        $body .= '</a>';
        $body .= '<br />';
        $body .= '&nbsp;';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '</table>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '</table>';

        // stats
        $body .= '<br />';
        $body .= '<br />';
        $body .= 'Keep in mind, online reviews start to lose credibility if they are out of date. Doctor.com recommends
            a goal of one review per week to ensure patients are seeing the most up-to-date information about your
            practice, and feel confident to call or book when they find you online. Remember, each review collected is
            multiplied because it will be visible in so many places across our network.';
        $body .= '<br />';
        $body .= '<br />';
        $body .= '<table width="90%" align="center">';
        $body .= '<tr>';
        // health
        $body .= '<td width="33%" align="center" style="text-align:center;">';
        $body .= '<img src="https://s3.amazonaws.com/assets.doctor.com/email/icon_health.gif" />&nbsp;&nbsp;';
        $body .= '<span style="font-size:48px; color:#63CECE; font-weight:100;">82%</span>';
        $body .= '<br />';
        $body .= '<small style="color:gray;">';
        $body .= 'of survey respondents said reviews have an<br /> influence on willingness to see a doctor';
        $body .= '</small>';
        $body .= '</td>';
        // reviews
        $body .= '<td width="33%" align="center" style="text-align:center;">';
        $body .= '<img src="https://s3.amazonaws.com/assets.doctor.com/email/icon_reviews.gif" />&nbsp;&nbsp;';
        $body .= '<span style="font-size:48px; color:#63CECE; font-weight:100;">90%</span>';
        $body .= '<br />';
        $body .= '<small style="color:gray;">';
        $body .= 'of patients say provider/practice<br /> reputation was a factor in their selection';
        $body .= '</small>';
        $body .= '</td>';
        // thumbs up
        $body .= '<td width="33%" align="center" style="text-align:center;">';
        $body .= '<img src="https://s3.amazonaws.com/assets.doctor.com/email/icon_thumbsup.gif" />&nbsp;&nbsp;';
        $body .= '<span style="font-size:48px; color:#63CECE; font-weight:100;">50%</span>';
        $body .= '<br />';
        $body .= '<small style="color:gray;">';
        $body .= 'of patients said good reviews would<br /> encourage them to select a doctor';
        $body .= '</small>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '</table>';
        $body .= '<br />';
        $body .= '<br />';
        $body .= 'Please don\'t hesitate to reach out with any questions. I will be reaching out to you after your
            program has been live for 60 days with an updated reputation scan. Congratulations on your early success and
             I\'m looking forward to continuing to work together.<br />';
        $body .= '<br />';

        // rep info
        $body .= 'All best,<br />';
        $body .= $accountRep;
        $body .= '<br />';
        $body .= '800-605-3582<br />';
        $body .= '<br />';

        return $body;
    }

    /**
     * Send the no-reviews-yet version, "needs attention" version of the 45 days report
     *
     * @param Organization $organization
     * @param string $accountRep
     * @return string
     */
    public static function get45DaysReportNoReviews($organization, $accountRep)
    {
        $body = '<style>td{width:auto !important;}</style>';
        $body .= '<br />';
        $body .= 'Hi,<br />';
        $body .= '<br />';
        $body .= 'I hope all is well! I am reaching out regarding your ReviewHub&trade;.';
        $body .= '<br />';
        $body .= '<br />';
        $body .= 'I was preparing to send you a ReviewHub&trade; Progress Report today and noticed that you haven\'t '
            . 'begun collecting reviews as of yet. I am writing a quick message to see if everything is ok, if there '
            . 'are technical issues, or if you require more training or information.';
        $body .= '<br />';
        $body .= '<br />';
        $body .= 'I would encourage you to visit <a href="https://www.doctor.com/training">www.doctor.com/training</a> '
            . 'if you haven\'t already. You\'ll also find contact information there for our support team if you require'
            . ' any technical assistance.';

        // stats
        $body .= '<br />';
        $body .= '<br />';
        $body .= '<strong>Keep in mind, current reviews start to lose credibility if they are out of date.</strong> '
            . 'Doctor.com recommends a goal of <strong>3-4 reviews</strong> per month to ensure patients are seeing the'
            . ' most up-to-date information about your practice, and feel confident to call or book when they find you '
            . 'online. Remember that each ReviewHub&trade; review collected is multiplied because it will be visible in'
            . ' so many places across our network.';
        $body .= '<br />';
        $body .= '<br />';
        $body .= '<table width="90%" align="center">';
        $body .= '<tr>';
        $body .= '<td width="33%" align="center" style="text-align:center;">';
        $body .= '<img src="https://s3.amazonaws.com/assets.doctor.com/email/icon_health.gif" />&nbsp;&nbsp;';
        $body .= '<span style="font-size:48px; color:#63CECE; font-weight:100;">82%</span>';
        $body .= '<br />';
        $body .= '<small style="color:gray;">';
        $body .= 'of survey respondents said reviews have an<br /> influence on willingness to see a doctor';
        $body .= '</small>';
        $body .= '</td>';
        $body .= '<td width="33%" align="center" style="text-align:center;">';
        $body .= '<img src="https://s3.amazonaws.com/assets.doctor.com/email/icon_reviews.gif" />&nbsp;&nbsp;';
        $body .= '<span style="font-size:48px; color:#63CECE; font-weight:100;">90%</span>';
        $body .= '<br />';
        $body .= '<small style="color:gray;">';
        $body .= 'of patients say provider/practice<br /> reputation was a factor in their selection';
        $body .= '</small>';
        $body .= '</td>';
        $body .= '<td width="33%" align="center" style="text-align:center;">';
        $body .= '<img src="https://s3.amazonaws.com/assets.doctor.com/email/icon_thumbsup.gif" />&nbsp;&nbsp;';
        $body .= '<span style="font-size:48px; color:#63CECE; font-weight:100;">50%</span>';
        $body .= '<br />';
        $body .= '<small style="color:gray;">';
        $body .= 'of patients said good reviews would<br /> encourage them to select a doctor';
        $body .= '</small>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '<tr>';
        $body .= '<td colspan="3" align="center" style="text-align:center;">';
        $body .= '<br />';
        $body .= '&nbsp;';
        $body .= '<a href="https://www.doctor.com/reviewhub#syndicate">';
        $body .= 'Where Do My Reviews Go?';
        $body .= '</a>';
        $body .= '</td>';
        $body .= '</tr>';
        $body .= '</table>';
        $body .= '<br />';
        $body .= '<br />';
        $body .= 'I am here to help your practice gain momentum with our service, so please don\'t hesitate to reach '
            . 'out directly.<br />';
        $body .= '<br />';

        // rep info
        $body .= 'All best,<br />';
        $body .= $accountRep;
        $body .= '<br />';

        return $body;
    }

    /**
     * Get providers from an organization
     * @param int $organizationId
     * @return array
     */
    public static function getProviders($organizationId, $condition = '')
    {
        // Tables references:
        // oa = organization_account
        // a = account
        // apro = account_provider
        // pro = provider
        // pp = provider_practice
        // pra = practice

        $sql = "SELECT pro.id, pro.first_last, pra.address
            FROM organization_account oa
            INNER JOIN account a
                ON a.id = oa.account_id
            INNER JOIN account_provider apro
                ON apro.account_id = oa.account_id
            INNER JOIN provider pro
                ON pro.id = apro.provider_id
            INNER JOIN provider_practice pp
                ON pp.provider_id = pro.id AND pp.primary_location = '1'
            INNER JOIN practice pra
                ON pra.id = pp.practice_id
            WHERE oa.organization_id = :organization_id
                AND oa.connection = :connection";

        $valuesToBind = [
            ':organization_id' => $organizationId,
            ':connection' => 'O'
        ];

        $condition = trim($condition);
        if ($condition != '') {
            $sql .= " AND pro.first_last LIKE :first_last_like";
            $valuesToBind[':first_last_like'] = '%' . $condition . '%';
        }

        return Yii::app()->db->createCommand($sql)
            ->bindValues($valuesToBind)
            ->queryAll();
    }

    /**
     * Get practices from an organization
     * @param int $organizationId
     * @return array
     */
    public static function getPractices($organizationId, $condition = '')
    {
        // Tables references:
        // oa = organization_account
        // a = account
        // apra = account_practice
        // pra = practice

        $condition = trim($condition);

        $sql = "SELECT pra.id, pra.name, pra.address
            FROM organization_account oa
            INNER JOIN account a
                ON a.id = oa.account_id
            INNER JOIN account_practice apra
                ON apra.account_id = oa.account_id
            INNER JOIN practice pra
                ON pra.id = apra.practice_id
            WHERE oa.organization_id = :organization_id
                AND oa.connection = :connection";

        $valuesToBind = [
            ':organization_id' => $organizationId,
            ':connection' => 'O'
        ];

        if ($condition != '') {
            $sql .= " AND pra.name LIKE :practice_name_like";
            $valuesToBind[':practice_name_like'] = $condition;
        }

        return Yii::app()->db->createCommand($sql)
            ->bindValues($valuesToBind)
            ->queryAll();
    }

    /**
     * Remove provider_practice associations with practices that aren't attached to active organizations
     * @param int $organizationId
     * @param int $accountId
     * @param int $practiceId
     * @return bool
     */
    private static function unwantedProviderPractices($organizationId, $accountId, $practiceId)
    {
        // current practice is related with other accounts
        $accountsPractice = AccountPractice::model()->findAll(
            "practice_id = :practice_id AND account_id <> :account_id",
            array(":practice_id" => $practiceId, ":account_id" => $accountId)
        );

        // if no exists we could remove provider_practice
        if (empty($accountsPractice)) {
            return true;
        }

        // if any account is related with other organization we couldn't delete the relation
        foreach ($accountsPractice as $accountPractice) {
            $exists = OrganizationAccount::model()->exists(
                'account_id = :account_id',
                array(':account_id' => $accountPractice->account_id)
            );
            if ($exists) {
                return false;
            }
        }

        return true;
    }

    /**
     * Update Scan Tool-related fields in Salesforce for a given organization
     * - Scan_Score__c
     * - Mobile_Website_Pass_Fail_No_Website__c
     * - Reviews_Scan_Date__c
     * - Yelp_Initial_Reviews__c
     * - Google_Initial_Reviews__c
     * - HealthGrades_Initial_Reviews__c
     *
     * @param array $arrOrganization
     * @return mixed
     */
    public static function updateSFScanToolStats($arrOrganization)
    {
        $organizationId = $arrOrganization['organization_id'];
        $salesforceId = $arrOrganization['salesforce_id'];

        $sfAccount = Yii::app()->salesforce->getObject("Account", $salesforceId);
        if (empty($sfAccount)) {
            // sf account not found
            return false;
        }

        // find the date of the latest scan
        $criteria = new CDbCriteria();
        $criteria->condition = 'organization_id=:organizationId';
        $criteria->params = array(':organizationId' => $organizationId);
        $criteria->order = 'id DESC';
        $scan = Scan::model()->find($criteria);
        $yearMonth = $scan['year_month'];

        // find the right scans
        // (but only for providers, otherwise we get the same data for providers and practices and then count is wrong)
        $criteria = new CDbCriteria();
        $criteria->condition =
            'organization_id=:organizationId AND `year_month`=:yearMonth AND provider_id IS NOT NULL';
        $criteria->params = array(
            ':organizationId' => $organizationId,
            ':yearMonth' => $yearMonth
        );
        $scans = Scan::model()->findAll($criteria);

        // go through scans calculating our data
        $scanCounter = $scanScore = $googleReviewNumber = $yelpReviewNumber = 0;
        $scanMobileWebsiteStatus = null;
        foreach ($scans as $scan) {

            // mobile website will be marked ok if it's ok for everyone
            if ($scanMobileWebsiteStatus == null) {
                $scanMobileWebsiteStatus = $scan->pagespeed_score >= 80;
            } else {
                $scanMobileWebsiteStatus = $scanMobileWebsiteStatus && $scan->pagespeed_score >= 80;
            }

            // accumulate score
            $scanScore += $scan->overall_score;

            // count reviews
            $reviews = ScanResult::model()->find(
                'scan_id=:scanId AND `code` = "google"',
                array(':scanId' => $scan->id)
            );
            if ($reviews) {
                $googleReviewNumber += $reviews->review_number;
            }

            $reviews = ScanResult::model()->find(
                'scan_id=:scanId AND `code` = "yelp"',
                array(':scanId' => $scan->id)
            );
            if ($reviews) {
                $yelpReviewNumber += $reviews->review_number;
            }

            // increment scan counter
            $scanCounter++;
        }

        // find the right scans
        // (but only for practices, otherwise we get the same data for providers and practices and then count is wrong)
        $criteria = new CDbCriteria();
        $criteria->condition =
            'organization_id=:organizationId AND `year_month`=:yearMonth AND provider_id IS NULL';
        $criteria->params = array(
            ':organizationId' => $organizationId,
            ':yearMonth' => $yearMonth
        );
        $scans = Scan::model()->findAll($criteria);

        // go through scans calculating our data
        $healthgradesReviewNumber = 0;
        foreach ($scans as $scan) {

            // count reviews
            $reviews = ScanResult::model()->find(
                'scan_id=:scanId AND `code` = "healthgrades"',
                array(':scanId' => $scan->id)
            );
            if ($reviews) {
                $healthgradesReviewNumber += $reviews->review_number;
            }
        }

        // done with scans

        // always update score and mobile website status
        $sfData = array(
            'Scan_Score__c' => (int) round($scanScore / $scanCounter),
            'Mobile_Website_Pass_Fail_No_Website__c' => (string) ($scanMobileWebsiteStatus === null
                ? 'No website'
                : (isset($scanMobileWebsiteStatus['mobileWebsite']) ? 'Pass' : 'Fail'))
        );

        // check if we have to update reviews as well
        if (($sfAccount['Google_Initial_Reviews__c'] == '' || $sfAccount['Google_Initial_Reviews__c'] == 'null')
            && $sfAccount['HealthGrades_Initial_Reviews__c'] == ''
            || $sfAccount['HealthGrades_Initial_Reviews__c'] == 'null'
            && $sfAccount['Yelp_Initial_Reviews__c'] == ''
            || $sfAccount['Yelp_Initial_Reviews__c'] == 'null'
        ) {

            $sfData['Google_Initial_Reviews__c'] = $googleReviewNumber;
            $sfData['HealthGrades_Initial_Reviews__c'] = $healthgradesReviewNumber;
            $sfData['Yelp_Initial_Reviews__c'] = $yelpReviewNumber;
            $sfData['Reviews_Scan_Date__c'] = TimeComponent::createSalesforceDatetime($scan->date_added);
        }

        // send a single request with everything
        Yii::app()->salesforce->updateAccount($salesforceId, $sfData);
    }

    /**
     * Export GMB Entities (associated or not).
     * @param int $orgID
     * @param bool $associated
     * @return array
     */
    public static function getGMBEntities($orgID, $associated)
    {
        // set empty result if something fail
        $result = [
            'providers_at_practices' => [],
            'practices' => []
        ];

        $organization = Organization::model()->findByPk($orgID);

        if (empty($organization)) {
            return $result;
        }

        $account = $organization->getOwnerAccount();

        if (empty($account)) {
            return $result;
        }

        // for organization with many providers/practices
        $sql = 'SET SESSION group_concat_max_len = 1000000;';
        Yii::app()->db->createCommand($sql)->execute();

        // get all providers and practices in a string
        $sql = 'SELECT GROUP_CONCAT(provider_id) FROM account_provider WHERE account_id = :account_id';
        $query = Yii::app()->db->createCommand($sql);
        $query->bindValue(':account_id', $account->id, PDO::PARAM_INT);
        $providers = $query->queryScalar();

        $sql = 'SELECT GROUP_CONCAT(practice_id)  FROM account_practice WHERE account_id = :account_id';
        $query = Yii::app()->db->createCommand($sql);
        $query->bindValue(':account_id', $account->id, PDO::PARAM_INT);
        $practices = $query->queryScalar();

        // different query if should return associated or not associated
        if ($associated) {
            $join = 'INNER ';
            $where = '1';
        } else {
            $join = 'LEFT ';
            $where = 'provider_practice_gmb.id IS NULL';
        }

        // get providers at practices relation
        $sql = sprintf(
            'SELECT DISTINCT
            provider_practice_gmb.place_id,
            practice.id AS practice_id,
            practice.name AS practice_name,
            address_full AS practice_address,
            provider.id AS provider_id,
            provider.npi_id AS provider_npi,
            provider.first_middle_last AS provider_name,
            provider_practice_gmb.date_added,
            IF(provider_practice.phone_number != "", provider_practice.phone_number, practice.phone_number) AS
            provider_phone,
            link_verified,
            has_sync_issues,
            GROUP_CONCAT(DISTINCT(specialty.name)) AS specialties
            FROM practice
            INNER JOIN provider_practice
            ON provider_practice.practice_id = practice.id
            INNER JOIN provider
            ON provider.id = provider_practice.provider_id
            %s JOIN provider_practice_gmb
            ON provider_practice_gmb.practice_id = practice.id
            AND provider_practice_gmb.provider_id = provider.id
            LEFT JOIN provider_specialty
            ON provider.id = provider_specialty.provider_id
            LEFT JOIN sub_specialty
            ON provider_specialty.sub_specialty_id = sub_specialty.id
            LEFT JOIN specialty
            ON sub_specialty.specialty_id = specialty.id
            WHERE %s
            %s
            %s
            GROUP BY provider_practice.id
            ORDER BY practice.name, provider.first_middle_last;',
            $join,
            $where,
            ($providers ? 'AND provider.id IN (' . $providers . ')' : ''),
            ($practices ? 'AND practice.id IN (' . $practices . ')' : '')
        );
        $result['providers_at_practices'] = Yii::app()->db->createCommand($sql)->queryAll();

        // get practices
        $sql = sprintf(
            'SELECT DISTINCT
            provider_practice_gmb.place_id,
            practice.id AS practice_id,
            practice.name AS practice_name,
            address_full AS practice_address,
            provider_practice_gmb.date_added,
            practice.phone_number AS practice_phone,
            link_verified,
            has_sync_issues,
            GROUP_CONCAT(DISTINCT(specialty.name)) AS specialties
            FROM practice
            %s JOIN provider_practice_gmb
            ON provider_practice_gmb.practice_id = practice.id
            AND provider_practice_gmb.provider_id IS NULL
            LEFT JOIN practice_specialty
            ON practice.id = practice_specialty.practice_id
            LEFT JOIN sub_specialty
            ON practice_specialty.sub_specialty_id = sub_specialty.id
            LEFT JOIN specialty
            ON sub_specialty.specialty_id = specialty.id
            WHERE %s
            %s
            GROUP BY practice.id
            ORDER BY practice.name;',
            $join,
            $where,
            ($practices ? 'AND practice.id IN (' . $practices . ')' : '')
        );
        $result['practices'] = Yii::app()->db->createCommand($sql)->queryAll();

        return $result;
    }

    /**
     * Update claimed value of all providers and practices related to the specified organization.
     */
    public function updateClaimedValue()
    {
        // get owner account
        $account = $this->getOwnerAccount();

        // get all providers related to the organization
        $arrProviders = Provider::model()->with('accountProviders')->findAll(
            'accountProviders.account_id = :account_id',
            [':account_id' => $account->id]
        );

        // update claimed value of each provider
        foreach ($arrProviders as $provider) {
            AccountProvider::updateClaimed($provider->id);
        }

        // empty memory
        unset($arrProviders);

        // get all practices related to the organization
        $arrPractices = Practice::model()->with('accountPractices')->findAll(
            'accountPractices.account_id = :account_id',
            [':account_id' => $account->id]
        );

        // update claimed value of each practice
        foreach ($arrPractices as $practice) {
            AccountPractice::updateClaimed($practice->id);
        }
    }

    /**
     * Remove nasp date of all providers and practices related to this organization.
     * If a provider or practice is also related to other active or paused organization, the nasp date will not be
     * removed on that provider or practice
     */
    public function removeNaspDate()
    {
        // get owner account
        $account = $this->getOwnerAccount();

        // query to get all providers that shuld have null nasp
        $sql = "SELECT DISTINCT provider.id
            FROM provider
            JOIN account_provider ON provider.id = account_provider.provider_id
            WHERE account_provider.account_id = :account_id AND
            NOT EXISTS (SELECT 1 FROM provider p
                JOIN account_provider ap ON p.id = ap.provider_id
                JOIN organization_account ON
                    organization_account.account_id = ap.account_id AND
                    organization_account.connection = 'O'
                JOIN organization ON organization_account.organization_id = organization.id AND
                    organization.status != 'C'
                WHERE p.id = provider.id AND organization.id <> :organization_id
            ) AND nasp_date IS NOT NULL";
        $query = Yii::app()->db->createCommand($sql);
        $query->bindValue(':account_id', $account->id);
        $query->bindValue(':organization_id', $this->id);
        $arrProviders = $query->queryColumn();

        // update claimed value of each provider
        foreach ($arrProviders as $providerId) {
            // update provider nasp date
            $provider = Provider::model()->findByPk($providerId);
            $provider->nasp_date = null;
            $provider->save();
        }

        // query to get all practices that shuld have null nasp
        $sql = "SELECT DISTINCT practice.id
            FROM practice
            JOIN account_practice ON practice.id = account_practice.practice_id
            WHERE account_practice.account_id = :account_id AND
            NOT EXISTS (SELECT 1 FROM practice p
                JOIN account_practice ap ON p.id = ap.practice_id
                JOIN organization_account ON
                    organization_account.account_id = ap.account_id AND
                    organization_account.connection = 'O'
                JOIN organization ON organization_account.organization_id = organization.id AND
                    organization.status != 'C'
                WHERE p.id = practice.id AND organization.id <> :organization_id
            ) AND nasp_date IS NOT NULL";
        $query = Yii::app()->db->createCommand($sql);
        $query->bindValue(':account_id', $account->id);
        $query->bindValue(':organization_id', $this->id);
        $arrPractices = $query->queryColumn();

        // update claimed value of each practice
        foreach ($arrPractices as $practiceId) {
            // update practice nasp date
            $practice = Practice::model()->findByPk($practiceId);
            $practice->nasp_date = null;
            $practice->save();
        }
    }

    /**
     * Update Salesforce with GMB-related information
     * @param int $organizationId
     * @return mixed
     */
    public static function updateSFAccountGMB($organizationId)
    {

        // look up the organization
        $organizationObj = Organization::model()->findByPk($organizationId);
        if (empty($organizationObj)) {
            // organization not found
            return false;
        }

        // look up the salesforce account
        $sfAccount = Yii::app()->salesforce->getObject("Account", $organizationObj['salesforce_id']);
        if (empty($sfAccount)) {
            // sf account not found
            return false;
        }

        // count how many listings we have
        $sql = sprintf(
            "SELECT COUNT(1) AS total
            FROM provider_practice_gmb
            INNER JOIN organization_account
            ON provider_practice_gmb.account_id = organization_account.account_id
            WHERE organization_account.organization_id = %d;",
            $organizationId
        );
        $listingsCount = Yii::app()->db->createCommand($sql)->queryScalar();

        // count how many claimed listings we have
        $sql = sprintf(
            "SELECT COUNT(1) AS total
            FROM provider_practice_gmb
            INNER JOIN organization_account
            ON provider_practice_gmb.account_id = organization_account.account_id
            WHERE organization_account.organization_id = %d
            AND is_claimed_by = 'DDC';",
            $organizationId
        );
        $claimedListingsCount = Yii::app()->db->createCommand($sql)->queryScalar();

        // count how many accounts we have with auto-publish enabled
        $sql = sprintf(
            "SELECT COUNT(1) AS total
            FROM account_details
            INNER JOIN organization_account
            ON account_details.account_id = organization_account.account_id
            WHERE organization_account.organization_id = %d
            AND google_auto_publish = 1;",
            $organizationId
        );
        $autoPublishCount = Yii::app()->db->createCommand($sql)->queryScalar();

        // count how many unclaimed listings with disabled posts we have
        $sql = sprintf(
            "SELECT COUNT(1) AS total
            FROM provider_practice_gmb
            INNER JOIN organization_account
            ON provider_practice_gmb.account_id = organization_account.account_id
            WHERE organization_account.organization_id = %d
            AND has_localposts_api = 0
            AND is_claimed_by != 'DDC';",
            $organizationId
        );
        $disabledAPIUnclaimedListingsCount = Yii::app()->db->createCommand($sql)->queryScalar();

        // count how many listings with disabled posts we have
        $sql = sprintf(
            "SELECT COUNT(1) AS total
            FROM provider_practice_gmb
            INNER JOIN organization_account
            ON provider_practice_gmb.account_id = organization_account.account_id
            WHERE organization_account.organization_id = %d
            AND has_localposts_api = 0;",
            $organizationId
        );
        $disabledAPIListingsCount = Yii::app()->db->createCommand($sql)->queryScalar();

        // count how many listings we have with the localposts api enabled
        $sql = sprintf(
            "SELECT COUNT(1) AS total
            FROM provider_practice_gmb
            INNER JOIN organization_account
            ON provider_practice_gmb.account_id = organization_account.account_id
            WHERE organization_account.organization_id = %d
            AND has_localposts_api = 1;",
            $organizationId
        );
        $hasLocalPostsAPICount = Yii::app()->db->createCommand($sql)->queryScalar();

        // do we have no listings?
        if ($listingsCount == 0) {
            // correct
            $googlePostsEnabled = "No listings";
        } elseif (
            $hasLocalPostsAPICount == 0 && $disabledAPIListingsCount == $listingsCount &&
            $claimedListingsCount >= 1
        ) {
            // we have no listings to enable it for (among our claimed listings)
            $googlePostsEnabled = 'Ineligible';
        } elseif ($autoPublishCount == 0 && $disabledAPIUnclaimedListingsCount == $listingsCount) {
            // we have no claimed listings to enable it for
            $googlePostsEnabled = "No claimed listings";
        } elseif ($autoPublishCount >= 1) {
            // we have auto-publish enabled!
            $googlePostsEnabled = 'Enabled';
        } else {
            // we have listings to enable it for but havne't
            $googlePostsEnabled = 'Disabled';
        }

        // check if we have to update whether posts are enabled
        if (
            empty($sfAccount['Google_Posts_Enabled__c']) || (!empty($sfAccount['Google_Posts_Enabled__c']) &&
                $sfAccount['Google_Posts_Enabled__c'] != $googlePostsEnabled)
        ) {
            // yes, we need to update
            return Yii::app()->salesforce->updateAccountField(
                $organizationObj->salesforce_id,
                'Google_Posts_Enabled__c',
                $googlePostsEnabled
            );
        }
        return true;
    }
    /**
     * Send an organization's data to Salesforce so that its account is created
     * Used when receiving organizations through an API, since usually the flow is the other way around:
     * Accounts are created in SF and closing them down calls the Closing tool which creates the organization
     * @param int $organizationId
     * @return boolean
     */
    public static function createInSalesforce($organizationId)
    {

        // look up the organization
        $organization = Organization::model()->findByPk($organizationId);

        // could we find the organization?
        if (!$organization) {
            // no, leave
            return false;
        }

        // look up the main account
        $account = $organization->getOwnerAccount();

        // could we find the account?
        if (!$account) {
            // no, leave
            return false;
        }

        // look up the main practice
        $sql = sprintf(
            "SELECT practice.phone_number, practice.website, practice.address
            FROM practice
            INNER JOIN account_practice
            ON account_practice.practice_id = practice.id
            WHERE account_practice.account_id = %d
            ORDER BY account_practice.id;",
            $account->id
        );
        $practice = Yii::app()->db->createCommand($sql)->queryRow();

        // look up the main specialty
        $sql = sprintf(
            "SELECT specialty.name
            FROM specialty
            INNER JOIN sub_specialty
            ON specialty.id = sub_specialty.specialty_id
            INNER JOIN provider_specialty
            ON sub_specialty.id = provider_specialty.sub_specialty_id
            AND primary_specialty = 1
            INNER JOIN account_provider
            ON account_provider.provider_id = provider_specialty.provider_id
            WHERE account_provider.account_id = %d
            ORDER BY account_provider.id;",
            $account->id
        );
        $primarySpecialty = Yii::app()->db->createCommand($sql)->queryScalar();

        // look up the main provider
        $sql = sprintf(
            "SELECT provider.npi_id, provider.first_m_last
            FROM provider
            INNER JOIN account_provider
            ON account_provider.provider_id = provider.id
            WHERE account_provider.account_id = %d
            ORDER BY account_provider.id;",
            $account->id
        );
        $provider = Yii::app()->db->createCommand($sql)->queryRow();

        // count providers
        $sql = sprintf(
            "SELECT COUNT(DISTINCT(provider_id)) FROM account_provider WHERE account_provider.account_id = %d;",
            $account->id
        );
        $providerCount = Yii::app()->db->createCommand($sql)->queryScalar();

        // create empty array
        $arrData = [];

        // is the partner site LocalIq (partner site 165, partner 75)?
        if ($organization->partner_site_id == 165) {
            // it is, we'll send some hardcoded data & some data specific to them

            // standard fields
            $arrData['OwnerId'] = '0051J00000508E7';
            $arrData['RecordTypeId'] = '012o00000008BjwAAE';
            $arrData['BillingStreet'] = '1100 Avenue des Canadiens-de-Montreal, Suite 150';
            $arrData['BillingCity'] = 'Montreal';
            $arrData['BillingState'] = 'Quebec';
            $arrData['BillingPostalCode'] = 'H3B 2S2';
            $arrData['BillingCountry'] = 'Canada';
            $arrData['ParentId'] = '0011J00001J9M3z';

            // custom fields
            $arrData['Recurring_Fee__c'] = (8.5 * $providerCount);
            $arrData['Account_Rep__c'] = '005o0000003cnlp';
            $arrData['Onboard_Rep__c'] = '005o0000003cnlp';
            $arrData['Contract_Term__c'] = '1 Year';
            $arrData['Sales_Channel__c'] = 'Business Development';
            $arrData['Billing_Email__c'] = 'email@email.com';
            $arrData['Doctor_Email__c'] = 'email@email.com';
            $arrData['Email_for_Patients__c'] = 'email@email.com';
            $arrData['Client_Support_Tier__c'] = 'Enablement only';
            $arrData['Channel_Partner__c'] = 'Local IQ';
            $arrData['Billing_Interval__c'] = 'Quarterly';
            $arrData['Billing_Name__c'] = 'SWEETIQ Analytics Corp.';
            $arrData['Package__c'] = 'Basic';
        }

        // build the rest of the fields with generic data
        $sfFormattedDateAdded = TimeComponent::createSalesforceDatetime($organization->date_added);
        $arrData['Name'] = $organization->organization_name;
        $arrData['Phone'] = (!empty($practice['phone_number']) ? $practice['phone_number'] : 'N/A');
        $arrData['Type'] = 'SMP';
        $arrData['Website'] = (!empty($practice['website']) ? $practice['website'] : null);
        $arrData['Specialty__c'] = (!empty($primarySpecialty) ? $primarySpecialty : null);
        $arrData['Top_Priorities_Other__c'] = '';
        $arrData['Account_Status__c'] = 'Open';
        $arrData['Billing_Status__c'] = 'Active';
        $arrData['Doctor_com_Account_Id__c'] = $account->id;
        $arrData['Contract_Start_Date__c'] = $sfFormattedDateAdded;
        $arrData['Onboard_Held_Date__c'] = $sfFormattedDateAdded;
        $arrData['Recurring_Fee_Restart_Date__c'] = $sfFormattedDateAdded;
        $arrData['Recurring_Fee_Start_Date__c'] = $sfFormattedDateAdded;
        $arrData['Setup_Fee_Start_Date__c'] = $sfFormattedDateAdded;
        $arrData['Closing_Tool_Completed__c'] = $sfFormattedDateAdded;
        $arrData['Onboard_Held_DateTime__c'] = $sfFormattedDateAdded;
        $arrData['Sale_Closed__c'] = $sfFormattedDateAdded;
        $arrData['Doctor_com_Org_Id__c'] = $organization->id;
        $arrData['Practice_Details__c'] = (!empty($practice['address']) ? $practice['address'] : null);
        $arrData['Mobile_Website_Pass_Fail_No_Website__c'] =
            (!empty($practice['website']) ? $practice['website'] : null);
        $arrData['Main_Doctor__c'] = (!empty($provider['first_m_last']) ? $provider['first_m_last'] : null);
        $arrData['Provider_NPI__c'] = (!empty($provider['npi_id']) ? $provider['npi_id'] : null);
        $arrData['Client_Insights_Program_Goals__c'] = 'N/A';
        $arrData['Perceived_Value__c'] = 'N/A';
        $arrData['Perceived_Concern__c'] = 'N/A';

        // try to create the account in salesforce
        $result = Yii::app()->salesforce->createAccount($arrData);

        if (!empty($result) && !empty($result["id"])) {
            // first update our own salesforce_id
            $organization->salesforce_id = $result["id"];

            // queue updating features and provider names and counts
            SqsComponent::sendMessage('updateSFOrganizationFeatures', $organization->id);

            return $organization->save();
        }
        return false;
    }

    /**
     * Return organizations for a given partner site id
     * @param $partnerSiteId
     * @return mixed
     */
    public static function findByPartnerSite($partnerSiteId)
    {
        $sql = sprintf(
            "
            SELECT
                organization.id,
                organization.organization_name,
                organization.date_added,
                organization.status,
                organization_account.account_id
            FROM organization
            INNER JOIN organization_account
                ON organization_account.organization_id = organization.id
            WHERE
                organization.partner_site_id = %d AND
                organization_account.connection = 'O';
            ",
            $partnerSiteId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    public function getListingsQaToolSelector()
    {
        $sql = "SELECT organization.id, organization_name, partner_site_id,
            partner_site.display_name AS partner_name
            FROM organization
            LEFT JOIN partner_site ON partner_site.id = organization.partner_site_id
            WHERE `status` = 'A'
                AND organization_name != ''
            ORDER BY organization_name ASC";
        return Organization::model()->cache(Yii::app()->params['cache_long'])->findAllBySql($sql);
    }

    public function getByIds($ids)
    {
        $c = new CDbCriteria();
        $c->addInCondition('id', $ids);

        return $this->findAll($c);
    }

    public function getByPartners($partnerIds)
    {
        $c = new CDbCriteria();
        $c->addInCondition('partner_site_id', $partnerIds);

        return $this->findAll($c);
    }

    public function getIdsFromPartners($partnerIds)
    {
        $ids = [];
        $orgs = $this->getByPartners($partnerIds);
        if ($orgs) {
            foreach ($orgs as $org) {
                $ids[] = $org->id;
            }
        }

        return $ids;
    }

    public function getFeatures()
    {
        foreach ($this->organizationFeatures as $feature) {
            if (!$feature->activate()) {
                $flawless = false;
            }
        }
    }

    /**
     * Get summary for EntitySync.
     *
     * @return array
     */
    public function getEntitySyncSummary()
    {
        $sql = 'SELECT
            epe."PARTNER_ENTITY_ID" AS provider_id, eqe."PARTNER_ENTITY_ID" AS practice_id, eq."NAME" AS practice_name,
            eq."ADDRESS1" AS practice_address_1, eq."ADDRESS2" AS practice_address_2, eq."CITY" as practice_city,
            eq."STATE" AS practice_state, eq."ZIP" AS practice_zip,
            ep."NAME" AS provider_name, ep."FIRST_NAME" AS provider_first_name, ep."MIDDLE_NAME" AS provider_middle_name,
            ep."LAST_NAME" AS provider_last_name, ep."CODE" AS provider_npi_number, ep."GENDER" AS provider_gender,
            ep."SPECIALTY" AS provider_specialty_name
            FROM bf_compass."PARTNER" p
            INNER JOIN bf_compass."PARTNER_CLIENT" pc ON p."ID" = pc."PARTNER_ID"
            INNER JOIN bf_compass."CLIENT" c ON pc."CLIENT_ID" = c."ID"
            INNER JOIN bf_compass."AVATAR" a ON a."CLIENT_ID" = c."ID"
            INNER JOIN bf_compass."ENTITY" ep ON a."PERSON_ID" = ep."ID"
            INNER JOIN bf_compass."PARTNER_ENTITY" epe ON ep."ID" = epe."ENTITY_ID"
            INNER JOIN bf_compass."ENTITY" eq ON a."LOCATION_ID" = eq."ID"
            INNER JOIN bf_compass."PARTNER_ENTITY" eqe ON eq."ID" = eqe."ENTITY_ID"
            WHERE
            p."ID" = 2 AND c."ID" = (SELECT "CLIENT_ID" FROM bf_compass."AVATAR" WHERE "ID" = :bf_brand_id);';

            return Yii::app()->dbPostgresql->createCommand($sql)
                ->bindValues(array(":bf_brand_id" => $this->bf_brand_id))
                ->queryAll();
    }
}
