<?php

Yii::import('application.modules.core_models.models.ScanResultProvider');

class ScanResultProviderEvaluation extends ScanResultProvider
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'scan_result_provider_evaluation';
    }
}
