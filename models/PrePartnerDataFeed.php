<?php

Yii::import('application.modules.core_models.models._base.BasePrePartnerDataFeed');

class PrePartnerDataFeed extends BasePrePartnerDataFeed
{

    /**
     * @var string value of the overriden table name
     */
    protected $_tableName = "";

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Changes the model's table name
     * @param string $tableName New table name
     */
    public function setTableName($tableName)
    {
        $this->_tableName = $tableName;
        $this->refreshMetadata();
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        if ($this->_tableName != "") {
            return $this->_tableName;
        }
        return parent::tableName();
    }

}
