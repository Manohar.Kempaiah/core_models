<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeCompetitor');

class ProviderPracticeCompetitor extends BaseProviderPracticeCompetitor
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
