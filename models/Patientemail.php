<?php

Yii::import('application.modules.core_models.models._base.BasePatientemail');

class Patientemail extends BasePatientemail
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get totals of different status messages for a practice through Automatic Review Requests
     *
     * @deprecated as only used by PADMv1
     *
     * @param int $practiceId
     * @param int $listAllAccounts
     * @param bool $totalized
     * @return array
     */
    public static function getAutomaticRREmails($practiceId = 0, $listAllAccounts = '', $totalized = true)
    {
        $sql = sprintf(
            "SELECT pe.id, auto_rr_sent_count, auto_rr_send_link, patient_email,
                auto_rr_send_link_detail, pc.communication_id, pc.communication_table,
                cl.is_processed,
                IF(cl.is_sent IS NULL, 1, cl.is_sent) AS is_sent,
                IF(cl.message_status IS NULL, 'send', cl.message_status) AS message_status
            FROM patientemail AS pe
                LEFT JOIN patientemail_communications AS pc ON pe.id = pc.patientemail_id
                LEFT JOIN communication_log AS cl ON pc.communication_id = cl.id
                    AND pc.communication_table = 'communication_log'
            WHERE account_id in (%s) AND practice_id = %d
            GROUP BY pe.id",
            $listAllAccounts,
            $practiceId
        );

        $result = Yii::app()->db
            ->cache(Yii::app()->params['cache_medium'])
            ->createCommand($sql)
            ->queryAll();

        if (empty($result) || !$totalized) {
            return $result;
        }

        // set an array to return
        $arrClicks = array(
            'click' => 0,
            'hard_bounce' => 0,
            'open' => 0,
            'send' => 0,
            'soft_bounce' => 0,
            'reject' => 0,
            'deferral' => 0,
            'spam' => 0,
        );

        // overwrite the default values
        foreach ($result as $message) {
            $arrClicks[$message['message_status']] = $arrClicks[$message['message_status']] + 1;
        }

        return $arrClicks;
    }

    /**
     * Get totals of different number of RR sent group by the link type
     * @param int $practiceId
     * @param int $accountId
     * @return array
     */
    public static function getRRLinkTypes($practiceId, $accountId)
    {
        $sql = sprintf(
            "SELECT COUNT(DISTINCT patientemail_id) AS total, auto_rr_send_link FROM (
            SELECT patientemail.id as patientemail_id,
            IF(auto_rr_send_link='DYNAMIC' AND auto_rr_send_link_detail LIKE '%%yelp%%','YELP',
                IF(auto_rr_send_link='DYNAMIC' AND auto_rr_send_link_detail LIKE '%%google%%','GOOGLE',
                IF(auto_rr_send_link='DYNAMIC' AND auto_rr_send_link_detail LIKE '%%ddc%%','DDC',
                    IF(auto_rr_send_link<>'DYNAMIC',auto_rr_send_link, 'N/A')))) as auto_rr_send_link
                      FROM patientemail
                      INNER JOIN patientemail_communications
                          ON patientemail.id = patientemail_communications.patientemail_id
                      INNER JOIN communication_log
                          ON patientemail_communications.communication_id = communication_log.id
                          AND communication_table = 'communication_log'
                      WHERE practice_id = %d
                      AND account_id= %d
                      AND communication_log.is_sent = 1
            ) as patient_email_status
            GROUP BY auto_rr_send_link
            HAVING total>0",
            $practiceId,
            $accountId
        );

        // set an array to return
        $arrLinkTypes = array(
            'GOOGLE' => 0,
            'YELP' => 0,
            'DDC' => 0,
            'N/A' => 0,
        );

        // overwrite the default values
        foreach (Yii::app()->db->createCommand($sql)->queryAll() as $message) {
            $arrLinkTypes[$message['auto_rr_send_link']] = $message['total'];
        }

        return $arrLinkTypes;
    }

    /**
     * ARR CMD: Gets the list of practices that Automatic ReviewRequest should be sent to
     * Order by practice.id just so that we know what to expect
     * @return array
     */
    public static function getARRPractices()
    {

        $sql = sprintf(
            "SELECT DISTINCT
            practice.id AS id, practice.name AS practice_name, practice.logo_path AS logo_path,
            review_request_auto_subject, review_request_auto_body, review_request_auto_daily_limit,
            review_request_email_title, review_request_email_body, review_request_sms_body,
            google_review_email_title, google_review_email_body, google_review_sms_body,
            yelp_review_email_title, yelp_review_email_body, yelp_review_sms_body
            FROM practice
            INNER JOIN patientemail
                ON patientemail.practice_id = practice.id
            LEFT JOIN practice_details
                ON practice.id = practice_details.practice_id
            WHERE review_request_auto_daily_limit != 0
                AND opt_out = 0
                AND auto_rr_sent_count = 0
            ORDER BY practice.id;"
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * ARR CMD: Get the list of patientemails to be contacted by a given $practiceId
     * @param int $practiceId
     * @param int $rrAutoDailyLimit
     * @return array
     */
    public static function getARRByPractice($practiceId, $rrAutoDailyLimit = 5)
    {

        if (empty($practiceId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT DISTINCT
                patientemail.id AS id,
                patientemail.practice_id,
                patientemail.auto_rr_sent_count,
                patientemail.auto_rr_send_link_detail,
                patientemail.auto_rr_send_link,
                patientemail.account_device_id,
                patient_email,
                patient_first_name,
                patient_last_name,
                ppl_google.id AS google_listing_id,
                ppl_yelp.id AS yelp_listing_id,
                '' AS ddc_url
            FROM patientemail
            LEFT JOIN provider_practice_listing AS ppl_google
                ON patientemail.practice_id = ppl_google.practice_id
                AND ppl_google.listing_type_id = %d
                AND ppl_google.provider_id IS NULL
            LEFT JOIN provider_practice_listing AS ppl_yelp
                ON patientemail.practice_id = ppl_yelp.practice_id
                AND ppl_yelp.listing_type_id = %d
                AND ppl_yelp.provider_id IS NULL
            LEFT JOIN patient
                ON patientemail.patient_email = patient.email
            WHERE opt_out = 0
                /* only contact patients who haven't been contacted before by that practice */
                AND auto_rr_sent_count = 0
                AND (patient.id IS NULL OR patient.receive_information = 1)
                /* make sure they were updated and not just freshly added */
                AND patientemail.patient_email NOT IN
                (
                    SELECT patient_email
                    FROM patientemail
                    WHERE date_updated BETWEEN DATE_SUB(NOW(), INTERVAL 90 DAY) AND NOW()
                        AND practice_id = %d
                        AND date_added != date_updated
                        AND auto_rr_sent_count <> 0
                )
                AND  patientemail.practice_id = %d
            ORDER BY patientemail.date_added ASC
            LIMIT %d;",
            ListingType::GOOGLE_PROFILE_ID,
            ListingType::YELP_PROFILE_ID,
            $practiceId,
            $practiceId,
            $rrAutoDailyLimit
        );
        $result = Yii::app()->db->createCommand($sql)->queryAll();



        if (!empty($result)) {
            foreach ($result as $k => $v) {
                // We send to DDC ReviewRequest ?
                if ($v['auto_rr_send_link'] == 'DDC') {
                    // Yes
                    // create the ddc_url
                    $accountDevice = AccountDevice::model()
                        ->cache(Yii::app()->params['cache_medium'])
                        ->findByPk($v['account_device_id']);

                    // adding encoded params to the url
                    $arrParams['provider_id'] = null;
                    $arrParams['patient_first_name'] = $v['patient_first_name'];
                    $arrParams['patient_last_name'] = $v['patient_last_name'];
                    $arrParams['patient_email'] = $v['patient_email'];
                    $arrParams['patient_phone'] = null;

                    // Adding key for unique links
                    $result[$k]['key'] = ['uid' => StringComponent::generateGUID()];
                    $result[$k]['ddc_url'] = $accountDevice->getReviewHubUrl($arrParams, $result[$k]['key']);
                }
            }
        }

        return $result;
    }

    /**
     * Same as before but uses a fake patient to be able to send a test email from within PADM
     * @param string $email
     * @param int $practiceId
     * @param int $providerId
     * @return array
     */
    public static function getRRDataForTestEmail($email, $practiceId, $providerId = 0)
    {
        $providerCondition = '';
        $ppField = ', "" AS google_pp_url';
        if ($providerId > 0) {
            $providerCondition = sprintf(
                "
                LEFT JOIN provider_practice_listing AS ppl_google_pp
                    ON practice.id = ppl_google_pp.practice_id
                    AND ppl_google_pp.listing_type_id = %d
                    AND ppl_google_pp.provider_id = %d ",
                ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID,
                $providerId
            );
            $ppField = ", ppl_google_pp.url AS google_pp_url";
        }
        $sql = sprintf(
            "SELECT -1 AS id, %d AS practice_id, 0 AS auto_rr_sent_count,
            'DYNAMIC' AS auto_rr_send_link, '%s' AS patient_email, 'John' AS patient_first_name,
            'Doe' AS patient_last_name, practice.name AS practice_name, practice.logo_path AS logo_path,
            review_request_auto_subject, review_request_auto_body,
            review_request_email_title, review_request_email_body, review_request_sms_body,
            google_review_email_title, google_review_email_body, google_review_sms_body,
            ppl_google.url AS google_url, ppl_yelp.url AS yelp_url
            %s
            FROM practice
            LEFT JOIN practice_details
                ON practice.id = practice_details.practice_id
                /* google practice level */
            LEFT JOIN provider_practice_listing AS ppl_google
                ON practice.id = ppl_google.practice_id
                AND ppl_google.listing_type_id = %d
                AND ppl_google.provider_id IS NULL
                /* yelp at practice level */
            LEFT JOIN provider_practice_listing AS ppl_yelp
                ON practice.id = ppl_yelp.practice_id
                AND ppl_yelp.listing_type_id = %d
                AND ppl_yelp.provider_id IS NULL
            /* GOOGLE at providerAtPractice level p@p */
            %s
            WHERE practice.id = %d;",
            $practiceId,
            addslashes($email),
            $ppField,
            ListingType::GOOGLE_PROFILE_ID,
            ListingType::YELP_PROFILE_ID,
            $providerCondition,
            $practiceId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryRow();
    }

    /**
     * Gets an account's first auto review request date
     * @param int $accountId
     * @return string
     */
    public static function getAccountFirstAutoReviewRequest($accountId)
    {
        $sql = sprintf(
            "SELECT MIN(communication_log.date_added)
            FROM communication_log
            INNER JOIN patientemail_communications
            ON patientemail_communications.communication_id = communication_log.id
            AND patientemail_communications.communication_table = 'communication_log'
            INNER JOIN patientemail
            ON patientemail.id = patientemail_communications.patientemail_id
            WHERE account_id = %d;",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Gets an account's last auto review request date
     * @param int $accountId
     * @return string
     */
    public static function getAccountLastAutoReviewRequest($accountId)
    {
        $sql = sprintf(
            "SELECT MAX(communication_log.date_added)
            FROM communication_log
            INNER JOIN patientemail_communications
            ON patientemail_communications.communication_id = communication_log.id
            AND patientemail_communications.communication_table = 'communication_log'
            INNER JOIN patientemail
            ON patientemail.id = patientemail_communications.patientemail_id
            WHERE account_id = %d;",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Count an account's auto review requests
     * @param int $accountId
     * @return string
     */
    public static function countAccountAutoReviewRequest($accountId)
    {
        $sql = sprintf(
            "SELECT COUNT(1)
            FROM communication_log
            INNER JOIN patientemail_communications
            ON patientemail_communications.communication_id = communication_log.id
            AND patientemail_communications.communication_table = 'communication_log'
            INNER JOIN patientemail
            ON patientemail.id = patientemail_communications.patientemail_id
            WHERE account_id = %d;",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Import a patient email spreadsheet for automated follow-up messages
     * @param array $data
     * @return json
     */
    public static function importPatientemailSpreadsheet($data = array())
    {
        $accountId = $data['accountId'];
        $practiceId = $data['practiceId'];
        $sendTo = $data['sendTo'];
        $accountDeviceId = !empty($data['accountDeviceId']) ? $data['accountDeviceId'] : null;
        $csv = $data['csv'];

        // Check if a file is present
        if (!$csv) {
            return array(
                'success' => false,
                'error' => 'MISSING_FILE',
                'data' => 'CSV/XLS file missing in the request'
            );
        }

        $inputFileName = $csv->getTempName();

        // unfortunately we need this because the class has undefined vars all over the place
        $errorReporting = ini_get('error_reporting');
        ini_set('error_reporting', E_ERROR);

        spl_autoload_unregister(array('YiiBase', 'autoload'));
        Yii::import('ext.phpexcel.Classes.PHPExcel', true);
        spl_autoload_register(array('YiiBase', 'autoload'));

        //  Read your Excel workbook
        try {
            $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
            $objReader = PHPExcel_IOFactory::createReader($inputFileType);
            $objPHPExcel = $objReader->load($inputFileName);
        } catch (Exception $e) {

            return array(
                'success' => false,
                'error' => 'INVALID_FILE',
                'data' => $e->getMessage()
            );
        }

        //  Get worksheet dimensions
        $sheet = $objPHPExcel->getSheet(0);
        $highestRow = $sheet->getHighestRow();
        $highestColumn = $sheet->getHighestColumn();

        $validatedItems = array();

        //  Loop through each row of the worksheet in turn
        for ($row = 1; $row <= $highestRow; $row++) {

            // Set new item
            $item = array(
                'email' => null,
                'first' => null,
                'last' => null,
                'middle' => null
            );

            //  Read a row of data into an array
            $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, null, true, false);

            // Detect structure, one of:
            //      1) Just emails
            //      2) Emails and name
            //      3) Emails and first, last
            //      4) Emails and  first, middle, last
            if (isset($rowData[0][1])) {
                $rowData[0][1] = trim($rowData[0][1]);
            }
            if (isset($rowData[0][2])) {
                $rowData[0][2] = trim($rowData[0][2]);
            }
            if (isset($rowData[0][3])) {
                $rowData[0][3] = trim($rowData[0][3]);
            }

            if (!empty($rowData[0][0])) {

                // ok, first column is email
                $item['email'] = $rowData[0][0];

                if (
                    !empty($rowData[0][3]) && !empty($rowData[0][1]) && strlen($rowData[0][3]) <= 80 &&
                    strlen($rowData[0][1] . ' ' . ($rowData[0][2] ? $rowData[0][2] : '')) <= 50
                ) {
                    // 4) if we have a 4th column then it's the last name, the 3rd is the middle name
                    // and the 2nd the first name
                    $item['last'] = $rowData[0][3];
                    $item['first'] = trim($rowData[0][1]);
                    $item['middle'] = trim($rowData[0][2]);
                } elseif (
                    !empty($rowData[0][2]) && !empty($rowData[0][1]) && strlen($rowData[0][2]) <= 80 &&
                    strlen($rowData[0][2]) <= 50
                ) {
                    // 3) if we have a 3rd column then it's the last name and the 2nd column is the
                    // first name
                    $item['last'] = $rowData[0][2];
                    $item['first'] = $rowData[0][1];
                } elseif (!empty($rowData[0][1]) && strlen($rowData[0][2]) <= 130) {
                    // 2) if we have a 2nd column then it's the first name
                    $item['first'] = trim($rowData[0][1]);
                }

                // Validate email address
                if (!filter_var($item['email'], FILTER_VALIDATE_EMAIL)) {
                    return array(
                        'success' => false,
                        'error' => 'INVALID_EMAIL',
                        'data' => 'The uploaded file has an invalid email (Row: ' . $row . ')'
                    );
                }

                //                // Validate first name
                //                if (empty($item['first'])) {
                //                    return array(
                //                        'success' => false,
                //                        'error' => 'BLANK_FIRST_NAME',
                //                        'data' => 'The uploaded file has a blank first name (Row: ' . $row .')'
                //                    );
                //                }
                //
                //                // Validate last name
                //                if (empty($item['last'])) {
                //                    return array(
                //                        'success' => false,
                //                        'error' => 'BLANK_LAST_NAME',
                //                        'data' => 'The uploaded file has a blank last name (Row: ' . $row .')'
                //                    );
                //                }

                // Add validated item to array
                $validatedItems[] = $item;
            }
        }

        // The uploaded file is not valid
        if (empty($validatedItems)) {
            return array(
                'success' => false,
                'error' => 'EMPTY_FILE',
                'data' => 'The uploaded file is empty or is not correctly formatted'
            );
        }


        $resultData = array();
        $creates = 0;
        $updates = 0;
        $duplicates = 0;

        // Add a new group record
        $patientemailBatch = new PatientemailBatch();
        $patientemailBatch->link_type = $sendTo;
        $patientemailBatch->account_id = $accountId;
        $patientemailBatch->practice_id = $practiceId;
        $patientemailBatch->date_added = date('Y-m-d H:i:s');
        if ($sendTo == 'DDC') {
            $patientemailBatch->account_device_id = $accountDeviceId;
        }
        // If there's a description
        if (!empty($data['description'])) {
            $patientemailBatch->description = $data['description'];
        }
        // If there's a custom title
        if (!empty($data['customTitle'])) {
            $patientemailBatch->custom_title = $data['customTitle'];
        }
        // If there's a custom body
        if (!empty($data['customBody'])) {
            $patientemailBatch->custom_body = $data['customBody'];
        }
        // If there's a custom url (Google, Yelp or Custom)
        if (!empty($data['customUrl']) && $sendTo != 'DDC') {
            $patientemailBatch->custom_link = $data['customUrl'];
        }
        // Template to be used
        if (!empty($data['template'])) {
            $patientemailBatch->template = $data['template'];
        }
        $patientemailBatch->save();

        foreach ($validatedItems as $item) {
            // look up patient to check if this account/practice already had him
            $item['email'] = StringComponent::validateEmail($item['email']);

            $patientemail = Patientemail::model()->find([
                'condition' => 'patient_email=:patient_email AND practice_id=:practice_id AND account_id=:account_id',
                'params' => [
                    ':patient_email' => $item['email'],
                    ':practice_id' => $practiceId,
                    ':account_id' => $accountId
                ],
                'order' => 'date_updated DESC'
            ]);

            // Do we have a record for this email/practice/account?
            if (!empty($patientemail)) {
                // Yes, we found
                $date1 = new DateTime(date("Y-m-d", strtotime($patientemail->date_updated)));
                $date2 = new DateTime(date("Y-m-d"));
                $daysPassed = $date1->diff($date2);

                // When was the last time that we contact this email?
                if ($daysPassed > 90) {
                    // More than 90 days
                    // so we can update and reactivate to send it again
                    $updates++;
                    $patientemail->auto_rr_sent_count = 0;
                    $patientemail->date_added = date('Y-m-d H:i:s');
                    $patientemail->date_updated = date('Y-m-d H:i:s');
                } else {
                    if ($patientemail->auto_rr_sent_count == 0) {
                        $updates++;
                        // Never was sent
                        // reset the updated date so we don't skip patient for 90 days
                        // (see ReviewsCommand->actionSendAutomaticReviewRequests)
                        $patientemail->date_added = date('Y-m-d H:i:s');
                        $patientemail->date_updated = date('Y-m-d H:i:s');
                    } else {
                        // Nop, was sent less than 90 days ago. Tagged as duplicated
                        // skip this record, there's nothing to do
                        $duplicates++;
                        continue;
                    }
                }
            } else {
                // create new patientemail entry
                $creates++;
                $patientemail = new Patientemail();

                $patientemail->patientemail_batch_id = $patientemailBatch->id;
                $patientemail->account_id = $accountId;
                $patientemail->practice_id = $practiceId;
                $patientemail->patient_email = $item['email'];
                $patientemail->date_added = date('Y-m-d H:i:s');
                $patientemail->date_updated = date('Y-m-d H:i:s');
                $patientemail->auto_rr_sent_count = 0;
            }

            $patientemail->auto_rr_send_link = $sendTo;
            $patientemail->patient_first_name = $item['first'];
            $patientemail->patient_middle_name = $item['middle'];
            $patientemail->patient_last_name = $item['last'];
            $patientemail->account_device_id = null;
            if ($sendTo == 'DDC') {
                $patientemail->account_device_id = $accountDeviceId;
            }
            $patientemail->save();
            $resultData[] = $patientemail->attributes;
        }

        // restore error reporting level
        ini_set('error_reporting', $errorReporting);

        return array(
            'success' => true,
            'error' => false,
            'data' => array(
                'patientemail' => $resultData,
                'creates' => ($creates + $updates),
                'updates' => $duplicates
            )
        );
    }

    /**
     * Get stats for a follow up RR
     *
     * @param int $accountId
     * @return array
     */
    public static function getMessagingStatsFollowUp($accountId = 0)
    {

        if (empty($accountId)) {
            return false;
        }

        $allAcountId = OrganizationAccount::getAllAccountId($accountId);
        $allAcountId = implode(', ', $allAcountId);

        if (empty($allAcountId) || trim($allAcountId) == ',') {
            return false;
        }

        $sql = sprintf(
            "SELECT COUNT(DISTINCT(pr.id)) AS total
            FROM provider_rating AS pr
            INNER JOIN communication_description AS cd
            ON reference_id = pr.id
            AND reference_table = 'provider_rating'
            WHERE pr.account_id IN (%s);",
            $allAcountId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get messaging stats for a Manual Review Request
     *
     * @param int $accountId
     * @param string $type  review_request
     * @return array
     */
    public static function getMessagingStatsManual($accountId = 0)
    {
        $result = [];
        $arrRR = ReviewRequest::getAccountReviewRequests($accountId, 'review_request');

        if (empty($arrRR)) {
            return $result;
        }

        foreach ($arrRR as $value) {
            $practiceId = $value['practice_id'];
            $clicked = $value['clicked'];
            $sendVia = !empty($value['send_via']) ? $value['send_via'] : 'n/a';
            $sendLink = !empty($value['send_link']) ? $value['send_link'] : 'n/a';

            // Initialize 'totalized' entry
            if (!isset($result['totalized']['total'])) {
                $result['totalized']['total'] = 0;
            }
            if (!isset($result['totalized']['clicked'][$clicked])) {
                $result['totalized']['clicked'][$clicked] = 0;
            }
            if (!isset($result['totalized']['send_via'][$sendVia])) {
                $result['totalized']['send_via'][$sendVia] = 0;
            }
            if (!isset($result['totalized']['send_link'][$sendLink])) {
                $result['totalized']['send_link'][$sendLink] = 0;
            }

            // Initialize practice´s entries
            if (!isset($result[$practiceId]['total'])) {
                $result[$practiceId]['total'] = 0;
            }
            if (!isset($result[$practiceId]['clicked'][$clicked])) {
                $result[$practiceId]['clicked'][$clicked] = 0;
            }
            if (!isset($result[$practiceId]['send_via'][$sendVia])) {
                $result[$practiceId]['send_via'][$sendVia] = 0;
            }
            if (!isset($result[$practiceId]['send_link'][$sendLink])) {
                $result[$practiceId]['send_link'][$sendLink] = 0;
            }

            $result[$practiceId]['details'][] = $value;
            $result[$practiceId]['total']++;
            $result[$practiceId]['clicked'][$clicked]++;
            $result[$practiceId]['send_via'][$sendVia]++;
            $result[$practiceId]['send_link'][$sendLink]++;

            // Total Stats
            $result['totalized']['total']++;
            $result['totalized']['clicked'][$clicked]++;
            $result['totalized']['send_via'][$sendVia]++;
            $result['totalized']['send_link'][$sendLink]++;
        }
        return $result;
    }

    /**
     * Get messaging stats for a given practice
     *
     * @deprecated as only used by PADM1
     *
     * @param int $practiceId
     * @param string $listAllAccounts
     * @return array
     */
    public static function getMessagingStatsAutomatic($practiceId = 0, $listAllAccounts = '')
    {
        $emailList = Patientemail::getAutomaticRREmails($practiceId, $listAllAccounts, false);

        // get count of status messages
        $status = array(
            'click' => 0,
            'hard_bounce' => 0,
            'open' => 0,
            'send' => 0,
            'soft_bounce' => 0,
            'reject' => 0,
            'deferral' => 0,
            'spam' => 0,
        );

        // group by link type
        $linkTypes = array('GOOGLE' => 0, 'YELP' => 0, 'DDC' => 0, 'CUSTOM' => 0, 'N/A' => 0,);

        // calculate stats for total | sent | pending
        $emailTotal = 0;  // Total of the emails
        $emailSent = 0; // Total of sent emails
        $emailPending = 0;   // Total of pending emails

        if (!empty($emailList)) {
            foreach ($emailList as $value) {
                $emailTotal++;
                if ($value['auto_rr_sent_count'] > 0) {
                    $emailSent++;
                } else {
                    $emailPending++;
                }
                // If the status key was not predefined, we need to added
                if (!isset($status[$value['message_status']])) {
                    $status[$value['message_status']] = 0;
                }
                $status[$value['message_status']]++;

                // get LinkTypes
                if (
                    trim($value['auto_rr_send_link']) == 'GOOGLE'
                    || strpos($value['auto_rr_send_link_detail'], 'google') !== false
                ) {
                    $linkTypes['GOOGLE']++;
                } elseif (
                    trim($value['auto_rr_send_link']) == 'YELP'
                    || strpos($value['auto_rr_send_link_detail'], 'yelp') !== false
                ) {
                    $linkTypes['YELP']++;
                } elseif (
                    trim($value['auto_rr_send_link']) == 'DDC'
                    || strpos($value['auto_rr_send_link_detail'], 'ddc') !== false
                ) {
                    $linkTypes['DDC']++;
                } elseif (
                    trim($value['auto_rr_send_link']) == 'CUSTOM'
                    || strpos($value['auto_rr_send_link_detail'], 'custom') !== false
                ) {
                    $linkTypes['CUSTOM']++;
                } else {
                    $linkTypes['N/A']++;
                }
            }
        }

        $linkTypes['totalLinkTypes'] = round($emailTotal / 100, 2);
        if ($linkTypes['totalLinkTypes'] > 0) {
            $linkTypes['percentGOOGLE'] = round($linkTypes['GOOGLE'] / $linkTypes['totalLinkTypes'], 1);
            $linkTypes['percentYELP'] = round($linkTypes['YELP'] / $linkTypes['totalLinkTypes'], 1);
            $linkTypes['percentDDC'] = round($linkTypes['DDC'] / $linkTypes['totalLinkTypes'], 1);
            $linkTypes['percentCUSTOM'] = round($linkTypes['CUSTOM'] / $linkTypes['totalLinkTypes'], 1);
            $linkTypes['percentN/A'] = round($linkTypes['N/A'] / $linkTypes['totalLinkTypes'], 1);
        }

        // how many emails do we send every day?
        $practiceDetails = PracticeDetails::getDetails($practiceId, false);
        // calculate when we'll be done
        if (!$practiceDetails || empty($practiceDetails->review_request_auto_daily_limit)) {
            if (!$practiceDetails) {
                $practiceDetails = new PracticeDetails();
                $practiceDetails->practice_id = $practiceId;
            }
            // default is 100
            $practiceDetails->review_request_auto_daily_limit = 100;
            $practiceDetails->save();
        }
        $eta = 0;
        if ((int) $practiceDetails->review_request_auto_daily_limit == 0) {
            // will never be done
            $eta = '-1';
        } elseif ($emailPending > 0) {
            // use settings to calculate
            $eta = ceil($emailPending / $practiceDetails->review_request_auto_daily_limit);
        }

        // get queue
        $criteria = new CDbCriteria();
        $criteria->condition = sprintf('practice_id=%d AND account_id IN (%s)', $practiceId, $listAllAccounts);
        $criteria->order = "id DESC";
        $queued = Patientemail::model()->cache(Yii::app()->params['cache_medium'])->findAll($criteria);

        $queueDetails = array();
        foreach ($queued as $patientemail) {
            $queueDetails[] = $patientemail->attributes;
        }

        return array(
            'patient_emailed' => $emailSent,
            'patient_emails' => $emailTotal,
            'patient_emails_pending' => $emailPending,
            'status' => !empty($status) ? $status : array(),
            'eta' => !empty($eta) ? $eta : 0,
            'queue' => !empty($queueDetails) ? $queueDetails : array(),
            'review_request_auto_daily_limit' => $practiceDetails->review_request_auto_daily_limit,
            'link_types' => !empty($linkTypes) ? $linkTypes : array()
        );
    }

    /**
     * Get messaging stats for practices in a given account
     * In use by PADM2
     *
     * @param array $arrPractices
     * @param string $strAccounts
     * @param bool $useCache
     * @return array
     */
    public static function getMessagingStatsARR($arrPractices = null, $strAccounts = '', $useCache = true)
    {

        // return empty by default
        if (empty($arrPractices) || empty($strAccounts)) {
            return array();
        }
        $cacheTime = ($useCache ? Yii::app()->params['cache_medium'] : 0);

        // default, per-practice return format:
        $arrStats = array(
            'click' => 0,
            'hard_bounce' => 0,
            'open' => 0,
            'send' => 0,
            'soft_bounce' => 0,
            'reject' => 0,
            'deferral' => 0,
            'spam' => 0,
            'emailed' => 0,
        );

        // link types
        $linkTypeNames = ['GOOGLE', 'YELP', 'DDC', 'CUSTOM', 'N/A'];

        // build list of practices as string
        $arrPracticeIds = array();
        foreach ($arrPractices as $practice) {
            $arrPracticeIds[] = $practice['practice_id'];
        }
        $strPractices = implode(', ', $arrPracticeIds);
        unset($arrPractices);

        if (empty($strPractices)) {
            return array();
        }

        // get queue
        $sql = sprintf(
            "SELECT pe.*,
            IF (cl.message_status IS NULL, 'send', cl.message_status) AS message_status,
            IF (practice_details.review_request_auto_daily_limit IS NOT NULL,
            practice_details.review_request_auto_daily_limit, 100) AS review_request_auto_daily_limit,
            IF (auto_rr_send_link = 'GOOGLE' || INSTR(auto_rr_send_link_detail, 'google') > 0, 'GOOGLE',
                IF (auto_rr_send_link = 'YELP' || INSTR(auto_rr_send_link_detail, 'yelp') > 0, 'YELP',
                    IF (auto_rr_send_link = 'DDC' || INSTR(auto_rr_send_link_detail, 'doctor.com') > 0, 'DDC',
                        IF (auto_rr_send_link = 'CUSTOM', 'CUSTOM', 'N/A')
                    )
                )
                ) AS link_type, practice.name AS practice_name, practice.address AS practice_address,
            practice.address_2 AS practice_address_2, practice.phone_number
            FROM patientemail AS pe
            INNER JOIN practice
            ON pe.practice_id = practice.id
            LEFT JOIN practice_details
            ON pe.practice_id = practice_details.practice_id
            LEFT JOIN patientemail_communications AS pc
            ON pe.id = pc.patientemail_id
            LEFT JOIN communication_log AS cl
            ON pc.communication_id = cl.id
            AND pc.communication_table = 'communication_log'
            WHERE account_id IN (%s)
            AND pe.practice_id IN (%s)
            GROUP BY pe.id
            ORDER BY pe.id DESC;",
            $strAccounts,
            $strPractices
        );
        $queueDetails = Yii::app()->db
            ->cache($cacheTime)
            ->createCommand($sql)
            ->queryAll();

        if (empty($queueDetails)) {
            return array();
        }

        $arrPractices['queue'] = $queueDetails;

        // walk through results building practice-indexed totals
        $arrPractices['totalized']['uploaded'] = 0;
        $arrPractices['totalized']['emailed'] = 0;

        foreach ($queueDetails as $stats) {

            // initialize status
            if (empty($arrPractices[$stats['practice_id']])) {
                $arrPractices[$stats['practice_id']]['status'] = $arrStats;
            }

            // count in status
            if (empty($arrPractices[$stats['practice_id']]['status'][$stats['message_status']])) {
                $arrPractices[$stats['practice_id']]['status'][$stats['message_status']] = 0;
            }
            $arrPractices[$stats['practice_id']]['status'][$stats['message_status']]++;

            // count into totals, sent or pending
            if (empty($arrPractices[$stats['practice_id']]['status']['uploaded'])) {
                $arrPractices[$stats['practice_id']]['status']['uploaded'] = 0;
            }
            $arrPractices[$stats['practice_id']]['status']['uploaded']++;

            // add to totals as well
            $arrPractices['totalized']['uploaded']++;

            if ($stats['auto_rr_sent_count'] > 0) {
                // sent
                if (empty($arrPractices[$stats['practice_id']]['status']['emailed'])) {
                    $arrPractices[$stats['practice_id']]['status']['emailed'] = 0;
                }
                $arrPractices[$stats['practice_id']]['status']['emailed']++;

                // add to totals as well
                $arrPractices['totalized']['emailed']++;
            } else {
                // pending
                if (empty($arrPractices[$stats['practice_id']]['status']['patient_emails_pending'])) {
                    $arrPractices[$stats['practice_id']]['status']['patient_emails_pending'] = 0;
                }
                $arrPractices[$stats['practice_id']]['status']['patient_emails_pending']++;
            }

            // keep practice details
            $arrPractices[$stats['practice_id']]['review_request_auto_daily_limit'] =
                $stats['review_request_auto_daily_limit'];
            $arrPractices[$stats['practice_id']]['practice_name'] = $stats['practice_name'];
            $arrPractices[$stats['practice_id']]['practice_address'] = $stats['practice_address'];
            $arrPractices[$stats['practice_id']]['practice_address_2'] = $stats['practice_address_2'];
            $arrPractices[$stats['practice_id']]['phone_number'] = $stats['phone_number'];

            // count link types
            if (empty($arrPractices[$stats['practice_id']]['link_types'][$stats['link_type']])) {
                $arrPractices[$stats['practice_id']]['link_types'][$stats['link_type']] = 0;
            }
            $arrPractices[$stats['practice_id']]['link_types'][$stats['link_type']]++;

            // calculate percentages constantly
            $arrPractices[$stats['practice_id']]['link_types']['totalLinkTypes'] =
                round($arrPractices[$stats['practice_id']]['status']['uploaded'] / 100, 2);
            if ($arrPractices[$stats['practice_id']]['link_types'] > 0) {
                foreach ($linkTypeNames as $linkTypeName) {
                    if (empty($arrPractices[$stats['practice_id']]['link_types'][$linkTypeName])) {
                        $arrPractices[$stats['practice_id']]['link_types'][$linkTypeName] = 0;
                    }
                    $arrPractices[$stats['practice_id']]['link_types']['percent' . $linkTypeName] =
                        round(
                            $arrPractices[$stats['practice_id']]['link_types'][$linkTypeName]
                            /
                            $arrPractices[$stats['practice_id']]['link_types']['totalLinkTypes'],
                            1
                        );
                }
            }

            // calculate eta constantly
            if (empty($arrPractices[$stats['practice_id']]['status']['patient_emails_pending'])) {
                $arrPractices[$stats['practice_id']]['status']['patient_emails_pending'] = 0;
            }
            $arrPractices[$stats['practice_id']]['status']['eta'] =
                ceil($arrPractices[$stats['practice_id']]['status']['patient_emails_pending']
                /
                $stats['review_request_auto_daily_limit']);

            // add message to queue
            unset($stats["message_status"]);
            unset($stats["review_request_auto_daily_limit"]);
            unset($stats["link_type"]);
            unset($stats["practice_name"]);
            unset($stats["practice_address"]);
            unset($stats["practice_address_2"]);
            unset($stats["phone_number"]);
            $arrPractices[$stats['practice_id']]['queue'][] = $stats;
        }

        unset($arrPractices['queue']);

        return $arrPractices;
    }

    /**
     * get Status
     */
    public function getStatus()
    {
        if ($this->auto_rr_sent_count > 0) {
            return 'SUCCESS';
        }
        return 'PENDING';
    }
}
