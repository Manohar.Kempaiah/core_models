<?php

Yii::import('application.modules.core_models.models._base.BasePartnerStatsReport');

class PartnerStatsReport extends BasePartnerStatsReport
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Save the model in the core doctor DB
     * @param json $pSReport
     * @return bool
     */
    public static function saveInCore($pSReport)
    {

        foreach ($pSReport as $report) {

            $sql = sprintf("SELECT count(*) AS psr FROM `partner_stats_report` WHERE id = '%d'; ", $report['id']);
            $psr = Yii::app()->db->createCommand($sql)->queryScalar();
            if ($psr > 0) {
                continue;
            }

            $sql = sprintf(
                "INSERT INTO `partner_stats_report`
                (`qualified_calls`, `appointment_requests`, `provider_ratings`, `providers`,
                `featured_providers`, `provider_times_logged_in`, `docpoints`, `accounts_created`,
                `partner_site_id`, `partner_site`, `date`, `id`)
                VALUES
                ('%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%d', '%s', '%s', '%d')",
                $report['qualified_calls'],
                $report['appointment_requests'],
                $report['provider_ratings'],
                $report['providers'],
                $report['featured_providers'],
                $report['provider_times_logged_in'],
                $report['docpoints'],
                $report['accounts_created'],
                $report['partner_site_id'],
                $report['partner_site'],
                $report['date'],
                $report['id']
            );

            Yii::app()->db->createCommand($sql)->execute();
        }

        return true;
    }

    /**
     * Calculate and save the total values for the report entry
     */
    public function calculate()
    {

        $this->qualified_calls = $this->getQualifiedCalls();
        $this->appointment_requests = $this->getAppointmentRequests();
        $this->provider_ratings = $this->getProviderRatings();
        $this->providers = $this->getProviders();
        $this->featured_providers = $this->getFeaturedProviders();
        $this->provider_times_logged_in = $this->getTimesLoggedIn();
        $this->docpoints = $this->getDocPoints();
        $this->accounts_created = $this->getAccountsCreated();
    }

    /**
     * Get the number of qualified calls from the partner site in the day
     * @return int
     */
    public function getQualifiedCalls()
    {
        $sql = sprintf(
            "SELECT COUNT(*) AS total
                FROM twilio_log tl
                INNER JOIN twilio_partner tp ON tl.twilio_partner_id = tp.id
                WHERE tp.partner_site_id = %d AND
                DATE_FORMAT(tl.time_started, '%%Y-%%m-%%d') = '%s';",
            $this->partner_site_id,
            $this->date
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the number of appointment requests from the partner site in the day
     * @return int
     */
    public function getAppointmentRequests()
    {
        $sql = sprintf(
            "SELECT COUNT(*) AS total
                FROM scheduling_mail
                WHERE partner_site_id = %d AND
                DATE_FORMAT(date_added, '%%Y-%%m-%%d') = '%s'",
            $this->partner_site_id,
            $this->date
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the number of reviews from the partner site in the day
     * @return int
     */
    public function getProviderRatings()
    {
        $sql = sprintf(
            "SELECT COUNT(*) as total
                FROM provider_rating
                WHERE partner_site_id = %d AND
                DATE_FORMAT(date_added, '%%Y-%%m-%%d') = '%s'",
            $this->partner_site_id,
            $this->date
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the number of providers sent to the partner site in the day
     * @return int
     */
    public function getProviders()
    {

        $providersQuery = $this->getProvidersQuery();

        if ($providersQuery == "") {
            return 0;
        }

        $sql = sprintf(
            "SELECT COUNT(*) as total FROM (%s) AS providers;",
            $providersQuery
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the number of featured providers sent to the partner site in the day
     * @return int
     */
    public function getFeaturedProviders()
    {

        $providersQuery = $this->getProvidersQuery();

        if ($providersQuery == "") {
            return 0;
        }

        $sql = sprintf(
            "SELECT COUNT(*) as total FROM (%s) AS provider
                WHERE provider.featured = 1;",
            $providersQuery
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the number of providers with accounts from the partner site that
     * accessed the PADM during the day
     * @return int
     */
    public function getTimesLoggedIn()
    {
        $sql = sprintf(
            "SELECT COUNT(*) as total
                FROM account a
                WHERE a.partner_site_id = %d AND
                DATE_FORMAT(a.date_signed_in, '%%Y-%%m-%%d') = '%s'",
            $this->partner_site_id,
            $this->date
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the sum of doc points of the providers sent to the partner site
     * in the given day
     * @return int
     */
    public function getDocPoints()
    {
        $providersQuery = $this->getProvidersQuery();

        if ($providersQuery == "") {
            return 0;
        }

        $sql = sprintf(
            "SELECT SUM(provider.docscore) as total
                FROM provider
                WHERE provider.id IN (SELECT id FROM (%s) as sent_provider);",
            $providersQuery
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the number of accounts created from the partner site during the day
     * @return int
     */
    public function getAccountsCreated()
    {
        $sql = sprintf(
            "SELECT COUNT(*) as total
            FROM account
            WHERE partner_site_id = %d AND
            DATE_FORMAT(date_added, '%%Y-%%m-%%d') = '%s'",
            $this->partner_site_id,
            $this->date
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the dump query for the provider entity in the given partner site
     * @return string
     */
    protected function getProvidersQuery()
    {
        // Look for the partner site entity
        $pse = PartnerSiteEntity::model()->find(
            "(name = :tmp_provider_stats)
            AND partner_site_id = :partner_site_id",
            array(
                ":partner_site_id" => $this->partner_site_id,
                ":tmp_provider_stats" => "tmp_provider_stats"
            )
        );

        // If no entity is found, return an empty string
        if (!isset($pse)) {
            return "";
        }

        // Else, return the query stripping the semicolons defined
        return str_replace(";", "", $pse->dump_query);
    }
}
