<?php

Yii::import('application.modules.core_models.models._base.BaseOrganizationDefaultFeature');

// autoload vendors class
Common::autoloadVendors();

class OrganizationDefaultFeature extends BaseOrganizationDefaultFeature
{

    public static function model($className = __class__)
    {
        return parent::model($className);
    }


    public function byOrganizationId($organizationId)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->getTableAlias(false) . '.organization_id = :organization_id',
            'params' => [':organization_id' => $organizationId],
        ]);
        return $this;
    }

    public function byFeatureId($featureId)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->getTableAlias(false) . '.feature_id = :feature_id',
            'params' => [':feature_id' => $featureId],
        ]);
        return $this;
    }

    public function byLevel($level)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->getTableAlias(false) . '.lavel = :level',
            'params' => [':level' => $level],
        ]);
        return $this;
    }


    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);

        if (!intval($this->organization_id) && is_string($this->organization_id) && strlen($this->organization_id) > 0) {
            $criteria->with[] = 'organization';
            $criteria->addSearchCondition("organization.organization_name", $this->organization_id, true);
        } else {
            $criteria->compare('organization_id', $this->organization_id);
        }

        if (!intval($this->feature_id) && is_string($this->feature_id) && strlen($this->feature_id) > 0) {
            $criteria->with[] = 'feature';
            $criteria->addSearchCondition("feature.name", $this->feature_id, true);
        } else {
            $criteria->compare('feature_id', $this->feature_id);
        }
        $criteria->compare('level', $this->level);
        $criteria->compare('active', $this->active);
        $criteria->compare('t.date_added', $this->date_added, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->order = 't.date_added DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 20)
        ));
    }

    /**
     * Update SalesForce if necessary:
     *
     * Number_of_Locations__c
     * Number_of_Providers__c
     * Number_of_ReviewHubs__c
     * Number_of_SiteEnhance_Mobiles__c
     * Number_of_Showcase_Pages__c
     * AdWords__c
     * Number_of_ProfilePromotes__c
     * Number_of_ProfileSyncs__c
     * Promoted_Provider_Names__c
     * ProfileSync_Provider_Names__c
     * Number_of_ProfilePromotes_Healthgrades__c
     * Number_of_Enhanced_Healthgrades__c
     * Number_of_ProfilePromotes_Vitals__c
     * Promoted_Healthgrades_Provider_Names__c
     * Enhanced_Healthgrades_Provider_Names__c
     * Promoted_Vitals_Provider_Names__c
     * VirtualVisit_Enabled__c
     *
     * @return mixed
     */
    public function beforeSave()
    {

        $statusChanged = false;

        // this is for prevent feature activation in canceled organizations
        // if the feature is active, first check if the org is not canceled
        if ($this->organization_id && $this->feature_id && $this->active) {
            $organization = Organization::model()->findByPk($this->organization_id);

            // the org is canceled so the feature can't be active
            if ($organization->status == 'C') {
                $this->addError('organization_id', 'Could not add or activate a feature in canceled organizations.');
                return false;
            }
        }

        if (!$this->isNewRecord) {
            // on update, check if active changed
            if (!empty($this->oldRecord->active) && $this->oldRecord->active != $this->active) {
                $statusChanged = true;
            }
        }

        if ($this->organization_id > 0 && $this->feature_id > 0 && ($this->isNewRecord || $statusChanged)) {

            if (
                $this->feature_id == 2 || $this->feature_id == 4 || $this->feature_id == 6  || $this->feature_id == 8 ||
                $this->feature_id == 9 || $this->feature_id == 10 || $this->feature_id == 14 || $this->feature_id == 15
                || $this->feature_id == 16 || $this->feature_id == 20
            ) {
                SqsComponent::sendMessage('updateSFOrganizationFeatures', $this->organization_id);
            }
        }

        return parent::beforeSave();
    }

}
