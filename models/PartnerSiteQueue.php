<?php
Yii::import('application.modules.core_models.models._base.BasePartnerSiteQueue');

class PartnerSiteQueue extends BasePartnerSiteQueue
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Find pending batches
     *
     * @param int $partnerSiteId
     * @return array
     */
    public static function getQueue($partnerSiteId)
    {
        $sql = sprintf(
            'SELECT DISTINCT external_id AS external_id
            FROM partner_site_queue
            WHERE partner_site_id = %d
            AND completed = 0;',
            $partnerSiteId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }
}
