<?php

Yii::import('application.modules.core_models.models._base.BaseScanCompetitorProvider');

class ScanCompetitorProvider extends BaseScanCompetitorProvider
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get all the related providers with joined relationships.
     *
     * @param string $placeId
     * @param string|integer $scanCompetitorPracticeId
     *
     * @return ScanCompetitorProvider[]
     */
    public function getByPlaceAndScpIdsJoined($placeId, $scanCompetitorPracticeId)
    {
        return ScanCompetitorProvider::model()
            ->with('scanCompetitorPractice')
            ->with('provider')
            ->with('provider.credential')
            ->findAll(
                'scanCompetitorPractice.place_id = :place_id AND t.scan_competitor_practice_id = :scp_id',
                [':place_id' => $placeId, ':scp_id' => $scanCompetitorPracticeId]
            );
    }

    /**
     * Get all the related providers.
     * The only joined relationship is ScanCompetitorPractice
     *
     * @param string $placeId
     * @param string|integer $scanCompetitorPracticeId
     *
     * @return ScanCompetitorProvider[]
     */
    public function getByPlaceAndScpId($placeId, $scanCompetitorPracticeId)
    {
        return ScanCompetitorProvider::model()
            ->with('scanCompetitorPractice')
            ->findAll(
                'scanCompetitorPractice.place_id = :place_id AND t.scan_competitor_practice_id = :scp_id',
                [':place_id' => $placeId, ':scp_id' => $scanCompetitorPracticeId]
            );
    }

    public function getLatestByProviderAndPlace($providerId, $placeId)
    {
        $criteria = new CDbCriteria();

        $criteria->with[] = 'scanCompetitorPractice';

        $criteria->compare('t.provider_id', $providerId);
        $criteria->compare('scanCompetitorPractice.place_id', $placeId);
        $criteria->order = 't.date_added DESC';

        return ScanCompetitorProvider::model()->find($criteria);
    }

    public function getLatestProviderByPlace($placeId)
    {
        $criteria = new CDbCriteria();

        $criteria->with[] = 'scanCompetitorPractice';
        $criteria->with[] = 'provider';

        $criteria->compare('scanCompetitorPractice.place_id', $placeId);
        $criteria->order = 't.date_added DESC';

        $result = $this->find($criteria);

        if ($result) {
            return $result->provider;
        } else {
            return null;
        }
    }

    public function getLatestFromPlaceId($placeId)
    {
        $criteria = new CDbCriteria();

        $criteria->with[] = 'scanCompetitorPractice';
        $criteria->with[] = 'provider';

        $criteria->compare('scanCompetitorPractice.place_id', $placeId);
        $criteria->order = 't.date_added DESC';

        return $this->findAll($criteria);
    }

    public function getByProvider($providerId)
    {
        $criteria = new CDbCriteria();

        $criteria->with[] = 'scanCompetitorPractice';
        $criteria->with[] = 'provider';

        $criteria->compare('t.provider_id', $providerId);
        $criteria->order = 't.date_added DESC';

        return $this->findAll($criteria);
    }
}
