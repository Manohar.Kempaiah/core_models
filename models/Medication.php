<?php

Yii::import('application.modules.core_models.models._base.BaseMedication');

class Medication extends BaseMedication
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        return array(
            'medicationManufacturers' => array(self::HAS_MANY, 'MedicationManufacturer', 'medication_id'),
            'providerMedications' => array(self::HAS_MANY, 'ProviderMedication', 'medication_id'),
            'specialtyMedications' => array(self::HAS_MANY, 'SpecialtyMedication', 'medication_id'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('ndc', $this->ndc, true);
        $criteria->compare('nonpropietary_name', $this->nonpropietary_name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('ingredients', $this->ingredients, true);
        $criteria->compare('pharmaceutical_class', $this->pharmaceutical_class, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100),
        ));
    }

    /**
     * @return string get medication name.
     */
    public function getFullMedication()
    {

        $medication = Medication::model()->findByPk($this->id);
        return addslashes($medication->name);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from mediction if associated records exist in provider_medication table
        $has_associated_records = ProviderMedication::model()->exists(
            'medication_id=:medication_id',
            array(':medication_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Medication can't be deleted because there are providers linked to it.");
            return false;
        }

        // avoid deleting from medication if associated records exist in medication_manufacturer table
        $has_associated_records = MedicationManufacturer::model()->exists(
            'medication_id=:medication_id',
            array(':medication_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Medication can't be deleted because there are providers linked to it.");
            return false;
        }

        // delete records from specialty_medication before delete this medication id
        $arrSpecialtyMedication = SpecialtyMedication::model()->findAll(
            "medication_id = :medication_id",
            array(":medication_id" => $this->id)
        );
        foreach ($arrSpecialtyMedication as $specialtyMedication) {
            if (!$specialtyMedication->delete()) {
                $this->addError("id", "Could not delete linked specialties.");
                return false;
            }
        }

        return parent::beforeDelete();
    }

}
