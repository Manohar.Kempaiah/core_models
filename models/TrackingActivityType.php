<?php

Yii::import('application.modules.core_models.models._base.BaseTrackingActivityType');

class TrackingActivityType extends BaseTrackingActivityType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
