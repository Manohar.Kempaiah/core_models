<?php

Yii::import('application.modules.core_models.models._base.BaseIntegrityCheckLog');

class IntegrityCheckLog extends BaseIntegrityCheckLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
