<?php

Yii::import('application.modules.core_models.models._base.BaseProviderTelemedicineState');


class ProviderTelemedicineState extends BaseProviderTelemedicineState
{

    /**
     * Return the status of the telemedicine
     * @param integer $providerId
     *
     * @return array
     */
    public static function getStatus($providerId = 0)
    {
        if (empty($providerId)) {
            return null;
        }

        // get waiting room (for states)
        $wrs = WaitingRoomSetting::model()->find("provider_id=:providerId", array(":providerId" => $providerId));
        $wrsId = !empty($wrs->id) ? $wrs->id : 0;

        // get provider details
        $providerDetails = ProviderDetails::model()->find(
            "provider_id=:providerId",
            array(":providerId" => $providerId)
        );
        $telemedicineEnabled = 0;
        $telemedicineAcceptNewPatients = 0;
        if (!empty($providerDetails)) {
            $telemedicineEnabled = $providerDetails->telemedicine_enabled;
            $telemedicineAcceptNewPatients = $providerDetails->telemedicine_accept_new_patients;
        }

        $schedule = null;
        // get telemedicine practice
        $providerPractice = ProviderPractice::getTelemedicinePractice($providerId);
        if (!empty($providerPractice['id'])) {
            $schedule = ProviderPracticeSchedule::getSchedule($providerPractice['id']);
        }

        $states = self::getStates($providerId, $wrsId);
        return array(
            "enabled" => ($telemedicineEnabled == 1 ? 1 : 0), // 0 = default, 1 = enabled, 2 = manually disabled
            "accept_new_patients" => $telemedicineAcceptNewPatients,
            "states" => $states,
            "schedule" => (array)$schedule
        );
    }

    /**
     * Return the status of the telemedicine
     * @param array $data
     *  {
     *       "enabled": 0,
     *       "accept_new_patients": 0,
     *       "states": [
     *           {
     *               "state_id": null,
     *           },
     *           ....
     *       ]
     *   },
     *
     * @param array $params
     *  array(
     *      'provider_id' => $providerId;
     *      'account_id' => $this->user()->getAccountId(),
     *      'owner_account_id' => $this->user()->getOwnerAccountId(),
     *  );
     *
     * @return array
     */
    public static function saveData($data = array(), $params = array())
    {

        $providerId = Common::get($params, 'provider_id', null);

        if (empty($providerId) || empty($data)) {
            return null;
        }

        $postStates = Common::get($data, 'states', array());

        // Do we have to update states?
        $pts = ProviderTelemedicineState::model()->findAll("provider_id=:provider_id", [":provider_id" => $providerId]);
        if (!empty($pts)) {
            // check if we need to deleted old records
            foreach ($pts as $v) {
                $exists = false;
                foreach ($postStates as $ps) {
                    if ($ps['state_id'] == $v->state_id) {
                        $exists = true;
                        break;
                    }
                }
                if (!$exists) {
                    $v->delete();
                }
            }

        } else {
            // create an empty obj
            $pts = json_decode(json_encode(array()));
        }

        // check if we have to add the new ones
        foreach ($postStates as $ps) {
            $stateToAdd = $ps['state_id'];
            foreach ($pts as $v) {
                // Exists in the db?
                if ($v->state_id == $stateToAdd) {
                    // yes, exists
                    $stateToAdd = 0;
                    break ;
                }
            }
            if ($stateToAdd > 0) {
                $new = new ProviderTelemedicineState;
                $new->provider_id = $providerId;
                $new->state_id = $stateToAdd;
                $new->save();
            }
        }

        return array(
            'success' => true,
            'error' => false,
            'data' => $data,
        );
    }

    /**
     * Return the states allowed to telemedicine
     * @param integer $providerId
     * @param integer $wsrId
     *
     * @return array
     */
    public static function getStates($providerId = 0, $wsrId = 0)
    {

        // Search in provider_practice
        $sql = sprintf(
            "SELECT state.id as state_id, practice.id AS practice_id, practice.`name` AS practice_name
            FROM provider_practice
                INNER JOIN practice ON provider_practice.practice_id = practice.id
                INNER JOIN location ON practice.location_id = location.id
                INNER JOIN city ON location.city_id = city.id
                INNER JOIN state ON city.state_id = state.id
            WHERE provider_practice.provider_id = %d
            GROUP BY state.id",
            $providerId
        );
        $practices = Yii::app()->db->createCommand($sql)->queryAll();

        // Search in provider_specialty_license
        $sql = sprintf(
            "SELECT pts.id, state.id as state_id, IF (pts.id IS NOT NULL, 1, 0) as selected, state.`name` AS state_name
            FROM state
                LEFT JOIN provider_telemedicine_state as pts
                    ON (pts.state_id = state.id AND pts.provider_id = %d)
            WHERE state.country_id = 1
            GROUP BY state.id
            ORDER BY state_name ASC
            ",
            $providerId
        );
        $states = Yii::app()->db->createCommand($sql)->queryAll();

        if (empty($practices) && empty($states)) {
            return array();
        }

        if ($wsrId > 0) {
            // We a waiting_room_setting, but check if we have any provider_telemedicine_state record
            $existsPts = ProviderTelemedicineState::model()
                ->find('provider_id=:provider_id', [':provider_id' => $providerId]);
        } else {
            $existsPts = true;
        }

        // is the firstTime we access here?
        if ($wsrId == 0 || !$existsPts) {
            // We have to set as selected by default
            foreach ($practices as $p) {
                foreach ($states as $k => $v) {
                    if ($p['state_id'] == $v['state_id']) {
                        $states[$k]['selected'] = 1;
                    }
                }
            }
        }

        return $states;
    }
}
