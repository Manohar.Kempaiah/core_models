<?php

Yii::import('application.modules.core_models.models._base.BaseMedicationManufacturer');

class MedicationManufacturer extends BaseMedicationManufacturer
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'medication_id' => Yii::t('app', 'Medication'),
            'manufacturer_id' => Yii::t('app', 'Manufacturer'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->with = array('manufacturer', 'medication');

        if (!intval($this->medication_id) && is_string($this->medication_id) && strlen($this->medication_id) > 0) {
            $criteria->addSearchCondition("medication.name", $this->medication_id);
        } else {
            $criteria->compare('medication_id', $this->medication_id);
        }
        if (!intval($this->manufacturer_id) && is_string($this->manufacturer_id) && strlen($this->manufacturer_id) > 0) {
            $criteria->addSearchCondition("manufacturer.name", $this->manufacturer_id);
        } else {
            $criteria->compare('manufacturer_id', $this->manufacturer_id);
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100),
        ));
    }

}
