<?php

Yii::import('application.modules.core_models.models._base.BaseTwilioCnam');

class TwilioCnam extends BaseTwilioCnam
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Normalize phone number
     * @return boolean
     */
    public function beforeSave()
    {

        if (!StringComponent::isValidPhoneNumber($this->number)) {
            $this->addError('number', 'Invalid number: ' . $this->number);
            return false;
        }

        // normalize phone number
        $unformattedNumber = $this->number;
        $this->number = StringComponent::normalizePhoneNumber($unformattedNumber);
        if (!$this->number) {
            $this->addError(
                'number',
                'Could not normalize phone number: ' . $unformattedNumber
            );
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * We should trust this cache for 30 days, so if the date the entry
     * was added was 30 days ago or more, we should return false, else true
     */
    public function isExpired()
    {

        if (!$this->date_added) {
            return true;
        }

        // returns true if older than 30 days
        if (!ini_get('date.timezone')) {
            date_default_timezone_set('UTC');
        }
        return (strtotime($this->date_added) < strtotime('-30 days'));
    }

}
