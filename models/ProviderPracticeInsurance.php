<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeInsurance');

class ProviderPracticeInsurance extends BaseProviderPracticeInsurance
{
    // Allow GADM search by ...
    public $provider_npi;
    public $practice_name;
    public $practice_address;
    public $company_name;
    public $insurance_name;
    public $gmb_network_id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Populate table provider_practice_insurance with data from provider_insurance in case they're out of sync
     * (e.g. the first time a provider goes into his control panel)
     * @param int $providerId
     * @return boolean
     */
    public function populateFromProviderInsurance($providerId)
    {

        // have any records in provider_practice_insurance ?
        $sql = sprintf(
            "SELECT id, provider_id, company_id, practice_id, insurance_id, network_status, details, date_added
            FROM provider_practice_insurance
            WHERE provider_id = %d
            ORDER BY company_id ASC, insurance_id ASC;",
            $providerId
        );
        $arrProviderPracticeInsurance = Yii::app()->db->createCommand($sql)->queryAll();

        if (empty($arrProviderPracticeInsurance)) {
            // OK, is the very first time that access here
            $sql = sprintf(
                "SELECT id, provider_id, company_id, insurance_id, date_added
                FROM provider_insurance
                WHERE provider_id = %d
                ORDER BY company_id ASC, insurance_id ASC;",
                $providerId
            );
            $arrProviderInsurance = Yii::app()->db->createCommand($sql)->queryAll();

            if (!empty($arrProviderInsurance)) {

                // Begin transaction
                $transaction = ProviderPracticeInsurance::model()->getDbConnection()->beginTransaction();
                try {

                    foreach ($arrProviderInsurance as $pi) {
                        $companyId = $pi['company_id'];
                        $insuranceId = $pi['insurance_id'];
                        $practiceId = 0;

                        ProviderPracticeInsurance::model()->saveCompanyInsurance(
                            $providerId,
                            $practiceId,
                            $companyId,
                            $insuranceId
                        );
                    }

                    $transaction->commit();
                } catch (Exception $ex) {
                    // If there is an exception, roll back the transaction...
                    $transaction->rollback();
                    throw $ex;
                }
                // End Transaction
            }
        }
        return true;
    }

    /**
     * Check if need to populate provider_insurance to provider_practice_insurance
     * @param int $providerId
     * @return boolean
     */
    public static function firstTimeLoginCheckToPopulate($providerId)
    {

        // have any records in provider_practice_insurance ?
        $arrProviderPracticeInsurance = ProviderPracticeInsurance::model()->findAll(
            array(
                'condition' => 'provider_id=:provider_id',
                'params' => array(':provider_id' => $providerId)
            )
        );

        $arrProviderInsurance = array();
        if (empty($arrProviderPracticeInsurance)) {

            $sql = sprintf(
                "SELECT pi.*, ic.name as company_name "
                . "FROM provider_insurance as pi "
                . "LEFT JOIN insurance_company AS ic ON pi.company_id = ic.id "
                . "WHERE pi.provider_id = %d "
                . "GROUP BY pi.company_id ; ",
                $providerId
            );
            $arrProviderInsurance = Yii::app()->db->createCommand($sql)->queryAll();

        }
        return $arrProviderInsurance;
    }

    /**
     * Add New Company to Provider Practice Insurance
     * @param int $providerId
     * @param int $practiceId
     * @param int $companyId
     * @param int $insuranceId
     * @param str $providerPracticeDetails
     * @param int $accountId
     */
    public function saveCompanyInsurance(
        $providerId = 0,
        $practiceId = 0,
        $companyId = 0,
        $insuranceId = 0,
        $providerPracticeDetails = '',
        $accountId = 0
    ) {

        if ($practiceId > 0) {
            // set only this practice
            $arrProviderPractice[0]['id'] = $practiceId;
        } else {
            // get all practices that this provider is working on
            $arrProviderPractice = ProviderPractice::model()->getProvidersPractices($providerId, $accountId, 'workingOn');
        }

        // now work in provider_practice_insurance
        if ($insuranceId == 0 && $companyId > 0) {
            // get all insurances for this company
            $returnList = true;
            $arrCompanyIds = InsuranceCompany::getSibblingCompanyIds($companyId, $returnList);

            $arrInsurances = Insurance::model()->findByCompanyId($arrCompanyIds);

            foreach ($arrInsurances as $insuranceId) {

                foreach ($arrProviderPractice as $pp) {
                    $newCompanyId = Insurance::model()->findByPk($insuranceId)->company_id;

                    $practiceId = $pp['id'];

                    if ($providerId > 0 && $newCompanyId > 0 && $practiceId > 0 && $insuranceId > 0) {
                        // check if it's actually missing in provider_practice_insurance one last time
                        $piCondition = sprintf(
                            'provider_id = %d AND company_id = %d AND practice_id = %d AND insurance_id = %d ',
                            $providerId,
                            $newCompanyId,
                            $practiceId,
                            $insuranceId
                        );

                        $arrProviderPracticeInsurance = ProviderPracticeInsurance::model()->exists($piCondition);

                        if (!$arrProviderPracticeInsurance) {
                            $arrProviderPracticeInsurance = new ProviderPracticeInsurance();
                            $arrProviderPracticeInsurance->provider_id = $providerId;
                            $arrProviderPracticeInsurance->practice_id = $practiceId;
                            $arrProviderPracticeInsurance->company_id = $newCompanyId;
                            $arrProviderPracticeInsurance->insurance_id = $insuranceId;
                            $arrProviderPracticeInsurance->details = $providerPracticeDetails;
                            if (!$arrProviderPracticeInsurance->save()) {
                                $this->addError("id", "Couldn't save Insurances");
                                return false;
                            }
                        }
                    }
                }
            }

        } else {

            // have specific plan
            foreach ($arrProviderPractice as $pp) {
                $practiceId = $pp['id'];

                // check if it's actually missing in provider_practice_insurance one last time
                $piCondition = sprintf(
                    'provider_id = %d AND company_id = %d AND practice_id = %d AND insurance_id = %d ',
                    $providerId,
                    $companyId,
                    $practiceId,
                    $insuranceId
                );

                $arrProviderPracticeInsurance = ProviderPracticeInsurance::model()->exists($piCondition);
                if (!$arrProviderPracticeInsurance) {
                    $arrProviderPracticeInsurance = new ProviderPracticeInsurance();
                    $arrProviderPracticeInsurance->provider_id = $providerId;
                    $arrProviderPracticeInsurance->practice_id = $practiceId;
                    $arrProviderPracticeInsurance->company_id = $companyId;
                    $arrProviderPracticeInsurance->insurance_id = $insuranceId;
                    $arrProviderPracticeInsurance->details = $providerPracticeDetails;
                    if (!$arrProviderPracticeInsurance->save()) {
                        $this->addError("id", "Couldn't save Insurances.");
                        return false;
                    }
                } else {
                    // save only if details change
                    if (isset($arrProviderPracticeInsurance->details)) {
                        if ($arrProviderPracticeInsurance->details != $providerPracticeDetails) {
                            $arrProviderPracticeInsurance->details = $providerPracticeDetails;
                            if (!$arrProviderPracticeInsurance->save()) {
                                $this->addError("id", "Couldn't update insurances.");
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Remove record from provider_practice_insurance
     * @param int $providerId
     * @param int $practiceId
     * @param int $companyId
     * @param int $insuranceId
     */
    public function deleteCompanyInsurance($providerId = 0, $practiceId = 0, $companyId = 0, $insuranceId = 0)
    {

        // check if exists
        $lstCompanyIds = InsuranceCompany::getSibblingCompanyIds($companyId, true);
        $piCondition = sprintf(
            'provider_id = %d AND company_id IN (%s) AND practice_id = %d AND insurance_id = %d ',
            $providerId,
            $lstCompanyIds,
            $practiceId,
            $insuranceId
        );

        $arrProviderPracticeInsurance = ProviderPracticeInsurance::model()->findAll($piCondition);
        if ($arrProviderPracticeInsurance) {
            foreach ($arrProviderPracticeInsurance as $arrPPi) {
                if (!$arrPPi->delete()) {
                    $this->addError("id", "Couldn't delete insurances.");
                    return false;
                }
            }
        }

        // Are there any other provider - company - insurance?
        $piCondition = sprintf(
            'provider_id = %d AND company_id IN (%s) AND insurance_id = %d ',
            $providerId,
            $lstCompanyIds,
            $insuranceId
        );
        $arrProviderPracticeInsurance = ProviderPracticeInsurance::model()->findAll($piCondition);
        if (!$arrProviderPracticeInsurance) {
            ProviderInsurance::model()->deleteCompanyInsurance($providerId, $companyId, $insuranceId);
        }
    }

    /**
     * Check if this provider-practice-insurance exist for provider
     * Keep consistency between company_id and insurance's company_id
     *
     * @return boolean
     */
    public function beforeSave()
    {
        /*
        if ($this->isNewRecord) {
            if ($this->provider_id > 0 && $this->company_id > 0 && $this->practice_id > 0 && $this->insurance_id > 0) {
                $alreadyExists = ProviderPracticeInsurance::model()->exists(
                    'provider_id=:provider_id AND practice_id=:practice_id AND insurance_id=:insurance_id '
                    . 'AND company_id=:company_id ',
                    array(
                        ':provider_id' => $this->provider_id,
                        ':practice_id' => $this->practice_id,
                        ':insurance_id' => $this->insurance_id,
                        ':company_id' => $this->company_id
                    )
                );
                if ($alreadyExists) {
                    $this->addError("id", "Couldn't update insurances.");
                    return false;
                }
            } else {
                $this->addError("id", "Couldn't update insurances.");
                return false;
            }
        }
        */

        // Force the ProviderInsurance->company_id to be consistent with the Insurance->company_id
        // This needs to be done since the PADM sends only one insurance company,
        // but with many plans that belong to the given company and its siblings.
        $insurance = Insurance::model()->cache(Yii::app()->params['cache_medium'])->findByPk($this->insurance_id);
        if ($insurance && $insurance->company_id != $this->company_id) {
            $this->company_id = $insurance->company_id;
        }

        return parent::beforeSave();
    }

    /**
     * If a new practice-insurance was saved, update practice score.
     * @return bool
     */
    public function afterSave()
    {
        // update practice score for new practice-insurance added
        if ($this->isNewRecord && $this->practice_id && $this->insurance_id) {
            $practice = Practice::model()->findByPk($this->practice_id);
            $practice->updateScore();
        }

        return parent::afterSave();
    }

    /**
     * If a practice-insurance was removed, update practice score.
     * @return bool
     */
    public function afterDelete()
    {
        // update practice score for new practice-insurance added
        if ($this->practice_id) {
            $practice = Practice::model()->findByPk($this->practice_id);
            $practice->updateScore();
        }

        return parent::afterDelete();
    }

    /**
     * Given provider_id, obtain all practices and their insurances
     * @param int $providerId
     * @param string $groupBy -> ( practices || company )
     * @return array
     */
    public static function getProviderPracticeInsurances($providerId = 0, $groupBy = 'practices')
    {

        if ($providerId == 0) {
            return null;
        }

        // get from provider_practice_insurances
        if ($groupBy == 'practices') {

            $sql = sprintf(
                "SELECT SQL_NO_CACHE practice.id AS practice_id, practice.name AS practice_name,
                insurance_company.id AS company_id, insurance_company.name AS company,
                insurance_id, insurance.name AS plan, details, '' AS accepted_at
                FROM provider_practice_insurance
                INNER JOIN practice
                ON provider_practice_insurance.practice_id = practice.id
                INNER JOIN insurance_company
                ON provider_practice_insurance.company_id = insurance_company.id
                LEFT JOIN insurance
                ON ( provider_practice_insurance.insurance_id = insurance.id
                    OR ( provider_practice_insurance.company_id = insurance.company_id
                        AND provider_practice_insurance.insurance_id = 0
                        )
                    )
                WHERE provider_id = '%s'
                ORDER BY practice_name, practice_id, priority DESC, company, plan;",
                $providerId
            );
        } else {

            $sql = sprintf(
                "SELECT SQL_NO_CACHE 0 AS practice_id, 'all' AS practice_name,
                MIN(ic2.id) AS company_id, ic2.name AS company,
                insurance_id, insurance.name AS plan, details, GROUP_CONCAT(practice.name SEPARATOR ', ') AS accepted_at
                FROM provider_practice_insurance
                INNER JOIN practice
                ON provider_practice_insurance.practice_id = practice.id
                INNER JOIN insurance_company ic1 ON provider_practice_insurance.company_id = ic1.id
                INNER JOIN  insurance_company ic2 ON ic1.name = ic2.name
                INNER JOIN insurance
                ON provider_practice_insurance.insurance_id = insurance.id
                WHERE provider_id = '%s'
                GROUP BY ic1.name, insurance_id
                ORDER BY ic1.priority DESC, company, plan;",
                $providerId
            );
        }
        $tmpInsurances = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();

        if (!$tmpInsurances) {
            // get from provider_insurance
            $sql = sprintf(
                "SELECT SQL_NO_CACHE 0 AS practice_id, 'All' AS practice_name,
                MIN(pi.id) AS company_id, ic.name AS company,
                0 AS insurance_id, 'All' AS plan, 'Details' AS details, '' AS accepted_at
                FROM provider_insurance AS pi
                INNER JOIN insurance_company AS ic
                ON pi.company_id = ic.id
                WHERE pi.provider_id = '%s'
                GROUP BY company
                ORDER BY ic.priority DESC, company, plan;",
                $providerId
            );
            $tmpInsurances = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
        }

        if ($tmpInsurances) {
            $arrInsurances = array();
            foreach ($tmpInsurances as $eachOne) {
                $practice_id = $eachOne['practice_id'] . '|' . $eachOne['practice_name'];
                $company_id = $eachOne['company_id'];
                $company = $eachOne['company'];
                $insurance['insurance_id'] = $eachOne['insurance_id'];
                $insurance['plan'] = $eachOne['plan'];
                $insurance['details'] = $eachOne['details'];
                $insurance['accepted_at'] = $eachOne['accepted_at'];
                $arrInsurances[$practice_id][$company_id]['company_id'] = $company_id;
                $arrInsurances[$practice_id][$company_id]['company'] = $company;
                $arrInsurances[$practice_id][$company_id]['insurances'][$eachOne['insurance_id']] = $insurance;
            }
            return $arrInsurances;
        }
        return null;
    }

    /**
     * Duplicate provider_insurances for new practice
     * @param int $providerId
     * @param int $practiceId
     */
    public function populateNewPractice($providerId = 0, $practiceId = 0)
    {

        if ($providerId == 0 || $practiceId == 0) {
            return;
        }

        // Checking if this provider works in this practice
        $existsProviderPractice = ProviderPractice::model()->exists(
            'provider_id=:provider_id AND practice_id=:practice_id ',
            array(':provider_id' => $providerId, ':practice_id' => $practiceId)
        );
        if (!$existsProviderPractice) {
            return;
        }

        // Get all company/insurance for this provider
        $sql = sprintf(
            "SELECT ppi.provider_id, ppi.company_id, ppi.insurance_id
            FROM provider_practice_insurance AS ppi
            WHERE ppi.provider_id = '%s'
            GROUP BY ppi.company_id, ppi.insurance_id",
            $providerId
        );
        $arrProviderInsurances = Yii::app()->db->createCommand($sql)->queryAll();

        if ($arrProviderInsurances) {
            foreach ($arrProviderInsurances as $provider_insurance) {
                $companyId = $provider_insurance['company_id'];
                $insuranceId = $provider_insurance['insurance_id'];

                if (!ProviderPracticeInsurance::model()->exists(
                    'provider_id=:provider_id AND company_id=:company_id AND practice_id=:practice_id '
                    . 'AND insurance_id=:insurance_id',
                    array(
                        ':provider_id' => $providerId,
                        ':company_id' => $companyId,
                        ':practice_id' => $practiceId,
                        ':insurance_id' => $insuranceId
                    )
                )) {
                    $ppi_model = new ProviderPracticeInsurance('insert');
                    $ppi_model->id = 0;
                    $ppi_model->provider_id = $providerId;
                    $ppi_model->company_id = $companyId;
                    $ppi_model->practice_id = $practiceId;
                    $ppi_model->insurance_id = $insuranceId;
                    $ppi_model->save();
                }
            }
        }
    }

    /**
     * PADM: Get insurance plans given a provider, practice and company
     * @param int $providerId
     * @param int $practiceId
     * @param string $practiceName
     * @param int $companyId
     * @return array
     */
    public static function getProviderPracticeCompanyPlans($providerId, $practiceId, $practiceName, $companyId)
    {

        $lstCompanyId = InsuranceCompany::getSibblingCompanyIds($companyId, true);

        $sql = sprintf(
            "SELECT
                %s AS practice_id, '%s' AS practice_name,
                insurance.id AS insurance_id,
                insurance.name AS insurance_name,
                details,
                IF(provider_practice_insurance.id IS NOT NULL OR provider_practice_insurance.insurance_id = 0, '1', '0')
                    AS checked
            FROM
                insurance
                LEFT JOIN provider_practice_insurance
                    ON insurance.company_id = provider_practice_insurance.company_id
                        AND provider_id = %d AND practice_id = %d
                        AND (insurance.id = provider_practice_insurance.insurance_id OR provider_practice_insurance.insurance_id = 0)
            WHERE
                insurance.company_id IN (%s)
            ORDER BY
                insurance_name;",
            $practiceId,
            addslashes($practiceName),
            $providerId,
            $practiceId,
            $lstCompanyId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Return each insurances for this provider, for each practice and each Company
     * @param int $providerId
     * @param array $practices
     * @param array $companies
     * @return array
     */
    public static function getProviderPracticeInsurancesDetails($providerId = 0, $practices= array(), $companies = array())
    {

        if ($providerId == 0 || empty($practices) || empty($companies)) {
            return false;
        }
        $response = array();
        foreach ($practices as $practice) {
            $practiceId = $practice['id'];
            $practiceName = $practice['name'];

            foreach ($companies as $company) {
                $companyId = $company['id'];

                // try to find records in provider_practice_insurance
                $arrDetails = ProviderPracticeInsurance::model()->getProviderPracticeCompanyPlans(
                    $providerId,
                    $practiceId,
                    $practiceName,
                    $companyId
                );
                if (!empty($arrDetails)) {
                    foreach ($arrDetails as $value) {
                        if ($value['checked'] == 1) {
                            $insuranceId = $value['insurance_id'];
                            $response[$companyId][$insuranceId][$practiceId]['practice_name'] = $practiceName;
                            $response[$companyId][$insuranceId][$practiceId]['details'] = $value['details'];
                            $response[$companyId][$insuranceId][$practiceId]['company_name'] = $company['name'];
                            $response[$companyId][$insuranceId][$practiceId]['insurance_name'] = $value['insurance_name'];
                        }
                    }
                }
            }
        }
        return $response;
    }

    /**
     * DA: Get a practice's insurances
     * @param int $practiceId
     * @param bool $useCache
     * @return array
     */
    public static function getProviderPracticeInsurancesByPractice($practiceId = 0, $useCache = true)
    {
        if ($practiceId == 0) {
            return null;
        }
        $sql = sprintf(
            "SELECT SQL_NO_CACHE provider_practice_insurance.company_id,
            insurance_company.name AS company_name, provider_practice_insurance.insurance_id,
            insurance.name AS insurance_name, provider_practice_insurance.provider_id
            FROM provider_practice_insurance
            INNER JOIN insurance_company
                ON provider_practice_insurance.company_id = insurance_company.id
            INNER JOIN insurance
                ON provider_practice_insurance.insurance_id = insurance.id
            WHERE provider_practice_insurance.practice_id = %d
            GROUP BY company_name, insurance_name
            ORDER BY company_name, insurance_name;",
            $practiceId
        );

        if ($useCache) {
            return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
        } else {
            return Yii::app()->db->createCommand($sql)->queryAll();
        }
    }

    /**
     * Provider Practice InsuranceCompany by Practice Id for all providers
     * @param int $practiceId
     * @param bool $useCache
     * @return array
     */
    public static function getProviderPracticeInsuranceCompaniesByPracticeWithProviders($practiceId, $useCache = true)
    {

        $sql = sprintf(
            "SELECT SQL_NO_CACHE provider_practice_insurance.company_id, insurance_company.name AS company_name,
            provider_practice_insurance.provider_id
            FROM provider_practice_insurance
            INNER JOIN insurance_company
                ON provider_practice_insurance.company_id = insurance_company.id
            WHERE provider_practice_insurance.practice_id = %d
            GROUP BY company_name, provider_practice_insurance.provider_id
            ORDER BY insurance_company.name, provider_practice_insurance.provider_id;",
            $practiceId
        );
        if ($useCache) {
            return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
        } else {
            return Yii::app()->db->createCommand($sql)->queryAll();
        }


    }

    /**
     * DA: Get a practice's insurance companies (we are assuming that in most cases they are the same for all providers)
     * @param int $practiceId
     * @return array
     */
    public static function getProviderPracticeInsuranceCompaniesByPractice($practiceId)
    {

        $sql = sprintf(
            "SELECT SQL_NO_CACHE provider_practice_insurance.company_id, insurance_company.name AS company_name
            FROM provider_practice_insurance
            INNER JOIN insurance_company
            ON provider_practice_insurance.company_id = insurance_company.id
            WHERE provider_practice_insurance.practice_id = %d
            GROUP BY insurance_company.name
            ORDER BY insurance_company.name;",
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();

    }

    /**
     * DA: Get a practice's provider-specific insurance companies
     * @param int $practiceId
     * @return array
     */
    public static function getProviderPracticeInsuranceCompaniesByPracticeProvider($practiceId, $providerId)
    {

        $sql = sprintf(
            "SELECT SQL_NO_CACHE provider_practice_insurance.company_id, insurance_company.name AS company_name
            FROM provider_practice_insurance
            INNER JOIN insurance_company
            ON provider_practice_insurance.company_id = insurance_company.id
            WHERE provider_practice_insurance.practice_id = %d
            AND provider_practice_insurance.provider_id = %d
            GROUP BY insurance_company.name
            ORDER BY insurance_company.name;",
            $practiceId,
            $providerId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();

    }

    /**
     * Override search so GADM can search by provider_npi, practice_name, practice_address, company_name, insurance_name, gmb_network_id
     *
     * @return \CActiveDataProvider
     */
    public function getGmbInsurances()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('t.id', $this->id);

        $searchTerm = Yii::app()->request->getParam('ProviderPracticeInsurance', []);

        if (!empty($searchTerm['provider_npi'])) {
            $criteria->compare('provider.npi_id', $searchTerm['provider_npi'], true);
        }

        if (!empty($this->provider_id)) {
            if (is_numeric($this->provider_id)) {
                $criteria->compare('provider.id', $this->provider_id);
            } else {
                $criteria->compare('provider.first_last', $this->provider_id, true);
            }
        }

        if (!empty($this->practice_id)) {
            $criteria->compare('practice.id', $this->practice_id);
        }

        if (!empty($searchTerm['practice_name'])) {
            $criteria->compare('practice.name', $searchTerm['practice_name'], true);
        }

        if (!empty($searchTerm['practice_address'])) {
            $criteria->compare('practice.address', $searchTerm['practice_address'], true);
        }

        if (!empty($searchTerm['company_name'])) {
            $criteria->compare('company.name', $searchTerm['company_name'], true);
        }

        if (!empty($searchTerm['insurance_name'])) {
            $criteria->compare('insurance.name', $searchTerm['insurance_name'], true);
        }

        if (!empty($searchTerm['gmb_network_id'])) {
            $criteria->compare('insurance.gmb_network_id', $searchTerm['gmb_network_id'], true);
        }

        $criteria->with['insurance'] = ['id', 'name', 'gmb_network_id'];
        $criteria->with['provider'] = ['id', 'npi_id', 'first_last'];
        $criteria->with['practice'] = ['name', 'address'];
        $criteria->with['insurance.company'] = ['id', 'name'];

        $criteria->addCondition('insurance.gmb_network_id IS NOT NULL');

        /*
        $criteria->join = ' INNER JOIN provider_practice_gmb USING (practice_id, provider_id)
            INNER JOIN provider_practice_gmb_data_point ON provider_practice_gmb.id = provider_practice_gmb_data_point.provider_practice_gmb_id
                AND gmb_data_point_id = 11 AND provider_practice_gmb_data_point.enabled';
        */

        $criteria->order = 't.id DESC';

        $data = new CActiveDataProvider(
            $this,
            [
                'criteria' => $criteria,
                'pagination' => [
                    'pageSize' => 100
                ]
            ]
        );

        return $data;
    }

    /**
     * Override rules defined by the base model
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules[] = array('id, provider_npi, provider_id, company_id, practice_id, practice_name, practice_address, company_name, insurance_name, gmb_network_id, insurance_id, network_status, details, date_added', 'safe', 'on' => 'search');

        return $rules;
    }

}
