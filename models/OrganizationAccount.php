<?php

Yii::import('application.modules.core_models.models._base.BaseOrganizationAccount');

class OrganizationAccount extends BaseOrganizationAccount
{

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Validate data beforeSave()
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->connection == 'O') {
            // T19093
            // The code must allow ONE organization_account record with connection='O' per organization_id

            $cond = '';
            if (!$this->isNewRecord) {
                $cond = " AND id != " . $this->id;
            }

            $sql = sprintf(
                "SELECT id
                FROM organization_account
                WHERE organization_id = %d
                    AND connection = 'O' %s
                LIMIT 1;",
                $this->organization_id,
                $cond
            );
            $exists = Yii::app()->db->createCommand($sql)->queryRow();

            if (!empty($exists)) {
                $this->addError("id", "Account doesn't exist.");
                return false;
            }
        }
        return parent::beforeSave();
    }

    /**
     * Set the right claimed value for all providers and practices related to the account
     */
    public function afterSave()
    {
        // every time a new org is added to an account as an owner,
        // find all providers and practices and update
        // the claimed value according to T74964
        if ($this->isNewRecord && $this->connection == 'O') {
            Account::updateClaimedValue($this->account_id);
        }

        if ($this->connection != 'O') {
            // Inherit the value of `is_beta_account` from the account owner

            // get the owner
            $ownerAccountId = OrganizationAccount::switchOwnerAccountID($this->account_id, true);
            $ownerAccount = Account::model()->findByPk($ownerAccountId);

            $thisAccount = Account::model()->findByPk($this->account_id);

            if ($thisAccount->is_beta_account != $ownerAccount->is_beta_account) {
                $thisAccount->is_beta_account = $ownerAccount->is_beta_account;
                $thisAccount->save();
            }
        }

        // Ensure that both (organization & account) have the same partner_site_id
        $organization = Organization::model()->findByPk($this->organization_id);
        $account = Account::model()->findByPk($this->account_id);
        if ($organization->partner_site_id != $account->partner_site_id) {
            $account->partner_site_id = $organization->partner_site_id;
            $account->save();

            // update log
            $auditAction = 'U';
            $auditTitle = 'Updated Account: ' . $account->id . ' set partner_site_id: ' . $account->partner_site_id;
            AuditLog::create(
                $auditAction,
                $auditTitle,
                'account',
                json_encode($account->attributes),
                false
            );
        }

        $auditAction = 'U';
        $auditTitle = 'Updated Account ' . $this->id;
        if ($this->isNewRecord) {
            $auditAction = 'C';
            $auditTitle = 'Created Account ' . $this->id;
        }
        AuditLog::create(
            $auditAction,
            $auditTitle,
            'organization_account',
            json_encode($this->attributes),
            false
        );

        // update scheduling if necessary
        if ($this->isNewRecord
            ||
            (!empty($this->oldRecord) && $this->oldRecord->connection != $this->connection)
            ||
            (!empty($this->oldRecord) && $this->oldRecord->account_id != $this->account_id)
        ) {
            if (!empty($this->account_id)) {
                // async update
                SqsComponent::sendMessage('updateBooking', json_encode(['account_id' => $this->account_id]));
            }
            if (!empty($this->oldRecord->account_id) && $this->oldRecord->account_id != $this->account_id) {
                // async update the previous account_id
                SqsComponent::sendMessage('updateBooking', json_encode(['account_id' => $this->oldRecord->account_id]));
            }
        }

        return parent::afterSave();
    }

    /**
     * Set the right claimed value for all providers and practices related to the account
     */
    public function afterDelete()
    {
        // every time a new owner org is removed from an account,
        // find all providers and practices and update the
        // claimed value according to T74964
        if ($this->connection == 'O') {
            Account::updateClaimedValue($this->account_id);
        }

        AuditLog::create(
            'D',
            'Deleted account ' . $this->id,
            'organization_account',
            json_encode($this->attributes),
            false
        );

        // update scheduling if necessary
        if (!empty($this->account_id)) {
            // async update
            SqsComponent::sendMessage('updateBooking', json_encode(['account_id' => $this->account_id]));
        }

        return parent::afterDelete();
    }

    /**
     * PADM: delete an account Staff or Manager type
     * @param int $accountId
     * @param int $accountIdToDelete
     * @param int $adminAccountId
     * @return array
     */
    public static function deleteOrgAccount($accountId = 0, $accountIdToDelete = 0, $adminAccountId = 0)
    {
        $response = array();

        $sql = sprintf("SELECT * FROM organization_account WHERE account_id = %d", $accountId);
        $currentAccountOrg = Yii::app()->db->createCommand($sql)->queryRow();

        if ($adminAccountId == 0) {
            // The user is NOT a DDC admin
            if (!$currentAccountOrg || $currentAccountOrg['connection'] == 'S') {
                $response['success'] = false;
                $response['error'] = 'ACCESS_DENIED';
                return $response;
            }
        }

        $targetAccount = Account::model()->findByPK($accountIdToDelete);
        if (empty($targetAccount)) {
            return $response;
        }

        if (!empty($currentAccountOrg['organization_id'])) {

            // Is linked to an organization
            $targetOrganizationAccount = OrganizationAccount::model()->find(
                'account_id = :account_id AND organization_id = :organization_id',
                array(
                    ':account_id' => $accountIdToDelete,
                    ':organization_id' => $currentAccountOrg['organization_id']
                )
            );

            if (empty($targetOrganizationAccount) || $targetOrganizationAccount->connection == "O") {
                $targetAccount->addError('id', 'Cannot delete the owner.');
                $response['success'] = false;
                $response['error'] = 'Cannot delete the Owner';
                return $response;
            }

            if ($targetOrganizationAccount->delete()) {
                $auditSection = 'Delete OrganizationAccount, organization: '
                    . $targetOrganizationAccount->organization_id
                    . ' account_id: ' . $targetOrganizationAccount->account_id;

                AuditLog::create('D', $auditSection, 'organization_account', null, false);
            } else {
                $response['success'] = false;
                $response['error'] = StringComponent::flatten($targetOrganizationAccount->getErrors());
                return $response;
            }
        }

        if ($targetAccount) {
            if ($targetAccount->delete()) {
                AuditLog::create('D', 'Delete account #' . $accountIdToDelete, 'account', null, false);
                $response['success'] = "done!";
            } else {
                $response['success'] = false;
                $response['error'] = StringComponent::flatten($targetAccount->getErrors());
            }
        } else {
            $response['success'] = false;
            $response['error'] = "Account not found";
        }
        return $response;
    }

    /**
     * Return Account ID from the Owner Organization if the Account is not owner
     * It's used when need manage account privileges for manager and staff users
     * @param int $accountId
     * @param bool $staffIdToo
     * @return Account->id
     */
    public static function switchOwnerAccountID($accountId, $staffIdToo = false)
    {

        // This account is related to an organization?
        $sql = sprintf(
            "SELECT id, organization_id, account_id, connection, date_added, date_updated
            FROM organization_account
            WHERE account_id = %d
            LIMIT 1",
            $accountId
        );
        $currentAccountOrg = Yii::app()->db->createCommand($sql)->queryRow();

        if (!empty($currentAccountOrg)) {
            // Yes, is linked to an organization
            if ($currentAccountOrg['connection'] == 'M' || $staffIdToo) {
                // If is not he owner, then Find the owner
                $sql = sprintf(
                    "SELECT account_id
                    FROM organization_account
                    WHERE organization_id = %d
                        AND `connection`='O'
                    LIMIT 1",
                    $currentAccountOrg['organization_id']
                );
                $ownerAccountOrg = Yii::app()->db->createCommand($sql)->queryRow();

                if ($ownerAccountOrg) {
                    $accountId = $ownerAccountOrg['account_id'];
                }
            }
        }
        return $accountId;
    }

    /**
     * Return the list of the account managers
     * @param int $accountId
     * @param bool $getPrivileges (optional to know if get privileges or not)
     * @return array $accountManagers
     */
    public static function getManagers($accountId = 0, $getPrivileges = false)
    {
        if ($accountId == 0) {
            return [];
        }
        $accountManagers = array();
        $currentAccountOrg = Organization::getByAccountId($accountId, true);
        $organizationId = !empty($currentAccountOrg['id']) ? $currentAccountOrg['id'] : 0;
        if ($organizationId == 0) {
            // For Free account
            $account = Account::model()->findByPk($accountId);
            $current = array();
            $current["organization_id"] = 0;
            $current["id"] = $accountId;
            $current["name"] = $account->first_name . " " . $account->last_name;
            $current["email"] = $account->email;
            $current["connection_type"] = 'O';
            $current['first_name'] = $account->first_name;
            $current['last_name'] = $account->last_name;
            $current["you"] = true;
            $accountManagers[] = $current;
        } else {
            $allAccountOrgs = OrganizationAccount::model()->findAll(
                'organization_id=:organization_id',
                array(':organization_id' => $organizationId)
            );

            foreach ($allAccountOrgs as $ao) {
                $current = array();
                $current["organization_id"] = $organizationId;
                $current["id"] = (int) $ao->account_id;
                $account = $ao->account;
                $current["name"] = $account->first_name . " " . $account->last_name;
                $current["email"] = $account->email;
                $current["connection_type"] = $ao->connection;
                $current['first_name'] = $account->first_name;
                $current['last_name'] = $account->last_name;

                $current["connection"] = "Staff";
                if ($ao->connection == "O") {
                    $current["connection"] = "Owner";
                } elseif ($ao->connection == "M") {
                    $current["connection"] = "Manager";
                }
                $current["you"] = ($ao->account_id == $accountId) ? true : false;

                if ($getPrivileges) {
                    $current['privileges'] = AccountPrivileges::getAccountPrivileges($ao->account_id);
                    if (!empty($current['privileges'])) {
                        unset($current['privileges']['connection_type']);
                        unset($current['privileges']['id']);
                        unset($current['privileges']['account_id']);
                    }
                }
                $accountManagers[] = $current;
            }
        }
        return $accountManagers;
    }

    /**
     * Find all organizations that own a given practice and have a Salesforce connection
     * @param int $practiceId
     * @return array
     */
    public static function getPracticeOwners($practiceId)
    {
        $sql = sprintf(
            "SELECT DISTINCT organization_account.organization_id
            FROM organization_account
            INNER JOIN account_practice
                ON organization_account.account_id = account_practice.account_id
            INNER JOIN organization
                ON organization_account.organization_id = organization.id
            WHERE organization_account.connection = 'O'
                AND (salesforce_id IS NOT NULL AND salesforce_id <> '')
                AND account_practice.practice_id = %d;",
            $practiceId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Returns true only if organization_account connection is 'O' (owner) for accountId parameter
     * @param type $accountId
     * @return boolean
     */
    public static function isOwnerAccount($accountId)
    {
        $currentAccountOrg = OrganizationAccount::model()->find(
            'account_id=:account_id',
            array(':account_id' => $accountId)
        );

        if (!empty($currentAccountOrg)) {
            if ($currentAccountOrg->connection == 'O') {
                return true;
            }
        }

        return false;
    }

    /**
     * Return all account from the same owner account
     * @param int $accountId
     * @param int $organizationId
     * @param bool $returnList (list || array)
     * @param bool $staffToo - If Im staff and this is false, only return my Id
     * @return array
     */
    public static function getAllAccountId($accountId = 0, $organizationId = 0, $returnList = false, $staffToo = true)
    {

        // What I'm?
        $orgAccount = OrganizationAccount::model()
            ->cache(Yii::app()->params['cache_medium'])
            ->find("account_id=:accountId", array(":accountId" => $accountId));
        if (!empty($orgAccount) && $orgAccount->connection == 'S' && !$staffToo) {

            if (Common::isTrue($returnList)) {
                return [$accountId];
            } else {
                return $accountId;
            }
        }

        $arrayOrg = array();
        if ($organizationId == 0) {
            $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId, true);

            $currentAccountOrg = OrganizationAccount::model()
                ->cache(Yii::app()->params['cache_short'])
                ->find("account_id=:accountId", array(":accountId" => $ownerAccountId));

            if (!empty($currentAccountOrg)) {
                $organizationId = $currentAccountOrg->organization_id;
            }
        }

        if ($organizationId > 0) {
            $arrAccountOrg = OrganizationAccount::model()
            ->cache(Yii::app()->params['cache_short'])
            ->findAll(
                "organization_id=:organization_id",
                array(":organization_id" => $organizationId)
            );

            foreach ($arrAccountOrg as $accountOrg) {
                $arrayOrg[$accountOrg->account_id] = $accountOrg->account_id;
            }
        } else {
            // free account
            $arrayOrg[$accountId] = $accountId;
        }

        if (Common::isTrue($returnList)) {
            return implode(',', $arrayOrg);
        }

        return $arrayOrg;
    }

    /**
     * Get all account linked to an organization
     *
     * @param int $organizationId
     * @return array
     */
    public static function getAccountsByOrg($organizationId)
    {
        $sql = "SELECT DISTINCT account_id FROM organization_account WHERE organization_id = " . $organizationId;

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryColumn();
    }

    /**
     * Find the owner account for a given organization
     * @param $organizationId
     * @return mixed
     */
    public function findOwnerAccount($organizationId)
    {
        return OrganizationAccount::model()->find(
            "organization_id = :organization_id AND connection = 'O'",
            ['organization_id' => $organizationId]
        );
    }

}
