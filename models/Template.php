<?php

Yii::import('application.modules.core_models.models._base.BaseTemplate');

class Template extends BaseTemplate
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'name';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Template|Templates', $n);
    }

    /**
     * Clean the plaintext content and make sure the composite key internal_type/lang_id is respected
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->content_plain) {
            $this->content_plain = StringComponent::cleanseString($this->content_plain);
        }

        $templateExists = false;
        if ($this->isNewRecord) {
            if (self::find('internal_type=:internalType AND lang_id=:langId', array(':internalType' =>
                $this->internal_type, ':langId' => $this->lang_id))) {
                $templateExists = true;
            }
        } else {
            if (self::find('internal_type=:internalType AND lang_id=:langId AND id!=:id', array(':internalType' =>
                $this->internal_type, ':langId' => $this->lang_id, ':id' => $this->id))) {
                $templateExists = true;
            }
        }
        if ($templateExists) {
            $this->addError('lang_id', 'There\'s already a template for that internal type/language combination');
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Prepare the list related to patient email template that can be unsubscribed
     * @return array
     */
    public static function getUnsubscribePatientTemplates()
    {
        // Patients Templates:
        $patientTemplates['PATIENT_MESSAGÏNG']['name'] = 'Patient Messaging';
        $patientTemplates['REVIEW_RESPONSE_APPROVED_TO_PATIENT']['name'] = 'Review Approved';
        $patientTemplates['APPOINTMENT_REQUESTED_FOR_PATIENT_S']['name'] = 'Appointment requested';
        $patientTemplates['APPOINTMENT_APPROVED_FOR_PATIENT']['name'] = 'Appointment approved';
        $patientTemplates['APPOINTMENT_CANCELLED_FOR_PATIENT']['name'] = 'Appointment cancelled';
        $patientTemplates['APPOINTMENT_MAIL_REMINDER_FOR_PATIENT']['name'] = 'Appointment Reminder';

        // Add Ids
        foreach ($patientTemplates as $key => $value) {
            $arrIds = Template::model()->cache(Yii::app()->params['cache_short'])->findAll(
                'internal_type=:internalType',
                array(':internalType' => $key)
            );
            $listId = '';
            if (!empty($arrIds)) {
                foreach ($arrIds as $v) {
                    $listId = $listId . $v->id . ',';
                }
            }
            $listId = trim($listId, ',');
            $patientTemplates[$key]['id_list'] = $listId;
            $patientTemplates[$key]['checked'] = '';
        }

        return $patientTemplates;
    }

    /**
     * Return email and sms templates reminders
     * @param int $practiceId
     * @return array
     */
    public static function getRemindersByPractice($practiceId = 0, $globalParameter = '')
    {

        $practice = Mail::getPractice($practiceId);

        $globalParameter = str_replace(chr(10), "<br>", $globalParameter);
        $globalParameter = str_replace('"', "'", $globalParameter);

        $templateMailReminder = Template::model()->cache(Yii::app()->params['cache_short'])->find(
            'internal_type=:internal_type',
            array(':internal_type' => 'APPOINTMENT_MAIL_REMINDER_FOR_PATIENT')
        );

        $templateSmsReminder = Template::model()->cache(Yii::app()->params['cache_short'])->find(
            'internal_type=:internal_type',
            array(':internal_type' => 'APPOINTMENT_SMS_REMINDER_FOR_PATIENT')
        );

        $templateMailReminder->content = MailComponent::doReplacements($practice, $templateMailReminder->content);

        $templateSmsReminder->content = MailComponent::doReplacements($practice, $templateSmsReminder->content);

        $arrHtml1 = explode("<img ", $templateMailReminder->content, 2);
        $arrHtml2 = explode(">", $arrHtml1[1], 2);

        $templateMailReminder->content = $arrHtml1[0];

        $practice['logo_path'] = !empty($practice['p_logo_path']) ? $practice['p_logo_path'] : '';
        if ($practice['logo_path']) {
            $templateMailReminder->content .= '<div id="practice-photo" class="practice-photos">';
            $templateMailReminder->content .= '<img class="profile-img" src="' . $practice['logo_path'] . '" />';
            $templateMailReminder->content .= '</div>';
        } else {
            $templateMailReminder->content .= '<div id="practice-photo" style="background-size: 605px; '
                . 'background-position: -90px -409px;" class="practice-photos profile-img default-img-practice"></div>';
        }

        $templateMailReminder->content .= $arrHtml2[1];

        $templateMailReminder->content = str_replace("%s_first_name%", "John", $templateMailReminder->content);
        $templateMailReminder->content = str_replace("%p_prefix%", "", $templateMailReminder->content);
        $templateMailReminder->content = str_replace("%p_first_name%", "John", $templateMailReminder->content);
        $templateMailReminder->content = str_replace("%p_last_name%", "Doe", $templateMailReminder->content);
        $templateMailReminder->content = str_replace(
            "%s_appointment_date%",
            date("m/d/Y"),
            $templateMailReminder->content
        );
        $templateMailReminder->content = str_replace(
            "%s_formatted_time_start%",
            "10:00 AM",
            $templateMailReminder->content
        );
        $templateMailReminder->content = str_replace("%s_insurance%", "", $templateMailReminder->content);
        $templateMailReminder->content = str_replace(
            "%s_reason%",
            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor",
            $templateMailReminder->content
        );
        $templateMailReminder->content = str_replace("INSURANCE SELECTED", "", $templateMailReminder->content);
        $templateMailReminder->content = str_replace(
            "<a href",
            "<a target='_blank' href",
            $templateMailReminder->content
        );

        $templateSmsReminder->content = str_replace("%u_first_name%", "John", $templateSmsReminder->content);
        $templateSmsReminder->content = str_replace(
            "%s_appointment_date%",
            date("m/d/Y"),
            $templateSmsReminder->content
        );
        $templateSmsReminder->content = str_replace(
            "%s_formatted_time_start%",
            "10:00 AM",
            $templateSmsReminder->content
        );

        $reminders['mail'] = $templateMailReminder->attributes;
        $reminders['sms'] = $templateSmsReminder->attributes;
        return $reminders;
    }
}
