<?php

Yii::import('application.modules.core_models.models._base.BaseCampaignEmailLink');

class CampaignEmailLink extends BaseCampaignEmailLink
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Set dates automatically if needed
     */
    public function beforeSave()
    {
        if (empty($this->expires) || $this->expires == '0000-00-00 00:00:00') {
            $this->expires = date('Y-m-d H:i:s');
        }

        return parent::beforeSave();
    }

    /**
     * Get email links for the campaign email
     * @param int $campaignEmailId
     * @param int $accountId
     * @param int $providerId
     * @return array
     */
    public static function generateLinks($campaignEmailId, $accountId, $providerId)
    {
        $expires = date('Y-m-d H:i:s', strtotime("+30 days"));

        $changeAction = "https://" . Yii::app()->params->servers['dr_pro_hn'] . "/myaccount/provider?id=" . $providerId
            . "&accountId=" . $accountId;
        $changeLink = "https://" . Yii::app()->params->servers['dr_pro_hn'] . "/campaign/action?id=" .
            md5($changeAction);

        $change = new CampaignEmailLink();
        $change->campaign_email_id = $campaignEmailId;
        $change->link = $changeLink;
        $change->action = $changeAction;
        $change->expires = $expires;
        $change->save();

        $visitReasonsAction = "https://" . Yii::app()->params->servers['dr_pro_hn'] .
            "/myaccount/accountEmrVisitReason?accountId=" . $accountId . "&providerId=" . $providerId;
        $visitReasonsLink = "https://" . Yii::app()->params->servers['dr_pro_hn'] . "/campaign/action?id=" .
            md5($visitReasonsAction);
        $visitReasons = new CampaignEmailLink();
        $visitReasons->campaign_email_id = $campaignEmailId;
        $visitReasons->link = $visitReasonsLink;
        $visitReasons->action = $visitReasonsAction;
        $visitReasons->expires = $expires;
        $visitReasons->save();

        $vacationsAction = "https://" . Yii::app()->params->servers['dr_pro_hn'] .
            "/myaccount/accountEmrExceptions?accountId=" . $accountId . "&providerId=" . $providerId;
        $vacationsLink = "https://" . Yii::app()->params->servers['dr_pro_hn'] . "/campaign/action?id=" .
            md5($vacationsAction);
        $vacations = new CampaignEmailLink();
        $vacations->campaign_email_id = $campaignEmailId;
        $vacations->link = $vacationsLink;
        $vacations->action = $vacationsAction;
        $vacations->expires = $expires;
        $vacations->save();

        return array(
            "change" => $change,
            "visitReasons" => $visitReasons,
            "vacations" => $vacations,
            "disable" => null
        );
    }

}
