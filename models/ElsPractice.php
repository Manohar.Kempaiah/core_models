<?php

class ElsPractice
{

    /**
     * Return formatted array to the view
     * @param array $practiceData
     * @param array $providerData
     * @return array
     */
    public static function formatArrayToView($practiceData, $providerData)
    {

        $result = array();

        // temporary change for index divergence
        if (isset($practiceData['hits']['hits'][0]['_source']['tracked_phone_number'])) {
            $practiceData['hits']['hits'][0]['_source']['featured_phone_number'] =
                    $practiceData['hits']['hits'][0]['_source']['tracked_phone_number'];
            unset($practiceData['hits']['hits'][0]['_source']['tracked_phone_number']);
        }

        $result['practice'] = $practiceData['hits']['hits'][0]['_source'];

        if (!empty($result['practice']['city_id'])) {
            $result['city'] = (object) array(
                        "id" => $result['practice']['city_id'],
                        "state_id" => $result['practice']['state_id'],
                        "name" => $result['practice']['city_name'],
                        "friendly_url" => $result['practice']['city_friendly_url'],
                        "state" => (object) array(
                            "id" => $result['practice']['state_id'],
                            "name" => $result['practice']['state_name'],
                            "abbreviation" => $result['practice']['state_abbreviation']
                        )
            );

            $result['practice']['location'] = (object) array(
                        'id' => $result['practice']['location_id'],
                        'zipcode' => $result['practice']['zipcode']
            );
        }

        $result['practice']['id'] = $result['practice']['practice_id'];
        $practiceId = $result['practice']['practice_id'];
        $arrTreatments = array();
        $quality = array();
        $qualityPath = array();

        $result['arrTreatments'] = $arrTreatments;
        $result['quality'] = $quality;
        $result['qualityPath'] = $qualityPath;

        // get providers related to this practice
        $result['providers'] = array();

        if (isset($result['practice']['provider'])) {
            foreach ($result['practice']['provider'] as $provider) {
                $result['providers'][$provider['provider_id']] = array(
                    "id" => $provider['provider_id'],
                    "name" => $provider['provider_name'],
                    "featured" => $provider['featured'],
                    "prefix" => $provider['prefix'],
                    "first_name" => $provider['first_name'],
                    "last_name" => $provider['last_name'],
                    "friendly_url" => $provider['friendly_url'],
                    "credential" => $provider['title'],
                );
            }
        }

        // get practice specialties
        $practiceSpecialties = array();
        $providerPracticeSpecialties = array();
        $addProviderSpecialties = true;

        $schedulingEnabled = false;
        $providersOfficeHours = array();
        $schedule_online = false;
        $practiceReviews = array();
        $providerSpecialties = array();

        if (isset($providerData['hits']['total']) && $providerData['hits']['total'] > 0) {
            // get providers specialties
            foreach ($providerData['hits']['hits'] as $provider) {

                $providerId = $provider['_source']['provider_id'];

                if (!empty($provider['_source']['specialty'])) {
                    $arrTmpProviderSpecialties = array();

                    foreach ($provider['_source']['specialty'] as $specialty) {
                        $providerPracticeSpecialties[$specialty['specialty_name'] ][] = $specialty['sub_specialty_name'];
                    }

                    $providerSpecialties = join(', ', array_keys($arrTmpProviderSpecialties));
                    $result['providers'][$providerId]['specialties'] = $providerSpecialties;
                }

                $arrInfo = array(
                    0 => array(
                        'provider_id' => $providerId,
                        'practice_id' => $result['practice']['practice_id'],
                        'provider' => $provider['_source']['provider_name'],
                        'brief_bio' => $provider['_source']['brief_bio'],
                        'friendly_url' => $provider['_source']['friendly_url'],
                        'credential' => $provider['_source']['title'],
                        'schedule_online' => $provider['_source']['schedule_online'],
                        'avg_rating' => $provider['_source']['avg_rating'],
                        'docscore' => $provider['_source']['docscore'],
                        'photo_path' => $provider['_source']['photo_path'],
                        'gender' => $provider['_source']['gender'],
                        'vanity_specialty' => $provider['_source']['vanity_specialty'],
                        'primary_location' => $result['practice']['address'],
                        'address_2' => isset($result['practice']['address_2']) ? $result['practice']['address_2'] : '',
                        'zipcode' => $result['practice']['zipcode'],
                        'city' => $result['practice']['city_name'],
                        'state' => $result['practice']['state_abbreviation'],
                        'board_certificated' =>
                            isset($provider['_source']['certification'])
                            && is_array($provider['_source']['certification'])
                            && count($provider['_source']['certification']) > 0,
                        'accepts_insurance' =>
                            isset($provider['_source']['insurance'])
                            && is_array($provider['_source']['insurance'])
                            && count($provider['_source']['insurance']) > 0,
                        'suspended' => $provider['_source']['suspended'],
                        'phone_number' => (
                            !empty($provider['_source']['phone_number'])
                            ? $provider['_source']['phone_number']
                            : ''
                        ),
                        'featured_phone_number' => (
                            !empty($provider['_source']['featured_phone_number'])
                            ? $provider['_source']['featured_phone_number']
                            : ''
                        ),
                        'primary_location' => '1',
                        'practice_type_id' => $result['practice']['practice_type_id'],
                        'coverage_location_id' => $result['practice']['coverage_location_id'],
                        'coverage_state_id' => $result['practice']['coverage_state_id'],
                        'claimed' => $provider['_source']['claimed']
                    )
                );

                $arrInfo[0]['href'] = '';
                if (!empty($arrInfo[0]['friendly_url'])) {
                    $arrInfo[0]['href'] = 'https://' . yii::app()->params->servers['dr_web_hn'] . '/'
                        . $arrInfo[0]['friendly_url'];
                }

                if (!empty($arrInfo[0]['photo_path'])) {
                    $arrInfo[0]['photo_href'] = AssetsComponent::generateImgSrc(
                        $arrInfo[0]['photo_path'],
                        'providers',
                        'S'
                    );
                } else {
                    $arrInfo[0]['photo_href'] = '';
                }

                $result['providers'][$providerId] = array_merge(
                    isset($result['providers'][$providerId]) ? (array) $result['providers'][$providerId] : [],
                    (array) $arrInfo[0]
                );

                // only at least 1 provider needs to have it enabled for us to show the button etc.
                if ($provider['_source']['schedule_online']) {
                    $schedulingEnabled = true;
                    $schedule_online = true;
                }

                if (isset($provider['_source']['rating'])) {
                    foreach ($provider['_source']['rating'] as $rating) {
                        // Is our review?
                        if ($rating['origin'] == 'D' && $rating['partner_id'] == 1) {
                            // Yes, so send to the view
                            $rating['practiceNameAndAddress'] = $result['practice']['name'] . ' - '
                                . $result['practice']['address']
                                . (isset($result['practice']['address_2']) ? ' ' . $result['practice']['address_2'] : '');
                            $rating['photo_path'] = $provider['_source']['photo_path'];
                            $practiceReviews[] = $rating;
                        }
                    }
                }
            }

            ksort($providerPracticeSpecialties);
            foreach ($providerPracticeSpecialties as $key => $value) {
                $providerPracticeSpecialties[$key] = array_unique($providerPracticeSpecialties[$key]);
            }
        }

        // Practice's specialty
        if (isset($result['practice']['specialty'])) {
            foreach ($result['practice']['specialty'] as $specialty) {
                $practiceSpecialties[$specialty['specialty_name'] ][] = $specialty['sub_specialty_name'];
            }
        } else {
            $practiceSpecialties = $providerPracticeSpecialties;
            $providerPracticeSpecialties = array();
        }

        $result['practiceSpecialties'] = $practiceSpecialties;
        $result['providerPracticeSpecialties'] = $providerPracticeSpecialties;
        $result['addProviderSpecialties'] = $addProviderSpecialties;

        $result['schedulingEnabled'] = $schedulingEnabled;
        $result['schedule_online'] = isset($schedule_online) ? $schedule_online : null;

        // Practice's videos
        $result['practiceVideos'] = isset($result['practice']['video']) ? $result['practice']['video'] : array();

        // Practice's photo
        $result['practicePhotos'] = isset($result['practice']['photo']) ? $result['practice']['photo'] : array();

        // Social Links
        // $practiceSocialLinks = ProviderPracticeListing::getPracticeSocialLinks($result['practice']['practice_id']);
        $practiceSocialLinks = array();
        if (isset($provider['_source']['practice_listing'])) {
            foreach ($provider['_source']['practice_listing'] as $listing) {
                if (!empty($listing['practice_id']) && $listing['practice_id'] == $result['practice']['practice_id']) {
                    if (!in_array($listing['url'], $practiceSocialLinks)) {
                        $practiceSocialLinks[] = array(
                            "name" => $listing['listing_type_name'],
                            "link" => $listing['url'],
                            "listing_type_id" => $listing['listing_type_id'],
                        );
                    }
                }
            }
        }
        $result['practiceSocialLinks'] = $practiceSocialLinks;

        // Practice Office Hours
        if (empty($result['practice']['office_hours'])) {
            $result['practiceOfficeHours'] = PracticeOfficeHour::getOfficeHours($practiceId);
        } else {
            $result['practiceOfficeHours'] = (
                !empty($result['practice']['office_hours'])
                ? $result['practice']['office_hours']
                : $providersOfficeHours
            );
            $dayNames = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
            for ($i = 0; $i < count($result['practiceOfficeHours']); $i++) {
                $result['practiceOfficeHours'][$i]['day'] = $dayNames[$result['practiceOfficeHours'][$i]['day_number']];

                // format office hours
                $h = $result['practiceOfficeHours'][$i];
                $arrHours = explode(" - ", $h["hours"]);
                $arrHours[0] = date_format(
                    date_create_from_format(
                        "H:i",
                        substr_count($arrHours[0], ":") == 2 ? substr($arrHours[0], 0, strrpos($arrHours[0], ":"))
                        : $arrHours[0]
                    ),
                    "h:i A"
                );
                $arrHours[1] = date_format(
                    date_create_from_format(
                        "H:i",
                        substr_count($arrHours[1], ":") == 2
                        ? substr($arrHours[1], 0, strrpos($arrHours[1], ":"))
                        : $arrHours[1]
                    ),
                    "h:i A"
                );
                $h["hours"] = implode(" - ", $arrHours);
                $result['practiceOfficeHours'][$i]['timeRange'] = $h["hours"];

            }
        }

        if (is_array($result['practiceOfficeHours'])) {
            usort($result['practiceOfficeHours'], function ($a, $b) {
                return (isset($a['day_number']) ? $a['day_number'] : null) - (isset($b['day_number']) ? $b['day_number'] : null);
            });
        }

        // Practice Insurance
        $result['practiceInsurance'] = isset($result['practice']['provider_insurance']) ? $result['practice']['provider_insurance'] : array();
        for ($k = 0; $k < count($result['practiceInsurance']); $k++) {
            $result['practiceInsurance'][$k]['company_name'] = $result['practiceInsurance'][$k]['insurance_company_name'];
        }

        // Practice Reviews
        $result['practiceReviews'] = $practiceReviews;

        // Calculate practice review ratings average
        $rating_average = $rating_average_count = 0;
        $rating_getting_appt = $rating_getting_appt_count = 0;
        $rating_appearance = $rating_appearance_count = 0;
        $rating_courtesy = $rating_courtesy_count = 0;
        $rating_waiting_time = $rating_waiting_time_count = 0;
        $rating_billing = $rating_billing_count = 0;

        foreach ($practiceReviews as $eachReview) {

            if ($eachReview['rating'] != '') {
                $rating_average_count += 1;
                $rating_average += $eachReview['rating'];
            }
            if ($eachReview['rating_getting_appt'] != '') {
                $rating_getting_appt_count += 1;
                $rating_getting_appt += $eachReview['rating_getting_appt'];
            }
            if ($eachReview['rating_getting_appt'] != '') {
                $rating_appearance_count += 1;
                $rating_appearance += $eachReview['rating_appearance'];
            }
            if ($eachReview['rating_getting_appt'] != '') {
                $rating_courtesy_count += 1;
                $rating_courtesy += $eachReview['rating_courtesy'];
            }
            if ($eachReview['rating_getting_appt'] != '') {
                $rating_waiting_time_count += 1;
                $rating_waiting_time += $eachReview['rating_waiting_time'];
            }
            if ($eachReview['rating_getting_appt'] != '') {
                $rating_billing_count += 1;
                $rating_billing += $eachReview['rating_billing'];
            }
        }

        // As Elastic Search doesn't sort data, manually sort reviews by date
        if (!empty($practiceReviews)) {
            $newOrder = array();
            $auxReviews = array();
            foreach ($practiceReviews as $review) {
                $newOrder[] = $review['date_added'];
            }
            arsort($newOrder);
            foreach ($newOrder as $index => $value) {
                $auxReviews[] = $practiceReviews[$index];
            }
            for ($i = 0; $i < count($practiceReviews); $i++) {
                $practiceReviews[$i] = $auxReviews[$i];
            }
        }

        if ($rating_average_count > 0) {
            $rating_average = ceil($rating_average / $rating_average_count);
        }
        if ($rating_getting_appt_count > 0) {
            $rating_getting_appt = ceil($rating_getting_appt / $rating_getting_appt_count);
        }
        if ($rating_courtesy_count > 0) {
            $rating_courtesy = ceil($rating_courtesy / $rating_courtesy_count);
        }
        if ($rating_appearance_count > 0) {
            $rating_appearance = ceil($rating_appearance / $rating_appearance_count);
        }
        if ($rating_billing_count > 0) {
            $rating_billing = ceil($rating_billing / $rating_billing_count);
        }
        if ($rating_waiting_time_count > 0) {
            $rating_waiting_time = ceil($rating_waiting_time / $rating_waiting_time_count);
        }

        $result['rating_average'] = $rating_average;
        $result['rating_getting_appt'] = $rating_getting_appt;
        $result['rating_appearance'] = $rating_appearance;
        $result['rating_courtesy'] = $rating_courtesy;
        $result['rating_waiting_time'] = $rating_waiting_time;
        $result['rating_billing'] = $rating_billing;

        $practiceReviewsRatings = array();
        $practiceReviewsRatings['rating_average_count'] = $rating_average_count;
        $practiceReviewsRatings['rating_average'] = $rating_average;
        $practiceReviewsRatings['rating_getting_appt'] = $rating_getting_appt;
        $practiceReviewsRatings['rating_appearance'] = $rating_appearance;
        $practiceReviewsRatings['rating_courtesy'] = $rating_courtesy;
        $practiceReviewsRatings['rating_waiting_time'] = $rating_waiting_time;
        $practiceReviewsRatings['rating_billing'] = $rating_billing;

        $result['practiceReviewsRatings'] = $practiceReviewsRatings;

        // metaTags for SEO
        $arrTags = [];
        $arrTags['title'] = $result['practice']['name'] . ' at Doctor.com';
        $arrTags['canonical'] = $result['practice']['friendly_url'];

        $result['arrTags'] = $arrTags;

        // MainSpecialties
        $mainSpecialties = '';
        if (!empty($practiceSpecialties)) {
            $mainSpecialties = $practiceSpecialties;
        } elseif (!empty($providerPracticeSpecialties)) {
            $mainSpecialties = $providerPracticeSpecialties;
        }

        $result['is_tracked_phone'] = false;
        $result['phone_number'] = $result['practice']['phone_number'];
        if (!empty($result['practice']['featured_phone_number']) &&
            (is_array($result['practice']['featured_phone_number']) ||
            is_object($result['practice']['featured_phone_number']))) {

            foreach ($result['practice']['featured_phone_number'] as $featured) {
                if ($featured['twilio_partner_id'] == Yii::app()->params->search_twilio_partner_id) {
                    $result['is_tracked_phone'] = true;
                    $result['phone_number'] = $featured['phone_number'];
                    break;
                }
            }
        }

        $result['mainSpecialties'] = $mainSpecialties;
        $result['practice'] = (object) $result['practice'];

        return $result;
    }

    /**
     * Formats the ElasticSearch practice for the autocomplete
     * @param array $data
     * @return array
     */
    public static function formatArrayToPracticeAutocomplete($data)
    {
        $formattedData = array();
        foreach ($data['hits']['hits'] as $practice) {
            $formattedData[] = array('id' => $practice['_source']['practice_id'],
                'name' => $practice['_source']['name'],
                'city' => $practice['_source']['city_name']);
        }

        return $formattedData;
    }
}
