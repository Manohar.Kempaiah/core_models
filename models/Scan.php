<?php

Yii::import('application.modules.core_models.models._base.BaseScan');

class Scan extends BaseScan
{

    /**
     * Used by the new monthly report
     * @var float
     */
    public $avgPes;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('organization_id, practice_id', 'required'),
            array('organization_id, provider_id, practice_id, google_places_optimized, yelp_claimed, completed_scans, data_elements, data_element_errors, missing_listings, review_tally, pagespeed_score, listing_accuracy, overall_score', 'numerical', 'integerOnly' => true),
            array('reviews_average, reviews_grade', 'length', 'max' => 10),
            array('zip', 'length', 'max' => 5),
            array('trigger, specialty_field', 'length', 'max' => 1),
            array('year_month', 'length', 'max' => 7),
            array('provider_name, practice_name, address, website', 'length', 'max' => 255),
            array('phone', 'length', 'max' => 32),
            array('date_added', 'safe'),
            array('google_places_optimized, yelp_claimed, completed_scans, data_elements, data_element_errors, missing_listings, review_tally, pagespeed_score, listing_accuracy, overall_score, reviews_average, reviews_grade, zip, trigger, specialty_field, year_month, provider_name, practice_name, address, phone, website, date_added', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, organization_id, provider_id, practice_id, google_places_optimized, yelp_claimed, completed_scans, data_elements, data_element_errors, missing_listings, review_tally, pagespeed_score, listing_accuracy, overall_score, reviews_average, reviews_grade, zip, trigger, specialty_field, year_month, provider_name, practice_name, address, phone, website, date_added', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Get a given month's trend given that momth and the previous month's report
     * @param int $year
     * @param int $month
     * @return boolean
     */
    public static function getTrend($year = '', $month = '')
    {

        if (empty($year)) {
            $year = date('Y');
        }
        if (empty($month)) {
            $month = date('m');
        }

        $date = new DateTime();
        $date->setDate((int)$year, (int)$month, 1);

        $previousMonth = new DateTime();
        $previousMonth->setDate((int)$year, (int)$month, 1);
        $previousMonth->modify('-1 month');

        $sql = sprintf(
            "SELECT s1.id, s1.organization_id, s1.provider_id,
                s1.practice_id, s1.google_places_optimized, s1.yelp_claimed,
                s1.completed_scans, s1.data_elements, s1.data_element_errors,
                s1.missing_listings, s1.review_tally, s1.pagespeed_score,
                s1.listing_accuracy, s1.overall_score, s1.reviews_average,
                s1.reviews_grade, s1.zip, s1.trigger,
                s1.specialty_field, s1.year_month, s1.provider_name,
                s1.practice_name, s1.address, s1.phone,
                s1.website, s1.date_added,
                (s1.overall_score - IF(s2.overall_score IS NULL, s1.overall_score, s2.overall_score)) AS trend
                FROM scan AS s1
                LEFT JOIN scan AS s2
                ON s1.organization_id = s2.organization_id
                AND s1.`trigger`= s2.`trigger`
                AND s2.`year_month`= '%s'
                AND s1.provider_id = s2.provider_id
                WHERE s1.`year_month` = '%s'
                AND s1.`trigger` = 'M';",
            ($previousMonth->format('Y') . '-' . $previousMonth->format('n')),
            ($date->format('Y') . '-' . $date->format('n'))
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();

    }

    /**
     * Get the average of the overall scores for the practices in an organization
     * @param int $organizationId
     * @param string $year
     * @param string $month
     * @return array
     */
    public static function getMRPESByOrganization($organizationId = null, $year = '', $month = '')
    {

        if (empty($organizationId)) {
            return false;
        }

        if (empty($year) || empty($month)) {
            $year = date('Y');
            $month = date('n');
        }

        $sql = sprintf(
            "SELECT ROUND(AVG(op_score)) AS pes, ROUND(AVG(aggregate_rating)) AS review_average
                FROM scan_practice
                WHERE organization_id = %d
                AND YEAR(date_added) = '%d'
                AND MONTH(date_added) = '%d';",
            $organizationId,
            $year,
            $month
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get the average of the overall scores for the providers in an organization
     * @param int $organizationId
     * @return int
     */
    public static function getMRProviderScoreByOrganization($organizationId = null)
    {

        if (empty($organizationId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT AVG(listing_accuracy)
                FROM scan_provider
                INNER JOIN scan_practice
                ON scan_provider.scan_practice_id = scan_practice.id
                WHERE organization_id = %d
                AND YEAR(scan_practice.date_added) = '%d'
                AND MONTH(scan_practice.date_added) = '%d';",
            $organizationId,
            date('Y'),
            date('n')
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get review volume history for an organization (last month and the one before)
     * @param int $organizationId
     * @return array
     */
    public static function getMRRVHistory($organizationId = null)
    {

        if (empty($organizationId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT AVG(aggregate_rating) AS aggregate_rating
                FROM scan_practice
                WHERE organization_id = %d
                AND DATE_FORMAT(date_added, '%%Y-%%m') = '%s'
                ORDER BY id DESC
                LIMIT 1;",
            $organizationId,
            date('Y-m', strtotime('first day of this month'))
        );
        $thisMonth = Yii::app()->db->createCommand($sql)->queryScalar();
        if (!empty($thisMonth)) {
            $thisMonth = round($thisMonth, 1);
        }

        $sql = sprintf(
            "SELECT AVG(aggregate_rating) AS aggregate_rating
                FROM scan_practice
                WHERE organization_id = %d
                AND DATE_FORMAT(date_added, '%%Y-%%m') = '%s'
                ORDER BY id DESC
                LIMIT 1;",
            $organizationId,
            date('Y-m', strtotime('first day of last month'))
        );
        $lastMonth = Yii::app()->db->createCommand($sql)->queryScalar();
        if (!empty($lastMonth)) {
            $lastMonth = round($lastMonth, 1);
        }

        return array($thisMonth, $lastMonth);
    }

    /**
     * Get an array with the list of practices and scan date for the given provider and month
     * @param int $providerId
     * @param string $yearMonth
     * @return array
     */
    public static function getPracticesByProviderAndMonth($providerId, $yearMonth)
    {
        $sql = sprintf(
            "SELECT practice_id, MAX(date_added) as scan_date
                FROM scan
                WHERE `year_month` = '%s' AND provider_id = '%d'
                GROUP BY practice_id
                ORDER BY practice_id ASC;",
            $yearMonth,
            $providerId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }

    /**
     * Get the last scan for this organization
     * @param int $organizationId
     * @return array
     */
    public static function getLastPeriodScanned($organizationId = null)
    {
        $sql = sprintf(
            "SELECT id, organization_id, provider_id, practice_id, google_places_optimized,
                yelp_claimed, completed_scans, data_elements, data_element_errors, missing_listings,
                review_tally, pagespeed_score, listing_accuracy, overall_score,
                reviews_average, reviews_grade, zip, `trigger`, specialty_field, `year_month`,
                provider_name, practice_name, address, phone, website, date_added,
                STR_TO_DATE(`year_month`, '%%Y-%%m') AS scan_period
                FROM scan
                WHERE `year_month` IS NOT NULL AND organization_id = %d
                ORDER BY scan_period DESC LIMIT 1",
            $organizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryRow();

    }

    /**
     * Get all records for this organization at this period
     * @param int $organizationId
     * @param string $period -> all | last | yyyy-m
     * @return array
     */
    public static function getScans($organizationId = null, $period = null)
    {
        if (empty($organizationId)) {
            return false;
        }

        $condition = '';
        if ($period == 'all') {
            $condition = '';
        } elseif ($period == 'last' || empty($period)) {
            // Try to find the last period
            $lastScan = self::getLastPeriodScanned($organizationId);
            if (!empty($lastScan)) {
                $period = $lastScan['year_month'];
            }
            $condition = sprintf(" AND `year_month` = '%s'", $period);
        } else {
            $condition = sprintf(" AND `year_month` = '%s'", $period);
        }

        $sql = sprintf(
            "SELECT id, organization_id, provider_id, practice_id, google_places_optimized,
                yelp_claimed, completed_scans, data_elements, data_element_errors, missing_listings,
                review_tally, pagespeed_score, listing_accuracy, overall_score,
                reviews_average, reviews_grade, zip, `trigger`, specialty_field, `year_month`,
                provider_name, practice_name, address, phone, website, date_added
                FROM scan
                WHERE organization_id = %d %s",
            $organizationId,
            $condition
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();

    }

    /**
     * Get the last scan && scan_results for organization
     * @param int $organizationId
     * @param int $practiceId
     * @param int $providerId
     * @return array
     */
    public static function getLastResults($organizationId = 0, $practiceId = 0, $providerId = 0)
    {
        if ($organizationId == 0) {
            return false;
        }

        $join = '';
        $condition = '';
        $order = 'scan.id DESC';
        if ($practiceId > 0) {
            $condition = sprintf(" AND scan.practice_id = %d AND scan.provider_id IS NULL", $practiceId);
        }

        if ($providerId > 0) {
            $join = "INNER JOIN provider_practice ON provider_practice.provider_id = scan.provider_id";
            $condition .= sprintf(" AND scan.provider_id = %d ", $providerId);
            $order = "primary_location DESC, scan.id DESC";
        }

        if (empty($condition)) {
            return false;
        }

        $sql = sprintf(
            "SELECT scan.* FROM scan %s WHERE organization_id = %d %s ORDER BY %s LIMIT 1",
            $join,
            $organizationId,
            $condition,
            $order
        );
        $scan = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryRow();
        $scanResults = '';
        $dataErrors = 0;
        if (!empty($scan)) {
            $scanId = $scan['id'];

            // Search scan results
            $sql = sprintf(
                "SELECT * FROM scan_result WHERE scan_id = %d ORDER BY id",
                $scanId
            );
            $scanResults = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
            if (!empty($scanResults)) {
                foreach ($scanResults as $k => $v) {
                    $scanResults[$k]['code_name'] = StringComponent::formatListingNames($v['code']);

                    if (
                        empty($scanResults[$k]['match_name']) &&
                        empty($scanResults[$k]['match_address']) &&
                        empty($scanResults[$k]['match_phone'])
                    ) {
                        // missing listing
                    } else {
                        // Count data element errors
                        if (empty($scanResults[$k]['match_name'])) {
                            $dataErrors++;
                        }
                        if (empty($scanResults[$k]['match_address'])) {
                            $dataErrors++;
                        }
                        if (empty($scanResults[$k]['match_phone'])) {
                            $dataErrors++;
                        }
                    }
                }
            }
        }

        return array('scan' => $scan, 'scan_results' => $scanResults, 'data_errors' => $dataErrors);
    }
}
