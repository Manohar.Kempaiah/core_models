<?php

Yii::import('application.modules.core_models.models._base.BaseProspect');

class Prospect extends BaseProspect
{
    /**
     * @var int The sales analysis id that will be used after creating a new prospect
     */
    public $sales_analysis_id;

    /**
     * Pending status.
     */
    const STATUS_PENDING = 'P';

    /**
     * In progress.
     */
    const STATUS_IN_PROGRESS = 'I';

    /**
     * Done.
     */
    const STATUS_DONE = 'D';

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Ensure account id for any new record.
     *
     * @return boolean
     */
    public function beforeValidate()
    {
        if (empty($this->id) && empty($this->account_id)) {
            $this->account_id = Yii::app()->user->id;
        }

        return parent::beforeValidate();
    }

    /**
     * Allow to set a property from data sent over the form.
     *
     * @param mixed $attr
     * @param mixed $value
     * @return boolean
     */
    public function __set($attr, $value)
    {
        if ($attr == 'sales_analysis_id') {
            $this->sales_analysis_id = $value;
        } elseif ($attr == 'attributes' && is_array($value) && isset($value['sales_analysis_id'])) {
            $this->sales_analysis_id = $value['sales_analysis_id'];
        }

        return parent::__set($attr, $value);
    }

    /**
     * Creates a new sales analysis setting and relate it to the new prospect inserted.
     *
     * @return type
     */
    public function afterSave()
    {
        // updates will not do anything here
        if (!$this->isNewRecord) {
            return parent::beforeSave();
        }

        // create a new sales analysis setting and relate it to this new prospect
        $sas = new SalesAnalysisSetting();
        $sas->sales_analysis_id = $this->sales_analysis_id;
        $sas->prospect_id = $this->id;
        $sas->save();

        return parent::beforeSave();
    }

    /**
     * Return prospect stats.
     *
     * @return array
     */
    public function getStats()
    {
        // get stats from mysql (scan)
        $sql = "SELECT
            COUNT(prospect_provider_practice.id) total, -- total to scan
            COUNT(prospect_listing_scan.id) scan, -- total scanned
            COUNT(prospect_listing_scan.scan_request_id) scan_result, -- total of success scans

            ROUND(COUNT(prospect_listing_scan.id) / COUNT(prospect_provider_practice.id) * 100) scan_percent,
            ROUND(COUNT(prospect_listing_scan.scan_request_id) / COUNT(prospect_listing_scan.id) * 100)
            AS scan_result_percent,

            COUNT(prospect_listing_scan.id) - COUNT(prospect_listing_scan.scan_request_id) scan_error,
            100 - ROUND(COUNT(prospect_listing_scan.scan_request_id) / COUNT(prospect_listing_scan.id) * 100)
            AS scan_error_percent
            FROM prospect
            INNER JOIN prospect_provider_practice
            ON prospect_provider_practice.prospect_id = prospect.id
            LEFT JOIN prospect_listing_scan
            ON prospect_listing_scan.prospect_provider_practice_id = prospect_provider_practice.id
            WHERE prospect.id = :prospect_id;";

        return Yii::app()->db->createCommand($sql)->queryRow(true, [':prospect_id' => $this->id]);
    }

    /**
     * Search for the health system into analytics DB.
     *
     * @param string $name
     * @return array
     */
    public static function searchHealthSystem($name = null)
    {
        if (strlen($name)) {
            $sql = 'SELECT DISTINCT health_system_id AS id, health_system_name AS name
                FROM definitive_healthcare.health_system_physicians
                WHERE health_system_name ILIKE :name
                ORDER BY health_system_name
                LIMIT 10';

            $params[':name'] = '%' . $name . '%';
        } else {
            $sql = 'SELECT DISTINCT health_system_id AS id, health_system_name AS name
                FROM definitive_healthcare.health_system_physicians
                WHERE health_system_name ILIKE :name
                ORDER BY health_system_name
                LIMIT 10';

            $params = [];
        }

        return Yii::app()->analyticsRO->createCommand($sql)->queryAll(true, $params);
    }

}
