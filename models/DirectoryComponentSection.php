<?php

Yii::import('application.modules.core_models.models._base.BaseDirectoryComponentSection');

class DirectoryComponentSection extends BaseDirectoryComponentSection
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getDirectoryComponentSectionByName($name)
    {
        // try to find a directory setting by their name
        return DirectoryComponentSection::model()->find(
            "name = :name",
            [
                ':name' => $name
            ]
        );
    }
}
