<?php

Yii::import('application.modules.core_models.models._base.BaseProviderHospital');

class ProviderHospital extends BaseProviderHospital
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    //called on rendering the column for each row
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' .
            $this->provider_id . '" rel="/administration/providerTooltip?id=' .
            $this->provider_id . '" title="' . $this->provider . '">' . $this->provider . '</a>');
    }

    /**
     * Return all hospital associated to a provider
     * @param int $providerId
     * @return array
     */
    public function getDetails($providerId, $cache = true)
    {

        $sql = sprintf(
            "SELECT ph.id, ph.provider_id, ph.practice_id, practice.name AS hospital,
            practice.address, IF(practice.address_2 IS NULL, '', practice.address_2) AS address_2,
            zipcode, city.name AS city, state.name AS state
            FROM provider_hospital AS ph
            INNER JOIN practice
                ON ph.practice_id = practice.id
            LEFT JOIN location
                ON practice.location_id = location.id
            LEFT JOIN city
                ON location.city_id = city.id
            LEFT JOIN state
                ON city.state_id = state.id
            WHERE ph.provider_id = '%s';",
            $providerId
        );

        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Checks a record's ownership
     *
     * @deprecated No longer used by internal code and not recommended.
     *
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_provider
            INNER JOIN provider_hospital
                ON provider_hospital.provider_id = account_provider.provider_id
            WHERE provider_hospital.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        foreach ($postData as $data) {
            $auditAction = '';
            $backendAction = !empty($data['backend_action']) ? $data['backend_action'] : '';
            if ($backendAction == 'delete' && $data['id'] > 0) {
                $model = ProviderHospital::model()->findByPk($data['id']);
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_HOSPITAL";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }
                    $auditAction = 'D';
                }
            } else {

                // Exist this record?
                $providerHospital = ProviderHospital::model()->find(
                    'provider_id=:provider_id AND practice_id=:practice_id',
                    array(':provider_id' => $providerId, ':practice_id' => $data['practice_id'])
                );
                if ($providerHospital) {
                    continue;
                }

                if (!empty($data['id']) && $data['id'] > 0) {
                    $model = ProviderHospital::model()->findByPk($data['id']);
                    $auditAction = 'U';
                } else {
                    $auditAction = 'C';
                    $model = new ProviderHospital;
                }
                $model->provider_id = $providerId;
                $model->practice_id = $data['practice_id'];

                if (!$model->save()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_HOSPITAL";
                    $results['data'] = $model->getErrors();
                    return $results;
                } else {
                    if ($auditAction == 'C') {
                        // return the new item
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                }
            }
            if (!empty($auditAction)) {
                $auditSection = 'ProviderHospital #' . $providerId . ' (#' . $model->practice_id . ')';
                AuditLog::create($auditAction, $auditSection, 'provider_hospital', null, false);
            }
        }
        return $results;
    }
}
