<?php

Yii::import('application.modules.core_models.models._base.BaseResearchModeLog');

class ResearchModeLog extends BaseResearchModeLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Research Practice Provider
     * @param int $accountId
     * @param int $providerId
     * @param int $practiceId
     * @return int
     */
    public static function getResearchPracticeProvider($accountId, $providerId, $practiceId)
    {

        if ($providerId != 0) {
            $strProvPractId = sprintf(" AND provider_id = %d", $providerId);
        } else {
            $strProvPractId = sprintf(" AND practice_id = %d", $practiceId);
        }
        $sql = sprintf(
            "SELECT id
            FROM research_mode_log
            WHERE note IS NOT NULL
            AND owner_account_id = %d
            %s
            AND note_read_by_account_id IS NULL
            LIMIT 1;",
            $accountId,
            $strProvPractId
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get notes from research mode from providers or practices
     * @param string $providerPractice
     * @param int $providerPracticeId
     * @return array
     */
    public static function getResearchNotes($providerPractice, $providerPracticeId)
    {

        $condition = '';
        if ($providerPractice == 'provider') {
            $condition = " provider_id = ". $providerPracticeId . " ";
        } else {
            $condition = " practice_id = ". $providerPracticeId ." ";
        }
        $sql = sprintf(
            "SELECT DATE_FORMAT(a.date_added, '%%m/%%d/%%Y')AS date_added,
            a.user_account_id AS user_id, a.note_read_by_account_id AS user_id_reader,
            a.note_read_date, a.section, b.first_name AS account, a.note,
            b.first_name AS accountWriter, c.first_name AS accountReader, a.id
            FROM research_mode_log a
            LEFT JOIN account b ON a.user_account_id = b.id
            LEFT JOIN account c ON a.note_read_by_account_id = c.id
            WHERE %s
            AND a.note <> ''
            ORDER BY a.date_added DESC, a.section ASC, a.user_account_id ASC;",
            $condition
        );

        return Yii::app()->db->createCommand($sql)->queryAll();

    }

    /**
     * Get notes from research mode from providers or practices
     * @param array $data
     * Required array props:
     * commentId
     * @return array
     */
    public static function saveResearchNote($data)
    {
        // We deconstruct the object params for ease of use
        $commentId = $data['comment_id'];
        $newNote = $data['new_note'];
        $accountId = $data['account_id'];
        $provPrac = $data['prov_prac'];
        $profileId = $data['profile_id'];
        $adminAccountId = $data['admin_account_id'];
        $section = $data['section'];

        if (empty($newNote)) {
            return;
        }
        if ($commentId > 0) {
            // Editing existing note
            $researchModeLog = ResearchModeLog::model()->findByPk($commentId);
            $researchModeLog->note = strip_tags(trim(preg_replace('/\s+/', ' ', $newNote)));
        } else {
            // Adding a new note
            $researchModeLog = new ResearchModeLog();
            $researchModeLog->owner_account_id = $accountId;
            if ($provPrac == 'provider') {
                $researchModeLog->provider_id = $profileId;
                $researchModeLog->practice_id = null;
            } else {
                $researchModeLog->provider_id = null;
                $researchModeLog->practice_id = $profileId;
            }
            $researchModeLog->user_account_id = $adminAccountId;
            $researchModeLog->section = self::researchSectionMenu($section);
            $researchModeLog->note = strip_tags(trim(preg_replace('/\s+/', ' ', $newNote)));
            $researchModeLog->date_added = date("Y-m-d H:i:s");
        }
        $saveResult = $researchModeLog->save();
        if (!$saveResult) {
            $results = [
                'success' => false,
                'http_response_code' => 400,
                'error' => 'DATA_NOT_SAVED',
                'data' => ''
            ];
        } else {
            $results = [
                'success' => true,
                'http_response_code' => 200,
                'error' => '',
                'data' => $saveResult
            ];
        }
        return $results;
    }

    /**
     * Get notes from research mode from providers or practices
     * @param array $data
     * Required array props:
     * researchId
     * userId
     * @return array
     */
    public static function saveResearchNoteRead($data)
    {
        // We deconstruct the object params for ease of use
        $researchId = $data['research_id'];
        $noteReadByAccountId = $data['note_read_by_account_id'];

        // Look for the object to update
        $researchModeLog = ResearchModeLog::model()->findByPK($researchId);

        if ($researchModeLog) {
            // If found, update the model and return the result
            $researchModeLog-> note_read_by_account_id = $noteReadByAccountId;
            $researchModeLog->note_read_date = date('Y-m-d H:i:s');
            $saveResult = $researchModeLog->save();
            if (!$saveResult) {
                $results = [
                    'success' => false,
                    'http_response_code' => 400,
                    'error' => 'DATA_NOT_SAVED',
                    'data' => $saveResult->getErrors()
                ];
            } else {
                $results = [
                    'success' => true,
                    'http_response_code' => 200,
                    'error' => '',
                    'data' => $saveResult
                ];
            }
        } else {
            // If not found, return an error
            $results = [
                'success' => false,
                'http_response_code' => 400,
                'error' => 'ITEM_NOT_FOUND',
                'data' => ""
            ];
        }
        return $results;
    }
    /**
     * Research Option Menu
     * @param string $option
     * @return string
     */
    public static function researchSectionMenu($option)
    {

        $rtnOption = "Provider Information";
        if ($option == "providerBio") {
            $rtnOption = "Biography";
        } elseif ($option == "providerPracticeOfficeHours") {
            $rtnOption = "Practice & Office Hours";
        } elseif ($option == "providerInsurances") {
            $rtnOption = "Insurances Affiliations";
        } elseif ($option == "providerFees") {
            $rtnOption = "Fees & Payment Options";
        } elseif ($option == "providerEducation") {
            $rtnOption = "Education & Training";
        } elseif ($option == "providerExperience") {
            $rtnOption = "Experience & Accolades";
        } elseif ($option == "providerTreatment") {
            $rtnOption = "Procedures Performed";
        } elseif ($option == "providerMedication") {
            $rtnOption = "Medications Prescribed";
        } elseif ($option == "providerPhotos") {
            $rtnOption = "Photos & Videos";
        } elseif ($option == "providerForms") {
            $rtnOption = "Patient Forms";
        } elseif ($option == "providerHospital") {
            $rtnOption = "Affiliated Hospitals";
        } elseif ($option == "providerAuthenticationInfo") {
            $rtnOption = "Authentication Info";
        } elseif ($option == "partnerData") {
            $rtnOption = "Partner Data";
        } elseif ($option == "providerSocialMediaLinks") {
            $rtnOption = "Social Media Links";
        } elseif ($option == "practice") {
            $rtnOption = "Practice Information";
        } elseif ($option == "practiceProviders") {
            $rtnOption = "Associate Providers";
        } elseif ($option == "practicePhotos") {
            $rtnOption = "Photos & Videos";
        } elseif ($option == "practiceOfficeHours") {
            $rtnOption = "Practice Office Hours";
        } elseif ($option == "practiceSpecialties") {
            $rtnOption = "Practice Specialties";
        } elseif ($option == "practiceSocialMediaLinks") {
            $rtnOption = "Social Media Links";
        } elseif ($option == "providerProfile") {
            $rtnOption = "Provider Profile";
        } elseif ($option == "locationProfile") {
            $rtnOption = "Location Profile";
        }

        return $rtnOption;
    }
}
