<?php

Yii::import('application.modules.core_models.models._base.BaseProviderLanguage');

class ProviderLanguage extends BaseProviderLanguage
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Return Provider's name associated
     * @return int|string
     */
    public function __toString()
    {
        if ($this->provider) {
            return trim(
                $this->provider->prefix . ' ' . $this->provider->first_name
                    . ' ' . $this->provider->last_name
            );
        }

        return $this->id;
    }

    //called on rendering the column for each row
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id
            . '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">'
            . $this->provider . '</a>');
    }

    // Make some validations Before save
    public function beforeSave()
    {
        $exists = ProviderLanguage::model()->exists(
            'provider_id=:provider_id AND language_id=:language_id',
            array(':provider_id' => $this->provider_id, ':language_id' => $this->language_id)
        );

        if ($exists) {
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Gets provider's spoken languages
     *
     * @param int $provider_id
     * @return mixed
     */
    public static function getSpokenLanguages($provider_id)
    {
        if ($provider_id > 0) {
            $sql = 'SELECT `language`.id AS id, `language`.name,
                IF(provider_language.id IS NULL, FALSE, TRUE ) AS checked
                FROM `language`
                LEFT JOIN provider_language
                ON `language`.id = provider_language.language_id
                AND provider_id = "' . $provider_id . '"
                ORDER BY `language`.name;';
        } else {
            $sql = 'SELECT `language`.id AS id, `language`.name,
                FALSE AS checked
                FROM `language`
                ORDER BY `language`.name;';
        }

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Gets provider's languages
     *
     * @param int $providerId
     * @return mixed
     */
    public static function getDetails($providerId, $cache = true)
    {
        $result = array();
        if ($providerId > 0) {
            $sql = sprintf(
                "SELECT pl.id, pl.provider_id, pl.language_id, l.name
                FROM provider_language AS pl
                LEFT JOIN `language` AS l ON l.id = pl.language_id
                WHERE pl.provider_id = %d
                ORDER BY l.name;",
                $providerId
            );

            $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

            $result = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
        }
        return $result;
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_provider
            INNER JOIN provider_language
            ON account_provider.provider_id = provider_language.provider_id
            WHERE provider_language.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * Save Data
     * @param string $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        /* BEGIN update spoken languages */
        $selectedProviderLanguage = array();
        if (!empty($postData)) {
            foreach ($postData as $value) {
                $backendAction = !empty($value['backend_action']) ? $value['backend_action'] : '';
                if ($backendAction != 'delete') {
                    $selectedProviderLanguage[$value['language_id']] = 1;
                }
            }
        }
        $storedProviderLanguages = ProviderLanguage::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $providerId)
        );

        if (!empty($storedProviderLanguages)) {
            foreach ($storedProviderLanguages as $spl) {
                $languageId = $spl->language_id;
                if (!isset($selectedProviderLanguage[$languageId])) {
                    if (!$spl->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_LANGUAGE";
                        $results['data'] = $spl->getErrors();
                        return $results;
                    }

                    $auditSection = 'Provider Language provider_id #' . $providerId . ')' .$languageId.')';
                    AuditLog::create('D', $auditSection, 'provider_language', null, false);

                } else {
                    unset($selectedProviderLanguage[$languageId]);
                }
            }
        }

        if (!empty($selectedProviderLanguage)) {
            foreach ($selectedProviderLanguage as $languageId => $value) {
                // add New Selected Language
                if (!ProviderLanguage::model()->exists(
                    'provider_id=:provider_id AND language_id=:language_id',
                    array(
                        ':provider_id' => $providerId,
                        ':language_id' => $languageId
                    )
                )) {
                    $providerLanguage = new ProviderLanguage();
                    $providerLanguage->provider_id = $providerId;
                    $providerLanguage->language_id = $languageId;

                    if ($providerLanguage->save()) {

                        $auditSection = 'Provider Language provider_id #' . $providerId . ')' .$languageId.')';
                        AuditLog::create('C', $auditSection, 'provider_language', null, false);

                        $results['success'] = true;
                        $results['error'] = "";
                        $results['data'][] = (array) $providerLanguage->attributes;
                    } else {
                        $results['success'] = false;
                        $results['error'] = "ERROR_SAVING_PROVIDER_LANGUAGE";
                        $results['data'] = $providerLanguage->getErrors();
                        return $results;
                    }

                }
            }
        }
        return $results;
        /* END update spoken language */

    }
}
