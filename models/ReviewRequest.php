<?php

Yii::import('application.modules.core_models.models._base.BaseReviewRequest');

class ReviewRequest extends BaseReviewRequest
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Some validations before save
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->isClicked()) {
            // The RR was sent and patient open it
            // update the status
            $this->status = 'SUCCESS';
        }
        if (empty($this->send_link) && !empty($this->custom_link)) {
            $this->send_link = 'CUSTOM';
        }

        return parent::beforeSave();
    }

    /**
     * When a RR is sent, update SF values for first, last and count of RR and ARR
     * @return boolean
     */
    public function afterSave()
    {
        if ($this->isNewRecord) {
            // we're creating a RR

            $account = Account::model()->findByPk($this->account_id);
            if (!$account) {
                return parent::afterSave();
            }

            // update first automatic review request sent
            ReviewRequest::updateFirstSent($account);

            // update last automatic review request sent
            ReviewRequest::updateLastSent($account);

            // update automatic review request count
            ReviewRequest::updateCount($account);

            // update first automatic review request sent
            PatientemailCommunications::updateFirstSent($account);

            // update last automatic review request sent
            PatientemailCommunications::updateLastSent($account);

            // update automatic review request count
            PatientemailCommunications::updateCount($account);
        }
        return parent::afterSave();
    }

    /**
     * PADM: Count all of the review requests an account has sent by date
     * @param int $accountId
     * @param int $practiceId
     * @return array
     */
    public static function countAccountReviewRequestsByDate($accountId, $practiceId = 0)
    {

        if (empty($accountId)) {
            return false;
        }

        $practiceCondition = '';
        if ($practiceId > 0) {
            $practiceCondition = sprintf('AND practice_id = %d', $practiceId);
        }

        $hidden = '';
        // check if we're an admin only if we're not in cli
        if (php_sapi_name() != 'cli' && !empty(Yii::app()->session['admin_account_id'])) {
            if (Yii::app()->session['admin_account_id'] == '') {
                $hidden = 'AND hidden = 0';
            }
        }

        // account management values
        $currentAccountOrg = OrganizationAccount::model()->cache(Yii::app()->params['cache_medium'])->find(
            'account_id=:account_id',
            array(':account_id' => $accountId)
        );

        $accountPracticeTable = "";

        if (!empty($currentAccountOrg) && $currentAccountOrg->connection == 'S') {
            $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId, true);
            $accountPracticeTable = sprintf(
                "INNER JOIN
                    (SELECT account_practice.practice_id AS id
                    FROM account_practice
                    INNER JOIN practice
                        ON practice.id = account_practice.practice_id
                    WHERE account_id = %d

                    UNION

                    SELECT account_practice.practice_id AS id
                    FROM provider_practice
                    INNER JOIN account_provider
                        ON provider_practice.provider_id = account_provider.provider_id
                    INNER JOIN account_practice
                        ON provider_practice.practice_id = account_practice.practice_id
                    INNER JOIN practice
                        ON practice.id = account_practice.practice_id
                        AND account_practice.account_id = %d
                    WHERE account_provider.account_id = %d
                        AND account_provider.all_practices = 1
                    ) pr
                    ON review_request.practice_id = pr.id",
                $accountId,
                $ownerAccountId,
                $accountId
            );
            $accountId = $ownerAccountId;
        }

        if (empty($accountId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT COUNT(1) AS requests, DATE_FORMAT(date_added, '%%Y-%%m-%%d') AS `date`
            FROM review_request
            %s
            WHERE account_id = %d
            %s
            %s
            GROUP BY `date`
            ORDER BY `date` ASC;",
            $accountPracticeTable,
            $accountId,
            $practiceCondition,
            $hidden
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get all of the review requests an account has sent
     * @param int $accountId
     * @param string $type -> review_request || provider_rating
     *      provider_rating is the followUpEmail
     * @return array
     */
    public static function getAccountReviewRequests($accountId = 0, $type = 'review_request')
    {

        if (empty($accountId) || $accountId < 0) {
            return false;
        }

        // by default only retrieve requests that are not hidden
        $hidden = 'AND hidden = 0';
        if (php_sapi_name() != 'cli' && !empty(Yii::app()->session['admin_account_id'])) {
            // if we're not in cli (so we're in PADM) and we're an admin, then retrieve all review requests
            $hidden = '';
        }

        // account management values
        $accountInfo = Account::getInfo($accountId, true, false);

        $accountPracticeTable = " practice AS pr ";

        if (!empty($accountInfo) && $accountInfo['connection_type'] == 'S') {
            // special case for Staff account
            $ownerAccountId = $accountInfo['owner_account_id'];
            $accountPracticeTable = sprintf(
                "(SELECT account_practice.practice_id AS id, practice.name
                FROM account_practice
                INNER JOIN practice ON practice.id = account_practice.practice_id
                WHERE account_id = %d
                UNION
                SELECT account_practice.practice_id AS id, practice.name
                FROM provider_practice
                INNER JOIN account_provider ON provider_practice.provider_id = account_provider.provider_id
                INNER JOIN account_practice ON provider_practice.practice_id = account_practice.practice_id
                INNER JOIN practice ON practice.id = account_practice.practice_id
                AND account_practice.account_id = %d
                WHERE account_provider.account_id = %d
                AND account_provider.all_practices = 1
                ) pr ",
                $accountId,
                $ownerAccountId,
                $accountId
            );
            $accountId = $ownerAccountId;
        }

        $allAcountId = OrganizationAccount::getAllAccountId($accountId);
        $allAcountId = implode(', ', $allAcountId);

        if (empty($allAcountId)) {
            return false;
        }

        $clickedBy = "IF(rr.google_clicked =1 AND yelp_clicked = 1, 'Both',
                        IF(rr.google_clicked =1 , 'Google',
                            IF(rr.yelp_clicked =1 , 'Yelp',
                                IF(rr.ddc_clicked =1 , 'Doctor',
                                    IF(rr.custom_clicked =1 , 'Custom', 'n/a')
                                )
                            )
                        )
                    ) AS clicked ";

        $sql = sprintf(
            "SELECT
                rr.id, rr.account_id, rr.practice_id, rr.provider_id, rr.patient_id, rr.google_clicked,
                rr.yelp_clicked, rr.ddc_clicked, rr.custom_clicked, rr.hidden, rr.status, rr.send_via, rr.source,
                DATE_FORMAT(rr.date_added, '%%m/%%d/%%y') AS date_added_f,
                DATE_FORMAT(rr.date_added, '%%h:%%i %%p') AS time_added_f,
                rr.send_link, pr.name AS practice_name, provider.first_last AS provider_name,
                pa.first_name, pa.last_name, IF(i.id IS NULL, pa.mobile_phone, i.patient_phone_number) as mobile_phone,
                IF(i.id IS NULL, pa.email, i.patient_email) AS email ,
                cd.communication_id, cd.communication_table,
                %s
            FROM
                review_request AS rr
                INNER JOIN %s ON rr.practice_id = pr.id
                LEFT JOIN waiting_room_invite i ON rr.id = i.review_request_id
                LEFT JOIN provider ON rr.provider_id = provider.id
                LEFT JOIN patient AS pa ON rr.patient_id = pa.id
                LEFT JOIN communication_description AS cd ON reference_id = rr.id AND reference_table = '%s'
            WHERE
                rr.account_id IN (%s)
                %s
            GROUP BY
                rr.id
            ORDER BY
                rr.date_added DESC;",
            $clickedBy,
            $accountPracticeTable,
            $type,
            $allAcountId,
            $hidden
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Return the query for getting a ReviewRequest report
     * @param string $dateCondition
     * @return string
     */
    public static function getReport($dateCondition)
    {

        return sprintf(
            'SELECT organization.organization_name, CONCAT("' . Yii::app()->salesforce->applicationUri . '",
            salesforce_id) AS salesforce_link, patient.first_name AS patient_first,
            patient.last_name AS patient_last, patient.mobile_phone AS patient_phone, patient.email AS patient_email,
            send_via, DATE_FORMAT(DATE(review_request.date_added), "%%m/%%d/%%y") AS sent_date,
            TIME_FORMAT(TIME(review_request.date_added), "%%h:%%i %%p") AS sent_time,
            send_link, IF(google_clicked, "Yes", "No") AS google_clicked,
            IF(yelp_clicked, "Yes", "No") AS yelp_clicked,
            google_link, yelp_link
            FROM review_request
            INNER JOIN organization_account
                ON organization_account.account_id = review_request.account_id
                AND `connection` = "O"
            INNER JOIN organization
                ON organization_account.organization_id = organization.id
            INNER JOIN patient
                ON review_request.patient_id = patient.id
            %s',
            $dateCondition
        );
    }

    /**
     * Get top 50 accounts per number of review requests in last 7 days
     * @return string
     */
    public static function getWeeklyRockstarCustomersReport()
    {
        return "SELECT account.id AS account_id, CONCAT(account.first_name, ' ', account.last_name) AS account_name,
            account.organization_name AS account_organization_name,
            practice.name AS practice_name, COUNT(1) AS review_request
            FROM review_request
            INNER JOIN account
                ON review_request.account_id = account.id
            INNER JOIN practice
                ON review_request.practice_id = practice.id
            WHERE DATEDIFF(CURDATE(), review_request.date_added) BETWEEN 1 AND 7
            GROUP BY review_request.account_id
            ORDER BY review_request DESC
            LIMIT 50;";
    }

    /**
     * Get top 50 accounts per number of review requests that were clicked in last 7 days
     * @return string
     */
    public static function getWeeklyRockstarCustomersReportClicked()
    {
        return "SELECT account.id AS account_id, CONCAT(account.first_name, ' ', account.last_name) AS account_name,
            account.organization_name AS account_organization_name,
            practice.name AS practice_name, COUNT(1) AS review_request
            FROM review_request
            INNER JOIN account
                ON review_request.account_id = account.id
            INNER JOIN practice
                ON review_request.practice_id = practice.id
            WHERE DATEDIFF(CURDATE(), review_request.date_added) BETWEEN 1 AND 7
                AND (google_clicked OR yelp_clicked)
            GROUP BY review_request.account_id
            ORDER BY review_request DESC
            LIMIT 50;";
    }

    /**
     * Update Salesforce: first review request sent: First_ReviewRequest_Sent__c
     * Fired from ReviewRequest::afterSave
     * @param Account $account
     * @return boolean
     */
    public static function updateFirstSent($account)
    {

        // do we have an account?
        if (empty($account) || !$account instanceof Account) {
            // no, leave
            return false;
        }

        // find the date this account sent its first review request ever
        $sql = sprintf(
            "SELECT MIN(review_request.date_added)
            FROM review_request
            WHERE account_id = %d;",
            $account->id
        );
        $firstDate = Yii::app()->db->createCommand($sql)->queryScalar();

        // do we have a date, and is it now? (otherwise we've already updated SF)
        if (!empty($firstDate) && date('Y-m-d H:i', strtotime($firstDate)) == date('Y-m-d H:i')) {

            // yes, then look up the organization
            $organizationObj = $account->getOwnedOrganization();
            if (!empty($organizationObj)) {
                // update SF
                $sfData = array(
                    'salesforce_id' => $organizationObj->salesforce_id,
                    'field' => 'First_ReviewRequest_Sent__c',
                    'value' => date('c', strtotime($firstDate))
                );
                return SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);
            }
        }

        return false;
    }

    /**
     * Update Salesforce: last review request sent: Last_ReviewRequest_Sent__c
     * Fired from ReviewRequest::afterSave
     * @param Account $account
     * @return boolean
     */
    public static function updateLastSent($account)
    {

        // do we have an account?
        if (empty($account) || !$account instanceof Account) {
            // no, leave
            return false;
        }

        // find the date this account sent its last review_request (should be now)
        $sql = sprintf(
            "SELECT MAX(review_request.date_added) FROM review_request WHERE account_id = %d;",
            $account->id
        );
        $lastDate = Yii::app()->db->createCommand($sql)->queryScalar();

        // do we have a date? (we always should)
        if (!empty($lastDate)) {

            // yes, then look up the organization
            $organizationObj = $account->getOwnedOrganization();
            if (!empty($organizationObj)) {
                // update SF
                $sfData = array(
                    'salesforce_id' => $organizationObj->salesforce_id,
                    'field' => 'Last_ReviewRequest_Sent__c',
                    'value' => date('c', strtotime($lastDate))
                );
                return SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);
            }
        }

        return false;
    }

    /**
     * Update Salesforce: count of review requests sent: Total_ReviewRequest_Sent__c
     * Fired from ReviewRequest::afterSave
     * @param Account $account
     * @return boolean
     */
    public static function updateCount($account)
    {

        // do we have an account?
        if (empty($account) || !$account instanceof Account) {
            // no, then leave
            return false;
        }

        // count how many review_requests this account has sent in total
        $sql = sprintf("SELECT COUNT(1) FROM review_request WHERE account_id = %d;", $account->id);
        (int) $count = Yii::app()->db->createCommand($sql)->queryScalar();

        // do we have a count? (we always should)
        if (!empty($count)) {
            // yes, then look up the organization
            $organizationObj = $account->getOwnedOrganization();
            if (!empty($organizationObj)) {
                // update SF
                $sfData = array(
                    'salesforce_id' => $organizationObj->salesforce_id,
                    'field' => 'Total_ReviewRequest_Sent__c',
                    'value' => $count
                );
                return SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);
            }
        }

        return false;
    }

    /**
     * Gets an account's first review request date
     * @param int $accountId
     * @return string
     */
    public static function getAccountFirstReviewRequest($accountId)
    {
        $sql = sprintf("SELECT MIN(date_added) FROM review_request WHERE account_id = %d;", $accountId);
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Gets an account's last review request date
     * @param int $accountId
     * @return string
     */
    public static function getAccountLastReviewRequest($accountId)
    {
        $sql = sprintf("SELECT MAX(date_added) FROM review_request WHERE account_id = %d;", $accountId);
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Count an account's review requests
     * @param int $accountId
     * @return string
     */
    public static function countAccountReviewRequests($accountId)
    {
        $sql = sprintf("SELECT COUNT(1) FROM review_request WHERE account_id = %d;", $accountId);
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Count patient messaging sent for account
     * @param int $accountId
     * @param int $organizationId
     * @return array
     */
    public static function totalPatientMessagesSent($accountId = 0, $organizationId = 0)
    {

        if ($organizationId == 0) {
            $accountInfo = Account::getInfo($accountId);
            $organizationId = !empty($accountInfo['organization_id']) ? $accountInfo['organization_id'] : 0;
        }

        if ($accountId == 0 || $organizationId == 0) {
            return [];
        }

        $sql = sprintf(
            "SELECT
            SUM(google) AS sum_google, SUM(google_clicked) AS google_clicked,
              CASE WHEN SUM(google) > 0 THEN ROUND(SUM(google_clicked)/SUM(google)*100,0) ELSE 0 END AS google_ctr,
            SUM(yelp) AS sum_yelp, SUM(yelp_clicked) AS yelp_clicked,
              CASE WHEN SUM(yelp) > 0 THEN ROUND(SUM(yelp_clicked)/SUM(yelp)*100,0) ELSE 0 END AS yelp_ctr,
            SUM(ddc) AS sum_ddc, SUM(ddc_clicked) AS ddc_clicked,
              CASE WHEN SUM(ddc) > 0 THEN ROUND(SUM(ddc_clicked)/SUM(ddc)*100,0) ELSE 0 END AS ddc_ctr
            FROM (

                -- Automatic Review Request: Practice / Provider at Practice level
                SELECT
                CASE WHEN google_link IS NULL THEN 0 ELSE 1 END AS google,
                CASE WHEN yelp_link IS NULL THEN 0 ELSE 1 END AS yelp,
                CASE WHEN ddc_link IS NULL THEN 0 ELSE 1 END AS ddc,
                google_clicked,
                yelp_clicked,
                ddc_clicked
                FROM review_request
                INNER JOIN account_practice
                    ON account_practice.practice_id = review_request.practice_id
                INNER JOIN organization_account
                    ON account_practice.account_id = organization_account.account_id
                LEFT JOIN provider_practice
                    ON provider_practice.practice_id = review_request.practice_id
                    AND provider_practice.provider_id = review_request.provider_id
                LEFT JOIN account_provider
                    ON account_provider.provider_id = review_request.provider_id
                    AND account_provider.account_id = organization_account.account_id
                    AND account_provider.account_id = account_practice.account_id
                WHERE organization_account.organization_id = %d
                    AND organization_account.account_id = %d
                    AND `status` = 'SUCCESS'
                    AND (account_provider.id IS NOT NULL OR review_request.provider_id IS NULL)
            UNION ALL
            -- Manual Review Request: Only at Practice level
                SELECT
                    CASE
                        WHEN (auto_rr_send_link IN ('BOTH','GOOGLE')
                            OR (auto_rr_send_link = 'DYNAMIC' AND auto_rr_send_link_detail LIKE '%%type=Rw==%%')) THEN 1
                        ELSE 0
                        END AS google,
                    CASE
                        WHEN (auto_rr_send_link IN ('YELP')
                            OR (auto_rr_send_link = 'DYNAMIC' AND auto_rr_send_link_detail LIKE '%%type=WQ==%%')) THEN 1
                        ELSE 0
                        END AS yelp,
                    CASE
                        WHEN auto_rr_send_link = 'DDC' THEN 1
                        ELSE 0
                        END AS ddc,
                    0 AS google_clicked,
                    0 AS yelp_clicked,
                    0 AS ddc_clicked
                FROM patientemail
                    INNER JOIN account_practice
                        ON account_practice.practice_id = patientemail.practice_id
                    INNER JOIN organization_account
                        ON account_practice.account_id = organization_account.account_id
                WHERE organization_account.organization_id = %d
                    AND organization_account.account_id = %d
            UNION ALL
            -- Digital Review Request: Practice / Provider at Practice level
                SELECT 0 AS google,
                    0 AS yelp,
                    COUNT(review_request_external.id) AS ddc,
                    0 AS google_clicked,
                    0 AS yelp_clicked,
                    SUM(ddc_clicked) AS ddc_clicked
                FROM prd01.review_request_external
                    INNER JOIN account_practice
                        ON account_practice.practice_id = review_request_external.practice_id
                    INNER JOIN organization_account
                        ON account_practice.account_id = organization_account.account_id
                    LEFT JOIN provider_practice
                        ON provider_practice.practice_id = review_request_external.practice_id
                        AND provider_practice.provider_id = review_request_external.provider_id
                    LEFT JOIN account_provider
                        ON account_provider.provider_id = review_request_external.provider_id
                        AND account_provider.account_id = organization_account.account_id
                        AND account_provider.account_id = account_practice.account_id
                WHERE organization_account.organization_id = %d
                    AND organization_account.account_id = %d
                    AND (account_provider.id IS NOT NULL OR review_request_external.provider_id IS NULL))
                        AS total_review_request",
            $organizationId,
            $accountId,
            $organizationId,
            $accountId,
            $organizationId,
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Send review request to a patient
     * @param array $data
     * @return json
     */
    public function resendReviewRequest()
    {
        $mailSent = false;
        $smsSent = false;

        if ($this->send_via === 'EMAIL' || $this->send_via === 'BOTH') {
            $mailSent = (bool) MailComponent::sendReviewRequest($this);
        }
        if ($this->send_via === 'SMS' || $this->send_via === 'BOTH') {
            $smsSent = (bool) SmsComponent::sendReviewRequest($this);
        }

        // Does the RR was sent?
        if ($mailSent || $smsSent) {

            // Yes
            $patient = Patient::model()->findByPk($this->patient_id);
            $practice = Practice::model()->findByPk($this->practice_id);
            // Create log
            AuditLog::create(
                'C',
                'Review request resent from ' . $practice->name . ' to patient #' . $patient->id .
                    ' (' . $patient->first_name . ' ' . $patient->last_name . ')',
                null,
                null,
                false
            );

            // then try to create the entry in review_request
            $newRR = new ReviewRequest;
            $newRR->account_id = $this->account_id;
            $newRR->practice_id = $this->practice_id;
            $newRR->provider_id = $this->provider_id;
            $newRR->patient_id = $this->patient_id;
            $newRR->status = 'SUCCESS';
            $newRR->send_via = $this->send_via;
            $newRR->send_link = $this->send_link;
            $newRR->source = 'PADM';
            $newRR->date_added = date('Y-m-d H:i:s');
            $newRR->google_link = $this->google_link;
            $newRR->yelp_link = $this->yelp_link;
            $newRR->ddc_link = $this->ddc_link;
            $newRR->custom_link = $this->custom_link;

            if ($newRR->save()) {
                // update the review request status
                $results['success'] = true;
                $results['error'] = '';
                $results['data'] = $newRR->attributes;
            } else {
                // fail
                Yii::log(
                    'Could not save ReviewRequest: ' . serialize($newRR->getErrors()),
                    'error',
                    'padm.reviewrequest.resend'
                );
                $results['success'] = false;
                $results['error'] = 'COULD_NOT_SAVE_REVIEWREQUEST';
                $results['data'] = $newRR->getErrors();
            }
        } else {
            $results['success'] = false;
            $results['error'] = 'COULD_NOT_RESEND_REVIEWREQUEST';
            $results['data'] = 'No email and/or sms was sent';
        }

        return $results;
    }

    /**
     * Send review request to a patient
     * @param array $data
     * @return json
     */
    public static function sendReviewRequest($data = array())
    {

        $results['success'] = false;
        $results['error'] = '';
        $results['data'] = '';
        $practiceId = Common::get($data, 'practiceId', null);
        $providerId = Common::get($data, 'providerId', null);
        $accountId = Common::get($data, 'accountId', 0);
        $sendTo = $data['sendTo']; // GOOGLE - YELP - DDC - CUSTOM
        $overallRating =  Common::get($data, 'overall_rating', null); // coming from virtualVisit
        $accountDeviceId = Common::get($data, 'accountDeviceId', 0);
        $source = Common::get($data, 'source', null);
        $data['coming_from'] = !empty($data['coming_from']) ? $data['coming_from'] : '';
        $data['send_via'] = !empty($data['send_via']) ? $data['send_via'] : ''; // EMAIL, SMS, BOTH, N/A, empty()
        // $data['partner_site_id'] = !empty($data['partner_site_id']) ? $data['partner_site_id'] : null;

        // get owner
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId, true);

        // prepare patient data
        $patientData = new stdClass();
        $patientData->email  = StringComponent::validateEmail($data['patientEmail']);
        $patientData->firstName  = Common::get($data, 'patientFirstName', null);
        $patientData->lastName  = Common::get($data, 'patientLastName', null);
        $patientData->lang_id = 'en';
        $patientData->account_device_id = $accountDeviceId;
        $patientData->location_id = 999999;
        $patientData->phone = Common::get($data, 'patientMobilePhone', null);

        // Have access to this provider?
        if (!empty($providerId)) {
            $canAccess = AccountProvider::canAccess($ownerAccountId, $providerId);
            if (!$canAccess) {
                // Provider not exists
                $results['success'] = false;
                $results['error'] = 'PROVIDER_NOT_ALLOWED';
                $results['data'] = 'This provider is not in your allowed providers.';
                return $results;
            }
        }

        // try to find primary practice
        if (empty($practiceId) && !empty($data['providerId'])) {
            $practiceId = Provider::getPrimaryPractice($data['providerId'], true, $ownerAccountId);
        }

        if (!empty($practiceId)) {
            // Have access to this practice?
            $canAccess = AccountPractice::canAccess($ownerAccountId, $practiceId);
            if (!$canAccess) {
                // Provider not exists
                $results['success'] = false;
                $results['error'] = 'PRACTICE_NOT_ALLOWED';
                $results['data'] = 'This practice is not in your allowed practices.';
                return $results;
            }

            // Look for the practice
            $practiceObj = Practice::model()->cache(Yii::app()->params['cache_medium'])->findByPk($practiceId);
            $patientData->location_id = $practiceObj->location_id;

            // Looking for the practice_details
            $pd = PracticeDetails::model()->cache(Yii::app()->params['cache_medium'])->find(
                'practice_id=:practiceId',
                array(':practiceId' => $practiceId)
            );
            if (!$pd) {
                // practice details missing, generate them
                $pd = new PracticeDetails;
                $pd->practice_id = $practiceId;
                $pd->save();
            }
        }
        // create or update a patient
        $patientData = Patient::createOrUpdatePatient($patientData);
        // Do we have a valid patient?
        $patient = $patientData['arrPatient'];
        if (empty($patient->id)) {
            // No
            $results['success'] = false;
            $results['error'] = 'PATIENT_NOT_EXISTS';
            $results['data'] = 'Patient not exists.';
            return $results;
        }

        if (!empty($practiceId)) {
            // Look for the practice<->patient relation
            $practicePatient = PracticePatient::model()->exists(
                'patient_id=:patientId AND practice_id=:practiceId',
                array(':patientId' => $patient->id, ':practiceId' => $practiceId)
            );
            if (!$practicePatient) {
                // link patient to practice if not already linked
                $practicePatient = new PracticePatient;
                $practicePatient->patient_id = $patient->id;
                $practicePatient->practice_id = $practiceId;
                $practicePatient->primary_practice = 1;
                $practicePatient->save();
            }
        }

        if ($sendTo == 'CUSTOM') {

            // Process sendTo CUSTOM
            if (empty($data['customUrl'])) {
                $results['success'] = false;
                $results['error'] = 'EMPTY_CUSTOM_URL';
                $results['data'] = 'No review links available. custom_url is empty.';
                return $results;
            }

            $shortCustomUrl = StringComponent::shortenURL($data['customUrl']);
            $arrReviewLinks = array(
                'custom' => $shortCustomUrl,
                'custom_id' => $practiceId,
            );
        } elseif ($sendTo == 'DDC' && $accountDeviceId > 0) {

            // send to Doctor ReviewRequest (digital device)
            $accountDevice = AccountDevice::model()
                ->cache(Yii::app()->params['cache_medium'])
                ->findByPk($accountDeviceId);

            if (empty($practiceId)) {
                $practiceId = $accountDevice->practice_id;
            }
            // Adding encoded params to the url
            $arrParams['provider_id'] = $providerId;
            $arrParams['patient_first_name'] = $patient->first_name;
            $arrParams['patient_last_name'] = $patient->last_name;
            $arrParams['patient_email'] = $patient->isFakeEmail() ? '' : $patient->email;
            $arrParams['patient_phone'] = $patient->mobile_phone;
            $arrParams['coming_from'] = $data['coming_from'];
            if (!empty($overallRating)) {
                $arrParams['overall_rating'] = $overallRating;
            }

            // Adding key for unique links
            $key = ['uid' => StringComponent::generateGUID()];
            $arrReviewLinks['ddc'] = $accountDevice->getReviewHubUrl($arrParams, $key);

            $arrReviewLinks['ddc_id'] = $accountDeviceId;
        } else {

            // Send to GOOGLE or YELP
            if (
                !empty($data['providerPracticeListingId'])
                && ($data['source'] == 'VirtualVisit' || $data['source'] == 'IB_DASHBOARD')
            ) {
                $ppl = ProviderPracticeListing::model()->findByPk($data['providerPracticeListingId']);
                $practiceId = $ppl->practice_id;
                if (empty($ppl->shortened_url)) {
                    $ppl->shortened_url = StringComponent::shortenURL($ppl->url);
                    $ppl->save();
                }
                if ($data['sendTo'] == 'YELP') {
                    $arrReviewLinks['yelp'] = $ppl->shortened_url;
                    $arrReviewLinks['yelp_id'] = $ppl->id;
                } elseif ($data['sendTo'] == 'GOOGLE') {
                    if ($source == 'Companion') {
                        $arrReviewLinks['google_pp'] = $ppl->shortened_url;
                        $arrReviewLinks['google_id_pp'] = $ppl->id;
                    } else {
                        // PADM v2 RR
                        $arrReviewLinks['google+'] = $ppl->shortened_url;
                        $arrReviewLinks['google+_id'] = $ppl->id;
                    }
                }
            } elseif (empty($providerId)) {
                // Is at practice Level
                $arrReviewLinks = ProviderPracticeListing::getReviewsLink(null, $practiceId);
            } else {
                // Is provider@practice level
                $arrReviewLinks = ProviderPracticeListing::getProviderReviewLinks($providerId, $practiceId);
            }

            if (
                $data['sendTo'] == 'GOOGLE'
                && empty($data['providerPracticeListingId'])
                && !empty($data['customUrl'])
                && ($data['source'] == 'VirtualVisit' || $data['source'] == 'IB_DASHBOARD')
            ) {

                if (empty($data['customUrl'])) {
                    $results['success'] = false;
                    $results['error'] = 'EMPTY_CUSTOM_URL';
                    $results['data'] = 'No review links available. custom_url is empty.';
                    return $results;
                }

                $shortCustomUrl = StringComponent::shortenURL($data['customUrl']);
                if (!empty($providerId)) {
                    $arrReviewLinks['google_pp'] = $shortCustomUrl;
                    $arrReviewLinks['google_id_pp'] = $data['waiting_room_invite_id'];
                } else {
                    $arrReviewLinks['google+'] = $shortCustomUrl;
                    $arrReviewLinks['google+_id'] = $data['waiting_room_invite_id'];
                }
            }
        }

        $yelpLink = $googleLink = $ddcLink = $customLink = '';
        if (!empty($arrReviewLinks['yelp'])) {
            $yelpLink = $arrReviewLinks['yelp'];
            $yelpId = $arrReviewLinks['yelp_id'];
        }
        // Do we have providerAtPractice link?
        if (!empty($arrReviewLinks['google_pp'])) {
            // Yes
            $googleLink = $arrReviewLinks['google_pp'];
            $googleId = $arrReviewLinks['google_id_pp'];
        } elseif (!empty($arrReviewLinks['google+'])) {
            // PADM v2 RR
            $googleLink = $arrReviewLinks['google+'];
            $googleId = $arrReviewLinks['google+_id'];
        }

        if ($sendTo == 'GOOGLE') {
            $yelpLink = '';
        } elseif ($sendTo == 'YELP') {
            $googleLink = '';
        } elseif ($sendTo == 'DDC') {
            if (!empty($arrReviewLinks['ddc'])) {
                $ddcLink = $arrReviewLinks['ddc'];
                $ddcId = $arrReviewLinks['ddc_id'];
            }
        } elseif ($sendTo == 'CUSTOM') {
            if (!empty($arrReviewLinks['custom'])) {
                $customLink = $arrReviewLinks['custom'];
                $customId = $arrReviewLinks['custom_id'];
            }
        } elseif ($sendTo == 'DYNAMIC' && !empty($yelpLink) && !empty($googleLink)) {

            // find out current ratings
            $googleRatings = 0;
            $yelpRatings = 0;
            $googleAvg = 0;
            $yelpAvg = 0;
            $google3m = null;
            $yelp3m = null;
            $practiceScan = ScanPractice::getLatestByPractice($practiceId);
            // did we find a practice scan?
            if (!empty($practiceScan)) {
                // yes: retrieve its data
                $googleRatings = $practiceScan['google_reviews'];
                $yelpRatings = $practiceScan['yelp_reviews'];
                $googleAvg = $practiceScan['googleAvg'];
                $yelpAvg = $practiceScan['yelpAvg'];
                $google3m = $practiceScan['google3m'];
                $yelp3m = $practiceScan['yelp3m'];
            }

            // decide which link to send based on email address and number of reviews
            $dynamicLink = Common::getReviewLink(
                $patient->email,
                $googleRatings,
                $yelpRatings,
                $googleAvg,
                $yelpAvg,
                $google3m,
                $yelp3m
            );

            if ($dynamicLink == 'GOOGLE') {
                // send only g+, so delete yelp
                $yelpLink = '';
            } else {
                // send only yelp, so delete google
                $googleLink = '';
            }
        }

        // do we have something to send? otherwise fail
        $mailSent = $smsSent = false;
        if (!empty($googleLink) || !empty($yelpLink) || !empty($ddcLink) || !empty($customLink)) {

            // first try to create the entry in the audit_log
            $auditSection = 'Review request sent to patient #' . $patient->id .
                ' (' . $patient->first_name . ' ' . $patient->last_name . ') (' . $sendTo . ')';
            AuditLog::create('C', $auditSection, 'review_request', null, false);

            // then try to create the entry in review_request
            $rr = new ReviewRequest;
            $rr->account_id = $accountId;
            $rr->practice_id = $practiceId;
            $rr->provider_id = $providerId;
            $rr->patient_id = $patient->id;

            $rr->status = $data['source'] == 'VirtualVisit' ? 'SUCCESS' : 'FAILURE';

            $fakeEmailAddress = $patient->isFakeEmail();
            $patient->mobile_phone = trim($patient->mobile_phone);
            $patient->email = trim($patient->email);

            // If we have send_via as params, use it
            $rr->send_via = $data['send_via'];

            if (empty($rr->send_via)) {
                if (!empty($patient->email) && !$fakeEmailAddress && !empty($patient->mobile_phone)) {
                    $rr->send_via = 'BOTH';
                } elseif (!empty($patient->email) && !$fakeEmailAddress) {
                    $rr->send_via = 'EMAIL';
                } elseif (!empty($patient->mobile_phone)) {
                    $rr->send_via = 'SMS';
                } else {
                    $rr->send_via = 'N/A';
                }
            }
            $rr->send_link = $sendTo;
            $rr->source = $source;
            $rr->date_added = date('Y-m-d H:i:s');

            if ($rr->save()) {

                $customFields = array();
                $customFields['send_to'] = $sendTo;
                $customFields['template'] = !empty($data['template']) ? $data['template'] : '';
                $customFields['source'] = !empty($data['source']) ? $data['source'] : '';
                $rrLink = '';
                // now that we've created the review_request entry, update it with correct id, insert links
                if (!empty($googleLink)) {
                    // create redirected link (so we can detect the click)
                    $googleLink = StringComponent::shortenURL(
                        'https://' . Yii::app()->params->servers['dr_pro_hn'] . '/site/reviewRequestRedirect'
                            . '?id=' . base64_encode($rr->id)
                            . '&type=' . base64_encode('G')
                            . '&to=' . base64_encode(urlencode($googleLink))
                            . '&toid=' . $googleId
                    );
                    $rr->google_link = $googleLink;
                    $rrLink = $googleLink;
                }

                if (!empty($yelpLink)) {
                    // create redirected link (so we can detect the click)
                    $yelpLink = StringComponent::shortenURL(
                        'https://' . Yii::app()->params->servers['dr_pro_hn'] . '/site/reviewRequestRedirect'
                            . '?id=' . base64_encode($rr->id)
                            . '&type=' . base64_encode('Y')
                            . '&to=' . base64_encode(urlencode($yelpLink))
                            . '&toid=' . $yelpId
                    );
                    $rr->yelp_link = $yelpLink;
                    $rrLink = $yelpLink;
                }

                if (!empty($ddcLink)) {
                    // create redirected link (so we can detect the click)
                    $ddcLink = StringComponent::shortenURL(
                        'https://' .  Yii::app()->params->servers['dr_pro_hn']  . '/site/reviewRequestRedirect'
                            . '?id='  . base64_encode($rr->id)
                            . '&type=' . base64_encode('D')
                            . '&to=' . base64_encode(urlencode($ddcLink))
                            . '&toid=' . $ddcId
                    );
                    $rr->rr_guid = $key['uid'];
                    $rr->ddc_link = $ddcLink;
                    $rrLink = $ddcLink;
                }

                if (!empty($customLink)) {
                    // create redirected link (so we can detect the click)
                    $customLink = StringComponent::shortenURL(
                        'https://' .  Yii::app()->params->servers['dr_pro_hn']  . '/site/reviewRequestRedirect'
                            . '?id='  . base64_encode($rr->id)
                            . '&type=' . base64_encode('C')
                            . '&to=' . base64_encode(urlencode($customLink))
                            . '&toid=' . $customId
                    );
                    $rr->custom_link = $customLink;
                    $rrLink = $customLink;
                }

                $rr->save();
                if ($data['coming_from'] == 'VirtualVisit' && !empty($rrLink)) {
                    // at this point, we have to return the RR->link
                    $data['rr_link'] = $rrLink;
                    $data['rr_id'] = $rr->id;
                    return array(
                        'success' => true,
                        'error' => false,
                        'data' => $data
                    );
                }
                // we have the right info in review_request, send email and/or sms
                if (!empty(trim($patient->email)) && !$fakeEmailAddress) {
                    switch ($sendTo) {
                        case 'GOOGLE':
                            // If there's custom fields for Google
                            if (!empty($data['googleReviewEmailTitle'])) {
                                $customFields['emailTitle'] = $data['googleReviewEmailTitle'];
                            }
                            if (!empty($data['googleReviewEmailBody'])) {
                                $customFields['emailBody'] = $data['googleReviewEmailBody'];
                            }
                            break;
                        case 'YELP':
                            // If there's custom fields for Yelp
                            if (!empty($data['yelpReviewEmailTitle'])) {
                                $customFields['emailTitle'] = $data['yelpReviewEmailTitle'];
                            }
                            if (!empty($data['yelpReviewEmailBody'])) {
                                $customFields['emailBody'] = $data['yelpReviewEmailBody'];
                            }
                            break;
                        default:
                            // If there's custom fields for DDC Review Request
                            if (!empty($data['reviewRequestEmailTitle'])) {
                                $customFields['emailTitle'] = $data['reviewRequestEmailTitle'];
                            }
                            if (!empty($data['reviewRequestEmailBody'])) {
                                $customFields['emailBody'] = $data['reviewRequestEmailBody'];
                            }
                            break;
                    }
                }

                //
                // Send ReviewRequest via EMAIL
                //
                if (
                    ($rr->send_via == 'BOTH' || $rr->send_via == 'EMAIL')
                    && !empty($patient->email) && !$fakeEmailAddress
                ) {
                    $mailSent = (bool) MailComponent::sendReviewRequest($rr, $customFields);
                }

                //
                // Send ReviewRequest via SMS
                //
                if (
                    ($rr->send_via == 'BOTH' || $rr->send_via == 'SMS')
                    &&  !empty($patient->mobile_phone)
                ) {

                    // If there's custom fields for Yelp
                    if (!empty($data['yelpReviewSmsBody'])) {
                        $customFields['smsBody'] = $data['yelpReviewSmsBody'];
                    }
                    // If there's custom fields for Google
                    if (!empty($data['googleReviewSmsBody'])) {
                        $customFields['smsBody'] = $data['googleReviewSmsBody'];
                    }
                    // If there's custom fields for DDC Review Request
                    if (!empty($data['reviewRequestSmsBody'])) {
                        $customFields['smsBody'] = $data['reviewRequestSmsBody'];
                    }

                    // send sms
                    $smsSent = (bool) SmsComponent::sendReviewRequest($rr, $customFields);

                    // for IB, send a follow up SMS to comply with TCPA
                    if ($data['source'] == 'IB_DASHBOARD') {
                        SmsComponent::sendReviewRequestFollowUp($rr);
                    }
                }

                if ($mailSent || $smsSent) {
                    // update the review request status
                    $rr->status = 'SUCCESS';
                    $rr->save();
                    $results['success'] = true;
                    $results['error'] = '';
                    $results['data'] = $rr->attributes;
                }
            } else {
                // fail
                $results['success'] = false;
                $results['error'] = 'COULD_NOT_SAVE_REVIEWREQUEST';
                $results['data'] = $rr->getErrors();
            }
        } else {
            $results['success'] = false;
            $results['error'] = 'COULD_NOT_SAVE_REVIEWREQUEST';
            $results['data'] = 'No review links available. Google and Yelp review links must be updated.';
        }

        return $results;
    }

    /**
     * Check if any link was clicked
     * @return bool
     */
    public function isClicked()
    {
        if (
            $this->google_clicked == 1 || $this->yelp_clicked == 1
            || $this->ddc_clicked == 1 || $this->custom_clicked == 1
        ) {
            return true;
        }
        return false;
    }

    public function getSite()
    {
        if ($this->google_link) {
            return 'GOOGLE';
        }

        if ($this->yelp_link) {
            return 'YELP';
        }

        if ($this->ddc_link) {
            return 'DDC';
        }

        if ($this->custom_link) {
            return 'CUSTOM';
        }

        return null;
    }
}
