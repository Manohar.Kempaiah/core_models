<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeDirectory');

class ProviderPracticeDirectory extends BaseProviderPracticeDirectory
{

    public $uploadPath;

    /**
     * Init
     */
    public function init()
    {
        parent::init();
        $basePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'providers';
        $this->uploadPath = $basePath . Yii::app()->params['upload_providers'];
    }

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * update Providers
     * @param array $postedProviders
     * Array
     *  (
     *       [3817546] => Array
     *           (
     *               [checked] => 3817546
     *               [provider_bio] => f sdf sd
     *           )
     *       [3817559] => Array
     *           (
     *               [checked] => 3817559
     *               [provider_bio] => fsdf sdf sdfsd ertyvrtyh
     *           )
     *
     * @param integer $directorySettingId
     *
     * @return array
     */
    public static function updateProviders($postedProviders, $directorySettingId)
    {


        foreach ($postedProviders as $providerId => $pp) {

            $ppd = ProviderPracticeDirectory::model()->find(
                'directory_setting_id=:directorySettingId AND provider_id=:provider_id',
                [
                    ':directorySettingId' => $directorySettingId,
                    ':provider_id' => $providerId
                ]
            );

            if (!empty($pp['checked'])) {
                // This provider is selected, checking if exists or not
                $haveToSave = false;
                if (empty($ppd)) {
                    // Not exists, is a new record
                    $ppd = new ProviderPracticeDirectory;
                    $ppd->directory_setting_id = $directorySettingId;
                    $ppd->provider_id = $providerId;
                    $ppd->provider_bio = $pp['provider_bio'];
                    $haveToSave = true;
                }
                if ($ppd->provider_bio != $pp['provider_bio']) {
                    $ppd->provider_bio = $pp['provider_bio'];
                    $haveToSave = true;
                }

                if (!empty($pp['remove_pic'])) {
                    $ppd->provider_photo_path = null;
                    $haveToSave = true;
                }

                if (Common::isTrue($haveToSave)) {
                    $ppd->save();
                }
            } else {
                // We have to remove it
                if (!empty($ppd)) {
                    $ppd->delete();
                }
            }
            // the providers already linked to the widget

        }

        // Do we have to upload a photo?
        if (!empty($_FILES['provider_photo_path'])) {
            File::uploadMultiple('ProviderPracticeDirectory', 'provider_photo_path', $directorySettingId);
        }

        return array(
            'success' => true,
            'error' => ''
        );
    }

    /**
     * Get the all providers associated with the account_id
     *
     * @param array $params
     * Eg:
     *  $params = [
     *       'directory_setting_id' => $model->id,
     *       'account_id' => $model->id,
     *       'page' => 1,
     *       'page_size' => 30,
     *  ];
     *
     * @return array The list of associated providers
     */
    public static function getAllProviders($params)
    {

        $directorySettingId = (integer) isset($params['directory_setting_id']) ? $params['directory_setting_id'] : 0;
        $accountId = (integer) isset($params['account_id']) ? $params['account_id'] : 0;
        $page = (integer) isset($params['page']) ? $params['page'] : 1;
        $pageSize = (integer) isset($params['page_size']) ? $params['page_size'] : 20;
        $cacheTime = (integer) isset($params['use_cache']) ? Yii::app()->params['cache_short'] : 0;

        $sql = sprintf(
            "(
                SELECT ap.provider_id, p.first_last, ppd.id AS ppd_id, provider_bio, provider_photo_path,
                    IF (ppd.id > 0, 1, 0) AS checked, 0 as external
                FROM account_provider AS ap
                INNER JOIN `provider` AS p
                    ON ap.provider_id = p.id
                LEFT JOIN provider_practice_directory AS ppd
                    ON ppd.provider_id = ap.provider_id
                    AND ppd.directory_setting_id = %d
                WHERE ap.account_id = %d
            )
            UNION
            (
                SELECT ppd.provider_id, p.first_last, ppd.id AS ppd_id, provider_bio, provider_photo_path,
                1 AS checked, 1 as external
                FROM provider_practice_directory AS ppd
                INNER JOIN `provider` AS p
                    ON ppd.provider_id = p.id
                WHERE ppd.directory_setting_id = %d
                    AND ppd.provider_id NOT IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
            )
            ORDER BY first_last ASC",
            $directorySettingId,
            $accountId,
            $directorySettingId,
            $accountId
        );

        $arrProviders = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
        $totalRecords = count($arrProviders);

        // Remove unsolicited records
        $kInitial = ($page-1) * $pageSize;
        $kEnd = $kInitial + $pageSize;
        foreach ($arrProviders as $k => $v) {
            if ($k<$kInitial || $k >= $kEnd) {
                unset($arrProviders[$k]);
            }
        }

        $totalPage = ceil($totalRecords / $pageSize);
        $pagination = array();
        $pagination['total_records'] = $totalRecords;
        $pagination['total_pages'] = $totalPage;
        $pagination['page_size'] = $pageSize;
        $pagination['current'] = $page;
        $pagination['previous'] = ($page-1) <= 1 ? 1 : ($page-1);
        $pagination['next'] = ($page+1) >= $totalPage ? $totalPage : ($page+1);

        return [
            'pagination' => $pagination,
            'arrProviders' => $arrProviders,
        ];
    }


    /**
     * Get all the provider_practice_widget records for a given widget
     *
     * @param int $directoryId
     * @return array
     */
    public static function getProviderPractices($directoryId)
    {
        $sql = sprintf(
            "SELECT ppw.id, ppw.widget_id, ppw.provider_id, ppw.practice_id, ppw.prefix, ppw.suffix,
            ppw.middle_name
            FROM provider_practice_widget ppw
            INNER JOIN provider_details pd
                ON ppw.provider_id = pd.provider_id
            WHERE ppw.widget_id = %d",
            $directoryId
        );
        return ProviderPracticeDirectory::model()->findAllBySql($sql);
    }

    /**
     * After Save -> save log
     */
    public function afterSave()
    {
        $action = $this->isNewRecord ? 'C' : 'U';
        $description = $this->isNewRecord ? 'Create' : 'Update';
        AuditLog::create($action, $description . ' Directory #' . $this->id);
    }
}
