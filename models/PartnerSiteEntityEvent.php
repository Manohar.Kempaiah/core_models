<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteEntityEvent');

class PartnerSiteEntityEvent extends BasePartnerSiteEntityEvent
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
