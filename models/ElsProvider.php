<?php

class ElsProvider
{

    /**
     * get provider data by id via elasticSearch engine
     * @param int $dataId
     * @return arr
     */
    public static function getProviderById($dataId = 0)
    {
        if ($dataId == 0) {
            return false;
        }

        $query = array(
            "query" => array(
                "bool" => array(
                    "must" => array(
                        array(
                            "term" => array(
                                "provider_id" => $dataId
                            )
                        )
                    )
                )
            )
        );

        $data = DoctorIndex::run(Yii::app()->params->elasticPrefixIndex . 'doctor_provider', 'provider', $query);

        if (empty($data['hits']['hits'][0]['_source']) || empty($data['hits']['hits'][0]['_source']['provider_id'])) {
            return false;
        } else {
            $data = $data['hits']['hits'][0]['_source'];
        }

        return ElsProvider::formatArrayToView($data);
    }

    /**
     * Return formatted array to the view
     * @param array $data
     * @return array
     */
    public static function formatArrayToView($data = '')
    {
        if (empty($data)) {
            return false;
        }
        $providerId = $data['provider_id'];
        $data['provider'] = $data['provider_name'];

        // set provider location data, from practice.primary_location
        $data['city'] = $data['city_friendly_url'] = $data['zipcode'] = '';
        $data['state'] = $data['state_friendly_url'] = $data['state_abbreviation'] = '';

        if (!empty($data['practice'])) {
            // first of all remove the unpublished practices from the results
            $tmpPractices = array();
            foreach ($data['practice'] as $pra) {
                if (empty($pra['unpublished'])) {
                    $tmpPractices[] = $pra;
                }
            }

            // sort practices by primary_location and rank_order
            usort($tmpPractices, function ($a, $b) {
                if (isset($a['rank_order']) && isset($b['rank_order'])) {
                    return $a['rank_order'] - $b['rank_order'];
                } else {
                    return $b['primary_location'] - $a['primary_location'];
                }
            });

            // if the first practice is not the primary set it as primary by default
            if (!empty($tmpPractices[0]) && $tmpPractices[0]['primary_location'] == 0) {
                $tmpPractices[0]['primary_location'] = 1;
            }

            // update practices
            $data['practice'] = $tmpPractices;

            foreach ($data['practice'] as $pra) {
                if ($pra['primary_location'] == 1) {
                    $data['city'] = $pra['city_name'];
                    $data['city_friendly_url'] = !empty($pra['city_friendly_url']) ? $pra['city_friendly_url'] : '';
                    $data['state'] = $pra['state_abbreviation'];
                    $data['state_abbreviation'] = $pra['state_abbreviation'];
                    $data['state_friendly_url'] = $pra['state_friendly_url'];
                    $data['zipcode'] = $pra['zipcode'];
                    break;
                }
            }
        }

        $masterArray = array(
            'arrAwards' => 'award',
            'arrCertification' => 'certification',
            'arrEducation' => 'education',
            'arrExperience' => 'experience',
            'arrForms' => 'form',
            'arrHospitals' => 'hospital',
            'arrMedications' => 'medication',
            'arrMemberships' => 'membership',
            'arrPaymentTypes' => 'payment_type',
            'arrPhotos' => 'photo',
            'arrPopulations' => 'population',
            'arrPractices' => 'practice',
            'arrPublications' => 'publication',
            'arrResidencies' => 'residency',
            'arrReviews' => 'rating',
            'arrTreatments' => 'treatment',
            'arrSocialLinks' => 'practice_listing',
            'arrVideos' => 'video'
        );
        foreach ($masterArray as $key => $value) {
            $return[$key] = !empty($data[$value]) ? $data[$value] : null;
            unset($data[$value]);
        }

        // show only reviews from ReviewHub, Reviews widget and SiteEnhance
        if (!empty($return['arrReviews'])) {
            $arrReviews = [];
            foreach ($return['arrReviews'] as $review) {
                if (
                    $review['partner_site_id'] == 1 ||
                    $review['partner_site_id'] == 63 ||
                    $review['partner_site_id'] == 152) {
                    $arrReviews[] = $review;
                }
            }
            $return['arrReviews'] = $arrReviews;
        }

        // calculate stats
        $return['arrReviewStats'] = empty($return['arrReviews']) ?
            ProviderRating::model()->getStats(array()) : ProviderRating::model()->getStats($return['arrReviews']);

        if (!is_array($return['arrReviews'])) {
            $return['arrReviews'] = array();
        }
        if (is_array($return['arrReviews'])) {
            for ($i = 0; $i < count($return['arrReviews']); $i++) {
                $return['arrReviews'][$i]['id'] = $return['arrReviews'][$i]['rel_id'];
            }
            usort($return['arrReviews'], function ($a, $b) {
                return strtotime($a['date_added']) < strtotime($b['date_added']);
            });
        }

        if (is_array($return['arrForms'])) {
            $i = 0;
            while ($i < count($return['arrForms'])) {
                if (empty($return['arrForms'][$i]['file_link']) && empty($return['arrForms'][$i]['file_path'])) {
                    array_splice($return['arrForms'], $i, 1);
                    continue;
                }
                $return['arrForms'][$i]['href'] = empty($return['arrForms'][$i]['file_link']) ?
                        '/images/uploads/forms/' . $return['arrForms'][$i]['file_path'] :
                        $return['arrForms'][$i]['file_link'];
                $i++;
            }
        }

        if (is_array($return['arrPopulations'])) {
            for ($i = 0; $i < count($return['arrPopulations']); $i++) {
                $return['arrPopulations'][$i]['name'] = $return['arrPopulations'][$i]['population_name'];
            }
        }

        if (!empty($return['arrPopulations'])) {
            usort($return['arrPopulations'], function ($a, $b) {
                return $a['name'] > $b['name'];
            });
        }

        if (is_array($return['arrSocialLinks'])) {
            foreach ($return['arrSocialLinks'] as $k => $v) {
                if ($v['listing_type_name'] == 'Google+') {
                    // Exclude Google+, because not exists anymore
                    unset($return['arrSocialLinks'][$k]);
                }
            }

            for ($i = 0; $i < count($return['arrSocialLinks']); $i++) {

                $return['arrSocialLinks'][$i]['name'] = (!empty($return['arrSocialLinks'][$i]['listing_type_name']) ?
                    $return['arrSocialLinks'][$i]['listing_type_name'] : '');
                $return['arrSocialLinks'][$i]['link'] = (!empty($return['arrSocialLinks'][$i]['url']) ?
                    $return['arrSocialLinks'][$i]['url'] : '');
            }

            // filter social links
            $return['arrSocialLinks'] = self::transformSocialLinks(
                $return['arrSocialLinks'],
                empty($return['arrPractices']) ? null : $return['arrPractices'][0]['practice_id']
            );
        }

        $return['isTrackedPhone'] = false;
        $return['phoneNumber'] = '';

        if (is_array($return['arrPractices'])) {
            // set the default phone number if there is only one practice, ignoring if is the primary practice
            if (count($return['arrPractices']) == 1) {
                $return['phoneNumber'] = $return['arrPractices'][0]['phone_number'];
            }

            for ($i = 0; $i < count($return['arrPractices']); $i++) {
                $return['arrPractices'][$i]['is_tracked_phone_number'] = false;
                $return['arrPractices'][$i]['featured_phone_number'] = '';

                if (!empty($data['featured_phone_number'])) {
                    foreach ($data['featured_phone_number'] as $featured) {
                        if ($return['arrPractices'][$i]['practice_id'] == $featured['practice_id'] &&
                                $featured['twilio_partner_id'] == Yii::app()->params->search_twilio_partner_id) {
                            $return['arrPractices'][$i]['is_tracked_phone_number'] = true;
                            $return['arrPractices'][$i]['featured_phone_number'] = $featured['phone_number'];

                            // set the provider phone from primary practice
                            if ($return['arrPractices'][$i]['primary_location']) {
                                $return['isTrackedPhone'] = true;
                                $return['phoneNumber'] = $featured['phone_number'];
                            }

                            break;
                        }
                    }
                }

                // if provider doesn't have a twilio number set the primary practice phone number
                if (!$return['arrPractices'][$i]['is_tracked_phone_number'] &&
                        $return['arrPractices'][$i]['primary_location']) {
                    $return['isTrackedPhone'] = false;
                    $return['phoneNumber'] = $return['arrPractices'][$i]['phone_number'];
                }

                $return['arrPractices'][$i]['id'] = $return['arrPractices'][$i]['practice_id'];
                $return['arrPractices'][$i]['city'] = $return['arrPractices'][$i]['city_name'];
                $return['arrPractices'][$i]['abbreviation'] = $return['arrPractices'][$i]['state_abbreviation'];
                $return['arrPractices'][$i]['address_2'] = isset($return['arrPractices'][$i]['address_2']) ?
                    $return['arrPractices'][$i]['address_2'] : '';
                $return['arrPractices'][$i]['pr_fax'] = isset($return['arrPractices'][$i]['fax_number']) ?
                    $return['arrPractices'][$i]['fax_number'] : '';
            }
        }

        if (is_array($return['arrEducation'])) {
            for ($i = 0; $i < count($return['arrEducation']); $i++) {
                $return['arrEducation'][$i]['education_type'] = $return['arrEducation'][$i]['education_type_name'];
            }
        }

        if (!empty($return['arrEducation'])) {
            usort($return['arrEducation'], function ($a, $b) {
                return $a['graduation_year'] - $b['graduation_year'];
            });
        }

        if (is_array($return['arrTreatments'])) {
            for ($i = 0; $i < count($return['arrTreatments']); $i++) {
                $return['arrTreatments'][$i]['name'] = $return['arrTreatments'][$i]['treatment_name'];
            }
        }

        if (!empty($return['arrTreatments'])) {
            usort($return['arrTreatments'], function ($a, $b) {
                return $a['name'] > $b['name'];
            });
        }

        if (is_array($return['arrPaymentTypes'])) {
            for ($i = 0; $i < count($return['arrPaymentTypes']); $i++) {
                $return['arrPaymentTypes'][$i]['name'] = $return['arrPaymentTypes'][$i]['payment_type_name'];
            }
        }

        if (is_array($return['arrVideos'])) {
            for ($i = 0; $i < count($return['arrVideos']); $i++) {
                $return['arrVideos'][$i] = (object) $return['arrVideos'][$i];
            }
        }

        if (is_array($return['arrPublications'])) {
            $arrPublicationTypes = array(
                '',
                'Book',
                'Dissertation',
                'Magazine Article',
                'Medical/Scholarly Journal Article',
                'Online Article',
                'Text Book',
                'Other Publication'
            );
            for ($i = 0; $i < count($return['arrPublications']); $i++) {
                $return['arrPublications'][$i]['publication_type'] =
                    $arrPublicationTypes[$return['arrPublications'][$i]['publication_type_id']];
            }
        }

        // providerDetails
        $return['providerDetails'] = !empty($data['details'][0]) ? $data['details'][0] : null;
        if (!empty($return['providerDetails'])) {
            $return['providerDetails']['philosophy'] = $return['providerDetails']['philosophy_name'];
            $return['providerDetails']['has_financing_plans'] =
                $return['providerDetails']['has_financing_plans'] === null ? 'Unknown' :
                ($return['providerDetails']['has_financing_plans'] ? 'Yes' : 'No');
            $return['providerDetails']['has_sliding_billing'] =
                $return['providerDetails']['has_sliding_billing'] === null ? 'Unknown' :
                ($return['providerDetails']['has_sliding_billing'] ? 'Yes' : 'No');
        }
        $return['arrExtraDetails'] = $return['providerDetails'];
        $return['arrExtraDetails']['schedule_mail_enabled'] =
            !empty($data['schedule_online']) || !empty($return['arrExtraDetails']['schedule_mail_enabled']);
        $return['noInsurancesReason'] = !empty($return['providerDetails']['no_insurance_reason']) ?
                $return['providerDetails']['no_insurance_reason'] : null;
        unset($data['details']);

        // try to find no-insurance reason
        $noInsurancesReason = false;
        $arrInsurances = array();
        if (!empty($return['providerDetails']['no_insurance_reason'])) {
            $noInsurancesReason = trim($return['providerDetails']['no_insurance_reason']);
        }
        if (empty($noInsurancesReason)) {
            // Get insurances
            $arrInsurances = !empty($data['practice_insurance']) ? $data['practice_insurance'] : null;
            if (empty($arrInsurances)) {
                //we only have data in our old insurance tables (provider_insurance)
                $arrInsurances = !empty($data['insurance']) ? $data['insurance'] : null;
            }
        }

        $return['noInsurancesReason'] = $noInsurancesReason;
        unset($data['practice_insurance']);
        unset($data['insurance']);

        // Format arrInsurances
        $tempInsurances = is_array($arrInsurances) ? $arrInsurances : array();
        $arrInsurances = array();
        $arrPlans = null;
        foreach ($tempInsurances as $value) {
            $companyName = explode('|', $value['insurance_company_alias']);
            $companyName = $companyName[0];
            $companyId = $value['company_id'];
            $insurance = array();
            $insurance['details'] = '';
            $arrInsurances[$companyName]['company'] = $companyName;
            $arrInsurances[$companyName]['priority'] = empty($value['insurance_company_priority']) ? 0 :
                    $value['insurance_company_priority'];

            if (!isset($arrInsurances[$companyName]['insurances'])) {
                $arrInsurances[$companyName]['insurances'] = [];
            }

            // set id or plan as index name for ignore duplicated insurances
            if ($value['insurance_name'] != 'ALL') {
                $insurance['plan'] = $value['insurance_name'];
                $arrInsurances[$companyName]['insurances'][$insurance['plan']] = $insurance;
            } else {
                $arrPlans = Insurance::model()->getInsurances($companyId);
                foreach ($arrPlans as $plan) {
                    $insurance['plan'] = $plan['name'];
                    $insurance['plan_id'] = $plan['id'];
                    $arrInsurances[$companyName]['insurances'][$insurance['plan_id']] = $insurance;
                }
            }
        }

        if (!empty($arrInsurances)) {
            usort($arrInsurances, function ($a, $b) {
                $priority = $b['priority'] - $a['priority'];

                if ($priority == 0) {
                    return $b['company'] < $a['company'];
                }

                return $priority;
            });
        }

        $InsuranceCompanyName = '';
        $return['InsuranceCompanyName'] = $InsuranceCompanyName;
        if (!empty($arrInsurances)) {
            $return['arrInsurances'] = array('0|all' => $arrInsurances);
        } else {
            $return['arrInsurances'] = false;
        }

        // Format medications
        if (is_countable($return['arrMedications'])) {
            $retValue = count($return['arrMedications']);
        } else {
            $retValue = 0;
        }

        $return['arrMedications'] = array($return['arrMedications'], $retValue);

        // language
        $arrDefaultLanguages = array(0 => array('language_id' => 67, 'language_name' => 'English'));
        $arrTempLanguages = !empty($data['language']) ? $data['language'] : array();
        $return['arrLanguages'] = array_merge($arrDefaultLanguages, $arrTempLanguages);
        for ($i = 0; $i < count($return['arrLanguages']); $i++) {
            $return['arrLanguages'][$i]['name'] = $return['arrLanguages'][$i]['language_name'];
        }
        unset($data['language']);
        unset($arrTempLanguages);
        unset($arrDefaultLanguages);

        // PracticeSchedule and arrHours
        $arrPracticeSchedule = !empty($data['practice_schedule']) ? $data['practice_schedule'] : array();
        unset($data['practice_schedule']);
        $arrDays = array(0 => 'Sun', 1 => 'Mon', 2 => 'Tue', 3 => 'Wed', 4 => 'Thu', 5 => 'Fri', 6 => 'Sat');
        $arrHours = array();

        if (empty($arrPracticeSchedule)) {
            // Try to get the Practice Office Hours
            if (!empty($return['arrPractices'][0]['practice_id'])) {
                $practiceId = $return['arrPractices'][0]['practice_id'];
                $practiceOfficeHours = PracticeOfficeHour::getOfficeHours($practiceId, true);
                if (!empty($practiceOfficeHours)) {
                    foreach ($practiceOfficeHours as $dayNumber => $poh) {
                        if ($poh['selected'] == 1) {
                            list($from, $to) = explode('-', $poh['timeRange']);
                            $from = date("H:i:s", strtotime(trim($from)));
                            $to = date("H:i:s", strtotime(trim($to)));

                            $arrPracticeSchedule[] = array(
                                "provider_practice_id" => 0,
                                "hours" => $from . ' - ' . $to,
                                "practice_id" => $practiceId,
                                "provider_id" => $providerId,
                                "day_number" => $dayNumber,
                                "id" => $practiceId,
                                "rel_id" => $practiceId,
                                "is_generated" => 0,
                            );
                        }
                    }
                }
            }
        }

        $dayNumber = array();
        foreach ($arrPracticeSchedule as $key => $ps) {
            if (!isset($dayNumber[$ps['practice_id']][$ps['day_number']])) {
                // get Hours for each Practice
                $tempData['hours'] = $ps['hours'];
                $tempData['day'] = $arrDays[$ps['day_number']];
                $tempData['day_number'] = $ps['day_number'];
                $arrHours[$ps['practice_id']][] = $tempData;
                usort($arrHours[$ps['practice_id']], function ($a, $b) {
                    return $a['day_number'] - $b['day_number'];
                });
            }
            $dayNumber[$ps['practice_id']][$ps['day_number']] = true;
        }

        $return['arrPracticeSchedule'] = $arrPracticeSchedule;
        $return['arrHours'] = $arrHours;

        // Specialties
        $return['arrSpecialties'] = !empty($data['specialty']) ? $data['specialty'] : [];
        for ($i = 0; $i < count($return['arrSpecialties']); $i++) {
            $return['arrSpecialties'][$i]['specialty'] = $return['arrSpecialties'][$i]['specialty_name'];
            $return['arrSpecialties'][$i]['sub_specialty'] = $return['arrSpecialties'][$i]['sub_specialty_name'];
        }
        if ($return['arrSpecialties']) {
            usort($return['arrSpecialties'], function ($a, $b) {
                return $a['primary_specialty'] - $b['primary_specialty'];
            });
        }

        // set specialties
        $data['specialties'] = !empty($return['arrSpecialties'][0]['specialty_name']) ?
            $return['arrSpecialties'][0]['specialty_name'] : '';
        $data['specialties'] .= !empty($return['arrSpecialties'][0]['sub_specialty_name']) ?
            ' - ' . $return['arrSpecialties'][0]['sub_specialty_name'] : '';
        $data['specialties'] .= !empty($return['arrSpecialties'][1]['specialty_name']) ?
            ', ' . $return['arrSpecialties'][1]['specialty_name'] : '';
        $data['specialties'] .= !empty($return['arrSpecialties'][1]['sub_specialty_name']) ?
            ' - ' . $return['arrSpecialties'][1]['sub_specialty_name'] : '';
        unset($data['specialty']);

        // Residencies
        if (is_countable($return['arrResidencies'])) {
            for ($i = 0; $i < count($return['arrResidencies']); $i++) {
                $return['arrResidencies'][$i]['residency_type'] = $return['arrResidencies'][$i]['residency_type_name'];
            }
        }

        // Certification
        if (is_countable($return['arrCertification'])) {
            for ($i = 0; $i < count($return['arrCertification']); $i++) {
                $return['arrCertification'][$i]['sub_specialty'] =
                    $return['arrCertification'][$i]['certification_sub_specialty_name'];
                $return['arrCertification'][$i]['specialty'] =
                    $return['arrCertification'][$i]['certification_specialty_name'];
                $return['arrCertification'][$i]['certifying_body'] =
                    $return['arrCertification'][$i]['certifying_body_name'];
                $return['arrCertification'][$i]['year_achieved'] =
                    isset($return['arrCertification'][$i]['year_achieved']) ?
                    $return['arrCertification'][$i]['year_achieved'] : '';
            }
        }

        $photo = $photo_small = null;
        if ($data['photo_path']) {
            $photo = AssetsComponent::generateImgSrc($data['photo_path'], 'providers', 'L');
            $photo_small = AssetsComponent::generateImgSrc($data['photo_path'], 'providers', 'S');
        }
        $return['photo'] = $photo;
        $return['photo_small'] = $photo_small;

        $return['claimed'] = !empty($data['claimed']) ? $data['claimed'] : false;

        if (isset(Yii::app()->request->cookies['premRef'])) {
            // don't show ads for premium referrers
            $return['showAds'] = false;
        } else {
            // show ads only on non-client pages
            $return['showAds'] = (Provider::isClient($providerId) ? false : true);
        }

        // Colleagues
        $comma = $listPracticesId = '';
        $return['arrColleagues'] = array();
        if (!empty($return['arrPractices'])) {
            foreach ($return['arrPractices'] as $pra) {
                $listPracticesId .= $comma . "'" . $pra['practice_id'] . "'";
                $comma = ",";
            }
            $return['arrColleagues'] = ProviderPractice::model()->getPracticeColleagues($listPracticesId, $providerId);
        }

        // Hide address for pharmacists
        $hideAddress = false;
        if (isset($return['arrSpecialties'][0])
            && in_array($return['arrSpecialties'][0]['specialty_id'], array(119, 122))) {
                $hideAddress = true;
        }

        $return['hideAddress'] = $hideAddress;

        $data['id'] = $data['provider_id'];
        $data['credential_title'] = $data['title'];

        return array_merge($return, $data);
    }

    /**
     * get providers data by id via elasticSearch engine
     * @param arr $dataList
     * @return arr
     */
    public static function getProvidersProfileShort($dataList = '', $elsIndex = null)
    {
        if (empty($dataList)) {
            return false;
        }

        // if $elsIndex is null
        if (is_null($elsIndex)) {
            $elsIndex = Yii::app()->params->elasticPrefixIndex . 'doctor_provider';
        }

        if (!isset($dataList['hits'])) {
            $arrShould = array();
            foreach ($dataList as $data) {
                $arrShould[]['term']['provider_id'] = $data['id'];
            }
            $query = array(
                "query" => array(
                    "bool" => array(
                        "minimum_should_match" => 1,
                        "should" => $arrShould,
                    )
                )
            );

            $data = DoctorIndex::run($elsIndex, 'provider', $query);

        } else {
            $data = $dataList;
        }

        if (empty($data)) {
            return false;
        }
        $result = array();

        // This array have all matching fields between both arrays
        $masterArray = array('featured', 'friendly_url', 'provider_name', 'phone_number', 'featured_phone_number',
            'accepts_insurance', 'schedule_online', 'photo_path', 'board_certificated', 'claimed', 'brief_bio',
            'suspended', 'gender', 'docscore', 'avg_rating', 'first_name', 'last_name', 'title', 'reputation_stats_avg',
            'reputation_stats_sum', 'external_id');

        foreach ($data['hits']['hits'] as $p) {
            $data = $p['_source'];
            $providerId = $data['provider_id'];

            $result[$providerId]['provider_id'] = $providerId;
            $result[$providerId]['id'] = $providerId;

            foreach ($masterArray as $value) {
                $result[$providerId][$value] = isset($data[$value]) ? $data[$value] : null;
            }

            // Do we have a reputation_stats ?
            $result[$providerId]['reputation_stats_avg'] = null;
            $result[$providerId]['reputation_stats_sum'] = null;
            if (!empty($data['reputation_stats'])) {

                // yes we have
                $score = $sum = $count = 0;
                foreach ($data['reputation_stats'] as $rs) {
                    $count++;
                    $sum = $sum + $rs['total_rating'];
                    $score = $score + $rs['avg_overall_score'];
                }
                $result[$providerId]['reputation_stats_avg'] = number_format($score/$count, 2);
                $result[$providerId]['reputation_stats_sum'] = $sum;
            }


            $result[$providerId]['specialties'] = '';
            $result[$providerId]['specialties_ids'] = '';
            $result[$providerId]['sub_specialty_ids'] = '';
            $result[$providerId]['sub_specialties'] = '';
            if (!empty($data['specialty'])) {
                $sep = '';
                foreach ($data['specialty'] as $specialty) {
                    $result[$providerId]['specialties'] .= strpos(
                        $result[$providerId]['specialties'],
                        $specialty['specialty_name']
                    ) === false ? $sep . $specialty['specialty_name'] .
                    (empty($specialty['sub_specialty_name']) ? '' : ' - ' . $specialty['sub_specialty_name']) : '';
                    $result[$providerId]['specialties_ids'] .= $sep . $specialty['specialty_id'];
                    $result[$providerId]['sub_specialty_ids'] .= $sep . $specialty['sub_specialty_id'];
                    $result[$providerId]['sub_specialties'] .= $sep . $specialty['sub_specialty_name'];
                    $sep = ', ';
                }
            }

            $result[$providerId]['provider'] = $data['provider_name'];
            $result[$providerId]['board_certificated'] = isset($data['certification'])
                && is_array($data['certification']) && count($data['certification']) > 0;
            $result[$providerId]['accepts_insurance'] = isset($data['insurance']) && is_array($data['insurance'])
                && count($data['insurance']) > 0;

            $result[$providerId]['practice_name'] = '';
            $result[$providerId]['primary_office_location'] = '';
            $result[$providerId]['primary_location'] = '';
            $result[$providerId]['city'] = '';
            $result[$providerId]['state'] = '';
            $result[$providerId]['zipcode'] = '';
            $result[$providerId]['address_2'] = '';
            $result[$providerId]['practice_type_id'] = '1';
            $result[$providerId]['coverage_state_id'] = 0;
            $result[$providerId]['featured_phone_number'] = 0;
            $result[$providerId]['phone_number'] = '';

            if (!empty($data['practice'])) {
                if (count($data['practice']) == 1) {
                    // if have only one practice, take it as primary
                    $data['practice'][0]['primary_location'] = 1;
                }
                $result[$providerId]['total_practices'] = count($data['practice']);

                foreach ($data['practice'] as $key => $practice) {
                    if ($practice['primary_location'] == 1) {
                        $practice['is_tracked_phone_number'] = false;
                        if (!empty($data['featured_phone_number'])) {
                            foreach ($data['featured_phone_number'] as $featured) {
                                if (
                                    $practice['practice_id'] == $featured['practice_id']
                                    &&
                                    $featured['twilio_partner_id'] == Yii::app()->params->search_twilio_partner_id
                                ) {
                                    $practice['is_tracked_phone_number'] = true;
                                    $practice['featured_phone_number'] = $featured['phone_number'];
                                    break;
                                }
                            }
                        }

                        $result[$providerId]['practice_name'] = $practice['name'];
                        $result[$providerId]['primary_office_location'] = $practice['address'];
                        $result[$providerId]['primary_location'] = $practice['address'];
                        $result[$providerId]['city'] = $practice['city_name'];
                        $result[$providerId]['state'] = $practice['state_name'];
                        $result[$providerId]['zipcode'] = $practice['zipcode'];
                        $result[$providerId]['latitude'] = $practice['latitude'];
                        $result[$providerId]['longitude'] = $practice['longitude'];
                        $result[$providerId]['address_2'] = '';
                        $result[$providerId]['practice_type_id'] = $practice['practice_type_id'];
                        $result[$providerId]['coverage_state_id'] = $practice['coverage_state_id'];
                        $result[$providerId]['phone_number'] = $practice['phone_number'];
                        $result[$providerId]['is_tracked_phone_number'] = $practice['is_tracked_phone_number'];
                        $result[$providerId]['featured_phone_number'] = $practice['is_tracked_phone_number'] ?
                                $practice['featured_phone_number'] : $practice['phone_number'];
                        break;
                    }
                }
            }

            // set the schedule mail enabled into this field
            $result[$providerId]['schedule_online'] = $data['details'][0]['schedule_mail_enabled'] ?? 0;
        }

        if (!isset($dataList['hits'])) {
            foreach ($dataList as $key => $value) {
                $providerId = $value['id'];
                if (!empty($result[$providerId])) {
                    $dataList[$key] = array_merge($dataList[$key], $result[$providerId]);
                } else {
                    unset($dataList[$key]);
                }
            }
        } else {
            $dataList = array();
            $i = 0;
            foreach ($result as $providerId => $data) {
                $dataList[$i] = $data;
                $i++;
            }
        }

        unset($result);
        return $dataList;
    }

    /**
     * Formats the ElasticSearch providers for the autocomplete
     * @param array $data
     * @return array
     */
    public static function formatArrayToProviderAutocomplete($data)
    {
        $formattedData = array();
        foreach ($data['hits']['hits'] as $provider) {
            $formattedData[] = array('id' => $provider['_source']['provider_id'],
                'name' => $provider['_source']['provider_name']);
        }
        return $formattedData;
    }

    /**
     * Transform social links array to filter to one of each type according to following sorting rules:
     * - Link without practice_id
     * - Link with practice_id equals to primary practice id
     * - Link with practice_id different to primary practice id
     *
     * @param array $data
     * @param int $primaryPracticeId
     * @return array
     */
    protected static function transformSocialLinks($data, $primaryPracticeId)
    {
        // array with links grouped by type
        $grouped = [];

        // result of the filtering
        $result = [];

        // group each link by the link name
        foreach ($data as $link) {
            $grouped[$link['name']][] = $link;
        }

        // sort each group by defined rules
        foreach ($grouped as $g) {

            // call to user defined sort function
            usort($g, function ($a, $b) use ($primaryPracticeId) {
                // sort for records with practice_id null
                if (empty($a['practice_id']) && empty($b['practice_id'])) {
                    return 0;
                }

                if (empty($a['practice_id']) && !empty($b['practice_id'])) {
                    return -1;
                }

                if (empty($b['practice_id']) && !empty($a['practice_id'])) {
                    return 1;
                }

                // sort for records with practice_id equals to the primary practice
                if ($a['practice_id'] == $b['practice_id']) {
                    return 0;
                }

                if ($a['practice_id'] == $primaryPracticeId) {
                    return -1;
                }

                if ($b['practice_id'] == $primaryPracticeId) {
                    return 1;
                }

                // rest of links
                return 0;
            });

            // put only the first filtered link into the result array
            $result[] = $g[0];
        }

        // done
        return $result;
    }

}
