<?php

Yii::import('application.modules.core_models.models._base.BaseSalesforceEntity');

class SalesforceEntity extends BaseSalesforceEntity
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getSalesforceLink()
    {
        return (
            '<a target="_blank" href="'.Yii::app()->salesforce->applicationUri . $this->salesforce_id . '">' .
                $this->salesforce_entity_id . '</a>'
        );
    }

    public function filtersalesforceCases()
    {	
        $criteria = new CDbCriteria();

		$criteria->compare('t.salesforce_id', $this->salesforce_id, true);
        $criteria->compare('t.status', $this->status, true);
        $criteria->compare('sem.id', $this->salesforce_entity_mapping_id, true);
		$criteria->compare('sem.entity_table', $this->entity_table, true);
		$criteria->compare('sem.entity_id', $this->entity_id, true);
		$criteria->compare('sem.salesforce_entity_id', $this->salesforce_entity_id, true);
		$criteria->compare('sfo.name', $this->salesforce_object, true);
		$criteria->compare('t.salesforce_object_type_id', $this->salesforce_object_type_id);
        
	    $criteria->select ='t.*, sem.id as salesforce_entity_mapping_id, sem.entity_id as entity_id, sem.entity_table as entity_table, sem.salesforce_entity_id as salesforce_entity_id, sfo.id as sfo_id, sfo.name as salesforce_object';
	    $criteria->join = '
        				INNER JOIN salesforce_entity_mapping sem
        					ON sem.salesforce_entity_id = t.id
        				INNER JOIN salesforce_object_type sfo
         					ON sfo.id = t.salesforce_object_type_id';

        return new CActiveDataProvider($this, array(
           'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     *
     * @return boolean
     */
    public function beforeDelete()
    {
    	// delete records from salesforce entity mapping before deleting this case
        $data = SalesforceEntityMapping::model()->find(
            'salesforce_entity_id=:salesforce_entity_id',
            array(':salesforce_entity_id' => $this->id)
        );

        if (!$data->delete()) {
            $this->addError("id", "Couldn't delete salesforce mapping.");
            return false;
        }
        
        return parent::beforeDelete();
    }

 //    public function search()
 //    {
	// 	$criteria = new CDbCriteria;
	// 	return new CActiveDataProvider($this, array(
	// 	'criteria' => $criteria,
	// 	));
	// }

}