<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeGmbDataPoint');

class ProviderPracticeGmbDataPoint extends BaseProviderPracticeGmbDataPoint
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
