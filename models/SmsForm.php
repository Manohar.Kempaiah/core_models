<?php

/**
 * SmsForm class.
 */
class SmsForm extends CFormModel
{

    public $name;
    public $phone;
    public $gmb_link;
    public $verifyCode;

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, email, subject and body are required
            array('name, phone, gmb_link', 'required'),
            // verifyCode needs to be entered correctly
            array('verifyCode', 'captcha', 'on' => 'captchaRequired'),
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'verifyCode' => 'Verification Code',
        );
    }

}
