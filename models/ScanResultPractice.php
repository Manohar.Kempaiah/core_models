<?php

Yii::import('application.modules.core_models.models._base.BaseScanResultPractice');
Yii::import('application.modules.core_models.models._interfaces.GenericScanResult');

class ScanResultPractice extends BaseScanResultPractice implements GenericScanResult
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getScrapedName()
    {
        return $this->scraped_name;
    }

    public function getScrapedAddress()
    {
        return $this->scraped_address;
    }

    public function getScrapedPhone()
    {
        return $this->scraped_phone_number;
    }

    /**
     * Determine if the type of the result is 'active' or not
     *
     * This is as convenience method to check if the current object
     * is an instance of 'ScanResultPractice' or not.
     * We are assuming that an instance of ScanResultPractice is
     * always an active type.
     *
     * @return boolean
     */
    public function isActive()
    {
        if (get_class($this) === 'ScanResultPractice') {
            return true;
        }

        return false;
    }
}
