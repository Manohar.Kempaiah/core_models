<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeVisitReason');

class ProviderPracticeVisitReason extends BaseProviderPracticeVisitReason
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Gets a provider-at-practice visit reasons and their duration
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function getVisitReasonsByProviderPractice($providerId = 0, $practiceId = 0)
    {

        $sql = sprintf(
            "SELECT SQL_NO_CACHE name, provider_practice_visit_reason.id, appointment_visit_duration AS
            duration
            FROM provider_practice_visit_reason
            INNER JOIN visit_reason
            ON provider_practice_visit_reason.visit_reason_id = visit_reason.id
            WHERE provider_id = %d
            AND practice_id = %d;",
            $providerId,
            $practiceId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Gets a practice-related visit reasons, in both collated and per-provider form
     * @param int $id
     * @return array
     */
    public static function getVisitReasonsByPractice($id)
    {

        $sql = sprintf(
            "SELECT SQL_NO_CACHE
                name,
                provider_practice_visit_reason.id AS localID,
                visit_reason.id AS globalID,
                IF (appointment_visit_duration IS NULL,
                    visit_reason.standard_visit_duration,
                    appointment_visit_duration
                ) AS duration,
                provider_id
            FROM provider_practice_visit_reason
            INNER JOIN visit_reason
            ON provider_practice_visit_reason.visit_reason_id = visit_reason.id
            WHERE practice_id = %d
            AND ((
                visit_reason.standard_visit_duration IS NOT NULL
                AND visit_reason.standard_visit_duration > 0
            ) OR (
                appointment_visit_duration IS NOT NULL
                AND appointment_visit_duration > 0
            ))
            ORDER BY visit_reason.id;",
            $id
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Validate UNIQUE KEY `un_provider_practice_visit_reason` (`provider_id`,`practice_id`,`visit_reason_id`)
     * @return boolean
     */
    public function beforeSave()
    {
        if ($this->isNewRecord) {
            // provider_id`,`practice_id`,`visit_reason_id`
            $exists = ProviderPracticeVisitReason::model()->exists(
                'practice_id=:practice_id AND provider_id=:provider_id AND visit_reason_id=:visit_reason_id',
                array(
                    ':practice_id' => $this->practice_id,
                    ':provider_id' => $this->provider_id,
                    ':visit_reason_id' => $this->visit_reason_id
                )
            );
            if ($exists) {
                $this->addError(
                    'practice_id_lookup',
                    'This Visit Reason is already associated to this provider-practice.'
                );
                return false;
            }
        }
        return parent::beforeSave();
    }

}
