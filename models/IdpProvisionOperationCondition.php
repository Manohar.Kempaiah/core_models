<?php

Yii::import('application.modules.core_models.models._base.BaseIdpProvisionOperationCondition');

class IdpProvisionOperationCondition extends BaseIdpProvisionOperationCondition
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
