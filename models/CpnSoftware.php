<?php

Yii::import('application.modules.core_models.models._base.BaseCpnSoftware');

class CpnSoftware extends BaseCpnSoftware
{
    const CPN_SYSTEM_TRAY = 26;
    const CPN_SV_LOADER = 27;
    const CPN_SV_MONITOR = 28;
    const CPN_SV_UPDATER = 29;
    const CPN_EHR_INTEGRATION = 30;
    const CPN_EHR_DENTRIX = 31;
    const CPN_UI_UPDATER = 32;
    const CPN_UI_LOADER = 33;
    const KILLPATCH_EHR_FILE = 'cpn_ehr_killpatch';
    const KILLPATCH_NON_EHR_FILE = 'cpn_killpatch';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get Software
     *
     * @param int $installationId
     * @return array
     */
    public function getSoftware($installationId)
    {
        // Query to cpn_software_version
        $sql = sprintf(
            "SELECT
                s.file_name AS fileName,
                s.guid AS identifier,
                s.installation_folder AS installationFolder,
                s.pre_installation_exe AS preProcessExe,
                s.post_installation_exe AS postProcessExe,
                s.delete_pre_after_install AS deletePreProcessExe,
                s.delete_post_after_install AS deletePostProcessExe,
                s.user_interface_flag AS userInterfaceFlag
            FROM cpn_software s
               INNER JOIN cpn_software_installation si on s.id = si.cpn_software_id
               INNER JOIN cpn_installation i on si.cpn_installation_id = i.id
            WHERE s.enabled_flag = 1
                  AND s.del_flag = 0
                  AND si.del_flag = 0
                  AND si.enabled_flag = 1
                  AND i.id = '%s'
                  AND si.installed_flag = 0;",
            $installationId
        );

        // Return all the results
        return Yii::app()->dbRO->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }


}
