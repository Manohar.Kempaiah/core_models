<?php

Yii::import('application.modules.core_models.models._base.BaseOrganizationEntityMatch');

class OrganizationEntityMatch extends BaseOrganizationEntityMatch
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
