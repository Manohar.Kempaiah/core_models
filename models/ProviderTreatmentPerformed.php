<?php

Yii::import('application.modules.core_models.models._base.BaseProviderTreatmentPerformed');

class ProviderTreatmentPerformed extends BaseProviderTreatmentPerformed
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
