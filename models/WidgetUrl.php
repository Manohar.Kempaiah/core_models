<?php

Yii::import('application.modules.core_models.models._base.BaseWidgetUrl');

class WidgetUrl extends BaseWidgetUrl
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Stores the URL of the site loading a widget and notifies
     * the resulting list to Salesforce.
     * @param mixed $key The loaded widget or the widget key_code
     * @param string $url URL of the host site
     * @return array
     */
    public static function reportWidgetUrl($key, $url)
    {

        if (empty($url) || strpos($url, Yii::app()->params->servers['dr_pro_hn']) !== false) {
            // if the widget is being loaded from doctor.com, return null
            return null;
        }

        // parse URL
        $arrUrl = parse_url($url);
        $host = $url;
        if (!empty($arrUrl['host'])) {
            $host = $arrUrl['host'];
        }

        $widget = null;
        if (!is_string($key)) {
            $widget = $key;
            $key = $widget->key_code;
        } else {
            $widget = Widget::model()->find("key_code = :key_code", array(":key_code" => $key));
        }

        // check in the cache if the host has already been processed
        $data = Yii::app()->cache->get("WU" . $key . "_url");
        if ($data) {
            if (strpos($data, $host) !== false) {
                return null;
            } else {
                $data .= ";" . $host;
                Yii::app()->cache->set("WU" . $key . "_url", $data, 7 * Yii::app()->params['cache_long']);
            }
        }

        // get organization owning the widget
        $organization = null;
        if ($widget && $widget->account && count($widget->account->organizationAccounts) > 0) {
            $oa = $widget->account->organizationAccounts[0];
            $organization = $oa->organization;
        }

        if (!isset($organization)) {
            // if no organization was found, return null
            return null;
        }

        // get current date
        $date = date("Y-m-d");

        // check if the url alredy exists
        $countUrl = WidgetUrl::model()->count(
            "widget_id = :widget_id AND url = :url",
            array(
                ":widget_id" => $widget->id,
                ":url" => $host
            )
        );

        $result = array();
        if ($countUrl == 0) {
            // create widget_url record
            $widgetUrl = new WidgetUrl();
            $widgetUrl->attributes = array(
                "widget_id" => $widget->id,
                "url" => $host,
                "date_added" => $date
            );

            // save the record
            if (!$widgetUrl->save()) {
                return null;
            }
            $result["saved"] = true;

            $sfResult = self::sendUrlsToSalesforce($widget);
            $result = array_merge($result, $sfResult);
        }

        return $result;
    }

    /**
     * Send the list of URLs that loaded a widget to Salesforce
     * @param Widget $widget Widget model.
     * @return array
     */
    public static function sendUrlsToSalesforce($widget)
    {
        $organization = null;
        $result = array();

        if (count($widget->account->organizationAccounts) > 0) {
            $oa = $widget->account->organizationAccounts[0];
            $organization = $oa->organization;
        }

        if (!isset($organization)) {
            // if no organization was found, return
            return $result;
        }

        // get updated URL list
        $urlList = self::getURLList($widget->account_id);

        // send URL list to salesforce
        $r = Yii::app()->salesforce->updateAccountField(
            $organization->salesforce_id,
            'SiteEnhance_URLs__c',
            $urlList
        );
        if (isset($r['success'])) {
            $result["SiteEnhance_URLs__c"] = true;
        }

        // get date widget was added
        $d = self::getDateAdded(
            $widget->account_id,
            strpos($widget->widget_type, 'REVIEW') !== false ?
                "'DISPLAY_REVIEWS', 'CREATE_REVIEW'" :
                "'" . $widget->widget_type . "'"
        );

        // If this is the first URL
        if (!empty($d)) {
            // send timestamp of when it went live
            $r = Yii::app()->salesforce->updateAccountField(
                $organization->salesforce_id,
                'SiteEnhance_Live__c',
                $d
            );
            if (isset($r['success'])) {
                $result["SiteEnhance_Live__c"] = true;
            }

            $arrFields = array(
                'DISPLAY_REVIEWS' => 'SiteEnhance_Review_Widget_Live__c',
                'CREATE_REVIEW' => 'SiteEnhance_Review_Widget_Live__c',
                'GET_APPOINTMENT' => 'SiteEnhance_Appt_Request_Live__c',
                'MOBILE_ENHANCE' => 'Mobile_SiteEnhance_Live__c'
            );

            if (!empty($widget->widget_type)) {
                if (!empty($arrFields[$widget->widget_type])) {
                    $r = Yii::app()->salesforce->updateAccountField(
                        $organization->salesforce_id,
                        $arrFields[$widget->widget_type],
                        $d
                    );
                    if (isset($r['success'])) {
                        $result[$arrFields[$widget->widget_type]] = true;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Return the list of URLs associated with the widget.
     * @param int $accountId
     * @return string
     */
    public static function getURLList($accountId)
    {
        $rl = "";
        $result = "";
        $sql = sprintf(
            "SELECT DISTINCT url, name, widget_type
            FROM widget_url
            INNER JOIN widget
            ON widget_url.widget_id = widget.id
            WHERE widget.account_id = %d;",
            $accountId
        );
        $widgetUrls = Yii::app()->db->createCommand($sql)->query();
        foreach ($widgetUrls as $wu) {
            //We don't want IP addresses to be stored in this field.
            if (!preg_match('/^[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}.[0-9]{1,3}$/', $wu['url'])) {
                $result .= $rl . $wu['url'] . ' ' . $wu['name'] . ' ( ' . $wu['widget_type'] . ' ) ';
                $rl = "\n";
            }
        }

        $arrayResult = explode("\n", $result);
        if (count($arrayResult) > 10) {
            $result = '10+ SiteEnhance URLs (see Doctor.com admin for details)';
        }

        return $result;
    }

    /**
     * Get the date a certain widget type was first added for a given account
     * @param int $accountId
     * @param string $widgetType
     * @return string
     */
    public static function getDateAdded($accountId, $widgetType = '')
    {

        $condition = '';
        if (!empty($widgetType)) {
            $condition = sprintf("AND widget.widget_type IN (%s)", $widgetType);
        }

        $sql = sprintf(
            "SELECT DATE_FORMAT(widget_url.date_added, '%%Y-%%m-%%d') AS date_added
            FROM widget_url
            INNER JOIN widget
            ON widget.id = widget_url.widget_id
            WHERE widget.account_id = %d
            %s
            ORDER BY date_added ASC LIMIT 1",
            $accountId,
            $condition
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryScalar();
    }
}
