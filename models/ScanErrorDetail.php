<?php

Yii::import('application.modules.core_models.models._base.BaseScanErrorDetail');

class ScanErrorDetail extends BaseScanErrorDetail
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Override save function to be able to intercept changes
     * and save them to the scan error logs table.
     *
     * @param boolean $runValidation whether to perform validation before saving the record.
     * @param array $attributes
     * @return bool
     * @throws CDbException
     */
    public function save($runValidation = true, $attributes = null)
    {
        if (!$this->isNewRecord) {
            ScanErrorLog::model()->scanErrorDetailChange($this);
        }

        return parent::save($runValidation, $attributes);
    }

    /**
     * Add a task to this detail
     *
     * @param ScanTask $task
     *
     * @return void
     * @throws Exception
     */
    public function addTask($task)
    {
        $scanErrorTask = new ScanErrorTask();
        $scanErrorTask->scan_error_detail_id = $this->id;
        $scanErrorTask->scan_task_id = $task->id;
        $scanErrorTask->save(false);
    }

    /**
     * Get the CSS class related to this instance
     *
     * @return string
     */
    public function getCssClass()
    {
        if ($this->isBookingButton() && $this->scanError->notFound()) {
            $found = false;
            $match = false;
        } elseif ($this->isBookingButton() && !$this->scanError->hasAppointments()) {
            // This listing doesn't have appointments, so mark it as OK
            $found = true;
            $match = true;
        } elseif ($this->isPhoto() && $this->scanError->isDefaultImage()) {
            // Default image found
            $found = true;
            $match = false;
        } else {
            $found = $this->getMappedFieldValue('result', '');
            $match = $this->getMappedFieldValue('match', false);
        }

        if (!$found && !$match) {
            $class = 'danger';
        } else {
            $class = $match ? 'success' : 'warning';
        }

        return $class;
    }

    /**
     * Get the CSS class related to this instance
     *
     * @return string
     */
    public function isError()
    {
        $found = $this->getMappedFieldValue('result', '');
        $match = $this->getMappedFieldValue('match', false);
        $isError = false;

        if (!$found || !$match) {
            $isError = true;
        }

        return $isError;
    }

    /**
     * Get the related ScanError attribute for this field type.
     * If the requested field doesn't exist, it will return
     * the $default value.
     * @see getFieldTypeMappings
     *
     * Eg:
     *
     * getMappedFieldValue('request', 'n/a')
     * will return the value requested for this field type
     *
     * getMappedFieldValue('result', 'n/a')
     * will return the value scraped/retrieved for this field type
     *
     * @param string $which can be ['request', 'result', 'match']
     * @param mixed $default The value to return if it isn't found
     * @param bool $applyConversions if true, will apply value conversions depending on field type
     *                               E.g: a specialty like 'spec1|spec2' will become 'spec 1 - spec 2'
     *
     * @return mixed
     */
    public function getMappedFieldValue($which, $default = null, $applyConversions = false)
    {
        $map = $this->getFieldTypeMappings();

        $whom = $this->scanError->isProvider() ? 'provider' : 'practice';
        $path = Common::get($map, $this->fieldType->code . "->$whom->$which");

        if ($path) {
            $value = Common::get($this->scanError, $path, $default);
            if ($applyConversions) {
                $value = $this->applyFieldConversions($value);
            }

            return $value;
        } else {
            return $default;
        }
    }

    public function applyFieldConversions($value)
    {
        if ($this->isSpecialty()) {
            $value = implode(' - ', explode('|', $value));
        }

        if ($this->isPhone()) {
            $value = StringComponent::formatPhone($value);
        }

        if ($this->isBookingButton()) {
            // Handle cases where we got the default booking url but with no provider
            // Just add the provider id
            $urlParts = parse_url($value);
            $query = Common::get($urlParts, 'query');
            if ($query) {
                $queryVars = [];
                parse_str($query, $queryVars);
                if (!Common::get($queryVars, 'provider') && $this->scanError->getProvider()) {
                    $value = str_replace('provider=', 'provider=' . $this->scanError->getProvider()->id, $value);
                }
            }

            if ($value && $value != 'n/a') {
                $value = '<a href="' . $value . '" target="_blank">' . $value . '</a>';
            }
        }

        return $value;
    }

    /**
     * Shortcut to determine if a field was a match
     *
     * @return boolean
     */
    public function matched()
    {
        return $this->getMappedFieldValue('match', false);
    }

    /**
     * Returns a mappings array.
     * This array contains the paths where the 'ScanError' object
     * has information related to the scan.
     *
     * For example if you need the provider name that was requested
     * in the scan, the ScanError path will be located at:
     *     $map['scan_field_name']['provider']['request']
     *
     * Another example, if you need the address that was scrapped:
     *     $map['scan_field_address']['practice']['result']
     *
     * Another example, if you need to know if a field matched
     *     $map['scan_field_address']['practice']['match']
     *
     * @param string $which can be ['request', 'result', 'match']
     * @param mixed $default The value to return if it isn't found
     *
     * @return array
     */
    public function getFieldTypeMappings()
    {
        return [
            'scan_field_name' => [
                'provider' => [
                    'request' => 'scanResultProvider->scanRequestProvider->provider_name',
                    'result' => 'scanResultProvider->scraped_name',
                    'match' => 'scanResultProvider->match_name'
                ],
                'practice' => [
                    'request' => 'scanResultPractice->scanRequestPractice->practice_name',
                    'result' => 'scanResultPractice->scraped_name',
                    'match' => 'scanResultPractice->match_name'
                ]
            ],
            'scan_field_address' => [
                'provider' => [
                    'request' => 'scanResultProvider->scanRequestProvider->scanRequestPractice->address',
                    'result' => 'scanResultProvider->scraped_address',
                    'match' => 'scanResultProvider->match_address'
                ],
                'practice' => [
                    'request' => 'scanResultPractice->scanRequestPractice->address',
                    'result' => 'scanResultPractice->scraped_address',
                    'match' => 'scanResultPractice->match_address'
                ]
            ],
            'scan_field_phone' => [
                'provider' => [
                    'request' => 'scanResultProvider->scanRequestProvider->scanRequestPractice->phone_number',
                    'result' => 'scanResultProvider->scraped_phone',
                    'match' => 'scanResultProvider->match_phone'
                ],
                'practice' => [
                    'request' => 'scanResultPractice->scanRequestPractice->phone_number',
                    'result' => 'scanResultPractice->scraped_phone_number',
                    'match' => 'scanResultPractice->match_phone'
                ]
            ],
            'scan_field_specialty' => [
                'provider' => [
                    'request' => 'scanResultProvider->scanRequestProvider->primarySpecialty->name',
                    'result' => 'scanResultProvider->scraped_specialty',
                    'match' => 'scanResultProvider->match_specialty'
                ],
                'practice' => [
                    'request' => 'scanResultPractice->scanRequestPractice->primarySpecialty->name'
                ]
            ],
            'scan_field_booking_button' => [
                'provider' => [
                    'result' => 'scanResultProvider->appointments_url',
                    'match' => 'scanResultProvider->appointments_url'
                ],
                'practice' => [
                    'request' => ''
                ]
            ],
            'scan_field_photo' => [
                'provider' => [
                    'result' => 'scanResultProvider->photo',
                    'match' => 'scanResultProvider->photo'
                ],
                'practice' => [
                    'request' => ''
                ]
            ],
        ];
    }

    /**
     * Get the latest ScanTask assigned to this detail
     *
     * @return ScanTask|null
     */
    public function getLatestTask()
    {
        if ($this->scanErrorTasks) {
            $latest = null;
            foreach ($this->scanErrorTasks as $errorTask) {
                if (!$latest || $latest->id < $errorTask->id) {
                    $latest = $errorTask;
                }
            }

            return $latest->scanTask;
        }

        return null;
    }

    /**
     * Is this a photo field?
     *
     * @return boolean
     */
    public function isPhoto()
    {
        return (int) ($this->fieldType->code === 'scan_field_photo');
    }

    /**
     * Is this a name field?
     *
     * @return boolean
     */
    public function isName()
    {
        return (int) ($this->fieldType->code === 'scan_field_name');
    }

    /**
     * Is this an address field?
     *
     * @return boolean
     */
    public function isAddress()
    {
        return (int) ($this->fieldType->code === 'scan_field_address');
    }

    /**
     * Is this a phone field?
     *
     * @return boolean
     */
    public function isPhone()
    {
        return (int) ($this->fieldType->code === 'scan_field_phone');
    }

    /**
     * Is this a specialty field?
     *
     * @return boolean
     */
    public function isSpecialty()
    {
        return (int) ($this->fieldType->code === 'scan_field_specialty');
    }

    /**
     * Is this an office hours field?
     *
     * @return boolean
     */
    public function isOfficeHours()
    {
        return (int) ($this->fieldType->code === 'scan_field_office_hours');
    }

    /**
     * Is this a booking button field?
     *
     * @return boolean
     */
    public function isBookingButton()
    {
        return (int) ($this->fieldType->code === 'scan_field_booking_button');
    }

    /**
     * Is this name, address or phone?
     *
     * @return boolean
     */
    public function isNAP()
    {
        return (int) ($this->isName() || $this->isAddress() || $this->isPhone());
    }

    /**
     * Get a string representing the provider's primary specialty
     *
     * @return string
     */
    public function getProviderSpecialty()
    {
        $txt = '';
        $spec = $this->scanError->getProvider()->getPrimarySpecialty();

        if ($spec) {
            $txt = $spec->subSpecialty->specialty->name . ' - ' . $spec->subSpecialty->name;
        }

        return $txt;
    }

    /**
     * Get the related partner specialty
     *
     * @return string
     */
    public function getPartnerSpecialty()
    {
        $partnerSpec = '';
        $spec = $this->scanError->getProvider()->getPrimarySpecialty();
        if ($spec) {
            $psem = PartnerSiteEntityMatch::model()->findPartnerSpecialty(
                $spec->subSpecialty->id,
                $this->scanError->getListingType()->listing_site_id
            );

            if ($psem) {
                $partnerSpec = $psem->retrieved_column_value;
            }
        }

        return $partnerSpec;
    }

    /**
     * Get the Salesforce issue text depending on the field type
     *
     * @return string
     */
    public function getSalesforceIssue()
    {
        $mappings = [
            'scan_field_name_provider' => 'Name (prov)',
            'scan_field_name_practice' => 'Name (prac)',
            'scan_field_address' => 'Address',
            'scan_field_phone' => 'Phone / Fax',
            'scan_field_photo' => 'Photo',
            'scan_field_specialty' => 'Specialty / Category',
            'scan_field_duplicate' => 'Duplicate',
            'scan_field_not_featured' => 'Not Featured',
            'scan_field_office_hours' => 'Office Hours',
            'scan_field_reviews' => 'Reviews',
            'scan_field_rich_date' => 'Rich Data',
            'scan_field_tracking_number' => 'Tracking Number',
            'scan_field_website' => 'Website',
            'scan_field_booking_button' => 'Booking Button',
            'scan_field_no_profile' => 'No Profile',
            'scan_field_practice_add' => 'Practice (add)',
            'scan_field_practice_remove' => 'Practice (remove)'
        ];

        $key = $this->fieldType->code;
        if ($key === 'scan_field_name') {
            $key = 'scan_field_name_practice';
            if ($this->scanError->isProvider()) {
                $key = 'scan_field_name_provider';
            }
        }

        return Common::get($mappings, $key);
    }

    /**
     * Determine if the instance has tracking numbers
     *
     * @return boolean
     */
    public function hasTrackingNumbers()
    {
        return (bool)$this->getTrackingNumbers();
    }

    /**
     * Get all the tracking numbers related to this detail
     *
     * @return array
     */
    public function getTrackingNumbers()
    {
        $matched = [];

        if ($this->scanError->isProvider()) {
            $practice = $this->scanError->getProvider()->primaryPractice();
        } else {
            $practice = $this->scanError->getPractice();
        }

        if ($practice) {
            $numbers = $practice->twilioNumbers;
            if ($numbers) {
                foreach ($numbers as $number) {
                    if ($number->twilioPartner && $number->twilioPartner->partnerSite) {
                        if ($this->scanError->getListingType()->listing_site_id ===
                            $number->twilioPartner->partnerSite->listing_site_id) {
                            $matched[] = $number;
                        }
                    }
                }
            }
        }

        return $matched;
    }

    /**
     * Utility function for the frontend.
     * Returns a CSS class for default/non-default field types
     *
     * @return string
     */
    public function getMainTableCssClass()
    {
        $class = 'detail-row default-field';
        if (!$this->fieldType->isDefaultForScanError()) {
            $class = 'detail-row extra-field';
        }

        return $class;
    }

    public function shouldDisplay()
    {
        if ($this->scanError->isPractice() && $this->fieldType->isProviderOnlyScanError()) {
            return false;
        }

        return true;
    }
}
