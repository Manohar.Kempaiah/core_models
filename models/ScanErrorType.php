<?php

Yii::import('application.modules.core_models.models._base.BaseScanErrorType');

class ScanErrorType extends BaseScanErrorType
{
    const FALSE_POSITIVE = 1;
    const SYNC_ERROR = 2;
    const PARTNER_ERROR = 3;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
