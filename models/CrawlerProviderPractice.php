<?php

Yii::import('application.modules.core_models.models._base.BaseCrawlerProviderPractice');

class CrawlerProviderPractice extends BaseCrawlerProviderPractice
{
    /**
     * Returns the static model of the specified AR class.
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * This method is invoked before validation starts.	
     */
    public function beforeValidate()
    {
        if (empty($this->date_added)) {
            $this->date_added = date('Y-m-d H:i:s');
        }
        $this->date_updated = date('Y-m-d H:i:s');

        return parent::beforeValidate();
    }

    /**
     * This method is invoked before saving a record (after validation).	
     */
    public function beforeSave()
    {
        // set address and address_2 default values
        $this->address = $this->street1;
        $this->address_2 = $this->street2;

        // Do you have a zipcode?
        if ($this->zipcode) {
            // Then search for the location
            $this->enrichLocationData();
        }

        return parent::beforeSave();
    }

    /**
     * Enrich location data from geocoding.
     */
    private function enrichLocationData()
    {
        // Get the location
        $location = Location::model()->find(
            'zipcode = :zipcode',
            [':zipcode' => substr($this->zipcode, 0, 5)]
        );

        // Do we found the location?
        if ($location) {

            // Get the geolocation data
            $address = $this->street1;
            if ($this->street2) {
                $address .= ' ' . $this->street2;
            }
            $locationData = Location::getLatitudeLongitude($address, $location->id, 'array');

            // Set the latitute and longitude from geodata
            if ($locationData && !empty($locationData['latitude']) && !empty($locationData['longitude'])) {
                $this->latitude = $locationData['latitude'];
                $this->longitude = $locationData['longitude'];
            }

            // Update the address, address_2, zipcode, city, state from the geolocation data
            if ($locationData && !empty($locationData['payload'])) {

                $this->updateFromAddressComponents(
                    json_decode($locationData['payload'])->results[0]->address_components
                );
            }
        }
    }

    /**
     * Update location attributes from address components
     * 
     * @param array $addressComponent 
     */
    private function updateFromAddressComponents($addressComponents)
    {
        $street = '';
        foreach ($addressComponents as $addressComponent) {

            if ($addressComponent->types[0] == 'street_number') {
                $street = $addressComponent->long_name;
            }
            if ($addressComponent->types[0] == 'route') {
                $street .= ' ' . $addressComponent->long_name;
            }
            if ($addressComponent->types[0] == 'subpremise') {
                $this->address_2 = $addressComponent->long_name;
            }
            if ($addressComponent->types[0] == 'locality') {
                $this->city = $addressComponent->long_name;
            }
            if ($addressComponent->types[0] == 'administrative_area_level_1') {
                $this->state = $addressComponent->short_name;
            }
            if ($addressComponent->types[0] == 'postal_code') {
                $this->zipcode = $addressComponent->short_name;
            }
        }
        if (!empty($street)) {
            $this->address = $street;
        }
    }
}
