<?php

Yii::import('application.modules.core_models.models._base.BaseAppointment');
Yii::import('application.modules.providers.protected.components.CapybaraCache');


class Appointment extends BaseAppointment
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Populate ny_start_datetime
     * @return bool
     */
    public function beforeSave()
    {
        $timeZone = $this->practice->getTimeZone();
        $time = $this->appointment_date . ' ' . $this->time_start;
        $nyTimeStart = new DateTime($time, new DateTimeZone($timeZone['phpTimezone']));
        $nyTimeStart->setTimezone(new DateTimeZone('America/New_York'));
        $this->ny_start_datetime = $nyTimeStart->format('Y-m-d H:i:s');

        return parent::beforeSave();
    }

    /**
     * Update cache according to status
     * @return void
     */
    public function afterSave()
    {
        // Is this an approved appointment?
        if ($this->status == 'A') {
            // yes, add to cache
            CapybaraCache::addAppointment($this);
        } else {
            // no, remove from cache
            CapybaraCache::removeAppointment($this);
        }

        return parent::afterSave();
    }

    public function afterDelete()
    {
        CapybaraCache::removeAppointment($this);

        return parent::afterDelete();
    }

    /**
     * MI7 - New Appointment
     *
     * @param array $appointment
     * @param int $accountId
     * @param int $aEmrId
     * @param int $practiceId
     * @return array
     */
    public static function mi7NewAppointment($appointment, $accountId, $aEmrId, $practiceId)
    {
        $result = array();

        $aPatientEmr = AccountPatientEmr::model()->find(
            "account_id = :account_id AND mi7_patient_id = :mi7_patient_id",
            array(":account_id" => $accountId, ":mi7_patient_id" => $appointment['PatientID_MI7'])
        );

        if (empty($aPatientEmr)) {
            $res = Patient::mi7NewPatient($appointment, $accountId, $aEmrId, $practiceId);

            if (!$res['result']) {
                return $res;
            } else {
                $patientId = $res['patientId'];
            }
        } else {

            Patient::mi7UpdatePatient($appointment, $accountId, $aEmrId, $practiceId);

            $patientId = $aPatientEmr->patient_id;
        }

        $resultProvider = AccountProviderEmr::mi7FindProviderEmr($appointment, $accountId, $aEmrId);

        if ($resultProvider['result']) {
            $providerId = $resultProvider['providerId'];
        } else {
            return $resultProvider;
        }

        $sM = Appointment::model()->find(
            "emr_appointment_id = :emr_appointment_id",
            array(":emr_appointment_id" => $aEmrId . $appointment['PlacerID'])
        );
        if ($sM) {
            $result['result'] = false;
            $result['results'] = "Appointment Already Created";
            return $result;
        } else {
            $timeZone = Practice::model()->findByPk($practiceId)->getTimeZone();

            $dateApp = explode("T", $appointment['StartDate']);
            $appointmentDate = $dateApp[0];

            $startDate = explode("Z", $dateApp[1]);
            $startDate = $startDate[0];

            $date = new DateTime($appointmentDate . ' ' . $startDate, new DateTimeZone('UTC'));
            $date->setTimezone(new DateTimeZone($timeZone['phpTimezone']));

            // covertion patch
            if ($date->format('i:s') == '59:59') {
                $date->modify('+1 second');
            }
            $appointmentDate = $date->format('Y-m-d');
            $startDate = $date->format('H:i:s');

            $dateApp = explode("T", $appointment['EndDate']);

            $endDate = explode("Z", $dateApp[1]);
            $endDate = $endDate[0];

            $date = new DateTime($appointmentDate . ' ' . $endDate, new DateTimeZone('UTC'));
            $date->setTimezone(new DateTimeZone($timeZone['phpTimezone']));

            // covertion patch
            if ($date->format('i:s') == '59:59') {
                $date->modify('+1 second');
            }
            $endDate = $date->format('H:i:s');

            //@TODO: Validate Appointment Already created
            //$sM = Appointment::model()->find("provider_id = :provider_id AND appointment_date = :appointment_date AND"
            //        . " ( time_start <= :timestart AND time_end >= :timestart OR "
            //        . " time_start <= :timeend AND time_end >= :timeend ) ",
            //        array(":provider_id" => $aProviderEmr->provider_id, ":appointment_date" => $appointmentDate,
            //             ":timestart" => $startDate,":timeend" => $endDate));
            //if ($sM) {
            //    $result['result'] = false;
            //    $result['results'] = "The provider already has an appointment assigned for this time";
            //    return $result;
            //}
            //

            $sm = new Appointment;
            $sm->emr_appointment_id = $aEmrId . $appointment['PlacerID'];
            $sm->provider_id = $providerId;
            $sm->patient_id = $patientId;
            $sm->practice_id = $practiceId;
            $sm->status = 'A';
            $sm->first_name = empty($appointment['FirstName']) ? 'Unknown' : $appointment['FirstName'];
            $sm->last_name = empty($appointment['LastName']) ? 'Unkown' : $appointment['LastName'];
            $sm->cell_phone = preg_replace('/[^0-9]/', '', $appointment['CellPhone']);

            if (!empty($appointment['HomeEmailAddress'])) {
                $sm->email = $appointment['HomeEmailAddress'];
            }

            $sm->reason = $appointment['AppointmentType'] . ' - ' . $appointment['AppointmentReason'];
            $sm->appointment_date = $appointmentDate;
            $sm->time_start = $startDate;
            $sm->time_end = $endDate;

            if ($sm->save()) {

                // Block timeblock in appointments widget
                AccountEmr::importAccountEmr($accountId);

                $result['result'] = true;
                $result['results'] = "success";
            } else {
                $result['result'] = false;
                $result['results'] = json_encode($sm->errors);
            }
        }

        return $result;
    }

    /**
     * MI7 - Rescheduled Appointment
     *
     * @param array $appointment
     * @param int $accountId
     * @param int $aEmrId
     * @param int $practiceId
     * @return array
     */
    public static function mi7RescheduledAppointment($appointment, $accountId, $aEmrId, $practiceId)
    {
        $ap = Appointment::model()->find(
            "emr_appointment_id = :emr_appointment_id",
            array(":emr_appointment_id" => $aEmrId . $appointment['PlacerID'])
        );

        $sm = SchedulingMail::model()->find(
            "emr_appointment_id = :emr_appointment_id",
            array(":emr_appointment_id" => $aEmrId . $appointment['PlacerID'])
        );

        if ($ap || $sm) {
            $timeZone = Practice::model()->findByPk($practiceId)->getTimeZone();

            $dateApp = explode("T", $appointment['StartDate']);
            $appointmentDate = $dateApp[0];

            $startDate = explode("Z", $dateApp[1]);
            $startDate = $startDate[0];

            $date = new DateTime($appointmentDate . ' ' . $startDate, new DateTimeZone('UTC'));
            $date->setTimezone(new DateTimeZone($timeZone['phpTimezone']));

            $appointmentDate = $date->format('Y-m-d');
            $startDate = $date->format('H:i:s');

            $dateApp = explode("T", $appointment['EndDate']);

            $endDate = explode("Z", $dateApp[1]);
            $endDate = $endDate[0];

            $date = new DateTime($appointmentDate . ' ' . $endDate, new DateTimeZone('UTC'));
            $date->setTimezone(new DateTimeZone($timeZone['phpTimezone']));
            $endDate = $date->format('H:i:s');

            $successAp = true;
            $successSm = true;
            $errors = null;

            if ($ap) {
                if (!empty($ap->previous_date) && !empty($ap->previous_time_start) && !empty($ap->previous_time_end)) {
                    $ap->previous_date = $ap->appointment_date;
                    $ap->previous_time_start = $ap->time_start;
                    $ap->previous_time_end = $ap->time_end;
                }
                $ap->appointment_date = $appointmentDate;
                $ap->time_start = $startDate;
                $ap->time_end = $endDate;

                $successAp = $ap->save();
                if (!$successAp) {
                    $errors = $ap->errors;
                }
            }

            if ($sm) {
                if (empty($sm->previous_date) && empty($sm->previous_time_start) && empty($sm->previous_time_end)) {
                    $sm->previous_date = $sm->appointment_date;
                    $sm->previous_time_start = $sm->time_start;
                    $sm->previous_time_end = $sm->time_end;
                }
                $sm->appointment_date = $appointmentDate;
                $sm->time_start = $startDate;
                $sm->time_end = $endDate;

                $successSm = $sm->save();
                if (!$successSm) {
                    $errors = $sm->errors;
                }
            }

            if ($successAp && $successSm) {
                // Block timeblock in appointments widget
                AccountEmr::importAccountEmr($accountId);

                $result['result'] = true;
                $result['results'] = "success";
            } else {
                $result['result'] = false;
                $result['results'] = json_encode($errors);
            }
        } else {
            $result['result'] = false;
            $result['results'] = "The appointment doesn't exist";
        }

        return $result;
    }

    /**
     * MI7 - Updated Appointment
     *
     * @param array $appointment
     * @param int $accountId
     * @param int $aEmrId
     * @param int $practiceId
     * @return array
     */
    public static function mi7UpdatedAppointment($appointment, $accountId, $aEmrId, $practiceId)
    {
        return self::mi7RescheduledAppointment($appointment, $accountId, $aEmrId, $practiceId);
    }

    /**
     * MI7 - Cancelled Appointment
     *
     * @param array $appointment
     * @param int $accountId
     * @param int $aEmrId
     * @return array
     */
    public static function mi7CancelledAppointment($appointment, $accountId, $aEmrId)
    {

        $result = array();

        $ap = Appointment::model()->find(
            "emr_appointment_id = :emr_appointment_id",
            array(":emr_appointment_id" => $aEmrId . $appointment['PlacerID'])
        );

        $sm = SchedulingMail::model()->find(
            "emr_appointment_id = :emr_appointment_id",
            array(":emr_appointment_id" => $aEmrId . $appointment['PlacerID'])
        );

        if ($ap || $sm) {

            if ($ap) {
                $ap->status = "R";
                $ap->save();
            }

            if ($sm) {
                $sm->status = "R";
                $sm->save();
            }

            $result['result'] = true;
            $result['results'] = "success";

            // Block timeblock in appointments widget
            AccountEmr::importAccountEmr($accountId);
        } else {
            $result['result'] = false;
            $result['results'] = "The appointment doesn't exist";
        }

        return $result;
    }

    /**
     * MI7 - Delete Appointment
     *
     * @param array $appointment
     * @param int $accountId
     * @param int $aEmrId
     * @return array
     */
    public static function mi7DeleteAppointment($appointment, $accountId, $aEmrId)
    {
        $ap = Appointment::model()->find(
            "emr_appointment_id = :emr_appointment_id",
            array(":emr_appointment_id" => $aEmrId . $appointment['PlacerID'])
        );

        $sm = SchedulingMail::model()->find(
            "emr_appointment_id = :emr_appointment_id",
            array(":emr_appointment_id" => $aEmrId . $appointment['PlacerID'])
        );

        if ($ap || $sm) {

            if ($ap) {
                $ap->reason = "DELETED " . $ap->reason;
                $ap->save();
            }

            if ($sm) {
                $sm->reason = "DELETED " . $sm->reason;
                $sm->save();
            }
        }
        return self::mi7CancelledAppointment($appointment, $accountId, $aEmrId);
    }

    /**
     * Find if this appointment is free or not
     * @param $appData
     * $appData['schedulingMailId']
     * $appData['newStatus']
     * $appData['newDate']
     * $appData['startHour']
     * $appData['startMin']
     * $appData['startAmPm']
     * $appData['endHour']
     * $appData['endMin']
     * $appData['endAmPm']
     * $appData['overBooking']
     * @return bool $isFree
     */
    public static function isAppointmentFree($appData = '')
    {
        $schedulingMail = SchedulingMail::model()->find('id=:id ', array(':id' => $appData['schedulingMailId']));

        $appDate = $appData['newDate'];
        $timeStart = $appData['startHour'] . ':' . $appData['startMin'] . ' ' . $appData['startAmPm'];
        $timeEnd = $appData['endHour'] . ':' . $appData['endMin'] . ' ' . $appData['endAmPm'];
        $newStartHour = date("H:i:s", strtotime($timeStart));
        $newEndHour = date("H:i:s", strtotime($timeEnd));


        // Disallow overbooking
        $allowOverbooking = false;

        // Get operatories at Provider-Practice level
        $operatories = ProviderPractice::getOperatories($schedulingMail->practice_id, $schedulingMail->provider_id);

        // Filter by ProviderId by default
        $providerSql = 'provider_id = ' . $schedulingMail->provider_id . ' AND';

        // If there's no operatories, get them at Practice level
        if (!$operatories) {
            $operatories = Practice::getOperatories($schedulingMail->practice_id);

            // If there's operatories at Practice level, modify query to include all appointments for Practice
            if ($operatories) {
                $providerSql = '';
            }
        }

        // Set default value in case there's no operatories
        if (!$operatories) {
            $operatories = 1;
            // Allow overbooking, since there are no operatories set
            $allowOverbooking = true;
        }


        $sql = sprintf(
            "SELECT COUNT(*) FROM appointment
                WHERE %s practice_id = %d
                AND appointment_date = '%s'
                AND del_flag = 0 AND enabled_flag = 1
                AND (
                    (TIME_TO_SEC('%s') < TIME_TO_SEC(time_start) AND TIME_TO_SEC('%s') > TIME_TO_SEC(time_end))
                    OR (TIME_TO_SEC('%s') >= TIME_TO_SEC(time_start) AND TIME_TO_SEC('%s') < TIME_TO_SEC(time_end))
                    OR (TIME_TO_SEC('%s') > TIME_TO_SEC(time_start) AND TIME_TO_SEC('%s') <= TIME_TO_SEC(time_end))
                )",
            $providerSql,
            $schedulingMail->practice_id,
            $appDate,
            $newStartHour,
            $newEndHour,
            $newStartHour,
            $newStartHour,
            $newEndHour,
            $newEndHour
        );

        $appointmentsCount = Yii::app()->db->createCommand($sql)->queryScalar();

        // If there are not appointments for date-time
        if (empty($appointmentsCount) || $appointmentsCount < $operatories) {
            // Appointment date-time is free
            return 1;
        }

        if ($allowOverbooking && $appData['overBooking'] == 1) {
            // Appointment date-time is free with overbooking
            return 2;
        }

        // Appointment date-time is in use
        return 0;
    }

    /**
     * Find if this new appointment is inside practice office hours range
     * @param array () $appData
     * $appData['schedulingMailId'] = Yii::app()->request->getParam('schedulingMailId', false);
     * $appData['newStatus'] = Yii::app()->request->getParam('newStatus', false);
     * $appData['newDate'] = Yii::app()->request->getParam('newDate', false);
     * $appData['startHour'] = Yii::app()->request->getParam('startHour', false);
     * $appData['startMin'] = Yii::app()->request->getParam('startMin', false);
     * $appData['startAmPm'] = Yii::app()->request->getParam('startAmPm', false);
     * $appData['endHour'] = Yii::app()->request->getParam('startHour', false);
     * $appData['endMin'] = Yii::app()->request->getParam('startMin', false);
     * $appData['endAmPm'] = Yii::app()->request->getParam('startAmPm', false);
     * $appData['overBooking'] = Yii::app()->request->getParam('overBooking', 0);
     * $appData['practiceId'] = Yii::app()->request->getParam('practiceId', 0);
     * @return bool $isFree
     */
    public static function isInsidePracticeOfficeHours($appData = '')
    {
        // Possible values for $isFree
        // 0: This appointment is in use
        // 1: Is free for use
        // 2: Is an overBooking
        // 3: Out of practice office hours

        $isFree = 1;
        if ($appData['newStatus'] == 'RE' && !empty($appData['practiceId'])) {
            $schedulingMail = SchedulingMail::model()->find('id=:id ', array(':id' => $appData['schedulingMailId']));

            if (!$schedulingMail) {
                return $isFree;
            }

            $apptDuration = empty($apptDuration) ? 30 : $apptDuration;

            // New date time setting for reschedule
            list($year, $month, $day) = explode('-', $appData['newDate']);
            $newDate = $month . '/' . $day . '/' . $year;

            $appScheduler = new AppointmentScheduler($schedulingMail->provider_id, $schedulingMail->practice_id);
            $appScheduler->setDuration($apptDuration);

            $minTime = $maxTime = $fMinTime = $fMaxTime = '';

            foreach ($appScheduler->getWeekAvailability($appData['newDate']) as $date => $values) {
                if ($date == $newDate) {
                    foreach ($values['times'] as $range) {
                        if (!empty($minTime)) {
                            $fMinTime = date("H:i", strtotime($minTime));
                        }
                        $rangeFrom = date("H:i", strtotime($range[0]));
                        if (empty($fMinTime) || $rangeFrom < $fMinTime) {
                            $minTime = $range[0];
                        }

                        if (!empty($maxTime)) {
                            $fMaxTime = date("H:i", strtotime($maxTime));
                        }
                        $rangeTo = date("H:i", strtotime($range[1]));
                        if (empty($fMaxTime) || $fMaxTime < $rangeTo) {
                            $maxTime = $range[1];
                        }

                    }
                }
            }

            // New date time setting for reschedule
            if (!empty($minTime) && !empty($maxTime)) {
                $rangeFrom = date("H:i", strtotime($minTime));
                $rangeTo = date("H:i", strtotime($maxTime));

                $timeStart = $appData['startHour'] . ':' . $appData['startMin'] . ' ' . $appData['startAmPm'];
                $timeEnd = $appData['endHour'] . ':' . $appData['endMin'] . ' ' . $appData['endAmPm'];
                $appFrom = date("H:i", strtotime($timeStart));
                $appTo = date("H:i", strtotime($timeEnd));
                if (strtotime($appFrom) < strtotime($rangeFrom) || strtotime($appTo) > strtotime($rangeTo)) {
                    // New time is outside practice office range
                    $isFree = 3;
                }

            } else {
                // This day the practice is closed
                $isFree = 3;
            }

        }
        return $isFree;
    }

    /**
     * Find if this new appointment is inside vacation range
     * @param array () $appData
     * $appData['schedulingMailId'] = Yii::app()->request->getParam('schedulingMailId', false);
     * $appData['newStatus'] = Yii::app()->request->getParam('newStatus', false);
     * $appData['newDate'] = Yii::app()->request->getParam('newDate', false);
     * $appData['startHour'] = Yii::app()->request->getParam('startHour', false);
     * $appData['startMin'] = Yii::app()->request->getParam('startMin', false);
     * $appData['startAmPm'] = Yii::app()->request->getParam('startAmPm', false);
     * $appData['endHour'] = Yii::app()->request->getParam('startHour', false);
     * $appData['endMin'] = Yii::app()->request->getParam('startMin', false);
     * $appData['endAmPm'] = Yii::app()->request->getParam('startAmPm', false);
     * $appData['overBooking'] = Yii::app()->request->getParam('overBooking', 0);
     * $appData['practiceId'] = Yii::app()->request->getParam('practiceId', 0);
     * @return bool $isFree
     */
    public static function isInsideVacation($appData = '')
    {
        // Possible values for $isFree
        // 0: This appointment is in use
        // 1: Is free for use
        // 2: Is an overBooking
        // 3: Out of practice office hours
        // 4: Invalid Date
        // 5: In Vacation
        // 6: In Scheduled Exception

        $isFree = 1;
        if ($appData['newStatus'] == 'RE' && !empty($appData['practiceId'])) {
            $schedulingMail = SchedulingMail::model()->find('id=:id ', array(':id' => $appData['schedulingMailId']));

            if (!$schedulingMail) {
                return $isFree;
            }

            // New date time setting for reschedule
            list($year, $month, $day) = explode('-', $appData['newDate']);
            $newDate = $month . '/' . $day . '/' . $year;

            $vacations =
                EmrScheduleException::getVacations($schedulingMail->provider_id, $schedulingMail->practice_id);
            foreach ($vacations as $values) {
                $scheduleDate= date('Y-m-d', strtotime($newDate));
                $vacationDateBegin = date('Y-m-d', strtotime($values->date_start));
                $vacationDateEnd = date('Y-m-d', strtotime($values->date_end));

                if (($scheduleDate >= $vacationDateBegin) && ($scheduleDate <= $vacationDateEnd)) {
                    $isFree = '5';
                }
            }
        }
        return $isFree;
    }

    /**
     * Get a list of appointments whose notifications need to be sent (mail, sms, autocall, staff call)
     * Conditions: one of the reminders is set in the appointment, days to appointment between 0 and 4, hours to
     * appointment match what's set by the practice in account_practice_emr
     * The "5 MINUTE" thing should be updated if the schedule for Jenkins is updated
     * @see AppointmentCommand.php::sendPatientReminders()
     * @return array
     */
    public static function getPendingEHRPatientReminders()
    {

        $sql = sprintf("SELECT *, appointment.id AS s_id,
            appointment.patient_id AS s_patient_id,
            appointment.practice_id AS s_practice_id,
            appointment.provider_id AS s_provider_id,
            IF(pre_appt_reminder_mail > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_mail HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_mail,
            IF(pre_appt_reminder_sms > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_sms HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_sms,
            IF(pre_appt_reminder_autocall > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_autocall HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_autocall,
            IF(pre_appt_reminder_staffcall > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_staffcall HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE), 1, 0) AS send_staffcall,
            pre_appt_reminder_mail, pre_appt_reminder_sms, pre_appt_reminder_autocall, pre_appt_reminder_staffcall
            FROM appointment
            INNER JOIN account_practice_emr ON appointment.practice_id = account_practice_emr.practice_id
            INNER JOIN account_emr ON account_emr.account_id = account_practice_emr.account_id
            INNER JOIN emr_type ON account_emr.emr_type_id = emr_type.id
            WHERE account_practice_emr.send_appointment_reminders='1'
            AND status = 'A'
            AND appointment.del_flag = 0 AND appointment.enabled_flag = 1
            AND (
              SELECT sm.emr_appointment_id FROM scheduling_mail sm
              WHERE sm.emr_appointment_id = appointment.emr_appointment_id) IS NULL
            AND (
                (pre_appt_reminder_mail > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_mail HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))

                OR (pre_appt_reminder_sms > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_sms HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))

                OR (pre_appt_reminder_autocall > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_autocall HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))

                OR (pre_appt_reminder_staffcall > 0
                AND DATE_SUB(ny_start_datetime, INTERVAL pre_appt_reminder_staffcall HOUR)
                BETWEEN NOW() AND DATE_ADD(NOW(), INTERVAL 5 MINUTE))
            )
            GROUP BY appointment.id;"
        );
        return Yii::app()->dbRO->createCommand($sql)->queryAll();
    }

    /**
     * More readable result
     * @param $isFree
     * @return array
     */
    public static function processResult($isFree = 0)
    {
        $dataReturn[0] = 'APPOINTMENT_IN_USE';
        $dataReturn[1] = 'FREE_FOR_USE';
        $dataReturn[2] = 'IS_OVERBOOKING';
        $dataReturn[3] = 'OUT_OF_PRACTICE_OFFICE_HOURS';
        $dataReturn[4] = 'INVALID_DATE';
        $dataReturn[5] = 'IS_IN_VACATION';
        $dataReturn[6] = 'IS_IN_EXCEPTION';

        $results['success'] = false;
        $results['error'] = $dataReturn[$isFree];
        // We added 3 as well because you can schedule out of office hours
        // We also added the status because we need to know to inform the user if the schedule was
        // out of office hours or in vacation
        if ($isFree == 1 || $isFree == 3 || $isFree == 5 || $isFree == 6) {
            $results['success'] = true;
            $results['data']['status'] = $dataReturn[$isFree];
        }
        return $results;
    }

    /**
     * Validate the appointment request
     * @param array () $appData
     * $appData['schedulingMailId'] ;
     * $appData['newStatus'] = REschedule | Approve | Reject;
     * $appData[newHour] => 09
     * $appData[newMin] => 30
     * $appData[newAmPm] => AM
     * $appData['newDate'] = mm-dd-yyyy;
     * $appData['startHour'] = 11;
     * $appData['startMin'] = 25;
     * $appData['startAmPm'] = AM;
     * $appData['endHour'] = 11;
     * $appData['endMin'] = 50;
     * $appData['endAmPm'] = AM;
     * $appData['overBooking'] = 0;
     * $appData['practiceId'] ;
     * $appData[mi7Integration] => 0
     * $appData[confirmReject] => 0
     * $appData[doNotSendEmail] => 0
     * $appData[emrAditionalInformation] => 0
     * $appData[schedulingMailEhrPatient] => 0
     * @return bool $isFree
     */
    public static function validateAppointmentRequest($appData = '')
    {
        // First of all check if it's a valid date
        $validDate = false;

        // Check date for the proper format - Transform to Y-m-d if needed
        $date = $appData['newDate'];
        if (!preg_match('/^d{4}/', $appData['newDate'])) {
            $date = substr($appData['newDate'], 6);
            $date.= '-' . substr($appData['newDate'], 0, 2);
            $date.= '-' . substr($appData['newDate'], 3, 2);

            $appData['newDate'] = $date; // Date format: YYYY-mm-dd
        }

        // Check if it's a valid date and it the appointment is not set in the past
        if (is_numeric(strtotime($date)) && $date >= date('Y-m-d')) {
            $validDate = true;
        }

        // Possible values for $isFree in self::processResult();
        if ($validDate) {

            $isFree = SchedulingMail::isAppointmentFree($appData);

            if ($isFree == 1 && $appData['mi7Integration'] == 1) {
                // Find in EHR appointments
                $isFree = Appointment::isAppointmentFree($appData);
            }
            // We are removing this check as we will allow users to schedule outside of office hours as well
            if ($isFree == 1) {
                $isFree = SchedulingMail::isInsidePracticeOfficeHours($appData);
                if ($appData['mi7Integration'] == 1) {
                    // Find in EHR calendar
                    $isFree = Appointment::isInsidePracticeOfficeHours($appData);
                }
            }
            // Check if it falls in a date where the provider has a vacation set
            if ($isFree == 1) {

                $isFree = Appointment::isInsideVacation($appData);
            }
            // Check if it falls in a date where the provider has a vacation set
            if ($isFree == 1) {
                $isFree = EmrScheduleException::isInsideScheduledException($appData);
            }
        } else {
            $isFree = 4;
        }
        return $isFree;

    }

    /**
     * Get appointment details
     * @param $schedulingMailId
     * @param $accountId
     * @param string $statusChange
     * @return array $return
     */
    public static function getAppointmentDetails($schedulingMailId = 0, $accountId = 0, $statusChange = 'ALL')
    {

        $arrScheduling = SchedulingMail::model()->getOnlineAppointmentRequestDetails($schedulingMailId, $accountId);
        if (empty($arrScheduling)) {
            return array();
        }
        $arrScheduling['statusChange'] = ($statusChange == 'undefined') ? 'ALL' : $statusChange;

        // ehr match patient
        $matchedPatientData = SchedulingMailEhrPatient::getMatchedPatientData($schedulingMailId);

        // Prepare hours range
        $arrHour = $arrMin = array();
        for ($z = 1; $z <= 12; $z = $z + 1) {
            $hour = str_pad($z, 2, "0", STR_PAD_LEFT);
            $arrHour[$hour] = $hour;
        }
        for ($i = 0; $i < 60; $i = $i + 5) {
            $min = str_pad($i, 2, "0", STR_PAD_LEFT);
            $arrMin[$min] = $min;
        }

        // processes the recorded time
        $dbTimeStart = $arrScheduling['time_start'];
        $dbTimeEnd = $arrScheduling['time_end'];
        if (empty($arrScheduling['time_start'])) {
            $regTimeStart = date("g:i:A", strtotime('09:00'));
            $regTimeEnd = date("g:i:A", strtotime('09:30'));
        } else {
            $regTimeStart = date("g:i:A", strtotime($dbTimeStart));
            $regTimeEnd = date("g:i:A", strtotime($dbTimeEnd));
        }
        $arrTime['dif'] = date("H:i:s", strtotime("00:00:00") + strtotime($dbTimeEnd) - strtotime($dbTimeStart));
        $arrTime['start'] = explode(':', $regTimeStart);
        $arrTime['end'] = explode(':', $regTimeEnd);

        $arrScheduling['estimated_end_time'] = str_pad($arrTime['end'][0], 2, "0", STR_PAD_LEFT) . ':' .
            str_pad($arrTime['end'][1], 2, "0", STR_PAD_LEFT) . ' ' . $arrTime['end'][2];

        // Get practice and Practice Office Hours
        $practiceId = $arrScheduling['practice_id'];
        $practice = Practice::getNameAndAddress($practiceId);

        $mi7UserId = AccountProviderEmr::isMi7User($arrScheduling['provider_id'], $accountId);

        // If is EmrUser
        $emrUserId = AccountProviderEmr::isEmrUser($arrScheduling['provider_id'], $accountId);
        // If is Mi7User, get data from EmrType
        $arrEmrType = AccountEmr::getAccountEmr($accountId, false);

        $provider = Provider::model()->find('id=:id', array(':id' => $arrScheduling['provider_id']))->first_middle_last;

        // was this a rejected request or an already confirmed cancelled appointment?
        $isCancelledAppointment = false;
        if ($arrScheduling['status'] == 'R') {
            $isCancelledAppointment = SchedulingMailHistory::model()->exists(
                'scheduling_mail_id=:schedulingMailId AND `status` = "A"',
                [':schedulingMailId' => $arrScheduling['id']]
            );
        }

        return array(
            'scheduling' => $arrScheduling,
            'practice' => $practice,
            'provider' => $provider,
            'account_id' => $accountId,
            'arrHour' => $arrHour,
            'arrMin' => $arrMin,
            'arrTime' => $arrTime,
            'mi7UserId' => $mi7UserId,
            'emrUserId' => $emrUserId,
            'arrEmrType' => $arrEmrType,
            'smEhrPatient' => $matchedPatientData["smEhrPatient"],
            'ehrPatient' => $matchedPatientData["ehrPatient"],
            'isCancelledAppointment' => $isCancelledAppointment
        );
    }

    /**
     * Update appointment - Change status between: Approve, Reject, Reschedule
     * @param array $apptData
     * @param array $newData
     * @param int $accountId
     * @return array $return
     */
    public static function updateAppointmentApi($apptData = null, $newData = null, $accountId = 0, $useCache = true)
    {
        //We get the owner_account_id based on the account id of the actual user
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);

        $confirmReject = $apptData['confirmReject'];
        $doNotSendEmail = $apptData['doNotSendEmail'];
        $schedulingMailId = !empty($apptData['schedulingMailId']) ? (int)$apptData['schedulingMailId'] : 0;
        $emrAditionalInformation = $apptData['emrAditionalInformation'];
        $schedulingMailEhrPatient = $apptData['schedulingMailEhrPatient'];
        $newStatus = !empty($apptData['newStatus']) ? $apptData['newStatus'] : '';

        $cacheTime = $useCache ? Yii::app()->params['cache_short'] : 0;
        $schedulingMail = SchedulingMail::model()->cache($cacheTime)->find(
            'id=:id',
            array(':id' => $schedulingMailId)
        );

        // appointment exists?
        if (!isset($schedulingMail->id)) {
            return array(
                'success' => false,
                'error' => 'NOTHING_TO_CHANGE',
                'data' => array()
            );
        }

        if (!empty($newData['newDate'])) {
            $newData['newDate'] = str_replace('/', '-', $newData['newDate']);

            // Check if date is in a valid format
            if (!preg_match('/^(\d{4}-\d{2}-\d{2})|(\d{2}-\d{2}-\d{4})$/', $newData['newDate'])) {
                return array(
                    'success' => false,
                    'error' => 'INVALID_DATE',
                    'data' => array()
                );
            }
        }

        $result['success'] = true;
        $result['error'] = '';
        $result['data']['id'] = $schedulingMail->id;
        if ($newStatus == 'RE' || ($newStatus != $schedulingMail->status)) {
            // Save new Status
            $objProvider = Provider::model()->cache($cacheTime)->findByPk($schedulingMail->provider_id);

            // save to audit log and notify changes to phabricator
            $providerName = $objProvider->first_name . ' ' . $objProvider->last_name;
            $msg = 'Appointment for provider #' . $schedulingMail->provider_id;
            $msg.= ' (' . $providerName . ') to ' . $newStatus;
            ApiComponent::audit('U', $msg, 'scheduling_mail', null, false, $accountId);

            if ($newStatus == 'A') {
                // Approve
                $schedulingMail->approve(
                    $accountId,
                    $emrAditionalInformation,
                    $doNotSendEmail,
                    $schedulingMailEhrPatient
                );
            } elseif ($newStatus == 'SEND') {
                // Sent to EMR
                if ($emrAditionalInformation == 1) {
                    // This function uses the $ownerAccountId instad of the current user account
                    $schedulingMail->sendToEhr($ownerAccountId, true);
                }
            } elseif ($newStatus == 'R' && $confirmReject == 1) {
                // Reject
                $schedulingMail->reject($accountId, $emrAditionalInformation, $doNotSendEmail);
            } elseif ($newStatus == 'RE') {
                // Reschedule
                $schedulingMail->reschedule(
                    $accountId,
                    $emrAditionalInformation,
                    $doNotSendEmail,
                    $newData,
                    $schedulingMailEhrPatient
                );
            } elseif ($newStatus == 'S') {
                // No Show
                $schedulingMail->noShow($accountId);
            }

            // create entry in audit log
            AuditLog::create(
                'U',
                'Appointment',
                'scheduling_mail',
                'SchedulingMailId:' . $schedulingMailId . ', New Status:' . $newStatus,
                false
            );
        } else {
            $result['error'] = 'NOTHING_TO_CHANGE';
        }

        return $result;
    }

    /**
     * Get the current New York DateTime
     * @return DateTime $ny_datetime
     */
    public function getCurrentNewYorkDateTime()
    {
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('America/New_York'));
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * Find an appointment basing the search in the Placer ID
     *
     * @param int $installationId
     * @param Appointment $appointment
     * @return Appointment $cpnAppointment
     */
    public function findAppointmentByPlacerId($installationId, $appointment)
    {
        // Find the CpnEhrInstallation
        $cpnEhrInstallation = CpnEhrInstallation::findByCpnInstallationId($installationId);

        // Find the Appointment
        if (isset($appointment->timing) && !empty($appointment->timing->start)) {
            $date = new DateTime($appointment->timing->start);
            $date->setTimezone(new DateTimeZone('America/New_York'));

            $cpnAppointment = Appointment::model()->find(
                'cpn_ehr_installation_id=:installation_id AND ny_start_datetime=:date
                    AND emr_appointment_id=:ehr_appointment_id  AND enabled_flag = 1 AND del_flag = 0',
                array(
                    ':installation_id' => $cpnEhrInstallation->id,
                    ':ehr_appointment_id' => $appointment->placerId,
                    ':date' => $date->format('Y-m-d H:i:s')
                )
            );
        } else {
            $cpnAppointment = Appointment::model()->find(
                'cpn_ehr_installation_id=:installation_id AND emr_appointment_id=:ehr_appointment_id',
                array(
                    ':installation_id' => $cpnEhrInstallation->id,
                    ':ehr_appointment_id' => $appointment->placerId
                )
            );
        }

        return $cpnAppointment;
    }

    /**
     * Creates a new Appointment
     * @param JSON Object $request
     * @param int $installationId
     * @param string $installationIdentifier
     * @param int $queueMessageId
     * @return bool $result
     */
    public function newAppointment($request, $installationId, $installationIdentifier, $queueMessageId = null)
    {
        if (! isset($request->appointment) || empty($request->appointment)) {
            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = 'Missing Appointment attribute - JSON:'.json_encode($request);
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }

        $appointment = $request->appointment;

        $patient = null;
        if (isset($request->patient)) {
            $patient = $request->patient;
        }

        // Find the CpnEhrInstallation
        $cpnEhrInstallation = CpnEhrInstallation::findByCpnInstallationId($installationId);

        // Find the appointment
        $cpnAppointment = Appointment::model()->find(
            "cpn_ehr_installation_id = :cpn_ehr_installation_id
                AND emr_appointment_id = :emr_appointment_id AND status != 'R' AND enabled_flag = 1 AND del_flag = 0",
            array(
                ":cpn_ehr_installation_id" => $cpnEhrInstallation->id,
                ":emr_appointment_id" => $request->appointment->placerId
            )
        );

        if (!empty($cpnAppointment)) {

            // The appointment already exist, log the error
            $errorMsg = 'Error creating the new appointment';
            $errorMsg.= ' - Appointment already exist - JSON:'.json_encode($request);

            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = $errorMsg;
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }

        $cpnAppointment  = new Appointment();
        $cpnAppointment->status = 'A'; // Approved

        // Get the Matching Installation Location ID
        $cpnInstallationLocationId = CpnInstallationLocation::getLocationIdByEhrLocationId(
            $installationId,
            $appointment->visitInfo->assignedLocation->facility
        );

        // If location doesn't exist
        if (empty($cpnInstallationLocationId)) {
            $cpnInstallationLocation = new CpnInstallationLocation();
            $cpnInstallationLocation->cpn_installation_id = $installationId;
            $cpnInstallationLocation->ehr_location_id = $appointment->visitInfo->assignedLocation->facility;
            $cpnInstallationLocation->ehr_location_data = $appointment->visitInfo->assignedLocation->poc;
            $cpnInstallationLocation->date_added = $this->getCurrentNewYorkDateTime();
            $cpnInstallationLocation->added_by_account_id = '1'; // Tech
            $cpnInstallationLocation->save();

            $cpnInstallationLocationId = $cpnInstallationLocation->id;
        }

        // Get the Doctor Practice ID
        $sql2 = sprintf(
            "SELECT practice_id FROM cpn_installation_practice
              WHERE
                cpn_installation_id = '%s' AND
                cpn_installation_location_id = '%s' ;",
            $installationId,
            $cpnInstallationLocationId
        );
        $doctorPracticeId = Yii::app()->db->createCommand($sql2)->queryScalar();

        // Get the Matching Doctor UserId
        $cpnUserId = CpnInstallationUser::getUserIdByEhrIdentifier(
            $installationId,
            $appointment->personnel[0]->identifierInternal
        );

        $doctorProviderId = '';
        if (!empty($cpnUserId)) {

            $sql2 = sprintf(
                "SELECT provider_id FROM cpn_installation_provider
                  WHERE cpn_installation_user_id= '%s'",
                $cpnUserId
            );

            $doctorProviderId = Yii::app()->db->createCommand($sql2)->queryScalar();

            if (empty($doctorProviderId)) {
                return false;
            }

        } else {
            // Create a new Installation User
            $cpnInstallationUser = new CpnInstallationUser();
            $cpnInstallationUser->cpn_installation_id = $installationId;
            $cpnInstallationUser->ehr_user_id = $appointment->personnel[0]->identifierInternal;
            $cpnInstallationUser->ehr_user_data = $appointment->personnel[0]->lastName;
            $cpnInstallationUser->date_added = $this->getCurrentNewYorkDateTime();
            $cpnInstallationUser->added_by_account_id = '1'; // Tech
            $cpnInstallationUser->save();
        }

        if (empty($doctorProviderId) || empty($doctorPracticeId)) {
            return false;
        }

        $cpnAppointment->provider_id = $doctorProviderId;
        $cpnAppointment->practice_id = $doctorPracticeId;


        // Check if patient data is available
        if (!is_null($patient)) {
            // Get the Matching Doctor PatientId
            $sql = sprintf(
                "SELECT ehr_patient.patient_id as patient_id FROM ehr_patient
                WHERE ehr_patient.ehr_patient_id = '%s' AND
                  cpn_ehr_installation_id = '%s'",
                $patient->identifierInternal,
                $cpnEhrInstallation->id
            );

            $doctorPatientId = Yii::app()->db->createCommand($sql)->queryScalar();

            if ($doctorPatientId) {
                EhrPatient::model()->updatePatient($patient, $doctorPatientId);
                $cpnAppointment->patient_id = $doctorPatientId;
            } else {
                $newPatientId = EhrPatient::model()->newPatient($installationId, $patient);
                if ($newPatientId) {
                    $cpnAppointment->patient_id = $newPatientId;
                }
            }

            $cpnAppointment->first_name = $patient->firstName;
            $cpnAppointment->last_name = $patient->lastName;

            if (isset($patient->cellPhone)) {
                $cpnAppointment->cell_phone = $patient->cellPhone;
            }

            if (isset($patient->homeEmail)) {
                $cpnAppointment->email = $patient->homeEmail;
            } else {
                if (isset($patient->workEmail)) {
                    $cpnAppointment->email = $patient->workEmail;
                }
            }
        }

        $cpnAppointment->cpn_ehr_installation_id = $cpnEhrInstallation->id;
        $cpnAppointment->emr_appointment_id = $appointment->placerId;

        // Find the Practice
        $practice = Practice::model()->findByPk($doctorPracticeId);
        $arrTimezone = $practice->getTimeZone();

        $dateStart = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $appointment->timing->start, new DateTimeZone('UTC'));
        $dateEnd = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $appointment->timing->end, new DateTimeZone('UTC'));
        $dateStart->setTimeZone(new DateTimeZone($arrTimezone['phpTimezone']));
        $dateEnd->setTimeZone(new DateTimeZone($arrTimezone['phpTimezone']));

        $cpnAppointment->appointment_date = $dateStart->format('Y-m-d');
        $cpnAppointment->time_start = $dateStart->format('H:i:s');
        $cpnAppointment->time_end = $dateEnd->format('H:i:s');

        $dateStart->setTimezone(new DateTimeZone('America/New_York'));
        $cpnAppointment->ny_start_datetime = $dateStart->format('Y-m-d H:i:s');

        if (isset($appointment->appointmentReason->description)) {
            $cpnAppointment->reason = $appointment->appointmentReason->description;
        }

        // Set the date_added and date_updated attributes with current date and Time
        $cpnAppointment->date_added = date('Y-m-d H:i:s');
        $cpnAppointment->date_updated = date('Y-m-d H:i:s');

        $this->attachQueueMessageId($cpnAppointment, $queueMessageId);

        // Validate the Appointment Model before saving it
        if ($cpnAppointment->validate()) {
            $cpnAppointment->save();
            // The saving was successful
            return true;
        } else {
            // Error validating the appointment, save the errors detected in ErrorLog Table
            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = 'Error validating the appointment:'.json_encode($cpnAppointment->getErrors());
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }
    }

    /**
     * Updates / Creates (in Dentrix most of the times we don't have new appointment messages) an Appointment
     * @param JSON Object $request
     * @param int $installationId
     * @param string $installationIdentifier
     * @param JSON Object $appointment
     * @param JSON Object $patient
     * @param int $queueMessageId
     * @return bool $result
     */
    public function updateAppointment(
        $request,
        $installationId,
        $installationIdentifier,
        $appointment,
        $patient,
        $queueMessageId = null
    ) {
        // Appointment data is required
        if (!isset($request->appointment) || empty($request->appointment) || !isset($request->appointment->visitInfo)) {
            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = 'Missing Appointment attribute - JSON:'.json_encode($request);
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }

        $cpnEhrInstallation = CpnEhrInstallation::findByCpnInstallationId($installationId);

        // Look for the appointment by placerID and status not canceled
        $cpnAppointment = $this->find(
            "cpn_ehr_installation_id = :cpn_ehr_installation_id
                AND emr_appointment_id = :emr_appointment_id AND status != 'R' AND enabled_flag = 1 AND del_flag = 0",
            array(
                ":cpn_ehr_installation_id" => $cpnEhrInstallation->id,
                ":emr_appointment_id" => $appointment->placerId
            )
        );

        // If appointment is not found/canceled, create a new appointment
        if (!$cpnAppointment) {
            return $this->newAppointment($request, $installationId, $installationIdentifier, $queueMessageId);
        }

        // Get the Matching Installation Location ID
        $cpnInstallationLocationId = CpnInstallationLocation::getLocationIdByEhrLocationId(
            $installationId,
            $appointment->visitInfo->assignedLocation->facility
        );

        // If location doesn't exist
        if (empty($cpnInstallationLocationId)) {
            return false;
        }

        $sql2 = sprintf(
            "SELECT practice_id FROM cpn_installation_practice
              WHERE cpn_installation_id = '%s'
                AND cpn_installation_location_id = '%s';",
            $installationId,
            $cpnInstallationLocationId
        );
        $doctorPracticeId = Yii::app()->db->createCommand($sql2)->queryScalar();

        // If PracticeId not found for this installation
        if (empty($doctorPracticeId)) {

            $errorMsg = 'Error updating appointment';
            $errorMsg.= '- PracticeId not found for this installation - JSON:'.json_encode($request);

            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = $errorMsg;
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();

            Yii::log(
                'Failed to save direct appointment: '
                    . 'Data: ' . serialize($request) . ' - '
                    . 'Errors: ' . 'PracticeId not found for this installation. Appointment couldnt be completed',
                CLogger::LEVEL_ERROR,
                'Companion'
            );

            return false;
        } else {
            $cpnAppointment->practice_id = $doctorPracticeId;
        }

        // Get the Matching Doctor UserId
        $cpnUserId = CpnInstallationUser::getUserIdByEhrIdentifier(
            $installationId,
            $appointment->personnel[0]->identifierInternal
        );

        $doctorInstallationProviderId = '';
        if (!empty($cpnUserId)) {
            $sql2 = sprintf(
                "SELECT provider_id FROM cpn_installation_provider
                  WHERE cpn_installation_user_id= '%s'",
                $cpnUserId
            );

            $doctorInstallationProviderId = Yii::app()->db->createCommand($sql2)->queryScalar();

            if (empty($doctorInstallationProviderId)) {
                $cpnErrorLog = new CpnErrorLog();
                $cpnErrorLog->error_text = 'Error updating appointment'
                    . ' - DoctorInstallationProviderId Not Found - JSON:'.json_encode($request);
                $cpnErrorLog->installation_identifier = $installationIdentifier;
                $cpnErrorLog->save();
                return false;
            }

        } else {
            // If ProviderId not found, return (this is a reschedule, ProviderId should exist, it must be an error)
            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = 'Error updating appointment'
                . '- CpnUserId Not Found (this is a reschedule, ProviderId should exist, it must be an error)'
                . ' - JSON:'.json_encode($request);
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }

        $cpnAppointment->provider_id = $doctorInstallationProviderId;

        $doctorPatientId = '';
        if (isset($cpnAppointment->patient_id)) {
            $doctorPatientId = $cpnAppointment->patient_id;
        }

        // Check if patient data is available
        if (! is_null($patient)) {
            // If the patient exist in Doctor, update his information
            if ($doctorPatientId) {
                EhrPatient::model()->updatePatient($patient, $doctorPatientId);
                $cpnAppointment->patient_id = $doctorPatientId;
            } else {
                // Patient doesn't exist (it may have failed creating the patient
                // before because of dirty or incomplete data), try to create it
                $newPatientId = EhrPatient::model()->newPatient($installationId, $patient);
                if ($newPatientId) {
                    $cpnAppointment->patient_id = $newPatientId;
                }
            }

            $cpnAppointment->first_name = $patient->firstName;
            $cpnAppointment->last_name = $patient->lastName;

            if (isset($patient->cellPhone)) {
                $cpnAppointment->cell_phone = $patient->cellPhone;
            }

            if (isset($patient->homeEmail)) {
                $cpnAppointment->email = $patient->homeEmail;
            } else {
                if (isset($patient->workEmail)) {
                    $cpnAppointment->email = $patient->workEmail;
                }
            }
        }

        $cpnEhrInstallation = CpnEhrInstallation::findByCpnInstallationId($installationId);

        $cpnAppointment->cpn_ehr_installation_id = $cpnEhrInstallation->id;

        $practice = Practice::model()->findByPk($doctorPracticeId);
        $arrTimezone = $practice->getTimeZone();

        $dateStart = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $appointment->timing->start, new DateTimeZone('UTC'));
        $dateEnd = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $appointment->timing->end, new DateTimeZone('UTC'));
        $dateStart->setTimeZone(new DateTimeZone($arrTimezone['phpTimezone']));
        $dateEnd->setTimeZone(new DateTimeZone($arrTimezone['phpTimezone']));

        $cpnAppointment->appointment_date = $dateStart->format('Y-m-d');
        $cpnAppointment->time_start = $dateStart->format('H:i:s');
        $cpnAppointment->time_end = $dateEnd->format('H:i:s');

        $dateStart->setTimezone(new DateTimeZone('America/New_York'));
        $cpnAppointment->ny_start_datetime = $dateStart->format('Y-m-d H:i:s');

        if (isset($appointment->appointmentReason->description)) {
            $cpnAppointment->reason = $appointment->appointmentReason->description;
        }

        $cpnAppointment->date_updated = date('Y-m-d H:i:s');

        if (isset($appointment->appointmentReason->description)) {
            $cpnAppointment->reason = $appointment->appointmentReason->description;
        }

        $this->attachQueueMessageId($cpnAppointment, $queueMessageId);

        if ($cpnAppointment->validate()) {
            $cpnAppointment->save();
            return true;
        } else {
            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = 'Error updating appointment:'.json_encode($cpnAppointment->getErrors());
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }
    }

    /**
     * Cancel an Appointment
     * @param int $installationId
     * @param string $installationIdentifier
     * @param JSON Object $appointment
     * @param JSON Object $patient
     * @param int $queueMessageId
     * @return bool result
     */
    public function cancelAppointment(
        $installationId,
        $installationIdentifier,
        $appointment,
        $patient,
        $queueMessageId = null
    ) {
        if (empty($appointment)) {
            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = 'Missing Appointment attribute';
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }

        if (isset($appointment->placerId)) {
            // Find the Appointment to Cancel
            $cpnAppointment = $this->findAppointmentByPlacerId($installationId, $appointment);

            if (!empty($cpnAppointment)) {
                $this->attachQueueMessageId($cpnAppointment, $queueMessageId);
                $cpnAppointment->status = 'R'; // Rejected

                return $cpnAppointment->save();
            }
            // if the appointment could´t be found, nothing to do then return true
            return true;
        }

        return false;
    }

    /**
     * Reschedule an Appointment
     * @param JSON Object message $request
     * @param int $installationId
     * @param string $installationIdentifier
     * @param int $queueMessageId
     * @return bool result
     */
    public function rescheduleAppointment($request, $installationId, $installationIdentifier, $queueMessageId = null)
    {
        if (! isset($request->appointment) || empty($request->appointment)) {
            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = 'Missing Appointment attribute - JSON:'.json_encode($request);
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }

        $appointment = $request->appointment;

        // Find the appointment to reschedule
        $cpnAppointment = null;
        if (isset($request->patient) && ! empty($request->patient)) {
            $cpnAppointment = $this->findAppointmentByProviderPatientPractice(
                $installationId,
                $installationIdentifier,
                $appointment,
                $request->patient
            );
        }

        if (!empty($cpnAppointment)) {
            $cpnAppointment->previous_date = $cpnAppointment->appointment_date;
            $cpnAppointment->previous_time_start = $cpnAppointment->time_start;
            $cpnAppointment->previous_time_end = $cpnAppointment->time_end;

            $date = new DateTime($appointment->timing->start);
            $date->setTimezone(new DateTimeZone('America/New_York'));
            $cpnAppointment->appointment_date = $date->format('Y-m-d');
            $cpnAppointment->time_start = $date->format('H:i:s');

            // SUM the elapsed minutes and hours to Appointment start time
            $min = '+'.$appointment->duration->totalMinutes.' minutes';
            $hour = '+'.$appointment->duration->totalHours.' hours';
            $time_end = $date;
            $time_end = $time_end->modify($hour);
            $time_end = $time_end->modify($min);
            $cpnAppointment->time_end = $time_end->format('H:i:s');

            $this->attachQueueMessageId($cpnAppointment, $queueMessageId);

            $cpnAppointment->save();
            return true;
        } else {
            // Insert new Appointment and send error message
            $cpnMessageType = CpnMessageType::model()->find("id = :id", array(":id" => '6'));

            $cpnCommunicationQueue = new CpnCommunicationQueue();
            $cpnCommunicationQueue->guid = Yii::app()->db->createCommand("SELECT uuid()")->queryScalar();
            $cpnCommunicationQueue->cpn_installation_id = $installationId;
            $cpnCommunicationQueue->cpn_message_type_id = $cpnMessageType->id;
            $cpnCommunicationQueue->message = $cpnMessageType->name;
            $cpnCommunicationQueue->url = 'https://www.doctor.com/';
            $cpnCommunicationQueue->title = 'Failed to Reschedule Appointment - Not found';
            $cpnCommunicationQueue->seconds = '0';
            $cpnCommunicationQueue->added_by_account_id = '1'; // Tech
            $cpnCommunicationQueue->save();

            $this->newAppointment($request, $installationId, $installationIdentifier, $cpnCommunicationQueue->id);
        }
        return false;
    }

    /**
     * Block a Schedule
     * @param int $installationId
     * @param string $installationIdentifier
     * @param JSON Object appointment $appointment
     * @return bool result
     */
    public function blockSchedule($installationId, $installationIdentifier, $appointment)
    {
        if (empty($appointment)) {

            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = 'Error Unblocking schedule - Appointment is empty:';
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }

        // Get the Matching Installation Location ID
        $cpnInstallationLocationId = CpnInstallationLocation::getLocationIdByEhrLocationId(
            $installationId,
            $appointment->visitInfo->assignedLocation->facility
        );

        // if location exist
        $practiceId = null;
        if (!empty($cpnInstallationLocationId)) {
            $sql2 = sprintf(
                "SELECT practice_id FROM cpn_installation_practice
                  WHERE cpn_installation_id = '%s' AND cpn_installation_location_id = '%s' ;",
                $installationId,
                $cpnInstallationLocationId
            );
            $practiceId = Yii::app()->db->createCommand($sql2)->queryScalar();
        }

        if (empty($practiceId)) {
            return false;
        }

        // If the personnel data is available, block it only for the given provider
        if (isset($appointment->personnel[0]->identifierInternal)) {

            // Get the Matching Doctor UserId
            $cpnUserId = CpnInstallationUser::getUserIdByEhrIdentifier(
                $installationId,
                $appointment->personnel[0]->identifierInternal
            );

            if (!empty($cpnUserId)) {
                $sql2 = sprintf(
                    "SELECT provider_id FROM cpn_installation_provider
                      WHERE cpn_installation_user_id= '%s'",
                    $cpnUserId
                );
                $providerIds = Yii::app()->db->createCommand($sql2)->queryColumn();
            } else {
                return false;
            }

        } else {

            // Get all providers for the current practice (checking against provider_practice)
            $sql2 = sprintf(
                "SELECT
                    cpn_installation_provider.provider_id
                FROM
                    cpn_installation_provider
                INNER JOIN
                    provider_practice
                        ON cpn_installation_provider.provider_id = provider_practice.provider_id
                            AND provider_practice.practice_id = %d
                WHERE
                    cpn_installation_provider.cpn_installation_id = %d",
                $practiceId,
                $installationId
            );
            $providerIds = Yii::app()->db->createCommand($sql2)->queryColumn();
        }

        // Get practice local time zone
        $practice = Practice::model()->findByPk($practiceId);
        $arrTimezone = $practice->getTimeZone();

        $dateTimeStart = new DateTime($appointment->timing->start);
        $dateTimeStart->setTimeZone(new DateTimeZone($arrTimezone['phpTimezone']));

        $dateTimeEnd = new DateTime($appointment->timing->end);
        $dateTimeEnd->setTimeZone(new DateTimeZone($arrTimezone['phpTimezone']));

        $dateStart = $dateTimeStart->format('Y-m-d');
        $timeStart = $dateTimeStart->format('H:i:s');
        $dateEnd =  $dateTimeEnd->format('Y-m-d');
        $timeEnd = $dateTimeEnd->format('H:i:s');

        // Get matching schedule exceptions for each provider
        foreach ($providerIds as $providerId) {

            $ese = EmrScheduleException::model()->find(
                "provider_id = :provider_id AND practice_id = :practice_id
                    AND date_start = :date_start AND date_end = :date_end
                    AND enabled_flag = 1 AND del_flag = 0",
                array(
                    ":provider_id" => $providerId,
                    ":practice_id" => $practiceId,
                    ":date_start" => $dateStart,
                    ":date_end" => $dateEnd
                )
            );


            if (!$ese) {
                $ese = new EmrScheduleException;
                $ese->provider_id = $providerId;
                $ese->practice_id = $practiceId;
                $ese->date_start = $dateStart;
                $ese->date_end = $dateEnd;
                $ese->day_number = 0;
                $ese->enabled_flag = 1;
                $ese->del_flag = 0;
                $ese->date_added = date('Y-m-d H:i:s');
                if (!$ese->save()) {
                    return $ese->getErrors();
                }
            }

            // Get matching schedule hour exception
            $eseh = EmrScheduleExceptionHour::model()->find(
                "emr_schedule_exception_id = :emr_schedule_exception_id AND placer_id = :placer_id
                    AND enabled_flag = 1 AND del_flag = 0",
                array(":emr_schedule_exception_id" => $ese->id, ":placer_id" => $appointment->placerId));

            // If hour exception doesn't exist create it
            if (!$eseh) {
                $eseh = new EmrScheduleExceptionHour;
                $eseh->emr_schedule_exception_id = $ese->id;
                $eseh->placer_id = $appointment->placerId;
                $eseh->enabled_flag = 1;
                $eseh->del_flag = 0;
            }

            $eseh->time_start = $timeStart;
            $eseh->time_end = $timeEnd;
            if (!$eseh->save()) {
                return $eseh->getErrors();
            }

            // Reload cache
            CapybaraCache::loadScheduleExceptions($practiceId, $providerId);
        }

        return true;
    }

    /**
     * UnBlock Schedule
     * @param int $installationId
     * @param string $installationIdentifier
     * @param JSON Object Appointment $appointment
     * @return bool result
     */
    public function unblockSchedule($installationId, $installationIdentifier, $appointment)
    {
        if (empty($appointment)) {
            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = 'Error Unblocking schedule - Appointment is empty:';
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }

        // Get the Matching Installation Location ID
        $cpnInstallationLocationId = CpnInstallationLocation::getLocationIdByEhrLocationId(
            $installationId,
            $appointment->visitInfo->assignedLocation->facility
        );

        // if location exist
        if (!empty($cpnInstallationLocationId)) {
            $sql2 = sprintf(
                "SELECT practice_id FROM cpn_installation_practice
                  WHERE cpn_installation_id = '%s' AND
                    cpn_installation_location_id = '%s' ;",
                $installationId,
                $cpnInstallationLocationId
            );
            $practiceId = Yii::app()->db->createCommand($sql2)->queryScalar();
        }

        if (empty($practiceId)) {
            return false;
        }

        // If the personnel data is available, block it only for the given provider
        if (isset($appointment->personnel[0]->identifierInternal)) {

            // Get the Matching Doctor UserId
            $cpnUserId = CpnInstallationUser::getUserIdByEhrIdentifier(
                $installationId,
                $appointment->personnel[0]->identifierInternal
            );

            if (!empty($cpnUserId)) {
                $sql2 = sprintf(
                    "SELECT provider_id FROM cpn_installation_provider
                      WHERE cpn_installation_user_id= '%s'",
                    $cpnUserId
                );
                $providerIds = Yii::app()->db->createCommand($sql2)->queryColumn();
            } else {
                return false;
            }

        } else {

            // Get all providers for the current practice
            $sql2 = sprintf(
                "SELECT provider_id FROM cpn_installation_provider WHERE cpn_installation_id = '%s'",
                $installationId
            );
            $providerIds = Yii::app()->db->createCommand($sql2)->queryColumn();

        }

        // Get practice local time zone
        $practice = Practice::model()->findByPk($practiceId);
        $arrTimezone = $practice->getTimeZone();

        $dateTimeStart = new DateTime($appointment->timing->start);
        $dateTimeStart->setTimeZone(new DateTimeZone($arrTimezone['phpTimezone']));

        $dateTimeEnd = new DateTime($appointment->timing->end);
        $dateTimeEnd->setTimeZone(new DateTimeZone($arrTimezone['phpTimezone']));

        $dateStart = $dateTimeStart->format('Y-m-d');
        $dateEnd =  $dateTimeEnd->format('Y-m-d');

        // Get matching schedule exceptions for each provider
        foreach ($providerIds as $providerId) {

            $exceptionDay = EmrScheduleException::model()->find(
                "provider_id = :provider_id AND practice_id = :practice_id
                    AND date_start = :date_start AND date_end = :date_end
                    AND enabled_flag = 1 AND del_flag = 0",
                array(
                    ":provider_id" => $providerId,
                    ":practice_id" => $practiceId,
                    ":date_start" => $dateStart,
                    ":date_end" => $dateEnd
                )
            );

            if ($exceptionDay) {
                // Get matching schedule hour exceptions
                $exceptionHours = EmrScheduleExceptionHour::model()->findAll(
                    "emr_schedule_exception_id = :emr_schedule_exception_id",
                    array(":emr_schedule_exception_id" => $exceptionDay->id)
                );

                // Go through all exceptions for the given day
                foreach ($exceptionHours as $exceptionHour) {
                    // Look for the given Placer ID
                    if ($exceptionHour->placer_id = $appointment->placerId) {
                        $exceptionHour->delete();

                        // If there's no more exceptions for the given day, delete it
                        if (count($exceptionHours) == 1) {
                            $exceptionDay->delete();
                        }
                    }
                }

                // Reload cache
                CapybaraCache::loadScheduleExceptions($practiceId, $providerId);
            }

        }

        return true;
    }

    /**
     * Mark an appointment not to be Shown
     * @param int $installationId
     * @param string $installationIdentifier
     * @param JSON Object appointment $appointment
     * @param JSON Object patient $patient
     * @param int $queueMessageId
     * @return bool result
     */
    public function noShowAppointment(
        $installationId,
        $installationIdentifier,
        $appointment,
        $patient,
        $queueMessageId = null
    ) {
        if (empty($appointment)) {
            $cpnErrorLog = new CpnErrorLog();
            $cpnErrorLog->error_text = 'Missing Appointment attribute';
            $cpnErrorLog->installation_identifier = $installationIdentifier;
            $cpnErrorLog->save();
            return false;
        }

        $cpnAppointment = $this->findAppointmentByProviderPatientPracticeDates(
            $installationId,
            $installationIdentifier,
            $appointment,
            $patient
        );

        if (! empty($cpnAppointment)) {
            $cpnAppointment->no_show = '1';
            $this->attachQueueMessageId($cpnAppointment, $queueMessageId);

            return $cpnAppointment->save();
        }
        return false;
    }

    /**
     * Attach queue message ID to Appointment model
     * @param Appointment $cpnAppointment
     * @param $queueMessageId
     */
    private function attachQueueMessageId(Appointment $cpnAppointment, $queueMessageId)
    {
        if ($queueMessageId) {
            if (is_null($cpnAppointment->cpn_ehr_communication_queue_id)) {
                $cpnAppointment->cpn_ehr_communication_queue_id = $queueMessageId;
            } else {
                $queueMessageIds = explode(',', $cpnAppointment->cpn_ehr_communication_queue_id);
                $queueMessageIds[] = $queueMessageId;
                $cpnAppointment->cpn_ehr_communication_queue_id = implode(',', $queueMessageIds);
            }
        }
    }


    /**
     * Find an appointment basing the search on the Provider, the Patient and the Practice
     * @param int $installationId
     * @param string $installationIdentifier
     * @param JSON Object $appointment
     * @param JSON Object $patient
     * @return Appointment $appointment
     */
    private function findAppointmentByProviderPatientPractice(
        $installationId,
        $installationIdentifier,
        $appointment,
        $patient
    ) {
        // Get the Matching Doctor ProviderId
        $sql = sprintf(
            "SELECT
              cpn_installation_user.id AS cpn_installation_user_id,
              cpn_installation.practice_id AS practice_id,
              cpn_installation_provider.provider_id AS provider_id
            FROM cpn_installation_user
            JOIN cpn_installation
              ON cpn_installation.id = cpn_installation_user.cpn_installation_id
            JOIN cpn_installation_provider
              ON cpn_installation_provider.cpn_installation_id = cpn_installation_user.cpn_installation_id AND
              cpn_installation_provider.cpn_installation_user_id = cpn_installation_user.id
            WHERE
              cpn_installation.installation_identifier= '%s' AND
              cpn_installation_user.ehr_user_id = '%s';",
            $installationIdentifier,
            $appointment->personnel[0]->identifierInternal
        );
        $cpnUserId = Yii::app()->db->createCommand($sql)->queryRow();

        if (!empty($cpnUserId)) {
            $providerId = $cpnUserId['provider_id'];
            $practiceId = $cpnUserId['practice_id'];

            $sql3 = sprintf(
                "SELECT ehr_patient.patient_id AS patient_id FROM ehr_patient
                JOIN cpn_ehr_installation ON ehr_patient.cpn_ehr_installation_id = cpn_ehr_installation.id
                WHERE ehr_patient.ehr_patient_id= '%s' AND cpn_ehr_installation.cpn_installation_id = '%s'",
                $patient->identifier,
                $installationId
            );

            $patientId = Yii::app()->db->createCommand($sql3)->queryScalar();

            return Appointment::model()->find(
                'provider_id=:provider_id AND patient_id=:patient_id AND practice_id=:practice_id ',
                array(':provider_id' => $providerId, ':patient_id' => $patientId, ':practice_id' => $practiceId)
            );
        }
        return null;
    }

    /**
     * Find an appointment basing the search on the Provider, the Patient, the Practice and the Dates
     * @param int $installationId
     * @param string $installationIdentifier
     * @param JSON Object $appointment
     * @param JSON Object $patient
     * @return Appointment $appointment
     */
    private function findAppointmentByProviderPatientPracticeDates(
        $installationId,
        $installationIdentifier,
        $appointment,
        $patient
    ) {
        // Get the Matching Doctor ProviderId
        $sql = sprintf(
            "SELECT
              cpn_installation_user.id AS cpn_installation_user_id,
              cpn_installation.practice_id AS practice_id,
              cpn_installation_provider.provider_id AS provider_id
            FROM cpn_installation_user
            JOIN cpn_installation
              ON cpn_installation.id = cpn_installation_user.cpn_installation_id
            JOIN cpn_installation_provider
              ON cpn_installation_provider.cpn_installation_id = cpn_installation_user.cpn_installation_id AND
              cpn_installation_provider.cpn_installation_user_id = cpn_installation_user.id
            WHERE
              cpn_installation.installation_identifier= '%s' AND
              cpn_installation_user.ehr_user_id = '%s';",
            $installationIdentifier,
            $appointment->personnel[0]->identifierInternal
        );
        $cpnUserId = Yii::app()->db->createCommand($sql)->queryRow();

        // we have the dates, continue the normal flow
        $dateStart = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $appointment->timing->start, new DateTimeZone('UTC'));
        if (empty($dateStart)) {
            // this will break the process below
            Yii::log(
                'Failed to parse starting date: '
                    . 'Date: ' . serialize($appointment->timing->start) . ' - '
                    . 'Errors: ' . serialize(DateTime::getLastErrors()),
                CLogger::LEVEL_ERROR,
                'Companion'
            );
        }

        $dateEnd = DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $appointment->timing->end, new DateTimeZone('UTC'));
        if (empty($dateEnd)) {
            // this will break the process below
            Yii::log(
                'Failed to parse ending date: '
                    . 'Date: ' . serialize($appointment->timing->end) . ' - '
                    . 'Errors: ' . serialize(DateTime::getLastErrors()),
                CLogger::LEVEL_ERROR,
                'Companion'
            );
        }

        if (!empty($cpnUserId) && !empty($dateStart) && !empty($dateEnd)) {
            $providerId = $cpnUserId['provider_id'];
            $practiceId = $cpnUserId['practice_id'];

            $cpnEhrInstallation = CpnEhrInstallation::findByCpnInstallationId($installationId);

            $sql3 = sprintf(
                "SELECT ehr_patient.patient_id AS patient_id FROM ehr_patient
                WHERE ehr_patient_id= '%s' AND cpn_ehr_installation_id = '%s'",
                $patient->identifier,
                $cpnEhrInstallation->id
            );

            $patientId = Yii::app()->db->createCommand($sql3)->queryScalar();

            $practice = Practice::model()->findByPk($practiceId);
            $arrTimezone = $practice->getTimeZone();

            if (empty($appointment->timing->start)) {
                // we don't have the dates (actually times) for the appointment!
                if (!empty($appointment->placerId)) {
                    // we have the placerId, so redirect to the function that looks up based on it
                    return self::findAppointmentByPlacerId($installationId, $appointment);
                } else {
                    // we don't have dates and we don't have the placerId, so we can't look up the appointment
                    return false;
                }
            }

            $dateStart->setTimeZone(new DateTimeZone($arrTimezone['phpTimezone']));
            $dateEnd->setTimeZone(new DateTimeZone($arrTimezone['phpTimezone']));

            $appointment_date = $dateStart->format('Y-m-d');
            $timeStart = $dateStart->format('H:i:s');
            $timeEnd = $dateEnd->format('H:i:s');

            return Appointment::model()->find(
                'provider_id=:provider_id AND patient_id=:patient_id AND practice_id=:practice_id
                    AND enabled_flag = 1 AND del_flag = 0
                    AND appointment_date=:appointment_date AND time_start=:time_start AND time_end=:time_end',
                array(
                    ':provider_id' => $providerId,
                    ':patient_id' => $patientId,
                    ':practice_id' => $practiceId,
                    ':appointment_date' => $appointment_date,
                    ':time_start' => $timeStart,
                    ':time_end' => $timeEnd
                )
            );
        }
        return null;
    }

}
