<?php

Yii::import('application.modules.core_models.models._base.BaseProviderRatingSurvey');

class ProviderRatingSurvey extends BaseProviderRatingSurvey
{

    /**
     * @var array partner site round robin
     */
    protected $partnerSiteRotation = [
        46, // Healthgrades
        46, // Healthgrades
        46, // Healthgrades
        111, // Vitals + WebMD
        111, // Vitals + WebMD
        46, // Healthgrades
        23, // Wellness
        2, // Healthline CareFinder
    ];

    /**
     * @var int Index of the selected partner site from rotation array
     */
    protected $partnerSiteIndex = null;

    /**
     * {@inheritdoc}
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeValidate()
    {
        if (empty($this->date_added)) {
            $this->date_added = date('Y-m-d H:i:s');
        }

        return parent::beforeValidate();
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave()
    {
        if (empty($this->listing_distribution)) {
            $this->listing_distribution = $this->getDistrutionType();
        }

        // set the partner site if it's empty
        if (empty($this->listing_partner_site_id) && $this->listing_distribution == 'Optimized') {
            $this->partnerSiteIndex = $this->getNextPartnerSiteIndex();
            $this->listing_partner_site_id = $this->partnerSiteRotation[$this->partnerSiteIndex];
        } else {
            $this->partnerSiteIndex = null;
        }

        return parent::beforeSave();
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave()
    {

        // save to idp entity match only if an index was setted
        if ($this->partnerSiteIndex !== null) {
            $this->updatePartnerSiteIndex();
        }

        // send to HG if applicable
        if (Yii::app()->params['environment'] == 'prod'
            && (
                $this->listing_partner_site_id == 46 ||
                (empty($this->listing_partner_site_id) && $this->listing_distribution == 'All-Sites')
            )
        ) {
            SqsComponent::sendMessage('propagateSurveyToHealthgrades', $this->id);
        }

        return parent::afterSave();
    }

    /**
     * Get the distribution type 'Optimized','All-Sites'
     *
     * @return string
     */
    protected function getDistrutionType()
    {
        $distributionType = Yii::app()->analytics->createCommand(
            "SELECT
                distribution_type
            FROM
                bin_syndication.syndication_reviews
                INNER JOIN bin_syndication.syndication_config
                    ON syndication_reviews.bf_client_id = bin_syndication.syndication_config.bf_client_id
            WHERE
                review_id = :review_id"
        )->bindValue(':review_id', $this->review_id, PDO::PARAM_STR)->queryScalar();

        if (empty($distributionType)) {
            return 'Optimized';
        }

        return $distributionType;
    }

    /**
     * Return the next partner site to store the rating survey id.
     *
     * @return int
     */
    protected function getNextPartnerSiteIndex()
    {
        // get the last position
        $key = 'pg_survey_round_robin_' . $this->pg_client_id . '_index';
        $gp = GlobalParameter::model()->find('`key` = :key', [':key' => $key]);

        // create the parameter if it doesn't exists, will have -1 to start from zero
        if (empty($gp)) {
            $gp = new GlobalParameter();
            $gp->key = $key;
            $gp->value = -1;
            $gp->save();
        }

        $index = $gp->value;

        $count = count($this->partnerSiteRotation) - 1;

        // increase or start again
        if ($index < $count) {
            ++$index;
        } else {
            $index = 0;
        }

        // return the index
        return $index;
    }

    /**
     * Save the partner site index for the next iteration.
     *
     * @param int $index
     */
    protected function updatePartnerSiteIndex()
    {
        $key = 'pg_survey_round_robin_' . $this->pg_client_id . '_index';
        $gp = GlobalParameter::model()->find('`key` = :key', [':key' => $key]);

        if (empty($gp)) {
            $gp = new GlobalParameter();
            $gp->key = $key;
        }

        $gp->value = $this->partnerSiteIndex;
        $gp->save();
    }

}
