<?php

Yii::import('application.modules.core_models.models._base.BaseCommunicationLog');

class CommunicationLog extends BaseCommunicationLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'id';
    }

    /**
     * Set dates automatically if needed
     */
    public function beforeSave()
    {
        if (empty($this->date_added) || $this->date_added == '0000-00-00 00:00:00') {
            $this->date_added = date('Y-m-d H:i:s');
        }

        if (empty($this->date_updated) || $this->date_updated == '0000-00-00 00:00:00') {
            $this->date_updated = date('Y-m-d H:i:s');
        }

        return parent::beforeSave();
    }

    /**
     * Transform a JSON encoded recipient string into a human readable string
     * @param string $sentTo
     * @return string
     */
    public static function formatSentTo($sentTo)
    {
        $to = [];
        $jsonTo = json_decode($sentTo);

        if (!is_array($jsonTo)) {
            // this was likely a phone number, return as it was
            return $sentTo;
        }

        foreach ($jsonTo as $recipient) {
            $to[] = ucfirst($recipient->type) . ': ' . $recipient->email;
        }
        return implode(PHP_EOL . ', ', $to);
    }

    /**
     * Get the ReviewHub Reports that were sent to an account (by its name or email address)
     * @param string $email
     * @return mixed
     */
    public static function getReviewHubReport($email = '')
    {

        if ($email != '') {
            $condition = sprintf("AND sent_to = '[{\"email\":\"%s\",\"type\":\"to\"}]'", $email);
        } else {
            $condition = " AND body LIKE '%weekly reviewhub report%' ";
        }

        // look up the reports for that email address
        $sql = sprintf(
            "SELECT id, is_processed, is_sent,
            communication_type, sent_from,
            REPLACE(REPLACE(sent_to, '\",\"type\":\"to\"}]', ''), '[{\"email\":\"', '') AS sent_to,
            communication_uid, DATE_FORMAT(date_added, '%%m/%%d/%%Y %%h:%%i %%p') AS date_added,
            DATE_FORMAT(date_updated, '%%m/%%d/%%Y %%h:%%i %%p') AS date_updated,
            message_status, communication_template, status_detail, body
            FROM communication_log
            WHERE communication_type = 'MAIL'
            %s
            ORDER BY id DESC
            LIMIT 100;",
            $condition
        );

        $tmpData = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();

        $data = array();
        if (!empty($tmpData) && !empty($email)) {

            // This record is for the: weekly reviewhub report ??
            foreach ($tmpData as $value) {

                $body = json_decode($value['body'], true);

                if (!empty($body[0]['content'])) {
                    $body = $body[0]['content'];
                    if (mb_stristr($body, 'weekly reviewhub report')) {
                        unset($value['body']);
                        $data[] = $value;
                    }
                }
            }
        } else {
            $data = $tmpData;
        }

        return $data;

    }

}
