<?php

Yii::import('application.modules.core_models.models._base.BaseCpnInstallationProvider');

class CpnInstallationProvider extends BaseCpnInstallationProvider
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function newInstallation($installationId)
    {
        // Look if provided installationId already exist
        $cIP = CpnInstallationProvider::model()->find(
            'cpn_installation_id=:installation_id',
            array(
                ':installation_id' => $installationId
            )
        );

        // If InstallationID is not present in DB, we create it
        if (empty($cIP)) {
            // Create new InstallationProvider record
            $cpnInstallationProvider = new CpnInstallationProvider();
            $cpnInstallationProvider->cpn_installation_id = $installationId;
            $cpnInstallationProvider->added_by_account_id = '1'; // ASK FOR THIS VALUE INPUT VARIABLE!
            $cpnInstallationProvider->save();
        }
    }

    /**
     * Get Providers Installation
     *
     * @param int $ownerAccountId
     * @param int $practiceId
     * @param int $cpnInstallationId
     * @return array
     */
    public function getAccountProvidersInstallation($ownerAccountId, $practiceId, $cpnInstallationId)
    {
        // Query
        $sql = sprintf(
            "SELECT DISTINCT
                    provider.id,
                    provider.first_middle_last as provider_name,
                    provider_practice.practice_id,
                    provider_practice.operatories,
                    cpn_installation_user.id AS cpn_user_id,
                    cpn_installation_provider.cpn_installation_user_id AS cpn_installation_user_id,
                    cpn_installation_user.ehr_user_data AS cpn_user_name
                FROM provider
                INNER JOIN account_provider ON account_provider.provider_id= provider.id
                INNER JOIN provider_practice ON provider_practice.provider_id= provider.id
                LEFT JOIN cpn_installation ON cpn_installation.account_id = account_provider.account_id
                    AND cpn_installation.practice_id= provider_practice.practice_id
                    AND cpn_installation.enabled_flag = 1
                    AND cpn_installation.del_flag = 0
                    AND cpn_installation.id = '%s'
                LEFT JOIN cpn_installation_provider
                    ON cpn_installation_provider.provider_id= provider.id
                    AND cpn_installation_provider.cpn_installation_id = cpn_installation.id
                LEFT JOIN cpn_installation_user
                    ON cpn_installation_provider.cpn_installation_user_id = cpn_installation_user.id
                WHERE account_provider.account_id = %s AND provider_practice.practice_id = %s;",
            $cpnInstallationId,
            $ownerAccountId,
            $practiceId
        );

        // Return all the result
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get all providers related to an account
     *
     * @param int $ownerAccountId
     * @param int $cpnInstallationId
     * @return array
     */
    public function getAccountProvidersInstallationV2($ownerAccountId, $cpnInstallationId)
    {
        // Query
        $sql = sprintf(
            "SELECT DISTINCT
                    provider.id,
                    provider.first_middle_last as provider_name,
                    cpn_installation_user.id AS cpn_user_id,
                    cpn_installation_provider.cpn_installation_user_id AS cpn_installation_user_id,
                    cpn_installation_user.ehr_user_data AS cpn_user_name,
                    cpn_installation_provider.scheduling_mail_notification,
                    cpn_installation_provider.twilio_log_notification,
                    cpn_installation_provider.provider_rating_notification
                FROM provider
                INNER JOIN account_provider ON account_provider.provider_id= provider.id
                LEFT JOIN cpn_installation ON cpn_installation.account_id = account_provider.account_id
                    AND cpn_installation.enabled_flag = 1
                    AND cpn_installation.del_flag = 0
                    AND cpn_installation.id = %s
                LEFT JOIN cpn_installation_provider
                    ON cpn_installation_provider.provider_id = provider.id
                    AND cpn_installation_provider.cpn_installation_id = cpn_installation.id
                LEFT JOIN cpn_installation_user
                    ON cpn_installation_provider.cpn_installation_user_id = cpn_installation_user.id
                WHERE account_provider.account_id = %s;",
            $cpnInstallationId,
            $ownerAccountId
        );

        // Return all the result
        return Yii::app()->db->createCommand($sql)->queryAll();
    }
}
