<?php

Yii::import('application.modules.core_models.models._base.BaseType');

class Type extends BaseType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getAllByGroup($groupCode)
    {
        $params = [
            ':group_code' => $groupCode
        ];

        return $this->with('typeGroup')
                ->findAll("typeGroup.code = :group_code", $params);
    }

    public function getScanErrorTypes()
    {
        return $this->getAllByGroup('scan_error');
    }

    public function getScanErrorFieldTypes()
    {
        return $this->getAllByGroup('scan_field');
    }

    public function getDefaultScanErrorFieldTypes()
    {
        $fieldTypes = $this->getAllByGroup('scan_field');
        $defaultTypes = [];

        foreach ($fieldTypes as $fieldType) {
            if ($fieldType->isDefaultForScanError()) {
                $defaultTypes[] = $fieldType;
            }
        }

        return $defaultTypes;
    }

    public function getScanErrorTaskTypes()
    {
        return $this->getAllByGroup('scan_task');
    }

    public function getByCodes($code, $groupCode)
    {
        $params = [
            ':code' => $code,
            ':group_code' => $groupCode
        ];

        return $this->with('typeGroup')
                ->find("t.code = :code AND typeGroup.code = :group_code", $params);
    }

    public function getAzureTaskType()
    {
        return $this->getByCodes('scan_task_azure', 'scan_task');
    }

    public function getSalesforceTaskType()
    {
        return $this->getByCodes('scan_task_salesforce', 'scan_task');
    }

    public function getDefaultScanErrorType()
    {
        return $this->getByCodes('scan_error_uncategorized', 'scan_error');
    }

    public function getMatchedErrorTypes($types)
    {
        $result = [];
        foreach ($types as $type) {
            if ($type->isMatchedError()) {
                $result[] = $type;
            }
        }

        return $result;
    }

    public function getUnmatchedErrorTypes($types)
    {
        $result = [];
        foreach ($types as $type) {
            if ($type->isUnmatchedError()) {
                $result[] = $type;
            }
        }

        return $result;
    }

    public function isMatchedError()
    {
        $matchErrors = ['scan_error_false_negative', 'scan_error_uncategorized'];
        if (in_array($this->code, $matchErrors)) {
            return true;
        }

        return false;
    }

    public function isUnmatchedError()
    {
        return $this->code === 'scan_error_uncategorized' ? true : !$this->isMatchedError();
    }

    public function isDefaultForScanError()
    {
        $defaults = $this->getDefaultCodes();
        return in_array($this->code, $defaults);
    }

    public function getDefaultCodes()
    {
        return [
            'scan_field_name',
            'scan_field_address',
            'scan_field_phone',
            'scan_field_photo',
            'scan_field_specialty',
            'scan_field_booking_button'
        ];
    }

    public function getScanErrorTypesForProviders()
    {
        return [
            'scan_field_booking_button'
        ];
    }

    public function getScanErrorTypesForPractices()
    {
        return [];
    }

    public function isDefaultForAll()
    {
        return (!$this->isProviderOnlyScanError() && !$this->isPracticeOnlyScanError());
    }

    public function isProviderOnlyScanError()
    {
        $codes = $this->getScanErrorTypesForProviders();
        return in_array($this->code, $codes);
    }

    public function isPracticeOnlyScanError()
    {
        $codes = $this->getScanErrorTypesForPractices();
        return in_array($this->code, $codes);
    }

    public function isFalseNegative()
    {
        if ($this->code === 'scan_error_false_negative') {
            return true;
        }

        return false;
    }

    public function isFalsePositive()
    {
        if ($this->code === 'scan_error_false_positive') {
            return true;
        }

        return false;
    }

    public function isFalsey()
    {
        return ($this->isFalsePositive() || $this->isFalseNegative());
    }

}
