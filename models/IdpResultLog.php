<?php

Yii::import('application.modules.core_models.models._base.BaseIdpResultLog');

class IdpResultLog extends BaseIdpResultLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if (empty($this->date_added)) {
            $this->date_added = date('Y-m-d H:i:s');
        }

        if (empty($this->date_updated)) {
            $this->date_updated = date('Y-m-d H:i:s');
        }

        return parent::beforeValidate();
    }

}
