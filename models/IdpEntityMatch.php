<?php

Yii::import('application.modules.core_models.models._base.BaseIdpEntityMatch');

class IdpEntityMatch extends BaseIdpEntityMatch
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if (empty($this->date_added)) {
            $this->date_added = date('Y-m-d H:i:s');
        }

        return parent::beforeValidate();
    }

}
