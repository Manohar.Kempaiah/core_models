<?php

Yii::import('application.modules.core_models.models._base.BaseSpecialtyVisitReason');

class SpecialtyVisitReason extends BaseSpecialtyVisitReason
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * called on rendering the column for each row
     * @return str
     */
    public function getSubSpecialtyTooltip()
    {
        return SubSpecialty::getSubSpecialty($this->sub_specialty_id);
    }

    /**
     * called on rendering the column for each row
     * @return str
     */
    public function getSpecialtyTooltip()
    {
        return Specialty::getSpecialty($this->specialty_id);
    }

    /**
     * called on rendering the column for each row
     * @return str
     */
    public function getVisitReasonTooltip()
    {
        return VisitReason::getVisitReasonName($this->visit_reason_id);
    }

    /**
     * Gets specialties Visit Reason from a given provider id
     * @param int $providerId
     * @return arr
     */
    public static function getProviderSpecialties($providerId = 0)
    {
        if ($providerId == 0) {
            return '';
        }

        $sql = sprintf(
            "SELECT ps.provider_id, 0 AS sub_specialty_id, 'ALL' AS sub_name, ss.specialty_id,
            sp.name AS specialty_name, vr.name AS visit_name, vr.standard_visit_duration AS visit_duration,
            vr.id AS visit_id
            FROM provider_specialty AS ps
            LEFT JOIN sub_specialty AS ss ON ss.id = ps.sub_specialty_id
            LEFT JOIN specialty AS sp ON sp.id = ss.specialty_id
            LEFT JOIN specialty_visit_reason AS svr ON svr.specialty_id = ss.specialty_id
            LEFT JOIN visit_reason AS vr ON svr.visit_reason_id = vr.id
            WHERE ps.provider_id = %d AND vr.id > 0 AND ISNULL(svr.sub_specialty_id)
            ORDER BY specialty_name ASC, sub_name ASC;",
            $providerId
        );
        $forAllSpecialty = Yii::app()->db->createCommand($sql)->queryAll();

        $sql = sprintf(
            'SELECT ps.provider_id, ps.sub_specialty_id, ss.name AS sub_name, ss.specialty_id,
            sp.name AS specialty_name, vr.name AS visit_name, vr.standard_visit_duration AS visit_duration,
            vr.id AS visit_id
            FROM provider_specialty AS ps
            LEFT JOIN sub_specialty AS ss ON ss.id = ps.sub_specialty_id
            LEFT JOIN specialty AS sp ON sp.id = ss.specialty_id
            LEFT JOIN specialty_visit_reason AS svr ON svr.sub_specialty_id = ps.sub_specialty_id
            LEFT JOIN visit_reason AS vr ON svr.visit_reason_id = vr.id
            WHERE ps.provider_id = %d and vr.id > 0
            ORDER BY specialty_name ASC, sub_name ASC;',
            $providerId
        );
        $forSubSpecialty = Yii::app()->db->createCommand($sql)->queryAll();

        return array_merge($forAllSpecialty, $forSubSpecialty);
    }

    /**
     * Gets subSpecialties from a given specialtyId and visitReasonId
     * @param int $specialtyId
     * @param int $visitReasonId
     * @return arr
     */
    public static function getSubSpecialties($specialtyId = 0, $visitReasonId = 0)
    {
        if ($specialtyId == 0 || $visitReasonId==0) {
            return '';
        }
        $sql = sprintf(
            'SELECT *
            FROM specialty_visit_reason AS svr
            WHERE svr.specialty_id = %d AND svr.visit_reason_id=%d ',
            $specialtyId,
            $visitReasonId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }


}
