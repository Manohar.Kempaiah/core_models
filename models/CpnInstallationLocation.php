<?php

Yii::import('application.modules.core_models.models._base.BaseCpnInstallationLocation');

class CpnInstallationLocation extends BaseCpnInstallationLocation
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get Practice Location
     *
     * @param int $cpnInstallationId
     * @param int $practiceId
     * @return array
     */
    public function getPracticeLocation($cpnInstallationId, $practiceId)
    {

        // Query
        $sql = sprintf(
            "SELECT
            cpn_installation_location.id,
            cpn_installation_location.ehr_location_id,
            cpn_installation_location.ehr_location_data,
            cpn_installation_practice.cpn_installation_location_id
            FROM cpn_installation_location
            INNER JOIN cpn_installation_practice
                ON (cpn_installation_location.id= cpn_installation_practice.cpn_installation_location_id
                AND cpn_installation_location.cpn_installation_id= cpn_installation_practice.cpn_installation_id)
            WHERE cpn_installation_location.cpn_installation_id = %s
                AND cpn_installation_practice.practice_id = %s; ",
            $cpnInstallationId,
            $practiceId
        );

        $res = Yii::app()->db->createCommand($sql)->queryAll();

        if (!$res) {

            // Query
            $sql = sprintf(
                "SELECT
                    cpn_installation_location.id,
                    cpn_installation_location.ehr_location_id,
                    cpn_installation_location.ehr_location_data
                FROM cpn_installation_location
                LEFT JOIN cpn_installation_practice
                    ON (cpn_installation_location.id= cpn_installation_practice.cpn_installation_location_id
                    AND cpn_installation_location.cpn_installation_id=cpn_installation_practice.cpn_installation_id)
                WHERE cpn_installation_location.cpn_installation_id = %s
                AND cpn_installation_practice.id IS NULL; ",
                $cpnInstallationId
            );

            $res = Yii::app()->db->createCommand($sql)->queryAll();
        }

        // Return all the result
        return $res;
    }

    /**
     * Get Companion Locations by Installation Id
     *
     * @param int $cpnInstallationId
     * @return array
     */
    public static function getLocationsByInstallationId($cpnInstallationId)
    {
        $sql = sprintf(
            "SELECT id,
                ehr_location_data AS name
            FROM cpn_installation_location
            WHERE cpn_installation_id = %s; ",
            $cpnInstallationId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get LocationID using the EHR Location ID
     * @param $cpnInstallationId
     * @param $ehrLocationId
     * @return mixed
     */
    public static function getLocationIdByEhrLocationId($cpnInstallationId, $ehrLocationId)
    {
        $sql = sprintf(
            "SELECT cpn_installation_location.id AS cpn_installation_location_id
                FROM cpn_installation_location
                WHERE cpn_installation_id= '%s'
                AND cpn_installation_location.ehr_location_id='%s';",
            $cpnInstallationId,
            $ehrLocationId
        );
        return  Yii::app()->db->createCommand($sql)->queryScalar();
    }

}
