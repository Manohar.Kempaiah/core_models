<?php

Yii::import('application.modules.core_models.models._base.BaseProviderCertification');

class ProviderCertification extends BaseProviderCertification
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row in Admin
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id .
                '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider .
                '">' . $this->provider . '</a>');
    }

    /**
     * Returns details of the certificates from a given provider
     * @param string $providerId id of the provider
     * @return array
     */
    public static function getDetails($providerId, $cache = false)
    {

        $sql = sprintf(
            "SELECT pc.id, pc.provider_id, pc.certifying_body_id, pc.certification_type_id,
            pc.certification_sub_specialty_id, pc.certification_specialty_id,
            pc.year_achieved, pc.certification_type_id_backup,
            CONCAT(certifying_body.name, ' (', certifying_body.abbreviation, ')') AS certifying_body,
            certification_type.name AS certification_type,
            certification_specialty.name AS specialty,
            certification_sub_specialty.name AS sub_specialty
            FROM provider_certification as pc
            INNER JOIN certifying_body
            ON pc.certifying_body_id = certifying_body.id
            INNER JOIN certification_type
            ON pc.certification_type_id = certification_type.id
            LEFT JOIN certification_specialty
            ON pc.certification_specialty_id = certification_specialty.id
            LEFT JOIN certification_sub_specialty
            ON pc.certification_sub_specialty_id = certification_sub_specialty.id
            WHERE pc.provider_id = %d;",
            $providerId
        );

        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Check if this certification exist for provider
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->certification_sub_specialty_id == 0) {
            $this->certification_sub_specialty_id = null;
        }

        if ($this->certification_specialty_id == 0) {
            $this->certification_specialty_id = null;
        }

        if ($this->isNewRecord) {

            if (is_null($this->year_achieved)) {
                $this->year_achieved = '';
            }

            $alreadyExists = ProviderCertification::model()->exists(
                'provider_id=:provider_id AND
                certification_type_id=:certification_type_id AND
                certifying_body_id=:certifying_body_id AND
                certification_specialty_id=:certification_specialty_id AND
                certification_sub_specialty_id=:certification_sub_specialty_id AND
                year_achieved=:year_achieved',
                array(
                    ':provider_id' => $this->provider_id,
                    ':certification_type_id' => $this->certification_type_id,
                    ':certifying_body_id' => $this->certifying_body_id,
                    ':certification_specialty_id' => $this->certification_specialty_id,
                    ':certification_sub_specialty_id' => $this->certification_sub_specialty_id,
                    ':year_achieved' => $this->year_achieved
                )
            );
            if (!empty($alreadyExists)) {
                return false;
            }
        }

        return parent::beforeSave();
    }

    /**
     * Count certified providers in a city practicing a specialty (optionally accepting an insurance)
     * @param int $cityId
     * @param int $specialtyId
     * @param int $insuranceId
     * @return int
     */
    public function countCertifiedByCitySpecialtyInsurance($cityId, $specialtyId, $insuranceId = null)
    {
        $criteria['practice.city_id'] = $cityId;
        $criteria['specialty.specialty_id'] = $specialtyId;
        if ($insuranceId) {
            $criteria['insurance.company_id'] = $insuranceId;
        }

        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Returns quantity of certified providers from a list of subspecialties and an insurance
     * @param int $specialtyId
     * @param int $insuranceId
     * @return int
     */
    public function countCertifiedBySpecialtyInsurance($specialtyId, $insuranceId = null)
    {

        $criteria['specialty.specialty_id'] = $specialtyId;
        if ($insuranceId) {
            $criteria['insurance.company_id'] = $insuranceId;
        }

        return DoctorIndex::count($criteria);
    }

    /**
     * Count providers who are certified in a state and specialty, and optionally an insurance
     * @param int $stateId
     * @param int $specialtyId
     * @param int $insuranceId
     * @return int
     */
    public function countCertifiedByStateSpecialtyInsurance($stateId, $specialtyId, $insuranceId = null)
    {
        $criteria['practice.state_id'] = $stateId;
        $criteria['specialty.specialty_id'] = $specialtyId;
        if ($insuranceId) {
            $criteria['insurance.company_id'] = $insuranceId;
        }

        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Save Data
     * @param string $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        foreach ($postData as $data) {
            if (isset($data['backend_action']) && $data['backend_action'] == 'delete' && $data['id'] > 0) {
                $model = ProviderCertification::model()->findByPk($data['id']);
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_CERTIFICATION";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }
                    $auditSection = 'ProviderCertification #' . $providerId . ' (#' . $model->certifying_body_id . ')';
                    AuditLog::create('D', $auditSection, 'provider_certification', null, false);
                }
            } else {

                if ($data['id'] > 0) {
                    $model = ProviderCertification::model()->findByPk($data['id']);
                    $auditAction = 'U';
                } else {
                    $auditAction = 'C';
                    $model = new ProviderCertification();
                }
                if (empty($data['certification_type_id'])) {
                    $data['certification_type_id'] = 1;
                }
                $model->attributes = $data;
                $model->provider_id = $providerId;

                if ($model->year_achieved < 1900) {
                    return array(
                        'success' => false,
                        'error' => 'ERROR_SAVING_PROVIDER_CERTIFICATION',
                        'data' => 'The year achieved is invalid, please use a valid year'
                    );
                }

                if ($model->year_achieved == 'yyyy') {
                    $model->year_achieved = null;
                }
                if (!$model->save()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_CERTIFICATION";
                    $results['data'] = $model->getErrors();
                    return $results;
                } else {
                    if ($auditAction == 'C') {
                        // return the new item
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                }
                $auditSection = 'ProviderCertification #' . $providerId . ' (#' . $model->certifying_body_id . ')';
                AuditLog::create($auditAction, $auditSection, 'provider_certification', null, false);
            }
        }
        return $results;
    }

}
