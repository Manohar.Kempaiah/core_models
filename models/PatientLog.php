<?php

Yii::import('application.modules.core_models.models._base.BasePatientLog');

class PatientLog extends BasePatientLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search($pageSize = 10)
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('event_name', $this->event_name, true);
        $criteria->compare('date', $this->date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => $pageSize),
            'sort' => array(
                'defaultOrder' => 'id DESC',
            ),
        ));
    }

    /**
     * Export to Csv
     *
     * @param obj $search
     */
    public static function exportCsv($search)
    {

        ini_set('max_execution_time', 0);

        $fp = fopen('php://temp', 'w');

        $arraySearch = (array) $search;

        if (is_array($arraySearch)) {

            @$arrKeys = array_keys($arraySearch[0]->attributes);

            if (!empty($arrKeys)) {

                $arrExport = array();

                foreach ($arrKeys as $key => $value) {

                    $arrExport[$key] = str_replace("_", " ", $value);
                }

                fputcsv($fp, $arrExport);

                foreach ($arraySearch as $key => $objSearch) {

                    fputcsv($fp, $objSearch->attributes);
                }
            }
        }

        rewind($fp);
        Yii::app()->request->sendFile('Exception_File_' . date("m_d_Y H_i") . '.csv', stream_get_contents($fp));
        fclose($fp);
    }

}
