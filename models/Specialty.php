<?php

Yii::import('application.modules.core_models.models._base.BaseSpecialty');

class Specialty extends BaseSpecialty
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Add friendly_url if necessary
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->id > 0) {

            // Validate level and sub-specialties
            if (!empty($this->oldRecord) && $this->oldRecord->level != $this->level) {
                $subSpecialty = SubSpecialty::model()->findAll(
                    'specialty_id=:specialty_id',
                    array(':specialty_id' => $this->id)
                );
                foreach ($subSpecialty as $sub) {

                    $hasAssociatedRecords = ProviderSpecialty::model()->exists(
                        'sub_specialty_id=:sub_specialty_id',
                        array(':sub_specialty_id' => $sub['id'])
                    );

                    if ($hasAssociatedRecords) {
                        $this->addError(
                            "level",
                            "Level cannot be changed if there are sub-specialties associated with a provider/practice."
                        );
                        return false;
                    }

                    $hasAssociatedRecords = PracticeSpecialty::model()->exists(
                        'sub_specialty_id=:sub_specialty_id',
                        array(':sub_specialty_id' => $sub['id'])
                    );

                    if ($hasAssociatedRecords) {
                        $this->addError(
                            "level",
                            "Level cannot be changed if there are sub-specialties associated with a provider/practice."
                        );
                        return false;
                    }
                }
            }
            return parent::beforeSave();
        } elseif ($this->friendly_url == '') {
            if ($this->name_plural != '') {
                $this->friendly_url = $this->getFriendlyUrl($this->name_plural);
            } else {
                $this->friendly_url = $this->getFriendlyUrl($this->name);
            }
        }
        return parent::beforeSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from specialty if associated records exist in sub_specialty table
        $hasAssociatedRecords = SubSpecialty::model()->exists(
            'specialty_id=:specialty_id',
            array(':specialty_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError("id", "Specialty can't be deleted because there are sub-specialties linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Find sub-specialty IDs by matching against subspecialty and specialty name
     * Note that search by specialty name has higher priority than subspecialty,
     * since search by subspecialty sends its ID when selecting so we wouldn't
     * reach this code. Also autocomplete filters by spec only and not sub.
     * @param string $q
     * @return array
     */
    public static function findByName($q)
    {

        $q = addslashes($q);

        $connection = Yii::app()->db;

        $sql = sprintf(
            "SELECT sub_specialty.id
            FROM specialty
            INNER JOIN sub_specialty
            ON specialty.id = sub_specialty.specialty_id
            WHERE specialty.name = '%s';",
            $q
        );
        $assocArray = $connection->cache(7 * Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();

        if (!$assocArray) {
            $sql = sprintf("SELECT id FROM sub_specialty WHERE name = '%s';", $q);
            $assocArray = $connection->cache(7 * Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
        }

        if (!$assocArray) {
            $sql = sprintf(
                "SELECT sub_specialty.id AS id
                FROM sub_specialty
                INNER JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
                WHERE sub_specialty.name LIKE '%s%%'
                OR specialty.name LIKE '%s'
                OR specialty.name_plural = '%s'
                OR CONCAT(specialty.name, ' - ', sub_specialty.name) = '%s'
                ORDER BY specialty.name = '%s' DESC;",
                $q,
                $q,
                $q,
                $q,
                $q
            );
            $assocArray = $connection->cache(7 * Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
        }

        $array = array();

        foreach ($assocArray as $v) {
            array_push($array, $v['id']);
        }

        if (sizeof($array) > 0 && is_array($array)) {
            return $array;
        }

        $sql = sprintf(
            "SELECT sub_specialty.id AS id
            FROM sub_specialty
            INNER JOIN specialty
            ON sub_specialty.specialty_id = specialty.id
            WHERE sub_specialty.name LIKE '%s%%'
            OR specialty.name LIKE '%s'
            OR specialty.name_plural = '%s'
            OR CONCAT(specialty.name, ' - ', sub_specialty.name) = '%s'
            ORDER BY specialty.name = '%s' DESC;",
            $q,
            $q,
            $q,
            $q,
            $q
        );
        $assocArray = $connection->cache(7 * Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
        $array = array();

        foreach ($assocArray as $v) {
            array_push($array, $v['id']);
        }

        if (sizeof($array) > 0 && is_array($array)) {
            return $array;
        }
        return false;
    }

    /**
     * Find specialty by friendly_url
     * @param string $friendlyUrl
     * @return Specialty
     */
    public function findByFriendlyUrl($friendlyUrl)
    {
        $specialty = Specialty::model()->find('friendly_url=:friendly_url', array(':friendly_url' => $friendlyUrl));
        if (!$specialty) {
            // look for redirects if the speciatly was not found
            $redirect = $this->searchRedirects($friendlyUrl);
            if ($redirect) {
                $specialty = Specialty::model()->find(
                    'friendly_url=:friendly_url',
                    array(
                        ':friendly_url' => $redirect
                    )
                );
            }
        }
        return $specialty;
    }

    /**
     * Get all specialties
     * @param string $level
     * @return array
     */
    public function getAllSpecialties($level = 'V')
    {
        $arrSpecialties = Specialty::model()->findAll([
            'select' => 'id, name',
            'condition' => 'level = :level',
            'order' => 't.name',
            'params' => [':level' => $level]
        ]);

        $allSpecialties = array();
        if (!empty($arrSpecialties)) {
            foreach ($arrSpecialties as $sp) {
                $allSpecialties[$sp['id']] = $sp['name'];
            }
        }
        return $allSpecialties;
    }

    /**
     * Autocomplete for specialties and subspecialties based on their name
     * @param string $q
     * @param int $limit
     * @param array $arrTreatment
     * @return array
     */
    public function autocomplete($q, $limit, $arrTreatment = false)
    {

        $q = trim($q);
        $join = "";

        if ($q === '') {
            $limit = 999;
        }

        if (is_array($arrTreatment) && sizeof($arrTreatment) > 0) {
            $join = " INNER JOIN specialty_treatment
                ON sub_specialty.id = specialty_treatment.sub_specialty_id
                AND specialty_treatment.treatment_id IN (" . implode("','", $arrTreatment) . ") ";
        }

        $sql = sprintf(
            "SELECT CONCAT('S-', sub_specialty.id) AS id, sub_specialty.name AS name
            FROM sub_specialty
            %s
            WHERE sub_specialty.name LIKE '%%%s%%'
            ORDER BY name
            LIMIT %d;",
            $join,
            BaseModel::sanitizeString($q),
            $limit
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Generate the friendly URL for a specialty given its name (in plural or not)
     *
     * @param string $specialtyPluralName
     * @param int $specialtyId
     * @return string
     */
    public static function getFriendlyUrl($specialtyPluralName, $specialtyId = 0)
    {
        $url = StringComponent::prepareNewFriendlyUrl($specialtyPluralName);
        return StringComponent::getNewFriendlyUrl($url, $specialtyId, 'specialty');
    }

    /**
     * Search specialties that have to be redirected
     *
     * @todo remove if no longer necessary (ask @andreiz, @davidm) just like Insurances
     *
     * @param string $url
     * @return string|boolean
     *
     * @todo This function works differently than InsuranceCompany::searchRedirects,
     * but it should work the same way (ie returning all info) -- to do so, adjust all calls to it in SiteController.php
     */
    public function searchRedirects($url)
    {

        //handle insurances that were redirected to another company
        $sql = sprintf(
            "SELECT friendly_url
            FROM specialty
            WHERE friendly_url_redirected
            LIKE '%%@%s@%%'
            LIMIT 1;",
            addslashes($url)
        );
        $array = Specialty::model()->cache(Yii::app()->params['cache_long'])->findBySql($sql);

        if ($array) {
            return $array->friendly_url;
        }

        return false;
    }

    /* copied */

    /**
     * Returns the specialties with more providers in a city
     * @param int $cityId
     * @return array
     */
    public function getTopSpecialtiesByCity($cityId)
    {
        $criteria['practice.city_id'] = $cityId;
        return $this->getTopSpecialties($criteria, 10);
    }

    /**
     * Get top 10 specialties in a given city for providers accepting a given insurance
     * @param int $cityId
     * @param int $insuranceId
     * @return int
     */
    public function getTopSpecialtiesByCityInsurance($cityId, $insuranceId)
    {
        $criteria['practice.city_id'] = $cityId;
        $criteria['insurance.company_id'] = $insuranceId;
        return $this->getTopSpecialties($criteria, 10);
    }

    /**
     * Get top specialties of providers accepting a given insurance
     * @param int $insuranceId
     * @return array
     */
    public function getTopSpecialtiesByInsurance($insuranceId)
    {

        $criteria['insurance.company_id'] = $insuranceId;
        return $this->getTopSpecialties($criteria, 1000);
    }

    /**
     * Get top 10 specialties by number of providers in a given state
     * @param int $stateId
     * @return array
     */
    public function getTopSpecialtiesByState($stateId)
    {
        $criteria['practice.state_id'] = $stateId;
        return $this->getTopSpecialties($criteria, 10);
    }

    /**
     * Get top 10 specialties in a given state for providers accepting a given insurance
     * @param int $stateId
     * @param int $insuranceId
     * @return array
     */
    public function getTopSpecialtiesByStateInsurance($stateId, $insuranceId)
    {
        $criteria['practice.state_id'] = $stateId;
        $criteria['insurance.company_id'] = $insuranceId;
        return $this->getTopSpecialties($criteria, 10);
    }

    /**
     * Return an array with all the specialties
     * Used by search forms in index.php and TabbedSearchForm.php
     * @param string $level
     * @return array
     */
    public function getSearchSpecialties($level = 'V')
    {
        $i = 0;
        $arrSpecialties = array();
        $objSpecialties = Specialty::model()->cache(Yii::app()->params['cache_long'])->findAll([
            'select' => 'id, name',
            'condition' => 'level = :level',
            'order' => 't.name',
            'params' => [
                ':level' => $level
            ]
        ]);
        foreach ($objSpecialties as $objSpecialty) {
            $arrSpecialties[$i]['id'] = $objSpecialty->id;
            $arrSpecialties[$i]['name'] = $objSpecialty->name;
            $i++;
        }
        return $arrSpecialties;
    }

    /**
     * PADM: Gets a specialty's subspecialties
     * @param int $specialtyId
     * @return array
     */
    public static function getSpecialtySubspecialties($specialtyId)
    {

        $sql = sprintf(
            "SELECT sub_specialty.id AS id, sub_specialty.name AS name
            FROM sub_specialty
            WHERE sub_specialty.specialty_id = %d
            ORDER BY sub_specialty.name ASC;",
            $specialtyId
        );
        return Yii::app()->db->createCommand($sql)->query();
    }

    /**
     * Get specialties to be shown in the footer of most pages
     * @return array
     */
    public static function getSpecialtiesForFooter()
    {
        $sql = "SELECT SQL_NO_CACHE name, friendly_url AS friendly
            FROM specialty
            WHERE friendly_url != ''
            AND id IN
            (4, 11, 14, 23, 25, 30, 31, 32, 38, 46, 69, 77, 71, 74, 78, 81, 107, 83, 118, 114)
            ORDER BY
            id = 4 DESC, id = 11 DESC, id = 14 DESC, id = 23 DESC, id = 25 DESC, id = 30 DESC, id = 31 DESC,
            id = 32 DESC, id = 38 DESC, id = 46 DESC, id = 69 DESC, id = 77 DESC, id = 71 DESC, id = 74 DESC,
            id = 78 DESC, id = 81 DESC, id = 107 DESC, id = 83 DESC, id = 118 DESC, id = 114;";
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }

    /**
     * Return Specialty name
     * @param int $specialtyId
     * @return string
     */
    public static function getSpecialty($specialtyId)
    {

        $specialty = Specialty::model()->cache(Yii::app()->params['cache_long'])->findByPk($specialtyId);
        if (!empty($specialty)) {
            return addslashes($specialty->name);
        }
        return '';
    }

    /**
     * Create or match a specialty with base on a specialty name provided by the Marketplace PUF
     * @param string $specialtyName
     * @return Specialty
     */
    public static function createOrMatchMarketplace($specialtyName)
    {
        $specialty = Specialty::model()->find('name = :name', array(':name' => $specialtyName));
        if (!$specialty) {
            $specialty = new Specialty();
            $specialty->name = $specialtyName;
            $specialty->name_plural = $specialtyName;
            $specialty->name_ads = strpos($specialtyName, ' ') ?
                    substr($specialtyName, 0, strpos($specialtyName, ' ') - 1) : $specialtyName;
            $specialty->api_abreviature = strtoupper(substr($specialtyName, 0, 5));
            $specialty->friendly_url = $specialty->getFriendlyUrl($specialty->name);
            if ($specialty->save()) {
                return $specialty;
            } else {
                return null;
            }
        }
        return $specialty;
    }

    /**
     * Return top specialties by number of providers based on criteria
     * @param array $criteria
     * @param int $maxResultSize
     * @return array
     */
    private function getTopSpecialties($criteria, $maxResultSize)
    {

        $topHits = DoctorIndex::topHitsAggregate($criteria, 'specialty.specialty_id', $maxResultSize);

        $specialtyIds = array_column($topHits, 'key');
        $providerCounts = array_column($topHits, 'doc_count');

        if (empty($specialtyIds)) {
            return array();
        }

        $specialtyIds = array_filter($specialtyIds);

        $ar = array();
        foreach ($specialtyIds as $value) {
            array_push($ar, "'" . $value . "'");
        }

        $specialtyIds = $ar;

        $sql = sprintf(
            "SELECT SQL_NO_CACHE id AS specialty_id, name AS specialty_name, friendly_url
            FROM specialty
            WHERE id IN (%s)
            ORDER BY FIELD(id, %s);",
            implode(',', $specialtyIds),
            implode(',', $specialtyIds)
        );

        $results = Yii::app()->db->cache(86400)->createCommand($sql)->queryAll();

        foreach ($results as $i => $result) {
            $results[$i]['providers'] = $providerCounts[$i];
        }

        return $results;
    }

    /**
     * Get specialties from Healthgrades partner data
     *
     * If $specialtyId is specified, this will return a string with the found value, otherwise will returns an
     * array with all specialties found.
     *
     * @param int $specialtyId
     * @return mixed
     */
    public static function getPartnerDataHealthgrades($specialtyId = null)
    {
        // partner_site.id = 46 => Healthgrades
        return PartnerSiteEntityMatch::getPartnerValue(46, 'specialty', 'specialty_code', $specialtyId);
    }

    /**
     * Get specialties from Vitals partner data
     *
     * If $specialtyId is specified, this will return a string with the found value, otherwise will returns an
     * array with all specialties found.
     *
     * @param int $specialtyId
     * @return mixed
     */
    public static function getPartnerDataVitals($specialtyId = null)
    {
        // partner_site.id = 111 => Vitals
        $specialties = PartnerSiteEntityMatch::getPartnerValue(
            PartnerSite::VITALS_SITE_ID,
            'vitals_sub_specialty_id',
            'vitals_specialty_subspecialty',
            $specialtyId
        );

        if (is_array($specialties)) {
            // create an array with unique specialties before the minus simbol (-)
            $unique = [];

            foreach ($specialties as $specialty) {
                $arrValues = explode(' _ ', $specialty);
                if (isset($arrValues[0])) {
                    $unique[$arrValues[0]] = true;
                }
            }

            // make an array with the standard format
            $return = [];
            foreach (array_keys($unique) as $specialty) {
                $return[] = $specialty;
            }

            return $return;
        } else {
            return $specialties;
        }
    }

    /**
     * Get specialties from Wellness partner data
     *
     * If $specialtyId is specified, this will return a string with the found value, otherwise will returns an
     * array with all specialties found.
     *
     * @param int $specialtyId
     * @return mixed
     */
    public static function getPartnerDataWellness($specialtyId = null)
    {
        // partner_site.id = 23 => Wellness
        return PartnerSiteEntityMatch::getPartnerValue(
            23,
            'specialty',
            'specialty_name',
            $specialtyId
        );
    }

    /**
     * Get the selectable GMB Category.
     * @return array
    */
    public function selectableSpecialty()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id,t.name';
        return self::model()->findAll($criteria);
    }

}
