<?php

Yii::import('application.modules.core_models.models._base.BaseAccountContact');

class AccountContact extends BaseAccountContact
{

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BaseAccountContact the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // check if has related records
        $accountContactId = $this->id;
        $contactSettings = AccountContactSetting::model()->findAll(
            "account_contact_id = :account_contact_id",
            array(":account_contact_id" => $accountContactId)
        );
        foreach ($contactSettings as $eachContactSetting) {
            $eachContactSetting->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * Get account notification contacts
     * @param int $accountId
     * @param string $returnFormat object || array
     * @return arr
     */
    public static function getAccountContacts($accountId = 0, $returnFormat = 'object')
    {

        if ($accountId == 0) {
            return false;
        }
        $result = AccountContact::model()->findAll(
            "account_id = :account_id",
            array(":account_id" => $accountId)
        );

        if (!empty($result) && $returnFormat == 'array') {
            $accountContacts = array();
            foreach ($result as $value) {
                $accountContacts[] = (array) $value->attributes;
            }
            return $accountContacts;
        }
        return $result;
    }

    /**
     * get contact details
     * @param int $contactId
     * @param int $accountId
     * @return arr
     */
    public static function getContact($contactId = 0, $accountId = 0)
    {

        if ($accountId == 0 || $contactId == 0) {
            return false;
        }

        $sql = sprintf(
            "SELECT id, account_id, name, mobile_phone, email, alert_app_request, alert_tracked_call,
            alert_review, alert_report, alert_billing, active
            FROM account_contact
            WHERE account_id = %d
            AND id = %d LIMIT 1;",
            $accountId,
            $contactId
        );

        $contact = Yii::app()->db->createCommand($sql)->queryRow();
        if (empty($contact)) {
            $contact = null;
        }
        return $contact;
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_contact
            WHERE account_contact.id = %d
            AND account_contact.account_id = %d;",
            $recordId,
            $accountId
        );
    }

    /**
     * Delete account contact
     * @param int $accountContactId
     * @param int $accountId
     * @return bool
     */
    public static function deleteContact($accountContactId = 0, $accountId = 0)
    {

        if ($accountId == 0 || $accountContactId == 0) {
            return false;
        }

        $contact = AccountContact::model()->findAll(
            "account_id = :account_id && id = :account_contact_id",
            array(
                ":account_id" => $accountId,
                ":account_contact_id" => $accountContactId
            )
        );

        foreach ($contact as $eachContact) {
            if (!$eachContact->delete()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Delete all contacts from account
     * @param int $accountId
     * @return bool
     */
    public static function deleteAllContactsByAccount($accountId = 0)
    {

        $arrContacts = AccountContact::model()->findAll("account_id = :account_id", array(":account_id" => $accountId));
        foreach ($arrContacts as $contact) {
            if (!$contact->delete()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Save Account contact
     * @param array $postData
     * @param int $accountId
     *
     * @return array $results
     */
    public static function saveData($postData = array(), $accountId = 0)
    {
        $results['success'] = false;
        $results['error'] = '';
        $results['data'] = [];

        $action = $postData['backend_action'];
        $accountContactId = isset($postData['id']) ? $postData['id'] : 0;

        switch ($action) {
            case 'delete':
                $results['success'] = self::deleteContact($accountContactId, $accountId);
                AuditLog::create('D', 'Account Contact', null, null, false);
                break;
            case 'save':

                $email = StringComponent::validateEmail(Common::get($postData, 'email', null));

                $mobilePhone = StringComponent::formatPhone(Common::get($postData, 'mobile_phone', null));

                if (empty($email) && empty($mobilePhone)) {
                    return [
                        'success' => false,
                        'error' => 'Email & Phone are empty'
                    ];
                }

                if ($accountContactId > 0) {
                    $accountContact = AccountContact::model()->findByPk($accountContactId);
                } elseif (!empty($email)) {
                    // find by email + account_id
                    $accountContact = AccountContact::model()->find(
                        "email = :email AND account_id = :account_id",
                        [':email' => $email, ':account_id' => $accountId]
                    );
                } elseif (!empty($mobilePhone)) {
                    // find by mobilePhone + account_id
                    $accountContact = AccountContact::model()->find(
                        "mobile_phone = :mobilePhone AND account_id = :account_id",
                        [":mobilePhone" => $mobilePhone, ':account_id' => $accountId]
                    );
                }

                if (empty($accountContact)) {
                    $accountContact = new AccountContact();
                    $accountContact->account_id = $accountId;
                    $accountContact->alert_app_request = null;
                    $accountContact->alert_tracked_call = null;
                    $accountContact->alert_review = null;
                    $accountContact->alert_report = null;
                    $accountContact->alert_billing = null;
                    $accountContact->active = 1;

                    $auditType = 'C';
                } else {
                    $auditType = 'U';
                }

                $accountContact->email = $email;
                $accountContact->mobile_phone = $mobilePhone;

                $name = Common::get($postData, 'name', $accountContact->name);
                if (empty($name)) {
                    $name = $email;
                }
                $accountContact->name = $name;

                $active = Common::get($postData, 'active', $accountContact->active);
                $accountContact->active = (int) $active;

                // Read old values
                $alertAppRequest = !empty($accountContact->alert_app_request) ? $accountContact->alert_app_request : null;
                $alertTrackedCall = !empty($accountContact->alert_tracked_call) ? $accountContact->alert_tracked_call : null;
                $alertReview = !empty($accountContact->alert_review) ? $accountContact->alert_review : null;
                $alertReport = !empty($accountContact->alert_report) ? $accountContact->alert_report : null;
                $alertBilling = !empty($accountContact->alert_billing) ? $accountContact->alert_billing : null;

                $accountContactAlertAppRequest = Common::get($postData, 'alert_app_request', $alertAppRequest);
                $accountContactAlertTrackedCall = Common::get($postData, 'alert_tracked_call', $alertTrackedCall);
                $accountContactAlertReview = Common::get($postData, 'alert_review', $alertReview);
                $accountContactAlertReport = Common::get($postData, 'alert_report', $alertReport);
                $accountContactAlertBilling = Common::get($postData, 'alert_billing', $alertBilling);

                if ($accountContactAlertAppRequest == 'N/A') {
                    $accountContactAlertAppRequest = null;
                }
                if ($accountContactAlertTrackedCall == 'N/A') {
                    $accountContactAlertTrackedCall = null;
                }
                if ($accountContactAlertReview == 'N/A') {
                    $accountContactAlertReview = null;
                }
                if ($accountContactAlertReport == 'N/A') {
                    $accountContactAlertReport = null;
                }
                if ($accountContactAlertBilling == 'N/A') {
                    $accountContactAlertBilling = null;
                }

                $accountContact->alert_app_request = $accountContactAlertAppRequest;
                $accountContact->alert_tracked_call = $accountContactAlertTrackedCall;
                $accountContact->alert_review = $accountContactAlertReview;
                $accountContact->alert_report = $accountContactAlertReport;
                $accountContact->alert_billing = $accountContactAlertBilling;

                if ($accountContact->save()) {
                    $auditMsg = ($auditType == 'C' ? 'New ' : 'Update') .
                        ' Account Contact: #' . $accountContact->id . ' (' .
                        $postData['name'] . ')';
                    AuditLog::create($auditType, $auditMsg, null, null, false);
                    $results['success'] = true;

                    $contact = $accountContact->attributes;
                    unset($contact['account_id']);
                    unset($contact['date_added']);

                    $contactId = $contact['id'];

                    // Search and destroy old records
                    $contactSettings = AccountContactSetting::model()->findAll(
                        "account_contact_id = :contactId",
                        array(":contactId" => $contactId)
                    );

                    if (!empty($contactSettings)) {
                        foreach ($contactSettings as $accountContactSetting) {
                            $accountContactSetting->delete();
                        }
                    }

                    // regenerate new settings
                    if ($postData['receive_all']) {
                        // only 1 record with practice_id && provider_id = NULL
                        $accountContactSetting = new AccountContactSetting();
                        $accountContactSetting->account_contact_id = $contactId;
                        $accountContactSetting->practice_id = null;
                        $accountContactSetting->provider_id = null;
                        $accountContactSetting->save();
                    } else {
                        if (!empty($postData['practice'])) {
                            $arrPractices = $postData['practice'];
                            foreach ($arrPractices as $practiceId => $practice) {
                                if (!empty($practice['checked']) && $practice['checked'] == 1) {
                                    // all providers of this practice are selected
                                    $accountContactSetting = new AccountContactSetting();
                                    $accountContactSetting->account_contact_id = $contactId;
                                    $accountContactSetting->practice_id = $practiceId;
                                    $accountContactSetting->provider_id = null;
                                    $accountContactSetting->save();
                                } else {
                                    // find slected providers
                                    foreach ($practice as $providerId => $selected) {
                                        if ($selected == 1) {
                                            $accountContactSetting = new AccountContactSetting();
                                            $accountContactSetting->account_contact_id = $contactId;
                                            $accountContactSetting->practice_id = $practiceId;
                                            $accountContactSetting->provider_id = $providerId;
                                            $accountContactSetting->save();
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $contact['settings'] = AccountContactSetting::model()->getContactSetting($contactId);
                    $results['data']['contact'] = $contact;
                } else {
                    $results['data'] = $accountContact->getErrors();
                }
                break;
            default:
                break;
        }

        SalesForceComponent::updateSmsAlerts($accountId);
        return $results;
    }
}
