<?php

Yii::import('application.modules.core_models.models._base.BaseProviderGa');

class ProviderGa extends BaseProviderGa
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @todo document
     * @return boolean
     */
    public function beforeSave()
    {

        // normalize phone number format
        if ($this->phone_number) {
            $this->phone_number = preg_replace('/[^0-9]/', '', $this->phone_number);
        }
        return parent::beforeSave();
    }

}
