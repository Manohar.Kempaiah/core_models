<?php

Yii::import('application.modules.core_models.models._base.BaseCpnSoftwareInstallation');

class CpnSoftwareInstallation extends BaseCpnSoftwareInstallation
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Confirm Load
     *
     * @param str $identifier
     * @param int $installationId
     * @param str $seconds
     */
    public function confirmLoad($identifier, $installationId, $seconds)
    {
        //--grab the software id
        $selectq = "SELECT s.id
            FROM cpn_software s
            WHERE s.guid = :guid
            AND s.del_flag = 0;";

        $command = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($selectq);
        $command->bindParam(":guid", $identifier, PDO::PARAM_STR);
        $softwareId = $command->queryScalar();

        //-- mark it as loaded
        if ($softwareId && $installationId) {
            // Query to cpn_software_installation
            $cpnSI = CpnSoftwareInstallation::model()->find(
                'cpn_software_id = :cpn_software_id '
                    . 'AND cpn_installation_id = :cpn_installation_id AND del_flag = :del_flag',
                array(
                    ':cpn_software_id' => $softwareId,
                    ':cpn_installation_id' => $installationId,
                    ':del_flag' => 0
                )
            );
            if ($cpnSI) {
                $cpnSI->installed_flag     = 1;
                $cpnSI->install_software_flag = 0;
                $cpnSI->date_installed = date("Y-m-d H:i:s");
                $cpnSI->seconds_to_install = $seconds;
                $cpnSI->date_updated = date("Y-m-d H:i:s");
                $cpnSI->save();
                return true;
            }
        }
        return false;
    }

    /**
     * Confirm Unload
     *
     * @param str $identifier
     * @param str $installationId
     * @param str $seconds
     */
    public function confirmUnload($identifier, $installationId, $seconds)
    {
        //--grab the software id
        $selectq = "SELECT s.id
            FROM cpn_software s
            WHERE s.guid = :guid
            AND s.del_flag = 0";

        $command = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($selectq);
        $command->bindParam(":guid", $identifier, PDO::PARAM_STR);
        $softwareId = $command->queryScalar();

        //-- mark it as unloaded
        if ($softwareId && $installationId) {
            // Query to cpn_software_installation
            $cpnSI = CpnSoftwareInstallation::model()->find(
                'cpn_software_id = :cpn_software_id '
                    . 'AND cpn_installation_id = :cpn_installation_id AND del_flag = :del_flag',
                array(
                    ':cpn_software_id' => $softwareId,
                    ':cpn_installation_id' => $installationId,
                    ':del_flag' => 0
                )
            );
            if ($cpnSI) {
                $cpnSI->uninstalled_flag = 1;
                $cpnSI->date_uninstalled = date("Y-m-d H:i:s");
                $cpnSI->seconds_to_uninstall = $seconds;
                $cpnSI->date_updated = date("Y-m-d H:i:s");
                $cpnSI->save();

                return true;
            }
            return false;
        }
    }

    /**
     * Get Software Using GUID
     *
     * @param int $installationId
     * @param str $softwareGUID
     * @return array
     */
    public function getSoftwareUsingGUID($installationId, $softwareGUID)
    {
        $query_select = sprintf(
            "SELECT
                si.uninstall_software_flag,
                s.name as SoftwareName,
                s.installation_folder,
                s.pre_installation_exe,
                s.post_installation_exe,
                s.pre_uninstallation_exe,
                s.post_uninstallation_exe,
                s.guid,
                s.checksum,
                s.file_name,
                s.delete_pre_after_install,
                s.delete_post_after_install
            FROM cpn_software_installation si
                INNER JOIN cpn_installation i on si.cpn_installation_id = i.id
                INNER JOIN cpn_software s on si.cpn_software_id = s.id
            WHERE s.guid = '%s'
                AND i.id = '%s'
                AND si.date_to_install <= '%s'
                AND i.enabled_flag = 1
                AND i.del_flag = 0
                AND s.del_flag = 0
                AND s.enabled_flag = 1
                AND si.del_flag = 0
                AND si.enabled_flag = 1
                AND (si.install_software_flag = 1 OR si.uninstall_software_flag   = 1); ",
            $softwareGUID,
            $installationId,
            gmdate("Y-m-d\TH:i:s\Z")
        );

        return Yii::app()->db->createCommand($query_select)->queryAll();
    }

    /**
     * Cet Software Without GUID
     *
     * @param int $installationIdentifier
     * @param str $softwareGUID
     * @return array
     */
    public function getSoftwareWithoutGUID($installationId)
    {
        $query_select = sprintf(
            "SELECT
                si.uninstall_software_flag,
                s.name as SoftwareName,
                s.installation_folder,
                s.pre_installation_exe,
                s.post_installation_exe,
                s.pre_uninstallation_exe,
                s.post_uninstallation_exe,
                s.checksum,
                s.guid,
                s.file_name,
                s.delete_pre_after_install,
                s.delete_post_after_install
            FROM cpn_software_installation si
                INNER JOIN cpn_installation i on si.cpn_installation_id = i.id
                INNER JOIN cpn_software s on si.cpn_software_id = s.id
            WHERE s.guid != '0DB327E7-973B-4368-9D7F-9569EB2DA811' -- this is our loader
                AND i.id = '%s'
                AND si.date_to_install <= '%s'
                AND i.enabled_flag = 1
                AND i.del_flag = 0
                AND s.del_flag = 0
                AND s.enabled_flag = 1
                AND si.del_flag = 0
                AND si.enabled_flag = 1
                AND (si.install_software_flag = 1 OR si.uninstall_software_flag = 1)",
            $installationId,
            gmdate("Y-m-d\TH:i:s\Z")
        );

        return Yii::app()->db->createCommand($query_select)->queryAll();
    }

    /**
     * Confirm Installation
     *
     * @param str $identifier
     * @param int $installationId
     * @param str $seconds
     * @return bool
     */
    public function confirmInstallation($identifier, $installationId, $seconds, $installedFlag)
    {
        //--grab the software id
        $selectq = "SELECT s.id
            FROM cpn_software s
            WHERE s.guid = :guid
            AND s.del_flag = 0";

        $command = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($selectq);
        $command->bindParam(":guid", $identifier, PDO::PARAM_STR);
        $softwareId = $command->queryScalar();

        //-- mark it as loaded
        if ($softwareId && $installationId) {
            // Query to cpn_software_installation
            $cpnSI = CpnSoftwareInstallation::model()->find(
                'cpn_software_id = :cpn_software_id AND cpn_installation_id = :cpn_installation_id '
                    . 'AND del_flag = :del_flag',
                array(
                    ':cpn_software_id' => $softwareId,
                    ':cpn_installation_id' => $installationId,
                    ':del_flag' => 0
                )
            );
            if ($cpnSI) {
                $cpnSI->installed_flag     = ($installedFlag ? 1 : 0);
                $cpnSI->install_software_flag = ($installedFlag ? 0 : 1);
                $cpnSI->date_installed = date("Y-m-d H:i:s");
                $cpnSI->seconds_to_install = $seconds;
                $cpnSI->date_updated = date("Y-m-d H:i:s");
                $res = $cpnSI->save();

                if ($res) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Heartbeat
     *
     * @param object $installationId
     * @param str $softwareIdentifier
     * @return bool
     */
    public function heartbeat($installationId, $softwareIdentifier, $ipAddress)
    {
        $arr = [];
        $arr['id'] = $installationId->id;
        $arr['date'] = date('Y-m-d H:i:s');
        $arr['softwareIdentifier'] = $softwareIdentifier;
        $arr['ipAddress'] = $ipAddress;
        $arr['installationId'] = $installationId->installation_identifier;

        $result = Yii::app()->redis->exec(
            'SET',
            array('heartbeat:'.$installationId->installation_identifier.'-'.$softwareIdentifier, json_encode($arr))
        );

        return (bool)$result;
    }
    /**
     * Confirm Uninstallation
     *
     * @param int $installationId
     * @return bool
     */
    public function confirmUninstallation($installationId)
    {
        //do we have the installation?
        //yes
        if ($installationId) {
            //grab the ids based on the installed flag and del flag
            $query_select = sprintf(
                "SELECT id
                FROM   cpn_software_installation
                WHERE  cpn_installation_id = '%s'
                    AND installed_flag = 1
                    AND enabled_flag = 1
                    AND del_flag = 0",
                $installationId
            );

            //grab our ids from the query above
            $ids = Yii::app()->db->createCommand($query_select)->queryall();

            //iterate through our IDs
            foreach ($ids as $id) {
                //find it by its id
                $cpnSI = CpnSoftwareInstallation::model()->find('id = :id', array(':id' => $id['id']));
                //set it to not installed
                $cpnSI->installed_flag = 0;
                //set it to uninstalled
                $cpnSI->uninstalled_flag = 1;
                //set the date uninstalled to today
                $cpnSI->date_uninstalled = date("Y-m-d H:i:s");
                //set the date updated
                $cpnSI->date_updated = date("Y-m-d H:i:s");
                //set it to deleted
                $cpnSI->del_flag = 1;
                //save it
                $cpnSI->save();
            }
            //return success
            return true;
        }
        //return failure if we got here
        return false;
    }

    /**
     * Setup Cpn software for a given installation
     * @param integer $cpnInstallationId
     * @param integer $accountId
     * @param integer|null $ehrType
     */
    public static function setup($cpnInstallationId, $accountId, $ehrType = null)
    {
        // Base Companion Software
        static::addEntry(CpnSoftware::CPN_SYSTEM_TRAY, $cpnInstallationId, $accountId);
        static::addEntry(CpnSoftware::CPN_UI_UPDATER, $cpnInstallationId, $accountId);
        static::addEntry(CpnSoftware::CPN_UI_LOADER, $cpnInstallationId, $accountId);

        if ($ehrType) {
            // EHR Companion Software
            static::addEntry(CpnSoftware::CPN_SV_LOADER, $cpnInstallationId, $accountId);
            static::addEntry(CpnSoftware::CPN_SV_MONITOR, $cpnInstallationId, $accountId);
            static::addEntry(CpnSoftware::CPN_SV_UPDATER, $cpnInstallationId, $accountId);
            static::addEntry(CpnSoftware::CPN_EHR_INTEGRATION, $cpnInstallationId, $accountId);

            // EHR Companion Dentrix Software
            if ($ehrType == 4) {
                static::addEntry(CpnSoftware::CPN_EHR_DENTRIX, $cpnInstallationId, $accountId);
            }
        }
    }

    /**
     * Add an entry for the installation ID with the given software ID
     *
     * @param $cpnSoftwareId
     * @param $cpnInstallationId
     * @param $accountId
     * @return bool
     */
    public static function addEntry($cpnSoftwareId, $cpnInstallationId, $accountId)
    {
        $cpnSoftwareInstallation = CpnSoftwareInstallation::model()->exists(
            'cpn_installation_id=:cpn_installation_id AND cpn_software_id=:cpn_software_id',
            array(':cpn_installation_id' => $cpnInstallationId, ':cpn_software_id' => $cpnSoftwareId)
        );

        if (!$cpnSoftwareInstallation) {
            $cpnSoftwareInstallation = new CpnSoftwareInstallation();
            $cpnSoftwareInstallation->guid = Yii::app()->db->createCommand("SELECT uuid()")->queryScalar();
            $cpnSoftwareInstallation->cpn_software_id = $cpnSoftwareId;
            $cpnSoftwareInstallation->cpn_installation_id = $cpnInstallationId;
            $cpnSoftwareInstallation->added_by_account_id = $accountId;
            return $cpnSoftwareInstallation->save();
        }

        return false;
    }
}
