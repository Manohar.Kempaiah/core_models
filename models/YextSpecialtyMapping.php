<?php

Yii::import('application.modules.core_models.models._base.BaseYextSpecialtyMapping');

class YextSpecialtyMapping extends BaseYextSpecialtyMapping
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Find up to 10 Yext's categories based on a practice's specialties or its provider's
     * @param int $practiceId
     * @return array
     */
    public static function getPracticeCategories($practiceId)
    {

        $sql = sprintf(
            "SELECT DISTINCT yext_category_id
            FROM yext_specialty_mapping
            INNER JOIN practice_specialty
            ON practice_specialty.sub_specialty_id = yext_specialty_mapping.sub_specialty_id
            WHERE practice_specialty.practice_id = %d
            LIMIT 10;",
            $practiceId
        );
        $arrCategories = Yii::app()->db->createCommand($sql)->queryAll();
        if (empty($arrCategories)) {
            $sql = sprintf(
                "SELECT DISTINCT yext_category_id
                FROM yext_specialty_mapping
                INNER JOIN provider_specialty
                ON provider_specialty.sub_specialty_id = yext_specialty_mapping.sub_specialty_id
                INNER JOIN provider_practice
                ON provider_specialty.provider_id = provider_practice.provider_id
                WHERE provider_practice.practice_id = %d
                LIMIT 10;",
                $practiceId
            );
            $arrCategories = Yii::app()->db->createCommand($sql)->queryAll();
        }
        if (!empty($arrCategories)) {
            $yextCategories = array();
            foreach ($arrCategories as $category) {
                $yextCategories[] = $category['yext_category_id'];
            }
            return $yextCategories;
        }
        return false;
    }

}
