<?php

Yii::import('application.modules.core_models.models._base.BaseProviderTreatment');

class ProviderTreatment extends BaseProviderTreatment
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for valid values before saving
     * @return boolean
     */
    public function beforeValidate()
    {
        if ($this->treatment_id == "") {
            $this->treatment_id = "0";
        }

        return parent::beforeValidate();
    }

    /**
     * Check if $this->treatment_id > 0
     * @return boolean
     */
    public function beforeSave()
    {
        if ($this->treatment_id == 0) {

            $treatment_id_save = Yii::app()->request->getPost('treatment_id_save');

            if ($treatment_id_save == '') {
                $treatment_id_save = Yii::app()->request->getPost('treatment_id_lookup');
            }

            if ($treatment_id_save != "") {

                // does this treatment exist?
                $mdlTreatment = Treatment::model()->find('name=:name', array(':name' => $treatment_id_save));
                $treatmentId = empty($mdlTreatment) ? 0 : $mdlTreatment->id;
                if ($treatmentId == 0) {
                    $newTreatment = new Treatment;
                    $newTreatment->name = $treatment_id_save;
                    $newTreatment->save();
                    $treatmentId = $newTreatment->id;
                }
                $this->treatment_id  = $treatmentId;
            }
            $this->addError("id", "Couldn't save treatment.");
            return false;
        }

        $duplicateProviderTreatment = ProviderTreatment::model()->exists(
            'provider_id=:provider_id AND treatment_id=:treatment_id',
            array(':provider_id' => $this->provider_id, ':treatment_id' => $this->treatment_id)
        );

        if ($duplicateProviderTreatment) {
            $this->addError('provider_treatment', 'Duplicate Provider Treatment.');
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id .
                '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">'
                . $this->provider . '</a>');
    }

    /**
     * PADM: Get Provider treatments
     * @param int $providerId
     * return array
     */
    public static function getProviderTreatment($providerId = 0)
    {

        if (intval($providerId) < 0) {
            return false;
        }

        $sql = sprintf(
            "SELECT pt.id, pt.treatment_id, t.name
            FROM provider_treatment AS pt
            LEFT JOIN treatment AS t
            ON pt.treatment_id = t.id
            WHERE pt.provider_id = %d; ",
            $providerId
        );
        $arrTemp = Yii::app()->db->createCommand($sql)->queryAll();

        $arrProviderTreatments = array();
        if ($arrTemp) {

            foreach ($arrTemp as $x) {
                $arrProviderTreatments[$x['treatment_id']]['reg_id'] = $x['id'];
                $arrProviderTreatments[$x['treatment_id']]['name'] = $x['name'];
            }
            return $arrProviderTreatments;
        }
        return false;
    }

    /**
     * Search: Gets the cost information for the treatments performed by a provider at different practices
     * @param int $providerId
     * @return array
     */
    public static function getDetails($providerId)
    {

        $sql = sprintf(
            "SELECT DISTINCT treatment.id, treatment.name AS name, 0 AS cost,
            treatment.id as treatment_id, %d as provider_id
            FROM provider_treatment
            INNER JOIN treatment
            ON provider_treatment.treatment_id = treatment.id
            WHERE provider_id = %d
            ORDER BY name;",
            $providerId,
            $providerId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();

    }

    /**
     * Save Data
     * @param array $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {

        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        $arrProviderTreatments = ProviderTreatment::getProviderTreatment($providerId);

        if ($postData) {
            foreach ($postData as $treatment) {

                $treatmentName = isset($treatment['name']) ? trim($treatment['name']) : trim($treatment);

                if (!empty($treatmentName)) {

                    // exists this treatment?
                    $mdlTreatment = Treatment::model()->find('name=:name', array(':name' => $treatmentName));
                    $treatmentId = empty($mdlTreatment) ? 0 : $mdlTreatment->id;
                    $isNew = false;
                    if ($treatmentId == 0) {
                        $newTreatment = new Treatment;
                        $newTreatment->name = trim($treatmentName);
                        $newTreatment->save();
                        if (!$newTreatment->save()) {
                            $results['success'] = false;
                            $results['error'] = "ERROR_SAVING_NEW_TREATMENT";
                            $results['data'] = $newTreatment->getErrors();
                            return $results;
                        }
                        $treatmentId = $newTreatment->id;
                        $isNew = true;
                    } else {
                        if (isset($arrProviderTreatments[$treatmentId])) {
                            // already existed
                            unset($arrProviderTreatments[$treatmentId]);
                        } else {
                            $isNew = true;
                        }
                    }

                    if ($isNew) {
                        // verify again if ProviderTreatment exists in db
                        $existsProviderTreatment = ProviderTreatment::model()->exists(
                            'provider_id=:providerId AND treatment_id=:treatmentId',
                            array(':providerId' => $providerId, ':treatmentId' =>$treatmentId)
                        );
                        if (!$existsProviderTreatment) {
                            $newProviderTreatment = new ProviderTreatment;
                            $newProviderTreatment->treatment_id = $treatmentId;
                            $newProviderTreatment->provider_id = $providerId;
                            if (!$newProviderTreatment->save()) {
                                $results['success'] = false;
                                $results['error'] = "ERROR_SAVING_PROVIDER_TREATMENT";
                                $results['data'] = $newProviderTreatment->getErrors();
                                return $results;
                            } else {
                                // return the new item
                                $results['data'][] = (array) $newProviderTreatment->attributes;
                            }
                            $auditSection = 'ProviderTreatment #' . $providerId .' (#' .
                                $newProviderTreatment->treatment_id . ')';
                            AuditLog::create('C', $auditSection, 'provider_treatment', null, false);
                        }

                    }
                }
            }
        }
        if (!empty($arrProviderTreatments)) {
            // delete old record
            $auditList = '';
            foreach ($arrProviderTreatments as $toDelete) {
                $providerTreatment = ProviderTreatment::model()->find('id=:id', array(':id' =>
                    $toDelete['reg_id']));
                if (isset($providerTreatment) && !empty($providerTreatment)) {
                    $auditList.= $providerTreatment->id . ' - ';
                    if (!$providerTreatment->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_TREATMENT";
                        $results['data'] = $providerTreatment->getErrors();
                        return $results;
                    }

                }
            }
            $auditSection = 'ProviderTreatment #' . $providerId .' (#' . $auditList . ')';
            AuditLog::create('D', $auditSection, 'provider_treatment', null, false);

        }

        return $results;
    }

}
