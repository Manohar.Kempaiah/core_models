<?php

Yii::import('application.modules.core_models.models._base.BaseOrganizationHistory');

class OrganizationHistory extends BaseOrganizationHistory
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Save a note in SF with the changes that were performed
     * @return bool
     */
    public function afterSave()
    {

        switch ($this->event) {
            case 'N':
                return parent::afterSave();
            case 'R':
                return parent::afterSave();
            case 'B':
                $subject = "Doctor.com: Onboarding complete";
                $description = "Onboarding was completed! ";
                break;
            case 'P':
                $subject = "Doctor.com: Paused";
                $description = "This organization was marked as paused in our internal system";
                break;
            case 'PR':
                $subject = "Doctor.com: Pause request";
                $description = "This organization was marked to be paused. Once 31 days go by it will be paused.";
                break;
            case 'C':
                $subject = "Doctor.com: Canceled";
                $description = "This organization's features have been deactivated";
                break;
            case 'A':
                $subject = "Doctor.com: Activated";
                $description = "This organization has been activated";
                break;
            case 'UP':
                $subject = "Doctor.com: Unpaused";
                $description = "Countdown clock has been stopped";
                break;
        }

        $currentOrg = Organization::model()->findByPK($this->organization_id);

        if (!empty($currentOrg->salesforce_id)) {
            $data = array(
                'salesforce_id' => $currentOrg->salesforce_id,
                'subject' => $subject,
                'description' => $description
            );
            SqsComponent::sendMessage('salesforceSaveAccountNote', $data);
        }

        return parent::afterSave();
    }

}
