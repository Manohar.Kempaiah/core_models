<?php

Yii::import('application.modules.core_models.models._base.BaseInsuranceCompanyAlias');

class InsuranceCompanyAlias extends BaseInsuranceCompanyAlias
{

    public $company_id_issuer;
    public $company_id_alias;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Before Save
     * @return boolean
     */
    public function beforeSave()
    {

        $typeAlias = Yii::app()->getRequest()->getParam('type_alias');

        if ($typeAlias == 1) {

            $error = 0;
            if (empty($this->issuer_id)) {
                $this->addError('issuer_id', 'Issuer cannot be blank.');
                $error = 1;
            }

            if (empty($this->marketplace_category)) {
                $this->addError('marketplace_category', 'Marketplace Category cannot be blank.');
                $error = 1;
            }

            if ($error == 1) {
                return false;
            }
        }


        if (!isset($this->issuer_id)) {
            $this->issuer_id = '';
        }

        // is this an existing record?
        if ($this->id > 0) {
            // yes, check duplicate name
            $duplicateName = InsuranceCompanyAlias::model()->exists(
                'issuer_id = :issuerId AND alias_name = :aliasName AND company_id = :companyId AND id != :thisId',
                array(
                    ':issuerId' => $this->issuer_id,
                    ':aliasName' => $this->alias_name,
                    ':companyId' => $this->company_id,
                    ':thisId' => $this->id
                )
            );
        } else {
            // no, check duplicate name
            $duplicateName = InsuranceCompanyAlias::model()->exists(
                'issuer_id = :issuerId AND alias_name = :aliasName AND company_id = :companyId ',
                array(
                    ':issuerId' => $this->issuer_id,
                    ':aliasName' => $this->alias_name,
                    ':companyId' => $this->company_id
                )
            );
        }

        // was there a duplicate?
        if ($duplicateName) {
            // yes, exit
            $this->addError('alias_name', 'Duplicate alias');
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from insurance_company if associated records exist in insurance table
        $has_associated_records = InsuranceCompany::model()->exists('name=:name', array(':name' => $this->alias_name));
        if ($has_associated_records) {
            $this->addError("id", "Alias can't be deleted because there are companies linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    /*
     * Get a complete list of insurance company aliases matched with companies
     * @return array
     */

    public static function allAliases()
    {
        $sql = "SELECT DISTINCT
                GROUP_CONCAT(DISTINCT insurance_company.id SEPARATOR '+') AS int_id,
                GROUP_CONCAT(DISTINCT insurance_company.name SEPARATOR '+') AS company_id,
                IF(insurance_company_alias.alias_name IS NULL, insurance_company.name,
                insurance_company_alias.alias_name) AS name,
                insurance_company.id AS id
                FROM insurance_company
                LEFT JOIN insurance_company_alias ON insurance_company_alias.company_name = insurance_company.name
                INNER JOIN insurance ON insurance.company_id = insurance_company.id
                GROUP BY name
                ORDER BY is_popular DESC, name ASC;";
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }

}
