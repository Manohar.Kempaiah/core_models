<?php

Yii::import('application.modules.core_models.models._base.BasePractice');

class Practice extends BasePractice
{

    /**
     * PracticeDetails attributes
     */
    public $practice_size;
    public $emr_id;
    public $practice_type_id;
    public $coverage_state_id;
    public $coverageLocation; // for display only
    public $coverage_location_id;
    public $coverage_radius;
    public $yext_special_offer;
    public $facebook_cover;
    public $reward_email;
    public $verified;
    public $uploadPath;
    public $nasp_date;
    // temporary, in order to place the twilio numbers until they're saved
    public $arrayTwilioNumbers;

    /**
     * Indicates the Practice isn't linked to any DDC Account.
     */
    const CLAIMED_NO_ACCOUNT = 0;

    /**
     * Indicates the Practice is linked to a Freemium Account. That means account that isn't related to an
     * Organization or Account that are related to a Canceled Organization (Churned Client).
     */
    const CLAIMED_FREEMIUM_ACCOUNT = 1;

    /**
     * Indicates the Practice is linked to a Paying Client (Active/ Paused Organization).
     */
    const CLAIMED_PAYING_ACCOUNT = 2;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Initializes the model with the correct basePath according to the active module.
     * @internal Avoid using __construct. Use init() instead.
     *  if you still need to use it, call parent::__construct($scenario). Read yii documentation.
     */
    public function init()
    {
        parent::init();
        $basePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'providers';
        $this->uploadPath = $basePath . Yii::app()->params['upload_practices'];
    }


    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        $relations = parent::relations();
        $relations['providers'] = array(
            self::MANY_MANY, 'Provider',
            'provider_practice(practice_id, provider_id)'
        );

        return $relations;
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['practice_group_id'] = Yii::t('app', 'Practice Group');
        $labels['providers'] = Yii::t('app', 'Providers');
        $labels['facebook_cover'] = Yii::t('app', 'Facebook Cover');
        $labels['aesthetic_procedure'] = Yii::t('app', 'Aesthetic Procedures Performed');
        $labels['verified'] = Yii::t('app', 'Verified');
        $labels['nasp_date'] = Yii::t('app', 'NASP Date');

        return $labels;
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = array(
            'facebook_cover',
            'file',
            'types' => 'jpg, jpeg, png',
            'maxSize' => 1024 * 1024 * 15,
            'allowEmpty' => true
        );
        $rules[] = array('website', 'url', 'message' => 'Invalid Website URL: must begin with http://');
        $rules[] = array('verified', 'numerical', 'integerOnly' => true);
        $rules[] = array('verified', 'safe', 'on' => 'search');
        $rules[] = array('nasp_date', 'safe', 'on' => 'search');

        return $rules;
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        //
        // AVOID ACTIONS
        //

        // avoid deleting from practice if associated records exist in listing_site_practice_stats table
        $has_associated_records = ListingSitePracticeStats::model()->exists(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Practice can't be deleted because it has listing site.");
            return false;
        }

        // avoid deleting from practice if associated records exist in provider_practice_gmb table
        $has_associated_records = ProviderPracticeGmb::model()->exists(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Practice can't be deleted because it has GMB.");
            return false;
        }

        // avoid deleting from practice if associated records exist in organization_feature table
        $has_associated_records = OrganizationFeature::model()->exists(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Practice can't be deleted because it has active features.");
            return false;
        }

        // avoid deleting from practice if associated records exist in organization_history table
        $has_associated_records = OrganizationHistory::model()->exists(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Practice can't be deleted because it belongs to an organization's history.");
            return false;
        }

        // Is this practice parent practice ?
        $existParentPractice = $this->exists(
            'id<>:id AND parent_practice_id=:parent_practice_id',
            array(':id' => $this->id, ':parent_practice_id' => $this->id)
        );
        if ($existParentPractice) {
            $this->addError(
                "id",
                "This practice cannot be deleted because there are practices linked to this one as parent practice."
            );
            return false;
        }

        // avoid deleting from practice if associated records exist in account_device table
        $existAccountDevice = AccountDevice::model()->exists(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        if ($existAccountDevice) {
            $this->addError(
                "id",
                "This practice cannot be deleted because there are account devices linked to this Practice."
            );
            return false;
        }

        // avoid deleting from practice if associated records exist in
        // scan
        $existScan = Scan::model()->exists('practice_id=:practice_id', array(':practice_id' => $this->id));
        if ($existScan) {
            $this->addError("id", "This practice cannot be deleted because there are scans linked to this Practice.");
            return false;
        }

        // avoid deleting from practice if associated records exist in
        // scan_practice
        $existScan = ScanPractice::model()->exists('practice_id=:practice_id', array(':practice_id' => $this->id));
        if ($existScan) {
            $this->addError("id", "This practice cannot be deleted because there are scans linked to this Practice.");
            return false;
        }

        // avoid deleting from provider_practice_widget if associated records exist in
        $arrPpw = ProviderPracticeWidget::model()->exists(
            'practice_id = :practiceId',
            array(':practiceId' => $this->id)
        );
        if (!empty($arrPpw)) {
            $this->addError("id", "This practice cannot be deleted because there are Widget linked to it.");
            return false;
        }

        // avoid deleting from scheduling_mail if associated records exist in
        $arrSM = SchedulingMail::model()->exists('practice_id = :practiceId', array(':practiceId' => $this->id));
        if (!empty($arrSM)) {
            $this->addError("id", "This practice cannot be deleted because there are Appointments linked to it.");
            return false;
        }

        //
        // DELETE ACTIONS
        //

        // delete records from partner_site_params before deleting this practice
        $arrPartnerSiteParams = PartnerSiteParams::model()->findAll(
            'practice_id=:practiceId',
            array(':practiceId' => $this->id)
        );
        foreach ($arrPartnerSiteParams as $partnerSiteParam) {
            if (!$partnerSiteParam->delete()) {
                $this->addError("id", "Couldn't delete from Partner Site Param.");
                return false;
            }
        }

        // delete records from account_practice before deleting this practice
        $arrAccountPractice = AccountPractice::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrAccountPractice as $accountPractice) {
            if (!$accountPractice->delete()) {
                $this->addError("id", "Couldn't unlink practice from account.");
                return false;
            }
        }

        // delete records from practice_details before deleting this practice
        $arrPracticeDetails = PracticeDetails::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrPracticeDetails as $practiceDetails) {
            if (!$practiceDetails->delete()) {
                $this->addError("id", "Couldn't delete practice's details");
                return false;
            }
        }

        // delete records from practice_language before deleting this practice
        $arrPracticeLanguage = PracticeLanguage::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrPracticeLanguage as $practiceLanguage) {
            if (!$practiceLanguage->delete()) {
                $this->addError("id", "Couldn't delete practice language.");
                return false;
            }
        }

        // delete records from practice_office_hour before deleting this practice
        $arrPracticeOfficeHour = PracticeOfficeHour::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrPracticeOfficeHour as $practiceOfficeHour) {
            if (!$practiceOfficeHour->delete()) {
                $this->addError("id", "Couldn't delete office hours.");
                return false;
            }
        }

        // delete records from practice_photo before deleting this practice
        $arrPracticePhoto = PracticePhoto::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrPracticePhoto as $practicePhoto) {
            if (!$practicePhoto->delete()) {
                $this->addError("id", "Couldn't delete photos.");
                return false;
            }
        }

        // delete records from practice_treatment before deleting this practice
        $arrPracticeTreatment = PracticeTreatment::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrPracticeTreatment as $practiceTreatment) {
            if (!$practiceTreatment->delete()) {
                $this->addError("id", "Couldn't delete treatments.");
                return false;
            }
        }

        // delete records from practice_video before deleting this practice
        $arrPracticeVideo = PracticeVideo::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrPracticeVideo as $practiceVideo) {
            if (!$practiceVideo->delete()) {
                $this->addError("id", "Couldn't delete videos.");
                return false;
            }
        }

        // delete records from practice_hospital before deleting this practice
        $arrProviderHospital = ProviderHospital::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrProviderHospital as $providerHospital) {
            if (!$providerHospital->delete()) {
                $this->addError("id", "Couldn't delete hospitals.");
                return false;
            }
        }

        // delete records from provider_practice before deleting this practice
        $arrProviderPractice = ProviderPractice::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrProviderPractice as $providerPractice) {
            if (!$providerPractice->delete()) {
                $this->addError("id", "Couldn't unlink providers from practice.");
                return false;
            }
        }

        // delete records from provider_practice_insurance before deleting this practice
        $arrProviderPracticeInsurance = ProviderPracticeInsurance::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrProviderPracticeInsurance as $providerPracticeInsurance) {
            if (!$providerPracticeInsurance->delete()) {
                $this->addError("id", "Couldn't delete insurances.");
                return false;
            }
        }

        // delete records from provider_treatment_peformed before deleting this practice
        $arrProviderTreatmentPerformed = ProviderTreatmentPerformed::model()->findAll(
            "practice_id = :practice_id",
            array(":practice_id" => $this->id)
        );
        foreach ($arrProviderTreatmentPerformed as $providerTreatmentPerformed) {
            if (!$providerTreatmentPerformed->delete()) {
                $this->addError("id", "Couldn't delete treatments.");
                return false;
            }
        }

        // delete records from provider_url_history before delete this provider id
        $arrPracticeUrlHistory = PracticeUrlHistory::model()->findAll(
            "practice_id = :practice_id",
            array(":practice_id" => $this->id)
        );
        foreach ($arrPracticeUrlHistory as $practiceUrlHistory) {
            if (!$practiceUrlHistory->delete()) {
                $this->addError("id", "Couldn't delete URL history.");
                return false;
            }
        }

        // delete records from practice_patient before deleting this practice
        $arrPracticePatient = PracticePatient::model()->findAll(
            "practice_id = :practiceId",
            array(":practiceId" => $this->id)
        );
        foreach ($arrPracticePatient as $practicePatient) {
            if (!$practicePatient->delete()) {
                $this->addError("id", "Couldn't delete patients.");
                return false;
            }
        }

        // delete records from provider_practice_listing before deleting this practice
        $arrProviderPracticeListing = ProviderPracticeListing::model()->findAll(
            "practice_id = :practiceId",
            array(":practiceId" => $this->id)
        );
        foreach ($arrProviderPracticeListing as $providerPracticeListing) {
            if (!$providerPracticeListing->delete()) {
                $this->addError("id", "Couldn't delete links.");
                return false;
            }
        }

        // delete records from review_request before deleting this practice
        $arrRR = ReviewRequest::model()->findAll("practice_id = :practiceId", array(":practiceId" => $this->id));
        foreach ($arrRR as $rr) {
            if (!$rr->delete()) {
                $this->addError("id", "Couldn't delete Patient Messaging.");
                return false;
            }
        }

        // check if has rating.
        $arrPR = ProviderRating::model()->findAll("practice_id = :practiceId", array(":practiceId" => $this->id));
        foreach ($arrPR as $pr) {
            if (empty($pr->provider_id)) {
                // if not has provider, delete
                if (!$pr->delete()) {
                    $this->addError("id", "Couldn't delete reviews.");
                    return false;
                }
            } else {
                // if has provider, set practice as null
                $pr->practice_id = null;
                if (!$pr->save()) {
                    $this->addError('id', "Couldn't update reviews.");
                    return false;
                }
            }
        }

        // delete records from patientemail before deleting this practice
        $arrP = Patientemail::model()->findAll('practice_id=:practiceId', array(':practiceId' => $this->id));
        foreach ($arrP as $p) {
            if (!$p->delete()) {
                $this->addError("id", "This practice cannot be deleted because it is linked to an Automatic RR.");
                return false;
            }
        }

        // delete records from provider_practice_post before deleting this practice
        $arrPosts = ProviderPracticePost::model()->findAll(
            'practice_id=:practiceId',
            array(':practiceId' => $this->id)
        );
        foreach ($arrPosts as $post) {
            if (!$post->delete()) {
                $this->addError('id', 'This practice cannot be deleted because posts are attached to it.');
                return false;
            }
        }

        // delete records from provider_practice_gp before deleting this practice
        $arrPosts = ProviderPracticeGp::model()->findAll(
            'practice_id=:practiceId',
            array(':practiceId' => $this->id)
        );
        foreach ($arrPosts as $post) {
            if (!$post->delete()) {
                $this->addError(
                    'id',
                    'This practice cannot be deleted because provider_practice_gp are attached to it.'
                );
                return false;
            }
        }

        // delete twilio_numbers
        $arrTwilioNumbers = TwilioNumber::model()->findAll(
            'practice_id=:practice_id',
            array(':practice_id' => $this->id)
        );
        foreach ($arrTwilioNumbers as $twilioNumber) {
            if (!$twilioNumber->delete()) {
                $this->addError("id", "Couldn't delete phone number");
                return false;
            }
        }

        // delete records from practice_specialty before deleting this practice
        $arrRR = PracticeSpecialty::model()->findAll("practice_id = :practiceId", array(":practiceId" => $this->id));
        foreach ($arrRR as $rr) {
            if (!$rr->delete()) {
                $this->addError("id", "Couldn't delete PracticeSpecialty.");
                return false;
            }
        }

        // if parent_practice_id is pointing to itself remove it
        if ($this->id == $this->parent_practice_id) {
            $this->parent_practice_id = null;
            $this->save();
        }

        return parent::beforeDelete();
    }

    /**
     * If change location, add to practice_latlong_history
     * @return boolean
     */
    public function beforeSave()
    {

        $this->normalizeAttributes();

        if (empty($this->name)) {
            $this->addError('name', "Please specify the practice's name.");
            return false;
        }

        // latitude and longitude should not be empty
        if ((empty($this->latitude) || empty($this->longitude)) && $this->location_id) {
            $location = Location::model()->cache(Yii::app()->params['cache_long'])->findByPk($this->location_id);

            // set same latitude and longitude as location
            if (!empty($location->latitude) && !empty($location->longitude)) {
                $this->latitude = $location->latitude;
                $this->longitude = $location->longitude;
            }
        }

        if ($this->isNewRecord) {
            $this->date_added = date('Y-m-d H:i:s');
            if (empty($this->score)) {
                // set default score (it will get recomputed in afterSave()
                $this->score = 20;
            }
        } elseif (!empty($this->oldRecord)) {

            $oldRecord = $this->oldRecord;

            // Some location info was changed?
            if (
                $oldRecord->address != $this->address
                || $oldRecord->address_2 != $this->address_2
                || $oldRecord->location_id != $this->location_id
                || $oldRecord->latitude != $this->latitude
                || $oldRecord->longitude != $this->longitude
            ) {
                // Yes, was changed
                // Save history practice's location
                $historyRecord = new PracticeLatlongHistory;
                $historyRecord->id = 0;
                $historyRecord->practice_id = $oldRecord->id;
                $historyRecord->address = $oldRecord->address;
                $historyRecord->address_full = $oldRecord->address_full;
                $historyRecord->address_2 = $oldRecord->address_2;
                $historyRecord->location_id = $oldRecord->location_id;
                $historyRecord->latitude = $oldRecord->latitude;
                $historyRecord->longitude = $oldRecord->longitude;
                $historyRecord->save();
            }

            // update score (don't save since that will happen after this current save anyway)
            $this->updateScore(true);

            if (empty($this->phone_number)) {
                // we can't allow emptying the phone number if we active twilio numbers
                $twilioNbrExists = TwilioNumber::model()->exists(
                    'practice_id=:practiceId AND  provider_id IS NULL AND enabled = 1',
                    array(':practiceId' => $this->id)
                );
                if ($twilioNbrExists) {
                    $this->addError('phone_number', 'Please specify the practice\'s phone number.');
                    return false;
                }
            }

            // automatically change the friendly url if the practice name was modified but not the friendly url
            if ($oldRecord->name != $this->name && $oldRecord->friendly_url == $this->friendly_url) {
                $this->friendly_url = self::getFriendlyUrl($this->name, $this->id);
            }

            // if the url is changed; save url history so redirects work properly
            if ($oldRecord->friendly_url != $this->friendly_url && $oldRecord->friendly_url != '') {
                if (!PracticeUrlHistory::model()->exists(
                    'friendly_url=:friendly_url',
                    array(':friendly_url' => $oldRecord->friendly_url)
                )) {
                    $puh = new PracticeUrlHistory();
                    $puh->practice_id = $this->id;
                    $puh->friendly_url = $oldRecord->friendly_url;

                    if (!$puh->save()) {
                        Yii::log(
                            "Error saving url history after friendly url change: " .
                                var_export($puh->getErrors(), true),
                            'error',
                            __METHOD__
                        );
                    }
                }
            }
        }

        $this->address_full = trim(trim($this->address) . ' ' . trim($this->address_2));

        $this->date_updated = date('Y-m-d H:i:s');
        $this->fax_number = preg_replace('/[^0-9]/', '', $this->fax_number);
        $this->phone_number = preg_replace('/[^0-9]/', '', $this->phone_number);

        // we need to update yelp
        if ($this->yelp_sync_approved == "1") {
            $oldAttributes = null;
            if (!$this->isNewRecord) {
                $oldAttributes = self::getDetails($this->id);
                $oldAttributes = $oldAttributes[0];
            }
            SqsComponent::sendMessage(
                'yelpSync',
                array('practiceId' => $this->id, 'oldAttributes' => $oldAttributes)
            );
        }

        return parent::beforeSave();
    }

    /**
     * Update location when a practice is updated
     * Update Novartis (76) widget matches
     * @return bool
     */
    public function afterSave()
    {

        // Initialize variabled to check if geocoding i needed
        $location = Location::model()->findByPk($this->location_id);
        $oldAddress = empty($this->oldRecord) ? null : $this->oldRecord->address;
        $oldAddress2 = empty($this->oldRecord) ?  null : $this->oldRecord->address_2;
        $oldLocationId = empty($this->oldRecord) ? null : $this->oldRecord->location_id;

        // Location or the address changed, or check if the lat/long is the same than for the location?
        if (
            (empty($this->latitude) || empty($this->longitude))
            || (!empty($location->latitude) && $location->latitude == $this->latitude
                && !empty($location->longitude) && $location->longitude == $this->longitude)
            || ($oldAddress != $this->address)
            || ($oldAddress2 != $this->address_2)
            || ($oldLocationId != $this->location_id)
        ) {
            // yes
            // Send practice location update to the queue
            $queueBody = array(
                'practice_id' => $this->id,
            );
            SqsComponent::sendMessage('updatePracticeLocation', $queueBody);
        }

        // location was changed?
        if (
            !$this->isNewRecord &&
            isset($this->oldRecord) &&
            !empty($this->oldRecord->location_id) &&
            $this->oldRecord->location_id != $this->location_id
        ) {
            // get all suggestions for novartis widget
            $sql = "SELECT partner_site_entity_match.*
                FROM partner_site_entity_match
                WHERE partner_site_id = 76
                    AND match_column_name = 'location_id'
                    AND internal_column_value IN (
                        SELECT provider_practice.id
                        FROM provider_practice
                        WHERE provider_practice.practice_id = :practice_id
                    )";
            $matches = PartnerSiteEntityMatch::model()->findAllBySql($sql, [':practice_id' => $this->id]);

            // remove each one (the daily job will add new relations)
            if ($matches) {
                foreach ($matches as $m) {
                    $m->delete();
                }
            }
        }

        return parent::afterSave();
    }

    /**
     * Create Hospital
     * @param array $data
     * @return json
     */
    public static function createHospital($data = array())
    {

        $cityId = !empty($data['city_id']) ? $data['city_id'] : 0;
        $hospitalName = isset($data['hospital_name']) ? trim($data['hospital_name']) : '';

        if (!empty($data['city_name'])) {
            $arrCityData = explode('-', $data['city_name']);
            $cityId = end($arrCityData);
            $cityId = str_replace('(', '', $cityId);
            $cityId = str_replace(')', '', $cityId);
            $cityId = intval($cityId);
        }

        // Support location ID (for PADM v2)
        if (!empty($data['location_id'])) {
            $sql = sprintf(
                "SELECT
                    c.name AS city_name,
                    s.abbreviation AS state_ab,
                    s.country_id, l.id AS location_id
                FROM location AS l
                LEFT JOIN `city` AS c ON l.city_id = c.id
                LEFT JOIN `state` AS s ON c.state_id = s.id
                WHERE l.id = %d LIMIT 1",
                $data['location_id']
            );

            $data = Yii::app()->db->createCommand($sql)->queryRow();
        } else {
            $data = City::getLocationStatebyCityId($cityId);
        }

        $data['name'] = $hospitalName;

        if (!empty($data['location_id']) && !empty($data['name'])) {

            $model = new Practice;
            $model->attributes = $data;

            if (empty($model->score)) {
                $model->score = 20; // set default score (it will get recomputed in afterSave()
                $model->health_system_id = 1; // Default Health System (in blank)
            }

            if ($model->save()) {
                $practiceDetails = new PracticeDetails();
                $practiceDetails->practice_id = $model->id;
                $practiceDetails->practice_type_id = 1;
                $practiceDetails->practice_size = 7; // Hospital
                $practiceDetails->save();
                $output = array(
                    "success" => true,
                    "msg" => 'The new hospital was added correctly.',
                    "data" => $model->attributes,
                    "searchString" => $data['name'] . ' At ' . $data['city_name'] . ' - ' . $data['state_ab'],
                );
            } else {
                $output = array(
                    "success" => false,
                    "msg" => 'Could not add the new hospital. Please try again later.',
                    "data" => '',
                    "searchString" => '',
                );
            }
        } else {
            $output = array(
                "success" => false,
                "msg" => 'Hospital name and city are required.',
                "data" => '',
                "searchString" => '',
            );
        }

        return $output;
    }

    /**
     * Returns true if the given NPI appears to be of a valid format
     * (this just checks format, not whether we have that NPI or not)
     *
     * @param string $npi
     * @return boolean
     */
    public static function isNPIFormatValid($npi)
    {
        return preg_match('/\d{10}/', $npi) ? true : false;
    }

    /**
     * Validate NPI format and prevent duplicate
     * @return bool
     */
    public function validateNPI()
    {
        $this->npi_id = trim($this->npi_id);

        if (empty($this->npi_id)) {
            $this->npi_id = null;
            return true;
        }

        $isValidFormat = $this->isNPIFormatValid($this->npi_id);

        if ($isValidFormat) {
            $practiceNpi = Practice::model()->find('npi_id=:npiId', array(':npiId' => $this->npi_id));
            if (!empty($practiceNpi) && ($this->isNewRecord || $practiceNpi->id <> $this->id)) {
                // check if npi is unique
                $this->addError('npi_id', 'This NPI number is already used.');
                return false;
            }
        } else {
            $this->addError('npi_id', 'Incorrect format of NPI.');
            return false;
        }
        return true;
    }

    /**
     * Validate Friendly URL format and prevent duplicate
     * @return bool
     */
    public function validateFriendlyURL()
    {
        $this->friendly_url = trim($this->friendly_url);

        // check if we've entered a friendly url
        if (empty($this->friendly_url)) {
            $this->addError('friendly_url', 'Friendly URL cannot be blank');
            return false;
        }

        // check for valid prefix in the friendly url
        $prefix = substr($this->friendly_url, 0, 3);
        if ($prefix != '/p/') {
            $this->addError('friendly_url', 'Friendly URL must begin with "/p/"');
            return false;
        }

        // check if the friendly url is a valid string (after the /p/)
        if (!preg_match('/^[\w-]+$/', substr($this->friendly_url, 3))) {
            $this->addError('friendly_url', 'Only characters from A to Z, numbers and underscores (_) are accepted');
            return false;
        }

        if ($this->isNewRecord) {
            // check that it doesnt exist for other practice
            if (Practice::model()->exists('friendly_url=:friendly_url', array(':friendly_url' =>
            $this->friendly_url))) {
                $this->addError('friendly_url', 'This friendly URL is already used.');
                return false;
            }
        } else {
            // if the friendly url has changed
            if (Practice::model()->exists('friendly_url=:friendly_url AND id!=:id', array(':friendly_url' =>
            $this->friendly_url, ':id' => $this->id))) {
                $this->addError('friendly_url', 'This friendly URL is already used.');
                return false;
            }
        }

        return true;
    }

    /**
     * Sanitize input before validating
     * @return mixed
     */
    public function beforeValidate()
    {

        $this->normalizeAttributes();

        // validate NPI
        if (!$this->validateNPI()) {
            return false;
        }

        // validate FriendlyURL
        if (!$this->validateFriendlyURL()) {
            return false;
        }

        if (empty($this->score)) {
            // set default score (it will get recomputed in afterSave()
            $this->score = 20;
        }
        return parent::beforeValidate();
    }

    /**
     * For attributes set to null values, set them to how they should
     * be set before saving (for use in validation and before save;
     * note that it's in both in the case of beforeSave(false) being
     * called, meaning no validation is done)
     */
    public function normalizeAttributes()
    {

        $this->name = StringComponent::cleanseString($this->name);
        $this->address = StringComponent::cleanseString($this->address);
        $this->address_2 = StringComponent::cleanseString($this->address_2);

        $this->email = StringComponent::validateEmail($this->email);

        if (!empty($this->friendly_url) && (!preg_match('/^[\w-]+$/', substr($this->friendly_url, 3)))) {
            // Check if have bad characters
            $this->friendly_url = '';
        }
        if (empty($this->friendly_url)) {
            // If the friendly_url is empty, generate new one
            $this->friendly_url = $this->getFriendlyUrl($this->name);
        }

        $this->country_id = !empty($this->country_id) ? $this->country_id : 1;
        $this->localization_id = !empty($this->localization_id) ? $this->localization_id : 1;

        // Validate website url
        $this->website = strtolower(trim($this->website));
        if (!empty($this->website) && filter_var($this->website, FILTER_VALIDATE_URL)) {
            if (
                substr($this->website, 0, 7) != 'http://'
                && substr($this->website, 0, 8) != 'https://'
            ) {
                $this->website = 'http://' . trim($this->website);
            }
        } else {
            $this->website = null;
        }
    }

    /**
     * Get basic details about a practice
     * @param int $practiceId
     * @param int $twilioPartnerId // if exists, try it to find the related twilio_number
     * @return array
     */
    public static function getDetails($practiceId, $twilioPartnerId = 0)
    {
        $sql = sprintf(
            "SELECT p.id, p.name, address, address_2, address_full, location_id, phone_number,
            fax_number, twilio_number.twilio_number, email,website, handicap_accesible, aesthetic_procedure,
            description, logo_path, p.friendly_url, p.visits_number, p.date_added, p.date_updated,
            date_umd_updated, location.zipcode, city.name AS city, state.name AS state, state.abbreviation AS
            state_abbreviation,
            country.code AS country_code, country.id AS country_id, p.latitude, p.longitude, country.name AS
            country_name,
                yelp_business_id, yelp_sync_approved, p.website
            FROM practice AS p
            LEFT JOIN location
                ON p.location_id = location.id
            LEFT JOIN city
                ON location.city_id = city.id
            LEFT JOIN state
                ON city.state_id = state.id
            LEFT JOIN country
                ON state.country_id = country.id
            LEFT JOIN twilio_number
                ON (practice_id = %d AND twilio_partner_id = %d )
            WHERE p.id = %d
            LIMIT 1;",
            $practiceId,
            $twilioPartnerId,
            $practiceId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Get name and address given a practiceId
     * @param int $practiceId
     * @return string
     */
    public static function getNameAndAddress($practiceId = 0)
    {
        if ($practiceId > 0) {
            $sql = sprintf(
                'SELECT name, address, address_2, address_full FROM practice WHERE id = %d',
                $practiceId
            );
            $practice = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryRow();

            if (!empty($practice)) {
                $address = trim($practice['address_full']);
                if (empty($address)) {
                    $address = trim($practice['address']) . ' ' . trim($practice['address_2']);
                }
                return trim($practice['name']) . ' - ' . $address;
            }
        }
        return '';
    }

    /**
     * Finds a practice ID by name
     * @param string $q
     * @return array
     */
    public static function findByName($q)
    {
        if (trim($q) != '') {
            $sql = "SELECT id
                FROM practice
                WHERE " . self::replaceMultiple("name") . " LIKE CONCAT('%'," .
                self::replaceMultiple('"' . $q . '"') . ",'%')
                LIMIT 100;";

            $assocArray = Yii::app()->db->createCommand($sql)->queryAll();
            $array = array();
            foreach ($assocArray as $v) {
                array_push($array, $v['id']);
            }
            return $array;
        }
        return false;
    }

    /**
     * Given a practice name, generate a unique friendly URL for it by appending an incremental number
     *
     * @param string $practiceName
     * @param int $practiceId
     * @return string
     */
    public static function getFriendlyUrl($practiceName, $practiceId = 0)
    {
        $url = '/p/' . StringComponent::prepareNewFriendlyUrl($practiceName);
        return StringComponent::getNewFriendlyUrl($url, $practiceId, 'practice');
    }

    /**
     * @todo document
     * @param string $practice
     * @param string $address
     * @param string $phone_number
     * @param string $zipcode
     * @return array
     */
    public function registrationLookup($practice, $address, $phone_number, $zipcode)
    {

        $practice = addslashes($practice);
        $address = addslashes($address);

        $sql = sprintf(
            "SELECT practice.id, practice.name, practice.address,
            practice.address_2, practice.phone_number, location.zipcode,
            state.abbreviation AS state, city.name AS city
            FROM practice
            INNER JOIN location
                ON practice.location_id = location.id
            INNER JOIN city
                ON city.id=location.city_id
            INNER JOIN state
                ON state.id=city.state_id
            WHERE
                (
                    MATCH(practice.address_full, practice.phone_number)
                    AGAINST ('%s' IN NATURAL LANGUAGE MODE) AND location.zipcode = '%s'
                )
            LIMIT 1;",
            $address . ' ' . $phone_number,
            $zipcode
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Given a $practice_id, return whether its profile is complete or not
     * @param int $practice_id
     * @return bool
     */
    public static function isPracticeComplete($practice_id)
    {
        $sql = "SELECT IF(COUNT(1) = 1, true, false) AS profile_complete
            FROM practice
            INNER JOIN practice_details
                ON practice.id = practice_details.practice_id
            WHERE practice.id = :practice_id
                AND practice.name != ''
                AND (
                    (practice.address != '' AND practice.location_id > 0)
                    OR practice_details.practice_type_id != 1
                    )
                AND practice.phone_number != ''
                AND practice_details.practice_size > 0;";

        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":practice_id", $practice_id, PDO::PARAM_INT);

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Find matching practices by name in the practice table
     * Searches for Hospitals (practice_size = 7)
     * @param string $q
     * @param bool $returnSql
     * @param int $limit
     * @return json
     */
    public static function autocompleteHospital($q = '', $returnSql = false, $limit = 7)
    {
        if (empty($q)) {
            return '';
        }

        $q = addslashes($q);
        $sql = sprintf(
            "SELECT p.id, p.name, l.city_name AS city, s.abbreviation as state_ab
            FROM practice AS p
            INNER JOIN location AS l
                ON p.location_id = l.id
            INNER JOIN practice_details AS pd
                ON p.id = pd.practice_id
            LEFT JOIN city AS c
                ON l.city_id = c.id
            LEFT JOIN state AS s
                ON c.state_id = s.id
            WHERE p.country_id = 1
                AND localization_id = 1
                AND pd.practice_size = '7'
                AND p.name LIKE '%%%s%%'
            ORDER BY p.name ASC
            LIMIT %d ;",
            $q,
            $limit
        );

        if ($returnSql) {
            return $sql;
        }
        $arrResult = Yii::app()->db->createCommand($sql)->queryAll();

        $out = array();
        foreach ($arrResult as $p) {
            $hospitalName = strtoupper(trim($p['name']));
            $out[] = array(
                'label' => $hospitalName . ' At ' . $p['city'] . ' - ' . $p['state_ab'],
                'value' => $hospitalName . ' At ' . $p['city'] . ' - ' . $p['state_ab'],
                'id' => $p['id'],
            );
        }
        return $out;
    }

    /**
     * Find matching practices by name in the practice table
     * @param string $q
     * @param int $limit
     * @param bool $countResults
     * @param int $locationId
     * @return array
     */
    public static function autocomplete($q, $limit, $countResults = false, $locationId = 0)
    {

        $q = trim($q);
        $order = "name";

        if ($locationId > 0) {
            //if we have a location, sort by distance instead

            $location = Location::model()->findByPk($locationId);
            if ($location) {
                $order = sprintf(
                    " SQRT(
                        POWER(SIN((%s - ABS(location.latitude)) * pi() / 180 / 2), 2)
                        +
                        COS(%s * pi() / 180)
                        *
                        COS(ABS(location.latitude) * pi() / 180)
                        *
                        POWER(SIN((%s - location.longitude) * pi() / 180 / 2), 2)
                        ) ASC ",
                    $location->latitude,
                    $location->longitude,
                    $location->longitude
                );
            }
        }

        $sqlBase = "SELECT %s CONCAT('P-', practice.id) AS id, practice.name AS name, location.city_name AS city
            FROM practice
            INNER JOIN location
                ON practice.location_id = location.id
            WHERE country_id = 1
                AND localization_id = 1
            %s
            ORDER BY %s
            LIMIT %s;";

        $sqlCalcFoundRows = ($countResults ? "SQL_CALC_FOUND_ROWS" : "");

        $sql = sprintf(
            $sqlBase,
            $sqlCalcFoundRows,
            sprintf(
                " AND %s LIKE CONCAT(%s, '%%')",
                self::replaceMultiple("practice.name"),
                self::replaceMultiple('"' . $q . '"')
            ),
            $order,
            $limit
        );
        $arrResult = Yii::app()->db->createCommand($sql)->queryAll();

        if (empty($arrResult)) {
            $sql = sprintf(
                $sqlBase,
                $sqlCalcFoundRows,
                sprintf(
                    " AND %s LIKE CONCAT('%%', %s)",
                    self::replaceMultiple("practice.name"),
                    self::replaceMultiple('"' . $q . '"')
                ),
                $order,
                $limit
            );
            $arrResult = Yii::app()->db->createCommand($sql)->queryAll();
        }

        if (!$countResults) {
            return $arrResult;
        }

        $count = Yii::app()->db->createCommand("SELECT FOUND_ROWS();")->queryScalar();

        return array($arrResult, $count);
    }

    /**
     * Return hospital Id (practice_id for practice_size=7)
     * @param str $hospitalName
     * @param str $hospitalCity
     */
    public function getHospitalId($hospitalName = '', $hospitalCity = '')
    {
        if (empty($hospitalCity) || empty($hospitalName)) {
            return 0;
        }

        $hospitalName = addslashes($hospitalName);
        $hospitalCity = addslashes($hospitalCity);

        $sql = sprintf(
            "SELECT p.id
            FROM practice AS p
            INNER JOIN location
                ON p.location_id = location.id
            INNER JOIN practice_details AS pd
                ON p.id = pd.practice_id
            WHERE country_id = 1
                AND localization_id = 1
                AND pd.practice_size = '7'
                AND p.name LIKE '%%%s%%'
                AND location.city_name LIKE '%%%s%%'
            LIMIT 1 ;",
            $hospitalName,
            $hospitalCity
        );
        $arrResult = Yii::app()->db->createCommand($sql)->queryRow();
        if (empty($arrResult)) {
            return 0;
        }
        return $arrResult['id'];
    }

    /**
     * Return hospital Name + city Name
     * @param int $hospitalId
     * @return str
     */
    public function findHospitalById($hospitalId = 0)
    {
        if ($hospitalId == 0) {
            return '';
        }

        $sql = sprintf(
            "SELECT p.id, p.name, location.city_name
            FROM practice AS p
            INNER JOIN location
                ON p.location_id = location.id
            WHERE p.id = %d
            LIMIT 1 ;",
            $hospitalId
        );
        $arrResult = Yii::app()->db->createCommand($sql)->queryAll();
        if (empty($arrResult)) {
            return '';
        }

        return $arrResult[0]['name'] . ' At ' . $arrResult[0]['city_name'];
    }

    /**
     * convert a Practice object into a Yext-friendly associative array
     *
     * @deprecated No longer used by internal code and not recommended.
     *
     */
    public function yextConversion()
    {
        $convert = array();

        if (!empty($this->yext_location_id)) {
            $convert['id'] = $this->yext_location_id;
        } else {
            // assign a new ID
            $this->yext_location_id = $this->id;
            $this->save();
            $convert['id'] = $this->yext_location_id;
        }

        $convert['locationName'] = $this->name;
        $convert['address'] = $this->address;
        if (!empty($this->address_2)) {
            $convert['address2'] = $this->address_2;
        }
        $convert['phone'] = preg_replace('/[^0-9]/', '', $this->phone_number);
        $convert['faxPhone'] = preg_replace('/[^0-9]/', '', $this->fax_number);

        $location = $this->location;

        $convert['city'] = (!empty($location->city_name) ? $location->city_name : '');
        $convert['zip'] = (!empty($location->zipcode) ? $location->zipcode : '');
        // warning: this code line returns a random state
        // $convert['state'] = $location->cities->state->abbreviation;

        if (!empty($location->city_id)) {
            $sqlState = sprintf(
                "SELECT state.abbreviation
                FROM city
                LEFT JOIN state
                    ON city.state_id = state.id
                WHERE city.id = %d",
                $location->city_id
            );
            $convert['state'] = Yii::app()->db->createCommand($sqlState)->queryScalar();
        }

        if (empty($convert['state']) || strlen($convert['state']) != 2) {
            // bug-preventing default, can be fixed by Client Services
            $convert['state'] = "NY";
        }

        if (!empty($this->latitude) && !empty($this->longitude)) {
            $convert['displayLat'] = $this->latitude;
            $convert['displayLng'] = $this->longitude;
        }

        if (!empty($this->practiceOfficeHours)) {
            $hourString = "";
            foreach ($this->practiceOfficeHours as $day) {
                $hourString = $hourString . ($day->day_number + 1) . ":" . substr($day->hours, 0, 5) . ":" .
                    substr($day->hours, 11, 5) . ",";
            }
            $convert['hours'] = rtrim($hourString, ",");
        }

        // assign a basic default:
        // 'Health & Medicine > Doctors & Physicians > General Practitioners'
        $convert['categoryIds'] = array(1255);

        if (!empty($this->website)) {
            $convert['websiteUrl'] = $this->website;
        }

        if (!empty($this->description)) {
            $convert['description'] = $this->description;
        }

        // Define photoPaths
        $photoPath['practice'] = Yii::app()->basePath . '/modules/providers/images/uploads/practices/';
        $photoPath['provider'] = Yii::app()->basePath . '/modules/providers/images/uploads/providers/';
        $photoPath['device'] = Yii::app()->basePath . '/modules/providers/images/uploads/devices/';

        $photoCount = 0;
        if (!empty($this->practicePhotos)) {
            $photoSet = array();
            foreach ($this->practicePhotos as $pic) {
                if ($pic->cover) {
                    // skip the cover since we'll use it as the business logo
                    continue;
                }

                // check the file actually exists
                $localPath = $photoPath['practice'] . $pic->photo_path;
                if (file_exists($localPath)) {
                    $photoData = array();
                    $photoData['url'] = AssetsComponent::generateImgSrc(
                        $pic->photo_path,
                        'practices',
                        'MXL',
                        false,
                        false
                    );
                    if (!empty($pic->caption)) {
                        $photoData['description'] = $pic->caption;
                    }
                    $photoSet[] = $photoData;
                }

                $photoCount++;
                if ($photoCount >= 50) {
                    break;
                }
            }
        }

        if ($photoCount < 50) {


            // if we still have room use the provider's photos as the practice's
            $photoSet = array();
            $providers = ProviderPractice::model()->findAll(
                'practice_id=:practiceId ',
                array(':practiceId' => $this->id)
            );
            foreach ($providers as $provider) {
                $providerPhotos = ProviderPhoto::model()->findAll(
                    'provider_id=:providerId',
                    array(':providerId' => $provider->provider_id)
                );
                if ($providerPhotos) {

                    foreach ($providerPhotos as $pic) {
                        // check the file actually exists
                        $localPath = $photoPath['provider'] . $pic->photo_path;

                        if (file_exists($localPath)) {
                            $photoData = array();
                            $photoData['url'] = AssetsComponent::generateImgSrc(
                                $pic->photo_path,
                                'providers',
                                'MXL',
                                false,
                                false
                            );
                            if (!empty($pic->caption)) {
                                $photoData['description'] = $pic->caption;
                            }
                            $photoSet[] = $photoData;
                        }
                        $photoCount++;
                        if ($photoCount >= 50) {
                            break 2;
                        }
                    }
                }
            }
            $convert['photos'] = $photoSet;
        }

        if (!empty($this->practiceVideos)) {
            $videoSet = array();
            foreach ($this->practiceVideos as $vid) {
                $videoSet[] = $vid->embed_code;
            }
            $convert['videoUrls'] = $videoSet;
        }

        $accountDevices = AccountDevice::model()->findAll(
            'practice_id = :practice_id',
            array(':practice_id' => $this->id)
        );

        if (!empty($accountDevices)) {


            foreach ($accountDevices as $device) {
                if (!empty($device->logo_path) && !strpos($device->logo_path, ".gif")) {
                    // check the file actually exists
                    $localPath = $photoPath['device'] . $device->logo_path;
                    if (file_exists($localPath)) {
                        $convert['logo'] = array();
                        $convert['logo']['url'] = AssetsComponent::generateImgSrc(
                            $device->logo_path,
                            'devices',
                            'MXL',
                            false,
                            false,
                            true
                        );
                    }
                }
            }
        }
        if (empty($convert['logo']['url']) && !empty($this->logo_path)) {
            // if there's no ReviewHub, use the practice logo
            // check the file actually exists
            $localPath = $photoPath['practice'] . $this->logo_path;
            if (file_exists($localPath)) {
                $convert['logo'] = array();
                $convert['logo']['url'] = AssetsComponent::generateImgSrc(
                    $this->logo_path,
                    'practices',
                    'MXL',
                    false,
                    false,
                    true
                );
            }
        }

        if (!empty($convert['logo']['url'])) {
            // if there's a logo, it'll do as the Facebook profile picture by default
            $convert['facebookProfilePicture']['url'] = $convert['logo']['url'];
        }

        // but check if we actually have a facebook profile picture
        $facebookCover = '/images/uploads/practices/facebook_cover_' . $this->id;
        // check the file actually exists
        $localPath = Yii::app()->basePath . '/modules/providers' . $facebookCover;
        if (file_exists($localPath)) {
            $convert['facebookProfilePicture']['url'] = 'https://' . Yii::app()->params->servers['dr_pro_hn'] .
                $facebookCover;
        }

        return $convert;
    }

    /**
     * Find practices a provider with account works at
     * @param int $providerId
     * @return array
     */
    public function getAccountProviderPracticeList($providerId)
    {
        $sql = sprintf(
            "SELECT DISTINCT p.id, p.name, p.address_full, p.address, p.address_2
            FROM practice p
            INNER JOIN provider_practice pp
                ON p.id = pp.practice_id
            INNER JOIN account_provider apro
                ON apro.provider_id = pp.provider_id
            INNER JOIN account_practice apra
                ON apra.practice_id = pp.practice_id
            WHERE pp.provider_id = '%s';",
            $providerId
        );

        $arrPractices = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
        if (!empty($arrPractices)) {
            foreach ($arrPractices as $key => $value) {
                $address = trim($value['address']) . ' ' . trim($value['address_2']);
                $arrPractices[$key]['list_address'] = $address;
            }
        }
        return $arrPractices;
    }

    /**
     *
     * Takes the provided parameters to either look up an existing
     * practice or create a new one. Returns the practice (if there
     * was an error, check the return object's getErrors() result
     *
     * @param object $practiceArgs
     * @param array $newPractices this array is added to if practice is new
     * @param boolean $strict Wehther the function validates attributes not
     * required at the model level
     * @return \Practice
     */
    public static function createOrFetchPractice($practiceArgs, &$newPractices, $strict = true)
    {
        $practiceArgs->phone_number = isset($practiceArgs->phone_number) ? $practiceArgs->phone_number : null;

        // ensure name, address, phone_number, and zipcode are all provided
        $practice = new Practice;
        $practice->attributes = (array) $practiceArgs;

        // beforeSave needed explicitly to initialize attributes
        $practice->beforeSave();

        $practiceArgs->zipcode = Location::cleanUpUsZipCodes($practiceArgs->zipcode);
        // ensure info about practice provided is valid
        if (!$practiceArgs->name) {
            $practice->addError('name', 'Name cannot be blank');
        }
        if (!$practiceArgs->address) {
            $practice->addError('address', 'Address cannot be blank');
        }
        if (empty($practiceArgs->zipcode)) {
            $practice->addError('zipcode', 'Zipcode cannot be blank');
        }

        if ($strict && !$practiceArgs->phone_number) {
            $practice->addError('phone_number', 'Phone number cannot be blank');
        }

        // exit early, showing errors
        if ($practice->getErrors()) {
            return $practice;
        }

        // try to look up existing practices instead of creating new one
        $practice = Practice::model()->registrationLookup(
            $practiceArgs->name,
            $practiceArgs->address,
            $practiceArgs->phone_number,
            $practiceArgs->zipcode
        );

        if (!empty($practice)) {
            // just return first match
            $practiceObject = new Practice;
            $practiceObject->attributes = $practice[0];
            $practiceObject->id = $practice[0]['id'];

            return $practiceObject;
        }

        $practice = new Practice;
        $practice->attributes = (array) $practiceArgs;
        // beforeSave needed explicitly to initialize attributes
        $practice->beforeSave();

        // get location id from zip code
        $zipcodeCheck = Location::model()->find('zipcode=:zipcode', array(':zipcode' => $practiceArgs->zipcode));

        // ensure zipcode is valid or return that error along with others
        if (!$zipcodeCheck) {
            $practice->validate();
            $practice->addError('zipcode', 'Zipcode is invalid');
            return $practice;
        }

        $practice->location_id = $zipcodeCheck->id;

        // try saving;
        // note: if this fails, $practice->getErrors() will have details
        if (!$practice->save()) {
            return $practice;
        }

        // this is a new practice, so add to $newPractices list
        $newPractices[] = $practice;

        return $practice;
    }

    /**
     * Deletes the new practices provided (intended for practices JUST
     * created, NOT for existing practices)
     *
     * @param array $newPractices
     */
    public static function deletePractices($newPractices = '')
    {
        if (!empty($newPractices)) {
            foreach ($newPractices as $newPractice) {
                if ($newPractice instanceof Practice) {
                    $newPractice->delete();
                }
            }
        }
    }

    /**
     * Checks a record's ownership
     * @param int $practiceId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($practiceId = 0, $accountId = 0)
    {
        if ($practiceId < 1 || $accountId < 1) {
            return false;
        }
        $sql = sprintf(
            "SELECT account_id
            FROM account_practice
            WHERE account_practice.practice_id = %d
                AND account_id = %d
            LIMIT 1;",
            $practiceId,
            $accountId
        );
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        return !empty($result) ? true : false;
    }

    /**
     * GADM: Data provider for practice autocomplete
     * Searches for practices belonging to organizations, else practices linked
     * to providers, else all practices
     * @param string $q
     * @param string $showAll
     * @return array
     */
    public static function autocompleteGADM($q = '', $showAll = '')
    {

        $condition = $clientJoin = $providerJoin = $order = '';

        // the base sql we'll use
        $sqlBase = "SELECT SQL_NO_CACHE DISTINCT practice.id
            FROM practice
            %s
            %s
            %s
            %s
            LIMIT 20;";

        $clientJoin = 'INNER JOIN account_practice
            ON practice.id = account_practice.practice_id
            INNER JOIN organization_account
            ON organization_account.account_id = account_practice.account_id
            AND `connection` = "O"
            INNER JOIN organization
            ON organization.id = organization_account.organization_id
            AND status = "A" ';

        $providerJoin = 'INNER JOIN provider_practice
            ON practice.id = provider_practice.practice_id ';

        // search for active clients by name
        $condition = sprintf('WHERE practice.name LIKE "%%%s%%" ', addslashes($q));

        $order = 'ORDER BY practice.name ';

        if ($showAll == 'all') {
            // all practices
            $sql = sprintf($sqlBase, '', '', $condition, $order);
            $array = Yii::app()->db->cache(
                Yii::app()->params['cache_short']
            )->createCommand($sql)->queryAll();
        } else {
            // try clients first
            $sql = sprintf($sqlBase, $clientJoin, '', $condition, $order);
            $array = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
            if (!$array) {
                // try practices linked to providers
                $sql = sprintf($sqlBase, '', $providerJoin, $condition, $order);
                $array = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
                if (!$array) {
                    // all practices
                    $sql = sprintf($sqlBase, '', '', $condition, $order);
                    $array = Yii::app()->db->cache(
                        Yii::app()->params['cache_short']
                    )->createCommand($sql)->queryAll();
                }
            }
        }

        // take the array of arrays and build an array we can implode
        $strPractices = [];
        if (!empty($array)) {
            foreach ($array as $practiceId) {
                $strPractices[] = $practiceId['id'];
            }
        }

        if (empty($strPractices)) {
            // nothing to get details about
            return array();
        }

        $strPractices = implode(', ', $strPractices);

        if (!empty($strPractices)) {
            // get details from the ids
            $sql = sprintf(
                "SELECT SQL_NO_CACHE DISTINCT practice.id, practice.name,
                practice.address,
                practice.address_2, city.name AS city_name, state.abbreviation AS
                abbreviation,
                location.zipcode AS zipcode
                FROM practice
                LEFT JOIN location
                ON practice.location_id = location.id
                LEFT JOIN city
                ON location.city_id = city.id
                LEFT JOIN state
                ON city.state_id = state.id
                WHERE practice.id IN (%s)
                ORDER BY practice.name;",
                $strPractices
            );
            return Yii::app()->dbRO->createCommand($sql)->queryAll();
        }

        // couldn't implode (!)
        return array();
    }

    /**
     * Returns a service-type-aware practice phone number
     * @param int $practiceId The practice id
     * @param int $serviceTypeId The service type id
     * @return string
     */
    public static function getServiceTypePhoneNumber($practiceId, $serviceTypeId)
    {
        $practice = Practice::model()->findByPk($practiceId);
        $practiceServiceType = PracticeServiceType::model()->find(
            "practice_id=:practice_id AND service_type_id=:service_type_id",
            array(":practice_id" => $practiceId, ":service_type_id" => $serviceTypeId)
        );

        if (isset($practiceServiceType) && !empty($practiceServiceType->phone_number)) {
            return $practiceServiceType->phone_number;
        }

        return $practice->phone_number;
    }

    /**
     * Determinate whether a practice belongs to an organization that's active and has ProfileSync enabled
     * @param int $practiceId
     * @return bool
     */
    public static function hasProfileSync($practiceId)
    {

        $sql = sprintf(
            "SELECT DISTINCT practice.id AS practice_id
            FROM organization
            INNER JOIN organization_feature
                ON organization.id = organization_feature.organization_id
                AND feature_id = 3
                AND active = 1
            INNER JOIN practice
                ON practice_id = practice.id
            INNER JOIN provider_practice
                ON practice.id = provider_practice.practice_id
            INNER JOIN provider
                ON provider.id = provider_practice.provider_id
            WHERE practice.id = %d
                AND organization.status = 'A'
                AND organization.yext_customer_id IS NOT NULL
                AND practice.country_id = 1
                AND provider.country_id = 1;",
            $practiceId
        );
        return (bool) Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Practice Command: Get all practices that have ProfileSync enabled for active organizations, with NASP enabled
     * for them and their providers
     * We only retrieve practices from the US since that's the only thing Yext accepts
     * @see PracticeCommand.php::actionSyncProfiles
     *
     * @param bool $recentlyChanged
     * @param bool $limit
     *
     * @return array
     */
    public static function getProfileSyncPractices($recentlyChanged = true, $limit = true)
    {

        $sql = sprintf(
            "SELECT DISTINCT organization.id AS organization_id, practice.id AS practice_id,
            yext_customer_id,
            yext_location_id, organization_name, practice.name AS practice_name,
            practice.address AS practice_address, `location`.zipcode, city.name AS city_name,
            `state`.abbreviation AS state_abbreviation, practice.phone_number AS practice_phone_number
            FROM organization
            INNER JOIN organization_feature
                ON organization.id = organization_feature.organization_id
                AND feature_id = 3
                AND active = 1
            INNER JOIN practice
                ON practice_id = practice.id
                AND practice.nasp_date IS NOT NULL
                AND DATE(practice.nasp_date) != '0000-00-00'
            INNER JOIN provider_practice
                ON practice.id = provider_practice.practice_id
            INNER JOIN provider
                ON provider.id = provider_practice.provider_id
                AND provider.nasp_date IS NOT NULL
                AND DATE(provider.nasp_date) != '0000-00-00'
            INNER JOIN location
                ON practice.location_id = location.id
            INNER JOIN city
                ON location.city_id = city.id
            INNER JOIN state
                ON city.state_id = state.id
                %s
            WHERE organization.status = 'A'
                AND organization.yext_customer_id IS NOT NULL
                AND practice.country_id = 1
                AND provider.country_id = 1
            ORDER BY organization.id DESC
            %s;",
            ($recentlyChanged
                ? 'AND (TIMESTAMPDIFF(MINUTE, provider.date_updated, CURRENT_TIMESTAMP()) BETWEEN 0 AND 2
                    OR TIMESTAMPDIFF(MINUTE, practice.date_updated, CURRENT_TIMESTAMP()) BETWEEN 0 AND 2)'
                : ''),
            ($limit ? 'LIMIT 10' : '')
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Practice Command: Get all practices that have ProfileSync enabled for active organizations
     * We only retrieve practices from US, CA, GB and AU since that's the only thing Manta accepts
     * @see Practice.php::mantaSync
     *
     * @return array
     */
    public static function getMantaSyncPractices()
    {

        $sql = sprintf(
            "SELECT DISTINCT organization.id AS organization_id, practice.id AS practice_id,
            organization_name,
            practice.name AS practice_name, practice.address AS practice_address, location.zipcode,
            city.name AS city_name, state.name AS state_name, country.code AS country_code,
            practice.phone_number AS practice_phone_number, practice.email, practice.logo_path,
            practice.description, practice.website
            FROM organization
            INNER JOIN organization_feature
                ON organization.id = organization_feature.organization_id
                AND feature_id = 3
                AND active = 1
            INNER JOIN practice
                ON practice_id = practice.id
                AND practice.country_id IN (1, 15, 39, 78) /* US, CA, GB, AU only */
            INNER JOIN provider_practice
                ON practice.id = provider_practice.practice_id
            INNER JOIN provider
                ON provider.id = provider_practice.provider_id
                AND provider.country_id IN (1, 15, 39, 78) /* US, CA, GB, AU only */
            INNER JOIN location
                ON practice.location_id = location.id
            INNER JOIN city
                ON location.city_id = city.id
            INNER JOIN state
                ON city.state_id = state.id
            INNER JOIN country
                ON state.country_id = country.id
                AND (
                    TIMESTAMPDIFF(MINUTE, provider.date_updated, CURRENT_TIMESTAMP()) BETWEEN 0 AND 10
                    OR TIMESTAMPDIFF(MINUTE, practice.date_updated, CURRENT_TIMESTAMP()) BETWEEN 0 AND 10
                    )
            WHERE organization.status = 'A'
            LIMIT 100;"
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Synchronize all information for a given practice from our database into Yext
     *
     * @deprecated No longer used by internal code and not recommended.
     *
     * @param array $practice
     */
    public static function yextSync($practice)
    {

        $practiceId = $practice['practice_id'];
        $yextCustomerId = $practice['yext_customer_id'];
        $yextLocationId = $practice['yext_location_id'];

        $practiceObj = Practice::model()->findByPk($practiceId);
        if (!$practiceObj) {
            return false;
        }

        $practiceDetailsObj = PracticeDetails::model()->find(
            'practice_id=:practiceId',
            array(':practiceId' => $practiceId)
        );

        // lists: check if the bios enhanced content list and the product list for this practice already
        // exists (we'll overwrite them) to get their ids
        $contentListId = $productListId = null;
        try {
            $arrLists = YextComponent::getLists(array('customer_id' => $yextCustomerId));
        } catch (Exception $e) {
            echo 'Customer not found for practice #' . $practiceId . ' (Yext location ID ' . $yextLocationId . ') - '
                . $e->getMessage() . PHP_EOL;
            return false;
        }
        $arrLists = $arrLists['lists'];

        if (!empty($arrLists)) {
            foreach ($arrLists as $list) {
                if ($list['type'] == 'BIOS') {
                    $contentListId = $list['id'];
                } elseif ($list['type'] == 'PRODUCTS') {
                    $productListId = $list['id'];
                }
            }
        }

        // content list: generate items
        $listItems = self::yextBiosContentList($practiceId);

        // content list: put those items in a section
        $listSection = [];
        $listSection['id'] = ($contentListId ? $contentListId : 'BIOS' . $practiceId);
        $listSection['name'] = 'BIOS';
        if (count($listItems) == 1) {
            // single provider
            $listSection['description'] = 'Meet ' . $listItems[0]['name'];
        } else {
            $listSection['description'] = 'Meet the Doctors';
        }
        $listSection['items'] = $listItems;

        // content list: generate list, put section in it
        $listData = [];
        $listData['customerId'] = $yextCustomerId;
        $listData['id'] = ($contentListId ? $contentListId : 'BIOS' . $practiceId);
        $listData['name'] = 'BIOS';
        $listData['title'] = $listSection['description'];
        $listData['type'] = 'BIOS';
        $listData['publish'] = true;
        $listData['sections'] = $listSection;

        if (!$contentListId) {
            // create bios enhanced content list
            YextComponent::postList($listData);

            $arrLists = YextComponent::getLists(array('customer_id' => $yextCustomerId));
            $arrLists = $arrLists['lists'];

            if (!empty($arrLists)) {
                foreach ($arrLists as $list) {
                    if ($list['type'] == 'BIOS') {
                        $contentListId = $list['id'];
                        break;
                    }
                }
            }
        } else {
            // update bios enhanced content list
            YextComponent::putList($listData);
        }

        // product list: generate items
        $listItems = self::getTreatments($practiceId);

        // product list: put those items in a section
        $listSection = [];
        $listSection['id'] = ($productListId ? $productListId : 'PRODUCTS' . $practiceId);
        $listSection['name'] = 'PRODUCTS';
        $listSection['description'] = 'Services';
        $listSection['items'] = $listItems;

        // product list: generate list, put section in it
        $listData = [];
        $listData['customerId'] = $yextCustomerId;
        $listData['id'] = ($productListId ? $productListId : 'PRODUCTS' . $practiceId);
        $listData['name'] = 'PRODUCTS';
        $listData['title'] = 'Services';
        $listData['type'] = 'PRODUCTS';
        $listData['publish'] = true;
        $listData['sections'] = $listSection;

        if (!$productListId) {
            // create product list
            YextComponent::postList($listData);

            $arrLists = YextComponent::getLists(array('customer_id' => $yextCustomerId));
            $arrLists = $arrLists['lists'];

            if (!empty($arrLists)) {
                foreach ($arrLists as $list) {
                    if ($list['type'] == 'PRODUCTS') {
                        $productListId = $list['id'];
                        break;
                    }
                }
            }
        } else {
            // update product list
            YextComponent::putList($listData);
        }

        // location: perform basic yext conversion
        $yextPractice = $practiceObj->yextConversion();

        // location: basic conversion sets the category to the most basic (1255: 'Health & Medicine > Doctors &
        // Physicians > General Practitioners') so look for something more accurate
        $arrYextCategories = YextSpecialtyMapping::getPracticeCategories($practiceId);
        if ($arrYextCategories) {
            $yextPractice['categoryIds'] = $arrYextCategories;
        }

        $hasOnlineScheduling = self::hasOnlineScheduling($practiceId);
        if ($hasOnlineScheduling) {
            // if practice has online scheduling, use its url as special offer
            $yextPractice['specialOfferUrl'] = 'https://patients.doctor.com/'
                . 'directAppointment?partner=yext&practice=' . $practiceId;
        }

        // use custom message if any, otherwise schedule cta
        $yextPractice['specialOffer'] = ($practiceDetailsObj && !empty($practiceDetailsObj->yext_special_offer)
            ? $practiceDetailsObj->yext_special_offer
            : ($hasOnlineScheduling ? 'Online Booking Available!' : 'Call Now to Schedule!'));

        // location: trim long attributes
        $yextPractice['locationName'] = substr($yextPractice['locationName'], 0, 100);
        $yextPractice['city'] = substr($yextPractice['city'], 0, 80);
        // our query only retrieves practices from the US
        $yextPractice['countryCode'] = 'US';
        // location: add payment options
        $yextPaymentTypes = array();
        $arrPaymentTypes = self::getPaymentTypes($practiceId);
        foreach ($arrPaymentTypes as $paymentType) {
            switch ($paymentType['payment_type_id']) {
                case 1:
                    $yextPaymentTypes[] = 'CASH';
                    break;
                case 2:
                case 3:
                    if (array_search('CHECK', $yextPaymentTypes) === false) {
                        $yextPaymentTypes[] = 'CHECK';
                    }
                    break;
                case 4:
                    $yextPaymentTypes[] = 'VISA';
                    break;
                case 5:
                    $yextPaymentTypes[] = 'MASTERCARD';
                    break;
                case 6:
                    $yextPaymentTypes[] = 'AMERICANEXPRESS';
                    break;
                case 7:
                    $yextPaymentTypes[] = 'DISCOVER';
                    break;
                default:
                    break;
            }
        }
        if (!empty($yextPaymentTypes)) {
            $yextPractice['paymentOptions'] = $yextPaymentTypes;
        }

        // location: add twitter handle
        $twitterHandle = ProviderPracticeListing::model()->find(
            'listing_type_id = 39 AND practice_id = :practiceId AND provider_id' . ' IS NULL',
            array(':practiceId' => $practiceId)
        );
        if ($twitterHandle) {
            $twitterHandle = str_replace(
                array('http://twitter.com/', 'https://twitter.com/'),
                '',
                $twitterHandle['url']
            );
            if (strlen($twitterHandle) <= 15) {
                $yextPractice['twitterHandle'] = $twitterHandle;
            }
        }

        // location: add routable lat/long
        if (
            !empty($yextPractice['displayLat'])
            && !empty($yextPractice['displayLng'])
        ) {
            $yextPractice['routableLat'] = $yextPractice['displayLat'];
            $yextPractice['routableLng'] = $yextPractice['displayLng'];
        }

        // location: add email address
        if (!empty($practiceObj['email'])) {
            $yextPractice['emails'] = $practiceObj['email'];
        }

        // location: add specialties
        $i = 0;
        $arrSpecialties = self::getSpecialties($practiceId);
        if (!empty($arrSpecialties)) {
            $yextSpecialties = array();
            foreach ($arrSpecialties as $specialty) {
                $yextSpecialties[] = $specialty['specialty'];
                $i++;
                if ($i >= 100) {
                    break;
                }
            }
            $yextPractice['specialties'] = $yextSpecialties;
        }

        // location: add services
        $i = 0;
        $arrServices = self::getTreatments($practiceId);
        if (!empty($arrServices)) {
            $yextServices = array();
            foreach ($arrServices as $service) {
                $yextServices[] = $service['name'];
                $i++;
                if ($i >= 100) {
                    break;
                }
            }
            $yextPractice['services'] = $yextServices;
            $yextPractice['keywords'] = $yextServices;
        }

        // location: add languages
        $i = 0;
        $arrLanguages = self::getLanguages($practiceId);
        if (!empty($arrLanguages)) {
            $yextLanguages = array();
            foreach ($arrLanguages as $language) {
                $yextLanguages[] = $language['language'];
                $i++;
                if ($i >= 100) {
                    break;
                }
            }
            $yextPractice['languages'] = $yextLanguages;
        }

        $yextPractice['lists'] = array();

        // location: link to enhanced content list
        if ($contentListId) {
            $yextPractice['lists'][] = array(
                'id' => $contentListId,
                'name' => 'Meet the Doctors',
                'type' => 'BIOS'
            );
        }

        // location: link to product and services list
        if ($productListId) {
            $yextPractice['lists'][] = array(
                'id' => $productListId,
                'name' => 'Services',
                'type' => 'PRODUCTS'
            );
        }

        $yextPractice['customerId'] = $yextCustomerId;
        $yextPractice['id'] = $yextLocationId;
        try {
            YextComponent::putLocation($yextPractice);
        } catch (Exception $e) {
            echo 'Location not found for practice #' . $practiceId . ' (Yext location ID ' . $yextLocationId
                . ', Yext customer ID ' . $yextCustomerId . ' )' . PHP_EOL;
            return false;
        }
        return true;
    }

    /**
     * Find practices associated with 'O'-level Account
     * @param int $ownerAccountId
     * @param bool $order
     * @param bool $cached
     * @param array $limit
     * @param bool $noTelemedicine
     * @return array
     */
    public static function getOwnerPractices(
        $ownerAccountId,
        $order = false,
        $cached = true,
        $limit = null,
        $noTelemedicine = false
    ) {
        // do we want to order the results?
        if ($order) {
            // yes
            $order = 'ORDER BY practice.name';
        } else {
            // no
            $order = '';
        }

        if ($limit) {
            $pager = 'LIMIT ' . (($limit['page'] - 1) * $limit['limit']) . ', ' . $limit['limit'];
        } else {
            $pager = '';
        }

        if ($noTelemedicine) {
            $inner = "INNER JOIN practice_details ON practice_details.practice_id = practice.id";
            $where = "AND practice_details.practice_type_id != 5";
        } else {
            $inner = "";
            $where = "";
        }

        $sql = sprintf(
            "SELECT practice.id, practice.name, address,
            address_2, unpublished, phone_number,
            fax_number, email, practice.friendly_url,
            practice.location_id, city_name, zipcode,
            state.abbreviation AS state, yelp_sync_approved, yelp_business_id,
            nasp_date, country.name AS country, state.name AS state_name,
            website, country.code AS country_code
            FROM practice
            INNER JOIN account_practice
            ON account_practice.practice_id = practice.id
            %s
            LEFT JOIN location
            ON location.id = practice.location_id
            LEFT JOIN city
            ON location.city_id = city.id
            LEFT JOIN state
            ON city.state_id = state.id
            LEFT JOIN country
            ON state.country_id = country.id
            WHERE account_practice.account_id = %d
            %s
            %s
            %s;",
            $inner,
            $ownerAccountId,
            $where,
            $order,
            $pager
        );

        // cached?
        if ($cached) {
            // yes cached
            return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
        } else {
            // no cache
            return Yii::app()->db->createCommand($sql)->queryAll();
        }
    }

    /**
     * Get the payment types a practice accepts, based on its provider's
     * @param int $practiceId
     * @return array
     */
    private static function getPaymentTypes($practiceId)
    {

        $sql = sprintf(
            "SELECT DISTINCT payment_type_id
            FROM provider_payment_type
            INNER JOIN provider_practice
                ON provider_payment_type.provider_id = provider_practice.provider_id
            WHERE provider_practice.practice_id = %d;",
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the specialties and subspecialties associated to a practice. If nothing found, it's provider's
     * @param int $practiceId
     * @return array
     */
    private static function getSpecialties($practiceId)
    {

        // try practice-level specialties
        $sql = sprintf(
            "SELECT DISTINCT CONCAT(specialty.name, ' - ', sub_specialty.name) AS specialty
            FROM practice_specialty
            INNER JOIN sub_specialty
                ON practice_specialty.sub_specialty_id = sub_specialty.id
            INNER JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
            WHERE practice_specialty.practice_id = %d;",
            $practiceId
        );
        $arrSpecialties = Yii::app()->db->createCommand($sql)->queryAll();
        if ($arrSpecialties) {
            return $arrSpecialties;
        }

        $sql = sprintf(
            "SELECT DISTINCT CONCAT(specialty.name, ' - ', sub_specialty.name) AS specialty
            FROM provider_specialty
            INNER JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
            INNER JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
            INNER JOIN provider_practice
                ON provider_specialty.provider_id = provider_practice.provider_id
            WHERE provider_practice.practice_id = %d;",
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the treatments associated to a practice through its provider's - Yelp can handle up to 100
     * @param int $practiceId
     * @return array
     */
    private static function getTreatments($practiceId)
    {

        $sql = sprintf(
            "SELECT DISTINCT treatment.id, LEFT(treatment.name, 100) AS name
            FROM provider_treatment
            INNER JOIN treatment
                ON provider_treatment.treatment_id = treatment.id
            INNER JOIN provider_practice
                ON provider_treatment.provider_id = provider_practice.provider_id
            WHERE provider_practice.practice_id = %d
            LIMIT 100;",
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the languages associated to a practice through its provider's
     * @param int $practiceId
     * @return array
     */
    private static function getLanguages($practiceId)
    {

        $sql = sprintf(
            "SELECT DISTINCT `language`.name AS `language`
            FROM provider_language
            INNER JOIN `language`
                ON provider_language.language_id = language.id
            INNER JOIN provider_practice
                ON provider_language.provider_id = provider_practice.provider_id
            WHERE provider_practice.practice_id = %d;",
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Generate the Bios Enhanced Content List data that will be sent to Yext for each of a practice's providers
     * @param int $practiceId
     * @return array
     */
    private static function yextBiosContentList($practiceId)
    {

        $contentArray = array();

        $practiceProviders = ProviderPractice::model()->findAll(
            'practice_id=:practiceId',
            array(':practiceId' => $practiceId)
        );
        $i = 0;
        foreach ($practiceProviders as $practiceProvider) {

            // get basic provider data
            $provider = Provider::getDetails($practiceProvider->provider_id);
            if (!empty($provider[0])) {
                $provider = $provider[0];
                $contentArray[$i]['id'] = $provider['id'];
                $contentArray[$i]['name'] = trim($provider['provider']);

                // try to set all specialties as title
                $contentArray[$i]['title'] = $provider['specialties'];
                if (strlen($contentArray[$i]['title']) > 60) {
                    // if too long, just use the main specialty's name
                    $specialty = Specialty::model()->findByPk($provider['specialty_id']);
                    $contentArray[$i]['title'] = $specialty->name;
                }

                $contentArray[$i]['description'] = (!empty($provider['bio'])
                    ? $provider['bio']
                    : $provider['brief_bio']);

                if (!empty($provider['photo_path'])) {
                    $contentArray[$i]['photo']['url'] = AssetsComponent::generateImgSrc(
                        $provider['photo_path'],
                        'providers',
                        'MXL',
                        false,
                        false
                    );
                }

                if (!empty($practiceProvider->phone_number)) {
                    $contentArray[$i]['phone'] = $practiceProvider->phone_number;
                }

                if (!empty($practiceProvider->email)) {
                    $contentArray[$i]['email'] = $practiceProvider->email;
                }

                $contentArray[$i]['url'] = 'https://www.doctor.com/' .
                    $provider['friendly_url'];

                $educationCounter = 0;

                // get provider's education
                $arrProviderEducation = ProviderEducation::getDetails($practiceProvider->provider_id);
                foreach ($arrProviderEducation as $providerEducation) {
                    if ($educationCounter == 9) {
                        continue;
                    }
                    $contentArray[$i]['education'][$educationCounter] = substr(
                        $providerEducation['education_type'] . ': ' . $providerEducation['area_of_focus'] . ' - '
                            . $providerEducation['education_name'] . ', ' . $providerEducation['graduation_year'],
                        0,
                        200
                    );
                    $educationCounter++;
                }

                // get provider's residency
                $arrProviderResidency = ProviderResidency::getDetails($practiceProvider->provider_id);
                foreach ($arrProviderResidency as $providerResidency) {
                    if ($educationCounter == 9) {
                        continue;
                    }
                    $contentArray[$i]['education'][$educationCounter] = substr(
                        $providerResidency['residency_type'] . ': ' . $providerResidency['area_of_focus'] . ' - '
                            . $providerResidency['hospital_name']
                            . ($providerResidency['year_completed'] ? ', ' . $providerResidency['year_completed'] : ''),
                        0,
                        200
                    );
                    $educationCounter++;
                }

                $certCounter = 0;

                // get provider's certifications
                $arrProviderCertifications = ProviderCertification::getDetails(
                    $practiceProvider->provider_id
                );
                foreach ($arrProviderCertifications as $providerCertification) {
                    if ($certCounter == 9) {
                        continue;
                    }
                    $contentArray[$i]['certifications'][$certCounter] = substr(
                        $providerCertification['certifying_body'] . ' - ' . $providerCertification['specialty']
                            . ($providerCertification['year_achieved']
                                ? ', ' . $providerCertification['year_achieved']
                                : ''),
                        0,
                        200
                    );
                    $certCounter++;
                }

                // get provider's memberships
                $arrProviderMemberships = ProviderMembership::getDetails($practiceProvider->provider_id);
                foreach ($arrProviderMemberships as $providerMembership) {
                    if ($certCounter == 9) {
                        continue;
                    }
                    $tmpstr = "";

                    if (($providerMembership['position'] . ' - ' . $providerMembership['organization'] .
                        $providerMembership['from_date'] && $providerMembership['to_date']) ==
                        (', ' . $providerMembership['from_date'] . ' - ' . $providerMembership['to_date'])) {
                        $tmpstr = $providerMembership['from_date'];
                    } elseif (($providerMembership['position'] . ' - ' . $providerMembership['organization'] .
                        $providerMembership['from_date'] && $providerMembership['to_date']) ==
                        (', ' . $providerMembership['from_date'])) {
                        $tmpstr = $providerMembership['to_date'];
                    } elseif (($providerMembership['position'] . ' - ' . $providerMembership['organization'] .
                        $providerMembership['from_date'] && $providerMembership['to_date']) ==
                        (', ' . $providerMembership['to_date'])) {
                        $tmpstr = '';
                    }

                    $contentArray[$i]['certifications'][$certCounter] = substr($tmpstr, 0, 200);

                    $certCounter++;
                }

                // get provider's awards
                $arrProviderAwards = ProviderAward::getDetails($practiceProvider->provider_id);
                foreach ($arrProviderAwards as $providerAward) {
                    if ($certCounter == 9) {
                        continue;
                    }
                    $contentArray[$i]['certifications'][$certCounter] = substr(
                        $providerAward['award_name'] . ' - ' . $providerAward['awarding_institution']
                            . ($providerAward['received_years'] ? ', ' . $providerAward['received_years'] : ''),
                        0,
                        200
                    );
                    $certCounter++;
                }

                $serviceCounter = 0;

                // get provider's treatments
                $arrProviderTreatments = ProviderTreatment::getDetails($practiceProvider->provider_id);
                foreach ($arrProviderTreatments as $providerTreatment) {
                    if ($serviceCounter == 9) {
                        continue;
                    }
                    $contentArray[$i]['services'][$serviceCounter] = substr($providerTreatment['name'], 0, 200);
                    $serviceCounter++;
                }

                $i++;

                if ($i >= 100) {
                    break;
                }
            }
        }

        return $contentArray;
    }

    /**
     * Return whether a practice has providers associated that have full online
     * scheduling enabled
     * @param int $practiceId
     * @return bool
     */
    public static function hasOnlineScheduling($practiceId)
    {
        $sql = sprintf(
            'SELECT SQL_NO_CACHE SUM(IF(schedule_mail_enabled = 1 OR pb_user IS NOT NULL, true, false)) AS
            has_full_online_scheduling
            FROM practice
            LEFT JOIN provider_practice
                ON practice.id = provider_practice.practice_id
            LEFT JOIN provider_details
                ON provider_practice.provider_id = provider_details.provider_id
            WHERE practice_id = %d;',
            $practiceId
        );
        // do not add caching here, always needs to be up-to-date
        return (bool) Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Return whether a practice has providers associated that have third party url
     * and gmb syndication is allowed
     * @param int $practiceId
     * @return bool
     */
    public static function hasThirdPartyPoviders($practiceId)
    {
        $sql = sprintf(
            'SELECT SQL_NO_CACHE SUM(IF(ppl.url is NOT NULL AND orf.active =1, true, false)) AS
            has_third_party_providers
            FROM practice p
            LEFT JOIN provider_practice pp
                ON pp.practice_id = p.id
            LEFT JOIN provider pr
                ON pr.id = pp.provider_id
            LEFT JOIN (SELECT * FROM provider_practice_listing WHERE listing_type_id IN (156,157) AND url is NOT NULL )
            ppl
            ON ppl.provider_id = pr.id
            LEFT JOIN (SELECT * FROM organization_feature WHERE feature_id = 26 AND active=1 ) orf
            ON orf.provider_id = pr.id
            WHERE p.id = %d;',
            $practiceId
        );
        // do not add caching here, always needs to be up-to-date
        return (bool) Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Create or match a practice with base on an array based on the Marketplace PUF
     * @param array $facility
     * @param int $sourceCompanyId
     * @return Practice
     */
    public static function createOrMatchMarketplace($facility, $sourceCompanyId)
    {
        $sourceDate = date_create_from_format('Y-m-d', $facility['last_updated_on']);

        if ($facility['npi']) {
            $log = InsuranceMarketplaceLog::model()->find(
                "company_id = :company_id AND npi_id = :npi_id AND practice_id IS NOT NULL ORDER BY id DESC",
                array(
                    ":company_id" => $sourceCompanyId,
                    ":npi_id" => $facility['npi']
                )
            );

            $model = Practice::model()->find("npi_id = :npi_id", array(":npi_id" => $facility['npi']));

            if ($log && $model) {
                $logDate = date_create_from_format('Y-m-d', $log->date_updated);
                if ($sourceDate <= $logDate) {
                    return $model;
                }
            }

            if (!$model) {
                $phone = str_replace(array('/', '-', '(', ')', ' '), '', $facility['addresses'][0]['phone']);
                if ($phone && $phone != '0000000000') {
                    $model = Practice::model()->find(
                        "phone_number = :phone_number AND npi_id IS NULL",
                        array(":phone_number" => $phone)
                    );
                }
            }
            if (!$model) {
                $model = new Practice();
            }
        } else {
            $phone = str_replace(array('/', '-', '(', ')', ' '), '', $facility['addresses'][0]['phone']);
            if ($phone && $phone != '0000000000') {
                $model = Practice::model()->find("phone_number = :phone_number", array(":phone_number" => $phone));
            }

            if ($model) {
                if ($model->location->zipcode != $facility['addresses'][0]['zip']) {
                    $model = new Practice();
                } else {
                    $log = InsuranceMarketplaceLog::model()->find(
                        "company_id = :company_id AND practice_id = :practice_id ORDER BY id DESC",
                        array(
                            ":company_id" => $sourceCompanyId,
                            ":practice_id" => $model->id
                        )
                    );
                    if ($log) {
                        $logDate = date_create_from_format('Y-m-d', $log->date_updated);
                        if ($sourceDate <= $logDate) {
                            return $model;
                        }
                    }
                }
            } else {
                $model = new Practice();
            }
        }

        $phone = str_replace(array('/', '-', '(', ')', ' '), '', $facility['addresses'][0]['phone']);

        $model->localization_id = 1;
        $model->country_id = 1;
        $model->name = $facility['facility_name'];
        $model->npi_id = $facility['npi'] ? $facility['npi'] : null;
        $model->practice_group_id = 1;
        $model->address_full = $facility['addresses'][0]['address'] . " " . $facility['addresses'][0]['address_2'];
        $model->address = $facility['addresses'][0]['address'];
        $model->address_2 = $facility['addresses'][0]['address_2'];
        $model->phone_number = $phone;

        $model->location_id = Location::model()->find('zipcode = :zipcode', array(':zipcode' =>
        $facility['addresses'][0]['zip']))->id;

        $model->save();

        $log = new InsuranceMarketplaceLog();
        $log->company_id = $sourceCompanyId;
        $log->practice_id = $model->id;
        $log->npi_id = $model->npi_id ? $model->npi_id : null;
        $log->date_updated = $facility['last_updated_on'];
        $log->date_processed = date("Y-m-d H:i:s");
        $log->json = json_encode($facility);
        $log->save();

        return $model;
    }

    /**
     * Find out whether a practice belongs to an organization, active or not
     * @param int $practiceId
     * @return mixed
     */
    public static function belongsToOrganization($practiceId)
    {

        $arrAccountPractice = AccountPractice::model()->cache(Yii::app()->params['cache_medium'])->findAll(
            'practice_id=:practiceId',
            array(':practiceId' => $practiceId)
        );

        if (!$arrAccountPractice) {
            // not linked to an account so can't be linked to an organization
            return false;
        }

        // check each account
        foreach ($arrAccountPractice as $accountPractice) {
            // check for owner organizations of this account
            $arrOrganizationAccounts = OrganizationAccount::model()
                ->cache(Yii::app()->params['cache_medium'])
                ->findAll(
                    'account_id=:accountId AND `connection` = "O"',
                    array(':accountId' => $accountPractice->account_id)
                );
            if ($arrOrganizationAccounts) {
                foreach ($arrOrganizationAccounts as $organizationAccount) {
                    // build an array with them
                    $arrOrganizations[] = $organizationAccount->organization_id;
                }
            }
        }

        if (!empty($arrOrganizations)) {
            // return an array with all the organization ids that were linked
            return $arrOrganizations;
        }

        // no account was linked to an organization
        return false;
    }

    public function getPhotoUrl($params = [])
    {
        if (!empty($this->logo_path)) {
            $logoPath = trim($this->logo_path);
        } elseif (!empty($params['logo_path'])) {
            $logoPath = trim($params['logo_path']);
        } else {
            return null;
        }

        $url = 'https://' . Yii::app()->params->servers['dr_ast_hn'] . '/img?p=practices/' . $logoPath . '&w=150';

        if ($params) {
            $querystring = http_build_query($params);
            $url .= '&' . $querystring;
        }

        return $url;
    }

    /**
     * Get partners related to this practice
     * @param int $practiceId
     * @param boolean $onlyIds
     * @return array
     */
    public static function getPartners($practiceId, $onlyIds = true)
    {
        $sql = sprintf(
            "SELECT ap.practice_id, ap.account_id, oa.organization_id, o.organization_name,
                o.partner_site_id, ps.display_name
            FROM account_practice AS ap
                INNER JOIN organization_account AS oa ON oa.account_id = ap.account_id
                INNER JOIN organization AS o ON oa.organization_id = o.id
                INNER JOIN partner_site AS ps ON o.partner_site_id = ps.id
            WHERE ap.practice_id = %d",
            $practiceId
        );
        $result = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();

        if (!empty($result) && $onlyIds) {
            $response = array();
            foreach ($result as $r) {
                $response[$r['partner_site_id']] = $r['partner_site_id'];
            }
            return $response;
        }
        return $result;
    }

    /**
     * Get practice phone number from twilio_number, if not exists from practice
     * or if not exists from provider_practice
     * @param int $practiceId
     * @return string
     */
    public static function getPhoneNumber($practiceId)
    {
        $twilioNumber = TwilioNumber::model()->cache(Yii::app()->params['cache_short'])->find(
            "practice_id = :practiceId AND enabled='1'",
            array(':practiceId' => $practiceId)
        );

        if (!empty($twilioNumber->twilio_number)) {
            return StringComponent::formatPhone($twilioNumber->twilio_number);
        } else {
            $practice = Practice::model()->cache(Yii::app()->params['cache_short'])->findByPk($practiceId);
            if (!empty($practice->phone_number)) {
                return StringComponent::formatPhone($practice->phone_number);
            } else {
                $sql = sprintf(
                    "SELECT provider_practice.phone_number
                    FROM provider_practice
                    INNER JOIN provider
                        ON provider_practice.provider_id = provider.id
                    WHERE provider_practice.practice_id = '%d'
                        AND provider_practice.phone_number <> ''
                    ORDER BY provider_practice.primary_location DESC, provider.docscore DESC
                    LIMIT 1;",
                    $practiceId
                );
                $phoneNumber = Yii::app()->db
                    ->cache(Yii::app()->params['cache_short'])
                    ->createCommand($sql)
                    ->queryScalar();
                if ($phoneNumber != '') {
                    return StringComponent::formatPhone($phoneNumber);
                } else {
                    return StringComponent::formatPhone('');
                }
            }
        }
    }

    /**
     * Return Twilio for a practice in a certain partner
     * @param int $practiceId
     * @param int $partnerId
     * @return string
     */
    public static function getTwilio($practiceId, $partnerId)
    {
        $sql = sprintf(
            'SELECT * FROM twilio_number WHERE practice_id = %d AND twilio_partner_id = %d',
            $practiceId,
            $partnerId
        );
        $result = Yii::app()->db->createCommand($sql)->queryRow();

        return StringComponent::formatPhone(isset($result['twilio_number']) ? $result['twilio_number'] : '');
    }

    /**
     * Create an ingestion job in Yelp with updated data for a practice
     * @param int $practiceId
     * @param array $oldAttributes
     * @param bool $create
     * @return mixed
     */
    public static function yelpSync($practiceId, $oldAttributes = null, $create = false)
    {

        // get practice details
        $practice = self::getDetails($practiceId);
        $practice = $practice[0];

        // get practice object
        $practiceObj = Practice::model()->findByPk($practiceId);

        // we shouldn't have gotten to this point, but do a safety check anyway
        if (!$practiceObj->yelp_sync_approved) {
            return false;
        }

        if ($oldAttributes == null) {
            // core attributes weren't updated so just use them as lookup
            $oldAttributes = $practice;
        }

        // get opening hours
        $arrHours = false;
        if (!empty($practiceObj->practiceOfficeHours)) {
            foreach ($practiceObj->practiceOfficeHours as $day) {
                $times = explode('-', $day->hours);
                // for business information
                $arrHours[] = [
                    'day' => strtolower(TimeComponent::dayNumberToFullWeekday($day->day_number)),
                    'open' => trim($times[0]),
                    'close' => trim($times[1])
                ];
            }
        }

        // get photos
        $arrPhotos = false;
        if (!empty($practiceObj->practicePhotos)) {
            foreach ($practiceObj->practicePhotos as $pic) {
                $photoData = array();
                $photoData['url'] = AssetsComponent::generateImgSrc($pic->photo_path, 'practices', 'MXL', false, false);
                if (!empty($pic->caption)) {
                    $photoData['caption'] = StringComponent::yelpNormalize(substr($pic->caption, 0, 128));
                }
                $arrPhotos[] = $photoData;
            }
        }

        // get categories
        $arrCategories = false;
        $foundCategories = PartnerSiteParams::getPracticeYelpCategories($practiceId);
        if (empty($foundCategories)) {
            $default = "physicians";
            // if any of the providers have a dental specialty, change the default
            foreach ($practiceObj->providers as $provider) {
                foreach ($provider->providerSpecialties as $ps) {
                    $specID = $ps->subSpecialty->specialty_id;
                    if ($specID == '23' || $specID == '24') {
                        $default = "dentists";
                        break;
                    }
                }
            }

            PartnerSiteParams::createPracticeYelpCategory($practiceId, $default);

            $arrCategories[] = $default;
        } else {
            foreach ($foundCategories as $cat) {
                $arrCategories[] = $cat->param_value;
            }
        }

        $yelpBusinessID = $practice['yelp_business_id'];
        if (!$yelpBusinessID) {
            $sql = sprintf(
                "SELECT url
                FROM provider_practice_listing
                WHERE listing_type_id = %d
                    AND practice_id = %d;",
                ListingType::YELP_PROFILE_ID,
                $practiceId
            );
            $urlCandidate = Yii::app()->db->createCommand($sql)->queryScalar();

            if ($urlCandidate) {
                $firstCut = strpos($urlCandidate, "biz/") + 4;
                $secondCut = strpos($urlCandidate, "?");
                if (!$secondCut) {
                    $secondCut = strlen($urlCandidate);
                }
                if ($firstCut && $secondCut) {
                    $yelpBusinessID = substr($urlCandidate, $firstCut, $secondCut - $firstCut);
                }
            }
        }

        // build main array
        $business = [
            "matching_criteria" => [
                "address1" => StringComponent::yelpNormalize($oldAttributes['address']),
                "city" => StringComponent::yelpNormalize($oldAttributes['city']),
                "country" => StringComponent::yelpNormalize($oldAttributes['country_code']),
                "name" => StringComponent::yelpNormalize($oldAttributes['name']),
                "phone" => $oldAttributes['phone_number'],
                "postal_code" => $oldAttributes['zipcode'],
                "state" => $oldAttributes['state_abbreviation'],
            ],
            "options" => [
                "create_if_missing" => false, // important! code later may overwrite
                "use_matching_criteria_for_update" => false,
            ],
            "partner_business_id" => $practiceId,
            "update" => [
                "address1" => StringComponent::yelpNormalize($practice['address']),
                "city" => StringComponent::yelpNormalize($practice['city']),
                "country" => StringComponent::yelpNormalize($practice['country_code']),
                "name" => StringComponent::yelpNormalize($practice['name']),
                "phone" => $practice['phone_number'],
                "postal_code" => $practice['zipcode'],
                "state" => StringComponent::yelpNormalize($practice['state_abbreviation']),
                "categories" => $arrCategories,
                "platform_service" => [
                    "active" => self::hasOnlineScheduling($practiceId),
                    "ownership" => "business",
                    "types" => [
                        [
                            "service_type" => "medical_compliant",
                            "hours" => [
                                [
                                    "day" => "monday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59",
                                ],
                                [
                                    "day" => "tuesday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                                [
                                    "day" => "wednesday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                                [
                                    "day" => "thursday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                                [
                                    "day" => "friday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                                [
                                    "day" => "saturday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                                [
                                    "day" => "sunday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                            ],
                        ],
                    ]
                ]
            ],
        ];

        if ($yelpBusinessID) {
            $business['matching_criteria']['yelp_business_id'] = $yelpBusinessID;
        } elseif ($create) {
            $business['options']['create_if_missing'] = true;
        }

        if (!empty($arrPhotos)) {
            $business['update']['photos'] = $arrPhotos;
        }
        if (!empty($arrHours)) {
            $business['update']['hours'] = $arrHours;
        }

        // do not send empty strings!
        $addressTwoOld = StringComponent::yelpNormalize($oldAttributes['address_2']);
        if ($addressTwoOld) {
            $business['matching_criteria']['address2'] = $addressTwoOld;
        }
        $addressTwoNew = StringComponent::yelpNormalize($practice['address_2']);
        if ($addressTwoNew) {
            $business['update']['address2'] = $addressTwoNew;
        }

        $jobID = YelpComponent::createJob($business);

        return array(
            "jobID" => $jobID,
            "business" => $business
        );
    }

    /**
     * Deactivate scheduling on Yelp and turn off sync
     * @return mixed
     */
    public function deactivateYelp()
    {

        if ($this->yelp_sync_approved != "1") {
            return;
        }

        // get practice details
        $practice = self::getDetails($this->id);
        $practice = $practice[0];

        $this->yelp_sync_approved = 0;
        $this->save();

        // get categories
        $arrCategories = false;
        $foundCategories = PartnerSiteParams::getPracticeYelpCategories($this->id);

        foreach ($foundCategories as $cat) {
            $arrCategories[] = $cat->param_value;
        }

        // build main array
        $business = [
            "matching_criteria" => [
                "address1" => StringComponent::yelpNormalize($practice['address']),
                "address2" => StringComponent::yelpNormalize($practice['address_2']),
                "address3" => "",
                "city" => StringComponent::yelpNormalize($practice['city']),
                "country" => StringComponent::yelpNormalize($practice['country_code']),
                "name" => StringComponent::yelpNormalize($practice['name']),
                "phone" => $practice['phone_number'],
                "postal_code" => $practice['zipcode'],
                "state" => $practice['state_abbreviation'],
                "yelp_business_id" => $practice['yelp_business_id'],
            ],
            "options" => [
                "create_if_missing" => false, // important, don't change without discussion
                "use_matching_criteria_for_update" => false,
            ],
            "partner_business_id" => $this->id,
            "update" => [
                "categories" => $arrCategories, // required field!
                "platform_service" => [
                    "active" => false,
                    "ownership" => "business",
                    "types" => [
                        [
                            "service_type" => "medical_compliant",
                            "hours" => [
                                [
                                    "day" => "monday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59",
                                ],
                                [
                                    "day" => "tuesday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                                [
                                    "day" => "wednesday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                                [
                                    "day" => "thursday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                                [
                                    "day" => "friday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                                [
                                    "day" => "saturday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ],
                                [
                                    "day" => "sunday",
                                    "start" => "00:00:01",
                                    "end" => "23:59:59"
                                ]
                            ]
                        ]
                    ]
                ]
            ]
        ];
        $business = (array) $business;
        return YelpComponent::createJob($business);
    }

    /**
     * Get all practice ids grouped by coverage radius
     * @return array
     */
    public static function getAllByCoverageRadius()
    {
        $sql = sprintf(
            "SELECT GROUP_CONCAT(practice_id SEPARATOR ',') as practices, coverage_radius as radius
            FROM practice_details
            WHERE coverage_radius > 0
            GROUP BY coverage_radius;"
        );
        return Yii::app()->db->cache(7 * Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }

    /**
     * Sync practices to Manta
     */
    public static function mantaSync()
    {

        // authenticate
        $token = MantaComponent::authenticate();

        // Query to find who we need to add or update
        $arrPractices = self::getMantaSyncPractices();

        $arrAdd = $arrEdit = array();
        foreach ($arrPractices as $practice) {

            // is this practice already linked to manta?
            $sql = sprintf(
                "SELECT param_value
                FROM partner_site_params
                WHERE partner_site_id = 35
                    AND param_name = 'existing_manta_id'
                    AND practice_id = %d;",
                $practice['practice_id']
            );
            $mantaExistingId = Yii::app()->db->createCommand($sql)->queryScalar();

            // find links and merge into array
            $sql = sprintf(
                "SELECT g_link.url AS g_link, f_link.url AS f_link, p_link.url AS p_link,
                t_link.url AS t_link
                FROM practice
                LEFT JOIN provider_practice_listing AS g_link
                    ON g_link.practice_id = practice.id
                    AND g_link.provider_id IS NULL
                    AND g_link.listing_type_id = 21 /* Google+ */
                LEFT JOIN provider_practice_listing AS f_link
                    ON f_link.practice_id = practice.id
                    AND f_link.provider_id IS NULL
                    AND f_link.listing_type_id = 16 /* Facebook */
                LEFT JOIN provider_practice_listing AS p_link
                    ON p_link.practice_id = practice.id
                    AND p_link.provider_id IS NULL
                    AND p_link.listing_type_id = 89 /* Pinterest */
                LEFT JOIN provider_practice_listing AS t_link
                    ON t_link.practice_id = practice.id
                    AND t_link.provider_id IS NULL
                    AND t_link.listing_type_id = 39 /* Twitter */
                WHERE practice.id = %d;",
                $practice['practice_id']
            );
            $arrLinks = Yii::app()->db->createCommand($sql)->queryRow();

            $practice = array_merge($practice, (array) $arrLinks);

            if ($mantaExistingId) {

                // update existing practice in Manta
                $practice['existing_manta_id'] = $mantaExistingId;
                $arrEdit[] = $practice;
            } else {

                // create new practice in Manta
                $arrAdd[] = $practice;
            }
        }

        /**
         * Add
         */

        if ($arrAdd) {

            echo 'Got ' . sizeof($arrAdd) . ' addition(s)' . PHP_EOL;

            $payload = array();
            foreach ($arrAdd as $add) {

                // add each practice to the payload
                $payload[] = MantaComponent::buildPayload('add', $add);

                if (strlen(json_encode($payload)) > 1024 * 1024) {
                    // limit is 1024KB per batch
                    array_pop($payload);
                    break;
                }

                if (sizeof($payload) >= 100) {
                    // limit is 100 messages per batch
                    // eventually we should make it so that the rest of the 100 is queued for later
                    break;
                }
            }
            $response = MantaComponent::sendRequest('/api/v1/claims/batch/add', 'POST', $token, ['adds' => $payload]);
            MantaComponent::processInitialResponse($response, false);
        }

        /**
         * Edit
         */

        if ($arrEdit) {

            echo 'Got ' . count($arrEdit) . ' edit(s)' . PHP_EOL;

            $payload = array();
            foreach ($arrEdit as $edit) {

                // add each practice to the payload
                $payload[] = MantaComponent::buildPayload('edit', $edit);

                if (strlen(json_encode($payload)) > 1024 * 1024) {
                    // limit is 1024KB per batch
                    array_pop($payload);
                    break;
                }

                if (sizeof($payload) >= 100) {
                    // limit is 100 messages per batch
                    // eventually we should make it so that the rest of the 100 is queued for later
                    break;
                }
            }

            $response = MantaComponent::sendRequest('/api/v1/claims/batch/edit', 'PUT', $token, ['edits' => $payload]);
            MantaComponent::processInitialResponse($response, false);
        }

        /**
         * Delete
         */

        // query who we'll delete: partners whose feature 3 (profilesync for practice) was deactivated in the last 10m
        // we limit to 100 here since that's our message limit per batch
        $sql = sprintf(
            "SELECT param_value, organization_feature.practice_id
            FROM partner_site_params
            INNER JOIN organization_feature
                ON organization_feature.practice_id = partner_site_params.practice_id
                AND param_name = 'existing_manta_id'
                AND param_value != ''
            WHERE feature_id = 3
                AND active = 0
                AND (
                    TIMESTAMPDIFF(MINUTE, organization_feature.date_added, CURRENT_TIMESTAMP()) BETWEEN 0 AND 10
                    OR TIMESTAMPDIFF(MINUTE, organization_feature.date_updated, CURRENT_TIMESTAMP()) BETWEEN 0 AND 10
                    )
            LIMIT 100;"
        );
        $arrPractices = Yii::app()->db->createCommand($sql)->queryAll();

        // if there are practices to delete
        if (!empty($arrPractices)) {
            $payload = array();
            // go through them
            foreach ($arrPractices as $practice) {
                // adding each one's existing_manta_id to the array
                $payload[] = array('EXISTING_MANTA_ID' => $practice['param_value']);
            }

            // send payload with all practices to be deleted
            $response = MantaComponent::sendRequest(
                '/api/v1/claims/batch/delete',
                'DELETE',
                $token,
                array('deletes' => $payload)
            );
            MantaComponent::processInitialResponse($response, true);
        }
    }

    /**
     * Get the timezone of the location
     * @return array
     */
    public function getTimeZone()
    {
        $sql = sprintf(
            "SELECT DISTINCT t.time_zone
            FROM location l
            INNER JOIN city c
                ON l.city_id = c.id
            INNER JOIN state s
                ON c.state_id = s.id
            INNER JOIN timezone t
                ON s.abbreviation = t.state_code
            WHERE l.id = '%d'
            ORDER BY t.area_code = (
                SELECT LEFT(p.phone_number, 3) as ac
                FROM practice p
                WHERE p.id = '%d'
                ) DESC;",
            $this->location_id,
            $this->id
        );
        $timeZone = Yii::app()->db->createCommand($sql)->queryScalar();

        // default timezone abbreviation
        if (empty($timeZone)) {
            $timeZone = 'EST';
        }

        // Get a valid php time zone
        $phpTimeZone = Timezone::convertToPhpTimeZone($timeZone);

        return array("abbreviation" => $timeZone, "phpTimezone" => $phpTimeZone);
    }

    /**
     * Get all practices with an incorrect verified flag, $verified can be 1 to get the verified practices that must be
     * not verified or 0 for get the practices that are not verified but must be verified
     * @param int $verified
     * @return array
     */
    public static function getPracticesWithIncorrectVerifiedFlag($verified)
    {

        if ($verified == 1) {
            $sql = 'SELECT DISTINCT practice.id
                    FROM practice
                    LEFT JOIN account_practice ON account_practice.practice_id = practice.id
                    LEFT JOIN account ON account.id = account_practice.account_id
                    LEFT JOIN organization_account ON organization_account.account_id = account.id
                        AND organization_account.`connection` = "O"
                    LEFT JOIN organization ON organization.id = organization_account.organization_id
                    WHERE practice.verified = 1
                    AND (((account.date_added < DATE_SUB(NOW(), INTERVAL 90 DAY)
                          OR account.date_signed_in < DATE_SUB(NOW(), INTERVAL 90 DAY)) AND organization.`id` IS NULL)
                          OR organization.`status` NOT IN ("A","P")
                          OR account.id IS NULL
                          OR account.enabled = 0)
                    AND NOT EXISTS (SELECT 1
                                    FROM account_practice
                                    INNER JOIN account ON (account.id = account_practice.account_id
                                        AND account.enabled = 1)
                                    LEFT JOIN organization_account ON (organization_account.account_id = account.id
                                        AND organization_account.`connection` = "O")
                                    LEFT JOIN organization ON organization.id = organization_account.organization_id
                                    WHERE account_practice.practice_id = practice.id
                                    AND (organization.status IN ("A","P")
                                         OR
                                         (account.date_added >= DATE_SUB(NOW(), INTERVAL 90 DAY) OR
                                            account.date_signed_in >= DATE_SUB(NOW(), INTERVAL 90 DAY))))';
        } else {
            $sql = 'SELECT DISTINCT p.id
                    FROM practice p
                    JOIN account_practice ap ON p.id = ap.practice_id
                    JOIN account a ON ap.account_id = a.id and a.enabled = 1
                    LEFT JOIN organization_account oa ON a.id = oa.account_id AND oa.`connection` = "O"
                    LEFT JOIN organization o ON oa.organization_id = o.id AND o.status IN ("A","P")
                    WHERE (p.verified = 0 OR p.verified IS NULL)
                    AND  ((a.date_added >= DATE_SUB(NOW(), INTERVAL 90 DAY) OR
                        a.date_signed_in >= DATE_SUB(NOW(), INTERVAL 90 DAY)) OR o.id is not null)';
        }

        return Yii::app()->db->createCommand($sql)->queryColumn();
    }

    /**
     * Update the score for practice $this
     * Must be called in object context!
     *
     * @param boolean $saveAttributesOnly
     * @return void
     */
    public function updateScore($saveAttributesOnly = false)
    {

        $scoreTotal = 0;
        $arrMissingData = array();

        // look up field that indicates missing fields
        $practiceDetails = PracticeDetails::model()->find('practice_id=:practiceId', array(':practiceId' => $this->id));
        if (!$practiceDetails) {
            $practiceDetails = new PracticeDetails;
            $practiceDetails->practice_id = $this->id;
            $practiceDetails->save();
        }

        // go through possible scores adding up
        for ($i = 1; $i <= 6; $i++) {

            list($thisScore, $missingData) = $this->getPracticeScore($i, $this, $practiceDetails->practice_type_id);

            $scoreTotal += $thisScore;

            if (!empty($missingData)) {
                foreach ($missingData as $missingDataPoint) {
                    $arrMissingData[] = $missingDataPoint;
                }
            }
        }

        if ($this->score != $scoreTotal) {
            $this->score = $scoreTotal;

            if ($saveAttributesOnly) {
                // so we don't call afterSave() twice in a row
                $this->saveAttributes(array('score' => $this->score));
            } else {
                $this->save();
            }
        }

        // build a string with what's missing
        if (!empty($arrMissingData)) {
            $strMissingData = implode(', ', $arrMissingData);
        } else {
            $strMissingData = '';
        }

        // save it if necessary
        if ($practiceDetails->score_missing_data != $strMissingData) {
            $practiceDetails->score_missing_data = $strMissingData;
            $practiceDetails->save();
        }
        return $this->score;
    }

    /**
     * Get Score for given ID ($scoreId) for given practice ($this)
     * Must be called in object context!
     *
     * @param int $scoreId
     * @param Practice $practice
     * @param int $practiceTypeId
     * @return int
     */
    public function getPracticeScore($scoreId, $practice, $practiceTypeId = 1)
    {
        $connection = Yii::app()->db;
        $scoreEarned = false;
        $missingData = array();

        switch ($scoreId) {
            case 1:
                // "Basic Information"
                $sql = sprintf(
                    "SELECT practice.id
                    FROM practice
                    LEFT JOIN practice_details
                        ON practice.id = practice_details.practice_id
                    WHERE practice.id = %d
                        AND (practice.name <> '' AND practice.name IS NOT NULL)
                        AND (practice.description <> '' AND practice.description IS NOT NULL)
                        AND (practice.address <> '' AND practice.address IS NOT NULL)
                        AND (
                            (practice_details.practice_type_id = 1 AND practice.location_id IS NOT NULL)
                            OR
                            practice_details.practice_type_id != 1
                            OR
                            practice_details.practice_type_id IS NULL
                        )
                        AND (practice.website <> '' AND practice.website IS NOT NULL)
                        AND (practice.email <> '' AND practice.email IS NOT NULL)
                        AND (practice.phone_number <> '' AND practice.phone_number IS NOT NULL);",
                    $practice->id
                );
                $scoreEarned = (bool) $connection->createCommand($sql)->queryScalar();

                if (!$scoreEarned) {
                    if (empty($practice->name)) {
                        $missingData[] = 'Name';
                    }
                    if (empty($practice->description)) {
                        $missingData[] = 'Description';
                    }
                    if (empty($practice->address)) {
                        $missingData[] = 'Address';
                    }
                    if ($practiceTypeId == 1 && empty($practice->location_id)) {
                        $missingData[] = 'State, City, Zipcode';
                    }
                    if (empty($practice->website)) {
                        $missingData[] = 'Website';
                    }
                    if (empty($practice->email)) {
                        $missingData[] = 'Email';
                    }
                    if (empty($practice->phone_number)) {
                        $missingData[] = 'Phone';
                    }
                }

                if (!$scoreEarned) {
                    // claimed practices get points anyway
                    $sql = sprintf(
                        "SELECT account_practice.id
                        FROM account_practice
                        INNER JOIN account ON account.id = account_practice.account_id
                        WHERE enabled = '1'
                            AND account_practice.practice_id = %d
                        LIMIT 1;",
                        $this->id
                    );
                    $scoreEarned = (bool) $connection->createCommand($sql)->queryScalar();
                }

                break;
            case 2:
                // "Related Provider"
                $sql = sprintf(
                    "SELECT provider.id
                    FROM provider_practice
                    INNER JOIN provider
                        ON provider_practice.provider_id =  provider.id
                    WHERE provider.suspended = '0'
                        AND practice_id = %d
                    LIMIT 1;",
                    $this->id
                );
                $scoreEarned = (bool) $connection->createCommand($sql)->queryScalar();
                if (!$scoreEarned) {
                    $missingData[] = StringComponent::practiceScoreSectionName(2);
                }
                break;
            case 3:
                // "Office Hours"
                $sql = sprintf("SELECT id FROM practice_office_hour WHERE practice_id = %d LIMIT 1;", $this->id);
                $scoreEarned = (bool) ($connection->createCommand($sql)->queryScalar() != '');
                if (!$scoreEarned) {
                    $missingData[] = StringComponent::practiceScoreSectionName(3);
                }
                break;
            case 4:
                // "Photo"
                $sql = sprintf(
                    "SELECT practice_photo.id FROM practice_photo WHERE practice_id = %d LIMIT 1;",
                    $this->id
                );
                $scoreEarned = (bool) ($connection->createCommand($sql)->queryScalar() > 0);
                if (!$scoreEarned) {
                    $missingData[] = StringComponent::practiceScoreSectionName(4);
                }
                break;
            case 5:
                // "Insurance"
                $sql = sprintf(
                    "SELECT id FROM provider_practice_insurance WHERE practice_id = %d LIMIT 1;",
                    $this->id
                );
                $scoreEarned = (bool) ($connection->createCommand($sql)->queryScalar() > 0);
                if (!$scoreEarned) {
                    $missingData[] = StringComponent::practiceScoreSectionName(5);
                }
                break;
            case 6:
                // "Specialty"
                $sql = sprintf(
                    "SELECT id FROM practice_specialty WHERE practice_id = %d LIMIT 1;",
                    $this->id
                );
                $scoreEarned = (bool) ($connection->createCommand($sql)->queryScalar() > 0);
                if (!$scoreEarned) {
                    $missingData[] = StringComponent::practiceScoreSectionName(6);
                }
                break;
            default:
        }

        if ($scoreEarned) {
            $score = 0;
            switch ($scoreId) {
                case 1:
                    $score = 20;
                    break;
                case 2:
                    $score = 15;
                    break;
                case 3:
                    $score = 15;
                    break;
                case 4:
                    $score = 10;
                    break;
                case 5:
                    $score = 10;
                    break;
                case 6:
                    $score = 10;
                    break;
                default:
            }
            return array($score, $missingData);
        }
        return array(false, $missingData);
    }

    /**
     * Save practice Data
     * @param array $postData
     * @param int $accountId
     * @param array $prohibitedFields -> those fields will be omitted
     * @return array
     */
    public static function savePractice($postData = '', $accountId = 0, $prohibitedFields = array())
    {

        $results['success'] = true;
        $results['error'] = "";

        if (empty($postData)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }
        $practiceId = !empty($postData['id']) ? $postData['id'] : 0;

        if ($practiceId == 0) {
            $auditMsg = 'New Practice #' . $postData['name'];
            $auditAction = 'C';

            $practice = new Practice();
        } else {

            $auditMsg = 'Practice #' . $practiceId . ' (' . $postData['name'] . ')';
            $auditAction = 'U';

            // validate access
            ApiComponent::checkPermissions('Practice', $accountId, $practiceId);

            $practice = Practice::model()->findByPk($practiceId);
        }

        if (empty($postData['location_id'])) {
            $arrAccount = Account::model()->findByPk($accountId);
            $postData['location_id'] = $arrAccount->location_id;
        }
        $countryId = (!isset($postData['country_id']) || empty($postData['country_id'])
            ? $practice->country_id
            : $postData['country_id']);
        $localizationId = (!isset($postData['localization_id']) || empty($postData['localization_id'])
            ? $practice->localization_id
            : $postData['localization_id']);
        if ($countryId == 0 || $localizationId == 0) {
            $account = Account::model()->cache(Yii::app()->params['cache_short'])->find(
                'id=:id',
                array(':id' => $accountId)
            );
            // set localization and country with base on the user's account
            $postData['localization_id'] = $account->localization_id;
            $postData['country_id'] = $account->country_id;
        } else {
            $postData['localization_id'] = $localizationId;
            $postData['country_id'] = $countryId;
        }

        $schema = Practice::model()->getSchema();
        foreach ($schema as $fieldName => $fieldType) {
            // if not prohibitted field
            if (!in_array($fieldName, $prohibitedFields) && isset($postData[$fieldName])) {
                // is posted?
                if ($fieldType == 'string') {
                    $value = trim($postData[$fieldName]);
                } elseif ($fieldType == 'email') {
                    $value = StringComponent::validateEmail($postData[$fieldName]);
                } elseif ($fieldType == 'integer') {
                    $value = (int) $postData[$fieldName];
                } else {
                    $value = $postData[$fieldName];
                }
                $practice->$fieldName = $value;
            }
        }

        $status = false;
        try {
            $status = $practice->save();
        } catch (Exception $e) {
            $status = false;
        }

        if ($status) {

            // Check if the practiceId is empty (it means it was recently created)
            // and the account is not impersonated, then trigger salesforce task
            if (empty($practiceId) && empty(Yii::app()->session['admin_account_id'])) {

                $organization = Organization::getByAccountId($accountId);

                if (!empty($organization['salesforce_id'])) {

                    $sfAccount = Yii::app()->salesforce->getObject("Account", $organization['salesforce_id']);

                    // Check for the Client Success Manager user ID
                    if (!empty($sfAccount['Account_Rep__c'])) {
                        $dueDate = new DateTime('tomorrow');

                        SqsComponent::sendMessage(
                            'salesforceCreateTask',
                            array(
                                'OwnerId' => $sfAccount['Account_Rep__c'],
                                'WhatId' => $organization['salesforce_id'],
                                'Subject' => 'Upsell: Confirm New Practice, ' . $practice->name,
                                'Status' => 'Not Started',
                                'ActivityDate' => $dueDate->format('Y-m-d')
                            )
                        );
                    }
                }
            }

            $results['success'] = true;
            $results['error'] = "";
            $practiceId = $results['practice_id'] = $practice->id;
            ApiComponent::audit($auditAction, $auditMsg, 'practice', null, false, $accountId);

            // if not exists, create new relation, otherwise do nothing
            AccountPractice::associateAccountPractice($accountId, $practiceId);

            // Sync EMR data
            SqsComponent::sendMessage('importAccountEmr', $accountId);

            $results['success'] = true;
            $results['error'] = "";
            $results['practice_id'] = $practiceId;
            $results['data'] = $practice->attributes;
        } else {
            $results['success'] = false;
            $results['error'] = "ERROR_SAVING_PRACTICE";
            $results['practice_id'] = $practiceId;
            $results['data'] = $practice->getErrors();
            ApiComponent::audit($auditAction, 'ERROR: ' . $auditMsg, 'practice', null, false, $accountId);
        }
        return $results;
    }

    /**
     * Return an array with needed data/format for the dashboard
     * @param array $params
     * @param boolean $useCache
     * @return array()
     */
    public static function prepareDataForDashboard($params = array(), $useCache = true)
    {

        $pageSize = $params['pageSize'];
        $practicePageFrom = $params['actualPage'];
        $accountInfo = $params['accountInfo'];
        $accountId = $params['accountId'];

        // How many pages of providers we have?
        $totalProviders =  count($accountInfo['providers']);
        $totalPagesProviders = 1;
        if ($totalProviders > 0) {
            $totalPagesProviders = ceil($totalProviders / $pageSize);
        }

        $arrPractices = array();
        $practiceList = array();
        if (!empty($accountInfo['practices'])) {
            $internalPage = $practicePageFrom - 1;
            $from = $internalPage * $pageSize;
            $to = $from + $pageSize;
            $i = 0;
            foreach ($accountInfo['practices'] as $k => $v) {
                if ($i >= $from && $i < $to) {
                    $practiceList[] = $k;
                }
                $i++;
                if ($i > $to) {
                    break;
                }
            }
            // get practices details
            if (!empty($practiceList)) {
                // Adding full data to the practices
                $criteria = new CDbCriteria();
                $criteria->addInCondition('t.id', $practiceList);
                $criteria->order = 'name ASC';
                $tmpPractices = (array) Practice::model()->findAll($criteria);
                if (!empty($tmpPractices)) {
                    foreach ($tmpPractices as $tmp) {
                        $practiceId = $tmp->id;
                        $arrPractices[$practiceId] = $tmp->attributes;

                        if (!empty($tmp->logo_path)) {
                            $arrPractices[$practiceId]['logo_path'] = AssetsComponent::generateImgSrc(
                                $tmp->logo_path,
                                'practices',
                                'M',
                                true,
                                true
                            );
                        }

                        // get Other Owners
                        $arrPractices[$practiceId]['otherOwners'] = AccountPractice::model()->getOtherOwners(
                            $practiceId,
                            $accountId
                        );

                        // Adding href's
                        $arrPractices[$practiceId]['href']['practicePhotos'] = Yii::app()->createUrl(
                            'myaccount/practicePhotos',
                            array('id' => $practiceId)
                        );
                        $arrPractices[$practiceId]['href']['profile'] = Yii::app()->createUrl(
                            'myaccount/practice',
                            array('id' => $practiceId)
                        );
                        $arrPractices[$practiceId]['href']['reviewReports'] = Yii::app()->createUrl(
                            'myaccount/reviewReports',
                            array('practice_id' => $practiceId)
                        );
                        $arrPractices[$practiceId]['href']['dashboard'] = Yii::app()->createUrl(
                            'myaccount/dashboard',
                            array('practice_id' => $practiceId)
                        );
                        $arrPractices[$practiceId]['href']['onlinePracticeAppointmentRequests'] = Yii::app()->createUrl(
                            'myaccount/dashboard',
                            array('filter_practice' => $practiceId)
                        );
                        $arrPractices[$practiceId]['href']['unlinkPractice'] = Yii::app()->createUrl(
                            'myaccount/unlinkPractice',
                            array('id' => $practiceId)
                        );

                        // Format address, city and phone_number
                        $practiceAddress = trim($tmp->address);
                        if (!empty($tmp->address) && !empty($tmp->address_2)) {
                            $practiceAddress .= ',&nbsp;' . trim($tmp->address_2);
                        }
                        $arrPractices[$practiceId]['practiceAddress'] = $practiceAddress;

                        $location = Location::model()->getLocationDetails($tmp->location_id);
                        $arrPractices[$practiceId]['location'] = '';
                        if (isset($location[0])) {
                            $location = $location[0];
                            $arrPractices[$practiceId]['location'] = Provider::formatProviderLocation($location);
                        }

                        // Find related providers
                        $arrPractices[$practiceId]['hasProviders'] = null;
                        if ($totalPagesProviders == 1) {
                            $arrPractices[$practiceId]['hasProviders'] = ProviderPractice::hasProviders(
                                $accountId,
                                $practiceId
                            );
                        }
                    }
                }
            }
        }
        return $arrPractices;
    }

    /**
     * Fetch the last 1000 practices that were updated (after a given date)
     * @param string $dateUpdated
     * @return array
     */
    public static function getPracticesForRSSFeed($dateUpdated = null)
    {

        // should we fetch only entries updated after certain time?
        $dateUpdatedCondition = '';
        if (!empty($dateUpdated)) {
            // yes, build condition
            $dateUpdatedCondition = sprintf("WHERE practice.date_updated > '%s'", $dateUpdated);
        }

        // build actual query
        $sql = sprintf(
            "SELECT practice.name, practice.friendly_url, practice.description,
            CONCAT(specialty.name, ' - ', sub_specialty.name) AS specialty, practice.address, practice.address_2,
            practice.date_updated, city.name AS city, state.abbreviation AS state
            FROM practice
            INNER JOIN location
                ON practice.location_id = location.id
            INNER JOIN city
                ON location.city_id = city.id
            INNER JOIN state
                ON city.state_id = state.id
            INNER JOIN practice_specialty
                ON practice.id = practice_specialty.practice_id
            INNER JOIN sub_specialty
                ON practice_specialty.sub_specialty_id = sub_specialty.id
            INNER JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
                %s
            GROUP BY practice.id
            ORDER BY practice.date_updated DESC, practice_specialty.id
            LIMIT 1000;",
            $dateUpdatedCondition
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Return the id of all the practices eligible to be sent to apple maps
     * @return CDbDataReader
     */
    public static function getAppleMapsPractices()
    {
        // query that gets the eligible practices
        $query = Yii::app()->db->createCommand(
            "SELECT DISTINCT q.id as id
                FROM practice q
                INNER JOIN
                    location l ON q.location_id = l.id
                INNER JOIN
                    provider_practice pp ON q.id = pp.practice_id
                INNER JOIN
                    provider_specialty ps ON pp.provider_id = ps.provider_id
                INNER JOIN
                    partner_site_entity_match pm ON ps.sub_specialty_id = pm.internal_column_value
                INNER JOIN
                    provider p ON pp.provider_id = p.id
                LEFT JOIN
                    account_practice ap ON q.id = ap.practice_id
                LEFT JOIN
                    organization_account oa ON ap.account_id = oa.account_id AND
                    oa.connection = 'O'
                LEFT JOIN
                    organization o ON oa.organization_id = o.id
                LEFT JOIN
                    provider_rating pr ON
                        q.id = pr.practice_id AND pr.status = 'A'
                        AND pr.partner_site_id = 1 AND pr.provider_id IS NOT NULL
                WHERE
                    pm.partner_site_id = 163
                    AND (p.docscore >= 50 OR pr.id IS NOT NULL)
                    AND (ap.id IS NOT NULL OR q.name NOT LIKE 'Practice%')
                    AND !(ap.id IS NOT NULL AND q.name LIKE 'Practice%')
                    AND (
                        (o.id IS NOT NULL AND o.status <> 'C' AND q.nasp_date IS NOT NULL)
                        OR
                        (o.id IS NULL)
                        OR
                        (o.id IS NOT NULL AND o.status = 'C')
                    )
                    AND q.phone_number IS NOT NULL AND q.phone_number != ''
                    AND l.zipcode != '99999'
                    AND (ap.id IS NOT NULL OR q.address REGEXP '\d{1}')
                    AND q.address NOT LIKE '%#%'
                    AND q.is_test = 0
                    AND q.suspended = 0
                    AND q.unpublished = 0
                    AND p.is_test = 0
                    AND p.suspended = 0"
        );
        return $query->query();
    }

    /**
     * Get practice data to export to Apple Maps
     * @param int $practiceId
     * @return array
     */
    public static function getAppleMapsData($practiceId)
    {

        // query practice info
        $query = Yii::app()->db->createCommand(
            'SELECT p.id as site_code,
            p.name as name,
            "en-US" as locale,
            IF (p.address_2 != "" AND p.address_2 IS NOT NULL,
                CONCAT(p.address, "|#|", p.address_2),
                p.address
            ) as street_address,
            c.name as locality,
            "us" as country_code,
            CONCAT("_", l.zipcode) as postal_code,
            p.phone_number as number,
            "Landline" as type,
            p.fax_number as alternate_phone_number,
            "Fax" as alternate_phone_number_type,
            p.website as home_page,
            p.friendly_url as friendly_url,
            p.description as short_abstract,
            GROUP_CONCAT(DISTINCT CONCAT(
                (CASE WHEN h.day_number = 7 then "Su"
                WHEN h.day_number = 2 then "Tu"
                WHEN h.day_number = 1 then "Mo"
                WHEN h.day_number = 3 then "We"
                WHEN h.day_number = 4 then "Th"
                WHEN h.day_number = 5 then "Fr"
                WHEN h.day_number = 6 then "Sa"
                ELSE null END),
                " ",
                LEFT(h.hours, 5),
                "-",
                IF(LENGTH(h.hours) = 19, LEFT(RIGHT(h.hours, 8), 5), RIGHT(h.hours, 5))
            ) SEPARATOR "; ") as hours,
            p.latitude as latitude,
            p.longitude as longitude,
            "Calculated" as source,
            IF(COUNT(DISTINCT pm.retrieved_column_value) <= 2,
                GROUP_CONCAT(DISTINCT pm.retrieved_column_value),
                IF(pd.practice_type_id = 7 OR p.name LIKE "%hospital%",
                    "health.hospitals",
                    IF(p.name LIKE "%clinic%", "health.clinic", "health")
                )
            ) as category,
            GROUP_CONCAT(DISTINCT ppl.url SEPARATOR "|") as social_media_urls,
            GROUP_CONCAT(DISTINCT pt.name SEPARATOR "|") as payment_methods,
            IF (COUNT(oa.id) > 0, 1, (IF(COUNT(ap.id) > 0, 2, 3))) as confidence,
            CONCAT("/directAppointment?practice=", p.id) as action_links,
            s.name AS state_province,
            IF (o.status = "A", 1, 0) AS authorized_business
            FROM practice p
            INNER JOIN location l ON p.location_id = l.id
            INNER JOIN city c ON l.city_id = c.id
            INNER JOIN state s ON c.state_id = s.id
            INNER JOIN provider_practice pp ON pp.practice_id = p.id
            INNER JOIN provider_specialty ps ON pp.provider_id = ps.provider_id
            INNER JOIN partner_site_entity_match pm ON ps.sub_specialty_id = pm.internal_column_value
            LEFT JOIN practice_details pd ON p.id = pd.practice_id
            LEFT JOIN practice_office_hour h ON h.practice_id = p.id
            LEFT JOIN provider_practice_listing ppl ON ppl.practice_id = p.id AND ppl.provider_id IS NULL
            LEFT JOIN provider_payment_type ppt ON pp.provider_id = ppt.provider_id
            LEFT JOIN payment_type pt ON ppt.payment_type_id = pt.id
            LEFT JOIN account_practice ap ON p.id = ap.practice_id
            LEFT JOIN organization_account oa ON ap.account_id = oa.account_id
            LEFT JOIN organization o ON o.id = oa.organization_id
            WHERE
                p.id = :practice_id AND pm.partner_site_id = :partner_site_id AND
                pm.retrieved_column_name = :retrieved_column_name
            GROUP BY p.id'
        );
        $query->bindValue(':practice_id', $practiceId);
        $query->bindValue(':partner_site_id', 163);
        $query->bindValue(':retrieved_column_name', 'AppleMapsCategoryID');
        return $query->queryRow();
    }

    /**
     * Get practice photos to export to Apple Maps
     * @param int $practiceId
     * @return array
     */
    public static function getAppleMapsPhotos($practiceId)
    {
        // query photos
        $query = Yii::app()->db->createCommand(
            'SELECT p.id as site_code, CONCAT("1", ph.id) as pref_photo_id,
            IF(
                ph.date_added IS NOT NULL AND ph.date_added != "0000-00-00 00:00:00",
                DATE_FORMAT(ph.date_added, "%Y-%m-%dT%TZ"),
                IF(
                    p.date_added IS NOT NULL AND p.date_added != "0000-00-00 00:00:00",
                    DATE_FORMAT(p.date_added, "%Y-%m-%dT%TZ"),
                    DATE_FORMAT("2019-01-01 00:00:01", "%Y-%m-%dT%TZ")
                )
            ) as date_added,
            ph.caption as caption,
            "practices/" as photo_path,
            ph.photo_path as file_name,
            "unspecified" as photo_type,
            ph.cover,
            ph.sort_order,
            0 as photo_group
            FROM practice p
                INNER JOIN practice_photo ph ON p.id = ph.practice_id
            WHERE p.id = ' . $practiceId . ' AND p.logo_path != ph.photo_path
                UNION
                SELECT p.id as site_code, CONCAT("2", ph.id) as pref_photo_id,
                IF(
                    ph.date_added IS NOT NULL AND ph.date_added != "0000-00-00 00:00:00",
                    DATE_FORMAT(ph.date_added, "%Y-%m-%dT%TZ"),
                    IF(
                        p.date_added IS NOT NULL AND p.date_added != "0000-00-00 00:00:00",
                        DATE_FORMAT(p.date_added, "%Y-%m-%dT%TZ"),
                        DATE_FORMAT("2019-01-01 00:00:01", "%Y-%m-%dT%TZ")
                    )
                ) as date_added,
                ph.caption as caption,
                "providers/" as photo_path,
                ph.photo_path as file_name,
                "unspecified" as photo_type,
                ph.cover,
                ph.sort_order,
                1 as photo_group
                FROM practice p
                    INNER JOIN provider_practice pp ON pp.practice_id = p.id AND pp.primary_location = 1
                    INNER JOIN provider_photo ph ON pp.provider_id = ph.provider_id
                    INNER JOIN provider v ON v.id = pp.provider_id
                WHERE p.id = ' . $practiceId . '
                UNION
                    SELECT p.id as site_code, CONCAT("3", p.id) as pref_photo_id,
                    IF(
                        p.date_added IS NOT NULL AND p.date_added != "0000-00-00 00:00:00",
                        DATE_FORMAT(p.date_added, "%Y-%m-%dT%TZ"),
                        DATE_FORMAT("2019-01-01 00:00:01", "%Y-%m-%dT%TZ")
                    ) as date_added,
                    "Practice logo" as caption,
                    "practices/" as photo_path,
                    p.logo_path as file_name,
                    "logo" as photo_type,
                    0 as cover,
                    0 as sort_order,
                    2 as photo_group
                    FROM practice p
                    WHERE p.id = ' . $practiceId . '
                    ORDER BY photo_group ASC, cover = 1 DESC, sort_order ASC'
        );

        return $query->queryAll();
    }

    /**
     * Get practice reviews to export to Apple Maps
     * @param int $practiceId
     * @return array
     */
    public static function getAppleMapsReviews($practiceId)
    {
        // query reviews
        $query = Yii::app()->db->createCommand(
            'SELECT p.id as site_code, pr.id as id,
                DATE_FORMAT(pr.date_added, "%Y-%m-%dT%TZ") as date_added,
                pr.description as review_text,
                pr.title as review_title,
                pr.rating as score,
                5 as scale,
                pr.reviewer_name as profile_name,
                pr.reviewer_name_anon as profile_name_anon
            FROM practice p
                INNER JOIN provider_practice pp
                    ON p.id = pp.practice_id
                INNER JOIN provider_rating pr
                    ON pp.practice_id = pr.practice_id AND pp.provider_id = pr.provider_id
                INNER JOIN provider v
                    ON pp.provider_id = v.id
            WHERE p.id = :practice_id
                AND pr.localization_id = 1
                AND pr.status = "A"
                AND (pr.origin = "D" OR pr.account_device_id IS NOT NULL)
                AND pr.reviewer_name NOT LIKE "%test%"
                AND pr.reviewer_email NOT LIKE "%test%"
                AND pr.title NOT LIKE "%test%"
                AND v.is_test = 0
                AND p.is_test = 0
            ORDER BY date_added DESC'
        );
        $query->bindValue(':practice_id', $practiceId);
        $reviews = $query->queryAll();

        // has reviews?
        // no
        if (!$reviews) {
            // return
            return $reviews;
        }

        // go through the reviews and apply requested checks
        $i = 0;
        // dummy text, title and caption values
        $arrDummyValues = array('null', 'na', 'n/a', 'none', 'undefined');
        while (isset($reviews[$i])) {
            // Remove all leading spaces and other whitespace chars from reviews
            $reviews[$i]["review_text"] = trim($reviews[$i]["review_text"]);

            // Remove embedded newlines from all reviews (e.g. \r\n\r\n\)
            $reviews[$i]["review_text"] = str_replace("\n", "", $reviews[$i]["review_text"]);
            $reviews[$i]["review_text"] = str_replace("\r", "", $reviews[$i]["review_text"]);

            if (ctype_digit($reviews[$i]["review_text"])) {
                // Exclude any reviews whose text field only includes an integer instead of a string (e.g. 8888)
                array_splice($reviews, $i, 1);
            } elseif (str_word_count($reviews[$i]["review_text"]) < 3) {
                // Exclude any reviews whose text field only includes N/A or the like
                array_splice($reviews, $i, 1);
            } elseif (in_array(strtolower($reviews[$i]["review_title"]), $arrDummyValues)) {
                // Exclude any reviews with dummy title
                array_splice($reviews, $i, 1);
            } elseif (substr($reviews[$i]["review_text"], -3) == '...') {
                // Remove any reviews whose text field ends with ellipses (...)
                array_splice($reviews, $i, 1);
            } elseif (strlen($reviews[$i]["review_text"]) != mb_strlen($reviews[$i]["review_text"], 'utf-8')) {
                // Remove any reviews with special characters within it
                array_splice($reviews, $i, 1);
            } elseif (StringComponent::hasBadWords($reviews[$i]["review_text"])) {
                // Remove any reviews with bad words
                array_splice($reviews, $i, 1);
            } else {
                $i++;
            }
        }

        return $reviews;
    }

    /**
     * Gets the basic practice info for a scan from a practice id
     *
     * @param integer $practiceId The practice ID
     * @return array
     */
    public function getForScan($practiceId)
    {
        $sql = "SELECT
            practice.name AS original_name,
            practice.address,
            practice.phone_number,
            practice.friendly_url,
            IF(practice.listing_name_override IS NULL, practice.name, practice.listing_name_override) AS name,
            IF(practice.logo_path IS NULL, practice_photo.photo_path, practice.logo_path) AS photo_path,
            COUNT(provider_rating.id) AS review_number,
            AVG(provider_rating.rating) AS review_average,
            practice.latitude,
            practice.longitude,
            location.zipcode,
            city.name AS city,
            state.name AS state_full,
            state.abbreviation AS state
            FROM practice
            LEFT JOIN location ON practice.location_id = location.id
            LEFT JOIN city ON location.city_id = city.id
            LEFT JOIN state ON city.state_id = state.id
            LEFT JOIN practice_photo
                ON practice_photo.practice_id=practice.id AND practice_photo.cover = 1
            LEFT JOIN provider_rating
                ON (provider_rating.practice_id = practice.id
                AND provider_rating.status = 'A' AND provider_rating.origin = 'D')
            WHERE practice.id = :practice_id
            GROUP BY practice.id";

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValue(':practice_id', $practiceId, PDO::PARAM_INT)
            ->queryAll();
    }

    /**
     * Searches for a practice by non-id parameters. Returns only the first result.
     *
     * @param string $name
     * @param string $address
     * @param string $phone
     * @param string $zip
     * @return array
     */
    public function searchForScan($name, $address, $phone, $zip)
    {
        $sql = "SELECT practice.id
                FROM practice
                    LEFT JOIN location
                        ON practice.location_id=location.id
                WHERE location.zipcode = :zip
                    AND practice.phone_number = :phone
                GROUP BY practice.id
                ORDER BY
                    practice.address LIKE :address DESC,
                    practice.name LIKE :name,
                    practice.listing_name_override LIKE :name2,
                    practice.nasp_date DESC
                LIMIT 1";

        $rawPhone = preg_replace('/\D/', '', $phone);
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValue(':name', $name . "%", PDO::PARAM_STR)
            ->bindValue(':name2', $name . "%", PDO::PARAM_STR)
            ->bindValue(':address', $address . "%", PDO::PARAM_STR)
            ->bindValue(':phone', $rawPhone, PDO::PARAM_STR)
            ->bindValue(':zip', $zip, PDO::PARAM_STR)
            ->queryRow();
    }

    /**
     * Get operatories value for a given Practice
     * @param $practiceId
     * @param $providerId
     * @return mixed
     */
    public static function getOperatories($practiceId)
    {
        $sql = sprintf(
            "SELECT operatories FROM practice WHERE id = %d LIMIT 1",
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get practices with default lat/long (equal to the locations centroid)
     * @return array
     * @deprecated No longer used by internal code and not recommended.
     */
    public static function getPracticesWithDefaultLatLong()
    {
        $sql = sprintf(
            "SELECT practice.id AS id
            FROM practice
                INNER JOIN location
                    ON practice.location_id = location.id
            WHERE
                (practice.latitude = location.latitude AND
                practice.longitude = location.longitude)
            OR
                (practice.latitude IS NULL AND practice.longitude IS NULL)
            ORDER BY practice.id ASC"
        );
        return Yii::app()->db->createCommand($sql)->query();
    }

    /**
     * Get the apple maps practices with Practice at names
     * @return array
     */
    public static function getPracticeAtAppleMapsPractices()
    {
        $sql = sprintf(
            "SELECT DISTINCT q.id AS practice_id, l.zipcode AS zipcode, s.abbreviation AS state_abbreviation,
                c.name AS city_name, q.name AS practice_name, q.address_full, p.first_last, q.phone_number, p.npi_id
            FROM practice q
            INNER JOIN tmp.practices_with_reviews r ON q.id = r.practice_id
            LEFT JOIN tmp.apple_maps_practices_sep a ON a.id = r.practice_id
                INNER JOIN provider_practice pp
                    ON q.id = pp.practice_id
                INNER JOIN provider_specialty ps
                    ON pp.provider_id = ps.provider_id
                INNER JOIN partner_site_entity_match pm
                    ON ps.sub_specialty_id = pm.internal_column_value
                INNER JOIN provider p
                    ON pp.provider_id = p.id
                INNER JOIN location l
                    ON q.location_id = l.id
                INNER JOIN city c
                    ON l.city_id = c.id
                INNER JOIN state s
                    ON c.state_id = s.id
                LEFT JOIN account_practice ap
                    ON q.id = ap.practice_id
            WHERE
                a.id IS NULL AND q.name LIKE 'Practice At%%' AND r.name IS NULL
            ORDER BY q.id ASC, pp.primary_location DESC;"
        );
        return Yii::app()->db->createCommand($sql)->query();
    }

    /**
     * Find and update location data (location_id, latitude, longitude)
     */
    public function updateLocation()
    {
        // If we have a location_id
        if (!empty($this->location_id)) {
            // find new latitude and longitude
            $arrLatLong = Location::getLatitudeLongitude($this->address, $this->location_id, 'array');

            // Round decimals since DB store 6 decimals only and round them causing lat/lng miss match
            $latitude = round($arrLatLong['latitude'], 6);
            $longitude = round($arrLatLong['longitude'], 6);

            // find location
            $location = Location::model()->cache(Yii::app()->params['cache_long'])->findByPk($this->location_id);

            // compare coords between location and practice, update and save only if are different
            if ($location && ($location->latitude != $latitude || $location->longitude != $longitude)) {
                $this->latitude = $latitude;
                $this->longitude = $longitude;
                $this->save();

                $practiceDetails = PracticeDetails::model()->find(
                    "practice_id = :practice_id",
                    array(":practice_id" => $this->id)
                );
                if (!$practiceDetails) {
                    $practiceDetails = new PracticeDetails();
                    $practiceDetails->practice_id = $this->id;
                }

                $practiceDetails->geolocation_date = date('Y-m-d H:i:s');
                $practiceDetails->geolocation_source = $arrLatLong['source'];
                $practiceDetails->geolocation_payload = $arrLatLong['payload'];
                $practiceDetails->save();
            }
        } elseif (!empty((float) $this->latitude) && !empty((float) $this->longitude)) {
            // Location is empty, but we have lat/long ?
            // do a cast convert of lat and long because "0.00000" is treated as not empty
            // Yes, try to get reverse geoLocation

            $response = GeoLocationComponent::reverseLocation(
                $this->latitude,
                $this->longitude,
                $this->address_full,
                'location_id'
            );

            $response = json_decode($response, true);
            if (Common::isTrue($response['success']) && is_numeric($response['data'])) {
                $this->location_id = $response['data'];
                $this->save();
            }
        }
    }

    public function getUrl()
    {
        $url = '';
        if ($this->friendly_url) {
            $url = 'https://' . Yii::app()->params->servers['dr_web_hn'] . $this->friendly_url;
        }

        return $url;
    }

    /**
     * Generates tracked numbers for the practices in a directory
     *
     * @todo move to DirectorySetting model once it reaches production
     */
    public static function cronTrackedNumbers()
    {

        // get directories that need twilio numbers
        $sql = "SELECT DISTINCT twilio_partner.id AS twilio_partner_id, twilio_partner.partner_site_id,
            directory_setting.id AS directory_setting_id
            FROM twilio_partner
            INNER JOIN directory_setting
            ON directory_setting.partner_site_id = twilio_partner.partner_site_id
            WHERE directory_setting.draft = 0
            ORDER BY partner_site_id;";
        $partnerSite = Yii::app()->db->createCommand($sql)->query();

        // for each directory
        foreach ($partnerSite as $ps) {
            // generate tracked numbers for this directory based on their id and their partner_site_id
            self::generateTrackedNumbers($ps['twilio_partner_id'], $ps['partner_site_id'], $ps['directory_setting_id']);
        }
    }

    /**
     * Generate tracked numbers for a given partner site and a given set of practice ids
     * @param int $twilioPartnerId Id of the Twilio partner
     * @param int $partnerSiteId Id of the partner site
     *
     *
     */
    public static function generateTrackedNumbersByPracticeId($twilioPartnerId, $partnerSiteId)
    {
        // do not continue if any argument is empty
        if (empty($twilioPartnerId) || empty($partnerSiteId)) {
            return;
        }

        // Get practices without twilio numbers
        $sql = sprintf(
            "SELECT DISTINCT provider_practice_twilio.practice_id
            FROM dba_tmp.provider_practice_twilio
            LEFT JOIN prd01.twilio_number
            ON (twilio_number.practice_id = provider_practice_twilio.practice_id
            AND twilio_number.provider_id = provider_practice_twilio.provider_id
            AND twilio_number.enabled = 1
            AND twilio_number.is_dynamic = 0
            AND twilio_partner_id = %d)
            AND twilio_number.id IS NULL;",
            $twilioPartnerId
        );
        $directoryPractices = Yii::app()->db->createCommand($sql)->query();

        // go through the practices that need new numbers
        foreach ($directoryPractices as $dp) {

            // output message to the console
            if (php_sapi_name() == 'cli') {
                echo "Practice --" . $dp['practice_id'] . '--' . PHP_EOL;
            }

            // Generate the twilio number for each of them
            $model = Practice::model()->findByPk($dp['practice_id']);
            $model->getTrackedNumbers($dp['practice_id'], $twilioPartnerId, $partnerSiteId);
            $model->saveTrackedNumbers($dp['practice_id'], $twilioPartnerId, $partnerSiteId);
        }
    }

    /**
     * Generate tracked numbers for a given directory
     * @param int $twilioPartnerId Id of the Twilio partner
     * @param int $partnerSiteId Id of the partner site
     * @param int $directorySettingId
     *
     * @todo move to DirectorySetting model once it reaches production
     */
    public static function generateTrackedNumbers($twilioPartnerId, $partnerSiteId, $directorySettingId)
    {

        // Get practices without twilio numbers
        $sql = sprintf(
            "SELECT DISTINCT account_practice.practice_id AS practice_id
            FROM prd01.provider_practice_directory
            INNER JOIN prd01.account_provider
            ON provider_practice_directory.provider_id = account_provider.provider_id
            INNER JOIN prd01.provider_practice
            ON provider_practice.provider_id = account_provider.provider_id
            INNER JOIN prd01.account_practice
            ON (account_practice.practice_id = provider_practice.practice_id
            AND account_provider.account_id = account_practice.account_id)
            LEFT JOIN prd01.twilio_number
            ON (twilio_number.practice_id = account_practice.practice_id
            AND twilio_number.enabled = 1
            AND twilio_number.is_dynamic = 0
            AND twilio_partner_id = %d)
            WHERE provider_practice.primary_location = 1
            AND provider_practice_directory.practice_id IS NULL
            AND provider_practice_directory.directory_setting_id = %d
            AND twilio_number.id IS NULL;",
            $twilioPartnerId,
            $directorySettingId
        );
        $directoryPractices = Yii::app()->db->createCommand($sql)->query();

        // go through the practices that need new numbers
        foreach ($directoryPractices as $dp) {


            // Generate the twilio number for each of them
            $model = Practice::model()->findByPk($dp['practice_id']);
            $model->getTrackedNumbers($dp['practice_id'], $twilioPartnerId, $partnerSiteId);
            $model->saveTrackedNumbers($dp['practice_id'], $twilioPartnerId, $partnerSiteId);
        }
    }

    /**
     * Get tracked numbers for a practice
     * @param int $practiceId
     * @param int $twilioPartnerId
     * @param int $partnerSiteId
     *
     * @return boolean
     *
     * @todo move to DirectorySetting model once it reaches production
     */
    public function getTrackedNumbers($practiceId, $twilioPartnerId, $partnerSiteId)
    {
        ini_set('max_execution_time', 0);

        $this->arrayTwilioNumbers = array();

        if (empty($practiceId) || empty($twilioPartnerId) || empty($partnerSiteId)) {
            // return if we don't have enough parameters
            return false;
        }

        // look up phone number and zipcode for this practice
        $sql = sprintf(
            "SELECT practice.id AS practice_id, location.zipcode, location.city_id, practice.phone_number
            FROM practice
            LEFT JOIN location ON practice.location_id = `location`.id
            WHERE practice.id = %d;",
            $practiceId
        );
        $practice = Yii::app()->db->createCommand($sql)->queryRow();

        // validate (again) that we don't already have a number for this practice for this partner
        $sql = sprintf(
            "SELECT COUNT(1) AS total
            FROM twilio_number
            WHERE provider_id IS NULL
            AND twilio_partner_id = %d
            AND practice_id = %d ",
            $twilioPartnerId,
            $practiceId
        );
        $twNumbers = Yii::app()->db->createCommand($sql)->queryRow();

        // did we find matching numbers?
        if ($twNumbers['total'] != 0) {
            // yes, then leave - they may have been created in the meantime
            return false;
        }

        // try to get a phone number that's similar to the current number
        $jsonTwilioNumber = Practice::actionTwilioSearchByZipCode(null, $practice['phone_number']);
        $arrayTwilioNumber = json_decode($jsonTwilioNumber, true);

        // did we find numbers?
        if ($arrayTwilioNumber['success']) {
            // yes, queue for saving
            $this->arrayTwilioNumbers['number'][] = $arrayTwilioNumber['number'];
        } else {
            // no, so try to get a phone number that's in the practice's zip code
            $jsonTwilioNumber = Practice::actionTwilioSearchByZipCode($practice['zipcode'], null);
            $arrayTwilioNumber = json_decode($jsonTwilioNumber, true);

            // did we find numbers?
            if ($arrayTwilioNumber['success']) {
                // yes, queue for saving
                $this->arrayTwilioNumbers['number'][] = $arrayTwilioNumber['number'];
            } elseif ($arrayTwilioNumber['errorType'] == 1) {
                // no, look up numbers in other zipcodes in the same city
                $sql = sprintf(
                    "SELECT DISTINCT `location`.zipcode AS zipcode
                    FROM location
                    WHERE zipcode != '%s'
                    AND city_id = %d;",
                    $practice['zipcode'],
                    $practice['city_id']
                );
                $nearZipcodes = Yii::app()->db->createCommand($sql)->queryAll();
                $nearTwilioNumbers = null;

                // go through nearby zipcodes
                foreach ($nearZipcodes as $zipcode) {

                    // try to find twilio numbers in nearby zipcode
                    $jsonNearTwilioNumber = Practice::actionTwilioSearchByZipCode(
                        $zipcode['zipcode'],
                        null
                    );
                    $arrayNearTwilioNumber = json_decode($jsonNearTwilioNumber, true);

                    // did we find a number?
                    if ($arrayNearTwilioNumber['success']) {
                        // yes, save it
                        $nearTwilioNumbers = $arrayNearTwilioNumber['number'];
                        // stop looking in nearby zipcodes
                        break;
                    }
                }

                // did we end up finding a number?
                if ($nearTwilioNumbers > 0) {
                    // yes, queue for saving
                    $this->arrayTwilioNumbers['number'][] = $nearTwilioNumbers;
                } else {
                    // No numbers available for this city
                    echo $jsonTwilioNumber;
                    return false;
                }
            } else {
                // Error processing search
                echo $jsonTwilioNumber;
                return false;
            }
        }

        return true;
    }

    /**
     * Creates a twilio number for the practice
     * @param int $practiceId
     * @param int $twilioPartnerId
     * @param int $partnerSiteId
     *
     * @return bool
     *
     * @todo move to DirectorySetting model once it reaches production
     */
    public function saveTrackedNumbers($practiceId, $twilioPartnerId, $partnerSiteId)
    {
        $twilioLogServerAddress = Yii::app()->params->servers['dr_pho_hn'];

        // make sure practice exists
        $practice = Practice::model()->cache(Yii::app()->params['cache_short'])->findByPk($practiceId);
        if (empty($practice)) {
            $this->addError('practice_id', 'Practice does not exist');
            return false;
        }

        // set target name in twilio number
        $targetName = $practice->name;

        // do we have numbers to buy?
        if (is_array($this->arrayTwilioNumbers) && isset($this->arrayTwilioNumbers['number'])) {

            // yes, go through the numbers that we need to buy
            foreach ($this->arrayTwilioNumbers['number'] as $purchasedNumber) {

                // create them in the db
                $newTwilioNumber = new TwilioNumber;
                $newTwilioNumber->id = null;
                $newTwilioNumber->twilio_number = $purchasedNumber;
                $newTwilioNumber->enabled = 0;
                $newTwilioNumber->is_dynamic = 0;
                $newTwilioNumber->practice_id = $practiceId;
                $newTwilioNumber->provider_id = null;
                $newTwilioNumber->twilio_partner_id = $twilioPartnerId;
                $newTwilioNumber->enabled_recording = 0;

                // check organization status
                $sql = sprintf(
                    "SELECT organization.status
                    FROM organization
                    INNER JOIN organization_account ON organization_account.organization_id = organization.id
                    INNER JOIN account_practice ON account_practice.account_id = organization_account.account_id
                    WHERE practice_id = %d
                    ORDER BY `status` = 'A' DESC;",
                    $practiceId
                );
                $organizationActive = Yii::app()->db->createCommand($sql)->queryRow();

                // is it active?
                if (isset($organizationActive['status']) && $organizationActive['status'] == "A") {
                    // yes, enable recording
                    $newTwilioNumber->enabled_recording = 1;
                }

                // instantiate twilio
                $client = new Yii_Services_Twilio();

                // try to save the number in the db
                if ($newTwilioNumber->save()) {

                    $newTwilioNumber->refresh();

                    try {
                        // purchase the selected PhoneNumber
                        $client->account->incoming_phone_numbers->create(array(
                            'PhoneNumber' => $purchasedNumber,
                            'VoiceUrl' => 'https://' . $twilioLogServerAddress . '/twilioLog/start?twilio_number_id=' .
                                $newTwilioNumber->id,
                            'VoiceFallbackUrl' => 'https://' . $twilioLogServerAddress . '/twilioLog/error',
                            'StatusCallback' => 'https://' . $twilioLogServerAddress . '/twilioLog/end',
                            'FriendlyName' => 'FWD (DNI): ' . urlencode($targetName)
                        ));
                    } catch (Exception $e) {

                        return CJavaScript::JsonEncode(array(
                            'success' => false,
                            'errorType' => 2,
                            'error' => "Error processing search: {$e->getMessage()}"
                        ));
                    }

                    // we could create the number in twilio, so enable it and update it
                    $newTwilioNumber->enabled = 1;
                    if (!$newTwilioNumber->save()) {
                        $this->addError("id", "Couldn't save phone number.");
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Search for Twilio phone numbers near a zipcode or another phone number
     *
     * @param int $zipcode
     * @param int $phoneNumber
     * @return string
     */
    public function actionTwilioSearchByZipCode($zipcode, $phoneNumber)
    {

        $numbers = null;
        $searchParams = array();

        $client = new Yii_Services_Twilio();

        if ($phoneNumber != "") {
            $searchParams['NearNumber'] = $phoneNumber;
        } else {
            $searchParams['InPostalCode'] = $zipcode;
        }

        try {

            // Initiate US Local PhoneNumber search with $searchParams list
            $numbers = $client->account->available_phone_numbers->getList('US', 'Local', $searchParams);

            // If we did not find any phone numbers let the user know
            if (sizeof($numbers->available_phone_numbers) == 0) {

                return CJavaScript::JsonEncode(array(
                    'success' => false,
                    'errorType' => 1,
                    'error' => 'No numbers available for purchase'
                ));
            } else {
                foreach ($numbers->available_phone_numbers as $availableNumber) {
                    $purchasedNumber = StringComponent::normalizePhoneNumber($availableNumber->phone_number, true);

                    // check that the number isn't already bought
                    $cnt = (int)TwilioNumber::model()->count(
                        "twilio_number = :number",
                        array(":number" => $purchasedNumber)
                    );
                    $notReserved = (empty($this->arrayTwilioNumbers['number'])
                        || !in_array($purchasedNumber, $this->arrayTwilioNumbers['number']));
                    if ($cnt == 0 && $notReserved) {
                        return CJavaScript::JsonEncode(array(
                            'success' => true,
                            'number' => $purchasedNumber,
                        ));
                    }
                }
                // if all numbers were bought, return an error
                return CJavaScript::JsonEncode(array(
                    'success' => false,
                    'errorType' => 1,
                    'error' => 'No numbers available for purchase'
                ));
            }
        } catch (Exception $e) {

            return CJavaScript::JsonEncode(array(
                'success' => false,
                'errorType' => 2,
                'error' => "Error processing search: {$e->getMessage()}"
            ));
        }
    }

    /**
     * Get the full address string
     *
     * @return string
     */
    public function getFullAddress()
    {
        $address = $this->address_full . ', ' . $this->location->city_name;
        $address .= ' (' . $this->location->zipcode . ')';

        return $address;
    }

    /**
     * Get a partial address string
     * This is the full address except the address line 1
     *
     * @return string
     */
    public function getPartialAddress()
    {
        $address = '';

        if ($this->address_2) {
            $address = $this->address_2 . ', ';
        }

        $address .= $this->location->city_name . ' (' . $this->location->zipcode . ')';

        return $address;
    }

    public function getCityStateText()
    {
        return $this->location->city->name . ', ' . $this->location->city->state->abbreviation;
    }

    public function getProviders()
    {
        $providers = [];
        $providerPractices = ProviderPractice::model()
            ->with('provider')
            ->findAllByAttributes([
                'practice_id' => $this->id
            ]);

        if ($providerPractices) {
            foreach ($providerPractices as $providerPractice) {
                $providers[] = $providerPractice->provider;
            }
        }

        return $providers;
    }

    public function getProvidersNpis()
    {
        $npis = [];
        $providers = $this->getProviders();
        if ($providers) {
            foreach ($providers as $provider) {
                if ($provider->npi_id) {
                    $npis[] = $provider->npi_id;
                }
            }
        }

        return $npis;
    }

    public function getSpecialtiesNames()
    {
        $specialties = self::getSpecialties($this->id);
        $results = [];

        if ($specialties) {
            foreach ($specialties as $specialty) {
                $name = Common::get($specialty, 'specialty');
                if ($name) {
                    $parts = explode(" - ", $name);
                    if (count($parts) >= 2) {
                        $results[] = [
                            "specialty_name" => $parts[0],
                            "sub_specialty_name" => $parts[1]
                        ];
                    } else {
                        $results[] = [
                            "specialty_name" => $parts[0],
                            "sub_specialty_name" => ''
                        ];
                    }
                }
            }
        }

        return $results;
    }

    /**
     * get Practices near me
     * @param array $params
     *
     * @return mixed
     */
    public static function getPracticeNearMe($params = array())
    {
        $latitude = Common::get($params, 'latitude', 40.7128);
        $longitude = Common::get($params, 'longitude', -74.0060);
        $limit = Common::get($params, 'limit', 500);
        $cacheTime = Common::get($params, 'cacheTime', 0);
        $distance = Common::get($params, 'distance', 20); // miles

        $limit = ($limit > 0) ? ' LIMIT ' . $limit : '';

        $sql = sprintf(
            "SELECT distinct practice.id, practice.`name`, practice.latitude, practice.longitude,
                location.city_name, location.zipcode,
                (
                3959 * ACOS(
                    COS(RADIANS(%s)) * COS(RADIANS(practice.latitude)) * COS(RADIANS(practice.longitude)
                    - RADIANS(%s))
                    + SIN(RADIANS(%s)) * SIN(RADIANS(practice.latitude))
                    )
                ) AS distance
            FROM practice
            INNER JOIN location ON location.id = practice.location_id
            inner join account_practice on account_practice.practice_id = practice.id
            HAVING distance < %d
            ORDER BY distance ASC
            %s;",
            $latitude,
            $longitude,
            $latitude,
            $distance,
            $limit
        );

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Get city name and id of all practices related to the specified partner site.
     *
     * @param int $partnerSiteId
     */
    public static function getCitiesByPartnerSite($partnerSiteId)
    {
        $sql = "SELECT DISTINCT location.city_id, location.city_name
            FROM practice
            INNER JOIN account_practice ON account_practice.practice_id = practice.id
            INNER JOIN organization_account ON organization_account.account_id = account_practice.account_id
            INNER JOIN organization ON organization.id = organization_account.organization_id
            INNER JOIN location ON location.id = practice.location_id
            WHERE organization.partner_site_id = :partner_site_id
            ORDER BY location.city_name";

        return Yii::app()->db->createCommand($sql)->queryAll(true, [':partner_site_id' => $partnerSiteId]);
    }

}
