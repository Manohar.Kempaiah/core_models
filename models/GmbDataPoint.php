<?php

Yii::import('application.modules.core_models.models._base.BaseGmbDataPoint');

class GmbDataPoint extends BaseGmbDataPoint
{
    public const BOOKING_URL = 1;
    public const DESCRIPTION = 2;
    public const NAME = 3;
    public const COVER = 4;
    public const ADDRESS = 5;
    public const WEBSITE = 6;
    public const PHONE = 7;
    public const OFFICE_HOURS = 8;
    public const ADDITIONAL_PHOTOS = 9;
    public const LOGO = 10;
    public const INSURANCES = 11;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

}
