<?php

Yii::import('application.modules.core_models.models._base.BaseScanErrorReport');

class ScanErrorReport extends BaseScanErrorReport
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Periods for the dropdown selector
     */
    public static function getScanPeriod()
    {
        $sql = "SELECT MAX(id) AS id, CONCAT(YEAR(date_added),'-',MONTH(date_added)) AS scan_date
                FROM scan_error_report
                GROUP BY YEAR(date_added), MONTH(date_added)
                ORDER BY date_added DESC";
        $arrData = Yii::app()->db
            ->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->queryAll();

        $result = [];
        if (!empty($arrData)) {
            foreach ($arrData as $r) {
                $result[$r['id']] = $r['scan_date'];
            }
        }
        return $result;
    }

    /**
     * Get the last available period
     *
     * @return ScanErrorReport
     */
    public function getLast()
    {
        return $this->find(['order' => 'date_added DESC']);
    }

    /**
     * Get a ScanErrorReport by year-month
     *
     * @param $dateMonth
     *
     * @return ScanErrorReport
     */
    public function getForDateMonth($dateMonth)
    {
        $parts = explode('-', $dateMonth);

        $c = new CDbCriteria();
        $c->addCondition('YEAR(date_added) = :year AND MONTH(date_added) = :month');
        $c->params = [':year' => $parts[0], ':month' => $parts[1]];
        $c->order = 'date_added DESC';

        return $this->find($c);
    }

    /**
     * Get the scan error report for the current month
     *
     * @return ScanErrorReport
     */
    public function getCurrent()
    {
        $date = date("Y-m");
        return $this->getForDateMonth($date);
    }

    /**
     * Determine if the current listing type was reported for the
     * practice belonging to a certain scan error from an organization.
     *
     * @param integer $organizationId
     * @param integer $scanReportId
     * @param mixed $scanResult
     * @param string $type
     * @return boolean
     */
    public function isReported($organizationId, $scanReportId, $scanResult, $type)
    {
        $criteria = new CDbCriteria;

        if ($type === 'V') {
            $criteria->with[] = 'scanResultProvider';
            $criteria->with['scanResultProvider.scanRequestProvider'] = [
                'alias' => 'srpv'
            ];
            $criteria->with['scanResultProvider.scanRequestProvider.scanRequestPractice'] = [
                'alias' => 'srp'
            ];
            $criteria->with['scanResultProvider.scanRequestProvider.scanRequestPractice.scanRequest'] = [
                'alias' => 'sr'
            ];
            $criteria->compare('srpv.provider_id', $scanResult['provider_id']);
            $criteria->compare('scanResultProvider.listing_type_id', $scanResult['listing_type_id']);
        } else {
            $criteria->with[] = 'scanResultPractice';
            $criteria->with['scanResultPractice.scanRequestPractice'] = [
                'alias' => 'srp'
            ];
            $criteria->with['scanResultPractice.scanRequestPractice.scanRequest'] = [
                'alias' => 'sr'
            ];
            $criteria->compare('scanResultPractice.listing_type_id', $scanResult['listing_type_id']);
        }

        $criteria->compare('t.scan_error_report_id', $scanReportId);
        $criteria->compare('sr.organization_id', $organizationId);
        $criteria->compare('srp.practice_id', $scanResult['practice_id']);

        $count = ScanError::model()->count($criteria);

        return ($count > 0);
    }
}
