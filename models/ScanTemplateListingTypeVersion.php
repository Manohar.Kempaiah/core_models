<?php

Yii::import('application.modules.core_models.models._base.BaseScanTemplateListingTypeVersion');

class ScanTemplateListingTypeVersion extends BaseScanTemplateListingTypeVersion
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
