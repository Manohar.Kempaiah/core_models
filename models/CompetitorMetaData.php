<?php

Yii::import('application.modules.core_models.models._base.BaseCompetitorMetaData');

class CompetitorMetaData extends BaseCompetitorMetaData
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getAccountCompetitorHandleFlag($providerId, $accountId)
    {
        return CompetitorMetaData::model()->findByAttributes([
            'entity' => 'provider_account_competitor',
            'entity_id' => $providerId . '-' . $accountId,
            'attribute_name' => 'provider_account_competitor_handled',
            'attribute_value' => 'true'
        ]);
    }

    public function getAccountPracticeCompetitorHandleFlag($practiceId, $accountId)
    {
        return CompetitorMetaData::model()->findByAttributes([
            'entity' => 'practice_account_competitor',
            'entity_id' => $practiceId . '-' . $accountId,
            'attribute_name' => 'practice_account_competitor_handled',
            'attribute_value' => 'true'
        ]);
    }

    public function isAccountProviderHandled($providerId, $accountId)
    {
        $record = $this->getAccountCompetitorHandleFlag($providerId, $accountId);
        return (bool)$record;
    }

    public function isAccountPracticeHandled($practiceId, $accountId)
    {
        $record = $this->getAccountPracticeCompetitorHandleFlag($practiceId, $accountId);
        return (bool)$record;
    }

    public function createPoviderAccountCompetitorHandledFlag($providerId, $accountId = '')
    {
        $cmd = new CompetitorMetaData();
        $cmd->entity = 'provider_account_competitor';
        $cmd->entity_id_attribute = 'provider_account_id';
        $cmd->entity_id = $providerId . '-' . $accountId;
        $cmd->attribute_name = 'provider_account_competitor_handled';
        $cmd->attribute_value = 'true';
        $cmd->data = json_encode([
            'provider_id' => $providerId,
            'account_id' => $accountId
        ]);

        $status = $cmd->save();
        if ($status) {
            return $cmd;
        } else {
            return null;
        }
    }

    public function createPracticeAccountCompetitorHandledFlag($practiceId, $accountId = '')
    {
        $cmd = new CompetitorMetaData();
        $cmd->entity = 'practice_account_competitor';
        $cmd->entity_id_attribute = 'practice_account_id';
        $cmd->entity_id = $practiceId . '-' . $accountId;
        $cmd->attribute_name = 'practice_account_competitor_handled';
        $cmd->attribute_value = 'true';
        $cmd->data = json_encode([
            'practice_id' => $practiceId,
            'account_id' => $accountId
        ]);

        $status = $cmd->save();
        if ($status) {
            return $cmd;
        } else {
            return null;
        }
    }

    public function markAsAutoSuggested($scanCompetitorProviderId, $scanCompetitorPracticeId)
    {
        $cmd = new CompetitorMetaData();
        $cmd->entity = 'scan_competitor_practice_provider';
        $cmd->entity_id_attribute = 'id';
        $cmd->entity_id = $scanCompetitorProviderId . '-' . $scanCompetitorPracticeId;
        $cmd->attribute_name = 'auto_suggested';
        $cmd->attribute_value = 'true';

        $cmd->data = json_encode([
            'scan_competitor_provider_id' => $scanCompetitorProviderId,
            'scan_competitor_practice_id' => $scanCompetitorPracticeId
        ]);

        $status = $cmd->save();
        if ($status) {
            return $cmd;
        } else {
            return null;
        }
    }

    public function markPracticeAsAutoSuggested($scanCompetitorPracticeId)
    {
        $cmd = new CompetitorMetaData();
        $cmd->entity = 'scan_competitor_practice';
        $cmd->entity_id_attribute = 'id';
        $cmd->entity_id = $scanCompetitorPracticeId;
        $cmd->attribute_name = 'auto_suggested';
        $cmd->attribute_value = 'true';

        $cmd->data = json_encode([
            'scan_competitor_practice_id' => $scanCompetitorPracticeId
        ]);

        $status = $cmd->save();
        if ($status) {
            return $cmd;
        } else {
            return null;
        }
    }
}
