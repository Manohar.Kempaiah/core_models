<?php

Yii::import('application.modules.core_models.models._base.BaseSalesforceEntityMapping');

class SalesforceEntityMapping extends BaseSalesforceEntityMapping
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
