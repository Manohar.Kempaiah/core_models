<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeSocialEnhance');

class ProviderPracticeSocialEnhance extends BaseProviderPracticeSocialEnhance
{

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * After Delete
     */
    public function afterDelete()
    {

        // Could i deleted the old SocialEnhanceToken->id?
        SocialEnhanceToken::tryToDelete($this->social_enhance_token_id);
    }

    /**
     * Create or Update provider practice social enhance
     *
     * @param array $eachOne
     *
     * @return array
     */
    public static function createOrUpdateFacebook($eachOne = array())
    {

        $accountId = $eachOne['account_id'];
        $ownerAccountId = $eachOne['owner_account_id'];
        $listingTypeId = 16; // Facebook
        $appType = $eachOne['appType'];
        $objectType = !empty($eachOne['object_type']) ? $eachOne['object_type'] : 'provider';

        $arrFbConfig = array(
            'app_token' => Yii::app()->params['fb_app_token'],
            'app_id' => Yii::app()->params['fb_app_id'],
            'app_secret' => Yii::app()->params['fb_app_secret'],
            'default_graph_version' => Yii::app()->params['fb_default_graph_version']
        );

        // verify the token
        $fbUrl = 'https://graph.facebook.com/me?access_token=' . $eachOne['access_token'];
        $result = @file_get_contents($fbUrl);
        if (empty($result)) {
            $fbResponse['error'] = "Error verifying th token";
        } else {
            $fbResponse = json_decode($result, true);
        }

        if (empty($fbResponse['error'])) {
            require_once(Yii::app()->getBasePath() . '/vendors/facebook/Facebook/autoload.php');
            $fb = new Facebook\Facebook($arrFbConfig);

            // OAuth 2.0 client handler
            $oAuth2Client = $fb->getOAuth2Client();
            // Exchanges a short-lived access token for a long-lived one
            $longLivedAccessToken = $oAuth2Client->getLongLivedAccessToken($eachOne['access_token']);

            $objectTypeCondition = '';
            if ($objectType == 'provider') {
                $providerId = $eachOne['provider_id'];
                $objectTypeCondition = 'AND provider_id=' . $providerId;
            } else {
                $practiceId = $eachOne['practice_id'];
                $objectTypeCondition = 'AND practice_id=' . $practiceId;
            }

            // Search for the providerPracticeSocialEnhance
            $providerPracticeSocialEnhance = ProviderPracticeSocialEnhance::model()->find(
                'account_id=:account_id ' . $objectTypeCondition . ' AND listing_type_id=:listing_type_id ' .
                    'AND app_type=:app_type',
                array(
                    ':account_id' => $ownerAccountId,
                    ':listing_type_id' => $listingTypeId,
                    ':app_type' => $appType,
                )
            );

            // We have saved this token before?
            $socialEnhanceToken = SocialEnhanceToken::model()->find(
                "listing_type_id=:listing_type_id AND profile_id=:profile_id",
                array(
                    ':listing_type_id' => $listingTypeId,
                    ':profile_id' => $eachOne['profile_id']
                )
            );

            // Is the same record?
            if (
                !empty($providerPracticeSocialEnhance) && !empty($socialEnhanceToken)
                && $providerPracticeSocialEnhance->social_enhance_token_id == $socialEnhanceToken->id
            ) {
                // Yes, we didnt make any changes
                return true;
            }

            // create or update SocialEnhanceToken
            if (empty($socialEnhanceToken)) {
                $socialEnhanceToken = new SocialEnhanceToken();
                $socialEnhanceToken->listing_type_id = $listingTypeId;
                $socialEnhanceToken->profile_id = $eachOne['profile_id'];
                $socialEnhanceToken->date_added = TimeComponent::getNow();
                $socialEnhanceToken->profile_name = $eachOne['profile_name'];
            }

            // We store de new onw
            $socialEnhanceToken->access_token = (string) $longLivedAccessToken;
            if (!empty($longLivedAccessToken->getExpiresAt())) {
                $socialEnhanceToken->fb_token_expiration_date =
                    $longLivedAccessToken->getExpiresAt()->format('Y-m-d H:i:s');
            }
            $socialEnhanceToken->date_updated = TimeComponent::getNow();
            $socialEnhanceToken->save();

            // create or update
            $actionAudit = 'U';
            if (empty($providerPracticeSocialEnhance)) {
                $providerPracticeSocialEnhance = new ProviderPracticeSocialEnhance();
                $providerPracticeSocialEnhance->account_id = $ownerAccountId;
                $providerPracticeSocialEnhance->updated_by_account_id = $accountId;
                $providerPracticeSocialEnhance->added_by_account_id = $accountId;
                if ($objectType == 'provider') {
                    $providerPracticeSocialEnhance->provider_id = $providerId;
                } else {
                    $providerPracticeSocialEnhance->practice_id = $practiceId;
                }
                $providerPracticeSocialEnhance->listing_type_id = $listingTypeId;
                $providerPracticeSocialEnhance->app_type = $appType;
                $providerPracticeSocialEnhance->social_enhance_token_id = 0;
                $actionAudit = 'C';
            }
            // Get the old social_enhance_token_id if was changed
            $oldSocialEnhanceTokenId = $providerPracticeSocialEnhance->social_enhance_token_id;

            // Assign the new social_enhance_token_id if was changed
            $providerPracticeSocialEnhance->social_enhance_token_id = $socialEnhanceToken->id;

            $providerPracticeSocialEnhance->updated_by_account_id = $accountId;
            if (empty($providerPracticeSocialEnhance->added_by_account_id)) {
                $providerPracticeSocialEnhance->added_by_account_id = $accountId;
            }

            if ($providerPracticeSocialEnhance->save()) {
                // Could i deleted the old SocialEnhanceToken->id?
                if (
                    $oldSocialEnhanceTokenId > 0
                    && $oldSocialEnhanceTokenId != $providerPracticeSocialEnhance->social_enhance_token_id
                ) {
                    // Yes
                    SocialEnhanceToken::tryToDelete($oldSocialEnhanceTokenId);
                }

                if ($objectType == 'provider') {
                    $auditSection = 'FB-' . $appType . ' account_id:' . $ownerAccountId . ', provider_id:'
                        . $providerId . ', fb_page:' . $eachOne['profile_id'];
                } else {
                    $auditSection = 'FB-' . $appType . ' account_id:' . $ownerAccountId . ', practice_id:'
                        . $practiceId . ', fb_page:' . $eachOne['profile_id'];
                }
                AuditLog::create($actionAudit, $auditSection, 'provider_practice_social_enhance', null, false);

                return $eachOne;
            }
        } else {
            // Is not a valid token
            $eachOne['backend_action'] = 'delete';
        }

        return $eachOne;
    }

    /**
     * Get all providers for this account with their social enhance records
     * ProviderPracticeSocialEnhance::getByAccountId()
     * @param int $ownerAccountId
     * @param int $listingTypeId: 16->Facebook, 39->Twitter
     * @param string $appType
     * @param string $queryType (provider || practice)
     * @return array
     */
    public static function getByAccountId(
        $ownerAccountId = 0,
        $listingTypeId = 0,
        $appType = 'autopublish',
        $queryType = 'provider'
    ) {

        if ($ownerAccountId == 0 || $listingTypeId == 0) {
            return false;
        }

        // Ensure that is the owner
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($ownerAccountId);

        if ($queryType == 'provider') {
            $sql = sprintf(
                "SELECT ap.provider_id, p.first_middle_last AS provider_name, p.last_name, p.first_name,
                ppse.account_id, ppse.added_by_account_id, ppse.updated_by_account_id,
                ppse.social_enhance_token_id, `set`.profile_name, `set`.profile_id
                FROM account_provider AS ap
                LEFT JOIN provider_practice_social_enhance AS ppse
                    ON (
                        ppse.provider_id = ap.provider_id
                        AND ppse.account_id=ap.account_id
                        AND listing_type_id = %d
                        AND ppse.app_type = '%s'
                        )
                LEFT JOIN provider AS p
                    ON ap.provider_id = p.id
                LEFT JOIN social_enhance_token AS `set`
                    ON `set`.id = ppse.social_enhance_token_id
                WHERE ap.account_id = %d
                GROUP BY ap.provider_id
                ORDER BY p.first_middle_last ASC",
                $listingTypeId,
                $appType,
                $ownerAccountId
            );
        } else {

            $sql = sprintf(
                "SELECT ap.practice_id, p.name as practice_name, ppse.social_enhance_token_id,
                `set`.profile_name, `set`.profile_id
                FROM account_practice AS ap
                LEFT JOIN provider_practice_social_enhance AS ppse
                    ON (
                        ppse.practice_id = ap.practice_id
                        AND ppse.account_id=ap.account_id
                        AND listing_type_id = %d
                        AND ppse.app_type = '%s'
                        )
                LEFT JOIN `practice` AS p
                    ON ap.practice_id = p.id
                LEFT JOIN social_enhance_token AS `set`
                    ON `set`.id = ppse.social_enhance_token_id
                WHERE ap.account_id = %d
                ORDER BY p.name ASC",
                $listingTypeId,
                $appType,
                $ownerAccountId
            );
        }
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Make autoPost to Social Networks
     * @param int $providerRatingId
     * @return bool
     */
    public static function makePost($providerRatingId = 0)
    {
        $recProviderRating = ProviderRating::model()->find('id = :id', array(':id' => $providerRatingId));

        if (empty($recProviderRating->partner_site_id)) {
            // We cannot determine if can be shared
            return true;
        }
        // 1: Doctor
        // 63: Site Enhance widget
        $validPartnerSiteId = array(1, 63);
        if (!in_array($recProviderRating->partner_site_id, $validPartnerSiteId)) {
            // It is not our review, so we cannot shared to other sites
            return true;
        }

        $providerId = $recProviderRating->provider_id;
        $accountId = $recProviderRating->account_id;

        // AutoPublish on Twitter -> listingTypeId = 39
        $arrPPSE = ProviderPracticeSocialEnhance::getSocialEnhanceSettings($providerId, $accountId, 39, 'autopublish');

        if (!empty($arrPPSE)) {

            // prepare link and description to share on Twitter
            $urls = StringComponent::prepareUrlToShareReview($recProviderRating, 39);
            $linkToShare = $urls['short'];
            $description = StringComponent::truncate($recProviderRating->description, 110);

            $arrTwConfig = array(
                'tw_consumer_key' => Yii::app()->params['tw_consumer_key'],
                'tw_consumer_secret' => Yii::app()->params['tw_consumer_secret'],
                'tw_outh_callback' => 'https://' . Yii::app()->params->servers['dr_pro_hn']
                    . '/myaccount/manageSocialEnhanceTW'
            );

            spl_autoload_unregister(array('YiiBase', 'autoload'));
            require_once(Yii::app()->getBasePath() . '/vendors/TwitterOAuth/loadClass.php');
            spl_autoload_register(array('YiiBase', 'autoload'));

            foreach ($arrPPSE as $providerPracticeSocialEnhance) {

                $connection = new Abraham\TwitterOAuth\TwitterOAuth(
                    $arrTwConfig['tw_consumer_key'],
                    $arrTwConfig['tw_consumer_secret'],
                    $providerPracticeSocialEnhance['oauth_token'],
                    $providerPracticeSocialEnhance['oauth_token_secret']
                );

                try {
                    // post on behalf of page
                    $connection->post(
                        'statuses/update',
                        array('status' => $description . ' ' . $linkToShare)
                    );
                } catch (Exception $e) {
                    // alert email to tech guys
                    MailComponent::alertAutoPublishSocialEnhanceFail(
                        $recProviderRating,
                        $providerPracticeSocialEnhance,
                        $e
                    );
                    Yii::app()->end();
                }
            }

            if (empty($recProviderRating->share_on_twitter)) {
                // Update the share_on_twitter date
                $recProviderRating->share_on_twitter = date("Y-m-d H:i:s");
                $recProviderRating->save();
            }
        }
        return true;
    }

    /**
     * get settings for provider social Enhance
     * @param int $id (provider or practice id)
     * @param int $ownerAccountId
     * @param int $listingType
     * @param string $appType
     * @param string $queryType (provider || practice)
     * @return array
     */
    public static function getSocialEnhanceSettings(
        $id = 0,
        $ownerAccountId = 0,
        $listingType = 0,
        $appType = 'autopublish',
        $queryType = 'provider'
    ) {
        if ($id == 0 || $listingType == 0) {
            return false;
        }

        $accountCondition = '';
        if (!empty($ownerAccountId)) {
            $accountCondition = sprintf(" AND account_id = %d", $ownerAccountId);
        }
        if ($queryType == 'provider') {
            $sql = sprintf(
                "SELECT ppse.id, first_m_last,
                account_id, added_by_account_id, updated_by_account_id,
                profile_id, profile_name, access_token, oauth_token, oauth_token_secret, auth_expire
                FROM provider_practice_social_enhance AS ppse
                INNER JOIN `provider`
                    ON ppse.provider_id = `provider`.id
                INNER JOIN social_enhance_token AS `set`
                    ON `set`.id = ppse.social_enhance_token_id
                WHERE provider_id = %d
                    %s
                    AND ppse.listing_type_id = %d
                    AND ppse.app_type = '%s'",
                $id,
                $accountCondition,
                $listingType,
                $appType
            );
        } else {

            $sql = sprintf(
                "SELECT ppse.id, practice.`name`,
                profile_id, profile_name, access_token, oauth_token, oauth_token_secret, auth_expire
                FROM provider_practice_social_enhance AS pse
                INNER JOIN `practice` ON ppse.practice_id = `practice`.id
                INNER JOIN social_enhance_token AS `set`
                    ON `set`.id = ppse.social_enhance_token_id
                WHERE practice_id = %d
                    %s
                    AND ppse.listing_type_id = %d
                    AND ppse.app_type = '%s'",
                $id,
                $accountCondition,
                $listingType,
                $appType
            );
        }
        return Yii::app()->db->createCommand($sql)->queryAll();
    }


    /**
     * Update the Twitter settings for the autopublish process
     *
     * @param array $postData
     *
     */
    public static function createOrUpdateTwitter($postData)
    {

        $listingTypeId = 39; // Twitter
        if (empty($postData['selected_page_provider'])) {
            return;
        }

        $accountId = $postData['account_id'];
        $appType = $postData['app_type'];

        // Ensure that is the owner
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);

        foreach ($postData['selected_page_provider'] as $providerId => $twPageId) {

            // Is there a Social Enhance setting for this account - provider - listing type?
            $providerPracticeSocialEnhance = ProviderPracticeSocialEnhance::model()->find(
                'account_id=:account_id AND provider_id=:provider_id
                AND listing_type_id=:listing_type_id AND app_type=:app_type',
                array(
                    ':account_id' => $ownerAccountId,
                    ':provider_id' => $providerId,
                    ':listing_type_id' => $listingTypeId,
                    ':app_type' => $appType
                )
            );

            // The user chooses to unlink a page, so no need to check for auth session data
            if (empty($twPageId) && !empty($providerPracticeSocialEnhance)) {

                $providerPracticeSocialEnhance->delete();

                // the provider has a linked page, so i have to delete
                ApiComponent::audit(
                    'D',
                    'TW-' . $appType,
                    'provider_practice_social_enhance',
                    'Provider_id:' . $providerId,
                    false,
                    $accountId
                );
            } elseif (!empty($twPageId)) {

                // We have saved this token before?
                $socialEnhanceToken = SocialEnhanceToken::model()->find(
                    "listing_type_id=:listing_type_id AND profile_id=:profile_id",
                    array(
                        ':listing_type_id' => $listingTypeId,
                        ':profile_id' => $twPageId
                    )
                );

                // create or update SocialEnhanceToken
                if (empty($socialEnhanceToken)) {
                    $socialEnhanceToken = new SocialEnhanceToken();
                    $socialEnhanceToken->listing_type_id = $listingTypeId;
                    $socialEnhanceToken->profile_id = $twPageId;
                    $socialEnhanceToken->date_added = TimeComponent::getNow();
                    $socialEnhanceToken->profile_name = $postData['tw_data']['profile_name'];
                    $socialEnhanceToken->oauth_token = $postData['tw_data']['oauth_token'];
                    $socialEnhanceToken->oauth_token_secret = $postData['tw_data']['oauth_token_secret'];
                    $socialEnhanceToken->auth_expire = $postData['tw_data']['auth_expires'];
                    $socialEnhanceToken->save();
                }

                // create or update
                $actionAudit = 'U';
                if (empty($providerPracticeSocialEnhance)) {
                    $providerPracticeSocialEnhance = new ProviderPracticeSocialEnhance();
                    $providerPracticeSocialEnhance->account_id = $ownerAccountId;
                    $providerPracticeSocialEnhance->updated_by_account_id = $accountId;
                    $providerPracticeSocialEnhance->added_by_account_id = $accountId;
                    $providerPracticeSocialEnhance->provider_id = $providerId;
                    $providerPracticeSocialEnhance->listing_type_id = $listingTypeId;
                    $providerPracticeSocialEnhance->app_type = $appType;
                    $providerPracticeSocialEnhance->social_enhance_token_id = 0;
                    $actionAudit = 'C';
                }
                // Get the old social_enhance_token_id if was changed
                $oldSocialEnhanceTokenId = $providerPracticeSocialEnhance->social_enhance_token_id;

                // Assign the new social_enhance_token_id if was changed
                $providerPracticeSocialEnhance->social_enhance_token_id = $socialEnhanceToken->id;

                $providerPracticeSocialEnhance->updated_by_account_id = $accountId;
                if (empty($providerPracticeSocialEnhance->added_by_account_id)) {
                    $providerPracticeSocialEnhance->added_by_account_id = $accountId;
                }

                if ($providerPracticeSocialEnhance->save()) {

                    ApiComponent::audit(
                        $actionAudit,
                        'TW-' . $appType,
                        'provider_practice_social_enhance',
                        'Provider_id:' . $providerId . ' - tw_page:' . $postData['tw_data']['profile_name'],
                        false,
                        $accountId
                    );

                    // Could i deleted the old SocialEnhanceToken->id?
                    if (
                        $oldSocialEnhanceTokenId > 0
                        && $oldSocialEnhanceTokenId != $providerPracticeSocialEnhance->social_enhance_token_id
                    ) {
                        // Yes
                        SocialEnhanceToken::tryToDelete($oldSocialEnhanceTokenId);
                    }
                }
            }
        }
        unset($selectedPageProvider);
    }

    /**
     * Return all practices that have Facebook and we need to sync the fan page
     * @param int $practiceId
     * @return array
     */
    public static function getPracticesAbleToBeSyncWithFacebook($practiceId = 0)
    {

        $practiceCondition = '';
        if ($practiceId > 0) {
            $practiceCondition = sprintf(" AND ppse.practice_id = %d", $practiceId);
        }
        $sql = sprintf(
            "SELECT ppse.id as pse_id, ppse.practice_id as practice_id,
            practice.name as practice_name
            FROM provider_practice_social_enhance AS ppse
            INNER JOIN practice
                ON practice.id = ppse.practice_id
            INNER JOIN social_enhance_token
                ON social_enhance_token_id = ppse.social_enhance_token_id
            WHERE ppse.listing_type_id = %d
                AND ppse.app_type = 'listing'
                %s
            GROUP BY ppse.id",
            ListingType::FACEBOOK_ID,
            $practiceCondition
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Try to update facebook fan page
     *
     * @param integer $practiceId
     * @param integer $pseId -> provider_practice_social_enhance->id
     * @return bool
     */
    public static function facebookSync($practiceId = 0, $pseId = 0)
    {

        $practiceDetails = Practice::getDetails($practiceId);
        $practiceDetails = $practiceDetails[0];

        $ppse = ProviderPracticeSocialEnhance::model()->findByPk($pseId);
        // Load settings for Facebook Sync APP
        $arrFbConfig = array(
            'app_token' => Yii::app()->params['fb_app_token'],
            'appsecret_proof' => Yii::app()->params['fb_appsecret_proof'],
            'app_id' => Yii::app()->params['fb_app_id'],
            'app_secret' => Yii::app()->params['fb_app_secret'],
            'default_graph_version' => Yii::app()->params['fb_default_graph_version']
        );

        $arrToPost = [];

        // Get token
        $set = SocialEnhanceToken::model()->findByPk($ppse->social_enhance_token_id);
        $fbAccessToken = $set->access_token;

        if (!empty($fbAccessToken) && !empty($practiceDetails)) {

            // Load Facebook API
            require_once(Yii::app()->getBasePath() . '/vendors/facebook/Facebook/autoload.php');

            $fb = new Facebook\Facebook($arrFbConfig);
            $fbPageId = $set->profile_id;

            // Data that we can get from facebook
            // more info: https://developers.facebook.com/docs/graph-api/reference/v3.2/page/
            // Fields allowed to update: https://developers.facebook.com/docs/graph-api/reference/v3.2/page/#Updating

            $fields = 'name,fan_count,contact_address,can_checkin,can_post,category,category_list,checkins,';
            $fields .= 'connected_instagram_account,country_page_likes,cover,';
            $fields .= 'current_location,description,description_html,display_subtext,displayed_message_response_time,';
            $fields .= 'emails,engagement,featured_video,founded,general_info,general_manager,global_brand_page_name,';
            $fields .= 'global_brand_root_id,has_added_app,has_whatsapp_number,hours,impressum,';
            $fields .= 'instagram_business_account,is_chain,is_owned,is_permanently_closed,is_published,is_unclaimed,';
            $fields .= 'verification_status,is_webhooks_subscribed,link,location,mission,overall_star_rating,';
            $fields .= 'parking,payment_options,phone,place_type,preferred_audience,price_range,privacy_info_url,';
            $fields .= 'products,rating_count,single_line_address,talking_about_count,unread_message_count,';
            $fields .= 'unread_notif_count,unseen_message_count,voip_info,website,were_here_count,whatsapp_number';

            try {

                $fb->setDefaultAccessToken($fbAccessToken);

                try {

                    // $url='https://graph.facebook.com/'.$fbPageId.'?access_token='.$fbAccessToken.'&fields='.$fields;
                    // get fan page setting
                    $response = $fb->get('/' . $fbPageId . '?fields=' . $fields, $fbAccessToken);
                    $fanPage = $response->getGraphNode()->asArray();

                    $arrToPost = [];

                    // Update practice office hours
                    // Get and format practice office hours
                    $practiceOfficeHours = PracticeOfficeHour::getOfficeHours($practiceId);
                    if (!empty($practiceOfficeHours)) {
                        $hours = [];
                        foreach ($practiceOfficeHours as $eachDay) {
                            if (Common::isTrue($eachDay['selected'])) {
                                $day = strtolower($eachDay['day']) . '_1_';
                                list($from, $to) = explode('-', $eachDay['timeRange']);
                                $hours[$day . 'open'] = date("H:i", strtotime($from));
                                $hours[$day . 'close'] = date("H:i", strtotime($to));
                            }
                        }
                        if (!empty($hours)) {
                            $arrToPost['hours'] = $hours;
                        }
                    }

                    // Is this a business account?
                    if (empty($fanPage['location'])) {
                        // Yes, we cannot update this master account, we can update each store
                        $queueBody['practice_id'] = $practiceId;
                        $queueBody['facebookPageName'] = $set->profile_name;
                        $queueBody['errorDescription'] = 'We cannot update a business account ';

                        $result['success'] = false;
                        $result['error'] = 'We cannot update a business account ';
                        $result['queueBody'] = $queueBody;
                        $result['arrToPost'] = [];
                    } else {
                        $location = $fanPage['location'];
                        $location['street'] = !empty($location['street']) ? $location['street'] : '';

                        // Compare Address
                        if (
                            $practiceDetails['address'] != $location['street']
                            || $practiceDetails['city'] != $location['city']
                            || $practiceDetails['zipcode'] != $location['zip']
                        ) {
                            $location = [
                                "city" => $practiceDetails['city'],
                                "country" => $practiceDetails['country_name'],
                                // We don't need to send Lat/Long to FB because are calculated automatically
                                // "latitude" => $practiceDetails['latitude'],
                                // "longitude" => $practiceDetails['longitude'],
                                "street" => $practiceDetails['address'],
                                "zip" => $practiceDetails['zipcode']
                            ];

                            if ($practiceDetails['country_name'] == 'USA') {
                                // if country is USA, state is mandatory
                                $location["country"] = 'United States';
                                $location["state"] = $practiceDetails['state'];
                            }

                            $arrToPost['ignore_coordinate_warnings'] = true;
                            $arrToPost['location'] = json_decode(json_encode($location));
                        }
                    }

                    // Compare email
                    $fbEmail = (!empty($fanPage['emails'][0]) ? $fanPage['emails'][0] : []);
                    if (!empty($practiceDetails['email']) && $practiceDetails['email'] != $fbEmail) {
                        $arrToPost['emails'][] = $practiceDetails['email'];
                    }
                    // Compare phone
                    $fbPhone = (!empty($fanPage['phone'])
                        ? StringComponent::normalizePhoneNumber($fanPage['phone'])
                        : '');

                    // Practice Phone number
                    $phone = StringComponent::normalizePhoneNumber($practiceDetails['phone_number']);
                    $isValidPhoneNumber = StringComponent::isValidPhoneNumber($phone);
                    if ($isValidPhoneNumber && $phone != $fbPhone) {
                        $arrToPost['phone'] = $phone;
                    }

                    // Compare website
                    if (!empty($practiceDetails['website'])) {
                        $arrToPost['website'] = $practiceDetails['website'];
                    }

                    $pDescription = StringComponent::sanitize($practiceDetails['description']);
                    if (
                        !empty($pDescription)
                        &&
                        (empty($fanPage['description']) || $pDescription != $fanPage['description'])
                    ) {
                        // Compare description
                        $arrToPost['description'] = $pDescription;
                    }

                    // Send to Facebook
                    if (!empty($arrToPost)) {
                        $response = $fb->post('/' . $fbPageId, $arrToPost, $fbAccessToken);
                        $result = $response->getGraphNode()->asArray();

                        // update last_post_date && last_change_log
                        $changes = [
                            'before' => $fanPage,
                            'after' => $arrToPost
                        ];
                        $ppse->last_change_log = json_encode($changes);
                        $ppse->last_post_date = TimeComponent::getNow();
                        $ppse->last_status = 'success';
                        $ppse->save();
                        return $result;
                    } else {

                        $queueBody['account_id'] = '';
                        $queueBody['facebookPageName'] = '';
                        $queueBody['errorDescription'] = '';

                        $result['success'] = false;
                        $result['error'] = 'No updates to submit..';
                        $result['queueBody'] = $queueBody;

                        $ppse->last_change_log = json_encode('No updates to submit..');
                        $ppse->last_post_date = TimeComponent::getNow();
                        $ppse->last_status = 'unchanged';
                        $ppse->save();
                    }
                } catch (Exception $e) {

                    // alert email to tech guys
                    $queueBody = array();
                    $queueBody['account_id'] = $ppse->updated_by_account_id;
                    $queueBody['practice_id'] = $practiceId;
                    $queueBody['practice_name'] = $practiceDetails['name'];
                    $queueBody['facebookPageName'] = $set->profile_name;
                    $queueBody['facebookPageId'] = $set->profile_id;
                    $queueBody['facebookToken'] = $fbAccessToken;
                    $queueBody['errorDescription'] = $e->getMessage();

                    $result['success'] = false;
                    $result['error'] = 'ERROR (1) trying to update facebook: ';
                    $result['queueBody'] = $queueBody;
                    $result['arrToPost'] = $arrToPost;

                    $ppse->last_change_log = json_encode($e->getMessage());
                    $ppse->last_post_date = TimeComponent::getNow();
                    $ppse->last_status = 'invalid';
                    $ppse->save();
                }
            } catch (Exception $e) {

                // alert email to tech guys
                $queueBody = array();
                $queueBody['account_id'] = $ppse->updated_by_account_id;
                $queueBody['practice_id'] = $practiceId;
                $queueBody['practice_name'] = $practiceDetails['name'];
                $queueBody['facebookPageName'] = $set->profile_name;
                $queueBody['facebookPageId'] = $set->profile_id;
                $queueBody['facebookToken'] = $fbAccessToken;
                $queueBody['errorDescription'] = $e->getMessage();

                $result['success'] = false;
                $result['error'] = 'ERROR (1) trying to update facebook: ';
                $result['queueBody'] = $queueBody;
                $result['arrToPost'] = $arrToPost;

                $ppse->last_change_log = json_encode($e->getMessage());
                $ppse->last_post_date = TimeComponent::getNow();
                $ppse->last_status = 'invalid';
                $ppse->save();
            }
        } else {
            $result['success'] = false;
            $result['error'] = '-- Empty access token ';
        }

        return $result;
    }

    /**
     * Prepare email to send to Primary Account Email, when there's an error trying to sync with facebook
     * @param array $queueBody
     *   Eg:
     * [account_id] => [
     *       [accountId] => 23106
     *       [practice_id] => 3700750
     *       [facebookPageName] => Actualidad y Tecnologia
     *       [facebookPageId] => 104667306275844
     *       [facebookToken] => xxx.yyy.zzz
     *       [errorDescription] => Error validating access token: The session has been invalidated because the user has
     *       changed the password.
     * ]
     */
    public static function alertFacebookSyncFail($queueBody = array())
    {

        if (empty($queueBody)) {
            return null;
        }

        foreach ($queueBody as $accountId => $accountQueueBody) {

            // When was the last email that we sent to this account?
            $accountDetails = AccountDetails::model()->find(
                "account_id=:account_id",
                [":account_id" => $accountId]
            );
            if (empty($accountDetails)) {

                // This account exists?
                $account = Account::model()->findByPk($accountId);
                if (empty($account->id)) {
                    // This account not exists anymore
                    continue;
                }

                $accountDetails = new AccountDetails();
                $accountDetails->account_id = $accountId;
                $accountDetails->save();
            }
            $sendEmail = true;
            if (!empty($accountDetails->fb_token_last_error_sent)) {
                // get time diff
                $now = TimeComponent::getNow();
                $timeDiff = TimeComponent::getDateDiff($now, $accountDetails->fb_token_last_error_sent, 1);
                if ($timeDiff < 30) {
                    $sendEmail = false;
                }
            }

            if (!$sendEmail) {
                // We dont have to send the notification email
                continue;
            }

            // Update the fb_token_last_error_sent field
            $accountDetails->fb_token_last_error_sent = TimeComponent::getNow();
            $accountDetails->save();

            $data = array();

            $account = Account::model()->findByPk($accountId);

            $data['account_id'] = $accountId;
            $data['account_email'] = $account->email;
            $data['sfEmail'] = null;
            // find related organization
            $organizationId = Account::belongsToOrganization($accountId);
            $recOrganization = Organization::model()->find(
                "id=:organizationId",
                array(":organizationId" => $organizationId)
            );

            // find SF details
            if (!empty($recOrganization['salesforce_id'])) {
                $salesForceDetails = Account::getSalesforceDetails($recOrganization['salesforce_id'], true);
                if (isset($salesForceDetails['account_rep_email'])) {
                    $data['sfEmail'] = $salesForceDetails['account_rep_email'];
                }
            }

            $tableListPages = '<table style="width:100%;">';
            $tableListPages .= '<tr>';
            $tableListPages .= '<td style="width:33%;"><strong>Practice</strong></td>';
            $tableListPages .= '<td style="width:33%;"><strong>Facebook Page</strong></td>';
            $tableListPages .= '<td style="width:33%;"><strong>Error Description</strong></td>';
            $tableListPages .= '</tr>';

            $isTest = false;
            foreach ($accountQueueBody as $qb) {

                $tableListPages .= '<tr>';
                $tableListPages .= '<td style="width:33%;">' . $qb['practice_name'] . '</td>';
                $tableListPages .= '<td style="width:33%;">' . $qb['facebookPageName'] . '</td>';
                $tableListPages .= '<td style="width:33%;">' . $qb['errorDescription'] . '</td>';
                $tableListPages .= '</tr>';

                if (isset($qb['isTest']) && Common::isTrue($qb['isTest'])) {
                    // only when coming from phpUnit Test
                    $isTest = true;
                }
            }
            $tableListPages .= '</table>';

            $data['tableListPages'] = $tableListPages;
            $data['alertType'] = 'errorFacebookAutoPublish';

            if ($isTest) {
                return $data;
            }

            MailComponent::alertFacebookSyncFail($data);
        }
    }
}
