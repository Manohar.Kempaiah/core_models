<?php

use application\components\Google\GooglePlace;

Yii::import('application.modules.core_models.models._base.BaseScanCompetitorPractice');

class ScanCompetitorPractice extends BaseScanCompetitorPractice
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get the latest ScanCompetitorPractice related to
     * a place id.
     *
     * @param string $placeId
     *
     * @return ScanCompetitorPractice
     */
    public function getLatestByPlaceId($placeId, $withRelations = false)
    {
        $criteria = new CDbCriteria();

        if ($withRelations) {
            $criteria->with[] = 'scanCompetitorProviders';
            $criteria->with[] = 'status';
            $criteria->with[] = 'reputationStatus';
            $criteria->with[] = 'scanCompetitorPracticeBatches';
        }

        $criteria->compare('place_id', $placeId);
        $criteria->order = 't.date_added DESC';

        return ScanCompetitorPractice::model()->find($criteria);
    }

    /**
     * Get the 'Status' object from a status code.
     * Shorthand method to avoid passing the status group code.
     *
     * @param string $code
     *
     * @return Status
     */
    public function getStatus($code)
    {
        return Status::model()->getByCodes(
            $code,
            'research_competitors_practice'
        );
    }

    /**
     * Change the stratus_id of the instance and
     * save it to the database.
     *
     * @param string $statusCode
     *
     * @return Status
     */
    public function markAs($statusCode)
    {
        $status = $this->getStatus($statusCode);

        $this->status_id = $status->id;
        $this->save();
    }

    /**
     * Change the status of the instance to 'in progress'
     *
     * @return void
     */
    public function markAsInProgress()
    {
        $this->markAs('research_competitors_practice_in_progress');
    }

    /**
     * Change the status of the instance to 'finished'
     *
     * @return void
     */
    public function markAsFinished()
    {
        $this->markAs('research_competitors_practice_finished');
    }

    /**
     * Change the status of the instance to 'created'
     * This method doesn't apply the changes to the database.
     *
     * @return void
     */
    public function setCreatedStatus()
    {
        $status = $this->getStatus('research_competitors_practice_created');
        $this->status = $status;
        $this->status_id = $status->id;
    }

    /**
     * Get the 'Status' object from a status code.
     * Shorthand method to avoid passing the status group code.
     *
     * @param string $code
     *
     * @return Status
     */
    public function getReputationStatus($code)
    {
        return Status::model()->getByCodes(
            $code,
            'reputation_request'
        );
    }

    /**
     * Change the reputation status of the instance to 'requested'
     * This method doesn't apply the changes to the database.
     *
     * @return void
     */
    public function setReputationRequested()
    {
        $status = $this->getReputationStatus('reputation_requested');
        $this->reputationStatus = $status;
        $this->reputation_status_id = $status->id;
    }

    /**
     * Change the status of the instance to 'requested'
     *
     * @return void
     */
    public function markReputationRequest()
    {
        $this->setReputationRequested();
        $this->save();
    }

    /**
     * Determine if the ScanCompetitorPractice record is expired
     *
     * We are using a hard limit to determine how long should all the data
     * requested for a place id should live.
     *
     * @return boolean
     */
    public function isExpired()
    {
        $currentTS = strtotime(date("Y-m-d H:i:s"));
        $scpTS = strtotime($this->date_added);

        // Transform seconds diff to days
        $diff = ($currentTS - $scpTS) / 60 / 60 / 24;

        // If diff is greater than 20 days, it's expired.
        if ($diff > 20) {
            return true;
        }

        return false;
    }

    /**
     * Get the first batch of scans that belong to this record
     *
     * @return ScanCompetitorPracticeBatch
     */
    public function getFirstBatch()
    {
        if ($this->scanCompetitorPracticeBatches) {
            return $this->scanCompetitorPracticeBatches[0];
        } else {
            return null;
        }
    }

    /**
     * Get the scan log associated to this record.
     *
     * @return ScanLog
     */
    public function getScanLog()
    {
        $batch = $this->getFirstBatch();
        if ($batch) {
            return ScanLog::model()->findByPk($batch->scan_log_id);
        }

        return null;
    }

    /**
     * Get the scan data object from this record
     *
     * @return ScanResultData
     */
    public function getScanData()
    {
        $scanLog = $this->getScanLog();
        if ($scanLog) {
            return $scanLog->getScanDataObject();
        }

        return null;
    }

    /**
     * Get the search result field decoded
     *
     * @return void
     */
    public function getSearchResult()
    {
        return json_decode($this->search_result);
    }

    /**
     * Get the results from google decoded
     *
     * @return array
     */
    public function getGoogleResult()
    {
        $searchResult = $this->getSearchResult();
        $googleJson = Common::get($searchResult, 'google');
        if ($googleJson) {
            return json_decode($googleJson);
        }

        return [];
    }

    /**
     * Get a GooglePlace object from the google
     *
     * @return GooglePlace
     */
    public function getPlace()
    {
        $data = $this->getGoogleResult();
        if ($data) {
            return new GooglePlace($data);
        }

        return [];
    }

    /**
     * Get the scan_log_id related to this record.
     * This is found via the first batch of scans.
     *
     * @return void
     */
    public function getScanLogId()
    {
        $batch = $this->getFirstBatch();
        if ($batch) {
            return $batch->scan_log_id;
        }

        return null;
    }

    /**
     * Get the practice id related to this record
     * This info is taken from the ScanRequest
     *
     * @return integer
     */
    public function getPracticeId()
    {
        $scanLogId = $this->getScanLogId();
        $scanRequestPractice = ScanRequestPractice::model()->getByScanLogId($scanLogId);

        if ($scanRequestPractice && $scanRequestPractice->practice_id) {
            return $scanRequestPractice->practice_id;
        }

        return null;
    }

    /**
     * Find a place by a practice id.
     * This method looks for the scan requests that used this practice id and
     * then joins via scan_log_id to the research competitors tables.
     *
     * @param integer $practiceId
     * @return array
     */
    public function findPlaceByPracticeId($practiceId)
    {
        $sql = "SELECT
                    scan_competitor_practice.place_id AS place_id,
                    scan_request.id AS scan_request_id,
                    scan_request_practice.id AS scan_request_practice_id,
                    scan_competitor_practice_batch.id AS batch_id,
                    scan_competitor_practice.id AS scan_competitor_practice_id
                FROM scan_request
                    JOIN scan_competitor_practice_batch
                    ON scan_competitor_practice_batch.scan_log_id = scan_request.scan_log_id
                    JOIN scan_competitor_practice
                    ON scan_competitor_practice_batch.scan_competitor_practice_id = scan_competitor_practice.id
                    JOIN scan_request_practice ON scan_request_practice.scan_request_id = scan_request.id
                WHERE
                    scan_request_practice.practice_id = :practice_id
                ORDER BY scan_request.id DESC";

        return Yii::app()->db->createCommand($sql)
            ->bindValue(':practice_id', $practiceId, PDO::PARAM_STR)
            ->queryRow();
    }

    /**
     * Get practice values from the related scan log object
     *
     * @return array
     */
    public function getPracticeInputFromScan()
    {
        $input = [
            'score' => 0,
            'name' => '',
            'rating' => 0,
            'city_state' => ''
        ];

        $scanLog = $this->getScanLog();
        if ($scanLog) {
            $data = $scanLog->getScanDataObject();
            $input['score'] = $data->getPracticeScore();
            $input['rating'] = $data->getPracticeAverageRating();
            $input['name'] = $data->getInputPracticeName();
            $input['address'] = $data->getInputAddress();
            $input['city_state'] = $data->getCityStateString();
        }

        return $input;
    }

    /**
     * Get practice attributes and scores/reviews from the ScanLog object
     *
     * @return array
     */
    public function getPracticeDataFromScanLog()
    {
        $result = [
            'name' => '',
            'city_state' => '',
            'score' => 0,
            'average_rating' => 0,
            'accuracy' => 0,
            'errors' => 0,
            'missing_listings' => 0,
            'reviews_total' => 0
        ];

        $scanLog = $this->getScanLog();
        if ($scanLog) {
            $data = $scanLog->getScanDataObject();

            $result['name'] = $data->getInputPracticeName();
            $result['address'] = $data->getInputAddress();
            $result['city_state'] = $data->getCityStateString();
            $result['score'] = $data->getPracticeScore();
            $result['average_rating'] = $data->getPracticeAverageRating();
            $result['accuracy'] = $data->getPracticeAccuracy();
            $result['errors'] = $data->getPracticeErrors();
            $result['missing_listings'] = $data->getPracticeMissingListings();
            $result['reviews_total'] = $data->getPracticeReviewsAmount();
            $result['lat'] = $data->getPracticeLatitude();
            $result['lng'] = $data->getPracticeLongitude();
        }

        return $result;
    }

    /**
     * Get a value from the google result by it's path.
     * Eg: $scp->getGoogleData('result->name', 'default_value');
     *
     * @param string $path
     * @param mixed $default
     * @return mixed
     */
    public function getGoogleData($path, $default = null)
    {
        $data = json_decode($this->search_result);
        $googleJson = Common::get($data, 'google');
        $value = $default;
        if ($googleJson) {
            $googleData = json_decode($googleJson);
            $value = Common::get($googleData, $path, $default);
        }

        return $value;
    }

    /**
     * Get the place latitude
     *
     * @return string
     */
    public function getGooglePlaceLatitude()
    {
        return $this->getGoogleData('result->geometry->location->lat', '');
    }

    /**
     * Get the place longitude
     *
     * @return string
     */
    public function getGooglePlaceLongitude()
    {
        return $this->getGoogleData('result->geometry->location->lng', '');
    }
}
