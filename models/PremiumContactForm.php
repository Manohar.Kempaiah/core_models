<?php

/**
 * PremiumContactForm class.
 * PremiumContactForm is the data structure for keeping
 * syndication form data. It is used by myaccount/services?service=syndication
 */
class PremiumContactForm extends CFormModel
{

    public $name;
    public $phone;
    public $email;
    public $bestTimeToCall;
    public $interests;

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'name' => 'Contact Person',
            'phone' => 'Contact Person Phone #',
            'email' => 'Contact Person Email',
            'bestTimeToCall' => 'Best Time to Call',
            'interests' => 'Interested In',
        );
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            // name, phone, email are required
            array('name, phone, email', 'required'),
            // email has to be a valid email address
            array('email', 'email'),
            array('phone', 'phoneNumber'),
            // declare remaining items to allow mass assignment
            array('bestTimeToCall, interests', 'safe'),
        );
    }

    /**
     * check the format of the phone number entered
     * @param string $attribute the name of the attribute to be validated
     * @param array $params options specified in the validation rule
     */
    public function phoneNumber($attribute, $params = '')
    {
        if (preg_match("/^\(?\d{3}\)?[\s-]?\d{3}[\s-]?\d{4}$/", $this->$attribute) === 0) {
            $this->addError($attribute, 'The phone number provided is invalid');
        }
    }

    /**
     * Generates subject to send to sales with customer information.
     */
    public function generateEmailSubject()
    {
        return 'Doctor.com - Consultation Request Received from ' . $this->name;
    }

    /**
     * Generates email body to send to sales with customer information.
     */
    public function generateEmailBody()
    {
        $body = '';

        if ($this->interests) {
            $interestedInSummary = implode(', ', $this->interests);
        } else {
            $interestedInSummary = 'unspecified';
        }

        $body .= 'Name: <b>' . $this->name . '</b><br />';
        $body .= 'Phone: <b>' . $this->phone . '</b><br />';
        $body .= 'Email: <b>' . $this->email . '</b><br />';
        $body .= 'Best time to call: <b>' . $this->bestTimeToCall . '</b><br />';
        $body .= 'Interested in: <b>' . $interestedInSummary . '</b><br />';
        $body .= '<br />Please reach out to this person and schedule a consultation.';

        return $body;
    }

    /**
     * Sends email to sales with customer information.
     */
    public function emailToSales()
    {

        // sends the premium contact form to sales via email
        // checked on 2014-12-22 and it's working at https://providers.doctor.com/myaccount/services
        $mailFrom = array('From' => 'notifications@corp.doctor.com', 'From_Name' => 'Doctor.com Notification');
        $mailTo = 'sales.inbound@corp.doctor.com';
        $mailSubject = $this->generateEmailSubject();
        $mailAltBody = $this->generateEmailBody();
        $mailBody = nl2br($mailAltBody);
        MailComponent::sendEmail(
            $mailFrom,
            $mailTo,
            false,
            false,
            $mailSubject,
            $mailBody,
            $mailAltBody,
            false,
            false,
            'Name:'
        );
    }

}
