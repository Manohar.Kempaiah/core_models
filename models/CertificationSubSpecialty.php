<?php

Yii::import('application.modules.core_models.models._base.BaseCertificationSubSpecialty');

class CertificationSubSpecialty extends BaseCertificationSubSpecialty
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getFullCertificationSpecialty()
    {

        $certification_specialty = CertificationSpecialty::model()->findByPk($this->certification_specialty_id);
        return $certification_specialty->name . ' - ' . $this->name;
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        return array(
            'providerCertifications' => array(self::HAS_MANY, 'ProviderCertification', 'certification_sub_specialty_id'),
            'certifyingBody' => array(self::BELONGS_TO, 'CertifyingBody', 'certifying_body_id'),
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'certifying_body_id' => Yii::t('app', 'Certifying Body'),
            'certification_specialty_id' => Yii::t('app', 'Certification Specialty'),
            'certification_sub_specialty_id' => Yii::t('app', 'Certification Sub Specialty'),
            'name' => Yii::t('app', 'Name'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        // this is to enable eager loading
        $criteria->with = array('certifyingBody');
        $criteria->compare('certifying_body_id', $this->certifying_body_id);
        $criteria->compare('t.name', $this->name, true);

        return new CActiveDataProvider(
            get_class($this),
            array(
                'criteria' => $criteria,
                'pagination' => array('pageSize' => 100)
            )
        );
    }

}
