<?php

Yii::import('application.modules.core_models.models._base.BaseSchedulingMailEhrPatient');

class SchedulingMailEhrPatient extends BaseSchedulingMailEhrPatient
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get data about the patients matched in the EHR
     * @param int $schedulingMailId
     * @param int $patientId
     * @return array
     */
    public static function getMatchedPatientData($schedulingMailId = 0, $patientId = 0)
    {

        if ($patientId == 0) {
            $scheduling = SchedulingMail::model()->findByPk($schedulingMailId);
            if (!empty($scheduling)) {
                $patientId = $scheduling->patient_id;
            }
        }

        // ehr match patient
        $ehrPatient = null;
        $smEhrPatient = SchedulingMailEhrPatient::model()->findAll(
            'scheduling_mail_id=:scheduling_mail_id',
            array(':scheduling_mail_id' => $schedulingMailId)
        );
        if ($smEhrPatient && !empty($patientId)) {
            $ehrPatient = Patient::model()->findByPk($patientId);
            if ($ehrPatient) {
                if (!empty($ehrPatient->date_of_birth)) {
                    $arrDateOfBirth = explode("-", $ehrPatient->date_of_birth);
                    $ehrPatient->date_of_birth = $arrDateOfBirth[1]
                        . '/' . $arrDateOfBirth[2]
                        . '/' . $arrDateOfBirth[0];
                }
            }
        }

        return array("ehrPatient" => $ehrPatient, "smEhrPatient" => $smEhrPatient);
    }
}
