<?php

Yii::import('application.modules.core_models.models._base.BaseGmbCategory');

class GmbCategory extends BaseGmbCategory
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check duplicate value for gcid
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->isNewRecord) {
            $sql = "SELECT count(id) AS count
                FROM gmb_category
                WHERE  gcid = '" . $this->gcid . "'";
        } else {
            $sql = "SELECT count(id) AS count
                FROM gmb_category
                WHERE id != " . $this->id . "  AND gcid = '" . $this->gcid . "'";
        }

        $res = Yii::app()->db->createCommand($sql)->queryRow();
        if (isset($res['count']) && $res['count'] > 0) {
            $this->addError(
                "level",
                "This GCID already exist"
            );
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Updating gmb_category_mapping and provider_practice_gmb categories value to NULL
     */
    public function deleteGMBCategory($gmb_category_id)
    {

        $sql = 'UPDATE gmb_category set is_active=0 where id=:id';
        $params = array(':id' => $gmb_category_id);
        Yii::app()->db->createCommand($sql)->execute($params);

        //updating gmb_category_mapping table with NULL value for deleted gmb_category_id
        $sql = 'UPDATE gmb_category_mapping set gmb_category_id=NULL where gmb_category_id=:gmb_category_id';
        $params = array(':gmb_category_id' => $gmb_category_id);
        Yii::app()->db->createCommand($sql)->execute($params);

        $sql = 'SELECT gcid FROM gmb_category WHERE id=' . $gmb_category_id;
        $gcid = Yii::app()->db->createCommand($sql)->queryRow();
        if (isset($gcid['gcid'])) {
            $gcid = $gcid['gcid'];

            //updating provider_practice_gmb table's categories value with NULL for deleted gcid
            $sql = 'UPDATE provider_practice_gmb set category_1=NULL where category_1=:category_1';
            $params = array(':category_1' => $gcid);
            Yii::app()->db->createCommand($sql)->execute($params);

            $sql = 'UPDATE provider_practice_gmb set category_2=NULL where category_2=:category_2';
            $params = array(':category_2' => $gcid);
            Yii::app()->db->createCommand($sql)->execute($params);

            $sql = 'UPDATE provider_practice_gmb set category_3=NULL where category_3=:category_3';
            $params = array(':category_3' => $gcid);
            Yii::app()->db->createCommand($sql)->execute($params);
        }

        parent::afterDelete();
    }

    /**
     * Get the selectable GMB Category.
     * @return array
     */
    public function selectableGmbCategory()
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id,t.name';
        $criteria->condition = 't.is_active = 1';
        return self::model()->findAll($criteria);
    }
}
