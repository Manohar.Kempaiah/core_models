<?php

Yii::import('application.modules.core_models.models._base.BaseUser');

class User extends BaseUser
{

    // passwords are stored in User->secret - we use the password variable when a user submits a change
    public $password;
    public $password_confirm;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
