<?php

Yii::import('application.modules.core_models.models._base.BaseWidget');

class Widget extends BaseWidget
{

    public $account_full_name;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Checks if a Widget exists
     * @param string $key Widget key_code
     * @return bool
     */
    public static function defined($key)
    {
        $json = Yii::app()->cache->get("W" . $key);

        if ($json) {
            return true;
        }

        return (int) Widget::model()->count("key_code = :key", array(":key" => $key)) > 0;
    }

    /**
     * Load the widget with the specified key from the DB or the cache.
     * @param string $key
     * @return Widget
     */
    public static function load($key)
    {
        $json = Yii::app()->cache->get("W" . $key);
        $widget = null;

        if ($json) {
            $data = json_decode($json, true);
            $widget = new Widget();
            $widget->attributes = $data['attributes'];
            $widget->setPrimaryKey($data['attributes']['id']);
            $widget->isNewRecord = false;
            $widget->options = base64_decode($widget->options);
        } else {
            $widget = Widget::model()->find("key_code = :key", array(":key" => $key));

            if (!$widget) {
                return null;
            }
            $attributes = $widget->attributes;

            $attributes['options'] = base64_encode($attributes['options']);

            Yii::app()->cache->set(
                "W" . $key,
                json_encode(
                    array(
                        'attributes' => $attributes
                    )
                ),
                Yii::app()->params['cache_long']
            );
        }

        return $widget;
    }

    /**
     * Clear the values stored in the cache for the widget with the specified key.
     * @param string $key
     */
    public static function clear($key)
    {
        Yii::app()->cache->delete("W" . $key);
        Yii::app()->cache->delete("W" . $key . "_churned");

        //Clear output for all the actions
        $jsonActions = Yii::app()->cache->get("W" . $key . "_actions");
        $actions = array();
        if ($jsonActions) {
            $actions = json_decode($jsonActions, true);
        }
        foreach ($actions as $action) {
            Yii::app()->cache->delete("W" . $key . "_output_" . $action);
        }

        //Clear output for all Read Review actions
        $jsonActions = Yii::app()->cache->get("W" . $key . "_false_false_actions");
        $actions = array();
        if ($jsonActions) {
            $actions = json_decode($jsonActions, true);
        }
        foreach ($actions as $action) {
            Yii::app()->cache->delete("W" . $key . "_false_false_output_" . $action);
        }

        //Clear output for all Read Review actions
        $jsonActions = Yii::app()->cache->get("W" . $key . "_true_true_actions");
        $actions = array();
        if ($jsonActions) {
            $actions = json_decode($jsonActions, true);
        }
        foreach ($actions as $action) {
            Yii::app()->cache->delete("W" . $key . "_true_true_output_" . $action);
        }

        //Clear output for all Read Review actions
        $jsonActions = Yii::app()->cache->get("W" . $key . "_false_true_actions");
        $actions = array();
        if ($jsonActions) {
            $actions = json_decode($jsonActions, true);
        }
        foreach ($actions as $action) {
            Yii::app()->cache->delete("W" . $key . "_false_true_output_" . $action);
        }

        //Clear output for all Read Review actions
        $jsonActions = Yii::app()->cache->get("W" . $key . "_true_false_actions");
        $actions = array();
        if ($jsonActions) {
            $actions = json_decode($jsonActions, true);
        }
        foreach ($actions as $action) {
            Yii::app()->cache->delete("W" . $key . "_true_false_output_" . $action);
        }
    }

    /**
     * Clear the values stored in the Account.
     * @param int $accountId
     */
    public static function clearAccountWidget($accountId)
    {
        $widgets = Widget::model()->findAll("account_id = :account_id", array(":account_id" => $accountId));

        foreach ($widgets as $widget) {
            Widget::clear($widget->key_code);
        }
    }

    /**
     * Whether the widget belongs to a churned organization or not.
     * @return bool
     */
    public static function belongsToChurnedOrganization($key)
    {
        $churned = null;

        $json = Yii::app()->cache->get("W" . $key . "_churned");
        if ($json) {
            $data = json_decode($json);
            $churned = (bool) $data->churned;
        } else {
            $widget = Widget::load($key);
            $churned = (bool) Account::model()->belongsToChurnedOrganization($widget->account_id);

            $data = array(
                "churned" => $churned
            );
            Yii::app()->cache->set("W" . $key . "_churned", json_encode($data), Yii::app()->params['cache_long']);
        }
        return $churned;
    }

    /**
     * Return the cached output of the widget for a given action.
     * @param string $key Widget key_code
     * @param string $action The action id
     * @return string
     */
    public static function getOutput($key, $action)
    {
        $output = Yii::app()->cache->get("W" . $key . "_output_" . $action);
        if (!empty($output)) {
            return $output;
        }
        return null;
    }

    /**
     * Store the output for a widget for a given action
     * @param string $key Widget key_code
     * @param string $action The action id
     * @param string $output Generated HTML for the widget.
     */
    public static function setOutput($key, $action, $output)
    {
        $jsonActions = Yii::app()->cache->get("W" . $key . "_actions");
        $actions = array();
        if ($jsonActions) {
            $actions = json_decode($jsonActions, true);
        }
        $actions[] = $action;
        Yii::app()->cache->set("W" . $key . "_actions", json_encode($actions), Yii::app()->params['cache_short']);
        Yii::app()->cache->set("W" . $key . "_output_" . $action, $output, Yii::app()->params['cache_short']);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting if it has facebook integration
        if (!empty($this->fb_page_id)) {
            $this->addError('id', 'This widget cannot be deleted because it has Facebook Integration');
            return false;
        }

        Widget::clear($this->key_code);

        // Clear al provider_rating.widget_id value
        $arrProviderRating = ProviderRating::model()->findAll(
            'widget_id=:widget_id',
            array(':widget_id' => $this->id)
        );
        foreach ($arrProviderRating as $pr) {
            $pr->widget_id = null;
            $pr->save();
        }

        // Delete associated provider_practice_widget records
        $arrProviderPracticeWidgets = ProviderPracticeWidget::model()->findAll(
            'widget_id=:widget_id',
            array(':widget_id' => $this->id)
        );
        foreach ($arrProviderPracticeWidgets as $ppw) {
            $ppw->delete();
        }

        // Delete associated provider_rating_widget records
        $arrProviderRatingWidgets = ProviderRatingWidget::model()->findAll(
            'widget_id=:widget_id',
            array(':widget_id' => $this->id)
        );
        foreach ($arrProviderRatingWidgets as $prw) {
            $prw->delete();
        }

        // Delete associated widget_url records
        $arrWidgetURL = WidgetUrl::model()->findAll(
            'widget_id=:widget_id',
            array(':widget_id' => $this->id)
        );
        foreach ($arrWidgetURL as $wu) {
            $wu->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * Clear cache and process afterSave
     * @return bool
     */
    public function afterSave()
    {
        Widget::clear($this->key_code);

        if (php_sapi_name() != "cli" && !empty(Yii::app()->user->id)) {
            // Set edition status for the widget
            // edition status will last 5 mins after the last time the widget was saved
            Yii::app()->cache->set(
                $this->key_code . "_editing",
                array(
                    "account_id" => empty(Yii::app()->session['admin_account_id'])
                        ? Yii::app()->user->id
                        : Yii::app()->session['admin_account_id'],
                    "date" => date("Y-m-d H:i:s"), 5 * 60
                )
            );
        }

        return parent::afterSave();
    }

    /**
     * Get the providers associated with a widget
     * @param bool $demoMode Whether we're in preview mode
     * @return array The list of associated providers
     */
    public function getProviders()
    {
        $sql = sprintf(
            "SELECT DISTINCT p.*
            FROM widget rw
            INNER JOIN provider_practice_widget pprw
                ON pprw.widget_id = rw.id
            INNER JOIN provider p
                ON pprw.provider_id = p.id
            WHERE rw.key_code = '%s';",
            $this->key_code
        );

        return Provider::model()->findAllBySql($sql);
    }

    /**
     * Get the practices associated with a widget
     * @param int providerId Optional provider id
     * @return array The list of associated practices
     */
    public function getPractices($providerId = null)
    {
        $sql = sprintf(
            "SELECT DISTINCT p.*
            FROM widget rw
            INNER JOIN provider_practice_widget pprw
                ON pprw.widget_id = rw.id
            INNER JOIN practice p
                ON pprw.practice_id = p.id
            WHERE rw.key_code = '%s' %s;",
            $this->key_code,
            isset($providerId) ? "AND provider_id = " . $providerId : ""
        );
        return Practice::model()->findAllBySql($sql);
    }

    /**
     * Get the reviews associated with the widget
     * @param int $page Page number
     * @param int $providerId Return reviews for a specific provider
     * @return array Associative array with the list of reviews and the total count of reviews for the widget.
     */
    public function getProviderRatings($page = null, $providerId = null)
    {

        // Get configuration values
        $options = json_decode($this->options, true);
        if (!isset($options["feed_max_pages"]) || $options["feed_max_pages"] == "") {
            $options["feed_max_pages"] = 999;
        }
        $size = (isset($options["feed_max_reviews"]) ? $options["feed_max_reviews"] : null);
        $maxPages = (isset($options["feed_max_pages"]) ? $options["feed_max_pages"] : null);

        // Based on 'Filter above XX stars' option
        $filterStars = '';
        if (!empty($options["display_filter_above"]) && !empty($options["display_filter_above_stars"])) {
            // Yes, get reviews with rating greater then XXX
            $filterStars = sprintf(" AND provider_rating.rating >= '%s' ", $options["display_filter_above_stars"]);
        }

        $onlyPromoted = '';
        // Based on 'Display Only Promoted Reviews' option
        if (!empty($options["display_only_promoted"])) {
            // Yes, get promoted reviews
            $sqlPromoted = sprintf(
                "SELECT GROUP_CONCAT(provider_rating_id) as id_list
                FROM provider_rating_widget
                WHERE widget_id = %d",
                $this->id
            );
            $promotedReviews = Yii::app()->dbRO->createCommand($sqlPromoted)->queryRow();
            $reviewList = '';
            if (!empty($promotedReviews)) {
                $reviewList = $promotedReviews['id_list'];
                $reviewList = rtrim($reviewList, ',');
            }

            if (empty($reviewList)) {
                $reviewList = '0';
            }

            $onlyPromoted = sprintf(" AND provider_rating.id IN (%s) ", $reviewList);
        }

        $fromReviewHub = '';
        // Based on 'Display only from reviewhub' option
        if (!empty($options["display_only_from_reviewhub"])) {
            // Yes, get reviews from reviewhub
            $fromReviewHub = " AND provider_rating.account_device_id IS NOT NULL ";
        }

        // If we get a providerId as param, then prepare providers condition
        $providerCondition = (!empty($providerId)
            ? sprintf(" AND provider_rating.provider_id = %d ", $providerId)
            : "");

        // get practices
        $practicesList = '';
        $sqlPracticesList = sprintf(
            "SELECT DISTINCT GROUP_CONCAT(practice_id) as practice_id_list
            FROM provider_practice_widget
            WHERE widget_id = %d",
            $this->id
        );
        $lstPractices = Yii::app()->dbRO->createCommand($sqlPracticesList)->queryScalar();

        // get providers
        $sqlProviderList = sprintf(
            "SELECT DISTINCT GROUP_CONCAT(provider_id) as provider_id_list
            FROM provider_practice_widget
            WHERE widget_id = %d
            AND provider_id IS NOT NULL;",
            $this->id
        );
        $lstProviders = Yii::app()->dbRO->createCommand($sqlProviderList)->queryScalar();

        if (!empty($lstPractices)) {
            $practicesList = rtrim($lstPractices, ',');
            $practicesList = sprintf(
                " AND (%s provider_rating.practice_id IN (%s)) ",
                (empty($lstProviders)
                    ? 'provider_rating.provider_id IS NULL AND '
                    : 'provider_rating.practice_id IS NULL OR'
                ),
                $practicesList
            );
        }

        // show only reviews from ReviewHub, Reviews widget, SiteEnhance and Google
        // partner_site_id = 1   -> Doctor
        // partner_site_id = 60  -> Google
        // partner_site_id = 63  -> Site Enhance Widget
        // partner_site_id = 152 -> Old Site Enhance Widget
        $partnerSiteCondition = ' AND provider_rating.partner_site_id IN (1, 60, 63, 152) ';

        // Format body of the query
        $sql = sprintf(
            "FROM widget
            INNER JOIN provider_practice_widget
                ON provider_practice_widget.widget_id = widget.id
            INNER JOIN provider_rating
                ON
                %s
            WHERE widget.key_code = '%s'
                AND provider_rating.status = 'A'
                AND provider_rating.origin <> 'E'
                %s
                %s
                %s
                %s
                %s
                %s
            ORDER BY provider_rating.date_added DESC ",
            (
                empty($lstProviders)
                ? 'provider_practice_widget.practice_id = provider_rating.practice_id'
                : 'provider_practice_widget.provider_id = provider_rating.provider_id'
            ),
            $this->key_code,
            $filterStars,
            $onlyPromoted,
            $fromReviewHub,
            $providerCondition,
            $partnerSiteCondition,
            $practicesList
        );

        // Format LIMIT statement
        $limitSql = "";
        if (isset($options["display_show_as"]) && $options["display_show_as"] == "reviewWidgetFeed"
                && isset($page) && isset($size) && isset($maxPages)) {
            $limitSql = " LIMIT " . ($page * $size) . ", " . $size . " ";
        } elseif (isset($options["display_show_as"]) && $options["display_show_as"] == "reviewWidgetSlideshow") {
            $limitSql = ' LIMIT 500 '; // Arbitrary limit defined on T33084
        }

        // prepare queries
        $ratingSql = sprintf(
            "SELECT DISTINCT
            provider_rating.`id`, provider_rating.`localization_id`, provider_rating.`provider_id`,
            provider_rating.`partner_id`, provider_rating.`partner_site_id`, provider_rating.`widget_id`,
            provider_rating.`practice_id`, provider_rating.`patient_id`, provider_rating.`account_device_id`,
            provider_rating.`account_id`, provider_rating.`device_id`, provider_rating.`shared_with`,
            provider_rating.`reviewer_name`, provider_rating.`reviewer_name_anon`, provider_rating.`reviewer_email`,
            provider_rating.`reviewer_location`, provider_rating.`reviewer_city_id`,
            provider_rating.`rating`, provider_rating.`title`,
            IF (provider_rating.`partner_site_id` = 60, '', provider_rating.`friendly_url`) AS friendly_url,
            provider_rating.`description`, provider_rating.`rating_getting_appt`, provider_rating.`rating_appearance`,
            provider_rating.`rating_courtesy`, provider_rating.`rating_waiting_time`,
            provider_rating.`rating_friendliness`, provider_rating.`rating_spending_time`,
            provider_rating.`rating_diagnosis`, provider_rating.`rating_follow_up`,
            provider_rating.`rating_billing`, provider_rating.`rating_listening_skills`,
            provider_rating.`rating_clear_explanations`, provider_rating.`rating_trust`,
            provider_rating.`status`, provider_rating.`status_reason`,
            provider_rating.`status_comments`, provider_rating.`origin`, provider_rating.`reference_url`,
            provider_rating.`yext_site_id`, provider_rating.`yext_review_id`, provider_rating.`source_ip_address`,
            provider_rating.`date_added`, provider_rating.`date_updated`, provider_rating.`date_approved`,
            provider_rating.`facebook_clicks`, provider_rating.`twitter_clicks`, provider_rating.`facebook_url`,
            provider_rating.`twitter_url`, provider_rating.`facebook_url_short`, provider_rating.`twitter_url_short`,
            provider_rating.`date_facebook_sent`, provider_rating.`share_on_facebook`,
            provider_rating.`share_on_twitter`,
            provider_practice_widget.suffix AS provider_suffix,
            provider_practice_widget.prefix AS provider_prefix,
            provider_practice_widget.middle_name AS provider_middle_name
            %s
            %s;",
            $sql,
            $limitSql
        );

        // Count sql
        $countSql = sprintf("SELECT DISTINCT provider_rating.`id` %s;", $sql);

        // get actual ratings
        $ratings = ProviderRating::model()->findAllBySql($ratingSql);

        // count ratings
        $count = Yii::app()->dbRO->createCommand($countSql)->query();
        $count = count($count);

        // average ratings
        $average = null;

        // do we have more ratings than the amount we retrieved? if we do, we want to get the average straight from the
        // db, otherwise it's calculated at runtime
        if ($count > count($ratings)) {
            // we do, get average rating value straight from the db with the same condition
            $averageSql = sprintf("SELECT AVG(rating) %s", $sql);
            $average = Yii::app()->dbRO->createCommand($averageSql)->queryScalar();
        }

        // Update count if the provider has more reviews than the max to be shown
        if (isset($size) && isset($maxPages)) {
            if ($count > ($size * $maxPages)) {
                $count = $size * $maxPages;
            }
        }

        return array("ratings" => $ratings, "total" => $count, "average" => $average);
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM widget
            WHERE id = %d;",
            $recordId,
            $accountId
        );
    }

    /**
     * Tells whether the widget has at least one provider with schedule_mail_enabled
     * @return boolean
     */
    public function hasProvidersWithBooking()
    {
        $sql = sprintf(
            "SELECT COUNT(ppw.provider_id) AS cnt
            FROM provider_practice_widget ppw
            INNER JOIN provider_details pd
                ON ppw.provider_id = pd.provider_id
            WHERE ppw.widget_id = %d AND pd.schedule_mail_enabled = 1",
            $this->id
        );
        return Yii::app()->db->createCommand($sql)->queryScalar() > 0;
    }

    /**
     * get widgets by Account ID
     * @param int $accountId
     * @param string $widgetType
     * @return arr
     */
    public function getWidgetsByAccountId($accountId = 0, $widgetType = '')
    {

        if (empty($widgetType)) {
            $widgetCondition = sprintf(
                "(widget_type = 'MOBILE_ENHANCE' AND w.options NOT LIKE '%%%s%%')
                    OR widget_type = 'GET_APPOINTMENT'",
                'hide_appointment_button'
            );
        } else {
            $widgetCondition = sprintf("widget_type = '%s'", $widgetType);
        }

        $sql = sprintf(
            "SELECT DISTINCT w.*, fb_page_id AS profile_name
            FROM widget w
            INNER JOIN provider_practice_widget ppw
                ON w.id = ppw.widget_id
            WHERE w.account_id = %d
                AND w.draft = 0
                AND w.hidden = 0
                AND (%s);",
            $accountId,
            $widgetCondition
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Remove draft widgets for the specified account, provider and practice.
     * @param int $accountId
     * @param int $providerId
     * @param int $practiceId
     * @return bool
     */
    public static function clearDraftWidgets($accountId = null, $providerId = null, $practiceId = null)
    {

        if (empty($providerId) && empty($practiceId)) {
            return false;
        }

        $and = '';
        $where = '';

        if (!empty($accountId)) {
            $account = Account::model()->findByPk($accountId);
            if (!AccountType::isAdminLevel($account->account_type_id)) {
                $where .= "w.account_id = '" . (int) $accountId . "'";
                $and = ' AND ';
            }
        } else {
            return false;
        }

        if (!empty($providerId)) {
            $where .= $and . "ppw.provider_id = '" . (int) $providerId . "'";
            $and = ' AND ';
        }

        if (!empty($practiceId)) {
            $where .= $and . "ppw.practice_id = '" . (int) $practiceId . "'";
        }

        $sql = sprintf(
            "SELECT w.id
            FROM widget w
            INNER JOIN provider_practice_widget ppw
                ON ppw.widget_id = w.id
            WHERE draft = 1 AND %s",
            $where
        );
        $widgetIds = Yii::app()->db->createCommand($sql)->queryColumn();

        $success = true;
        foreach ($widgetIds as $id) {
            $w = Widget::model()->findByPk($id);
            $jsonMark = Yii::app()->cache->get($w->key_code . "_editing");
            $inEdition = false;
            if (php_sapi_name() != "cli" && !empty($accountId)) {
                if (!empty($jsonMark) && is_string($jsonMark)) {
                    $arrMark = json_decode($jsonMark, true);
                    if ($arrMark['account_id'] != $accountId) {
                        $inEdition = true;
                    }
                }
            }

            if (!$inEdition) {
                $success = $success && $w->delete();
            }
        }
        return $success;
    }

    /**
     * Check if a widget with widget_type = MICRODATA exists with the same set of provider_practice_widgets as the
     * widget specified in $widgetId
     * @param int $widgetId
     * @return array
     */
    public static function checkMicrodataWidgetExists($widgetId)
    {

        $sql = sprintf(
            "SELECT microdata_widget.id AS microdata_widget_id, COUNT(DISTINCT pp_source_widget.id)
            AS pp_source_widget_records, COUNT(DISTINCT pp_microdata_widget.id) AS microdata_widget_records,
            microdata_widget.key_code AS microdata_widget_key_code
            FROM widget AS source_widget
            INNER JOIN provider_practice_widget AS pp_source_widget
                ON pp_source_widget.widget_id = source_widget.id
            LEFT JOIN widget AS microdata_widget
                ON (microdata_widget.account_id = source_widget.account_id
                AND microdata_widget.widget_type = 'MICRODATA')
            LEFT JOIN provider_practice_widget AS pp_microdata_widget
                ON (microdata_widget.id = pp_microdata_widget.widget_id
                AND pp_microdata_widget.provider_id = pp_source_widget.provider_id
                AND pp_microdata_widget.practice_id = pp_source_widget.practice_id)
            WHERE source_widget.id = %d
            GROUP BY source_widget.id
            HAVING pp_source_widget_records <= microdata_widget_records",
            $widgetId
        );
        return Yii::app()->dbRO->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Search override
     *
     * @return void
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('hidden', $this->hidden);
        $criteria->compare('draft', $this->draft);
        $criteria->compare('key_code', $this->key_code, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('widget_type', $this->widget_type);
        $criteria->compare('options', $this->options, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
