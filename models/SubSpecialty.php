<?php

Yii::import('application.modules.core_models.models._base.BaseSubSpecialty');

class SubSpecialty extends BaseSubSpecialty
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // delete records from provider_practice_specialty before deleting this sub_specialty id
        $arrProviderPracticeSpecialty = ProviderPracticeSpecialty::model()->findAll(
            'sub_specialty_id = :sub_specialty_id',
            array(':sub_specialty_id' => $this->id)
        );

        foreach ($arrProviderPracticeSpecialty as $providerPracticeSpecialty) {
            if (!$providerPracticeSpecialty->delete()) {
                $this->addError("id", "Couldn't delete linked providers & practices.");
                return false;
            }
        }

        // delete records from provider_specialty before delete this sub_specialty id
        $arrProviderSpecialty = ProviderSpecialty::model()->findAll(
            'sub_specialty_id = :sub_specialty_id',
            array(':sub_specialty_id' => $this->id)
        );

        foreach ($arrProviderSpecialty as $providerSpecialty) {
            if (!$providerSpecialty->delete()) {
                $this->addError("id", "Couldn't delete linked provider specialties.");
                return false;
            }
        }

        // delete records from specialty_medication before delete this sub_specialty id
        $arrSpecialtyMedication = SpecialtyMedication::model()->findAll(
            'sub_specialty_id = :sub_specialty_id',
            array(':sub_specialty_id' => $this->id)
        );

        foreach ($arrSpecialtyMedication as $specialtyMedication) {
            if (!$specialtyMedication->delete()) {
                $this->addError('id', "Couldn't delete linked medications");
                return false;
            }
        }

        // delete records from specialty_treatment before delete this sub_specialty id
        $arrSpecialtyTreatment = SpecialtyTreatment::model()->findAll(
            'sub_specialty_id = :sub_specialty_id',
            array(':sub_specialty_id' => $this->id)
        );

        foreach ($arrSpecialtyTreatment as $specialtyTreatment) {
            if (!$specialtyTreatment->delete()) {
                $this->addError("id", "Couldn't delete linked medications/treatments.");
                return false;
            }
        }

        return parent::beforeDelete();
    }

    /**
     * @todo document
     * @return string
     */
    public function getFullSpecialty()
    {

        $specialty = Specialty::model()->findByPk($this->specialty_id);
        return addslashes($specialty->name . ' - ' . $this->name);
    }

    /**
     * Get all subSpecialties
     * @param int $specialtyId
     * @return arr
     */
    public function getAllSubSpecialties($specialtyId = 0)
    {
        if ($specialtyId == 0) {
            return '';
        }

        $sql = sprintf("SELECT id, name FROM sub_specialty WHERE specialty_id = %d ORDER BY name ASC", $specialtyId);
        $arrSubSpecialties = Yii::app()->db->createCommand($sql)->queryAll();

        $allSubSpecialties = array();
        if (!empty($arrSubSpecialties)) {
            foreach ($arrSubSpecialties as $sp) {
                $allSubSpecialties[$sp['id']]['name'] = $sp['name'];
                $allSubSpecialties[$sp['id']]['checked'] = 0;
            }
        }
        return $allSubSpecialties;
    }

    /**
     * Return two arrays of subspecialties - one with all of them and one with
     * the ones not named "General"
     * Used by search forms in index.php and TabbedSearchForm.php
     * @return array
     */
    public function getSearchSubSpecialties()
    {

        $arrSubSpecialties = array();
        $arrAllSubSpecialties = array();
        $objAllSubspecialties = SubSpecialty::model()
            ->cache(Yii::app()->params['cache_long'])
            ->findAll(array('order' => 'name')
        );

        foreach ($objAllSubspecialties as $objSubSpecialty) {

            $arrAllSubSpecialties[] = array('id' => $objSubSpecialty->id, 'specialty_id' =>
                $objSubSpecialty->specialty_id, 'name' => $objSubSpecialty->name);

            if ($objSubSpecialty->name !== 'General') {
                // create a separate array with the ones that aren't named "General", indexed by specialty id
                $arrSubSpecialties[$objSubSpecialty->specialty_id][] = array('id' => $objSubSpecialty->id,
                    'specialty_id' => $objSubSpecialty->specialty_id, 'name' => $objSubSpecialty->name);
            }
        }

        return array($arrAllSubSpecialties, $arrSubSpecialties);
    }

    /**
     * Return SubSpecialty name
     * @param int $subSpecialtyId
     * @return string
     */
    public static function getSubSpecialty($subSpecialtyId)
    {
        if ($subSpecialtyId > 0) {
            $subSpecialty = SubSpecialty::model()->cache(Yii::app()->params['cache_long'])->findByPk($subSpecialtyId);
            if (!empty($subSpecialty)) {
                return addslashes($subSpecialty->name);
            }
        } elseif (is_null($subSpecialtyId)) {
            return 'ALL';
        }
        return '';
    }

    /**
     * PADM: Provider: Generates SQL for getting a provider's subspecialties
     * @param int $providerId
     * @param int $secondaryId
     * @param int $primaryProviderSpecialtyId
     * @return array
     */
    public static function getProviderSubSpecialties($providerId, $secondaryId, $primaryProviderSpecialtyId)
    {

        if ($providerId) {
            $sql = sprintf(
                'SELECT sub_specialty.id AS id, sub_specialty.name AS name,
                IF(provider_specialty.id IS NULL, false, true ) AS checked
                FROM sub_specialty
                LEFT JOIN provider_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
                AND provider_id = %d
                WHERE sub_specialty.specialty_id = %d
                AND (provider_specialty.id != %d
                OR provider_specialty.id IS NULL)
                ORDER BY provider_specialty.id IS NULL ASC, provider_specialty.primary_specialty,
                sub_specialty.name ASC;',
                $providerId,
                $secondaryId,
                $primaryProviderSpecialtyId
            );
        } else {
            $sql = 'SELECT sub_specialty.id AS id, sub_specialty.name AS name,
                false AS checked
                FROM sub_specialty
                INNER JOIN specialty ON sub_specialty.specialty_id = specialty.id
                WHERE specialty.level = "V"
                ORDER BY sub_specialty.name;';
        }
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get sub specialties from Healthgrades partner data
     *
     * If $subSpecialtyId is specified, this will return a string with the found value, otherwise will returns an
     * array with all sub specialties found.
     *
     * @param int $subSpecialtyId
     * @return mixed
     */
    public static function getPartnerDataHealthgrades($subSpecialtyId = null)
    {
        // partner_site.id = 46 => Healthgrades
        return PartnerSiteEntityMatch::getPartnerValue(46, 'sub_specialty', 'sub_specialty_code', $subSpecialtyId);
    }

    /**
     * Get sub specialties from Vitals partner data
     *
     * If $subSpecialtyId is specified, this will return a string with the found value, otherwise will returns an
     * array with all sub specialties found.
     *
     * @param int $subSpecialtyId
     * @return mixed
     */
    public static function getPartnerDataVitals($primaryFilter = null, $subSpecialtyId = null)
    {
        // partner_site.id = 111 => Vitals
        $specialties = PartnerSiteEntityMatch::getPartnerValue(
            PartnerSite::VITALS_SITE_ID,
            'vitals_sub_specialty_id',
            'vitals_specialty_subspecialty',
            $subSpecialtyId
        );

        if (is_array($specialties)) {
            // create an array with unique specialties before the minus simbol (-)
            $unique = [];

            foreach ($specialties as $specialty) {
                $arrValues = explode(' _ ', $specialty);

                if ($arrValues[0]) {
                    if ($arrValues[0] == $primaryFilter) {
                        if (isset($arrValues[1])) {
                            $unique[$arrValues[1]] = true;
                        }
                    }
                }
            }

            // make an array with the standard format
            $return = [];
            foreach (array_keys($unique) as $specialty) {
                $return[] = $specialty;
            }

            return $return;
        } else {
            return $specialties;
        }
    }

    /**
     * Get specialties from WebMD partner data
     *
     * If $specialtyId is specified, this will return a string with the found value, otherwise will returns an
     * array with all specialties found.
     *
     * @param int $subSpecialtyId
     * @return mixed
     */
    public static function getPartnerDataWebmd($subSpecialtyId = null)
    {
        // partner_site.id = 86 => WebMD
        return PartnerSiteEntityMatch::getPartnerValue(
            86,
            'webmd_sub_specialty_id',
            'webmd_specialty_name',
            $subSpecialtyId
        );
    }

    public function findBySpecialtyIds($specialtyIds)
    {
        $criteria=new CDbCriteria();
        $criteria->addInCondition('specialty_id', $specialtyIds);
        return $this->findAll($criteria);
    }

    public function getByTaxonomyCodes($codes)
    {
        $specialties = [];
        if ($codes) {
            $criteria = new CDbCriteria();
            $criteria->addInCondition('taxonomy_code', $codes);
            $specialties = SubSpecialty::model()
                ->with('specialty')
                //->cache(Yii::app()->params['cache_long'])
                ->findAll($criteria);
        }

        return $specialties;
    }

    /**
     * Get the selectable SubSpecialty.
     * @return array
    */
    public function selectableSubSpecialty($specialtyId="")
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id,t.name';
        if ($specialtyId>0) {
            $criteria->compare('t.specialty_id', $specialtyId);
        }
        return self::model()->cache(Yii::app()->params['cache_long'])->findAll($criteria);
    }

    /**
     * Get the selectable GMB Category.
     * @return array
    */
    public function getSubSpecialtyBySpecialtyId($specialtyId)
    {
        $criteria = new CDbCriteria;
        $criteria->select = 't.id,t.name';
        $criteria->compare('specialty_id', $specialtyId);
        $data = self::model()->cache(Yii::app()->params['cache_long'])->findAll($criteria);
        $allSubSpecialties = array();
        if (!empty($data)) {
            foreach ($data as $row) {
                $object=new stdClass();
                $object->id=$row['id'];
                $object->name=$row['name'];
                $allSubSpecialties[]=$object;
            }
        }
        return $allSubSpecialties;
    }
}
