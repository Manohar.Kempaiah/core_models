<?php

Yii::import('application.modules.core_models.models._base.BaseReferenceParams');

class ReferenceParams extends BaseReferenceParams
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
