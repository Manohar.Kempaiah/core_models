<?php

Yii::import('application.modules.core_models.models.ScanResultPractice');

class ScanResultPracticeEvaluation extends ScanResultPractice
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'scan_result_practice_evaluation';
    }
}
