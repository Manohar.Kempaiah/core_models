<?php

use Aws\Result;

Yii::import('application.modules.core_models.models._base.BaseListingSiteGlobalStats');

class ListingSiteGlobalStats extends BaseListingSiteGlobalStats
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get the the global stats settings for a given period
     *
     * @param string $period
     * @param string $entity    V | T
     *
     * @return array
     */
    public function getSettings($period, $entity)
    {
        $result = [];

        // get last period
        $lastActiveGlobal = ListingSiteGlobalStats::model()->find(
            array(
                'order' => 'period_start DESC',
                'condition' => 'period_start <= :period AND per = :per',
                'params' => array(
                    ':period' => $period,
                    ':per' => $entity
                )
            )
        );

        // active global stats not found
        if (!$lastActiveGlobal) {
            echo "Couldn't find active global stats for the period " . $period . PHP_EOL;
        } else {
            // find stats for the active period
            $activePeriod = $lastActiveGlobal->period_start;
            $result = ListingSiteGlobalStats::model()->findAll(
                array(
                    'order' => 'estimated_page_views DESC',
                    'condition' => 'period_start = :period AND per = :per',
                    'params' => array(
                        ':period' => $activePeriod,
                        ':per' => $entity
                    )
                )
            );
        }

        return $result;
    }

}
