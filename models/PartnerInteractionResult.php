<?php

/**
 * This is the model class for table "partner_interaction_result".
 *
 * The followings are the available columns in table 'partner_interaction_result':
 * @property integer $id
 * @property integer $partner_session_id
 * @property string $interaction_type
 * @property string $result
 */
class PartnerInteractionResult extends MyActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @return PartnerInteractionResult the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'partner_interaction_result';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('partner_session_id, interaction_type, result', 'required'),
            array('partner_session_id', 'numerical', 'integerOnly' => true),
            array('interaction_type', 'length', 'max' => 45),
            array('result', 'length', 'max' => 500),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, partner_session_id, interaction_type, result', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'partner_session_id' => 'Partner Session',
            'interaction_type' => 'Interaction Type',
            'result' => 'Result',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('partner_session_id', $this->partner_session_id);
        $criteria->compare('interaction_type', $this->interaction_type, true);
        $criteria->compare('result', $this->result, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
