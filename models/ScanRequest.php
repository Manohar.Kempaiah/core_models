<?php

Yii::import('application.modules.core_models.models._base.BaseScanRequest');

class ScanRequest extends BaseScanRequest
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get score, rating and total reviews for a given month/year.
     * The total reviews is the total up to a given date, not just
     * the reviews for that year/month.
     *
     * @param integer $organizationId
     * @param string $year
     * @param string $month
     * @return array
     */
    public function getOrganizationTotalsForMonth($organizationId, $year, $month)
    {
        $sql = "
            SELECT
                ROUND(AVG(scan_stats.op_score)) AS avg_score,
                ROUND(AVG(scan_stats.aggregate_rating)) AS avg_aggregate_rating,
                MAX(scan_stats.total_reviews) AS max_reviews,
                scan_request.date_added AS last_scan_date
            FROM scan_request
                JOIN scan_stats
                    ON (scan_request.id = scan_stats.scan_request_id)
            WHERE
                scan_request.organization_id = :organization_id
                AND YEAR(scan_request.date_added) = :year
                AND MONTH(scan_request.date_added) = :month
            ORDER BY
                scan_request.date_added DESC
        ";

        return Yii::app()->db
            ->createCommand($sql)
            ->bindValues([
                ":organization_id" => $organizationId,
                ":year" => $year,
                ":month" => $month
            ])
            ->queryRow();
    }

    /**
     * Get an organization's scan website score
     *
     * @param integer $organizationId
     * @return integer
     */
    public function getOrganizationWebsiteStatus($organizationId)
    {
        $sql = "
            SELECT
                scan_stats.website_mobile
            FROM scan_request
                JOIN scan_stats
                    ON (scan_request.id = scan_stats.scan_request_id)
            WHERE
                scan_request.organization_id = :organization_id
            ORDER BY
                scan_request.date_added DESC
            LIMIT 1
        ";

        $result = Yii::app()->db
            ->createCommand($sql)
            ->bindValues([
                ":organization_id" => $organizationId
            ])
            ->queryRow();

        if ($result && $result['website_mobile']) {
            return $result['website_mobile'];
        } else {
            return 0;
        }
    }

    /**
     * Get the total number of reviews of an organization's practice.
     * Also returns a detail for google and yelp.
     *
     * @param integer $organizationId
     * @return array
     */
    public function getOrganizationPracticeReviewTotals($organizationId)
    {
        $sql = "
            SELECT
                scan_stats_practice.google_reviews,
                scan_stats_practice.yelp_reviews,
                scan_stats_practice.total_reviews
            FROM scan_request
                JOIN scan_request_practice ON (scan_request_practice.scan_request_id = scan_request.id)
                JOIN scan_stats_practice ON (scan_request_practice.id = scan_stats_practice.scan_input_practice_id)
            WHERE
                scan_request.organization_id = :organization_id
            ORDER BY
                scan_request.date_added DESC
            LIMIT 1
        ";

        return Yii::app()->db
            ->createCommand($sql)
            ->bindValues([
                ":organization_id" => $organizationId
            ])
            ->queryRow();
    }

    /**
     * Get the total number of reviews of an organization's provider.
     * Also returns a detail for healthgrades.
     *
     * @param integer $organizationId
     * @return array
     */
    public function getOrganizationProviderReviewTotals($organizationId)
    {
        $sql = "
            SELECT
                scan_stats_provider.healthgrades_reviews,
                scan_stats_provider.total_reviews
            FROM scan_request
                JOIN scan_request_practice ON (scan_request_practice.scan_request_id = scan_request.id)
                JOIN scan_request_provider ON
                  (scan_request_provider.scan_request_practice_id = scan_request_practice.id)
                JOIN scan_stats_provider ON (scan_request_provider.id = scan_stats_provider.scan_input_provider_id)
            WHERE
                scan_request.organization_id = :organization_id
            ORDER BY
                scan_request.date_added DESC
            LIMIT 1
        ";

        return Yii::app()->db
            ->createCommand($sql)
            ->bindValues([
                ":organization_id" => $organizationId
            ])
            ->queryRow();
    }

    /**
     * Get an organization's result for a given month
     *
     * @param int $organizationId The organization ID
     * @param string $per practice (T), provider (V)
     * @param string $yearMonth Y-m (deaults to date('Y-m'))
     *
     * @return array
     * @throws Exception
     */
    public function getMonthResults($organizationId, $per, $yearMonth = '')
    {
        $yearMonth = $yearMonth ? $yearMonth : date('Y-m');
        $date = new DateTime($yearMonth);
        $from = $date->modify('first day of +0 month')->format('Y-m-d') . " 00:00:00";
        $to = $date->modify('last day of +0 month')->format('Y-m-d') . " 23:59:59";

        // Are we looking for practice results
        if ($per == 'T') {
            // Yes
            $tableMatch = "scan_result_practice";
            $groupBy = "scan_request.organization_id,
                scan_request_practice.practice_id,
                scan_result_practice.listing_type_id";
            $join = "INNER JOIN scan_result_practice
                        ON scan_result_practice.scan_request_practice_id = scan_request_practice.id
            ";
        } else {
            // No
            // Looking for provider resutls
            $tableMatch = "scan_result_provider";
            $groupBy = "scan_request.organization_id,
                scan_request_practice.practice_id,
                scan_request_provider.provider_id,
                scan_result_provider.listing_type_id";
            $join = "INNER JOIN scan_request_provider
                        ON scan_request_provider.scan_request_practice_id = scan_request_practice.id
                    INNER JOIN scan_result_provider
                        ON scan_result_provider.scan_request_provider_id = scan_request_provider.id
            ";
        }
        $selectFields = $groupBy;

        $sql = "SELECT DISTINCT
                $selectFields,
                scan_request.date_added,
                scan_request.id AS scan_request_id,
                $tableMatch.id,
                $tableMatch.match_name,
                $tableMatch.match_address,
                $tableMatch.match_phone
            FROM
                scan_request
                INNER JOIN scan_request_practice ON scan_request_practice.scan_request_id = scan_request.id
                $join
                INNER JOIN
                    (
                        SELECT DISTINCT
                          $selectFields,
                          MAX(scan_request.date_added) AS date_added
                        FROM
                          scan_request
                        INNER JOIN scan_request_practice ON scan_request_practice.scan_request_id = scan_request.id
                        $join
                        WHERE
                          scan_request.organization_id = :organization_id
                              AND scan_request.date_added BETWEEN :from_date AND :to_date
                        GROUP BY $groupBy
                    )
                _request ON
                    scan_request.organization_id = _request.organization_id
                    AND scan_request_practice.practice_id = _request.practice_id
                    AND $tableMatch.listing_type_id = _request.listing_type_id
                    AND scan_request.date_added = _request.date_added
            WHERE
                scan_request.organization_id = :organization_id2
        ";

        // Get the results for the previous SQL query
        $valuesToBind = [
            ":organization_id" => $organizationId,
            ":organization_id2" => $organizationId,
            ":from_date" => $from,
            ":to_date" => $to
        ];

        return Yii::app()->db
            ->createCommand($sql)
            ->bindValues($valuesToBind)
            ->queryAll();
    }

    /**
     * Get the latest results by organization
     *
     * @param int $organizationId The organization ID
     * @param string $per practice (T), provider (V)
     *
     * @return array
     * @throws Exception
     */
    public function getLatestResults($organizationId, $per)
    {
        return $this->getMonthResults($organizationId, $per);
    }

    /* Get a scan log by id joining with the specified account id.
     *
     * @param integer $scanLogId
     * @param integer $accountId
     * @param integer $partnerSiteId
     * @param boolean $isChannelClient
     * @return array
     */
    public function getAccountScanLogInfo($scanLogId, $accountId, $partnerSiteId, $isChannelClient)
    {
        $values = [
            ":scan_log_id" => $scanLogId
        ];

        if ($isChannelClient) {
            $mainCondition = "AND (
                organization.partner_site_id = :partner_site_id
                OR
                scan_request.partner_site_id = :partner_site_id_bis
            )";
            $values[':partner_site_id'] = $partnerSiteId;
            $values[':partner_site_id_bis'] = $partnerSiteId;
            $joins = "";
        } else {
            $joins = "
                LEFT JOIN organization_account ON (organization_account.organization_id = organization.id)
                LEFT JOIN account ON (organization_account.account_id = account.id)
            ";
            $mainCondition = "AND account.id = :account_id";
            $values[':account_id'] = $accountId;
        }

        $sql = "
            SELECT
                scan_log.id AS scan_log_id,
                organization.id AS organization_id
            FROM scan_request
                JOIN scan_log ON (scan_request.scan_log_id = scan_log.id)
                LEFT JOIN organization ON (scan_request.organization_id = organization.id)
                $joins
            WHERE
                scan_log.id = :scan_log_id
                $mainCondition
            ORDER BY
                scan_request.date_added DESC
            LIMIT 1
        ";

        return Yii::app()->db
            ->createCommand($sql)
            ->bindValues($values)
            ->queryRow();
    }

    /**
     * Get leads by partner
     * @param array $params
     * Eg:
     *   $params['partner_site_id']
     *   $params['from_date']
     *   $params['to_date']
     *   $params['page']
     *   $params['page_size']
     *
     * @return array
     */
    public function getLeadsByPartner($params = array())
    {
        if (empty($params['partner_site_id'])) {
            return null;
        }
        if (empty($params['from_date'])) {
            $params['from_date'] = TimeComponent::getFirstDay();
        }
        if (empty($params['to_date'])) {
            $params['to_date'] = TimeComponent::getNow();
        }

        $scanWeb = Yii::app()->params['servers']['scan_web'] . '/historical?id=';

        $useCache = !empty($params['use_cache']) ? $params['use_cache'] : 1;
        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;

        $page = !empty($params['page']) ? $params['page'] : 1;
        // page has a negative number?
        $page = $page < 1 ? 1 : $page;

        // count the scan_request
        $sql = sprintf(
            "SELECT COUNT(1)
            FROM scan_request
            WHERE
                partner_site_id = %d
                AND date_added BETWEEN '%s' AND '%s'",
            $params['partner_site_id'],
            $params['from_date'] . ' 00:00:01',
            $params['to_date'] . ' 23:59:59'
        );
        $totalRows = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryScalar();

        // Prepare the offset
        $pageSize = !empty($params['page_size']) ? $params['page_size'] : $totalRows;
        if ($pageSize > $totalRows) {
            $pageSize = $totalRows;
        }
        $offset = ($page -1) * $pageSize;

        $sql = sprintf(
            "SELECT
            scan_request_practice.practice_name,
            scan_request_practice.address,
            scan_request_practice.zipcode,
            scan_request_practice.phone_number,
            scan_request.email,
            scan_request_practice.website,
            group_concat(scan_request_provider.provider_name) as providers,
            concat('%s',scan_request.scan_log_id) as historical_url,
            scan_request.date_added
            FROM
                scan_request
                INNER JOIN scan_request_practice
                    ON scan_request_practice.scan_request_id = scan_request.id
                INNER JOIN scan_request_provider
                    ON scan_request_provider.scan_request_practice_id = scan_request_practice.id
            WHERE
                partner_site_id = %d
                AND date_added BETWEEN '%s' AND '%s'
            GROUP BY
                scan_request.id
            LIMIT %d, %d;",
            $scanWeb,
            $params['partner_site_id'],
            $params['from_date'] . ' 00:00:01',
            $params['to_date'] . ' 23:59:59',
            $offset,
            $pageSize
        );

        $scans = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
        return [
            'scan_requests' => $scans,
            'current_page' => $page,
            'page_size' => $pageSize,
            'total_rows' => $totalRows
        ];
    }


}
