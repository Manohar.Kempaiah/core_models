<?php

Yii::import('application.modules.core_models.models._base.BaseFriendlyUrlHistory');

class FriendlyUrlHistory extends BaseFriendlyUrlHistory
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
