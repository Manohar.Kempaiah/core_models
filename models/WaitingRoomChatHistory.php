<?php

Yii::import('application.modules.core_models.models._base.BaseWaitingRoomChatHistory');

class WaitingRoomChatHistory extends BaseWaitingRoomChatHistory
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Read the chat from Twilio and save in our db
     * @param string $guid
     *
     */
    public static function saveHistory($guid = '')
    {

        // validate guid is not empty
        if (empty($guid)) {
            $response = array(
                'success' => false,
                'error' => 'Missing required field: guid',
                'data' => '',
            );

            // log the error so we can be aware and try to fix it later
            Yii::log('curl error on WaitingRoomChatHistory:'. json_encode($response));

            return;
        }
        // $guid = 'BBE5C7F0-948D-46DE-967E-7313FAE29558';

        // find invite
        $invite = WaitingRoomInvite::model()->find("guid = :guid", array(":guid" => $guid));
        if (!$invite) {
            $response = array(
                'success' => false,
                'error' => 'Waiting Rooom Invite not found',
                'data' => '',
            );

            // log the error so we can be aware and try to fix it later
            Yii::log('curl error on WaitingRoomChatHistory:'. json_encode($response));


            return;
        }

        // open the company directory
        $userPwd = Yii::app()->params['twilio_account_sid_prod'] . ':' . Yii::app()->params['twilio_auth_token_prod'];

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, 'https://chat.twilio.com/v2/Services');
        curl_setopt($curl, CURLOPT_USERPWD, $userPwd);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);

        if (!$json) {

            // log the error so we can be aware and try to fix it later
            Yii::log('curl error on WaitingRoomChatHistory:'. curl_error($curl));

            echo curl_error($curl) . "\n";
            return false;
        }
        curl_close($curl);
        $data = json_decode($json, true);

        if (empty($data['services'][0]['links']['channels'])) {
            $response = array(
                'success' => false,
                'error' => 'There no chat channels',
                'data' => '',
            );

            // log the error so we can be aware and try to fix it later
            Yii::log('Empty channels sending WaitingRoomChatHistory:'. json_encode($response));

            return;
        }

        // Read the channel
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $data['services'][0]['links']['channels'] . '/' . $guid);
        curl_setopt($curl, CURLOPT_USERPWD, $userPwd);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $json = curl_exec($curl);
        if (!$json) {
            $error = curl_error($curl) . "\n";

            $response = array(
                'success' => false,
                'error' => 'Curl error',
                'data' => $error,
            );

            // log the error so we can be aware and try to fix it later
            Yii::log('curl error on WaitingRoomChatHistory:'. json_encode($response));

            ApiComponent::displayResults($response);
            return;
        }
        curl_close($curl);
        $channel = json_decode($json, true);

        // Read the messages
        if (empty($channel['links']['messages'])) {
            $response = array(
                'success' => false,
                'error' => 'There no messages for this chat channel',
                'data' => '',
            );

            // log the error so we can be aware and try to fix it later
            Yii::log('curl error on WaitingRoomChatHistory:'. json_encode($response));

            ApiComponent::displayResults($response);
            return;
        }

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $channel['links']['messages']);
        curl_setopt($curl, CURLOPT_USERPWD, $userPwd);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        $messages = curl_exec($curl);
        if (!$messages) {
            $error = curl_error($curl) . "\n";
            $response = array(
                'success' => false,
                'error' => 'Curl error',
                'data' => $error,
            );

            // log the error so we can be aware and try to fix it later
            Yii::log('curl error on WaitingRoomChatHistory:'. json_encode($response));

            return;
        }
        curl_close($curl);
        $messages = json_decode($messages, true);
        $chat = array(
            'channel' => $channel,
            'messages' => $messages
        );

        // Check if exists
        $chatHistory = WaitingRoomChatHistory::model()->find(
            "waiting_room_invite_id=:waiting_room_invite_id",
            array(":waiting_room_invite_id" => $invite->id)
        );

        if (empty($chatHistory)) {
            $chatHistory = new WaitingRoomChatHistory();
            $chatHistory->waiting_room_invite_id = $invite->id;
        }
        $chatHistory->messages_count = !empty($channel['messages_count']) ? $channel['messages_count'] : 0;
        $chatHistory->chat = json_encode($chat);
        $chatHistory->date_added = TimeComponent::getNow();
        $chatHistory->save();

        return $chatHistory->messages_count;
    }

    /**
     * Get chat history
     * @param string $guid
     *
     */
    public static function getHistory($guid = '')
    {

        // validate guid is not empty
        if (empty($guid)) {
            return array(
                'success' => false,
                'error' => 'Missing required field: guid',
                'data' => '',
            );
        }

        // find invite
        $invite = WaitingRoomInvite::model()->find("guid = :guid", array(":guid" => $guid));
        if (!$invite) {
            return array(
                'success' => false,
                'error' => 'Waiting Rooom Invite not found',
                'data' => '',
            );
        }

        $chatHistory = WaitingRoomChatHistory::model()->find(
            array(
                "condition" => "waiting_room_invite_id = " . $invite->id,
                "order" => "date_added DESC"
            )
        );
        $data = (array) $chatHistory->attributes;
        $data['chat'] = json_decode($data['chat'], true);
        return array(
            'success' => true,
            'error' => '',
            'data' => $data,
        );
    }
}
