<?php

Yii::import('application.modules.core_models.models._base.BaseProviderEducation');

class ProviderEducation extends BaseProviderEducation
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Create education_name entry if necessary before saving
     * @return boolean
     */
    public function beforeSave()
    {
        if (empty($this->education_name_id)) {

            // Look for education name into parameters
            $educationName = isset($_POST['education_name_id_save']) ? $_POST['education_name_id_save'] : '';
            if (empty($educationName)) {
                $educationName =
                    isset($_POST['education_name_id_lookup']) ? $_POST['education_name_id_lookup'] : '';
            }

            // No education name (required parameter)
            if (empty($educationName)) {
                $this->addError("education_name_id", "No education_name_id provided");
                return false;
            }

            // Check if education name already existed
            $educationNameId = EducationName::model()->find('name=:name', array(':name' => $educationName));

            // Does the education name exist?
            // Yes
            if ($educationNameId) {
                $this->education_name_id = $educationNameId->id;
            } else {
                // Education name doesn't exist, so create it
                $education_name = new EducationName;
                $education_name->name = $educationName;

                if ($education_name->save()) {
                    $this->education_name_id = $education_name->id;
                } else {
                    $this->addError("id", "Couldn't save education.");
                    return false;
                }
            }

        }

        if ($this->education_type_id == 0) {

            $educationType = isset($_POST['education_type_id_save']) ? $_POST['education_type_id_save'] : '';

            if ($educationType == '') {
                $educationType = isset($_POST['education_type_id_lookup']) ?
                    $_POST['education_type_id_lookup'] : '';
            }

            if ($educationType != "") {

                // check if already existed
                $educationTypeId = EducationType::model()->find('name=:name', array(':name' =>
                    $educationType));
                if ($educationTypeId) {
                    $this->education_type_id = $educationTypeId->id;
                } else {
                    $educationType = new EducationType;
                    $educationType->name = $educationType;
                    $educationType->display_name = $educationType;

                    if ($educationType->save()) {
                        $this->education_type_id = $educationType->id;
                    } else {
                        $this->addError("id", "Couldn't save education type.");
                        return false;
                    }
                }
            }
        }

        if ($this->started_year && $this->graduation_year && $this->graduation_year < $this->started_year) {
            $this->addError("graduation_year", "Graduation year cannot be earlier than started year.");
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id='
            . $this->provider_id . '" rel="/administration/providerTooltip?id=' . $this->provider_id
            . '" title="' . $this->provider . '">' . $this->provider . '</a>');
    }

    /**
     * Get the list of schools or universities a provider has attended from provider_education
     * @param int $providerId
     * @return array
     */
    public static function getDetails($providerId, $cache = true)
    {

        /**
         * NOTE: we use name instead of display_name here because display_name is intended for display for selecting the
         * item, whereas name is intended for display as it is actually listed
         */
        $sql = sprintf(
            "SELECT pe.id, pe.provider_id, pe.education_type_id, pe.education_name_id,
                pe.area_of_focus, pe.started_year, pe.graduation_year,
                education_type.name AS education_type, education_name.name AS education_name
                FROM provider_education AS pe
                INNER JOIN education_type
                ON pe.education_type_id = education_type.id
                INNER JOIN education_name
                ON pe.education_name_id = education_name.id
                WHERE pe.provider_id = %d;",
            $providerId
        );

        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;
        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Save Data
     * @param string $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        if (empty($postData)) {
            return $results;
        }

        foreach ($postData as $v) {

            if (isset($v['backend_action']) && $v['backend_action'] == 'delete' && $v['id'] > 0) {
                $model = ProviderEducation::model()->findByPk($v['id']);
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_EDUCATION";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }
                    $auditSection = 'Provider Education id #' . $providerId .' (' . $model->education_name_id . ')';
                    AuditLog::create('D', $auditSection, 'provider_education', null, false);
                }

            } else {

                $model = false;
                if ($v['id']) {
                    $model = ProviderEducation::model()->find(
                        'provider_id=:provider_id AND id=:id',
                        array(':provider_id' => $providerId, ':id' => $v['id'])
                    );
                    $auditAction = 'U';
                }
                if (!$model) {
                    $auditAction = 'C';
                    $model = new ProviderEducation;
                    $model->provider_id = $providerId;
                }

                $model->education_type_id = $v['education_type_id'];
                $model->education_name_id = $v['education_name_id'];
                $model->area_of_focus = $v['area_of_focus'];
                $model->started_year = $v['started_year'];
                $model->graduation_year = $v['graduation_year'];
                if ($model->graduation_year == 'yyyy') {
                    $model->graduation_year = null;
                }

                if ($model->save()) {

                    $auditSection = 'Provider Education #' . $providerId . ' (#' . $v['education_name_id'] . ')';
                    AuditLog::create($auditAction, $auditSection, 'provider_education', null, false);

                    $results['success'] = true;
                    $results['error'] = "";
                    $results['data'][] = (array) $model->attributes;
                } else {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_EDUCATION";
                    $results['data'][] = $model->getErrors();
                }
            }
        }
        return $results;
    }
}
