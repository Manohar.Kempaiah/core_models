<?php
Yii::import('application.modules.core_models.models._base.BaseInsuranceCompany');

class InsuranceCompany extends BaseInsuranceCompany
{
    const OPTUM_ID = 346;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Override rules from base model
     * {@inheritDoc}
     * @see BaseInsuranceCompany::rules()
     */
    public function rules()
    {
        $rules = parent::rules();
        array_pop($rules);
        $rules[] = array(
            'id, name, friendly_url, visits_number',
            'safe',
            'on' => 'search'
        );

        return $rules;
    }

    /**
     * Override search from base model
     * {@inheritDoc}
     * @see BaseInsuranceCompany::search()
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('internal_name', $this->internal_name, true);

        $sort = new CSort();
        $sort->attributes = array(
            'internal_name' => array(
                'asc' => 'internal_name',
                'desc' => 'internal_name desc'
            ),
            'name' => array(
                'asc' => 'name',
                'desc' => 'name desc'
            )
        );
        $sort->defaultOrder = 'internal_name';

        return new CActiveDataProvider(
            get_class($this),
            array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 100
                ),
                'sort' => $sort
            )
        );
    }

    /**
     * Override attribute labels from base model
     * {@inheritDoc}
     * @see BaseInsuranceCompany::attributeLabels()
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['friendly_url_redirected'] = Yii::t('app', 'Redirected URLs');

        return $labels;
    }

    /**
     * Search the "friendly_url_redirected" field in insurance_company for old URLs that need to be redirected
     *
     * @todo this is unused, but this logic may come in handy -- check with @andreiz + @davidm
     * @param string $url
     * @return array
     */
    public function searchRedirects($url)
    {
        if (trim($url) == '') {
            return false;
        }

        // handle insurances that were wiped from the DB
        $deletedInsurances = [];
        $deletedInsurances[] = 'American_Medical_Security';
        $deletedInsurances[] = 'Banker_s_Fidelity_Life_Insurance_Company';
        $deletedInsurances[] = 'Clarendon';
        $deletedInsurances[] = 'CNA_Highway_to_Health';
        $deletedInsurances[] = 'Cole_Managed_Vision';
        $deletedInsurances[] = 'Destiny_Health';
        $deletedInsurances[] = 'Fairmont_Specialty_Group';
        $deletedInsurances[] = 'Fortis_Health';
        $deletedInsurances[] = 'Health_Plan_Administrators';
        $deletedInsurances[] = 'Intermountain_Health_Care_IHC_';
        $deletedInsurances[] = 'John_Alden_Insurance_Company';
        $deletedInsurances[] = 'Kanawha_Healthcare_Solutions_KHS_';
        $deletedInsurances[] = 'Labor_Industry';
        $deletedInsurances[] = 'Midwest_Security';
        $deletedInsurances[] = 'Monumental_Life';
        $deletedInsurances[] = 'Multiflex_Dental_Security_Life';
        $deletedInsurances[] = 'Principal_Life_Insurance_Company';
        $deletedInsurances[] = 'QualChoice';
        $deletedInsurances[] = 'Security_Life';
        $deletedInsurances[] = 'Torchmark';
        $deletedInsurances[] = 'United_Wisconsin_Life_Insurance_Company';

        if (array_search($url, $deletedInsurances) !== false) {
            header('HTTP/1.1 301 Moved Permanently');
            header('Location:search-by-health-insurance');
            Yii::app()->end();
        }

        // handle insurances that were redirected to another company
        $sql = sprintf(
            "SELECT id, name, friendly_url,
            visits_number
            FROM insurance_company
            WHERE friendly_url_redirected
            LIKE '%%@%s@%%'
            LIMIT 1;",
            addslashes($url)
        );
        $cls= InsuranceCompany::model()->cache(Yii::app()->params['cache_long'])->findBySql($sql);

        if ($cls) {
            return $cls;
        }

        // last try: search in our insurances table for insurances themselves instead of companies
        $sql = sprintf(
            "SELECT insurance_company.id, insurance_company.name, insurance_company.friendly_url,
            insurance_company.visits_number
            FROM insurance
            INNER JOIN insurance_company
            ON insurance.company_id = insurance_company.id
            WHERE insurance.friendly_url
            LIKE '%%%s%%'
            LIMIT 1;",
            addslashes($url)
        );

        return Insurance::model()->cache(Yii::app()->params['cache_long'])->findBySql($sql);
    }

    /**
     * Get an array with the ids of the companies with the same name than the provided one
     *
     * @param int $companyId
     * @param bool $returnList
     * @return array
     */
    public static function getSibblingCompanyIds($companyId = 0, $returnList = false)
    {
        $sql = sprintf(
            "SELECT c.id
            FROM insurance_company c
            WHERE c.name =
            (SELECT c2.name
            FROM insurance_company c2
            WHERE c2.id = %d)",
            $companyId
        );

        $arrCompanyIds = Yii::app()->dbRO->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryColumn();
        if (empty($arrCompanyIds)) {
            $arrCompanyIds[] = $companyId;
        }

        if (Common::isTrue($returnList)) {
            $list = '';

            foreach ($arrCompanyIds as $companyId) {
                $list .= $companyId . ',';
            }
            $arrCompanyIds = rtrim($list, ',');
        }
        return $arrCompanyIds;
    }

    /**
     * Get an array with the base insurance company ids
     *
     * @return array
     */
    public static function getMainCompanyIds()
    {
        $sql = sprintf(
            "SELECT MIN(c.id) as id
            FROM insurance_company c
            GROUP BY c.name
            ORDER BY id ASC");
        return Yii::app()->db->cache(7 * Yii::app()->params['cache_long'])
            ->createCommand($sql)
            ->queryColumn();
    }

    /**
     * Get an array with the the base insurance companies
     *
     * @return array
     */
    public static function getMainCompanies()
    {
        $arrIds = self::getMainCompanyIds();

        $sql = sprintf("SELECT * FROM insurance_company WHERE id IN (%s)", implode(',', $arrIds));

        return InsuranceCompany::model()->cache(7 * Yii::app()->params['cache_long'])->findAllBySql($sql);
    }

    /**
     * Get an array with the plans of the company group
     * @return array
     */
    public function getGroupPlans()
    {
        $arrCompanyIds = InsuranceCompany::getSibblingCompanyIds($this->id, true);
        if (!empty($arrCompanyIds)) {
            $sql = sprintf(
                "SELECT MIN(id)
                FROM insurance
                WHERE company_id IN (%s)
                GROUP BY name
                ORDER BY name ASC;",
                $arrCompanyIds
            );

            $arrIds = Yii::app()->db->cache(7 * Yii::app()->params['cache_long'])->createCommand($sql)->queryColumn();
            if (!empty($arrIds)) {
                $sql = sprintf("SELECT * FROM insurance WHERE id IN (%s)", implode(',', $arrIds));
                return Insurance::model()->cache(7 * Yii::app()->params['cache_long'])->findAllBySql($sql);
            }
        }
        return array();
    }

    /**
     * Return all the plans of all the base companies
     * @return type
     */
    public static function getAllMainCompanyPlans()
    {
        $arrCompanies = self::getMainCompanies();
        $result = array();
        foreach ($arrCompanies as $company) {
            $plans = $company->getGroupPlans();
            foreach ($plans as $plan) {
                $result[]= $plan;
            }
        }
        return $result;
    }

    /**
     * Parse a comanyId value and returns the base company_id or null
     *
     * @param mixed $companyId
     * @return mixed
     * @todo This function could be deleted
     */
    public static function parseCompanyId($companyId)
    {
        if (is_numeric($companyId)) {
            return (int) $companyId;
        } elseif (is_string($companyId)) {
            if (strpos($companyId, ' ') !== false) {
                $arrId = explode(' ', $companyId);
            } elseif (strpos($companyId, ',') !== false) {
                $arrId = explode(',', $companyId);
            } elseif (strpos($companyId, '+') !== false) {
                $arrId = explode('+', $companyId);
            }

            if (empty($arrId)) {
                return null;
            } else {
                sort($arrId);
                return $arrId[0];
            }
        } elseif (is_array($companyId)) {
            sort($companyId);
            return (int) $companyId[0];
        }

        return null;
    }

    /**
     * Generate a Friendly URL given an Insurance company name
     *
     * @param string $companyName
     * @param int $companyId
     * @return string
     */
    public static function getFriendlyUrl($companyName, $companyId = 0)
    {
        $url = StringComponent::prepareNewFriendlyUrl($companyName);
        return StringComponent::getNewFriendlyUrl($url, $companyId, 'insurance_company');
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from insurance_company if associated records exist in insurance table
        $has_associated_records = Insurance::model()->exists(
            'company_id=:company_id',
            array(':company_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Company can't be deleted because there are plans linked to it.");
            return false;
        }

        // avoid deleting from insurance_company if associated records exist in insurance_company_alias table
        $has_associated_records = InsuranceCompanyAlias::model()->exists(
            'company_id=:company_id',
            array(':company_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError('id', "Company can't be deleted because there are aliases linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Return the list of company associated to one provider and one practice
     * or the list of company associated to one provider
     *
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public function insurerList($providerId = 0, $practiceId = 0)
    {
        if ($providerId > 0) {
            if ($practiceId == 0) {
                $sql = sprintf(
                    "SELECT pi.company_id AS id, ic.name AS text
                    FROM provider_insurance AS pi
                    LEFT JOIN insurance_company AS ic ON ic.id = pi.company_id
                    WHERE pi.provider_id = '%s' GROUP BY pi.company_id;",
                    $providerId
                );
            } else {
                $sql = sprintf(
                    "SELECT ppi.company_id AS id, ic.name AS text
                    FROM provider_practice_insurance AS ppi
                    LEFT JOIN insurance_company AS ic ON ic.id = ppi.company_id
                    WHERE ppi.provider_id = '%s' AND ppi.practice_id = '%s'
                    GROUP BY ppi.company_id;",
                    $providerId,
                    $practiceId
                );
            }
        } else {
            $sql = sprintf(
                "SELECT '-1' AS id, 'Don\'t have insurance' as text
                UNION
                (SELECT id, name as text
                FROM insurance_company
                ORDER BY text ASC);");
        }

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the minimum id for each company name
     *
     * @param string $name
     * @return array
     */
    public static function findAllByName($name)
    {
        $sql = sprintf(
            "SELECT MIN(insurance_company.id) AS id,
            insurance_company.name as name
            FROM insurance_company
            INNER JOIN insurance_company_alias
            ON insurance_company_alias.company_name = insurance_company.name
            INNER JOIN insurance
            ON insurance.company_id = insurance_company.id
            WHERE insurance_company_alias.alias_name LIKE '%s%%'
            OR insurance_company.name LIKE '%s%%'
            GROUP BY insurance_company.name
            ORDER BY is_popular DESC, name ASC",
            addslashes($name),
            addslashes($name)
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])
            ->createCommand($sql)
            ->queryAll();
    }

    /**
     * Find insurance's IDs looking up their names or their parent companies'
     *
     * @param string $q
     * @return array
     */
    public function findByName($q)
    {
        $q = addslashes($q);

        $sql = sprintf(
            "SELECT insurance.id
            FROM insurance
            INNER JOIN insurance_company
            ON insurance.company_id = insurance_company.id
            WHERE insurance_company.name LIKE '%%%s%%';",
            $q
        );

        $assocArray = Yii::app()->db->createCommand($sql)->queryAll();
        $array = array();
        foreach ($assocArray as $v) {
            array_push($array, $v['id']);
        }
        return $array;
    }

    /**
     * Get companies to be shown in the footer of most pages
     *
     * @return array
     */
    public static function getInsuranceCompaniesForFooter()
    {
        $sql = "SELECT SQL_NO_CACHE curr.name, curr.friendly FROM
            (SELECT DISTINCT name, CONCAT('insurance/', friendly_url, '-Doctors') AS friendly, SUM(visits_number) AS vn
            FROM insurance_company
            GROUP BY name, friendly_url
            ORDER BY vn DESC
            LIMIT 20) curr
            ORDER BY curr.name;";
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])
            ->createCommand($sql)
            ->queryAll();
    }

    /**
     * After Save
     * @return boolean
     */
    public function afterSave()
    {
        $alias = InsuranceCompanyAlias::model()->exists(
            'company_name =:company_name',
            array(':company_name' => $this->name)
        );

        if (!$alias) {

            $iCA = new InsuranceCompanyAlias();
            $iCA->company_name = $this->name;
            $iCA->alias_name = $this->name;
            $iCA->company_id = null;
            $iCA->marketplace_category = null;
            $iCA->state_abbreviation = null;
            $iCA->issuer_id = '';
            $iCA->save();
        }

        return parent::afterSave();
    }

    /**
     * Before Save
     * @return boolean
     */
    public function beforeSave()
    {
        $duplicateUrlSubmitted = false;

        // is this an existing record?
        if ($this->id > 0) {
            // yes, validate name
            $duplicateName = InsuranceCompany::model()->exists(
                'internal_name = :internalName AND id != :thisId',
                array(':internalName' => $this->internal_name, ':thisId' => $this->id)
            );
        } else {
            // no, validate name
            $duplicateName = InsuranceCompany::model()->exists(
                'internal_name = :internalName',
                array(':internalName' => $this->internal_name)
            );
        }
        // was the name a duplicate?
        if ($duplicateName) {
            // yes, exit
            $this->addError('internal_name', 'Duplicate internal name');
            return false;
        }

        // is this an existing record?
        if ($this->id > 0) {
            // yes, validate URLs
            $duplicateUrl = InsuranceCompany::model()->exists(
                'url_submitted = :urlSubmitted
                    AND url_providers = :urlProviders
                    AND url_plans = :urlPlans
                    AND id != :thisId',
                array(
                    ':urlSubmitted' => $this->url_submitted,
                    ':urlProviders' => $this->url_providers,
                    ':urlPlans' => $this->url_plans,
                    ':thisId' => $this->id
                )
            );
        } else {
            // no, validate URLs
            $duplicateUrl = InsuranceCompany::model()->exists(
                'url_submitted = :urlSubmitted AND url_providers = :urlProviders AND url_plans = :urlPlans',
                array(
                    ':urlSubmitted' => $this->url_submitted,
                    ':urlProviders' => $this->url_providers,
                    ':urlPlans' => $this->url_plans
                )
            );
        }
        // was one of the URLs a duplicate?
        if ($duplicateUrl) {
            // yes, exit
            $this->addError('url_submitted', 'Duplicate URL Submitted');
            return false;
        }

        // do we have only the url_submitted field?
        if ($this->url_providers == "" && $this->url_plans == "") {
            // yes: is this an existing record?
            if ($this->id > 0) {
                // yes, validate URLs
                if ($this->url_providers == "" && $this->url_plans == "") {
                    $duplicateUrlSubmitted = InsuranceCompany::model()->exists(
                        'url_submitted = :urlSubmitted AND id != :thisId',
                        array(':urlSubmitted' => $this->url_submitted, ':thisId' => $this->id)
                    );
                }
            } else {
                // no, validate URLs
                if ($this->url_providers == "" && $this->url_plans == "") {
                    $duplicateUrlSubmitted = InsuranceCompany::model()->exists('url_submitted = :urlSubmitted ', array(
                        ':urlSubmitted' => $this->url_submitted
                    ));
                }
            }
        }

        // was url_submitted a duplicate?
        if ($duplicateUrlSubmitted) {
            $this->addError('url_submitted', 'Duplicate URL Submitted');
            return false;
        }

        // is this an existing record?
        if ($this->id > 0) {
            $newName = $this->name;
            // did the name change?
            if (!empty($this->oldRecord->name) && $this->name != $this->oldRecord->name) {
                // yes: find by name
                $arrICAlias = InsuranceCompanyAlias::model()->findAll(
                    '(company_name = :companyName AND company_id IS NULL) OR company_id = :companyId',
                    array(
                        ':companyName' => $this->oldRecord->name,
                        ':companyId' => $this->id
                    )
                );
                // did we find records?
                if (!empty($arrICAlias)) {
                    // yes, update them
                    foreach ($arrICAlias as $iCAlias) {
                        $iCAlias->company_name = $newName;
                        $iCAlias->alias_name = $newName;
                        $iCAlias->save();
                    }
                }
            }
        }

        // generate a friendly url if it's empty
        if (empty($this->friendly_url)) {
            $this->friendly_url = self::getFriendlyUrl($this->name);
        }

        return parent::beforeSave();
    }

    /**
     * Get all insurances with their company type.
     *
     * @param string $name
     * @return array
     */
    public function getInsuranceByCompanyType($name = null)
    {
        // filter by name if was specified
        if ($name) {
            $sql = '
                SELECT DISTINCT
                    insurance_company.id AS insurance_company_id,
                    insurance_company.internal_name AS insurance_company_name,
                    CASE
                        WHEN insurance.name LIKE "%medicare%"
                            THEN "Medicare"
                        WHEN insurance.name LIKE "%medicaid%"
                            THEN "Medicaid"
                        ELSE
                            "Commercial insurance"
                        END AS insurance_company_type
                    FROM insurance
                    INNER JOIN insurance_company ON insurance_company.id = insurance.company_id
                    WHERE insurance.name LIKE :name
                    ORDER BY insurance_company_name;
            ';

            return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll(
                true,
                [':name' => '%' . $name . '%']
            );
        }

        // return all
        $sql = '
            SELECT DISTINCT
                insurance_company.id AS insurance_company_id,
                insurance_company.internal_name AS insurance_company_name,
                CASE
                    WHEN insurance.name LIKE "%medicare%"
                        THEN "Medicare"
                    WHEN insurance.name LIKE "%medicaid%"
                        THEN "Medicaid"
                    ELSE
                        "Commercial insurance"
                    END AS insurance_company_type
                FROM insurance
                INNER JOIN insurance_company ON insurance_company.id = insurance.company_id
                ORDER BY insurance_company_name;
        ';

        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }
}
