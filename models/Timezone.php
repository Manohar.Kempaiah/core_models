<?php

Yii::import('application.modules.core_models.models._base.BaseTimezone');

class Timezone extends BaseTimezone
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Converts a DB timezone eg: UTC-7 to a valid php time zone
     * @param $timeZone
     * @return mixed
     */
    public static function convertToPhpTimeZone($timeZone)
    {
        $phpTimeZones = array(
            "EST" => "America/New_York",
            "CST" => "America/Chicago",
            "MST" => "America/Denver",
            "PST" => "America/Los_Angeles",
            "AKST" => "America/Anchorage",
            "HI" => "Pacific/Honolulu",
            "AST" => "America/Puerto_Rico",
            "UTC-7" => "America/Phoenix"
        );

        // If the time zone exists
        if (isset($phpTimeZones[$timeZone])) {
            return $phpTimeZones[$timeZone];
        }

        return $timeZone;
    }

}
