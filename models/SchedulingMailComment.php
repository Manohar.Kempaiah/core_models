<?php

Yii::import('application.modules.core_models.models._base.BaseSchedulingMailComment');

class SchedulingMailComment extends BaseSchedulingMailComment
{
    public $note_edit;
    public $account_name;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get comments
     * @param int $schedulingMailId
     * @param int $accountId
     * @return array
     */
    public static function getComments($schedulingMailId = 0, $accountId = 0)
    {

        // this appointment exists and is mine ?
        $isMine = SchedulingMail::checkPermissions($schedulingMailId, $accountId);
        if (!$isMine) {
            $results['success'] = false;
            $results['error'] = "NOT_IS_MINE";
            return $results;
        }

        $sql = sprintf(
            "SELECT id, note, date_added
            FROM scheduling_mail_comment
            WHERE scheduling_mail_id = %d
            ORDER BY date_added ASC",
            $schedulingMailId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Save appointment Comments
     * @param array $postData
     * @param int $accountId
     * @return array
     */
    public static function saveData($postData = array(), $accountId = 0)
    {
        $results['success'] = true;
        $results['error'] = '';
        $results['data'] = [];

        if (empty($postData) || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        if (empty($postData['note'])) {
            $results['success'] = false;
            $results['error'] = "EMPTY_NOTE";
            return $results;
        }

        if (empty($postData['scheduling_mail_id']) || $postData['scheduling_mail_id'] < 1) {
            $results['success'] = false;
            $results['error'] = "EMPTY_SCHEDULING_MAIL_ID";
            return $results;
        }

        $schedulingMailId = $postData['scheduling_mail_id'];
        // this appointment exists and is mine ?
        $isMine = SchedulingMail::checkPermissions($schedulingMailId, $accountId);
        if (!$isMine) {
            $results['success'] = false;
            $results['error'] = "NOT_IS_MINE";
            return $results;
        }

        $schedulingMailCommentId = !empty($postData['id']) ? (int)$postData['id'] : null;
        $data = null;

        if ($schedulingMailCommentId) {
            // update note
            $data = SchedulingMailComment::model()->find(
                'id=:id AND scheduling_mail_id=:scheduling_mail_id',
                array(":id" => $schedulingMailCommentId, ':scheduling_mail_id' => $schedulingMailId)
            );
        }
        if (!$data) {
            // create new note
            $data = new SchedulingMailComment();
            $data->scheduling_mail_id = $schedulingMailId;
            $data->account_id = $accountId;
            $data->date_added = date("Y-m-d H:i:s");
        }

        $data->note = StringComponent::sanitize($postData['note']);
        $data->date_updated = date("Y-m-d H:i:s");

        if ($data->save()) {
            $results['success'] = true;
            $results['http_response_code'] = 201;
            $results['error'] = "";
            $results['data'][] = $data->attributes;
        } else {
            $results['success'] = false;
            $results['error'] = "ERROR_SAVING_APPOINTMENT_COMMENTS";
            $results['data'] = $data->getErrors();
            return $results;
        }
        return $results;
    }
}
