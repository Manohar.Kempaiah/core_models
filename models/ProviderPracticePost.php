<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticePost');

class ProviderPracticePost extends BaseProviderPracticePost
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Create a new post in the provider_practice_post table based on a provider_rating review
     * This $providerRatingObj will always have provider_id and practice_id
     * @param ProviderRating $providerRatingObj
     * @return bool
     */
    public static function createGooglePostFromReview($providerRatingObj)
    {

        // a review is required
        if (!($providerRatingObj instanceof ProviderRating)) {
            // no review received, leave
            return false;
        }

        // do we have a valid provider and practice?
        if (empty($providerRatingObj->provider_id) || empty($providerRatingObj->practice_id)) {
            // no, leave
            return false;
        }

        // instantiate a new post
        $ppp = new ProviderPracticePost();
        $ppp->provider_id = $providerRatingObj->provider_id;
        $ppp->practice_id = $providerRatingObj->practice_id;
        // we use the magic account_id = 1 to indicate this was an automatic process
        $ppp->added_by_account_id = 1;
        $ppp->post_status = 'published';

        // url to book appointment
        $ppp->post_offer_url = 'https://' . Yii::app()->params->servers['dr_pat_hn'] .
            '/directAppointment?provider=' . $ppp->provider_id . '&practice=' . $ppp->practice_id .
            '&partner=GooglePosts';

        // build image path: use teal by default
        // count p@p posts
        $olderPostCount = ProviderPracticePost::model()->count(
            'provider_id = :providerId AND practice_id = :practiceId',
            array(':providerId' => $ppp->provider_id, ':practiceId' => $ppp->practice_id)
        );

        $imageIndex = 1;
        // check number of old posts - the hardcoded numbers match the right images in s3
        if ($olderPostCount % 4 == 0) {
            // use blue
            $imageIndex = 4;
        } elseif ($olderPostCount % 3 == 0) {
            // use orange
            $imageIndex = 3;
        } elseif ($olderPostCount % 2 == 0) {
            // use violet
            $imageIndex = 2;
        }
        $ppp->post_image = 'https://assets.doctor.com/email/Book-now-' . $providerRatingObj->rating . '-rating-' .
            $imageIndex . '.png';

        // build body
        $ppp->post_body = (
            str_word_count($providerRatingObj->description) < 280 ?
            $providerRatingObj->description :
            StringComponent::truncateWords($providerRatingObj->description, 280)
        );
        $ppp->post_body .= ' - ' . StringComponent::anonymizeReviewerName(
            $providerRatingObj->reviewer_name,
            $providerRatingObj->reviewer_name_anon
        );
        $ppp->post_body .= ' (verified patient)';

        // now that we've got everything ready, check for old instances of this post
        $oldPpps = ProviderPracticePost::model()->findAll(
            'provider_id = :providerId
            AND practice_id = :practiceId
            AND post_body = :postBody',
            array(
                ':providerId' => $ppp->provider_id,
                ':practiceId' => $ppp->practice_id,
                ':postBody' => $ppp->post_body
            )
        );

        // did we find old posts?
        if ($oldPpps) {
            // yes, do not create this post as it'd be a duplicate
            return false;
        }

        // actually save the new post
        return $ppp->save();
    }

}
