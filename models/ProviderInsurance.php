<?php

/**
 * @important In case of commiting changes of ProviderInsurance to the pdi, check compatibility with
 * MyAccountController::actionProviderInsurances
 */
Yii::import('application.modules.core_models.models._base.BaseProviderInsurance');

class ProviderInsurance extends BaseProviderInsurance
{

    /**
     * Set to true before calling delete() to avoid removal of provider_practice_insurance records.
     * This is useful on ingestion process.
     * @var boolean
     */
    public $skipProviderPracticeInsuranceRemoval = false;

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Sets value for date_added field.
     * @return boolean
     */
    public function beforeValidate()
    {
        // adds the date_added only for new records and avoid overwrite any value set from actions
        if (empty($this->id) && empty($this->date_added)) {
            $this->date_added = date('Y-m-d H:i:s');
        }

        return parent::beforeValidate();
    }

    /**
     * Keep consistency between company_id and insurance's company_id
     *
     * @return boolean
     */
    public function beforeSave()
    {
        // Force the ProviderInsurance->company_id to be consistent with the Insurance->company_id
        // This needs to be done since the PADM sends only one insurance company,
        // but with many plans that belong to the given company and its siblings.
        $insurance = Insurance::model()->cache(Yii::app()->params['cache_medium'])->findByPk($this->insurance_id);
        if ($insurance && $insurance->company_id != $this->company_id) {
            $this->company_id = $insurance->company_id;
        }

        return parent::beforeSave();
    }

    /**
     * Check if this provider no longer accepts insurances
     * @return boolean
     */
    public function afterDelete()
    {

        // do not remove provider_practice_insurance if this property is set to true
        if ($this->skipProviderPracticeInsuranceRemoval) {
            return parent::afterDelete();
        }

        // remove association if it exists in provider_practice_insurance
        if ($this->insurance_id == 0) {
            $piCondition = sprintf(
                'provider_id = "%s" AND company_id = "%s" ',
                $this->provider_id,
                $this->company_id
            );
        } else {
            $piCondition = sprintf(
                'provider_id = "%s" AND company_id = "%s" AND insurance_id = "%s"',
                $this->provider_id,
                $this->company_id,
                $this->insurance_id
            );
        }

        $arrPpi = ProviderPracticeInsurance::model()->findAll($piCondition);

        foreach ($arrPpi as $ppi) {
            $ppi->delete();
        }

        return parent::afterDelete();
    }

    /**
     * Consolidate data in provider_insurance
     * @important If a provider accepts all the plans in an insurance company,
     * we save provider_id=$provider_id | company_id=$company_id | insurance_id=0
     * @desc This is used to avoid having each individual plan listed in the provider_insurance table
     * @note Note that this only takes place in provider_insurance, NOT in provider_practice_insurance
     * (because we save all the details there, that's why we use it to check against)
     * @param int $providerId
     * @param int $companyId
     * @return boolean
     */
    public static function consolidate($providerId, $companyId)
    {

        //find whether there are plans in the company that this provider doesn't accept
        $sql = sprintf(
            "SELECT id
            FROM insurance
            WHERE company_id = '%s'
            AND id NOT IN
                (SELECT insurance_id
                FROM provider_practice_insurance
                WHERE company_id = '%s'
                AND provider_id = '%s');",
            $companyId,
            $companyId,
            $providerId
        );
        $arr = Yii::app()->db->createCommand($sql)->queryAll();

        if (is_array($arr) && sizeof($arr) == 0) {
            //this provider accepts all plans in this company -- is he flagged so?
            $piCondition = sprintf(
                "provider_id = '%s' AND company_id = '%s' AND insurance_id = '0'",
                $providerId,
                $companyId
            );
            if (!ProviderInsurance::model()->exists($piCondition)) {
                //he wasn't flagged as accepting all insurances, create record indicating so
                $pi = new ProviderInsurance();
                $pi->provider_id = $providerId;
                $pi->company_id = $companyId;
                $pi->insurance_id = '0';
                $pi->save();
            }
            //just in case, try to remove particular insurance records
            $arr = ProviderInsurance::model()->findAll(
                'provider_id=:provider_id AND company_id=:company_id AND insurance_id != "0"',
                array(':provider_id' => $providerId, ':company_id' => $companyId)
            );
            foreach ($arr as $record) {
                $record->delete();
            }

            // check if have incosistence between provider_insurances and provider_practice_insurance
            // I have to put here, because when delete from provider_insurance (3 lines above),
            // call method providerInsurance::afterDelete()
            // and delete record from provider_practice_insurance
            ProviderPracticeInsurance::model()->populateFromProviderInsurance($providerId);
        }

        return true;
    }

    /**
     * called on rendering the column for each row in the administration
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' .
            $this->provider_id . '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' .
            $this->provider . '">' . $this->provider . '</a>');
    }

    /**
     * Count number of providers accepting a given insurance company
     * @param int $insuranceId
     * @return int
     */
    public function countProvidersByCompany($insuranceId)
    {
        $criteria['insurance.company_id'] = $insuranceId;
        return DoctorIndex::search($criteria, 0);
    }

    /**
     * Find data in our old insurance tables (provider_insurance)
     * @param int $providerId
     * @return array
     */
    public static function getProviderInsurances($providerId = 0)
    {

        if ($providerId == 0) {
            return null;
        }
        $sql = sprintf(
            "SELECT 0 AS practice_id, '' AS practice_name,
            insurance_company.id AS company_id, insurance_company.name AS company,
            insurance.id AS insurance_id, insurance.name AS plan, '' AS details
            FROM provider_insurance
            INNER JOIN insurance_company
                ON provider_insurance.company_id = insurance_company.id
            LEFT JOIN insurance
                ON (provider_insurance.insurance_id = insurance.id
                    OR (provider_insurance.company_id = insurance.company_id AND provider_insurance.insurance_id = 0)
                )
            WHERE provider_id = %d
            ORDER BY priority DESC, company, plan;",
            $providerId
        );

        $tmpInsurances = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();

        if ($tmpInsurances) {
            $arrInsurances = array();
            foreach ($tmpInsurances as $eachOne) {
                $practice_id = '0|all';
                $company_id = $eachOne['company_id'];
                $company = $eachOne['company'];
                $insurance['insurance_id'] = $eachOne['insurance_id'];
                $insurance['plan'] = $eachOne['plan'];
                $insurance['details'] = $eachOne['details'];
                $arrInsurances[$practice_id][$company_id]['company_id'] = $company_id;
                $arrInsurances[$practice_id][$company_id]['company'] = $company;
                $arrInsurances[$practice_id][$company_id]['insurances'][$eachOne['insurance_id']] = $insurance;
            }
            return $arrInsurances;
        }
        return null;
    }

    /**
     * Find DISTINCT companies in provider_insurance
     * @param int $providerId
     * @return array
     */
    public static function getOnlyCompanyProviderInsurances($providerId = 0, $grouped = true)
    {

        if ($providerId == 0) {
            return null;
        }

        if ($grouped) {
            $sql = sprintf(
                "SELECT DISTINCT MIN(insurance_company.id) AS id, insurance_company.name AS `name`
                FROM provider_insurance
                INNER JOIN insurance_company
                    ON provider_insurance.company_id = insurance_company.id
                WHERE provider_id = %d
                GROUP BY `name`
                ORDER BY insurance_company.name;",
                $providerId
            );
        } else {
            $sql = sprintf(
                "SELECT DISTINCT insurance_company.id AS id, insurance_company.name AS `name`
                FROM provider_insurance
                INNER JOIN insurance_company
                    ON provider_insurance.company_id = insurance_company.id
                WHERE provider_id = %d;",
                $providerId
            );
        }
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Remove association between a provider and an insurance company
     * @param int $providerId
     * @param int $companyId
     * @return bool
     */
    public static function deleteCompany($providerId = 0, $companyId = 0)
    {

        if (empty($providerId) || empty($companyId)) {
            return false;
        }

        // check for existing association in provider_insurance
        $lstCompanyIds = InsuranceCompany::getSibblingCompanyIds($companyId, true);

        $piCondition = sprintf(
            'provider_id = %d AND company_id IN (%s) ',
            $providerId,
            $lstCompanyIds
        );
        $arrPi = ProviderInsurance::model()->findAll($piCondition);

        if ($arrPi) {
            foreach ($arrPi as $pi) {
                if (!$pi->delete()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Remove record from provider_insurance
     * @param int $providerId
     * @param int $companyId
     * @param int $insuranceId
     * @return bool
     */
    public static function deleteCompanyInsurance($providerId = 0, $companyId = 0, $insuranceId = 0)
    {

        if (empty($providerId) || empty($companyId) || empty($insuranceId)) {
            return false;
        }

        $lstCompanyIds = InsuranceCompany::getSibblingCompanyIds($companyId, true);
        // Are there any other provider - company - insurance?
        $piCondition = sprintf(
            'provider_id = %d AND company_id IN (%s) AND insurance_id = %d ',
            $providerId,
            $lstCompanyIds,
            $insuranceId
        );
        $arrProviderInsurance = ProviderInsurance::model()->findAll($piCondition);
        if ($arrProviderInsurance) {
            foreach ($arrProviderInsurance as $arrPi) {
                if (!$arrPi->delete()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Add New Company to Provider Practice Insurance
     * @param int $providerId
     * @param int $companyId
     * @param int $insuranceId
     */
    public static function saveCompanyInsurance($providerId = 0, $companyId = 0, $insuranceId = 0)
    {

        if (empty($providerId) || empty($companyId)) {
            return false;
        }

        // get sibbling companies
        $strCompanyId = InsuranceCompany::getSibblingCompanyIds($companyId, true);

        // Check status in provider_insurance
        if ($insuranceId == 0) {
            // is for all insurances son i need only 1 record with insurance_id = 0
            $piCondition = sprintf(
                'provider_id = %d AND company_id IN (%s) AND insurance_id != "0"',
                $providerId,
                $strCompanyId
            );
            $arrProviderInsurance = ProviderInsurance::model()->findAll($piCondition);
            if ($arrProviderInsurance) {
                foreach ($arrProviderInsurance as $providerInsurance) {
                    $providerInsurance->delete();
                }
            }
        } else {
            // is for specific insurances, so delete insurances=0 if exists
            $piCondition = sprintf(
                'provider_id = %d AND company_id IN (%s) AND insurance_id = 0 ',
                $providerId,
                $strCompanyId
            );
            $arrProviderInsurance = ProviderInsurance::model()->findAll($piCondition);
            if ($arrProviderInsurance) {
                foreach ($arrProviderInsurance as $providerInsurance) {
                    $providerInsurance->delete();
                }
            }
        }

        $arrCompanyInsert = $insuranceId == 0 ? explode(",", $strCompanyId) : array($companyId);

        foreach ($arrCompanyInsert as $companyIdInsert) {
            // add new provider_practice if not exists
            $piCondition = sprintf(
                'provider_id = %d AND company_id = %d AND insurance_id = %d ',
                $providerId,
                $companyIdInsert,
                $insuranceId
            );

            $arrProviderInsurance = ProviderInsurance::model()->exists($piCondition);
            if (!$arrProviderInsurance) {
                $newPi = new ProviderInsurance();
                $newPi->provider_id = $providerId;
                $newPi->company_id = $insuranceId == 0
                    ? $companyIdInsert
                    : Insurance::model()->findByPk($insuranceId)->company_id;
                $newPi->insurance_id = $insuranceId;

                if (!$newPi->save()) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * PADM: Get insurance plans given a provider, practice and company
     * @param int $providerId
     * @param int $practiceId
     * @param string $practiceName
     * @param int $companyId
     * @return array
     */
    public static function getProviderPracticeCompanyPlans($providerId, $practiceId, $practiceName, $companyId)
    {

        $sql = sprintf(
            "SELECT DISTINCT %s AS practice_id, '%s' AS practice_name, insurance.id AS insurance_id,
            insurance.name AS insurance_name, '' AS details, 1 AS checked
            FROM provider_insurance
            INNER JOIN insurance
                ON (
                    insurance.id = provider_insurance.insurance_id OR
                    (
                    provider_insurance.insurance_id = 0 AND insurance.company_id IN
                        (SELECT c2.id
                        FROM insurance_company c1
                        INNER JOIN insurance_company c2 ON c1.name = c2.name
                        WHERE c1.id = insurance.company_id)
                    )
                )
            WHERE provider_id = %d
                AND provider_insurance.company_id = %d
            ORDER BY insurance_name;",
            $practiceId,
            addslashes($practiceName),
            $providerId,
            $companyId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * update providerInsurances from SQScommand
     * @param array $queueBody
     *      $queueBody['providerId'] = $providerId;
     *      $queueBody['accountId'] = $accountId;
     *      $queueBody['data'] = json_encode($postData);
     * @throws Exception
     */
    public static function updateProviderInsurances($queueBody)
    {

        $providerId = $queueBody['providerId'];
        $accountId = $queueBody['accountId'];
        $data = json_decode($queueBody['data']);

        if (!empty($providerId) && !empty($data)) {

            ignore_user_abort(true);
            set_time_limit(3600); // 1 hour

            // set this provider as 'procesing..'
            $arrProviderDetails = ProviderDetails::model()->find(
                'provider_id=:provider_id',
                array(':provider_id' => $providerId)
            );

            if (empty($arrProviderDetails)) {
                $arrProviderDetails = new ProviderDetails();
                $arrProviderDetails->provider_id = $providerId;
            }
            $arrProviderDetails->is_insurance_currently_saving = time();
            $arrProviderDetails->save();

            // Begin transaction to delete plans process
            $transaction = ProviderInsurance::model()->getDbConnection()->beginTransaction();

            try {
                foreach ($data as $eachCompany) {
                    $companyId = $eachCompany->id;
                    $practicePlan = $eachCompany->practicePlan;
                    $status = $eachCompany->status;

                    if ($status == 'updated' || $status == 'isNew') {

                        $practices = array();
                        $insurancesPractices = array();
                        $allCompanyChecked = true;
                        $atLeastOneInsuranceSelected = true;
                        if (!empty($practicePlan)) {
                            // format array and find if all are checked
                            $arrPP = array();
                            foreach ($practicePlan as $pp) {
                                $arrPP = array_merge($arrPP, $pp);
                            }

                            foreach ($arrPP as $pp) {
                                $practices[$pp->practice_id] = $pp->practice_id;
                                $insurancesPractices[$pp->insurance_id][$pp->practice_id]['checked'] = $pp->checked;
                                $insurancesPractices[$pp->insurance_id][$pp->practice_id]['details'] = $pp->details;
                                if ($pp->checked == 0) {
                                    $allCompanyChecked = false;
                                } else {
                                    $atLeastOneInsuranceSelected = true;
                                }
                            }

                            if (!$atLeastOneInsuranceSelected) {
                                // if no one insurance are selected, so delete
                                ProviderInsurance::model()->deleteCompany($providerId, $companyId);
                            }
                        }

                        if ($atLeastOneInsuranceSelected) {

                            // setting for all to make cleanup
                            ProviderInsurance::model()->saveCompanyInsurance($providerId, $companyId, 0);

                            // setting for all to make cleanup
                            ProviderPracticeInsurance::model()->saveCompanyInsurance(
                                $providerId,
                                0,
                                $companyId,
                                0,
                                '',
                                $accountId
                            );

                            if (!$allCompanyChecked) {

                                // determine the state of plan selection: all | none | some
                                foreach ($insurancesPractices as $insuranceId => $practiceData) {
                                    $practiceCount = count($practiceData);
                                    $countZero = $countOne = 0;
                                    foreach ($practiceData as $practiceId => $value) {
                                        if ($value['checked'] == 0) {
                                            $countZero++;
                                        } else {
                                            $countOne++;
                                        }
                                    }
                                    if ($countOne == $practiceCount) {
                                        $insurancesPractices[$insuranceId]['thisInsuranceChecked'] = 'all';
                                    } elseif ($countZero == $practiceCount) {
                                        $insurancesPractices[$insuranceId]['thisInsuranceChecked'] = 'none';
                                    } else {
                                        $insurancesPractices[$insuranceId]['thisInsuranceChecked'] = 'some';
                                    }
                                }

                                foreach ($insurancesPractices as $insuranceId => $practiceData) {
                                    $thisInsuranceChecked = $practiceData['thisInsuranceChecked'];

                                    if ($thisInsuranceChecked != 'none') {
                                        ProviderInsurance::model()->saveCompanyInsurance(
                                            $providerId,
                                            $companyId,
                                            $insuranceId
                                        );

                                        foreach ($practiceData as $practiceId => $value) {
                                            if (isset($value['checked']) && $value['checked'] == 1) {
                                                $details = !empty($value['details']) ? $value['details'] : '';
                                                ProviderPracticeInsurance::model()->saveCompanyInsurance(
                                                    $providerId,
                                                    $practiceId,
                                                    $companyId,
                                                    $insuranceId,
                                                    $details,
                                                    $accountId
                                                );
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } elseif ($eachCompany->status == 'deleted') {
                        ProviderInsurance::model()->deleteCompany($providerId, $companyId);
                    }
                }
                $transaction->commit();
            } catch (Exception $ex) {
                // set this provider as 'finish'
                $arrProviderDetails->is_insurance_currently_saving = 0;
                $arrProviderDetails->save();
                // If there is an exception, roll back the transaction...
                $transaction->rollback();
                throw $ex;
            }
            // End Transaction
            // set this provider as 'finish' - set flag as zero
            $arrProviderDetails->is_insurance_currently_saving = 0;
            $arrProviderDetails->save();

        } elseif (!empty($providerId)) {

            // setting flag to zero
            $arrProviderDetails = ProviderDetails::model()->find(
                'provider_id=:provider_id',
                array(':provider_id' => $providerId)
            );
            $arrProviderDetails->is_insurance_currently_saving = 0;
            $arrProviderDetails->save();

        }
    }

    /**
     * update providerInsurances API from Front End App
     * @param array $queueBody
     *      $queueBody['providerId'] = $providerId;
     *      $queueBody['accountId'] = $accountId;
     *      $queueBody['data'] = json_encode($postData);
     * @throws Exception
     */
    public static function updateProviderInsurancesAPI($queueBody)
    {
        $providerId = $queueBody['providerId'];
        $data = json_decode($queueBody['data']);
        $postProviderInsurances = $data->provider_insurances;
        $postProviderPracticeInsurances = $data->provider_practice_insurances;

        if (!empty($providerId) && !empty($data)) {

            ignore_user_abort(true);
            set_time_limit(600);

            // set this provider as 'procesing..'
            $arrProviderDetails = ProviderDetails::model()->find(
                'provider_id=:provider_id',
                array(':provider_id' => $providerId)
            );
            if (empty($arrProviderDetails)) {
                $arrProviderDetails = new ProviderDetails();
                $arrProviderDetails->provider_id = $providerId;
            }
            $arrProviderDetails->is_insurance_currently_saving = time();
            $arrProviderDetails->save();


            // Start provider_insurance / provider_practice_insurance updates

            // 1. Process deletions

            // 1a. provider_insurance
            $sql = sprintf(
                "SELECT DISTINCT company_id FROM provider_insurance WHERE provider_id = %d",
                $providerId
            );
            $arrProviderInsurance = Yii::app()->db->createCommand($sql)->queryAll();

            // all pre-existent provider_insurance, exists in this new post?
            foreach ($arrProviderInsurance as $actualData) {
                $companyId = $actualData['company_id'];
                $exists = false;
                foreach ($postProviderInsurances as $value) {
                    if ($value->id == $companyId) {
                        $exists = true;
                        $backendAction = !empty($value->backend_action) ? $value->backend_action : '';
                        if ($backendAction == 'delete') {
                            // exists, but is mark as delete
                            $exists = false;
                        }
                        break;
                    }
                }
                if (!$exists) {
                    // Not exists or is mark as delete
                    ProviderInsurance::model()->deleteCompany($providerId, $companyId);
                }
            }

            // 1b. provider_practice_insurance
            $sql = sprintf(
                "SELECT DISTINCT company_id FROM provider_practice_insurance WHERE provider_id = %d",
                $providerId
            );
            $arrProviderPracticeInsurance = Yii::app()->db->createCommand($sql)->queryAll();

            // all pre-existent provider_practice_insurance, exists in this new post?
            foreach ($arrProviderPracticeInsurance as $actualData) {
                $companyId = $actualData['company_id'];
                $exists = false;
                foreach ($postProviderInsurances as $value) {
                    $exists = true;
                    $backendAction = !empty($value->backend_action) ? $value->backend_action : '';
                    if ($backendAction == 'delete') {
                        // exists, but is mark as delete
                        $exists = false;
                    }
                }
                if (!$exists) {
                    // exists in db, but not exists in the new post, so delete
                    $toRemove = ProviderPracticeInsurance::model()->findAll(
                        'provider_id = :provider_id AND company_id = :company_id',
                        [':provider_id' => $providerId, ':company_id' => $companyId]
                    );
                    if (!empty($toRemove)) {
                        foreach ($toRemove as $providerPracticeInsuranceToDelete) {
                            $insuranceId = $providerPracticeInsuranceToDelete->insurance_id;
                            ProviderInsurance::model()->deleteCompanyInsurance($providerId, $companyId, $insuranceId);
                        }
                    }
                }
            }

            // 2. Process creation/updates
            $transaction = ProviderPracticeInsurance::model()->getDbConnection()->beginTransaction();

            $arrInsurancePlans = [];    // associative array to represent the post
            $arrPractices = [];         // all unique practices array

            try {

                // go through all the companies to delete those companies with updates
                foreach ($postProviderInsurances as $company) {
                    $backendAction = !empty($company->backend_action) ? trim($company->backend_action) : '';
                    $companyId = $company->id;
                    $arrCompanies[$companyId] = [];

                    if ($backendAction == 'update') {
                        ProviderInsurance::model()->deleteCompany($providerId, $companyId);
                        $company->backend_action = 'create';
                    }

                    if ($backendAction == 'delete') {
                        continue;
                    }

                    // Create an associative array ($arrInsurancePlans) as per company > plan > practice
                    if (!empty($postProviderPracticeInsurances->$companyId)) {
                        foreach ($postProviderPracticeInsurances->$companyId as $insuranceId => $practices) {
                            $arrInsurancePlans[$companyId][$insuranceId] = [];

                            foreach ($practices as $practiceId => $content) {
                                if (!empty($content) && ($content->backend_action != 'delete' || $content->checked != 0)) {
                                    $arrInsurancePlans[$companyId][$insuranceId][$practiceId] = $content;
                                    $arrPractices[$practiceId] = [];
                                }
                            }
                        }
                    }
                }

                // Fill the blanks in $arrInsurancePlans associative array

                $allInsurances = [];    // all insurances by practice array

                // Go through all the companies
                foreach ($arrInsurancePlans as $companyId => $insurance) {
                    $allInsurances[$companyId] = [];

                    // Go through all the insurances
                    foreach (array_keys($insurance) as $insuranceId) {

                        // Go through all the practices
                        foreach (array_keys($arrPractices) as $practiceId) {
                            $allInsurances[$companyId][$practiceId] = true;
                            if (!isset($arrInsurancePlans[$companyId][$insuranceId][$practiceId]))
                                $arrInsurancePlans[$companyId][$insuranceId][$practiceId] = [];
                        }
                    }
                }

                // Search if all_insurances attribute can be applied

                // Go through all the companies
                foreach ($arrInsurancePlans as $companyId => $insurance) {

                    // Go through all the practices
                    foreach (array_keys($arrPractices) as $practiceId) {

                        // Go through all the insurances
                        foreach (array_keys($insurance) as $insuranceId) {
                            if (empty($arrInsurancePlans[$companyId][$insuranceId][$practiceId])) {
                                $allInsurances[$companyId][$practiceId] = false;
                            } else {
                                if (!empty($arrInsurancePlans[$companyId][$insuranceId][$practiceId]->details)) {
                                    $allInsurances[$companyId][$practiceId] = false;
                                }
                            }
                        }
                    }
                }

                // Go through all the companies
                foreach ($arrInsurancePlans as $companyId => $insurance) {

                    // Go through all the practices
                    foreach (array_keys($arrPractices) as $practiceId) {

                        // all_insurances
                        if ($allInsurances[$companyId][$practiceId]) {

                            $lstCompanyIds = InsuranceCompany::getSibblingCompanyIds($companyId);

                            foreach ($lstCompanyIds as $companyIdM) {
                                // all insurances
                                $providerPracticeInsurance = ProviderPracticeInsurance::model()->find(
                                    "provider_id = :provider_id
                                        AND practice_id = :practice_id
                                        AND company_id = :company_id
                                        AND insurance_id = 0",
                                    [
                                        "provider_id" => $providerId,
                                        "company_id" => $companyIdM,
                                        "practice_id" => $practiceId
                                    ]
                                );

                                if (empty($providerPracticeInsurance)) {
                                    $providerPracticeInsurance = new ProviderPracticeInsurance();
                                }
                                $providerPracticeInsurance->provider_id = $providerId;
                                $providerPracticeInsurance->company_id = $companyIdM;
                                $providerPracticeInsurance->practice_id = $practiceId;
                                $providerPracticeInsurance->insurance_id = 0;
                                $providerPracticeInsurance->save();
                            }

                        } else {

                            // Go through all the insurances
                            foreach (array_keys($insurance) as $insuranceId) {
                                if (
                                    !empty($arrInsurancePlans[$companyId][$insuranceId][$practiceId])
                                    && !empty($arrInsurancePlans[$companyId][$insuranceId][$practiceId]->checked)
                                ) {
                                    $providerPracticeInsurance = ProviderPracticeInsurance::model()->find(
                                        "provider_id = :provider_id
                                            AND practice_id = :practice_id
                                            AND company_id = :company_id
                                            AND insurance_id = :insurance_id",
                                        [
                                            "provider_id" => $providerId,
                                            "company_id" => $companyId,
                                            "practice_id" => $practiceId,
                                            "insurance_id" => $insuranceId
                                        ]
                                    );

                                    if (empty($providerPracticeInsurance)) {
                                        $providerPracticeInsurance = new ProviderPracticeInsurance();
                                    }
                                    $providerPracticeInsurance->provider_id = $providerId;
                                    $providerPracticeInsurance->company_id = $companyId;
                                    $providerPracticeInsurance->details = $arrInsurancePlans[$companyId][$insuranceId][$practiceId]->details;
                                    $providerPracticeInsurance->insurance_id = $insuranceId;
                                    $providerPracticeInsurance->practice_id = $practiceId;

                                    $providerPracticeInsurance->save();
                                }
                            }
                        }
                    }
                }

                // Let's keep updated provider_insurance from provider_practice_insurance
                $sql = "SELECT DISTINCT company_id, insurance_id FROM provider_practice_insurance
                    WHERE provider_id = :provider_id ORDER BY insurance_id, company_id";

                $insurances = Yii::app()->db->createCommand($sql)->bindValues([':provider_id' => $providerId])
                    ->queryAll();

                $ppis = [];
                foreach ($insurances as $i) {
                    $ppis[$i['company_id']][$i['insurance_id']] = true;
                    if ($i['insurance_id'] > 0 && isset($ppis[$i['company_id']][0])) {
                        unset($ppis[$i['company_id']][$i['insurance_id']]);
                    }
                }

                foreach ($ppis as $companyId => $insurance) {
                    foreach (array_keys($insurance) as $insuranceId) {
                        $providerInsurance = ProviderInsurance::model()->find(
                            "provider_id = :provider_id AND company_id = :company_id AND insurance_id = :insurance_id",
                            ["provider_id" => $providerId, "company_id" => $companyId, "insurance_id" => $insuranceId]
                        );
                        if (empty($providerInsurance)) {
                            $providerInsurance = new ProviderInsurance();
                        }
                        $providerInsurance->provider_id = $providerId;
                        $providerInsurance->company_id = $companyId;
                        $providerInsurance->insurance_id = $insuranceId;
                        $providerInsurance->save();
                    }
                }
                $transaction->commit();

                /*
                foreach ($postProviderInsurances as $eachCompany) {
                    $backendAction = !empty($eachCompany->backend_action) ? trim($eachCompany->backend_action) : '';
                    $allInsurances = !empty($eachCompany->all_insurances) ? trim($eachCompany->all_insurances) : 'no';
                    $companyId = $eachCompany->id;

                    if ($backendAction == 'update') {
                        //remove and recreate
                        ProviderInsurance::model()->deleteCompany($providerId, $companyId);
                        $backendAction = 'create';
                    }

                    if ($backendAction == 'create') {
                        // Do we received provider_practice_insurances for the given insurance company?
                        if (empty($postProviderPracticeInsurances->$companyId) || !isset($ppi[$companyId][0])) {
                            $allInsurances = 'yes';
                        }

                        if ($allInsurances == 'yes') {

                            $providerInsurance = ProviderInsurance::model()->find(
                                "provider_id=:provider_id AND company_id=:company_id AND insurance_id = 0",
                                array("provider_id" => $providerId, "company_id" => $companyId)
                            );

                            if (empty($providerInsurance)) {
                                $providerInsurance = new ProviderInsurance();
                            }
                            $providerInsurance->provider_id = $providerId;
                            $providerInsurance->company_id = $companyId;
                            $providerInsurance->insurance_id = 0;
                            $providerInsurance->save();

                            // Saving ProviderPracticeInsurance
                            foreach ($ppi[$companyId] as $practiceId) {
                                $allInsurances = 'yes';
                                foreach ($practiceId as $action) {
                                    if ($action != 'create') {
                                        $allInsurances = 'no';
                                    }
                                }
                                // TODO
                            }

                        } elseif (!empty($postProviderPracticeInsurances->$companyId)) {

                            foreach ($postProviderPracticeInsurances->$companyId as $insuranceId => $newPPI) {
                                $needToSave = false;

                                foreach ($newPPI as $practiceId => $details) {

                                    if ($details->backend_action != 'delete') {
                                        $needToSave = true;
                                        $providerPracticeInsurance = ProviderPracticeInsurance::model()->find(
                                            "provider_id=:provider_id AND company_id=:company_id AND
                                            insurance_id=:insurance_id AND practice_id=:practice_id",
                                            array(
                                                "provider_id" => $providerId, "company_id" => $companyId,
                                                "insurance_id" => $insuranceId, "practice_id" => $practiceId
                                            )
                                        );
                                        if (empty($providerPracticeInsurance)) {
                                            $providerPracticeInsurance = new ProviderPracticeInsurance();
                                        }
                                        $providerPracticeInsurance->provider_id = $providerId;
                                        $providerPracticeInsurance->company_id = $companyId;
                                        $providerPracticeInsurance->insurance_id = $insuranceId;
                                        $providerPracticeInsurance->practice_id = $practiceId;
                                        $providerPracticeInsurance->details = $details->details;
                                        $providerPracticeInsurance->save();
                                    }

                                }

                                // update the insurance
                                if ($needToSave) {
                                    $providerInsurance = ProviderInsurance::model()->find(
                                        "provider_id=:provider_id AND company_id=:company_id AND insurance_id=:insurance_id",
                                        array(
                                            "provider_id" => $providerId,
                                            "company_id" => $companyId,
                                            "insurance_id" => $insuranceId
                                        )
                                    );
                                     // get wether to create or delete

                                    if (empty($providerInsurance)) {
                                        $providerInsurance = new ProviderInsurance();
                                    }
                                    $providerInsurance->provider_id = $providerId;
                                    $providerInsurance->company_id = $companyId;
                                    $providerInsurance->insurance_id = $insuranceId;
                                    $providerInsurance->save();
                                }
                            }
                        }

                    } elseif ($backendAction == 'delete') {
                        ProviderInsurance::model()->deleteCompany($providerId, $companyId);
                    }
                }
                */

            } catch (Exception $ex) {
                // set this provider as 'finish'
                $arrProviderDetails->is_insurance_currently_saving = 0;
                $arrProviderDetails->save();
                // If there is an exception, roll back the transaction...
                $transaction->rollback();
                throw $ex;
            }
            // End Transaction
            // set this provider as 'finish' - set flag as zero
            $arrProviderDetails->is_insurance_currently_saving = 0;
            $arrProviderDetails->save();

        } elseif (!empty($providerId)) {

            // setting flag to zero
            $arrProviderDetails = ProviderDetails::model()->find(
                'provider_id=:provider_id',
                array(':provider_id' => $providerId)
            );
            $arrProviderDetails->is_insurance_currently_saving = 0;
            $arrProviderDetails->save();
        }

        $result['success'] = true;
        return $result;
    }

    /**
     * {@inheritdoc}
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        //this is to enable eager loading - looks like it's no good to add provider here
        if (!intval($this->provider_id) && is_string($this->provider_id) && strlen($this->provider_id) > 0) {
            $criteria->with = array('provider');
            $criteria->compare('provider.first_last', $this->provider_id, true);
            $criteria->group = 't.company_id';
        } elseif (intval($this->provider_id)) {
            $criteria->compare('provider_id', $this->provider_id);
            $criteria->group = 't.company_id';
        }

        if (!intval($this->insurance_id) && is_string($this->insurance_id) && strlen($this->insurance_id) > 0) {
            $criteria->with = array('insurance');
            $criteria->compare('insurance.name', $this->insurance_id, true);
        } elseif (intval($this->insurance_id)) {
            $criteria->compare('insurance_id', $this->insurance_id);
        }

        if (intval($this->company_id)) {
            $criteria->compare('t.company_id', $this->company_id);
        }

        return new CActiveDataProvider(
            get_class($this),
            ['criteria' => $criteria, 'pagination' => ['pageSize' => 25]]
        );
    }

    /**
     * Find companies in provider_insurance
     * @param int $providerId
     * @return array
     */
    public static function getProviderInsurancesCompany($providerId = 0)
    {

        $sql = sprintf(
            "SELECT DISTINCT
                provider_insurance.company_id,
                insurance_company.name AS company_name
            FROM provider_insurance
            INNER JOIN insurance_company
                ON provider_insurance.company_id = insurance_company.id
            WHERE provider_id = %d
            GROUP BY insurance_company.name
            ORDER BY insurance_company.name;",
            $providerId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

}
