<?php
Yii::import('application.modules.core_models.models._base.BaseAccountEmr');
Yii::import('application.modules.providers.protected.components.NoEmrImporter');

class AccountEmr extends BaseAccountEmr
{

    /**
     * beforeDelete
     * @return bool
     */
    public function beforeDelete()
    {
        if (!empty($this->account_id)) {
            // delete records from account_practice_emr before deleting this account
            $arrAccountPracticeEmr = AccountPracticeEmr::model()->findAll(
                'account_id=:account_id',
                array(':account_id' => $this->account_id)
            );
            foreach ($arrAccountPracticeEmr as $accountPracticeEmr) {
                if (!$accountPracticeEmr->delete()) {
                    $this->addError(
                        'id',
                        'This account cannot be deleted because a practice\'s EMR data is attached to it.'
                    );
                    return false;
                }
            }

            // delete records from account_provider_emr before deleting this account
            $arrAccountProviderEmr = AccountProviderEmr::model()->findAll(
                'account_id=:account_id',
                array(':account_id' => $this->account_id)
            );
            foreach ($arrAccountProviderEmr as $accountProviderEmr) {
                if (!$accountProviderEmr->delete()) {
                    $this->addError(
                        'id',
                        'This account cannot be deleted because a provider\'s EMR data is attached to it.'
                    );
                    return false;
                }
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Save event to audit log
     *
     * @return void
     */
    public function beforeSave()
    {

        $accountId = null;
        if (!empty($this->account_id)) {
            $accountId = $this->account_id;
        }

        if ($this->isNewRecord) {
            AuditLog::create('C', 'Account EMR', 'account_emr', null, false, $accountId, true);
        } else {
            AuditLog::create('U', 'Account EMR', 'account_emr', null, false, $accountId, true);
        }

        return parent::beforeSave();
    }

    /**
     * Update EHR integration status in Salesforce after saving
     * @return bool
     */
    public function afterSave()
    {
        if (!empty($this->account_id)) {
            $organizations = OrganizationAccount::model()->findAll(
                'account_id=:accountId',
                array(':accountId' => $this->account_id)
            );
            if (!empty($organizations)) {
                foreach ($organizations as $organization) {
                    SqsComponent::sendMessage('updateSFEHR', $organization->organization_id);
                }
            }
        }

        // update scheduling if necessary
        if ($this->isNewRecord
            ||
            (!empty($this->oldRecord) && $this->oldRecord->emr_type_id != $this->emr_type_id)
            ||
            (!empty($this->oldRecord) && $this->oldRecord->account_id != $this->account_id)
        ) {
            if (!empty($this->account_id)) {
                // async update
                SqsComponent::sendMessage('updateBooking', json_encode(['account_id' => $this->account_id]));
            }
            if (!empty($this->oldRecord->account_id) && $this->oldRecord->account_id != $this->account_id) {
                // async update the previous account_id
                SqsComponent::sendMessage('updateBooking', json_encode(['account_id' => $this->oldRecord->account_id]));
            }
        }

        return parent::afterSave();
    }

    /**
     * Update EHR integration status in Salesforce after deleting
     * @return boolean
     */
    public function afterDelete()
    {
        if (!empty($this->account_id)) {
            $organizations = OrganizationAccount::model()->findAll('account_id=:accountId', array(
                ':accountId' => $this->account_id
            ));
            if (!empty($organizations)) {
                foreach ($organizations as $organization) {
                    SqsComponent::sendMessage('updateSFEHR', $organization->organization_id);
                }
            }

            AuditLog::create('D', 'Account EMR', 'account_emr', null, false, $this->account_id, true);
        }

        // update scheduling if necessary
        if (!empty($this->emr_type_id) && $this->emr_type_id == 3 && !empty($this->account_id)) {
            // async update
            SqsComponent::sendMessage('updateBooking', json_encode(['account_id' => $this->account_id]));
        }

        return parent::afterDelete();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->with = array('account', 'emr_type');

        if ($this->account_id > 0) {
            $criteria->compare('account_id', $this->account_id);
        } else {
            $criteria->addSearchCondition('account.first_name', "%" . trim($this->account) . "%", false, 'OR', 'LIKE');
            $criteria->addSearchCondition('account.last_name', "%" . trim($this->account) . "%", false, 'OR', 'LIKE');
        }

        $criteria->compare('id', $this->id);
        $criteria->compare('emr_type_id', $this->emr_type_id);
        $criteria->compare('account_id', $this->account_id);
    }

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Needed for the extension ERememberFiltersBehavior, which saves the filters inputted after refreshing the page.
     * @return array
     */
    public function behaviors()
    {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'ERememberFiltersBehavior',
            ),
        );
    }

    /**
     * Import EMR data for the current record
     */
    public function importEmrData()
    {
        if ($this->emr_type_id == '1') {
            $options = json_decode($this->options, true);
            if (!empty($options['origin_db'])) {
                PracticeBrainImporter::import($options['origin_db'], $this->id);
            }
        } elseif ($this->emr_type_id == '2') {
            DentrixV5Importer::import($this->account_id);
        } elseif ($this->emr_type_id == '3') {
            NoEmrImporter::import($this->account_id);
        } elseif ($this->emr_type_id == '4') {
            DentrixG5Importer::import($this->account_id);
        } elseif ($this->emr_type_id == '5') {
            NextechImporter::import($this->account_id);
        }
        return true;
    }

    /**
     * get information on EmrType for this account
     * @param int $accountId
     * @param boolean $useCache
     * @return array
     */
    public static function getAccountEmr($accountId = 0, $useCache = true)
    {
        if ($accountId == 0) {
            return false;
        }

        $sql = sprintf(
            "SELECT ae.*, et.name, et.type, et.driver, et.logo_path,
            et.mi7_integration, et.read_only_integration, ae.id AS account_emr_id,
            et.sync_exceptions
            FROM account_emr AS ae
            LEFT JOIN organization_account oa ON oa.account_id = ae.account_id
            LEFT JOIN organization_account oa2 ON oa.organization_id = oa2.organization_id
            LEFT JOIN emr_type AS et ON ae.emr_type_id = et.id
            WHERE (ae.account_id = %d AND oa.id IS NULL) OR oa2.account_id = %d
            ORDER BY oa.id IS NULL DESC
            LIMIT 1;",
            $accountId,
            $accountId
        );

        $cacheTime = $useCache ? Yii::app()->params['cache_long'] : 0;
        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryRow();
    }

    /**
     * Import the emr information for all the account_emr records of an account
     * @param int $accountId
     * @return mixed
     * @throws CDbException
     */
    public static function importAccountEmr($accountId)
    {

        // Don't import EMR data if the account is not owner account.
        $currentAccountOrg = OrganizationAccount::model()->cache(Yii::app()->params['cache_short'])->find(
            'account_id=:account_id',
            array(':account_id' => $accountId)
        );
        if (!empty($currentAccountOrg) && $currentAccountOrg->connection != 'O') {
            return;
        }

        // Don't import EMR data if the account is in research mode.
        $account = Account::model()->cache(Yii::app()->params['cache_short'])->findByPk($accountId);

        $emrImportationDisabled = !empty(Yii::app()->params['emr_importation_disabled']);
        if (empty($account) || $account->research_mode != 0 || $emrImportationDisabled) {
            return;
        }

        // import each account
        // account_id is a unique_key, so could be only one record for that account_id
        $accountEmr = AccountEmr::model()->find("account_id = :account_id", array(":account_id" => $accountId));

        // if no account_emr exists for the account
        if (empty($accountEmr->id)) {
            // check if there are providers with appointment scheduling enabled
            $sql = sprintf(
                "SELECT COUNT(1) as cnt
                FROM account_provider ap
                INNER JOIN provider_details pd ON ap.provider_id = pd.provider_id
                WHERE ap.account_id = %d AND pd.schedule_mail_enabled = 1",
                $accountId
            );
            $cnt = Yii::app()->db->createCommand($sql)->queryScalar();
            if ((int)$cnt > 0) {
                // initialize EMR data
                $accountEmr = new AccountEmr();
                $accountEmr->account_id = $accountId;
                $accountEmr->emr_type_id = 3; // Doctor.com -> NO EMR
                $accountEmr->save();
                $accountEmr->importEmrData();
            }
        } else {
            // update EMR data
            $accountEmr->importEmrData();
        }

        $cpnInstallation = CpnInstallation::model()->find(
            "account_id = :account_id",
            array(":account_id" => $accountId)
        );

        if ($cpnInstallation) {
            $cpnEhrInstallation = CpnEhrInstallation::model()->find(
                "cpn_installation_id = :cpn_installation_id",
                array(":cpn_installation_id" => $cpnInstallation->id)
            );

            if ($cpnEhrInstallation) {
                NoEmrImporter::import($accountId);
            }
        }
    }

     public static function accountEmrData($accountId, $appointRequest = null )
    {
        // Don't import EMR data if the account is not owner account.
        $currentAccountOrg = OrganizationAccount::model()->cache(Yii::app()->params['cache_short'])->find(
            'account_id=:account_id',
            array(':account_id' => $accountId)
        );

        if (!empty($currentAccountOrg) && $currentAccountOrg->connection != 'O') {
            return;
        }

        // Don't import EMR data if the account is in research mode.
        $account = Account::model()->cache(Yii::app()->params['cache_short'])->findByPk($accountId);
        $emrImportationDisabled = !empty(Yii::app()->params['emr_importation_disabled']);

        if (empty($account) || $account->research_mode != 0 || $emrImportationDisabled) {
            return;
        }

        // import each account
        $arrAccountEmr = AccountEmr::model()->findAll("account_id = :account_id", array(":account_id" => $accountId));

        // if no account_emr exists for the account
        if (empty($arrAccountEmr)) {

                $accountEmr = new AccountEmr();
                $accountEmr->account_id = $accountId;
                $accountEmr->emr_type_id = 3; // Doctor.com -> NO EMR
                $accountEmr->save();
        }

    }

    /**
     * Given a Provider & Practice return a Account Emr Type
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function accountEmrProviderPractice($providerId, $practiceId)
    {
        $sql = sprintf(
            "SELECT account_emr.id, emr_type_id
            FROM account_emr
            INNER JOIN account_provider_emr
            ON account_emr.account_id= account_provider_emr.account_id
            INNER JOIN account_practice_emr
            ON account_practice_emr.account_id= account_provider_emr.account_id
            AND account_emr.account_id= account_provider_emr.account_id
            WHERE account_provider_emr.provider_id = %s
            AND account_practice_emr.practice_id = %s;",
            $providerId,
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Search if this account has an mi7 integration
     * @param int $accountId
     *
     * @deprecated No longer used by internal code and not recommended
     *
     * @return array
     */
    public static function hasMi7Integration($accountId = 0)
    {
        if ($accountId == 0) {
            return false;
        }
        $sql = sprintf(
            "SELECT ae.account_id, ae.emr_type_id, ae.date_added,
            et.mi7_integration, et.name AS emr_name
            FROM account_emr AS ae
            LEFT JOIN emr_type AS et
            ON ae.emr_type_id = et.id
            WHERE ae.account_id = %d;",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Return the account_emr record
     * @param int $accountId
     * @param bool $useCache
     * @return object
     */
    public static function getByAccountId($accountId = 0, $useCache = true)
    {
        $cacheTime = $useCache ? Yii::app()->params['cache_short'] : 0;
        return AccountEmr::model()->cache($cacheTime)->find(
            'account_id=:account_id',
            array(':account_id' => $accountId)
        );
    }

    /**
     * Save the account_emr settings
     * @param int $accountId
     * @param array $postData
     * @return array
     * @throws CDbException
     */
    public static function saveSettings($accountId = 0, $postData = array())
    {

        $settingsSaved = false;
        $postAccountEmr = $postData['accountEmr'];
        $options = $postAccountEmr['options'];
        $postAccountProviderEmr = $postData['postAccountProviderEmr'];
        $postAccountPracticeEmr = $postData['postAccountPracticeEmr'];
        $messageDisabled = Common::getAttribute($postData, 'messageDisabled');
        $messageWarning = Common::getAttribute($postData, 'messageWarning');

        $return['success'] = true;
        $return['error'] = '';
        $return['data'] = array();
        $errorString = '';

        $accountEmrImported = false;

        // Check if EMR Type change
        $emrTypeId = !empty($postAccountEmr['emr_type_id']) ? (int)$postAccountEmr['emr_type_id'] : 0;
        $syncProviderPracticeSchedule = 0;
        if (!empty($postAccountEmr['sync_provider_practice_schedule'])) {
            $syncProviderPracticeSchedule = $postAccountEmr['sync_provider_practice_schedule'];
        }
        $jsonOptions = '';
        $mi7Integration = false;
        $syncScheduleType = 0;
        if ($emrTypeId > 0) {
            $arrEmrType = EmrType::getEmrType($emrTypeId);
            if ($arrEmrType) {
                $emrType = $arrEmrType->type;
                $syncScheduleType = $arrEmrType->sync_provider_practice_schedule;
                $mi7Integration = $arrEmrType->mi7_integration == 1 ? true : false;
                if ($emrType == 'Web') {
                    $jsonOptions = json_encode($options);
                }
            }
        }

        $accountEmr = AccountEmr::getByAccountId($accountId, false);

        $isNewAccountEmr = false;
        if (!$accountEmr) {
            $isNewAccountEmr = true;
            $accountEmr = new AccountEmr();
            AuditLog::create('C', 'EMR options', 'account_emr', 'Emr Type id: ' . $emrTypeId, false, $accountId);
        } else {

            AuditLog::create('U', 'EMR options', 'account_emr', 'Emr Type id: ' . $emrTypeId, false, $accountId);

            // EMR configuration was changed
            if ($emrTypeId != $accountEmr->emr_type_id) {
                // Yes, delete emr config if a different driver is selected

                // If change the emrType and new emr has mi7_integration so inform to salesforce
                if ($mi7Integration) {
                    SalesForceComponent::updateEmrIntegration($accountId, $arrEmrType->name);
                }

                // Delete emr config if a different driver is selected
                EmrScheduleException::deleteByAccountId($accountId);

                // Delete AccountProviderEmr records for this account
                $accountProviders = AccountProviderEmr::model()->findAll(
                    "account_id = :account_id",
                    array(":account_id" => $accountId)
                );
                foreach ($accountProviders as $ap) {
                    $ap->delete();
                }

                // Delete AccountPracticeEmr records for this account
                $accountPractices = AccountPracticeEmr::model()->findAll(
                    "account_id = :account_id",
                    array(":account_id" => $accountId)
                );
                foreach ($accountPractices as $ap) {
                    $ap->delete();
                }

                $isNewAccountEmr = true;
                $accountEmr->sync_provider_practice_schedule = 0;
            }
        }

        $accountEmr->account_id = $accountId;
        $accountEmr->options = $jsonOptions;
        $accountEmr->emr_type_id = $emrTypeId;
        $accountEmr->message_disabled = $messageDisabled;
        $accountEmr->message_warning = $messageWarning;

        // Accept overbooking?
        $accountEmr->overbooking = empty($postAccountEmr['overbooking']) ? 0 : $postAccountEmr['overbooking'];

        if ($emrTypeId > 0) {

            if ($isNewAccountEmr) {
                $accountEmr->sync_provider_practice_schedule = $syncScheduleType;
            } else {
                if ($syncScheduleType == 1) {
                    if ($syncProviderPracticeSchedule == 1) {
                        $accountEmr->sync_provider_practice_schedule = 1;
                    } else {
                        $accountEmr->sync_provider_practice_schedule = 0;
                    }
                } else {
                    $accountEmr->sync_provider_practice_schedule = 0;
                }
            }

            if ($accountEmr->save()) {
                $settingsSaved = true;
            }
        } else {
            $return['success'] = true;
            $return['error'] = '';
            $return['data']['emr_type_id'] = 0;
            $return['data']['redirect_to'] = '/myaccount/accountEmr';
            return $return;
        }

        if ($emrTypeId == 99) {
            // 99 is the emr_type->id for None (Disabled)
            // No EMR selected so disable scheduling
            $accountProvider = AccountProvider::getAllByAccountId($accountId);
            foreach ($accountProvider as $ap) {
                $providerId = $ap->provider_id;
                $providerDetails = ProviderDetails::getDetail($providerId);
                if (!empty($providerDetails->provider_id)) {
                    $providerDetails->schedule_mail_enabled = 0;
                    $providerDetails->save();

                    AuditLog::create(
                        'U',
                        'Provider #' . $providerId . ' scheduling mail changed',
                        'provider_details',
                        'Schedule Mail Enabled: 0',
                        false,
                        $accountId
                    );
                }
            }
            $accountEmrImported = true;
        } else {

            $accountPracticeEmr = AccountPracticeEmr::getAllByAccountId($accountId);
            $accountProviderEmr = AccountProviderEmr::getAllByAccountId($accountId);

            if (empty($accountProviderEmr) || empty($accountPracticeEmr)) {
                // import EMR settings into the ingestion server because is the first time
                if ($settingsSaved) {
                    AccountEmr::importAccountEmr($accountId);
                    $accountEmrImported = true;
                }
                $accountProviderEmr = AccountProviderEmr::getAllByAccountId($accountId);
                $accountPracticeEmr = AccountPracticeEmr::getAllByAccountId($accountId);
            }

            /**
             * Config providers
             */
            foreach ($accountProviderEmr as $ape) {
                // Exists this account_provider_emr record in the post array?
                if (!empty($postAccountProviderEmr[$ape->id])) {
                    // Yes
                    $isChecked = empty($postAccountProviderEmr[$ape->id]["checked"]) ? false : true;
                    $providerId = !empty($postAccountProviderEmr[$ape->id]["provider_id"])
                        ? (int)$postAccountProviderEmr[$ape->id]["provider_id"]
                        : $postAccountProviderEmr[$ape->id]["present"];

                    if ((int)$providerId < 1) {
                        $providerId = $ape->provider_id;
                    }

                    // Enabled Schedule by default
                    $scheduleEnabled = 1;
                    
                   // AccountProviderEmr -> did we turn this provider on or off?
                    if ($isChecked) {
                        // Turn on
                        $ape->provider_id = $providerId;
                    } else {
                        if (empty($providerId)) {
                            // Was not selected before, and is not selected now. Do nothing
                            continue;
                        }
                        if (empty($ape->fb_page_id)) {
                            // Turn off
                            $sqlaccid = sprintf(
                                "SELECT account_id, emr_user_data
                                FROM  account_provider_emr
                                WHERE provider_id = %d
                                AND emr_user_data  IS NOT NULL ",
                                $providerId
                            );
                            $telemedicineBooking=sprintf(
                                " SELECT provider_practice.id AS id, provider_practice.practice_id AS practice_id,
                                provider_practice.provider_id AS provider_id,
                                provider_details.telemedicine_accept_new_patients
                                FROM provider_practice
                                INNER JOIN practice_details
                                ON provider_practice.practice_id = practice_details.practice_id
                                AND practice_details.practice_type_id = 5
                                LEFT JOIN provider_details
                                ON provider_practice.provider_id = provider_details.provider_id
                                WHERE provider_practice.provider_id = %d
                                LIMIT 1", 
                                $providerId
                        );
                            $providerAccountId = Yii::app()->db->createCommand($sqlaccid)->queryColumn();
                            $telemedicineEnable=Yii::app()->db->createCommand($telemedicineBooking)->queryColumn();
                     
                            if (count($providerAccountId) == 1 && count($telemedicineEnable) == 0 ) {
                                $ape->provider_id = null;
                                $scheduleEnabled = 0; 
                            } else {
                                $ape->provider_id = null;
                                $scheduleEnabled = 1; 
                            }                  
                        }
                    }

                    // ProviderDetails -> turn scheduling on or off
                    // if ProviderDetails doesn't exists, create it

                    $pdResult = false;
                    $providerDetails = ProviderDetails::getDetail($providerId);
                    if (empty($providerDetails)) {
                        $providerDetails = new ProviderDetails;
                        $providerDetails->provider_id = $providerId;
                    }
                    $providerDetails->schedule_mail_enabled = $scheduleEnabled;
                    $pdResult = $providerDetails->save();

                    AuditLog::create(
                        'U',
                        'Provider #' . $providerId . ' scheduling mail changed',
                        'provider_details',
                        'Schedule Mail Enabled: ' . $scheduleEnabled,
                        false,
                        $accountId
                    );

                    if (!$pdResult) {
                        $providerRecord = Provider::model()->findByPk($providerId);
                        if ($providerDetails && $providerDetails->getError('schedule_mail_enabled')) {
                            $errorString = $providerRecord->first_m_last . ' -> '
                                . $providerDetails->getError('schedule_mail_enabled');
                        } else {
                            $errorString = $providerRecord->first_m_last . ' -> ' . $providerDetails->getErrors();
                        }
                        $return['data']['settingsSaved'] = false;
                        $return['data']['accountEmrImported'] = $accountEmrImported;
                        $return['data']['errorString'] = $errorString;
                        return $return;
                    }

                    // Save changes in AccountProviderEmr
                    if (!$ape->save()) {
                        $return['data']['errorString'] = $ape->getErrors();
                        $return['data']['accountProviderEmr'][$providerId] = $ape->getErrors();
                    }
                }
            }

            /**
             * Config Practices
             */
            foreach ($accountPracticeEmr as $ape) {

                if (!empty($postAccountPracticeEmr[$ape->id])) {
                    $practiceId = !empty($postAccountPracticeEmr[$ape->id]["practice_id"])
                        ? (int)$postAccountPracticeEmr[$ape->id]["practice_id"]
                        : $postAccountPracticeEmr[$ape->id]["present"];

                    $isChecked = !empty($postAccountPracticeEmr[$ape->id]["checked"]) ? true : false;
                    if ($isChecked) {
                        $ape->practice_id = $practiceId;
                    } else {
                        $ape->practice_id = null;
                        $ape->fb_page_id = null;
                    }

                    if (!$ape->save()) {
                        $return['data']['accountPracticeEmr'][$practiceId] = $ape->getErrors();

                        $practiceRecord = Practice::model()->findByPk($practiceId);

                        if ($ape->getError('practice_id')) {
                            $errorString = $ape->getError('practice_id');
                        } else {
                            $errorString = "There was a problem making changes to a practice.";
                        }

                        $return['data']['settingsSaved'] = false;
                        $return['data']['errorString'] = $practiceRecord->name . '-> ' . $errorString;
                        return $return;
                    } else {

                        AuditLog::create(
                            'U',
                            'Practice #' . $practiceId . ' scheduling mail changed',
                            'practice_details',
                            'Schedule Mail Enabled: ' . ($isChecked ? 1 : 0),
                            false,
                            $accountId
                        );
                    }
                }
            }

            if (empty($accountPracticeEmr) && empty($accountProviderEmr) && $mi7Integration) {
                AccountEmr::importAccountEmr($accountId);
                $accountEmrImported = true;
            }
        }

        /*
        FE is sending:
         ["telemedicineProviders"]=>
        array(3) {
            [70]=>
            array(1) {
            ["checked"]=>
            int(0)
            }
            [2933002]=>
            array(1) {
            ["checked"]=>
            int(0)
            }
            [3817450]=>
            array(1) {
            ["checked"]=>
            int(0)
            }
        }
        */
        if (!empty($postData['telemedicineProviders'])) {
            // for each provider on the list
            foreach ($postData['telemedicineProviders'] as $providerId => $isChecked) {
                if (Common::isTrue($isChecked['checked'])) {
                    // make sure the provider has a telemedicine practice
                    ProviderPractice::getOrCreateTelemedicinePractice(
                        $providerId,
                        $accountId,
                        ['enabled' => 1, 'accept_new_patients' => 1]
                    );
                    $providerDetails = ProviderDetails::getDetail($providerId);
                    if ($providerDetails->schedule_mail_enabled == 0) {
                        $providerDetails->schedule_mail_enabled = 1;
                        $providerDetails->save();
                        AuditLog::create(
                            'U',
                            'Provider #' . $providerId . ' scheduling mail changed',
                            'provider_details',
                            'Schedule Mail Enabled: 1',
                            false,
                            $accountId
                        );
                    }
                } else {
                    // unlink provider from telemedicine practice and delete it if possible
                    $sqlaccid = sprintf(
                        "SELECT account_id, emr_user_data
                        FROM account_provider_emr
                        WHERE provider_id = %d AND emr_user_data  IS NOT NULL ",
                        $providerId
                    );
                    $telemedicineBooking=sprintf(
                        " SELECT provider_practice.id AS id, provider_practice.practice_id AS practice_id,
                        provider_practice.provider_id AS provider_id, provider_details.telemedicine_accept_new_patients
                        FROM provider_practice
                        INNER JOIN practice_details
                        ON provider_practice.practice_id = practice_details.practice_id
                        AND practice_details.practice_type_id = 5
                        LEFT JOIN provider_details
                        ON provider_practice.provider_id = provider_details.provider_id
                        WHERE provider_practice.provider_id = %d
                        LIMIT 1", 
                        $providerId
                    );
                    $providerAccountId = Yii::app()->db->createCommand($sqlaccid)->queryColumn();
                    $telemedicineEnable=Yii::app()->db->createCommand($telemedicineBooking)->queryColumn();
                    if (count($providerAccountId) == 0 && count($telemedicineEnable) == 1 ) {
                        $providerDetails = ProviderDetails::getDetail($providerId);
                        if ($providerDetails->schedule_mail_enabled == 1) {
                        $providerDetails->schedule_mail_enabled = 0;
                        $providerDetails->save();
                        AuditLog::create(
                            'U',
                            'Provider #' . $providerId . ' scheduling mail changed',
                            'provider_details',
                            'Schedule Mail Enabled: 0',
                            false,
                            $accountId
                        ); 
                        }
                    }
                    ProviderPractice::removeTelemedicinePractice($providerId);
                }               
            }
        }

        $return['data']['settingsSaved'] = $settingsSaved;
        $return['data']['accountEmrImported'] = $accountEmrImported;
        $return['data']['errorString'] = $errorString;
        return $return;
    }

    /**
     * Get Account/EMR configuration
     * @param array $data
     * @return array
     * @throws CDbException
     */
    public static function getAccountEmrConfig($data = array())
    {
        $accountId = $data['accountId'];
        $settingsSaved = $data['settingsSaved'];
        $accountEmrImported = $data['accountEmrImported'];
        $errorString = $data['errorString'];

        $accountEmr = AccountEmr::getByAccountId($accountId, false);

        $options['origin_db'] = '';
        if (!empty($accountEmr->options)) {
            $options = json_decode($accountEmr->options, true);
        }

        $arrEmrTypes = EmrType::model()->getAllEmrTypes();

        // import EMR settings into the ingestion server
        if ($settingsSaved && !$accountEmrImported) {
            AccountEmr::importAccountEmr($accountId);
        }

        $accountProviders = AccountProvider::getAccountProviders($accountId);
        $accountProviderEmr = AccountProviderEmr::getProviders($accountId, true);

        $accountPractices = AccountPractice::getAccountPractices($accountId);
        $accountPracticeEmr = AccountPracticeEmr::getPractices($accountId);

        $telemedicineProviders = AccountProvider::getAccountTelemedicineProviders($accountId);

        // Check Facebook Integration
        $seResult = SocialEnhanceComponent::checkFacebookIntegration(
            $accountId,
            $accountProviderEmr,
            $accountPracticeEmr
        );

        $hasFacebookIntegration = $seResult[0];
        $accountProviderEmr = $seResult[1];
        $accountPracticeEmr = $seResult[2];

        // has Mi7Integration ?
        $mi7Integration = $readOnlyIntegration = $syncScheduleType = $allowOverbooking = false;

        $emrTypeId = null;

        if (isset($accountEmr['emr_type_id'])) {
            $emrTypeId = $accountEmr['emr_type_id'];
        }

        $btnSubmitCaption = "Enable integration";
        $regressionCounter = 0;
        $aditionalInfo = '';

        if ($emrTypeId > 0) {
            foreach ($arrEmrTypes as $eachEmr) {
                if ($eachEmr['id'] == $emrTypeId) {
                    $readOnlyIntegration = ($eachEmr['read_only_integration'] == 1) ? true : false;
                    $mi7Integration = ($eachEmr['mi7_integration'] == 1) ? true : false;
                    $syncScheduleType = ($eachEmr['sync_provider_practice_schedule'] == 1) ? true : false;
                    $allowOverbooking = ($eachEmr['overbooking'] == 1) ? true : false;
                    break;
                }
            }

            // Submit Text
            $cnt = AccountPracticeEmr::countAccountPractices($accountId);
            if ($cnt > 0) {
                $cnt1 = AccountPracticeEmr::countPractices($accountId, $accountEmr->id);
                if ($cnt1 > 0) {
                    $cnt1 = AccountProviderEmr::countProviders($accountId, $accountEmr->id);
                    if ($cnt1 > 0) {
                        $btnSubmitCaption = "Save Settings";
                    } else {
                        $regressionCounter = EhrCommunicationQueue::waitingProviderList($accountEmr->id);
                        if ($regressionCounter > 0) {
                            $regressionCounter = 10; // reload after 10 seconds
                            $btnSubmitCaption = "Waiting for providers list, checking again in 5 seconds";
                        } else {
                            $btnSubmitCaption = "Get providers list from EHR";
                        }
                    }
                } else {

                    if ($mi7Integration) {
                        $aditionalInfo = 'display_activate_base_info';
                    }
                    $btnSubmitCaption = "Match practices and providers";
                }
            } else {
                $btnSubmitCaption = "Initialize";
                if ($mi7Integration) {
                    $aditionalInfo = 'display_activate_base_info';
                    // check if we're an admin only if we're not in cli
                    if (php_sapi_name() != 'cli' && !empty(Yii::app()->session['admin_account_id'])) {
                        $cnt = EhrCommunicationQueue::model()->count(
                            "emr_id = :emr_id
                            AND (message_type = :message_providers OR message_type = :message_practices)
                            AND processed = :processed",
                            array(
                                ":emr_id" => $accountEmr->id,
                                ":message_providers" => Mi7Importer::$GET_PROVIDER_LIST,
                                ":message_practices" => Mi7Importer::$GET_PRACTICE_LIST,
                                ":processed" => 0
                            )
                        );

                        if ($cnt) {
                            $regressionCounter = 10; // reload after 10 seconds
                            $btnSubmitCaption = "Initialization in progress, checking again in 10 seconds";
                        }
                    } else {
                        $btnSubmitCaption = "Match practices and providers";
                    }
                }
            }
        }

        // Set default value for hasOperatories
        $hasOperatories = false;

        // Create array with practice for providers
        $providersForPractices = array();

        // Do we have providers linkef to this account?
        if (!empty($accountProviders)) {
            // Yes, we have

            foreach ($accountProviders as $ap) {
                if (!empty($ap['provider_id'])) {
                    $providerId = $ap['provider_id'];
                    // Get the practices where that provider is working on
                    $providersPractice = ProviderPractice::getProvidersPractices($providerId, $accountId, 'workingOn');

                    // Do we have practices related to this provider?
                    if (!empty($providersPractice)) {
                        // Yes, we have

                        foreach ($providersPractice as $eachPractice) {
                            // Check for operatories at provider-practice level
                            if ($eachPractice['operatories'] > 0) {
                                $hasOperatories = true;
                            }
                            $providersForPractices[$eachPractice['id']][$providerId] = $eachPractice['provider_name'];
                        }
                    }
                }
            }
        }

        // Check consistency between AccountPractices <-> AccountPracticeEmr
        foreach ($accountPractices as $key => $practice) {

            // Check for operatories at practice level
            if ($practice['operatories'] > 0) {
                $hasOperatories = true;
            }

            $practiceId = $practice['id'];
            $exists = false;
            foreach ($accountPracticeEmr as $key => $value) {
                if ($value['practice_id'] == $practiceId || $value['emr_location_id'] == $practiceId) {
                    $exists = true;
                    $accountPracticeEmr[$key]['practice_name'] = $practice['practice_name'];
                    $accountPracticeEmr[$key]['practice_address'] = $practice['practice_address'];
                    $accountPracticeEmr[$key]['practice_phone'] = $practice['phone_number'];
                    break;
                }
            }
            if (!$exists) {
                $new = array();
                $new["id"] = 0;
                $new["account_id"] = $accountId;
                $new["emr_location_id"] = $practiceId;
                $new["emr_location_data"] = $practice['practice_name'] . '; ' . $practice['practice_address']
                    . '(' . $practiceId . ')';
                $new["practice_id"] = 0;
                $new["fb_page_id"] = 0;
                $new["is_in_widget"] = null;
                $new['practice_name'] = $practice['practice_name'];
                $new['practice_address'] = $practice['practice_address'];
                $new['practice_phone'] = $practice['phone_number'];
                $accountPracticeEmr[] = $new;
            }
        }

        // Check consistency between AccountProviders <-> AccountProviderEmr
        foreach ($accountProviders as $key => $provider) {
            $providerId = $provider['id'];
            $exists = false;
            foreach ($accountProviderEmr as $key => $value) {
                if ($value['provider_id'] == $providerId || $value['emr_user_id'] == $providerId) {
                    $exists = true;
                    $accountProviderEmr[$key]['provider_name'] = $provider['name'];
                    $accountProviderEmr[$key]['research_status'] = $provider['research_status'];
                    break;
                }
            }
            if (!$exists) {
                $new = array();
                $new["id"] = 0;
                $new["account_id"] = $accountId;
                $new["emr_user_id"] = $providerId;
                $new["emr_user_data"] = $provider['name'];
                $new['research_status'] = $provider['research_status'];
                $new["provider_id"] = 0;
                $new["mi7_user_id"] = null;
                $new["fb_page_id"] = null;
                $new["is_in_widget"] = null;
                $accountProviderEmr[] = $new;
            }
        }

        // Is involved in a Widget?
        $sql = sprintf(
            "SELECT GROUP_CONCAT(distinct ppw.provider_id) AS providers,
                GROUP_CONCAT(distinct ppw.practice_id) AS practices
            FROM provider_practice_widget AS ppw
            INNER JOIN widget AS w ON ppw.widget_id = w.id
            WHERE w.account_id = %d AND w.hidden=0 AND w.draft=0",
            $accountId
        );
        $ppw = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();

        $providersInWidget = !empty($ppw['providers']) ? explode(',', $ppw['providers']) : array();
        $practicesInWidget = !empty($ppw['practices']) ? explode(',', $ppw['practices']) : array();

        foreach ($accountProviderEmr as $key => $value) {
            $accountProviderEmr[$key]['is_in_widget'] = 0;
            if (!empty($value['provider_id']) && in_array($value['provider_id'], $providersInWidget)) {
                $accountProviderEmr[$key]['is_in_widget'] = 1;
            }
        }
        foreach ($accountPracticeEmr as $key => $value) {
            $accountPracticeEmr[$key]['is_in_widget'] = 0;
            if (!empty($value['practice_id']) && in_array($value['practice_id'], $practicesInWidget)) {
                $accountPracticeEmr[$key]['is_in_widget'] = 1;
            }
        }

        $arrWidget = Widget::model()->getWidgetsByAccountId($accountId);

        return array(
            'arrAccountEmr' => $accountEmr,
            'arrEmrTypes' => $arrEmrTypes,
            'mi7Integration' => $mi7Integration,
            'allowOverbooking' => $allowOverbooking,
            'hasOperatories' => $hasOperatories,
            'readOnlyIntegration' => $readOnlyIntegration,
            'btnSubmitCaption' => $btnSubmitCaption,
            'regressionCounter' => $regressionCounter,
            'aditionalInfo' => $aditionalInfo,
            'options' => $options,
            'accountProviders' => $accountProviders,
            'providersForPractices' => $providersForPractices,
            'accountPractices' => $accountPractices,
            'accountProviderEmr' => $accountProviderEmr,
            'accountPracticeEmr' => $accountPracticeEmr,
            'settingsSaved' => $settingsSaved,
            'errorString' => $errorString,
            'hasFacebookIntegration' => $hasFacebookIntegration,
            'arrWidget' => $arrWidget,
            'syncScheduleType' => $syncScheduleType,
            'telemedicineProviders' => $telemedicineProviders
        );
    }

    /**
     * Set Scheduling for DDC as Enable
     * Eg: This is called from /api/account/create when we create AAD accounts
     *     and we want to enable Booking by default
     * @param int $accountId
     * @param int $providerId
     */
    public static function enableSchedulingForDDC($accountId, $providerId)
    {

        // automatically turn on online appointments for this provider
        $pd = ProviderDetails::model()->find('provider_id=:providerId', array(':providerId' => $providerId));
        if (!$pd) {
            $pd = new ProviderDetails;
            $pd->provider_id = $providerId;
        }
        $pd->schedule_mail_enabled = 1;
        $pd->save();

        // Setting default emr for DDC
        $data = array();
        $accountEmr = array(
            'emr_type_id' => 3,  // EMR Type: Doctor.com
            'id' => 0, // account_emr.id
            'options' => ''
        );
        $data['accountEmr'] = $accountEmr;

        // Adding provider
        $accountProvider = AccountProvider::model()->find(
            "account_id = :account_id AND provider_id = :provider_id",
            array(':account_id' => $accountId, ':provider_id' => $providerId)
        );
        if (!empty($accountProvider)) {
            $data['postAccountProviderEmr'] = array(
                $accountProvider->id => array(
                    'present' => $providerId,
                    'checked' => 'on',
                    'provider_id' => $providerId
                )
            );
        }

        // Adding practices
        $accountPractices = (array)AccountPractice::getAccountPractices($accountId, 'array', false);
        if (empty($accountPractices)) {
            return;
        }

        $accountPracticesEmr = array();
        foreach ($accountPractices as $apra) {
            $accountPracticesEmr[$apra['id']] = array(
                'present' => '',
                'checked' => 'on',
                'practice_id' => $apra['practice_id']
            );
        }
        $data['postAccountPracticeEmr'] = $accountPracticesEmr;

        // Assign EMR
        AccountEmr::saveSettings($accountId, $data);
        NoEmrImporter::import($accountId);

        // Match practices
        $accountPracticeEmr = AccountPracticeEmr::getAllByAccountId($accountId);
        foreach ($accountPracticeEmr as $pape) {

            $practiceId = $pape->emr_location_id;
            if (empty($pape->practice_id)) {
                $pape->practice_id = $practiceId;
                $pape->save();
            }
            // Filling practice office hours
            $arrDays = PracticeOfficeHour::getOfficeHours($practiceId);

            // Enabled for Mon to Fri
            foreach ($arrDays as $k => $v) {
                // Days between Mon and Fri
                if (($k >= 1 && $k < 6) && $v['selected'] == 0) {
                    $arrDays[$k]['selected'] = 1;
                }
            }

            // Save provider_practice_schedule
            $postData = array();
            $postData['providers'][$providerId] = 1;
            $postData['provider_practice_schedule'][$providerId] = $arrDays;

            $results = ProviderPracticeSchedule::saveDataByPractice($postData, $practiceId, $accountId);
            if (Common::isTrue($results['success'])) {
                // Update Score
                Practice::model()->findByPk($practiceId)->updateScore();
            }
        }
    }

    /**
     * Get account EMR configuration for disabled and warning messages, given a provider and a practice
     *
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function getProviderPracticeMessages($providerId, $practiceId)
    {
        $sql = sprintf(
            "SELECT account_emr.message_disabled, account_emr.message_warning
            FROM account_emr
            INNER JOIN account_provider
                ON account_emr.account_id = account_provider.account_id
            INNER JOIN account_practice
                ON account_emr.account_id = account_practice.account_id
            INNER JOIN provider_practice
                ON (provider_practice.provider_id = account_provider.provider_id
                AND provider_practice.practice_id = account_practice.practice_id)
            INNER JOIN account_practice_emr
                ON (account_practice_emr.account_id = account_practice.account_id
                AND account_practice_emr.practice_id = account_practice.practice_id)
            INNER JOIN account_provider_emr
                ON (account_provider_emr.account_id = account_provider.account_id
                AND account_provider_emr.provider_id = account_provider.provider_id)
            WHERE provider_practice.provider_id = %d
                OR provider_practice.practice_id = %d
            ORDER BY account_emr.message_disabled IS NOT NULL DESC,
            account_emr.message_warning IS NOT NULL DESC;",
            $providerId,
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get account EMR configuration for disabled and warning messages, given a provider
     *
     * @param int $providerId
     * @return array
     */
    public static function getProviderMessages($providerId)
    {
        $sql = sprintf(
            "SELECT account_emr.message_disabled, account_emr.message_warning
            FROM account_emr
            INNER JOIN account_provider
            ON account_emr.account_id = account_provider.account_id
            INNER JOIN account_provider_emr
            ON (account_provider_emr.account_id = account_provider.account_id
            AND account_provider_emr.provider_id = account_provider.provider_id)
            WHERE account_provider.provider_id = %d
            ORDER BY account_emr.message_disabled IS NOT NULL DESC,
            account_emr.message_warning IS NOT NULL DESC;",
            $providerId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get account EMR configuration for disabled and warning messages, given a practice
     *
     * @param int $practiceId
     * @return array
     */
    public static function getPracticeMessages($practiceId)
    {
        $sql = sprintf(
            "SELECT account_emr.message_disabled, account_emr.message_warning
            FROM account_emr
            INNER JOIN account_practice
            ON account_emr.account_id = account_practice.account_id
            INNER JOIN account_practice_emr
            ON (account_practice_emr.account_id=account_practice.account_id
            AND account_practice_emr.practice_id = account_practice.practice_id)
            WHERE account_practice.practice_id = %d
            ORDER BY account_emr.message_disabled IS NOT NULL DESC,
            account_emr.message_warning IS NOT NULL DESC;",
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }
}
