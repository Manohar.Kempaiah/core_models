<?php

Yii::import('application.modules.core_models.models._base.BasePracticePhoto');

class PracticePhoto extends BasePracticePhoto
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getPracticeTooltip()
    {
        return '<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id .
            '" rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice . '">' .
            $this->practice . '</a>';
    }

    /**
     * returns a html link, used in the admin view
     * @return string
     */
    public function getPhotoPath()
    {
        if ($this->photo_path) {
            return '<a href="' . AssetsComponent::generateImgSrc($this->photo_path, 'practices', 'XL', true) .
                '"  target="_blank" title="Click to open">' .
                $this->photo_path . '</a>';
        } else {
            return '<span>No photo</span>';
        }
    }

    /**
     * @todo document
     * @param array $attributes
     * @param string $condition
     * @param array $params
     * @return int
     */
    public function updateAll($attributes, $condition = '', $params = array())
    {
        $numRows = parent::updateAll($attributes, $condition, $params);

        if ($numRows > 0) {
            $dataObject = array(
                "values" => $attributes
            );

            $dataObject["condition"] = array();
            foreach ($params as $key => $value) {
                $k = substr($key, 1);
                $dataObject["condition"][$k] = $value;
            }
        }

        return $numRows;
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_practice
            INNER JOIN practice_photo
                ON account_practice.practice_id = practice_photo.practice_id
            WHERE practice_photo.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * Clear widget cache, regenerate thumbnails and send new photos to Yelp
     * @return boolean
     */
    public function afterSave()
    {

        $sql = sprintf(
            "SELECT DISTINCT account_id
            FROM account_practice
            INNER JOIN practice_photo
                ON account_practice.practice_id = practice_photo.practice_id
            WHERE practice_photo.id = %d;",
            $this->id
        );
        $account = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($account as $account) {
            Widget::clearAccountWidget($account['account_id']);
        }

        if (empty($this->width) || empty($this->height)) {
            // Get and Update width and height
            $dir = str_replace('\\', '/', __DIR__);
            $folderPath = '/../modules/providers/images/uploads/practices/';
            $image = $dir . $folderPath . $this->photo_path;

            if (file_exists($image)) {
                $imageInfo = getimagesize($image);
                if (!empty($imageInfo)) {
                    // Update record
                    $this->width = $imageInfo[0];
                    $this->height = $imageInfo[1];
                    $this->save();
                }
            }
        }

        // regenerate thumbnails
        SqsComponent::sendMessage('thumbnailPracticeRegeneration', $this->photo_path);

        // send new photos to yelp
        SqsComponent::sendMessage('yelpSync', array('practiceId' => $this->practice_id, 'oldAttributes' => null));

        return parent::afterSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        $sql = sprintf(
            "SELECT DISTINCT account_id
            FROM account_practice
            INNER JOIN practice_photo
                ON account_practice.practice_id = practice_photo.practice_id
            WHERE practice_photo.id = %d;",
            $this->id
        );
        $account = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($account as $account) {
            Widget::clearAccountWidget($account['account_id']);
        }

        return parent::beforeDelete();
    }

    /**
     * Get all photos for this practice
     * @param int $practiceId
     * @return array
     */
    public static function getPracticePhotos($practiceId = 0)
    {

        if ($practiceId == 0) {
            return null;
        }

        $sql = sprintf(
            "SELECT id, practice_id, photo_path, photo_name, sort_order, cover, caption, '' AS real_path
            FROM practice_photo
            WHERE practice_id = %d
            ORDER BY cover DESC, sort_order ASC",
            $practiceId
        );

        $data = Yii::app()->db->createCommand($sql)->queryAll();

        if (!empty($data)) {
            foreach ($data as $key => $value) {
                $data[$key]['real_path'] = AssetsComponent::generateImgSrc(
                    $value['photo_path'],
                    'practices',
                    'XL',
                    true
                );
            }
        }
        return $data;
    }

    /**
     * Refresh cache every time a Photo is updated
     * @param array $queue
     */
    public static function regenerateThumbnail($queue)
    {
        $arrSizes = array(
            '1' => 'XS',
            '2' => 'S',
            '3' => 'L',
            '4' => 'XL',
            '5' => 'XXL',
            '6' => 'M',
            '7' => 'Other',
        );

        foreach ($arrSizes as $size) {
            $imgPhotoPath = AssetsComponent::generateImgSrc(
                $queue['body'],
                'practices',
                $size,
                false,
                false,
                false,
                true,
                true
            );
            if (!empty($imgPhotoPath)) {
                @file_get_contents($imgPhotoPath);
            }
        }
    }

    /**
     * Set as cover Photo
     * @param int $ppId practice_photo.id
     * @return string $photoRealPath
     */
    public static function makeCoverPhoto($ppId = 0)
    {

        $pp = PracticePhoto::model()->findByPk($ppId);
        if (empty($pp)) {
            return false;
        }

        PracticePhoto::model()->updateAll(
            array('cover' => '0'),
            'practice_id=:practice_id',
            array(':practice_id' => $pp->practice_id)
        );

        $pp->cover = 1;
        $pp->save();

        $practice = Practice::model()->findByPk($pp->practice_id);
        $practice->logo_path = ($pp->photo_path);
        $practice->save();

        // save to audit log and notify changes to phabricator
        $auditSection = 'Cover photo for provider #' . $pp->practice_id . '(' . $practice->name . ')';
        AuditLog::create('U', $auditSection, 'practice_photo', null, true);

        $realPhotoPath = '';
        if ($practice->logo_path) {
            $realPhotoPath = AssetsComponent::generateImgSrc($practice->logo_path, 'practices', 'XL', true, true);
        }

        return $realPhotoPath;
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $practiceId
     * @param int $accountId
     * @return array $results
     */
    public static function saveData($postData = '', $practiceId = 0, $accountId = 0)
    {
        $results['success'] = true;
        $results['error'] = "";

        if (empty($postData)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        if ($practiceId == 0 || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "PRACTICE_ID_ZERO";
            return $results;
        }

        // validate access
        ApiComponent::checkPermissions('Practice', $accountId, $practiceId);

        foreach ($postData as $data) {
            $model = null;
            $backendAction = !empty($data['backend_action']) ? trim($data['backend_action']) : '';
            $data['id'] = isset($data['id']) ? $data['id'] : 0;

            if ($data['id'] > 0) {
                $model = PracticePhoto::model()->findByPk($data['id']);
            }

            if ($backendAction == 'delete') {
                // delete the row
                if (!empty($model)) {
                    $cover = $model->cover;

                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PRACTICE_PHOTO";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }

                    if ($cover == 1) {
                        $practice = Practice::model()->findByPk($practiceId);
                        $practice->logo_path = '';
                        $practice->save();
                    }
                    $auditSection = 'Practice Photo practice_id #' . $practiceId;
                    AuditLog::create('D', $auditSection, 'practice_photo', null, false);
                }
                continue;
            }

            $auditAction = 'U';
            if (!$model) {
                $model = new PracticePhoto;
                $auditAction = 'C';
                $model->practice_id = $practiceId;
                // upload the new photo
                $resultUpload = File::upload($model, "photo_path", true);
                if (!$resultUpload['success']) {
                    $results['error'] = $resultUpload['error'];
                    $results['data'] = $resultUpload['data'];
                    return $results;
                }
                $model->photo_name = $resultUpload['uploaded_file_name'];
            }

            $model->sort_order = $data['sort_order'];
            $model->cover = $data['cover'];
            $model->caption = StringComponent::truncate($data['caption'], 100);

            if ($model->save()) {
                $auditSection = 'Practice Photo practice_id #' . $practiceId;
                AuditLog::create($auditAction, $auditSection, 'practice_photo', null, false);
                $results['success'] = true;
                $results['error'] = "";
                if ($auditAction == 'C') {
                    $data['id'] = $model->id;
                    unset($data['backend_action']);
                    $data['real_path'] = AssetsComponent::generateImgSrc(
                        $model->photo_path,
                        'practices',
                        'XL',
                        true,
                        true
                    );
                    $results['data'][] = $data;
                }

                if ($model->cover == 1) {
                    // set as Cover Photo
                    PracticePhoto::makeCoverPhoto($model->id);
                }
            } else {
                $results['success'] = false;
                $results['error'] = "ERROR_SAVING_PRACTICE_PHOTO";
                $results['data'] = $model->getErrors();
                return $results;
            }
        }

        return $results;
    }
}
