<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPreviousPractice');

class ProviderPreviousPractice extends BaseProviderPreviousPractice
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        $arrLabels = parent::attributeLabels();
        $arrLabels['aesthetic_procedure'] = Yii::t('app', 'Aesthetic Procedures Performed');
        return $arrLabels;
    }

}
