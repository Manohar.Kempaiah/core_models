<?php

Yii::import('application.modules.core_models.models._base.BaseScanStatsProvider');

class ScanStatsProvider extends BaseScanStatsProvider
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('scan_input_provider_id', 'required'),
            array('scan_input_provider_id, listing_accuracy, missing_listing, profile_completion_score, completed_scans, cnt_errors, data_element, total_reviews, healthgrades_reviews, op_score', 'numerical', 'integerOnly' => true),
            array('reviews_average, reviews_grade', 'length', 'max' => 10),
            array('healthgrades_primary_specialty', 'length', 'max' => 255),
            array('missing_listing_names, profile_completion_missing_data', 'safe'),
            array('listing_accuracy, missing_listing, profile_completion_score, completed_scans, cnt_errors, data_element, reviews_average, total_reviews, healthgrades_reviews, healthgrades_primary_specialty, reviews_grade, missing_listing_names, profile_completion_missing_data, op_score', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, scan_input_provider_id, listing_accuracy, missing_listing, profile_completion_score, completed_scans, cnt_errors, data_element, reviews_average, total_reviews, healthgrades_reviews, healthgrades_primary_specialty, reviews_grade, missing_listing_names, profile_completion_missing_data, op_score', 'safe', 'on' => 'search'),
        );
    }

    public function getLatestByProvider($providerId)
    {
        $criteria = new CDbCriteria();

        $criteria->with[] = 'scanInputProvider';
        $criteria->with[] = 'scanInputProvider.scanRequestPractice';
        $criteria->with[] = 'scanInputProvider.scanRequestPractice.scanRequest';


        $criteria->condition = 'scanInputProvider.provider_id = :provider_id';
        $criteria->params = [
            ':provider_id' => $providerId
        ];

        $criteria->order = 'scanInputProvider.id DESC, scanInputProvider.is_primary DESC';

        return $this->find($criteria);
    }

    public function getLatestByProviderForCompetitors($providerId)
    {
        $criteria = new CDbCriteria();

        $criteria->with[] = 'scanInputProvider';
        $criteria->with[] = 'scanInputProvider.scanRequestPractice';
        $criteria->with[] = 'scanInputProvider.scanRequestPractice.scanRequest';
        $criteria->with[] = 'scanInputProvider.scanRequestPractice.scanRequest.scanTemplate';
        $criteria->with['scanInputProvider.scanRequestPractice.scanRequest.scanTemplate.scanTemplateType'] = [
            'alias' => 'stt'
        ];

        $criteria->addCondition('scanInputProvider.provider_id = :provider_id');

        // Ignore initial competitors scan wich is reduced in listings
        $criteria->addCondition('stt.name != "Competitors"');

        $criteria->params = [
            ':provider_id' => $providerId
        ];

        $criteria->order = 'scanInputProvider.id DESC, scanInputProvider.is_primary DESC';

        return $this->find($criteria);
    }

    public function averageForStateSpecialty($stateId, $specialtyId)
    {
        $date = new DateTime("-2 months");
        $values = [
            ":state_id" => $stateId,
            ":sub_specialty_id" => $specialtyId,
            ":date" => $date->format("Y-m-d H:i:s")
        ];

        $sql = "
        SELECT
                AVG(scan_stats_provider.listing_accuracy) AS accuracy,
                AVG(scan_stats_provider.total_reviews) AS total_reviews,
                AVG(scan_stats_provider.reviews_average) AS reviews_average,
                AVG(scan_stats_provider.cnt_errors) AS amount_errors,
                AVG(scan_stats_provider.missing_listing) AS missing_listings,
                AVG(scan_stats_provider.op_score) AS score
            FROM scan_stats_provider
                JOIN scan_request_provider ON scan_request_provider.id = scan_stats_provider.scan_input_provider_id
                JOIN scan_request_practice ON scan_request_practice.id = scan_request_provider.scan_request_practice_id
                JOIN scan_request ON scan_request.id = scan_request_practice.scan_request_id
                JOIN provider ON provider.id = scan_request_provider.provider_id
                JOIN provider_specialty ON provider_specialty.provider_id = provider.id
                JOIN provider_practice ON provider_practice.provider_id = provider.id
                JOIN practice ON practice.id = provider_practice.practice_id
                JOIN location ON location.id = practice.location_id
                JOIN city ON city.id = location.city_id
                JOIN state ON state.id = city.state_id
            WHERE
                provider_specialty.sub_specialty_id = :sub_specialty_id
                AND state.id = :state_id
                AND scan_request.date_added > :date
                AND scan_stats_provider.total_reviews > 1
        ";

        return Yii::app()->db
            ->createCommand($sql)
            ->bindValues($values)
            ->queryRow();
    }
}
