<?php
Yii::import('application.modules.core_models.models._base.BaseUberallDashboard');

class UberallDashboard extends BaseUberallDashboard
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
