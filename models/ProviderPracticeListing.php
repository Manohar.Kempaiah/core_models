<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeListing');

class ProviderPracticeListing extends BaseProviderPracticeListing
{
    public $account_id;     // Allow GADM search by account_id
    public $organization_id; // Allow GADM search by organization_id

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'listing_type_id' => 'Listing Type',
            'practice_id' => 'Practice',
            'provider_id' => 'Provider',
            'url' => Yii::t('app', 'URL'),
            'date_added' => Yii::t('app', 'Date Added'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'listingType' => 'Listing Type',
            'practice' => 'Practice',
            'provider' => 'Provider',
        );
    }

    /**
     * Override rules so GADM can search by account_id
     * @return \CActiveDataProvider
     */
    public function rules()
    {
        return array(
            array(
                'listing_type_id, url',
                'required'
            ),
            array(
                'listing_type_id, practice_id, provider_id, verified',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'source, sync_status, claim_status',
                'length',
                'max' => 20
            ),
            array(
                'lrd_left, lrd_right',
                'length',
                'max' => 18
            ),
            array(
                'shortened_url',
                'length',
                'max' => 255
            ),
            array(
                'url',
                'length',
                'max' => 65535
            ),
            array(
                'date_added, date_updated, date_sync_checked, url',
                'safe'
            ),
            array(
                'practice_id, provider_id, date_added, date_updated, date_sync_checked, source, sync_status,
                    claim_status, lrd_left, lrd_right, shortened_url, url, verified',
                'default',
                'setOnEmpty' => true,
                'value' => null
            ),
            array(
                'id, listing_type_id, practice_id, provider_id, date_added, date_updated, date_sync_checked,
                    source, sync_status, claim_status, lrd_left, lrd_right, shortened_url, url, verified, account_id,organization_id',
                'safe',
                'on' => 'search'
            )
        );
    }

    /**
     * If we're saving a link to google mobile or yext, generate its shortened url
     * @return bool
     */
    public function beforeSave()
    {

        // Validate url
        $this->url = trim($this->url);

        // Validate not empty url
        if (empty($this->url)) {
            $this->addError('url', 'Please enter an URL');
            return false;
        }

        if (!filter_var($this->url, FILTER_VALIDATE_URL)) {
            $this->addError('url', 'Please enter a valid URL');
            return false;
        }

        // Validate duplicate url
        if (self::isDuplicateListing(
            $this->id,
            $this->url,
            $this->provider_id,
            $this->practice_id,
            $this->listing_type_id
        )) {
            $this->addError('url', 'This URL (' . $this->url . ') is already associated to a provider or practice');
            return false;
        }

        // Validate duplicate provider, practice listing_type
        if ($this->isNewRecord) {
            if (self::isDuplicateListing(0, '', $this->provider_id, $this->practice_id, $this->listing_type_id)) {
                $this->addError(
                    'practice_id',
                    'This provider is already associated to this practice and listing type: ' . $this->url
                );
                return false;
            }
        } else {
            if (self::isDuplicateListing(
                $this->id,
                '',
                $this->provider_id,
                $this->practice_id,
                $this->listing_type_id
            )) {
                $this->addError(
                    'practice_id',
                    'This provider is already associated to this practice and listing type: ' . $this->url
                );
                return false;
            }
        }

        // Validate url related to  url type
        switch ($this->listing_type_id) {
            case ListingType::YELP_PROFILE_ID:
                $errorString = 'Please enter a valid Yelp Review URL (beggining with "https://www.yelp.com/biz/...")';
                if (!strpos($this->url, '.yelp.')) {
                    $this->addError('url', $errorString);
                    return false;
                }

                if (!preg_match(
                    "/\b(?:(?:https?):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i",
                    $this->url
                )) {
                    $this->addError('url', $errorString);
                    return false;
                }
                break;
            case ListingType::YELP_WRITEREVIEW_ID:
                if (!stristr($this->url, '/writeareview/')) {
                    $errorString = 'Please enter a valid Yelp "Write a Review" URL (starts with ' .
                        '"https://www.yelp.com/writeareview/")';
                    $this->addError('url', $errorString);
                    return false;
                }
                break;
            case ListingType::GOOGLE_PROFILE_ID:
                // check it indludes the "login" section
                if (substr($this->url, 0, 50) != 'https://accounts.google.com/ServiceLogin?continue=') {
                    $this->addError('url', 'Please enter a valid Google Review URL
                    (beggining with "https://accounts.google.com/ServiceLogin?continue=")');
                    return false;
                }

                // check it's one of the new format urls:
                // http://search.google.com/local/writereview?placeid=ChIJN-93ZsBZwokRKqmdupb-XFA
                // or the old ones
                // https://www.google.com/search?q=Test&ludocid=12444014172228775909#lrd=0x89c25bfd25bfa2fb:0xacb204f55b9f6fe5,3,5
                if (strpos($this->url, 'placeid') === false && (strpos($this->url, 'ludocid') === false ||
                    strpos($this->url, 'lrd') === false)) {
                    $this->addError(
                        'url',
                        'Please enter a valid Google Review URL (including "placeid", or '
                            . '"ludocid" and "lrd" parameters)'
                    );
                    return false;
                }
                break;
            default:
                $baseUrl = ListingType::model()->findByPk($this->listing_type_id)->base_url;

                if (!empty($baseUrl)) {

                    $validUrl = false;
                    $arrUrl = explode(',', $baseUrl);

                    // Validate link
                    foreach ($arrUrl as $key) {
                        $key = trim($key);
                        if (empty($key)) {
                            continue;
                        }
                        if (strpos($this->url, $key) !== false) {
                            $validUrl = true;
                            break;
                        }
                    }

                    if (!$validUrl) {
                        $this->addError('url', 'Please enter a valid URL (with "' . $baseUrl . '")');
                        return false;
                    }
                }
                break;
        }

        // Shorte url for google, yelp, yelp write review
        if ($this->url != '' && ($this->listing_type_id == ListingType::YELP_PROFILE_ID ||
            $this->listing_type_id == ListingType::GOOGLE_PROFILE_ID ||
            $this->listing_type_id == ListingType::YELP_WRITEREVIEW_ID)) {
            // generate it
            $this->shortened_url = StringComponent::shortenURL($this->url);
        }

        // Validate google url
        if ($this->url != '' && $this->listing_type_id == ListingType::GOOGLE_PROFILE_ID) {
            // if it's a google write review url, update our lrd
            $urlPieces = parse_url($this->url);
            if (!empty($urlPieces['query'])) {
                $query = str_replace('continue=', '', $urlPieces['query']);
                $targetUrl = urldecode($query);
                $lrds = substr($targetUrl, strpos($targetUrl, '#lrd=') + 5);
                $lrds = substr($lrds, 0, strpos($lrds, ','));
                $lrds = explode(':', $lrds);
                if (!empty($lrds[0])) {
                    $lrdLeft = $lrds[0];
                }
                if (!empty($lrds[1])) {
                    $lrdRight = $lrds[1];
                }
                if (!empty($lrdLeft) && !empty($lrdRight)) {
                    $this->lrd_left = $lrdLeft;
                    $this->lrd_right = $lrdRight;
                }
            }
        }

        // Update Salesforce for google or yelp
        if (
            $this->listing_type_id == ListingType::YELP_PROFILE_ID || $this->listing_type_id ==
            ListingType::GOOGLE_PROFILE_ID || $this->listing_type_id == ListingType::YELP_WRITEREVIEW_ID
        ) {
            // google or yelp link, maybe we have to update SalesForce: does this practice belong to an organization?
            $arrOrganizations = Practice::belongsToOrganization($this->practice_id);
            if (!empty($arrOrganizations)) {

                $changed = false;
                if (empty($this->oldRecord) && $this->url != '') {
                    $changed = true;
                } else {
                    $oldRecord = $this->oldRecord;
                    if ($oldRecord->url != $this->url) {
                        $changed = true;
                    }
                }

                if ($changed) {
                    // send request to update salesforce
                    foreach ($arrOrganizations as $organizationId) {
                        SqsComponent::sendMessage('updateSFOrganizationLinks', $organizationId);
                    }
                }
            }
        }

        return parent::beforeSave();
    }

    /**
     * Check if we have to update SalesForce (in case a Google or Yelp link is deleted)
     * @return bool
     */
    public function beforeDelete()
    {
        if (
            $this->listing_type_id == ListingType::YELP_PROFILE_ID
            || $this->listing_type_id == ListingType::GOOGLE_PROFILE_ID
            || $this->listing_type_id == ListingType::YELP_WRITEREVIEW_ID
        ) {
            // google or yelp link, maybe we have to update SalesForce: does this practice belong to an organization?
            $arrOrganizations = Practice::belongsToOrganization($this->practice_id);
            if (!empty($arrOrganizations)) {
                // send request to update salesforce
                foreach ($arrOrganizations as $organizationId) {
                    SqsComponent::sendMessage('updateSFOrganizationLinks', $organizationId);
                }
            }
        }

        // delete from provider_practice_gmb
        $arrGmb = ProviderPracticeGmb::model()->findAll(
            'provider_practice_listing_id = :providerPracticeListingId',
            array(':providerPracticeListingId' => $this->id)
        );

        if (!empty($arrGmb)) {
            foreach ($arrGmb as $gmb) {
                $gmb->delete();
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Enable ReviewHub sharing when a Yelp (52) or Google Mobile (87) links is added at the practice level
     * Save to audit_log
     * @return mixed
     */
    public function afterSave()
    {

        // if a yelp or google mobile link is added at the practice level
        if (
            ($this->listing_type_id == ListingType::YELP_PROFILE_ID
                || $this->listing_type_id == ListingType::GOOGLE_PROFILE_ID)
            && $this->practice_id > 0 && $this->provider_id == null
        ) {

            // look up practice details
            $practiceDetails = PracticeDetails::model()->find(
                'practice_id=:practiceId',
                array(':practiceId' => $this->practice_id)
            );

            if (empty($practiceDetails)) {
                // create entry if necessary
                $practiceDetails = new PracticeDetails;
                $practiceDetails->practice_id = $this->practice_id;
                $practiceDetails->reviewhub_sharing = 'DYNAMIC';
                $practiceDetails->save();
            } elseif ($practiceDetails->reviewhub_sharing == '') {
                // just update it
                $practiceDetails->reviewhub_sharing = 'DYNAMIC';
                $practiceDetails->save();
            }
        }

        // save to audit_log in case something goes wrong
        AuditLog::create(
            $this->isNewRecord ? 'C' : 'U',
            'ProviderPracticeListing: afterSave',
            'provider_practice_listing',
            'Type #' . $this->listing_type_id .
                ($this->practice_id ? ' - Practice #' . $this->practice_id : '') .
                ($this->provider_id ? ' - Provider #' . $this->provider_id : ''),
            false
        );

        return parent::afterSave();
    }

    /**
     * Save to audit log
     * @return mixed
     */
    public function afterDelete()
    {

        // save to audit_log in case something goes wrong
        AuditLog::create(
            'D',
            'ProviderPracticeListing: afterDelete',
            'provider_practice_listing',
            'Type #' . $this->listing_type_id .
                ($this->practice_id ? ' - Practice #' . $this->practice_id : '') .
                ($this->provider_id ? ' - Provider #' . $this->provider_id : ''),
            false
        );

        return parent::afterDelete();
    }

    /**
     * GADM: Called on rendering the column for each row
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id='
            . $this->provider_id . '" rel="/administration/providerTooltip?id='
            . $this->provider_id . '" title="' . $this->provider . '">' . $this->provider . '</a>');
    }

    /**
     * GADM: Called on rendering the column for each row
     */
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id='
            . $this->practice_id . '" rel="/administration/practiceTooltip?id='
            . $this->practice_id . '" title="' . $this->practice . '">' . $this->practice . '</a>');
    }

    /**
     * take a practice_id-indexed array of G+ and Yelp links, and update or create necessary entries
     */
    public static function processPracticeLinkUpdates($set)
    {
        if (empty($set)) {
            return false;
        }

        $arrErrorReviews = array();

        foreach ($set as $practiceId => $links) {
            $arrErrorReviews[$practiceId]['yelp'] = '';
            $arrErrorReviews[$practiceId]['googleplus'] = '';
            $arrErrorReviews[$practiceId]['yelpwriteareview'] = '';
            $arrErrorReviews[$practiceId]['practice_id'] = $practiceId;

            // yelp
            $currYelpLink = ProviderPracticeListing::model()->find(
                'practice_id = :practiceId AND listing_type_id=:listingTypeId',
                array(
                    ':practiceId' => $practiceId,
                    ':listingTypeId' => ListingType::YELP_PROFILE_ID
                )
            ); // 52 is Yelp
            if (!empty($links['yelp'])) {

                // save yelp link
                if (!$currYelpLink) {
                    $currYelpLink = new ProviderPracticeListing;
                    $currYelpLink->listing_type_id = ListingType::YELP_PROFILE_ID; // Yelp
                    $currYelpLink->practice_id = $practiceId;
                    $currYelpLink->source = 'Manual (Org. Tool)';
                    $currYelpLink->verified = 1;

                    // create entry in audit log
                    AuditLog::create('C', 'Org. Tool', false, 'Create Yelp link for Pract. #' . $practiceId, false);
                } else {
                    // create entry in audit log
                    AuditLog::create('U', 'Org. Tool', false, 'Update Yelp link for Pract. #' . $practiceId, false);
                }

                $currYelpLink->url = $links['yelp'];

                if (!$currYelpLink->save()) {
                    $arrErrorReviews[$practiceId]['yelp'] = 'Sorry, this is not a valid Yelp link: ' .
                        (strlen($links['yelp']) > 35 ? substr($links['yelp'], 0, 31) . '...' : $links['yelp']);
                } else {
                    $arrErrorReviews[$practiceId]['yelp'] = '';
                }
            } elseif ($currYelpLink) {

                // create entry in audit log
                AuditLog::create('D', 'Org. Tool', false, 'Delete Yelp link for Pract. #' . $practiceId, false);

                // delete yelp link
                $currYelpLink->delete();
                $arrErrorReviews[$practiceId]['yelp'] = '';
            }

            // yelp's "write a review" link
            $reviewPPL = ProviderPracticeListing::model()->find(
                'practice_id <> :practiceId AND url=:url AND listing_type_id=:listingTypeId',
                array(
                    ':practiceId' => $practiceId,
                    ':url' => $links['yelpwriteareview'],
                    ':listingTypeId' => ListingType::YELP_WRITEREVIEW_ID
                )
            );

            if ($reviewPPL) {
                $practiceName = Practice::model()->findByPk($reviewPPL->practice_id)->name;
                $accountId = AccountPractice::model()->find(
                    'practice_id = :practice_id',
                    array(':practice_id' => $reviewPPL->practice_id)
                );

                // account exists?
                if (isset($accountId->account_id)) {
                    $arrErrorReviews[$practiceId]['yelpwriteareview'] = 'This link is already in use by
                        "' . $practiceName . '" - (Account Id: ' . $accountId->account_id . ').';
                } else {
                    $arrErrorReviews[$practiceId]['yelpwriteareview'] = 'This link is already in use by
                        "' . $practiceName . '".';
                }
            } else {
                $currYelpLink = ProviderPracticeListing::model()->find(
                    'practice_id = :practiceId AND listing_type_id=:listingTypeId',
                    array(
                        ':practiceId' => $practiceId,
                        ':listingTypeId' => ListingType::YELP_WRITEREVIEW_ID
                    )
                ); // 91 is Yelp's "Write a review"
                if (!empty($links['yelpwriteareview'])) {

                    // save yelp link
                    if (!$currYelpLink) {
                        $currYelpLink = new ProviderPracticeListing;
                        $currYelpLink->listing_type_id = ListingType::YELP_WRITEREVIEW_ID; // Yelp's "Write a review"
                        $currYelpLink->practice_id = $practiceId;
                        $currYelpLink->source = 'Manual (Org. Tool)';
                        $currYelpLink->verified = 1;

                        // create entry in audit log
                        AuditLog::create(
                            'C',
                            'Org. Tool',
                            false,
                            'Create Yelp Write a review link for Pract. #' . $practiceId,
                            false
                        );
                    } else {
                        // create entry in audit log
                        AuditLog::create(
                            'U',
                            'Org. Tool',
                            false,
                            'Update Yelp Write a review link for Pract. #' . $practiceId,
                            false
                        );
                    }

                    $currYelpLink->url = $links['yelpwriteareview'];

                    if (!$currYelpLink->save()) {
                        $arrErrorReviews[$practiceId]['yelpwriteareview'] =
                            'Sorry, this is not a valid Yelp\'s "Write a review" link: ' .
                            (strlen($links['yelpwriteareview']) > 35
                                ? substr($links['yelpwriteareview'], 0, 31) . '...'
                                : $links['yelpwriteareview']);
                    } else {
                        $arrErrorReviews[$practiceId]['yelpwriteareview'] = '';
                    }
                } elseif ($currYelpLink) {

                    // create entry in audit log
                    AuditLog::create(
                        'D',
                        'Org. Tool',
                        false,
                        'Deleted Yelp Write a Review link for Pract. #' . $practiceId,
                        false
                    );

                    // delete yelp link
                    $currYelpLink->delete();
                    $arrErrorReviews[$practiceId]['yelpwriteareview'] = '';
                }
            }

            // google
            $currGoogleLink = ProviderPracticeListing::model()->find(
                'practice_id = :practiceId AND listing_type_id=:listingTypeId',
                array(
                    ':practiceId' => $practiceId,
                    ':listingTypeId' => ListingType::GOOGLE_PROFILE_ID
                )
            ); // 87 is Google Mobile, was 21: Google+
            if (!empty($links['googleplus'])) {

                // save google link
                if (!$currGoogleLink) {
                    $currGoogleLink = new ProviderPracticeListing;
                    $currGoogleLink->listing_type_id = ListingType::GOOGLE_PROFILE_ID; // 87 is Google Mobile
                    $currGoogleLink->practice_id = $practiceId;
                    $currGoogleLink->source = 'Manual (Org. Tool)';
                    $currGoogleLink->verified = 1;

                    // create entry in audit log
                    AuditLog::create('C', 'Org. Tool', false, 'Create Google link for Pract. #' . $practiceId, false);
                } else {
                    // create entry in audit log
                    AuditLog::create('U', 'Org. Tool', false, 'Update Google link for Pract. #' . $practiceId, false);
                }

                $currGoogleLink->url = $links['googleplus'];

                if (!$currGoogleLink->save()) {
                    $arrErrorReviews[$practiceId]['googleplus'] = 'Sorry, this is not a valid Google link: ' .
                        (strlen($links['googleplus']) > 35
                            ? substr($links['googleplus'], 0, 31) . '...'
                            : $links['googleplus']);
                } else {
                    $arrErrorReviews[$practiceId]['googleplus'] = '';
                }
            } elseif ($currGoogleLink) {

                // create entry in audit log
                AuditLog::create('D', 'Org. Tool', false, 'Deleted Google link for Pract. #' . $practiceId, false);

                // delete google link
                $currGoogleLink->delete();
                $arrErrorReviews[$practiceId]['googleplus'] = '';
            }
        }

        return $arrErrorReviews;
    }

    /**
     * Get social review links: Returns array with G+ and Yelp links
     * @param int $providerId
     * @param int $practiceId
     * @param array $listingTypes
     * @return array $arrLinks
     */
    public static function getReviewsLink($providerId = 0, $practiceId = 0, $listingTypes = [])
    {

        if (empty($providerId) && empty($practiceId)) {
            return false;
        }

        $retLinks = $arrLinks = null;

        if (empty($listingTypes)) {
            // If is empty, use the default listingType (Yelp && Google)
            $listingTypes = [
                ListingType::YELP_PROFILE_ID,
                ListingType::GOOGLE_PROFILE_ID
            ];
        }

        foreach ($listingTypes as $listingTypeId) {

            $providerAtPracticeCondition = sprintf("OR (provider_id IS NULL AND practice_id = %d )", $practiceId);
            if (in_array($listingTypeId, array(ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID, ListingType::YELP_PROVIDER_ID))) {
                // add the providers@practice level
                if (empty($providerId)) {
                    $providerAtPracticeCondition = sprintf(
                        "OR (provider_id IS NOT NULL AND practice_id = %d )",
                        $practiceId
                    );
                } else {
                    $providerAtPracticeCondition = sprintf(
                        "OR (provider_id = %d AND practice_id = %d )",
                        $providerId,
                        $practiceId
                    );
                }
            }

            // first at the provider_practice level, then provider level, then practice level
            $sql = sprintf(
                "SELECT CAST(
                        CONCAT(
                            IF(provider_id IS NOT NULL AND practice_id IS NOT NULL, '1',
                                IF (provider_id IS NOT NULL AND practice_id IS NULL, '2', '3')
                            ), listing_type_id
                        ) AS UNSIGNED
                    ) AS option_order,
                    listing_type_id, IF(shortened_url != '', shortened_url, url) AS url,
                    provider_id,
                    `provider`.first_m_last as provider_name,
                    practice_id, url AS long_url,
                    IF(source IS NOT NULL, source, 'GADM') AS source,
                    provider_practice_listing.id
                FROM provider_practice_listing
                LEFT JOIN provider ON provider_practice_listing.provider_id = `provider`.id
                WHERE (
                    (provider_id = %d AND practice_id = %d) /* 1 */
                    OR (provider_id = %d AND practice_id IS NULL) /* 2 */
                    %s /* 3 */
                )
                AND listing_type_id = %d
                ORDER BY option_order ASC
                LIMIT 1;",
                $providerId,
                $practiceId,
                $providerId,
                $providerAtPracticeCondition,
                $listingTypeId
            );

            $links = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();
            if (!empty($links)) {
                $arrLinks[$listingTypeId] = $links;
            }
        }

        if (empty($arrLinks)) {
            // neither google nor yelp was found so leave
            return false;
        }

        foreach ($arrLinks as $link) {
            $retLinks['provider_id'] = $link['provider_id'];
            $retLinks['provider_name'] = $link['provider_name'];

            if ($link['listing_type_id'] == ListingType::GOOGLE_PROFILE_ID) {

                // 87: Google mobile, was 21: Google+
                $retLinks['google+_id'] = $link['id'];
                $retLinks['google+_long_url'] = $link['long_url'];
                $retLinks['google+_source'] = $link['source'];
                $retLinks['google+'] = $link['url'];
                $retLinks['google+_placeid'] = '';
                if ($link['source'] == 'Partner Dashboard') {
                    $retLinks['google+_placeid'] = ProviderPracticeListing::getPlaceId($link['long_url']);
                }
                $retLinks['google+_provider_id'] = $link['provider_id'];
                $retLinks['google+_provider_name'] = $link['provider_name'];
                $retLinks['google+_listing_type_id'] = $link['listing_type_id'];

            } elseif ($link['listing_type_id'] == ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID) {

                // 146: Google
                $retLinks['google_id_pp'] = $link['id'];
                $retLinks['google_pp_long_url'] = $link['long_url'];
                $retLinks['google_pp_source'] = $link['source'];
                $retLinks['google_pp'] = $link['url'];
                $retLinks['google_pp_placeid'] = '';
                if ($link['source'] == 'Partner Dashboard') {
                    $retLinks['google_pp_placeid'] = ProviderPracticeListing::getPlaceId($link['long_url']);
                }
                $retLinks['google_pp_provider_id'] = $link['provider_id'];
                $retLinks['google_pp_provider_name'] = $link['provider_name'];
                $retLinks['google_pp_listing_type_id'] = $link['listing_type_id'];

            } elseif ($link['listing_type_id'] == ListingType::YELP_PROFILE_ID) {

                // 52: yelp
                $retLinks['yelp_id'] = $link['id'];
                $retLinks['yelp_long_url'] = $link['long_url'];
                $retLinks['yelp_source'] = $link['source'];
                $retLinks['yelp'] = $link['url'];
                $retLinks['yelp_placeid'] = '';
                $retLinks['yelp_provider_id'] = $link['provider_id'];
                $retLinks['yelp_provider_name'] = $link['provider_name'];
                $retLinks['yelp_listing_type_id'] = $link['listing_type_id'];

            }
        }

        return $retLinks;
    }


    /**
     * Get all review links for a practice and the associated providers
     *
     * @param $practiceId
     * @return array
     */
    public static function getAllReviewLinks($practiceId)
    {
        $listingTypes = array(
            ListingType::GOOGLE_PROFILE_ID,
            ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID,
            ListingType::YELP_PROFILE_ID,
            ListingType:: YELP_PROVIDER_ID
        );

        $sql = sprintf(
            "SELECT
                id,
                listing_type_id,
                provider_id,
                practice_id,
                IF(shortened_url != '', shortened_url, url) AS url,
                url AS long_url,
                IF(source IS NOT NULL, source, 'GADM') AS source
            FROM provider_practice_listing
            WHERE listing_type_id IN (%s)
              AND (
                    (provider_id IS NULL AND practice_id = %d)
                    OR
                    (
                        provider_id IN (SELECT provider_id FROM provider_practice WHERE practice_id = %d)
                        AND (practice_id = %d OR practice_id IS NULL)
                    )
                )
            ORDER BY id DESC;",
            implode(',', $listingTypes),
            $practiceId,
            $practiceId,
            $practiceId
        );

        $results = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();

        $response = array(
            'google+' => null,
            'google+_id' => null,
            'google+_long_url' => null,
            'google+_source' => null,
            'yelp' => null,
            'yelp_id' => null,
            'yelp_long_url' => null,
            'yelp_source' => null,
            'providers' => array()
        );
        foreach ($results as $result) {
            switch ($result['listing_type_id']) {
                case ListingType::GOOGLE_PROFILE_ID:
                    $response['google+_id'] =  $result['id'];
                    $response['google+'] =  $result['url'];
                    $response['google+_long_url'] =  $result['long_url'];
                    $response['google+_source'] =  $result['source'];
                    break;

                case ListingType::YELP_PROFILE_ID:
                    $response['yelp_id'] =  $result['id'];
                    $response['yelp'] =  $result['url'];
                    $response['yelp_long_url'] =  $result['long_url'];
                    $response['yelp_source'] =  $result['source'];
                    break;

                case ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID:
                    if (!isset($response['providers'][$result['provider_id']])) {
                        $response['providers'][$result['provider_id']] = array(
                            'google+' => null,
                            'google+_id' => null,
                            'yelp' => null,
                            'yelp_id' => null,
                        );
                    }
                    $response['providers'][$result['provider_id']]['google+_id'] = $result['id'];
                    $response['providers'][$result['provider_id']]['google+'] = $result['url'];
                    break;

                case ListingType::YELP_PROVIDER_ID:
                    if (!isset($response['providers'][$result['provider_id']])) {
                        $response['providers'][$result['provider_id']] = array(
                            'google+' => null,
                            'google+_id' => null,
                            'yelp' => null,
                            'yelp_id' => null,
                        );
                    }
                    $response['providers'][$result['provider_id']]['yelp_id'] = $result['id'];
                    $response['providers'][$result['provider_id']]['yelp'] = $result['url'];
                    break;
                default:
                    break;
            }
        }

        return $response;
    }

    /**
     * Get a review links for a provider
     * @param $providerId
     * @param $practiceId
     * @param $listingTypes
     * @return array
     */
    public static function getProviderReviewLinks($providerId, $practiceId = null, $listingTypes = null)
    {
        if (!$listingTypes) {
            $listingTypes = array(
                ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID,
                ListingType:: YELP_PROVIDER_ID
            );
        }

        if (is_array($listingTypes)) {
            $listingTypes = implode(',', $listingTypes);
        }

        $sql = sprintf(
            "SELECT
                id,
                listing_type_id,
                provider_id,
                practice_id,
                IF(shortened_url != '', shortened_url, url) AS url
            FROM provider_practice_listing
            WHERE listing_type_id IN (%s)
              AND ((provider_id = %d AND (practice_id = %d OR practice_id IS NULL)))
            ORDER BY id DESC;",
            $listingTypes,
            $providerId,
            $practiceId
        );

        $results = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();

        $response = array(
            'google+' => null,
            'google+_id' => null,
            'yelp' => null,
            'yelp_id' => null,
        );

        foreach ($results as $result) {
            if ($result['listing_type_id'] == ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID) {
                $response['google+_id'] = $result['id'];
                $response['google+'] = $result['url'];
            }
            elseif ($result['listing_type_id'] == ListingType:: YELP_PROVIDER_ID) {
                $response['yelp_id'] = $result['id'];
                $response['yelp'] = $result['url'];
            }
        }

        return $response;
    }

    /**
     * Process and parse the url
     */
    public static function getPlaceId($longUrl = null)
    {

        if (empty($longUrl)) {
            return null;
        }

        $parsedUrl = parse_url($longUrl);
        if (!empty($parsedUrl['query'])) {
            parse_str($parsedUrl['query'], $response);
            if (!empty($response['continue'])) {
                // we need to analyze it again
                // [continue] => http://search.google.com/local/writereview?placeid=ChIJSdxhGQFZwokRToQ2gjFZXKw
                $parsedUrl = parse_url($response['continue']);
                if (!empty($parsedUrl['query'])) {
                    parse_str($parsedUrl['query'], $response);
                    if (!empty($response['placeid'])) {
                        return $response['placeid'];
                    }
                }
            } elseif (!empty($response['placeid'])) {
                return $response['placeid'];
            }
            return $response;
        }

        return $parsedUrl;
    }


    /**
     * Gets account and practice information for people missing their Google+ or Yelp ReviewHub sharing links as long as
     * they belong to an active organization
     * @see actionMissingReviewLinksReminder
     * @return array
     */
    public static function getReviewHubsMissingLinks()
    {
        $sql = sprintf(
            "SELECT GROUP_CONCAT(DISTINCT(salesforce_id) SEPARATOR ', ') AS salesforce_id,
            GROUP_CONCAT(DISTINCT(account.id) SEPARATOR ', ') AS account_id,
            GROUP_CONCAT(DISTINCT(CONCAT(account.first_name, ' ', account.last_name)) SEPARATOR ', ') AS account,
            GROUP_CONCAT(DISTINCT(device.id) SEPARATOR ', ') AS device_id,
            GROUP_CONCAT(DISTINCT(device.nickname) SEPARATOR ', ') AS device,
            practice.id AS practice_id,
            practice.name AS practice,
            IF(yelp_practice_link.id IS NULL AND google_practice_link.id IS NULL, 'Both',
            IF(yelp_practice_link.id IS NULL, 'Yelp', 'Google+')) AS missing
            FROM account_device
            INNER JOIN account
                ON account_device.account_id = account.id
            INNER JOIN device
                ON account_device.device_id = device.id
                AND device.status = 'U'
            INNER JOIN practice
                ON account_device.practice_id = practice.id
            INNER JOIN organization_account
                ON account.id = organization_account.account_id
                AND connection = 'O'
            INNER JOIN organization
                ON organization_account.organization_id = organization.id
                AND organization.status = 'A'
                AND organization.salesforce_id != ''
            LEFT JOIN provider_practice_listing AS yelp_practice_link
                ON account_device.practice_id = yelp_practice_link.practice_id
                AND yelp_practice_link.listing_type_id = 52 /* Yelp */
            LEFT JOIN provider_practice_listing AS google_practice_link
                ON account_device.practice_id = google_practice_link.practice_id
                AND google_practice_link.listing_type_id = 87 /* Google Mobile, was 21: Google+ */
            WHERE account_device.status = 'A'
                AND (yelp_practice_link.id IS NULL
                OR google_practice_link.id IS NULL)
            GROUP BY practice_id;",
            ListingType::YELP_PROFILE_ID,
            ListingType::GOOGLE_PROFILE_ID,
            ListingType::YELP_PROFILE_ID,
            ListingType::GOOGLE_PROFILE_ID
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Synchronize all social listings for a given customer from Yext into our database
     * @param Organization $organization
     * @param Account $account
     * @return boolean
     */
    public static function yextSync($organization, $account)
    {

        // loop through this account's practices
        foreach ($account->accountPractices as $accountPractice) {

            // look up each practice
            $practice = Practice::model()->findByPk($accountPractice->practice_id);
            if (empty($practice) || empty($practice->yext_location_id)) {
                // continue if we can't find the practice
                continue;
            }

            // get all listings for this customer/location
            $arrListings = self::yextGetLocationListings(
                $organization->yext_customer_id,
                $practice->yext_location_id,
                $practice->id
            );
            if (!empty($arrListings)) {
                // go through all the listings and create/update them
                foreach ($arrListings as $listing) {
                    self::yextCreate($practice->id, $listing);
                }
            }
        }

        return true;
    }

    /**
     * For a given Yext location belonging to a given Yext customer, get listings
     * @param int $customerId
     * @param int $locationId
     * @return array
     */
    private static function yextGetLocationListings($customerId, $locationId)
    {

        $arrListings = array(); // array of listings
        // get this location's listings
        $objListings = YextComponent::getListings(array('customerId' => $customerId, 'locationId' => $locationId));

        if (empty($objListings)) {
            // no listings, leave
            return false;
        }

        // go through all listings
        foreach ($objListings['statuses'] as $listing) {
            if ($listing['status'] == 'LIVE' && !empty($listing['url']) && $listing['url'] != '') {
                // add it to the list of reviews to be inserted
                $arrListings[] = $listing;
            }
        }

        // return the remaining listings (to be created/updated)
        return $arrListings;
    }

    /**
     * Create entries in provider_practice_listing with information coming from Yext
     * @param int $practiceId
     * @param array $yextListing
     * @return bool
     */
    private static function yextCreate($practiceId, $yextListing)
    {

        // look for the listing site for this siteId
        $siteObj = ListingSite::model()->find('name=:name', array(':name' => $yextListing['siteId']));

        if (!$siteObj) {
            return false;
        }

        // look for the listingType for this listing site
        $listingType = ListingType::model()->find('listing_site_id=:siteId', array(':siteId' => $siteObj->id));

        if (!$listingType) {
            return false;
        }

        // load the existing model
        $ppl = ProviderPracticeListing::model()->find(
            'practice_id=:practiceId AND provider_id IS NULL AND listing_type_id=:listingTypeId',
            array(
                ':practiceId' => $practiceId,
                ':listingTypeId' => $listingType->id
            )
        );
        if (!$ppl) {
            $ppl = new ProviderPracticeListing;
            $ppl->practice_id = $practiceId;
            $ppl->url = substr($yextListing['url'], 0, 255);
            $ppl->listing_type_id = $listingType->id;
            $ppl->source = 'Yext';
            $ppl->verified = 1;
            $ppl->sync_status = substr($yextListing['status'], 0, 20);

            if ($ppl->validate() && !$ppl->save()) {
                echo 'Error while saving provider_practice_listing for practice #' . $practiceId
                    . ' (URL was ' . $ppl->url . '): ';
                echo serialize($ppl->getErrors());
                echo PHP_EOL;
            }
        }

        return true;
    }

    /**
     * Look for ppl duplicate for provider, practice, listing_type and url
     * @param int $id
     * @param string $url
     * @param int $providerId
     * @param int $practiceId
     * @param int $listingTypeId
     * @return boolean
     */
    private static function isDuplicateListing($id, $url = '', $providerId, $practiceId, $listingTypeId)
    {

        // replace single quote
        $url = str_replace("'", "\'", $url);

        $idCondition = $id == 0 ? '' : sprintf(' AND id <> %d ', $id);
        $urlCondition = empty($url) ? '' : sprintf(" AND url = '%s' ", $url);
        $providerCondition = empty($providerId)
            ? ' AND provider_id IS NULL '
            : sprintf(' AND provider_id = %d ', $providerId);

        $practiceCondition = empty($practiceId)
            ? ' AND practice_id IS NULL '
            : sprintf(' AND practice_id = %d ', $practiceId);

        $sql = sprintf(
            "SELECT COUNT(1)
            FROM provider_practice_listing
            WHERE listing_type_id = %d
            %s
            %s
            %s
            %s;",
            $listingTypeId,
            $urlCondition,
            $providerCondition,
            $practiceCondition,
            $idCondition
        );

        $exist = Yii::app()->db->createCommand($sql)->queryScalar();

        if ($exist <> 0) {
            return true;
        }
        return false;
    }

    /**
     * RH: Get Name, Address, Phone Google+/Yelp links for a provider at a practice according to the practice's
     * settings
     * @param int $providerId
     * @param int $practiceId
     * @param Patient $patient
     * @param int $providerRatingId
     * @param bool $isNewTemplate
     * @return array
     */
    public static function getNAPLinks(
        $providerId,
        $practiceId,
        $patient,
        $providerRatingId = 0,
        $isNewTemplate = false
    ) {

        $htmlLink = $plaintextLink = $gmailActionLink = '';
        $sendGPlus = $sendYelp = false;

        // get nap links
        $listingTypes = [
            ListingType::YELP_PROFILE_ID,
            ListingType::GOOGLE_PROFILE_ID,
            ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID
        ];
        $arrLinks = ProviderPracticeListing::getReviewsLink($providerId, $practiceId, $listingTypes);

        if (empty($arrLinks['google+']) && empty($arrLinks['yelp']) && empty($arrLinks['google_pp'])) {
            return false;
        }

        if (!empty($arrLinks['google_pp'])) {
            // We have p@p links, so we use it instead the google+ links
            $arrLinks['google+_id'] = $arrLinks['google_id_pp'];
            $arrLinks['google+_long_url'] = $arrLinks['google_pp_long_url'];
            $arrLinks['google+_source'] = $arrLinks['google_pp_source'];
            $arrLinks['google+'] = $arrLinks['google_pp'];
            $arrLinks['google+_placeid'] = (isset($arrLinks['google_pp_placeid'])) ? $arrLinks['google_pp_placeid'] : '';
            $arrLinks['google+_provider_id'] = $arrLinks['google_pp_provider_id'];
            $arrLinks['google+_provider_name'] = $arrLinks['google_pp_provider_name'];
            $arrLinks['google+_listing_type_id'] = $arrLinks['google_pp_listing_type_id'];

            // unset unused items
            unset($arrLinks['google_id_pp']);
            unset($arrLinks['google_pp_long_url']);
            unset($arrLinks['google_pp_source']);
            unset($arrLinks['google_pp']);
            unset($arrLinks['google_pp_placeid']);
            unset($arrLinks['google_pp_provider_id']);
            unset($arrLinks['google_pp_provider_name']);
            unset($arrLinks['google_pp_listing_type_id']);
        }


        $shareLinks = 'DYNAMIC';
        $practiceDetails = PracticeDetails::model()->find(
            'practice_id=:practiceId',
            array(':practiceId' => $practiceId)
        );
        if ($practiceDetails && $practiceDetails->reviewhub_sharing != '') {
            $shareLinks = $practiceDetails->reviewhub_sharing;
        }

        if ($shareLinks != 'NONE') {
            if ($shareLinks == 'DYNAMIC') {
                // decide where to link to dynamically (based on patient's email address)
                if (!empty($arrLinks['google+']) && !empty($arrLinks['yelp'])) {
                    if (!empty($patient->email) && substr($patient->email, -10) == '@gmail.com') {
                        // only g+
                        $sendGPlus = true;
                    } else {
                        // only yelp
                        $sendYelp = true;
                    }
                } elseif (!empty($arrLinks['google+'])) {
                    // only g+
                    $sendGPlus = true;
                } else {
                    // only yelp
                    $sendYelp = true;
                }
            } elseif ($shareLinks == 'BOTH') {
                $sendGPlus = !empty($arrLinks['google+']);
                $sendYelp = !empty($arrLinks['yelp']);
            } elseif ($shareLinks == 'GOOGLE+') {
                $sendGPlus = !empty($arrLinks['google+']);
            } elseif ($shareLinks == 'YELP!') {
                $sendYelp = !empty($arrLinks['yelp']);
            }
        }

        $gPlusLink = false;
        $yelpLink = false;

        if (!$sendGPlus && !$sendYelp) {
            return false;
        }

        // actual link - $providerRating is only present for SMS links so that we can show the "copy text" interstitial
        if ($sendGPlus) {
            $gPlusLink = 'https://' . Yii::app()->params->servers['dr_pro_hn'] . '/site/reviewRedirect?to='
                . base64_encode($arrLinks['google+']) . '&id=' . base64_encode($providerRatingId)
                . '&type=' . base64_encode('G');
        }
        if ($sendYelp) {
            $yelpLink = 'https://' . Yii::app()->params->servers['dr_pro_hn'] . '/site/reviewRedirect?to='
                . base64_encode($arrLinks['yelp']) . '&id=' . base64_encode($providerRatingId)
                . '&type=' . base64_encode('Y');
        }

        // gmail action link defaults to google+
        $gmailActionLink = ($gPlusLink ? $gPlusLink : $yelpLink);

        // html version
        $htmlLink = '';
        if ($sendGPlus) {
            // are we using the new template? (with different graphics)
            if (!$isNewTemplate) {
                // no, use old graphics
                $googleButton = '<br><img src="https://assets.doctor.com/email/u_social_links_google.png" alt="'
                    . Yii::t('kiosk', "mail_click_to_post") . ' Google">';
                $htmlLink .= '<a href="' . $gPlusLink . '" title="' . Yii::t('kiosk', "mail_click_to_post")
                    . ' Google">' . $googleButton . '</a><br />';
            } else {
                // yes, use new graphics
                $htmlLink .= AssetsComponent::getGoogleButton($gPlusLink);
            }
        }
        if ($sendYelp) {
            // are we using the new template? (with different graphics)
            if (!$isNewTemplate) {
                // no, use old graphics
                $yelpButton = '<br><img src="https://assets.doctor.com/email/u_social_links_yelp.png" alt="' .
                    Yii::t('kiosk', "mail_click_to_post") . ' Yelp">';
                $htmlLink .= '<a href="' . $yelpLink . '" title="' . Yii::t('kiosk', "mail_click_to_post") .
                    ' Yelp">' . $yelpButton . '</a><br />';
            } else {
                // yes, use new graphics
                $htmlLink .= AssetsComponent::getYelpButton($yelpLink);
            }
        }

        // plaintext version
        $plaintextLink = PHP_EOL;
        // do we have a google link?
        if ($sendGPlus) {
            // yes, add it
            $plaintextLink .= StringComponent::shortenURL($gPlusLink) . PHP_EOL;
        }
        // do we have a yelp link?
        if ($sendYelp) {
            // yes, add it
            $plaintextLink .= StringComponent::shortenURL($yelpLink) . PHP_EOL;
        }

        return array($sendGPlus, $sendYelp, $htmlLink, $plaintextLink, $gmailActionLink, $gPlusLink, $yelpLink);
    }

    /**
     * Get practice Social Links
     *
     * @param int $practiceId
     * @param bool $useCache
     * @param bool $useInternalCondition
     * @param int $providerId (if not null, add as condition)
     * @return array
     */
    public static function getPracticeSocialLinks(
        $practiceId = 0,
        $useCache = true,
        $useInternalCondition = true,
        $providerId = 0
    ) {
        // Set the cache time based on use cache parameter
        $cacheTime = $useCache ? Yii::app()->params['cache_medium'] : 0;

        if ($practiceId == 0) {
            return null;
        }

        $providerCondition = ' AND provider_id IS NULL ';
        if (!empty($providerId)) {
            $providerCondition = sprintf(" AND (provider_id IS NULL OR provider_id = %d) ", $providerId);
        }

        $internalCondition = '';
        if (Common::isTrue($useInternalCondition)) {
            $internalCondition = ' AND is_internal = FALSE';
        }

        $sql = sprintf(
            "SELECT ppl.id, ppl.listing_type_id, ppl.practice_id, ppl.provider_id,
            `listing_site`.`name`, ppl.url AS link, ppl.date_added, ppl.date_updated, ppl.date_sync_checked,
            `source`, ppl.sync_status, ppl.claim_status, ppl.lrd_left, ppl.lrd_right, ppl.shortened_url, ppl.url
            FROM provider_practice_listing AS ppl
            INNER JOIN listing_type AS lt
                ON ppl.listing_type_id = lt.id
            INNER JOIN listing_site
                ON lt.listing_site_id = listing_site.id
            WHERE practice_id = %d
                %s
                %s ;",
            $practiceId,
            $providerCondition,
            $internalCondition
        );

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Get provider Social Links
     * @param int $providerId
     * @return array
     */
    public static function getProviderSocialLinks($providerId = 0)
    {
        if ($providerId == 0) {
            return null;
        }

        $sql = sprintf(
            "SELECT ppl.id, ppl.listing_type_id, ppl.practice_id, ppl.provider_id,
            `listing_site`.`name`, ppl.url AS link, ppl.date_added, ppl.date_updated, ppl.date_sync_checked,
            `source`, sync_status, claim_status, lrd_left, lrd_right, shortened_url, url
            FROM provider_practice_listing AS ppl
            INNER JOIN listing_type
                ON ppl.listing_type_id = listing_type.id
            INNER JOIN listing_site
                ON listing_type.listing_site_id = listing_site.id
            WHERE provider_id = %d
                AND practice_id IS NULL
                AND is_internal = FALSE;",
            $providerId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get provider Social Links for API
     * @param string $which practice_id || provider_id
     * @param int $recordId
     * @param string $listingTypeName
     * @return array
     */
    public static function getSocialLinks($which = '', $recordId = 0, $listingTypeName = 'name')
    {
        if ($recordId == 0 || empty($which)) {
            return null;
        }

        if ($which == 'provider') {
            $listingTypePer = 'V';
            $leftJoin = '';
            $tableAssociation = 'provider_id';
            $negateAssociation = 'practice_id';
        } else {
            $listingTypePer = 'T';
            $leftJoin = 'AND provider_practice_listing.practice_id IS NOT NULL';
            $tableAssociation = 'practice_id';
            $negateAssociation = 'provider_id';
        }

        $sql = sprintf(
            "SELECT provider_practice_listing.id, listing_type.id AS listing_type_id,
            listing_site.name AS %s, provider_practice_listing.url
            FROM listing_type
            INNER JOIN listing_site
                ON listing_type.listing_site_id = listing_site.id
            LEFT JOIN provider_practice_listing
                ON listing_type.id = provider_practice_listing.listing_type_id
                %s
                AND provider_practice_listing.%s IS NULL
                AND provider_practice_listing.%s = %d
            WHERE listing_type.per='%s'
                AND url_type='P'
                AND listing_type.active='1'
                AND listing_type.editable='1'
                AND managed_padm='1'
            ORDER BY listing_type.name",
            $listingTypeName,
            $leftJoin,
            $negateAssociation,
            $tableAssociation,
            $recordId,
            $listingTypePer
        );

        $tempResult = Yii::app()->db->createCommand($sql)->queryAll();
        $arrSocialLinks = array();

        if (!empty($tempResult)) {
            foreach ($tempResult as $value) {
                $arrSocialLinks[$value['listing_type_id']] = $value;
            }
        }
        return $arrSocialLinks;
    }

    /**
     * Get provider/practice Social Links
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function getProviderPracticeSocialLinks($providerId = 0, $practiceId = 0)
    {
        if ($providerId == 0 || $practiceId == 0) {
            return null;
        }

        $sql = sprintf(
            "SELECT listing_site.name, url AS link, listing_type_id
            FROM provider_practice_listing
            INNER JOIN listing_type
                ON provider_practice_listing.listing_type_id = listing_type.id
            INNER JOIN listing_site
                ON listing_type.listing_site_id = listing_site.id
            WHERE provider_id = %d
                AND practice_id = %d
                AND is_internal = FALSE; ",
            $providerId,
            $practiceId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Check whether all practices have their Yelp and Google+ links in order to update Salesforce:
     * Has_Social_Links__c
     * @param int $organizationId
     * @return bool
     */
    public static function updateSFOrganizationLinks($organizationId)
    {

        $organization = Organization::model()->findByPk($organizationId);
        if (!$organization || empty($organization->salesforce_id)) {
            return false;
        }

        // count number of links this organization has
        $linkCount = self::countOrganizationLinks($organizationId);
        if ($linkCount == 0) {
            // no links at all
            return Yii::app()->salesforce->updateAccountField(
                $organization->salesforce_id,
                'Has_Social_Links__c',
                'false'
            );
        }

        // count number of practices this organization has
        $practiceCount = self::countOrganizationPractices($organizationId);
        if ($practiceCount == $linkCount / 3) {
            // we have 3 links per practice, so we're good!
            return Yii::app()->salesforce->updateAccountField(
                $organization->salesforce_id,
                'Has_Social_Links__c',
                'true'
            );
        }

        // we're missing links
        return Yii::app()->salesforce->updateAccountField(
            $organization->salesforce_id,
            'Has_Social_Links__c',
            'false'
        );
    }

    /**
     * Count the number of links an organization has
     * @param int $organizationId
     * @return int
     */
    public static function countOrganizationLinks($organizationId)
    {

        if (empty($organizationId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT COUNT(1) AS link_count
            FROM organization
            INNER JOIN organization_account
                ON organization.id = organization_account.organization_id
                AND `connection` = 'O'
            INNER JOIN account_practice
                ON organization_account.account_id = account_practice.account_id
            INNER JOIN provider_practice_listing
                ON account_practice.practice_id = provider_practice_listing.practice_id
                AND provider_practice_listing.listing_type_id IN (%d, %d, %d) /* Yelp, Google */
                AND provider_practice_listing.provider_id IS NULL
            WHERE organization.id = %d;",
            ListingType::YELP_PROFILE_ID,
            ListingType::YELP_WRITEREVIEW_ID,
            ListingType::GOOGLE_PROFILE_ID,
            $organizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
    }

    /**
     * Count the number of practices an organization has
     * @param int $organizationId
     * @return int
     */
    public static function countOrganizationPractices($organizationId)
    {

        if (empty($organizationId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT COUNT(1) AS practice_count
            FROM organization
            INNER JOIN organization_account
                ON organization.id = organization_account.organization_id
                AND `connection` = 'O'
            INNER JOIN account_practice
                ON organization_account.account_id = account_practice.account_id
            WHERE organization.id = %d;",
            $organizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
    }

    /**
     * Find the yelp link just as the review is -- don't simulate having a practice it doesn't have
     * @param int $providerId
     * @param int $practiceId
     * @return mix
     */
    public static function findLinkYelp($providerId, $practiceId)
    {
        // if we have nothing, try with the standard yelp link
        $ppl = ProviderPracticeListing::model()->find(
            'listing_type_id=:listingTypeId AND practice_id=:practiceId AND provider_id=:providerId',
            array(
                ':listingTypeId' => ListingType::YELP_PROFILE_ID,
                ':practiceId' => $practiceId,
                ':providerId' => $providerId
            )
        );

        // if we have nothing, try without the provider
        if (empty($ppl) && $practiceId) {
            // try with the standard yelp link
            $ppl = ProviderPracticeListing::model()->find(
                'listing_type_id=:listingTypeId AND practice_id=:practiceId AND provider_id IS NULL',
                array(
                    ':listingTypeId' => ListingType::YELP_PROFILE_ID,
                    ':practiceId' => $practiceId
                )
            );
        }

        return $ppl;
    }

    /*
     * Send email to Account Manager when links are created, modified or deleted for paying clients.
     * @param int $arrData
     * @return bool
     */
    public static function alertCSLinksChanged($arrData)
    {
        $organization = Organization::model()->findByPk($arrData['organization_id']);
        if (!$organization) {
            return false;
        }

        $mailTo = '';
        $practiceDetail = Practice::model()->findByPk($arrData['practice_id']);

        // Salesforce Details
        if (!empty($organization->salesforce_id)) {
            $sfDetails = Account::getSalesforceDetails($organization->salesforce_id);
            if ($sfDetails) {
                $mailTo = $sfDetails['account_rep_email'];
            }
        }

        // Mail is empty
        if (empty($mailTo)) {
            $mailTo = 'cs@corp.doctor.com';
        }

        // send email notification to "Account Rep" associated
        $mailFrom = array(
            'From' => "notifications@corp.doctor.com",
            'From_Name' => 'Doctor.com Link ' . $arrData['status']
        );

        switch ($arrData['type']) {
            case ListingType::GOOGLE_PROFILE_ID:
                $linkType = 'Google';
                break;
            case ListingType::YELP_PROFILE_ID:
                $linkType = 'Yelp';
                break;
            case ListingType::YELP_WRITEREVIEW_ID:
                $linkType = 'Yelp write a review';
                break;
            default:
                $linkType = '';
                break;
        }

        $mailSubject = 'Link was ' . $arrData['status'];

        // use practice id by default, and name if we have it
        $practiceName = '#' . $arrData['practice_id'];
        if (!empty($practiceDetail)) {
            $practiceDetail = $practiceDetail->name;
        }

        // prepare email body
        $mailAltBody = $linkType . ' link (' . $arrData['url'] . ') was ' . $arrData['status']
            . ' from practice ' . $practiceName;
        $mailBody = nl2br($mailAltBody);

        // send the email
        MailComponent::sendEmail(
            $mailFrom,
            $mailTo,
            false,
            false,
            $mailSubject,
            $mailBody,
            $mailAltBody,
            false,
            false,
            false
        );

        return true;
    }

    /**
     * get urls for export on organization tool
     * @param int $organizationId
     * @return array
     */
    public static function getUrlsExport($organizationId)
    {

        if (empty($organizationId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT account_id
            FROM organization_account
            WHERE `connection`= 'O' AND organization_id = %d",
            $organizationId
        );
        $ownerAccountId = Yii::app()->db->createCommand($sql)->queryScalar();

        if ($ownerAccountId) {
            $sql = sprintf(
                "SELECT DISTINCT
                first_m_last, practice_name, address_full,
                city_name, state_name, zipcode,
                phone_number, location_name, url,
                dateadded, sync_status, claim_status,
                date_sync_checked, source
                FROM
                (
                    SELECT pro.first_m_last, pra.name AS practice_name, pra.address_full,
                    cit.name AS city_name, sta.name AS state_name, loc.zipcode,
                    pra.phone_number, listing_site.name AS location_name, ppl.url,
                    DATE_FORMAT(ppl.date_added,'%%m/%%d/%%Y') AS dateadded, ppl.sync_status, ppl.claim_status,
                    ppl.date_sync_checked, ppl.source
                    FROM account_provider AS ap
                    INNER JOIN provider_practice_listing AS ppl
                    ON ap.provider_id = ppl.provider_id
                    INNER JOIN listing_type AS lt
                    ON lt.id = ppl.listing_type_id
                    INNER JOIN listing_site
                    ON lt.listing_site_id = listing_site.id
                    INNER JOIN provider AS pro
                    ON pro.id = ppl.provider_id
                    INNER JOIN provider_practice AS pp
                    ON pro.id = pp.provider_id
                    INNER JOIN practice AS pra
                    ON pp.practice_id = pra.id
                    INNER JOIN location AS loc
                    ON loc.id = pra.location_id
                    INNER JOIN city AS cit
                    ON cit.id = loc.city_id
                    INNER JOIN state AS sta
                    ON sta.id = cit.state_id
                    WHERE pp.primary_location = 1
                    AND ap.account_id = %d
                UNION ALL
                    SELECT pro.first_m_last, pra.name AS practice_name, pra.address_full,
                    cit.name AS city_name, sta.name AS state_name, loc.zipcode,
                    pra.phone_number, listing_site.name AS location_name, ppl.url,
                    DATE_FORMAT(ppl.date_added,'%%m/%%d/%%Y') AS dateadded, ppl.sync_status, ppl.claim_status,
                    ppl.date_sync_checked, ppl.source
                    FROM account_practice AS ap
                    INNER JOIN provider_practice_listing AS ppl
                    ON ap.practice_id = ppl.practice_id
                    INNER JOIN listing_type AS lt
                    ON lt.id = ppl.listing_type_id
                    INNER JOIN listing_site
                    ON lt.listing_site_id = listing_site.id
                    INNER JOIN practice AS pra
                    ON ppl.practice_id = pra.id
                    INNER JOIN provider_practice AS pp
                    ON pra.id = pp.practice_id
                    INNER JOIN provider AS pro
                    ON pro.id = pp.provider_id
                    INNER JOIN location AS loc
                    ON loc.id = pra.location_id
                    INNER JOIN city AS cit
                    ON cit.id = loc.city_id
                    INNER JOIN state AS sta
                    ON sta.id = cit.state_id
                    WHERE ap.account_id = %d
                ) AS l
                ORDER BY first_m_last, practice_name, location_name;",
                $ownerAccountId,
                $ownerAccountId
            );
            return Yii::app()->db->createCommand($sql)->queryAll();
        }
        return array();
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $providerId
     * @param int $practiceId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = null, $practiceId = null, $ownerAccountId = 0)
    {
        $results['data'] = [];
        if ($ownerAccountId == 0 || ($providerId == 0 && $practiceId == 0)) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        // validate access
        $tblModel = 'Practice';
        $checkingId = $practiceId;
        if ($providerId > 0) {
            $tblModel = 'Provider';
            $checkingId = $providerId;
        }
        ApiComponent::checkPermissions($tblModel, $ownerAccountId, $checkingId);

        foreach ($postData as $data) {
            $auditAction = '';
            $url = empty($data['url']) ? '' : trim($data['url']);
            $backendAction = (isset($data['backend_action'])) ? trim(strtolower($data['backend_action'])) : '';

            $listingTypeId = $data['listing_type_id'];

            if ($providerId > 0) {
                $practiceId = null;
                $model = ProviderPracticeListing::model()->find(
                    'listing_type_id=:listingTypeId AND provider_id=:providerId AND practice_id IS NULL',
                    array(':listingTypeId' => $listingTypeId, ':providerId' => $providerId)
                );
            } else {
                $providerId = null;
                $model = ProviderPracticeListing::model()->find(
                    'listing_type_id=:listingTypeId AND practice_id=:practiceId AND provider_id IS NULL',
                    array(':listingTypeId' => $listingTypeId, ':practiceId' => $practiceId)
                );
            }

            if ($backendAction == 'delete' ||  empty($url)) {
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_PRACTICE_LISTING";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }
                    $auditAction = 'D';
                }
            } else {

                if (!StringComponent::validateUrl($url)) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_PRACTICE_LISTING";
                    $results['data'] = $data;
                    return $results;
                }

                $auditAction = 'U';
                if (empty($model)) {
                    $auditAction = 'C';
                    $model = new ProviderPracticeListing;
                }
                $model->provider_id = $providerId;
                $model->practice_id = $practiceId;
                $model->listing_type_id = $listingTypeId;
                $model->url = $url;
                // Validate source
                if (empty($model->source)) {
                    $model->source = 'PADM';
                }
                if (!$model->save()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_PRACTICE_LISTING";
                    $results['data'] = $model->getErrors();
                    return $results;
                } else {
                    $data['id'] = $model->id;
                    unset($data['backend_action']);
                    $results['data'][] = $data;
                }
            }

            if (!empty($auditAction)) {
                $auditSection = 'ProviderPracticeListing #' . $providerId
                    . ' (#' . $model->practice_id . '-' . $model->url . ')';
                AuditLog::create($auditAction, $auditSection, 'provider_practice_listing', null, false);
            }
        }
        return $results;
    }

    /**
     * Get url for provider/practice and code
     * @param string $code
     * @param int $providerId
     * @param int $practiceId
     */
    public static function getListingUrl($code, $providerId = 0, $practiceId = 0)
    {
        $practiceNullCondition = '';
        if ($code == 'doctorpractice') {
            $code = 'doctor';
            $practiceNullCondition = 'AND provider_practice_listing.provider_id IS NULL';
        }

        if ($code == 'googleplus') {
            $code = 'google+';
        }

        if ($providerId == 0) {
            $providerPracticeCondition = sprintf("provider_practice_listing.practice_id = %d", $practiceId);
        } else {
            $providerPracticeCondition = sprintf("provider_practice_listing.provider_id = %d", $providerId);
        }

        $sql = sprintf(
            "SELECT provider_practice_listing.id,
                provider_practice_listing.url, provider_practice_listing.verified
            FROM provider_practice_listing
            INNER JOIN listing_type
            ON provider_practice_listing.listing_type_id = listing_type.id
            WHERE listing_type.code = '%s'
            AND %s %s;",
            $code,
            $providerPracticeCondition,
            $practiceNullCondition
        );

        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Override search so GADM can search by provider or practice name
     * @return \CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria();

        $criteria->compare('t.id', $this->id);
        $criteria->compare('listing_type_id', $this->listing_type_id);
        if (strlen($this->practice_id)) {
            if (is_numeric($this->practice_id)) {
                $criteria->compare('practice.id', $this->practice_id);
            } else {
                $criteria->compare('practice.name', $this->practice_id, true);
            }
        }
        if (strlen($this->provider_id)) {
            if (is_numeric($this->provider_id)) {
                $criteria->compare('provider.id', $this->provider_id);
            } else {
                $criteria->compare('provider.first_last', $this->provider_id, true);
            }
        }
        $criteria->compare('t.date_added', $this->date_added, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('date_sync_checked', $this->date_sync_checked, true);
        $criteria->compare('source', $this->source, true);
        $criteria->compare('sync_status', $this->sync_status, true);
        $criteria->compare('claim_status', $this->claim_status, true);
        $criteria->compare('lrd_left', $this->lrd_left, true);
        $criteria->compare('lrd_right', $this->lrd_right, true);
        $criteria->compare('shortened_url', $this->shortened_url, true);
        $criteria->compare('t.verified', $this->verified, true);
        $criteria->compare('url', $this->url, true);
        if (!empty($this->account_id)) {
            $criteriaAccount = new CDbCriteria();
            $criteriaAccount->compare('account_provider.account_id', $this->account_id, true, 'OR');
            $criteriaAccount->compare('account_practice.account_id', $this->account_id, true, 'OR');
            $criteria->mergeWith($criteriaAccount, 'AND');
        }
        $criteria->join .= ' LEFT JOIN provider ON provider.id = t.provider_id';
        $criteria->join .= ' LEFT JOIN practice ON practice.id = t.practice_id';
        if (!empty($this->account_id)) {
            $criteria->join .= ' LEFT JOIN account_provider ON account_provider.provider_id = t.provider_id';
            $criteria->join .= ' LEFT JOIN account_practice ON account_practice.practice_id = t.practice_id';
        }

        return new CActiveDataProvider(
            $this,
            array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 100
                )
            )
        );
    }

    /**
     * Override search so GADM can search by provider or practice name
     * @return \CActiveDataProvider
     */
    public function searchthirdPartyUrl()
    {
        $criteria1 = new CDbCriteria();

        $criteria1->compare('t.id', $this->id);
        if (strlen($this->practice_id)) {
            if (is_numeric($this->practice_id)) {
                $criteria1->compare('practice.id', $this->practice_id);
            } else {
                $criteria1->compare('practice.name', $this->practice_id, true);
            }
        }
        if (strlen($this->provider_id)) {
            if (is_numeric($this->provider_id)) {
                $criteria1->compare('provider.id', $this->provider_id);
            } else {
                $criteria1->compare('provider.first_last', $this->provider_id, true);
            }
        }

        if (strlen($this->organization_id)) {
            if (is_numeric($this->organization_id)) {
                $criteria1->compare('organization.id', $this->organization_id);
            } else {
                $criteria1->compare('organization.organization_name', $this->organization_id, true);
            }
        }


        $criteria1->compare('t.date_added', $this->date_added, true);
        $criteria1->compare('t.date_updated', $this->date_updated, true);
        $criteria1->compare('t.verified', $this->verified);
        $criteria1->compare('url', $this->url, true);
        $criteria1->addInCondition('t.listing_type_id', array(156));

        $criteria1->join .= ' INNER JOIN provider ON provider.id = t.provider_id';
        $criteria1->join .= ' INNER JOIN practice ON practice.id = t.practice_id';
        $criteria1->join .= ' INNER JOIN account_provider ON account_provider.provider_id = t.provider_id';
        $criteria1->join .= ' INNER JOIN account_practice ON account_practice.practice_id = t.practice_id AND account_practice.account_id = account_provider.account_id';
        $criteria1->join .= ' INNER JOIN organization_account ON organization_account.account_id = account_provider.account_id and organization_account.connection ="O"';
        $criteria1->join .= ' INNER JOIN organization ON organization.id = organization_account.organization_id';
        $criteria1->select = ['t.*,provider.first_last as provider_name,practice.name as practice_name,GROUP_CONCAT( DISTINCT organization.organization_name SEPARATOR "/ ") as organization_name'];
        $criteria1->group='t.provider_id,t.practice_id,t.listing_type_id';
        $criteria1->order='t.id desc';

        $prov1= new CActiveDataProvider(
            $this,
            array(
                'criteria' => $criteria1,
                'pagination' => false
            )
        );

        $criteria2 = new CDbCriteria();

        $criteria2->compare('t.id', $this->id);
        if (strlen($this->provider_id)) {
            if (is_numeric($this->provider_id)) {
                $criteria2->compare('provider.id', $this->provider_id);
            } else {
                $criteria2->compare('provider.first_last', $this->provider_id, true);
            }
        }

        if (strlen($this->organization_id)) {
            if (is_numeric($this->organization_id)) {
                $criteria2->compare('organization.id', $this->organization_id);
            } else {
                $criteria2->compare('organization.organization_name', $this->organization_id, true);
            }
        }


        $criteria2->compare('t.date_added', $this->date_added, true);
        $criteria2->compare('t.date_updated', $this->date_updated, true);
        $criteria2->compare('t.verified', $this->verified);
        $criteria2->compare('url', $this->url, true);
        $criteria2->addInCondition('t.listing_type_id', array(157));
        if (strlen($this->practice_id)) {
            $criteria2->addCondition('practice_name IS NOT NULL');
        }

        $criteria2->join .= ' INNER JOIN provider ON provider.id = t.provider_id';
        $criteria2->join .= ' INNER JOIN account_provider ON account_provider.provider_id = t.provider_id';
        $criteria2->join .= ' INNER JOIN organization_account ON organization_account.account_id = account_provider.account_id and organization_account.connection ="O"';
        $criteria2->join .= ' INNER JOIN organization ON organization.id = organization_account.organization_id';
        $criteria2->select = ['t.*,provider.first_last as provider_name," " as practice_name,GROUP_CONCAT( DISTINCT organization.organization_name SEPARATOR "/ ") as organization_name'];
        $criteria2->group='t.provider_id,t.practice_id,t.listing_type_id';
        $criteria2->order='t.id desc';

        $prov2= new CActiveDataProvider(
            $this,
            array(
                'criteria' => $criteria2,
                'pagination' => false
            )
        );
        $criteria3 = new CDbCriteria();

        $criteria3->compare('t.id', $this->id);
        if (strlen($this->practice_id)) {
            if (is_numeric($this->practice_id)) {
                $criteria3->compare('practice.id', $this->practice_id);
            } else {
                $criteria3->compare('practice.name', $this->practice_id, true);
            }
        }

        if (strlen($this->organization_id)) {
            if (is_numeric($this->organization_id)) {
                $criteria3->compare('organization.id', $this->organization_id);
            } else {
                $criteria3->compare('organization.organization_name', $this->organization_id, true);
            }
        }


        $criteria3->compare('t.date_added', $this->date_added, true);
        $criteria3->compare('t.date_updated', $this->date_updated, true);
        $criteria3->compare('t.verified', $this->verified);
        $criteria3->compare('url', $this->url, true);
        $criteria3->addInCondition('t.listing_type_id', array(158));
        if (strlen($this->provider_id)) {
            $criteria3->addCondition('provider_name IS NOT NULL');
        }

        $criteria3->join .= ' INNER JOIN practice ON practice.id = t.practice_id';
        $criteria3->join .= ' INNER JOIN account_practice ON account_practice.practice_id = t.practice_id';
        $criteria3->join .= ' INNER JOIN organization_account ON organization_account.account_id = account_practice.account_id and organization_account.connection ="O"';
        $criteria3->join .= ' INNER JOIN organization ON organization.id = organization_account.organization_id';
        $criteria3->select = ['t.*," " as provider_name,practice.name as practice_name,GROUP_CONCAT( DISTINCT organization.organization_name SEPARATOR "/ ") as organization_name'];
        $criteria3->group='t.provider_id,t.practice_id,t.listing_type_id';
        $criteria3->order='t.id desc';

        $prov3= new CActiveDataProvider(
            $this,
            array(
                'criteria' => $criteria3,
                'pagination' => false
            )
        );

        $records = array();

        for ($i = 0; $i < $prov1->totalItemCount; $i++) {
            $data = $prov1->data[$i];
            array_push($records, $data);
        }
        if (!strlen($this->practice_id)) {
            for ($i = 0; $i < $prov2->totalItemCount; $i++) {
                $data = $prov2->data[$i];
                array_push($records, $data);
            }
        }
        if (!strlen($this->provider_id)) {
            for ($i = 0; $i < $prov3->totalItemCount; $i++) {
                $data = $prov3->data[$i];
                array_push($records, $data);
            }
        }
        //or you could use $records=array_merge($prov1->data , $prov2->data);

        return new CArrayDataProvider(
            $records,
            array(
                'sort' => array(
                    'attributes' => array(
                        'id',
                        'title'
                    ),
                ),
                'pagination' => array('pageSize' => 50)
            )
        );
    }

    /**
     * Gets the listings for a provider or practice. The type parameter
     * defines what type of listing we are searching for.
     *
     * @param integer $id The entity ID (provider_id or practice_id)
     * @param string $type The type (Provider or Practice)
     *
     * @return mixed
     */
    public function getListings($id, $type)
    {
        $field = $type === "provider" ? "provider" : "practice";

        $sql = "SELECT
                  provider_practice_listing.id,
                  provider_practice_listing.listing_type_id,
                  provider_practice_listing.practice_id,
                  provider_practice_listing.provider_id,
                  provider_practice_listing.date_added,
                  provider_practice_listing.date_updated,
                  provider_practice_listing.date_sync_checked,
                  provider_practice_listing.source,
                  provider_practice_listing.sync_status,
                  provider_practice_listing.claim_status,
                  provider_practice_listing.lrd_left,
                  provider_practice_listing.lrd_right,
                  provider_practice_listing.shortened_url,
                  provider_practice_listing.url,
                  provider_practice_listing.verified,
                  provider_practice_listing.harvester_id,
                 listing_type.code
                FROM provider_practice_listing
                  LEFT JOIN listing_type ON provider_practice_listing.listing_type_id = listing_type.id
                WHERE " . $field . "_id = :id";

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValue(':id', $id, PDO::PARAM_INT)
            ->queryAll();

    }

    public function byCodeProviderAndPractice($code, $practiceId = null, $providerId = null)
    {
        $criteria = new CDbCriteria;
        $criteria->with = [
            'listingType',
            'practice',
            'provider'
        ];

        // Eager loading
        $criteria->together = true;

        $criteria->compare('listingType.code', $code);
        $criteria->compare('t.practice_id', $practiceId);
        $criteria->compare('t.provider_id', $providerId);

        return $this->find($criteria);
    }
    public static function getProviderThirdpartyUrl($providerId = null)
    {

        if (empty($providerId)) {
            return null;
        }

        $sql = "SELECT IF( EXISTS( Select url
                        FROM prd01.provider_practice_listing
                        WHERE listing_type_id = 157 AND url is not null and provider_id = :provider_id limit 1 ),
                        (SELECT url FROM prd01.provider_practice_listing where listing_type_id = 157 and provider_id = :provider_id limit 1)
                        , ( select url from prd01.provider_practice_listing where listing_type_id = 156 and provider_id = :provider_id limit 1)
                        ) as url";
        $command  = Yii::app()->db->createCommand($sql);
        return $command->queryScalar(array(
                        ':provider_id'=> $providerId));
    }
}
