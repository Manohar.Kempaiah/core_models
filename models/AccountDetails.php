<?php

Yii::import('application.modules.core_models.models._base.BaseAccountDetails');

class AccountDetails extends BaseAccountDetails
{

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BaseAccountDetails the static model class
     */
    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Check if this account have details
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->isNewRecord) {
            $exists = AccountDetails::model()->exists(
                'account_id=:account_id',
                array(':account_id' => $this->account_id)
            );
            if ($exists) {
                $this->addError(
                    'AccountDetails_account_id',
                    'This account already has a details entry'
                );
                return false;
            }
        } elseif ($this->google_auto_publish != $this->oldRecord->google_auto_publish) {
            // this is an existing record and google's auto-publish settings changed, so we need to update salesforce
            $accountObj = Account::model()->findByPk($this->account_id);
            // did we find the account?
            if (!empty($accountObj)) {
                // yes, look up the organization
                $organizationObj = $accountObj->getOwnedOrganization();
                // did we find the organization?
                if (!empty($organizationObj)) {
                    // yes, queue updating it in salesforce through sqs
                    SqsComponent::sendMessage('salesforceUpdateAccountGMB', $organizationObj->id);
                }
            }
        }
        return parent::beforeSave();
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['claimed_provider_id'] = Yii::t('app', 'Claimed Provider ID');

        return $labels;
    }

    /**
     * Save the account details
     * @param arr $postData
     * @param arr $accessData - Account data for the logged user
     * @return array
     */
    public static function saveData($accountId, $postData)
    {
        $response['success'] = false;

        // AccountId Exist?
        if (empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "ERROR_SAVING_ACCOUNT_DETAILS";
            $results['data'] = "";
            return $results;
        }

        $accountDetails = AccountDetails::model()->find('account_id=:account_id', array(':account_id' => $accountId));
        if (empty($accountDetails)) {
            $accountDetails = new AccountDetails();
            $accountDetails->account_id = $accountId;
        }

        $details = !empty($postData) ? $postData : null;

        // this fields must be omitted from post
        $prohibitedFields = array('id', 'account_id');

        $schema = AccountDetails::model()->getSchema();
        foreach ($schema as $fieldName => $fieldType) {
            // if not prohibited field
            if (!in_array($fieldName, $prohibitedFields)) {

                // is posted?
                if (isset($postData[$fieldName])) {

                    if ($fieldType == 'string') {
                        if (is_array($postData[$fieldName])) {
                            $value = json_encode($postData[$fieldName]);
                        } else {
                            $value = trim($postData[$fieldName]);
                        }
                    } elseif ($fieldType == 'email') {
                        $value = StringComponent::validateEmail($postData[$fieldName]);
                    } elseif ($fieldType == 'integer') {
                        $value = (int) $postData[$fieldName];
                    } else {
                        $value = $postData[$fieldName];
                    }

                    $accountDetails->$fieldName = $value;
                }
            }
        }

        if ($accountDetails->save()) {
            $results['success'] = true;
            $results['error'] = "";
            $results['data'] = (array) $accountDetails->attributes;
            return $results;
        }

        $results['error'] = "ERROR_SAVING_ACCOUNT_DETAILS";
        $results['data'] = $details->getErrors();

        return $results;
    }

    /**
     * Return account_details record
     *
     * @param int $accountId
     * @param bool $useCache
     * @return obj account_details record
     */
    public static function getAccountDetails($accountId = 0, $useCache = true)
    {
        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;
        return AccountDetails::model()->cache($cacheTime)->find(
            "account_id =:accountId",
            array(':accountId' => $accountId)
        );
    }

    /**
     * Return account_details->frontend_settings
     *
     * @param int $accountId
     * @return array account_details->frontend_settings
     */
    public static function getSettings($accountId = 0)
    {
        $predefinedVars = array(
            'manual_review_alert' => 1,
            'reviewhub_upsell' => 1,
            'premium_upsell' => 1,
            'companion_upsell' => 1,
            'first_time_login' => 1,
            'first_time_review_request' => 1
        );

        $account = Account::model()->cache(Yii::app()->params['cache_short'])->find(
            'id=:account_id',
            array(':account_id' => $accountId)
        );

        if (empty($account)) {
            return array(
                'success' => false,
                'http_response_code' => 400,
                'error' => 'INVALID_ACCOUNT_ID'
            );
        }

        $details = AccountDetails::model()->find('account_id=:account_id', array(':account_id' => $accountId));
        $accountDetails = array();
        if (!empty($details)) {
            $accountDetails = (array) $details->attributes;
        }

        $frontendSettings = (array) json_decode(
            empty($accountDetails['frontend_settings'])
                ? ''
                : $accountDetails['frontend_settings']
        );

        if ($account->is_beta_account == 1) {
            $frontendSettings['first_time_login'] = 0;
        }

        $results['success'] = true;
        $results['http_response_code'] = 200;
        $results['error'] = '';
        $results['data'] = array_merge($predefinedVars, $frontendSettings);

        return $results;
    }

    /**
     * Save account_details->frontend_settings
     *
     * @param int $accountId
     * @param array $data
     * @return arr account_details->frontend_settings
     */
    public static function saveSettings($accountId = 0, $data)
    {

        $details = AccountDetails::getSettings($accountId);
        $frontendSettings = $details['data'];

        $postData = array_merge($frontendSettings, $data);

        $results = AccountDetails::saveData($accountId, array('frontend_settings' => $postData));
        if ($results['success']) {
            $results['data'] = json_decode($results['data']['frontend_settings'], true);
        }
        return $results;
    }
}
