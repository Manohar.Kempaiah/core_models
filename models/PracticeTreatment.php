<?php

Yii::import('application.modules.core_models.models._base.BasePracticeTreatment');

class PracticeTreatment extends BasePracticeTreatment
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @todo document
     * @return boolean
     */
    public function beforeValidate()
    {
        if ($this->treatment_id == "") {
            $this->treatment_id = "0";
        }

        return parent::beforeValidate();
    }

    /**
     * Look up linked treatment or create if necessary
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->treatment_id == "0") {

            $treatment_id_save = CHttpRequest::getPost('treatment_id_save');

            if ($treatment_id_save == '') {
                $treatment_id_save = CHttpRequest::getPost('treatment_id_lookup');
            }

            if ($treatment_id_save != "") {

                //check if already existed
                $treatment_id = Treatment::model()->find('name=:name', array(':name' => $treatment_id_save))->id;
                if ($treatment_id > 0) {
                    $this->treatment_id = $treatment_id;
                    return parent::beforeSave();
                }

                $treatment = new Treatment;

                $treatment->name = $treatment_id_save;
                $treatment->save();
                $this->treatment_id = $treatment->id;

                return parent::beforeSave();
            }
            $this->addError("id", "Couldn't save treatment.");
            return false;
        }
        return parent::beforeSave();
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id .
                '" rel="/administration/practiceTooltip?id=' . $this->practice_id . '" '
                . 'title="' . $this->practice . '">' . $this->practice . '</a>');
    }

}
