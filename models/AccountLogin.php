<?php

Yii::import('application.modules.core_models.models._base.BaseAccountLogin');

class AccountLogin extends BaseAccountLogin
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * This function will fire an email to the account owner if:
     * - The account has logged in before
     * - The browser doesn't have our special cookie
     * - The IP address hasn't been seen for this account
     * @param int $accountId
     * @param string $remoteIp
     * @param string $sessionId
     * @param string $cookieDDCSeen
     * @param string $unixTime
     * @return boolean
     * @throws Exception
     */
    public static function checkLogin(
        $accountId = 0,
        $remoteIp = '',
        $sessionId = '',
        $cookieDDCSeen = '',
        $unixTime = ''
    )
    {

        // do we have the data we need to check / create / update login information?
        if (empty($accountId) || empty($remoteIp) || empty($sessionId)) {
            // no, just leave
            return false;
        }

        // have we seen this account at all before? (check because we don't want to send notifications for the first
        // login ever)
        $seenAccount = AccountLogin::model()->exists('account_id = :accountId', array(':accountId' => $accountId));

        // Check #1: have we seen this account in this IP address?
        $seenAccountAtIP = AccountLogin::model()->find(
            'account_id = :accountId AND ip_address = :ipAddress',
            array(':accountId' => $accountId, ':ipAddress' => $remoteIp)
        );

        // hopefully we did
        if (!empty($seenAccountAtIP)) {
            // we did
            // save the new date
            $seenAccountAtIP->date_updated = date('Y-m-d H:i:s', $unixTime);
            // save the new session
            $seenAccountAtIP->session_id = $sessionId;
            $seenAccountAtIP->save();
            // leave
            return true;
        }

        // Check #1 failed: we haven't seen this account in this IP address

        // Check #2: have we seen this account with the cookie (past) session ID in any other IP address?
        $seenAccountSession = AccountLogin::model()->find(
            'account_id = :accountId AND session_id = :sessionId',
            array(':accountId' => $accountId, ':sessionId' => $cookieDDCSeen)
        );

        // create new login record
        $accountLogin = new AccountLogin();
        $accountLogin->account_id = $accountId;
        $accountLogin->date_added = date('Y-m-d H:i:s', $unixTime);
        $accountLogin->ip_address = $remoteIp;
        $accountLogin->session_id = $sessionId;
        $accountLogin->save();

        // hopefully we have
        if (!empty($seenAccountSession)) {
            // we have, so no need to notify - just return since we have the new record now

            return true;
        } else {
            // we haven't seen this account in this IP address nor with this session ID
            // we'll need to notify the account - but only if it's not its first login
            if ($seenAccount) {
                // we've seen this account, notify
                MailComponent::notifyLogin($accountId, $remoteIp, date('m/d/Y g:ia', $unixTime));
            }
            return true;
        }
    }

}
