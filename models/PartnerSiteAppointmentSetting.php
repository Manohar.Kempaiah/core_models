<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteAppointmentSetting');

class PartnerSiteAppointmentSetting extends BasePartnerSiteAppointmentSetting
{

    /*
     * Model
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /*
     * Get Partner Site with directory
     */
    public static function getPartnerSiteDirectory()
    {
        return Yii::app()->db->createCommand()
                ->select('partner_site.id AS id, partner_site.display_name as name')
                ->from('partner_site')
                ->join('directory_setting', 'partner_site.id =  directory_setting.partner_site_id')
                ->where('partner_site.active = 1 AND partner_site.is_directory = 1 AND draft = 0')
                ->order('partner_site.display_name')
                ->queryAll();
    }

    /**
     * Clean up data before validating
     * @return boolean
     */
    public function beforeValidate()
    {

        // make sure we clean up null values
        if (is_null($this->added_by_account_id)) {
            $this->added_by_account_id = Yii::app()->user->id;
        }
        return parent::beforeValidate();
    }

    /**
     * Check PartnerSiteAppointmentSetting
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->isNewRecord) {

            $exists = PartnerSiteAppointmentSetting::model()->exists(
                'partner_site_id=:partner_site_id',
                array(':partner_site_id' => $this->partner_site_id)
            );
            if ($exists) {
                $this->addError(
                    'PartnerSiteAppointmentSetting_partner_site_id',
                    'This Partner Site has already been added'
                );
                return false;
            }
        } else {

            $this->updated_by_account_id = Yii::app()->user->id;

            $exists = PartnerSiteAppointmentSetting::model()->exists(
                'partner_site_id=:partner_site_id AND id <> :id',
                array(':partner_site_id' => $this->partner_site_id, ':id' => $this->id)
            );
            if ($exists) {
                $this->addError(
                    'PartnerSiteAppointmentSetting_partner_site_id',
                    'This Partner Site has already been added'
                );
                return false;
            }
        }

        return parent::beforeSave();
    }
}
