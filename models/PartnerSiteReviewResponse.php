<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteReviewResponse');

class PartnerSiteReviewResponse extends BasePartnerSiteReviewResponse
{

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Before save partner_site_review_response
     * @return boolean
     */
    public function beforeSave()
    {

        if (empty($this->date_added)) {
            $this->date_added = date("Y-m-d H:i:s");
        }
        $this->date_updated = date("Y-m-d H:i:s");

        return parent::beforeSave();
    }

    /**
     * Create new record in partner_site_review_response
     * @param array $responseRequest
     * @return boolean
     */
    public static function create($responseRequest)
    {

        $partnerSiteId = !empty($responseRequest['partner_site_id']) ? (int)$responseRequest['partner_site_id'] : 0;

        // Exists this partner_site_id ?
        $partnerSite =  PartnerSite::model()->findByPk($partnerSiteId);
        if (empty($partnerSite)) {
            // Nop
            $results['success'] = false;
            $results['http_response_code'] = 200;
            $results['error'] = 'INVALID_PARTNER_SITE_ID';
            $results['data'] = $responseRequest;

        } else {

            $partnerSiteReviewResponse = new PartnerSiteReviewResponse();
            $partnerSiteReviewResponse->date_added = date("Y-m-d H:i:s");
            $partnerSiteReviewResponse->partner_site_id = $partnerSiteId;
            $partnerSiteReviewResponse->partner_site_review_id = $responseRequest['partner_site_review_id'];
        $partnerSiteReviewResponse->internal_review_id = $responseRequest['internal_review_id'];
            $partnerSiteReviewResponse->review_response_date_added = $responseRequest['review_response_date_added'];
            $partnerSiteReviewResponse->review_response = $responseRequest['review_response'];
            $partnerSiteReviewResponse->external_username = $responseRequest['external_username'];
            $partnerSiteReviewResponse->external_password = $responseRequest['external_password'];

            // The review Response was saved ok?
            if ($partnerSiteReviewResponse->save()) {
                // Yes
                $results['success'] = true;
                $results['http_response_code'] = 200;
                $results['error'] = '';
                $results['data'] = '';

                PartnerSiteReviewResponseLog::add($partnerSiteReviewResponse);
            } else {
                // Nop
                $results['success'] = false;
                $results['http_response_code'] = 200;
                $results['error'] = 'SAVING_ERROR';
                $results['data'][] = $partnerSiteReviewResponse->getErrors();
            }

        }

        return $results;
    }

    /**
     * Update record in partner_site_review_response
     * @param array $data
     *  {
     *      "id": 1,
     *      "status": "F",
     *      "json_details": "Invalid login data"
     *  }
     * @return boolean
     */
    public static function saveUpdate($data)
    {

        $psrrId = !empty($data['id']) ? (int)$data['id'] : 0;

        // Exists this ReviewResponse?
        $psrr = PartnerSiteReviewResponse::model()->findByPk($psrrId);
        if (!empty($psrr)) {
            // Yes
            $psrr->status = !empty($data['status']) ? $data['status'] : $psrr->status;
            $psrr->json_details = !empty($data['json_details']) ? json_encode($data['json_details']) : '';

            if ($psrr->save()) {
                $results['success'] = true;
                $results['http_response_code'] = 200;
                $results['error'] = '';
                $results['data'] = '';

                PartnerSiteReviewResponseLog::add($psrr);
            } else {
                $results['success'] = false;
                $results['http_response_code'] = 200;
                $results['error'] = 'SAVING_ERROR';
                $results['data'] = $psrr->getErrors();
            }

        } else {
            // Not exists
            $results['success'] = false;
            $results['http_response_code'] = 200;
            $results['error'] = 'PARTNER_SITE_REVIEW_RESPONSE_NOT_EXISTS';
            $results['data'] = $data;
        }

        return $results;
    }

    /**
     * List data in partner_site_review_response
     * @post json $data
     *      $data['partner_site_id']
     *      $data['status']
     *      $data['date_field'] -> added || updated
     *      $data['date_from']
     *      $data['date_to']
     * @return array
     */
    public static function getList($data)
    {

        $condPartnerSite = '';
        if (!empty($data['partner_site_id'])) {
            // Add filter by partner_site_id
            $condPartnerSite = sprintf(" AND partner_site_id = %d ", $data['partner_site_id']);
        }

        $condStatus = '';
        if (!empty($data['status'])) {
            // Add filter by status
            $condStatus = sprintf(" AND `status` = '%s' ", $data['status']);
        }

        $condDateRange = '';
        if (!empty($data['date_from']) && !empty($data['date_to'])) {
            // Add filter by date_range
            $dateField = ($data['date_field'] == 'added') ? 'date_added' : 'date_updated';
            $condDateRange = sprintf(
                " AND ('%s' >= '%s' AND '%s' <= '%s') ",
                $dateField,
                $data['date_from'],
                $dateField,
                $data['date_to']
            );
        }

        $sql = sprintf(
            "SELECT partner_site_review_response.id,
            partner_site_review_response.partner_site_id,
            partner_site_review_response.partner_site_review_id,
            partner_site_review_response.internal_review_id,
            partner_site_review_response.review_response,
            partner_site_review_response.review_response_date_added,
            partner_site_review_response.status,
            partner_site_review_response.external_username,
            partner_site_review_response.external_password,
            partner_site_review_response.json_details,
            partner_site_review_response.date_added,
            partner_site_review_response.date_updated,
            partner_site.display_name
            FROM partner_site_review_response
            LEFT JOIN partner_site
                ON partner_site_review_response.partner_site_id = partner_site.id
            WHERE 1=1 %s %s %s
            ORDER BY date_updated DESC
            LIMIT 500;",
            $condPartnerSite,
            $condStatus,
            $condDateRange
        );
        return Yii::app()->db->createCommand($sql)->queryAll();

    }

}
