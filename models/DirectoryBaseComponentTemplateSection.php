<?php

Yii::import('application.modules.core_models.models._base.BaseDirectoryBaseComponentTemplateSection');

class DirectoryBaseComponentTemplateSection extends BaseDirectoryBaseComponentTemplateSection
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
