<?php

Yii::import('application.modules.core_models.models._base.BaseTwilioIgnoreNumber');

class TwilioIgnoreNumber extends BaseTwilioIgnoreNumber
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Normalize phone number
     * @return boolean
     */
    public function beforeSave()
    {

        if (!StringComponent::isValidPhoneNumber($this->number)) {
            $this->addError('number', 'Invalid number: ' . $this->number);
            return false;
        }

        // normalize phone number
        $unformattedNumber = $this->number;
        $this->number = StringComponent::normalizePhoneNumber($unformattedNumber);
        if (!$this->number) {
            $this->addError('number', 'Could not normalize phone number: ' . $unformattedNumber);
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * @todo document
     * @return array
     */
    public static function getTypes()
    {
        return array(
            'B' => 'Business partner',
            'E' => 'Doctor.com employee',
            'O' => 'Other'
        );
    }

    //@todo why here and not in Base?
    public static function label($n = 1)
    {
        return Yii::t('app', 'Twilio Ignore Number|Twilio Ignore Numbers', $n);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'number';
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'type' => Yii::t('app', 'Type'),
            'description' => Yii::t('app', 'Description'),
        );
    }

}
