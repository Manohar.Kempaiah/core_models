<?php
Yii::import('application.modules.core_models.models._base.BaseCondition');

class Condition extends BaseCondition
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Show condition suggestions in autocomplete
     * Used by AutocompleteController.php
     *
     * @param string $q
     * @param int $limit
     * @return array
     */
    public static function autocomplete($q, $limit = 10)
    {
        $connection = Yii::app()->db;

        $addCommonSpecialties = "";

        $rq = $q; // don't mess with $q, we'll use it later

        $rq = trim($rq);
        $req = '/' . $rq . '/';
        if (((stristr($rq, 'doctor') !== false) || (stristr($rq, 'dr') !== false) ||
             (stristr($rq, 'md') !== false) || (stristr($rq, 'physician') !== false) ||
             (stristr($rq, 'primary care') !== false)) || (preg_match($req, 'doctor') || preg_match($req, 'dr') ||
             preg_match($req, 'md') || preg_match($req, 'physician') || preg_match($req, 'primary care'))) {
            $addCommonSpecialties = " OR sub_specialty.id IN (54, 62, 68, 198) ";
        }

        $sql = "SELECT 0 AS all_in, CONCAT('C-', condition.id) AS id, `condition`.name AS name,
            0 AS bottom
            FROM `condition`
            WHERE (name LIKE '" . $q . "%')
            UNION ALL
            SELECT DISTINCT 0 AS all_in,
            CONCAT('S-', sub_specialty.id) AS id, CONCAT(specialty.name, ' - ', sub_specialty.name) AS name,
            IF(sub_specialty.name = 'General', 0, 1) AS bottom
            FROM sub_specialty
            INNER JOIN specialty
            ON sub_specialty.specialty_id = specialty.id
            WHERE (sub_specialty.name LIKE '%" . $q . "%'
            OR specialty.name LIKE '%" . $q . "%'
            OR specialty.name_plural = '" . $q . "')
            " . $addCommonSpecialties . "
            ORDER BY bottom, name
            LIMIT " . $limit . ";";
        $array = $connection->createCommand($sql)->queryAll();

        $sqlSpec = sprintf(
            "SELECT 1 AS all_in, specialty.name_plural, specialty.name, 0 AS id
            FROM specialty
            WHERE (specialty.name LIKE '%s%%' OR specialty.name LIKE '%s%%')
            AND (specialty.name_plural IS NOT NULL AND specialty.name_plural != '')
            ORDER BY specialty.name_plural
            LIMIT 1;",
            $q,
            $q
        );
        $arrSpec = $connection->createCommand($sqlSpec)->queryAll();
        if (sizeof($arrSpec) > 0) {
            $array = array_merge($arrSpec, $array);
        }

        return $array;
    }

    /**
     * Finds a condition by name
     *
     * @param string $condition
     * @return array
     */
    public static function findByName($condition = '')
    {
        $condition = trim($condition);

        $q = new CDbCriteria();
        $q->addSearchCondition('name', $condition);
        $assocArray = Condition::model()->cache(7 * Yii::app()->params['cache_long'])->findAll($q);

        $array = array();
        foreach ($assocArray as $v) {
            array_push($array, $v['id']);
        }
        if (is_array($array) && sizeof($array) > 0) {
            return $array;
        }
        return false;
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from condition if associated records exist in specialty_condition table
        $hasLinkedRecords = SpecialtyCondition::model()->exists(
            'condition_id=:condition_id',
            array(':condition_id' => $this->id));
        if ($hasLinkedRecords) {
            $this->addError('id', 'Condition cannot be deleted because there are specialties linked to it.');
            return false;
        }

        return parent::beforeDelete();
    }
}
