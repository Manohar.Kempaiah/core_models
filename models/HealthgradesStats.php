<?php
Yii::import('application.modules.core_models.models._base.BaseHealthgradesStats');

class HealthgradesStats extends BaseHealthgradesStats
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get the latest stats from Healthgrades
     * @return array
     */
    public static function getStats()
    {
        $sql = sprintf("SELECT organization.id AS organization_id, organization.organization_name, organization.status
            AS organization_status,
            healthgrades_stats.npi_id, client_code, provider_code,
            provider_name, profile_completion_rate, fields_to_complete,
            profile_visits, oar_submits, video_plays,
            survey_count, avg_survey_score, total_calls,
            answered_calls, avg_call_duration, profile_promos,
            profile_visits_mtd, oar_submits_mtd, video_plays_mtd,
            survey_count_mtd, avg_survey_score_mtd, total_calls_mtd,
            answered_calls_mtd, avg_call_duration_mtd, image_url,
            practice_site_clicks, practice_site_clicks_mtd, profile_promos_mtd,
            data, healthgrades_stats.date_added
            FROM healthgrades_stats
            LEFT JOIN provider
            ON healthgrades_stats.npi_id = provider.npi_id
            LEFT JOIN account_provider
            ON account_provider.provider_id = provider.id
            LEFT JOIN organization_account
            ON organization_account.account_id = account_provider.account_id
            AND `connection` = 'O'
            LEFT JOIN organization
            ON organization.id = organization_account.organization_id
            GROUP BY healthgrades_stats.npi_id;");
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }
}
