<?php

Yii::import('application.modules.core_models.models._base.BaseTreatment');

class Treatment extends BaseTreatment
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name', 'required'),
            array('maximum_cost, minimum_cost, provider_level_search', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 250),
            array('code', 'length', 'max' => 5),
            array(
                'code, maximum_cost, minimum_cost, provider_level_search',
                'default',
                'setOnEmpty' => true,
                'value' => null
            ),
            array('id, name, code, maximum_cost, minimum_cost, provider_level_search', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'name';
    }

    /**
     * Returns the treatment name
     *
     * @deprecated No longer used by internal code and not recommended.
     *
     * @return string
     */
    public function getFullTreatment()
    {
        $treatment = Treatment::model()->findByPk($this->id);
        return addslashes($treatment->name);
    }

    /**
     * Capitalize first letter in Procedures & Treatments
     * @return boolean
     */
    public function beforeSave()
    {
        $this->name = ucfirst($this->name);

        return parent::beforeSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from treatment if associated records exist in provider_treatment table
        $hasAssociatedRecords = ProviderTreatment::model()->exists(
            'treatment_id=:treatment_id',
            array(':treatment_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError("id", "there are providers that perform it.");
            return false;
        }

        // delete records from specialty_treatment before delete this treatment id
        $arrSpecialtyTreatment = SpecialtyTreatment::model()->findAll(
            "treatment_id = :treatment_id",
            array(":treatment_id" => $this->id)
        );
        foreach ($arrSpecialtyTreatment as $specialtyTreatment) {
            if (!$specialtyTreatment->delete()) {
                $this->addError("id", "Could not delete related specialties.");
                return false;
            }
        }

        // delete records from specialty_treatment_performed before delete this treatment id
        $arrProviderTreatmentPerformed = ProviderTreatmentPerformed::model()->findAll(
            "treatment_id = :treatment_id",
            array(":treatment_id" => $this->id)
        );
        foreach ($arrProviderTreatmentPerformed as $providerTreatmentPerformed) {
            if (!$providerTreatmentPerformed->delete()) {
                $this->addError("id", "Could not delete linked providers.");
                return false;
            }
        }

        // delete records from practice_treatment before delete this treatment id
        $arrPracticeTreatment = PracticeTreatment::model()->findAll(
            'treatment_id=:treatment_id',
            array(':treatment_id' => $this->id)
        );
        foreach ($arrPracticeTreatment as $practiceTreatment) {
            if (!$practiceTreatment->delete()) {
                $this->addError("id", "Could not delete linked practices.");
                return false;
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Find treatment IDs by matching against name and code
     * @param string $q
     * @return array
     */
    public static function findByName($q)
    {

        if (!empty($q)) {
            $q = trim(addslashes($q));
            $sql = sprintf(
                "SELECT treatment.id as id
                FROM treatment
                INNER JOIN treatment_friendly_treatment
                    ON treatment_friendly_treatment.treatment_id = treatment.id
                INNER JOIN treatment_friendly
                    ON treatment_friendly_treatment.treatment_friendly_id = treatment_friendly.id
                WHERE treatment.name LIKE '%s%%'
                    OR treatment.code LIKE '%s%%'
                    OR treatment_friendly.name LIKE '%s%%'
                ORDER BY treatment.id;",
                $q,
                $q,
                $q
            );
            $command = Yii::app()->db->cache(7 * Yii::app()->params['cache_long'])->createCommand($sql);
            $assocArray = $command->queryAll();

            $array = array();

            foreach ($assocArray as $v) {
                array_push($array, $v['id']);
            }

            if (sizeof($array) > 0 && is_array($array)) {
                return $array;
            }

            $sql = sprintf(
                "SELECT id
                FROM treatment
                WHERE name LIKE '%%%s%%'
                OR code LIKE '%%%s%%'
                ORDER BY name = '%%%s%%' DESC;",
                $q,
                $q,
                $q
            );
            $command = Yii::app()->db->cache(7 * Yii::app()->params['cache_long'])->createCommand($sql);
            $assocArray = $command->queryAll();
            $array = array();
            foreach ($assocArray as $k => $v) {
                array_push($array, $v['id']);
            }
            if (is_array($array) && sizeof($array) > 0) {
                return $array;
            }
        }
        return false;
    }

    /**
     * Autocomplete for treatments based on name and code
     * @param string $q
     * @param int $limit
     * @return array
     */
    public function autocomplete($q, $limit = 20)
    {

        $q = trim(addslashes($q));

        $sql = sprintf(
            "SELECT DISTINCT
            CONCAT('T-', treatment.id) AS id, IF(treatment_friendly.name != '', treatment_friendly.name, treatment.name)
            AS name, treatment.code AS code, treatment.provider_level_search
            FROM treatment
            LEFT JOIN treatment_friendly_treatment
                ON treatment_friendly_treatment.treatment_id = treatment.id
            LEFT JOIN treatment_friendly
                ON treatment_friendly_treatment.treatment_friendly_id = treatment_friendly.id
            WHERE treatment.name LIKE '%%%s%%'
                OR treatment.code LIKE '%%%s%%'
                OR treatment_friendly.name LIKE '%%%s%%'
            ORDER BY name
            LIMIT %s;",
            $q,
            $q,
            $q,
            $limit
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('maximum_cost', $this->maximum_cost);
        $criteria->compare('minimum_cost', $this->minimum_cost);
        $criteria->compare('provider_level_search', $this->provider_level_search);
        $criteria->compare('common_treatment', $this->common_treatment);

        return new CActiveDataProvider(
            $this,
            array('criteria' => $criteria, 'pagination' => array('pageSize' => 100))
        );
    }
}
