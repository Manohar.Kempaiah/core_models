<?php

Yii::import('application.modules.core_models.models._base.BaseCpnEhrAttributeType');

class CpnEhrAttributeType extends BaseCpnEhrAttributeType
{
    const BASE_QUEUE_FOLDER = 22;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get Attribute Type
     *
     * @param str $ehrId
     * @param str $cpnEhrInstallationId
     * @param int $socketsFlag
     * @param int $folderFlag
     * @param int $sftpFlag
     * @param int $customFlag
     * @return array
     */
    public function getAttributeType($ehrId, $cpnEhrInstallationId, $socketsFlag, $folderFlag, $sftpFlag, $customFlag)
    {
        // Query
        if (isset($cpnEhrInstallationId)) {
            $sql = sprintf(
                "SELECT
                    cpn_ehr_attribute_type.id,
                    cpn_ehr_attribute_type.name,
                    cpn_ehr_attribute_type.description,
                    cpn_ehr_attribute_type.encrypted_flag,
                    cpn_ehr_attribute_type.default_value,
                    cpn_ehr_installation_attribute.attribute_value
                FROM cpn_ehr_attribute_type
                LEFT JOIN cpn_ehr_installation_attribute
                    ON ( cpn_ehr_installation_attribute.cpn_ehr_attribute_type_id = cpn_ehr_attribute_type.id
                    AND cpn_ehr_installation_attribute.cpn_ehr_installation_id = %s)
                WHERE cpn_ehr_attribute_type.enabled_flag=1
                    AND cpn_ehr_attribute_type.del_flag=0
                    AND (
                        (
                            cpn_ehr_attribute_type.cpn_ehr_id IS NULL AND cpn_ehr_attribute_type.sockets_flag = %s
                            AND cpn_ehr_attribute_type.folder_flag = %s AND cpn_ehr_attribute_type.sftp_flag = %s
                            AND cpn_ehr_attribute_type.custom_flag = %s
                        )
                        OR cpn_ehr_attribute_type.cpn_ehr_id = '%s' OR cpn_ehr_attribute_type.id = 22
                    )
                ORDER BY cpn_ehr_attribute_type.id desc;",
                $cpnEhrInstallationId,
                $socketsFlag,
                $folderFlag,
                $sftpFlag,
                $customFlag,
                $ehrId
            );
        } else {
            $sql = sprintf(
                "SELECT
                cpn_ehr_attribute_type.id,
                cpn_ehr_attribute_type.name,
                cpn_ehr_attribute_type.encrypted_flag,
                cpn_ehr_attribute_type.default_value,
                cpn_ehr_attribute_type.description,
                '' AS attribute_value
                FROM cpn_ehr_attribute_type
                WHERE enabled_flag=1
                AND del_flag=0
                AND (
                    (cpn_ehr_id IS NULL AND sockets_flag = %s AND folder_flag = %s AND sftp_flag = %s
                        AND custom_flag = %s)
                    OR
                    cpn_ehr_id = '%s' OR cpn_ehr_attribute_type.id = 22
                )
                ORDER BY cpn_ehr_id, cpn_ehr_attribute_type.name;",
                $socketsFlag,
                $folderFlag,
                $sftpFlag,
                $customFlag,
                $ehrId
            );
        }

        // Return all the result
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Find attribute using AttributeTypeGUID
     * @param $guid
     * @return mixed
     */
    public function findByGUID($guid)
    {
        return CpnEhrAttributeType::model()->cache(Yii::app()->params['cache_long'])->find(
            'guid = :guid',
            array(':guid' => $guid)
        );
    }
}
