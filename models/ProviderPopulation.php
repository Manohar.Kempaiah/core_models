<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPopulation');

class ProviderPopulation extends BaseProviderPopulation
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id .
            '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">' .
            $this->provider . '</a>');
    }

    /**
     * Get Provider's Population(s)
     * @param int $providerId
     * @return array
     */
    public static function getProviderPopulation($providerId = 0, $cache = true)
    {

        $result = array();

        if ($providerId > 0) {
            $sql = sprintf(
                "SELECT pp.id, pp.provider_id, pp.population_id, p.name
                FROM provider_population AS pp
                LEFT JOIN population AS p ON p.id = pp.population_id
                WHERE pp.provider_id = %d",
                $providerId
            );

            $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

            $result = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
        }
        return $result;
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {

        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }

        $results['success'] = true;
        $results['error'] = "";

        // population
        $providerPopulations = ProviderPopulation::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $providerId)
        );
        foreach ($providerPopulations as $providerPopulation) {
            if (!$providerPopulation->delete()) {

                $auditSection = 'Provider Population provider_id #' . $providerId;
                AuditLog::create('D', $auditSection, 'provider_population', null, false);

                $results['success'] = false;
                $results['error'] = "ERROR_DELETING_PROVIDER_POPULATION";
                $results['data'] = $providerPopulation->getErrors();
                return $results;
            }
        }

        if ($postData) {
            foreach ($postData as $population) {
                $backendAction = !empty($population['backend_action']) ? $population['backend_action'] : '';
                if ($backendAction == 'delete') {
                    continue;
                }

                $exists = ProviderPopulation::model()->exists(
                    'provider_id=:provider_id AND population_id=:population_id',
                    array(':provider_id' => $providerId, ':population_id' => $population['population_id'])
                );
                if (!$exists) {
                    $pp = new ProviderPopulation();
                    $pp->provider_id = $providerId;
                    $pp->population_id = $population['population_id'];

                    if ($pp->save()) {
                        $auditSection = 'Provider Population provider_id #' . $providerId;
                        AuditLog::create('C', $auditSection, 'provider_population', null, false);

                        $results['success'] = true;
                        $results['error'] = "";
                    } else {
                        $results['success'] = false;
                        $results['error'] = "ERROR_SAVING_PROVIDER_POPULATION";
                        $results['data'] = $pp->getErrors();
                        return $results;
                    }
                }
            }
        }

        return $results;
    }
}
