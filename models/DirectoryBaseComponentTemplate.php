<?php

Yii::import('application.modules.core_models.models._base.BaseDirectoryBaseComponentTemplate');

class DirectoryBaseComponentTemplate extends BaseDirectoryBaseComponentTemplate
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Return all records by the component_section_id.
     *
     * @param int $componentSectionId
     * @return array
     */
    public static function getBySection($componentSectionId)
    {
        $sql = "SELECT directory_base_component_template.*
            FROM directory_base_component_template
            JOIN directory_base_component_template_section
                ON directory_base_component_template_section.base_component_template_id = directory_base_component_template.id
            WHERE directory_base_component_template_section.component_section_id = :component_section_id
            ORDER BY directory_base_component_template.name";

        return Yii::app()->db->createCommand($sql)->queryAll(true, [':component_section_id' => $componentSectionId]);
    }

}
