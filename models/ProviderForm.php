<?php

Yii::import('application.modules.core_models.models._base.BaseProviderForm');

class ProviderForm extends BaseProviderForm
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerToolftip" href="/administration/providerTooltip?id=' . $this->provider_id .
                '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider .
                '">' . $this->provider . '</a>');
    }

    /**
     * Get file path for internal forms, used in the admin gridview
     * @return string
     */
    public function getFilePath()
    {
        return '<a href="/images/uploads/forms/' . $this->file_path . '" target="_blank" title="Click to download">' .
                $this->file_path . '</a>';
    }

    /**
     * Get file path for external links, used in the admin gridview
     * @return string
     */
    public function getFileLink()
    {
        return '<a href="' . $this->file_link . '" target="_blank">' . $this->file_link . '</a>';
    }

    /**
     * Get provider forms
     * @param int $providerId
     * @return array
     */
    public static function getProviderForms($providerId = 0, $cache = true)
    {
        $sql = "SELECT *
            FROM provider_form
            WHERE provider_id = :providerId
            ORDER BY id ASC;";

        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

        $arrForms = Yii::app()->db->cache($cacheTime)->createCommand($sql)
            ->bindParam(":providerId", $providerId, PDO::PARAM_STR)->queryAll();

        $absolutePathToFile = 'https://' . Yii::app()->params['doctor_url_for_module']
                . Yii::app()->params['upload_forms'];
        $relativePathToFile = './' . Yii::app()->params['upload_forms'];

        $pdiId = null;
        if (isset(Yii::app()->params->pdi_id)) {
            $pdiId = Yii::app()->params->pdi_id;
        }
        $newArrForms = array();
        if (!empty($arrForms)) {
            // check that all files exist or have file_link
            foreach ($arrForms as $value) {

                $file = $relativePathToFile . $value['file_path'];
                $value['href'] = '';
                if (!empty($value['file_link'])) {
                    // external link
                    $value['href'] = $value['file_link'];
                } elseif (!empty($value['file_path']) && ($pdiId != 1 || file_exists($file))) {
                    // if file exists
                    $value['href'] = $absolutePathToFile . $value['file_path'];
                }
                if (!empty($value['href'])) {
                    $newArrForms[] = $value;
                }
            }
        }

        return $newArrForms;
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_provider
            INNER JOIN provider_form
            ON account_provider.provider_id = provider_form.provider_id
            WHERE provider_form.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $providerId
     * @param int $accountId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0, $accountId = 0)
    {
        $results['success'] = true;
        $results['error'] = "";

        if (empty($postData)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        if ($providerId == 0 || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "PROVIDER_ID_ZERO";
            return $results;
        }

        // validate access
        ApiComponent::checkPermissions('Provider', $accountId, $providerId);

        foreach ($postData as $data) {
            $model = null;
            $backendAction = !empty($data['backend_action']) ? trim($data['backend_action']) : '';
            $data['id'] = isset($data['id']) ? $data['id'] : 0;

            if ($data['id'] > 0) {
                $model = ProviderForm::model()->findByPk($data['id']);
            }

            if ($backendAction == 'delete') {
                // delete the row
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_FORM";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }
                    $auditSection = 'Provider Form provider_id #' . $providerId;
                    AuditLog::create('D', $auditSection, 'provider_form', null, false);
                }
                continue;
            }
            $fileLink = !empty($data['file_link']) ? $data['file_link'] : '';
            $auditAction = 'U';
            if (empty($model)) {
                $model = new ProviderForm;
                $auditAction = 'C';
                $model->provider_id = $providerId;
                if (empty($fileLink)) {
                    // upload the new photo
                    $resultUpload = File::upload($model, "file_path", true);
                    if (!$resultUpload['success']) {
                        $results['error'] = $resultUpload['error'];
                        $results['data'] = $resultUpload['data'];
                        return $results;
                    }
                }
            }

            if (!StringComponent::validateUrl($fileLink)) {
                $results['success'] = false;
                $results['error'] = "ERROR_INVALID_URL";
                $results['data'] = $data;
                return $results;
            }

            $model->file_link = $fileLink;
            $model->description = $data['description'];

            if ($model->save()) {
                $auditSection = 'Provider Form provider_id #' . $providerId;
                AuditLog::create($auditAction, $auditSection, 'provider_form', null, false);
                $results['success'] = true;
                $results['error'] = "";
                if ($auditAction == 'C') {
                    $data['id'] = $model->id;
                    unset($data['backend_action']);
                    $data['href'] = $fileLink;
                    if (!empty($model->file_path)) {
                        $absolutePathToFile = 'https://' . Yii::app()->params['doctor_url_for_module']
                            . Yii::app()->params['upload_forms'];
                        $data['href'] = $absolutePathToFile . $model->file_path;
                    }
                    $results['data'][] = $data;

                }
            } else {
                $results['success'] = false;
                $results['error'] = "ERROR_SAVING_PROVIDER_FORM";
                $results['data'] = $model->getErrors();
                return $results;
            }
        }

        return $results;
    }

}
