<?php

Yii::import('application.modules.core_models.models._base.BaseListingSite');

class ListingSite extends BaseListingSite
{

    public $uploadPath;

    /**
     * Initializes the model with the correct basePath according to the active module.
     * @internal Avoid using __construct. Use init() instead.
     *  if you still need to use it, call parent::__construct($scenario). Read yii documentation.
     */
    public function init()
    {
        parent::init();
        $this->uploadPath = $this->basePath . Yii::app()->params['upload_listing_sites'];
    }

    /**
     * Declares the validation rules
     * @return array
     */
    public function rules()
    {
        return array(
            array('name, active, is_doctor_alias', 'required'),
            array('active, priority, is_doctor_alias', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 80),
            array('priority', 'length', 'max' => 4),
            array('logo_path', 'ext.CustomValidators.CheckDirectory', 'uploadPath' => $this->uploadPath),
            array('date_added', 'safe'),
            array('priority, logo_path, date_added', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, active, priority, is_doctor_alias, logo_path, date_added', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Model
     * @param type $className
     * @return type
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from ListingSite table if associated records exist in ListingType table
        $has_associated_records = ListingType::model()->exists(
            'listing_site_id=:listing_site_id',
            array(':listing_site_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError('id', 'Listing site cannot be deleted because there are listing types linked to it.');
            return false;
        }
        return parent::beforeDelete();
    }

    /**
     * Before Save
     * @return boolean
     */
    public function beforeSave()
    {

        // is this an existing record?
        if ($this->id > 0) {
            // yes, check for duplicate names
            $duplicateName = ListingSite::model()->exists(
                'name = :name AND id != :thisId',
                array(':name' => $this->name, ':thisId' => $this->id)
            );
        } else {
            // no, check for duplicate names
            $duplicateName = ListingSite::model()->exists('name = :name', array(':name' => $this->name));
        }

        // was the name a duplicate?
        if ($duplicateName) {
            // yes, exit
            $this->addError('name', 'Duplicate name');
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * get scanned site codes
     * @param string $codeType -> T:Practice | V:Provider
     * @return array
     */
    public static function getScannedSiteCodes($codeType = 'T')
    {
        $sql = sprintf(
            "SELECT DISTINCT lt.code, s.name, lt.per, lt.name AS listing_type_name
            FROM listing_site AS s
            INNER JOIN listing_type AS lt ON s.id = lt.listing_site_id
            LEFT JOIN scan_template_listing_type AS stlt ON stlt.listing_type_id = lt.id
            LEFT JOIN scan_template_listing_type_version AS stltv ON stltv.listing_type_id = lt.id
            WHERE
                (stltv.listing_type_id IS NOT NULL OR stlt.listing_type_id IS NOT NULL)
                AND s.active = 1
                AND lt.active = 1
                AND lt.url_type = 'P'
                AND lt.per='%s'
            ORDER BY s.name ASC;",
            $codeType
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get Listing Manager Headers
     * @param string $codeType -> T:Practice | V:Provider
     * @return array
     */
    public static function getListingManagerHeaders($codeType = 'T')
    {
        $sql = sprintf(
            "SELECT DISTINCT
                listing_type.id, listing_site.name, listing_type.code
            FROM
                listing_site
            INNER JOIN listing_type
                ON listing_type.listing_site_id= listing_site.id
            WHERE
                listing_type.per = '%s'
                AND listing_type.url_type = 'P'
                AND listing_type.active = '1'
                AND listing_site.active = '1'
                AND listing_type.editable = '1'
                AND listing_type.managed_lm = '1'
            ORDER BY isnull(listing_type.lm_order), listing_type.lm_order, listing_type.name;",
            $codeType
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }
}
