<?php

Yii::import('application.modules.core_models.models._base.BaseProductElement');

class ProductElement extends BaseProductElement
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
