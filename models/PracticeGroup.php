<?php

Yii::import('application.modules.core_models.models._base.BasePracticeGroup');

class PracticeGroup extends BasePracticeGroup
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
