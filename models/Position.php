<?php

Yii::import('application.modules.core_models.models._base.BasePosition');

class Position extends BasePosition
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from position if associated records exist in account table
        $has_associated_records = Account::model()->exists(
            'position_id=:position_id',
            array(
                ':position_id' => $this->id
            )
        );
        if ($has_associated_records) {
            $this->addError("id", "Position can't be deleted because there are accounts linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    public function autocomplete($q, $limit)
    {

        $q = trim($q);

        $sql = "SELECT name
            FROM position
            WHERE name LIKE '" . $q . "%'
            ORDER BY name
            LIMIT " . $limit . ";";

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        return $command->queryAll();
    }

}
