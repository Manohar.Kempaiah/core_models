<?php

Yii::import('application.modules.core_models.models._base.BaseFeature');

class Feature extends BaseFeature
{
    const REVIEWHUB = 2;
    const PROFILE_SYNC = 10;
    const GMB = 17;
    const REP_INSIGHTS = 19;
    const REVIEW_REQUEST = 20;
    const TELEMEDICINE = 23;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'per' => 'Per',
            'name' => 'Name',
            'description' => 'Description',
            'date_added' => 'Date Added',
            'date_updated' => 'Date Updated',
        );
    }

    /**
     * Verifies that the set of feature ids provided is valid.
     *
     * @param array|string $featuresParam
     *
     * @return boolean
     */
    public function checkValidFeatures($featuresParam = array())
    {
        // Build the features ids array from input parameter
        if (is_array($featuresParam)) {
            // Array of ids received
            $featureIds = $featuresParam;
        } elseif (is_string($featuresParam)) {
            // Comma separated list of ids received
            $featureIds = explode(',', $featuresParam);
        } else {
            // Unknown parameter type provided
            return false;
        }

        if ($featureIds) {
            foreach ($featureIds as $featureId) {
                $feature = Feature::model()->findByPk($featureId);
                if (!$feature) {
                    // If only one feature doesn't exist, then it's invalid
                    return false;
                }
            }

            return true;
        }

        return false;
    }
}
