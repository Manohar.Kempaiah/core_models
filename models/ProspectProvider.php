<?php

Yii::import('application.modules.core_models.models._base.BaseProspectProvider');

class ProspectProvider extends BaseProspectProvider
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
