<?php

Yii::import('application.modules.core_models.models._base.BaseInsuranceMarketplaceLog');

class InsuranceMarketplaceLog extends BaseInsuranceMarketplaceLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Synchronize provider_insurance and provider_practice_insurance data
     * @param int $companyId
     */
    public static function syncProviderInsuranceData($companyId)
    {
        $sql = sprintf(
            "SELECT id, marketplace_id
            FROM insurance
            WHERE company_id = '%d'",
            $companyId
        );
        $all = Yii::app()->db->createCommand($sql)->queryAll();
        $allIds = array_column($all, "id");

        $sql = sprintf(
            "SELECT id, marketplace_id
            FROM insurance
            WHERE company_id = '%d'",
            $companyId
        );
        $allCore = Yii::app()->dbCore->createCommand($sql)->queryAll();
        $allCoreMap = array();
        foreach ($allCore as $c) {
            $allCoreMap[$c['marketplace_id']] = $c['id'];
        }

        $end = false;
        $start = 0;
        $size = 1000;
        while (!$end) {
            $sql = sprintf(
                "SELECT provider_id
                FROM insurance_marketplace_log
                WHERE company_id = '%d'
                LIMIT %d, %d",
                $companyId,
                $start,
                $size
            );
            $result = Yii::app()->db->createCommand($sql)->queryColumn();

            $start += $size;

            if (count($result) < $size) {
                $end = true;
            }

            foreach ($result as $providerId) {
                $sql = sprintf(
                    "SELECT insurance_id, marketplace_id
                    FROM provider_insurance
                    INNER JOIN insurance ON provider_insurance.insurance_id = insurance.id
                    WHERE provider_insurance.provider_id = '%d' AND provider_insurance.company_id = '%d'",
                    $providerId,
                    $companyId
                );
                $enabled = Yii::app()->db->createCommand($sql)->queryAll();
                $enabledIds = array_column($enabled, 'insurance_id');
                $enabledMarketplaceIds = array_column($enabled, 'marketplace_id');

                if (count($allIds) == count($enabledIds) && !array_diff($allIds, $enabledIds)) {
                    // if all the insurances are enabled in the insurance db
                    $sql = sprintf(
                        "SELECT insurance_id FROM provider_insurance
                        WHERE provider_id = '%d' AND company_id = '%d'",
                        $providerId,
                        $companyId
                    );
                    $currentPlans = Yii::app()->dbCore->createCommand($sql)->queryAll();

                    if (count($currentPlans) > 1) {
                        // if specific plans were enabled, delete all the entries
                        $sql = sprintf(
                            "DELETE FROM provider_insurance
                            WHERE provider_id = '%d' AND company_id = '%d';",
                            $providerId,
                            $companyId
                        );
                        Yii::app()->dbCore->createCommand($sql)->execute();
                    }

                    if (!count($currentPlans) == 1 && $currentPlans[0]['insurance_id'] == 0) {
                        // insert a signle record for all the plans if it not exists
                        $sql = sprintf(
                            "INSERT INTO provider_insurance
                            (provider_id, company_id, insurance_id, date_added)
                            VALUES ('%d', '%d', 0, '%s')",
                            $providerId,
                            $companyId,
                            date('Y-m-d H:i:s')
                        );
                        Yii::app()->dbCore->createCommand($sql)->execute();
                    }
                } else {
                    $enabledCoreIds = array();
                    foreach ($enabledMarketplaceIds as $marketplaceId) {
                        $sql = sprintf(
                            "INSERT IGNORE INTO provider_insurance
                            (provider_id, company_id, insurance_id, date_added)
                            VALUES ('%d', '%d', %d, '%s')",
                            $providerId,
                            $companyId,
                            $allCoreMap[$marketplaceId],
                            date('Y-m-d H:i:s')
                        );
                        Yii::app()->dbCore->createCommand($sql)->execute();
                        $enabledCoreIds[] = $allCoreMap[$marketplaceId];
                    }
                    // if not all the insurances are enabled in the insurance db
                    $sql = sprintf(
                        "DELETE FROM provider_insurance
                        WHERE provider_id = '%d' AND company_id = '%d' AND insurance_id NOT IN (%s)",
                        $providerId,
                        $companyId,
                        implode(',', $enabledCoreIds)
                    );
                    Yii::app()->dbCore->createCommand($sql)->execute();
                }

                // check if the provider has a paid account
                $sql = sprintf(
                    "SELECT COUNT(account_provider.provider_id)
                    FROM account_provider
                    INNER JOIN organization_account ON organization_account.account_id = account_provider.account_id
                    INNER JOIN organization ON organization.id = organization_account.organization_id
                    WHERE organization.`status`='A'
                    AND organization_account.`connection`='O'
                    AND account_provider.provider_id = '%d'
                    LIMIT 1;",
                    $providerId
                );
                $cnt = Yii::app()->dbCore->createCommand($sql)->queryScalar();
                if ($cnt > 0) {
                    continue;
                }

                // check if the provider haven't been associated with an account yet
                $exists = AccountProvider::model()->exists("provider_id = :provider_id", array(":provider_id" =>
                    $providerId));
                if (!$exists) {
                    continue;
                }

                // insert provider_practice_insurance for providers with free accounts
                $sql = sprintf(
                    "SELECT practice_id FROM provider_practice
                    WHERE provider_id = '%d'",
                    $providerId
                );
                $arrPractices = Yii::app()->dbCore->createCommand($sql)->queryColumn();

                foreach ($arrPractices as $practiceId) {
                    $enabledCoreIds = array();
                    foreach ($enabledMarketplaceIds as $marketplaceId) {
                        $sql = sprintf(
                            "INSERT IGNORE INTO provider_practice_insurance
                            (provider_id, practice_id, company_id, insurance_id, date_added)
                            VALUES
                            ('%d', '%d', '%d', %d, '%s')",
                            $providerId,
                            $practiceId,
                            $companyId,
                            $allCoreMap[$marketplaceId],
                            date('Y-m-d H:i:s')
                        );
                        Yii::app()->dbCore->createCommand($sql)->execute();
                        $enabledCoreIds[] = $allCoreMap[$marketplaceId];
                    }
                    // if not all the insurances are enabled in the insurance db
                    $sql = sprintf(
                        "DELETE FROM provider_practice_insurance
                        WHERE provider_id = '%d'
                        AND practice_id = '%d'
                        AND company_id = '%d'
                        AND insurance_id NOT IN (%s)",
                        $providerId,
                        $practiceId,
                        $companyId,
                        implode(',', $enabledCoreIds)
                    );
                    Yii::app()->dbCore->createCommand($sql)->execute();
                }
            }
        }
    }

}
