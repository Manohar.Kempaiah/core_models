<?php

Yii::import('application.modules.core_models.models._base.BaseDma');

class Dma extends BaseDma
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Before Save
     * @return boolean
     */
    public function beforeSave()
    {

        // is this an existing record?
        if ($this->id > 0) {
            // yes, check if the code already exists
            $duplicateCode = Dma::model()->exists(
                'code = :code AND id != :id',
                array(
                    ':code' => $this->code,
                    ':id' => $this->id
                )
            );
        } else {
            // no, check if the code already exists
            $duplicateCode = Dma::model()->exists(
                'code = :code',
                array(
                    ':code' => $this->code
                )
            );
        }

        // was it a duplicate?
        if ($duplicateCode) {
            // yes, exit
            $this->addError('code', 'Duplicate code');
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from dma table if associated records exist in location table
        $has_associated_records = Location::model()->exists('dma_id=:dma_id', array(':dma_id' => $this->id));
        if ($has_associated_records) {
            $this->addError('id', 'This dma cannot be deleted because it has locations in it.');
            return false;
        }
        return parent::beforeDelete();
    }

}
