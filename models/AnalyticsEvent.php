<?php

Yii::import('application.modules.core_models.models._base.BaseAnalyticsEvent');

class AnalyticsEvent extends BaseAnalyticsEvent
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'twilio_number_id' => 'Twilio Number',
            'source_ip' => 'Source Ip',
            'screen_width' => 'Screen Width',
            'screen_height' => 'Screen Height',
            'viewport_width' => 'Viewport Width',
            'viewport_height' => 'Viewport Height',
            'date_added' => 'Date Added',
            'user_id' => 'User ID',
            'campaign_source' => 'Campaign Source',
            'campaign_medium' => 'Campaign Medium',
            'campaign_keyword' => 'Campaign Keyword',
            'campaign_content' => 'Campaign Content',
            'campaign_name' => 'Campaign Name',
            'campaign_id' => 'Campaign ID',
            'url' => 'Url',
            'user_agent' => 'User Agent',
            'gclid' => 'Gclid',
            'dclid' => 'Dclid',
            'referer' => 'Referer',
        );
    }

    /**
     * Set date automatically if needed
     */
    public function beforeSave()
    {
        if (empty($this->date_added) || $this->date_added == '0000-00-00 00:00:00') {
            $this->date_added = date('Y-m-d H:i:s');
        }
        return parent::beforeSave();
    }

}
