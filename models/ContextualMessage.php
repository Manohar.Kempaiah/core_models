<?php

Yii::import('application.modules.core_models.models._base.BaseContextualMessage');

class ContextualMessage extends BaseContextualMessage
{

    protected $contextMessages = null;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'loggedin_status';
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('id, service_type_id', 'required'),
            array('id, employer_id, network_id, sub_specialty_id, practice_group_id, practice_id, treatment_id, '
                . 'quality_area_id, provider_id, warning_level, service_type_id', 'numerical', 'integerOnly' => true),
            array('loggedin_status', 'length', 'max' => 1),
            array('contextual_message', 'safe'),
            array(
                'employer_id, network_id, sub_specialty_id, practice_group_id, practice_id, treatment_id, ' .
                    'quality_area_id, provider_id, loggedin_status, warning_level, contextual_message',
                'default',
                'setOnEmpty' => true,
                'value' => null
            ),
            array(
                'id, employer_id, network_id, sub_specialty_id, practice_group_id, practice_id, treatment_id, ' .
                    'quality_area_id, provider_id, loggedin_status, warning_level, service_type_id, contextual_message',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('employer_id', $this->employer_id);
        $criteria->compare('network_id', $this->network_id);
        $criteria->compare('sub_specialty_id', $this->sub_specialty_id);
        $criteria->compare('practice_group_id', $this->practice_group_id);
        $criteria->compare('practice_id', $this->practice_id);
        $criteria->compare('treatment_id', $this->treatment_id);
        $criteria->compare('quality_area_id', $this->quality_area_id);
        $criteria->compare('provider_id', $this->provider_id);
        $criteria->compare('loggedin_status', $this->loggedin_status, true);
        $criteria->compare('warning_level', $this->warning_level);
        $criteria->compare('service_type_id', $this->service_type_id);
        $criteria->compare('contextual_message', $this->contextual_message, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Looks for contextual messages in the given context
     * @param array $context Associative array containing the search parameters
     */
    public function evaluateContext($context)
    {

        $subSpecialty = isset($context['specialty']) ? $context['specialty'] : false;
        $practice = isset($context['practice']) ? $context['practice'] : false;
        $treatment = isset($context['treatment']) ? $context['treatment'] : false;
        $qualityArea = isset($context['quality_area']) ? $context['quality_area'] : false;
        $provider = isset($context['provider']) ? $context['provider'] : false;
        $practiceType = isset($context['s_practice_type']) ? $context['s_practice_type'] : false;

        $employerId = isset($context['employer']) ? (int) $context['employer'] : 0;
        $networkId = 0;
        $subSpecialtyId = 0;
        $practiceId = 0;
        $practiceGroupId = 0;
        $treatmentId = 0;
        $qualityAreaId = 0;
        $providerId = 0;
        $practiceTypeId = 0;

        if ($employerId > 0) {
            $employer = Employer::model()->cache(Yii::app()->params['cache_long'])->
                find("employer_id = :employer_id", array(":employer_id" => $employerId));
            if ($employer) {
                $networkId = $employer->network_id;
            }
        }



        if ($subSpecialty != "") {
            $arrSpecialty = SubSpecialty::model()->cache(Yii::app()->params['cache_long'])->
                findAll("name = :sub_specialty_name", array(":sub_specialty_name" => $subSpecialty));
            if ($arrSpecialty && count($arrSpecialty) == 1) {
                $subSpecialtyId = $arrSpecialty[0]["id"];
            }
        }

        if ($practice != "") {
            $arrPractices = Practice::model()->cache(Yii::app()->params['cache_long'])->
                findAll("name = :name", array(":name" => $practice));
            if ($arrPractices && count($arrPractices) == 1) {
                $practiceId = $arrPractices[0]["id"];
                $practiceGroupId = $arrPractices[0]["practice_group_id"];
            }
        }

        if ($practiceId == 0 && !empty($context['practice_id'])) {
            if (strpos($context['practice_id'], "-") === false) {
                $practiceId = $context['practice_id'];
            } else {
                $practiceIdArr = explode("-", $context['practice_id']);
                $practiceId = $practiceIdArr[1];
            }
        }

        if ($treatment != "") {
            $qualityAreaId = (int) QualityArea::findQualityPathAreaByName($treatment);

            if (!$qualityAreaId) {
                $arrTreatment = Treatment::findByName($treatment);

                if ($arrTreatment && count($arrTreatment) == 1) {
                    $treatmentId = $arrTreatment[0];
                }
            }
        }

        if ($qualityArea != "") {
            $qualityAreaId = QualityArea::findQualityPathAreaByName($qualityArea);

            if (!$qualityAreaId) {
                $qualityAreaId = QualityArea::findByName($qualityArea);
            }
        }

        if ($provider != "") {
            $sql = sprintf(
                "SELECT DISTINCT id
                FROM provider
                WHERE first_last LIKE \"%%%s%%\"
                ORDER BY first_last
                LIMIT 2;",
                str_replace('"', '\"', $provider)
            );

            $arrProviders = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
            if (count($arrProviders) == 1) {
                $providerId = $arrProviders[0]["id"];
            }
        }

        if ($practiceType != "") {
            $practiceTypeId = (int) $practiceType;
        }

        $whereNetwork = "";

        if (php_sapi_name() != 'cli') {
            $loggedIn = (!Yii::app()->user->isGuest ? "1" : "0");
        } else {
            $loggedIn = 0;
        }

        if ($qualityAreaId == '') {
            $qualityAreaId = 0;
        }

        $sql = sprintf(
            "SELECT id, warning_level, contextual_message
            FROM contextual_message
            WHERE warning_level <> '4' AND (
                IF (employer_id = 0 OR employer_id IS NULL, %d, employer_id) = %d AND
                IF (network_id = 0 OR network_id IS NULL, %d, network_id) = %d AND
                IF (loggedin_status = '' OR loggedin_status IS NULL, '%s', loggedin_status) = '%s' AND
                IF (sub_specialty_id = 0 OR sub_specialty_id IS NULL, %d, sub_specialty_id) = %d AND
                IF (practice_id = 0 OR practice_id IS NULL, %d, practice_id) = %d AND
                IF (practice_group_id = 0 OR practice_group_id IS NULL, %d, practice_group_id) = %d AND
                IF (treatment_id = 0 OR treatment_id IS NULL, %d, treatment_id) = %d AND
                IF (quality_area_id = 0 OR quality_area_id IS NULL, %d, quality_area_id) = %d AND
                %s
                IF (provider_id = 0 OR provider_id IS NULL, %d, provider_id) = %d AND
                IF (service_type_id = 0 OR service_type_id IS NULL, %d, service_type_id) = %d
            )
            ORDER BY warning_level ASC;",
            $employerId,
            $employerId,
            $networkId,
            $networkId,
            $loggedIn,
            $loggedIn,
            $subSpecialtyId,
            $subSpecialtyId,
            $practiceId,
            $practiceId,
            $practiceGroupId,
            $practiceGroupId,
            $treatmentId,
            $treatmentId,
            $qualityAreaId,
            $qualityAreaId,
            $whereNetwork,
            $providerId,
            $providerId,
            $practiceTypeId,
            $practiceTypeId
        );
        $contextMessages = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();

        $sql = sprintf(
            "SELECT id, warning_level, contextual_message
            FROM contextual_message
            WHERE warning_level = '4' AND (
                IF (employer_id = 0 OR employer_id IS NULL, %d, employer_id) = %d AND
                IF (network_id = 0 OR network_id IS NULL, %d, network_id) = %d AND
                IF (loggedin_status = '' OR loggedin_status IS NULL, '%s', loggedin_status) = '%s' AND
                IF (sub_specialty_id = 0 OR sub_specialty_id IS NULL, %d, sub_specialty_id) = %d AND
                IF (practice_id = 0 OR practice_id IS NULL, %d, practice_id) = %d AND
                IF (practice_group_id = 0 OR practice_group_id IS NULL, %d, practice_group_id) = %d AND
                IF (treatment_id = 0 OR treatment_id IS NULL, %d, treatment_id) = %d AND
                IF (quality_area_id = 0 OR quality_area_id IS NULL, %d, quality_area_id) = %d AND
                %s
                IF (provider_id = 0 OR provider_id IS NULL, %d, provider_id) = %d AND
                IF (service_type_id = 0 OR service_type_id IS NULL, %d, service_type_id) = %d
            )
            ORDER BY RAND()
            LIMIT 1;",
            $employerId,
            $employerId,
            $networkId,
            $networkId,
            $loggedIn,
            $loggedIn,
            $subSpecialtyId,
            $subSpecialtyId,
            $practiceId,
            $practiceId,
            $practiceGroupId,
            $practiceGroupId,
            $treatmentId,
            $treatmentId,
            $qualityAreaId,
            $qualityAreaId,
            $whereNetwork,
            $providerId,
            $providerId,
            $practiceTypeId,
            $practiceTypeId
        );
        $contextMessagesLevel4 = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->
            queryAll();
        $this->contextMessages = array_merge($contextMessages, $contextMessagesLevel4);

        $session = Yii::app()->session;
        if (isset($session['contextMessagesWarning']) && count($this->contextMessages) > 0) {
            foreach ($this->contextMessages as $key => $message) {
                if (isset($session['contextMessagesWarning'][$message['id']])) {
                    $this->contextMessages[$key]['warning_level'] = '3';
                }
            }
        }
    }

    /**
     * Get the context messages for a given level:
     *   0: All levels
     *   1: Exclusions
     *   2: Alerts
     *   3: Notifications
     * @param int $level
     * @return array
     */
    public function getContextMessages($level = 0)
    {
        $result = array();
        if ($level == 0) {
            $result = $this->contextMessages;
            $session = Yii::app()->session;
            $contextMessagesWarning = $session['contextMessagesWarning'];
            if (count($this->contextMessages) > 0) {
                foreach ($this->contextMessages as $message) {
                    if ($message['warning_level'] == '2') {
                        $contextMessagesWarning[$message['id']] = 1;
                    }
                }
            }
            $session['contextMessagesWarning'] = $contextMessagesWarning;
        } else {
            if (count($this->contextMessages) > 0) {
                foreach ($this->contextMessages as $message) {
                    if ($message['warning_level'] == $level) {
                        $result[] = $message;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get the text of a contextual message
     * @param int $id Id of the contextual message
     * @return string
     */
    public function getMessage($id)
    {
        $message = "";
        $contextMessage = ContextualMessage::model()->findByPk($id);
        if ($contextMessage) {
            $message = $contextMessage->contextual_message;
        }
        return $message;
    }

}
