<?php

Yii::import('application.modules.core_models.models._base.BaseScanProvider');

class ScanProvider extends BaseScanProvider
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * get the las scan providers by providerId
     * @param int $providerId
     */
    public static function getLatest($providerId = 0)
    {
        $sql = sprintf(
            "SELECT
                id, scan_practice_id, rating, listing_accuracy, missing_listings, profile_completion_score,
                completed_scans, cnt_errors, data_elements, reviews, healthgrades_primary_specialty,
                reviews_grade, missing_listing_names, profile_completion_missing_data, date_added
                FROM scan_provider
                WHERE provider_id = %d
                ORDER BY date_added DESC LIMIT 1",
            $providerId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();
    }

}

