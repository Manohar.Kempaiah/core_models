<?php

/**
 * This is the model class for table "partner_session".
 *
 * The followings are the available columns in table 'partner_session':
 * @property integer $id
 * @property string $ip
 * @property string $expire_date
 * @property string $user_agent
 * @property string $email
 * @property integer $site_id
 */
class PartnerSession extends MyActiveRecord
{

    /**
     * Returns the static model of the specified AR class.
     * @return PartnerSession the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'partner_session';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('ip, expire_date, user_agent, email, site_id', 'required'),
            array('site_id', 'numerical', 'integerOnly' => true),
            array('ip', 'length', 'max' => 30),
            array('user_agent', 'length', 'max' => 255),
            array('email', 'length', 'max' => 45),
            // The following rule is used by search().
            // Please remove those attributes that should not be searched.
            array('id, ip, expire_date, user_agent, email, site_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'ip' => 'Ip',
            'expire_date' => 'Expire Date',
            'user_agent' => 'User Agent',
            'email' => 'Email',
            'site_id' => 'Site',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
     */
    public function search()
    {
        // Warning: Please modify the following code to remove attributes that
        // should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('ip', $this->ip, true);
        $criteria->compare('expire_date', $this->expire_date, true);
        $criteria->compare('user_agent', $this->user_agent, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('site_id', $this->site_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
