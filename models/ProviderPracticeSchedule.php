<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeSchedule');
Yii::import('application.modules.providers.protected.components.CapybaraCache');

class ProviderPracticeSchedule extends BaseProviderPracticeSchedule
{

    public $provider;
    public $practice;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Validate that not exists duplicated record for each provider/practice/day
     * @return bool
     */
    public function beforeSave()
    {
        $providerPracticeId = !empty($this->provider_practice_id) ? $this->provider_practice_id : 0;
        $dayNumber = $this->day_number;

        if ($providerPracticeId == 0) {
            $this->addError('day_number', 'Provider Practice is not associated');
            return false;
        }

        if ($this->isNewRecord) {
            $exists = ProviderPracticeSchedule::model()->exists(
                "provider_practice_id =:providerPracticeId AND day_number=:dayNumber",
                array(':providerPracticeId' => $providerPracticeId, ':dayNumber' => $dayNumber)
            );
        } else {
            $exists = ProviderPracticeSchedule::model()->exists(
                "provider_practice_id =:providerPracticeId AND day_number=:dayNumber AND id!=:id",
                array(':providerPracticeId' => $providerPracticeId, ':dayNumber' => $dayNumber, ':id' => $this->id)
            );
        }

        if ($exists) {
            $this->addError('day_number', 'There is already a record for that day');
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * save OfficeHours
     * @param int $providerPracticeId
     * @param arr $scheduleDays
     *     "1": {
     *       "name": "Mon",
     *       "hoursFrom": "10",
     *       "minsFrom": "00",
     *       "amFrom": "selected",
     *       "pmFrom": "",
     *       "hoursTo": "4",
     *       "minsTo": "00",
     *       "amTo": "",
     *       "pmTo": "selected"
     *     },
     */
    public static function saveOfficeHours($providerPracticeId = 0, $scheduleDays = '')
    {
        if ($providerPracticeId > 0) {

            $prevSchedule = array();

            // old schedule
            $arrProviderPracticeSh = ProviderPracticeSchedule::model()->findAll(
                'provider_practice_id = :provider_practice_id',
                array(':provider_practice_id' => $providerPracticeId)
            );
            if (!empty($arrProviderPracticeSh)) {
                foreach ($arrProviderPracticeSh as $providerPracticeSh) {
                    // set previous schedule
                    $prevSchedule[$providerPracticeSh->day_number] = 1;
                }
            }

            if (!empty($scheduleDays)) {
                // Save the new Office Hours
                foreach ($scheduleDays as $dayNumber => $value) {
                    if (!empty($value) && !empty($value['selected']) && Common::isTrue($value['selected'])) {
                        $formattedHours = TimeComponent::formatHoursRange($value);

                        // update
                        if (isset($prevSchedule[$dayNumber])) {

                            $arrProviderPracticeSh = ProviderPracticeSchedule::model()->find(
                                'provider_practice_id = :provider_practice_id AND day_number = :day_number',
                                array(':provider_practice_id' => $providerPracticeId, ':day_number' => $dayNumber)
                            );
                            $arrProviderPracticeSh->hours = $formattedHours;
                            if (!$arrProviderPracticeSh->save()) {
                                return [
                                    'success' => false,
                                    'error' => $arrProviderPracticeSh->getErrors()
                                ];
                            }
                            // previous schedule processed
                            $prevSchedule[$dayNumber] = 2;
                        } else {

                            // insert
                            $providerPracticeSchedule = new ProviderPracticeSchedule;
                            $providerPracticeSchedule->day_number = $dayNumber;
                            $providerPracticeSchedule->hours = $formattedHours;
                            $providerPracticeSchedule->provider_practice_id = $providerPracticeId;
                            if (!$providerPracticeSchedule->save()) {
                                return [
                                    'success' => false,
                                    'error' => $providerPracticeSchedule->getErrors()
                                ];
                            }
                        }
                    }
                }
            }

            // previous chedule
            foreach ($prevSchedule as $key => $value) {

                // without process
                if ($value == 1) {

                    $arrProviderPracticeSh = ProviderPracticeSchedule::model()->find(
                        'provider_practice_id = :provider_practice_id AND day_number = :day_number',
                        array(':provider_practice_id' => $providerPracticeId, ':day_number' => $key)
                    );
                    // delete
                    if (!$arrProviderPracticeSh->delete()) {
                        return [
                            'success' => false,
                            'error' => $arrProviderPracticeSh->getErrors()
                        ];
                    }
                }
            }

            // Look for provider practice data
            $providerPractice = ProviderPractice::model()->findByPK($providerPracticeId);

            // Update capybara cache
            if ($providerPractice) {
                CapybaraCache::loadSchedule($providerPractice->practice_id, $providerPractice->provider_id);
            }
            return $scheduleDays;
        }
    }

    /**
     * save OfficeHours
     * @param int $providerPracticeId
     * @param arr $scheduleDays
     *     "1": {
     *       "name": "Mon",
     *       "hoursFrom": "10",
     *       "minsFrom": "00",
     *       "amFrom": "selected",
     *       "pmFrom": "",
     *       "hoursTo": "4",
     *       "minsTo": "00",
     *       "amTo": "",
     *       "pmTo": "selected"
     *     },
     *
     * {"provider_id":1331495,"data":{"provider_telemedicine":{"accept_new_patients":0,"enabled":1,"states":[{"id":108,
     * "state_id":10,"selected":1,"state_name":"California"},{"id":109,"state_id":8,"selected":1,"state_name":"New York"
     * },{"id":110,"state_id":22,"selected":1,"state_name":"North Carolina"}],"schedule":{"mon":{"hours":{"start":
     * "09:00 am","end":"05:30 pm"},"selected":true},"tue":{"hours":{"start":"09:00 am","end":"05:30 pm"},"selected":
     * true},"wed":{"hours":{"start":"09:00 am","end":"05:30 pm"},"selected":true},"thur":{"hours":{"start":"09:00 am",
     * "end":"05:30 pm"},"selected":true},"thu":{"hours":{"start":"9:00 am","end":"5:00 am"},"selected":true}}}}}
     */
    public static function saveTelemedicineOfficeHours($providerPracticeId = 0, $scheduleDays = '')
    {
        if ($providerPracticeId > 0) {

            $prevSchedule = array();

            // old schedule
            $arrProviderPracticeSh = ProviderPracticeSchedule::model()->findAll(
                'provider_practice_id = :provider_practice_id',
                array(':provider_practice_id' => $providerPracticeId)
            );
            if (!empty($arrProviderPracticeSh)) {
                foreach ($arrProviderPracticeSh as $providerPracticeSh) {
                    // set previous schedule
                    $prevSchedule[$providerPracticeSh->day_number] = 1;
                }
            }

            if (!empty($scheduleDays)) {

                // Save the new Office Hours
                foreach ($scheduleDays as $dayNumber => $value) {
                    if (!empty($value) && !empty($value['selected']) && Common::isTrue($value['selected'])) {

                        /*
                        format is:
                        ["from"]=>
                        string(7) "09:00 am"
                        ["to"]=>
                        string(7) "05:00 pm"
                        */
                        $splitFrom = explode(' ', $value['hours']['start']);
                        if (empty($splitFrom[0]) || empty($splitFrom[1])) {
                            // invalid data
                            continue;
                        }
                        $hoursFrom = explode(':', $splitFrom[0]);
                        if (empty($hoursFrom[0]) || empty($hoursFrom[1])) {
                            // invalid data
                            continue;
                        }
                        $hours['hoursFrom'] = $hoursFrom[0];
                        $hours['minsFrom'] = $hoursFrom[1];
                        $hours['amFrom'] = (strtoupper($splitFrom[1]) == 'AM' ? 'AM' : null);

                        $splitTo = explode(' ', $value['hours']['end']);
                        if (empty($splitTo[0]) || empty($splitTo[1])) {
                            // invalid data
                            continue;
                        }
                        $hoursTo = explode(':', $splitTo[0]);
                        if (empty($hoursTo[0]) || empty($hoursTo[1])) {
                            // invalid data
                            continue;
                        }
                        $hours['amTo'] = (strtoupper($splitTo[1]) == 'AM' ? 'AM' : null);
                        $hours['hoursTo'] = $hoursTo[0];
                        $hours['minsTo'] = $hoursTo[1];

                        $formattedHours = TimeComponent::formatHoursRange($hours);

                        // convert eg Mon to 1
                        $dayNumber = TimeComponent::weekdayTodayNumber($dayNumber);

                        // update
                        if (isset($prevSchedule[$dayNumber])) {

                            $arrProviderPracticeSh = ProviderPracticeSchedule::model()->find(
                                'provider_practice_id = :provider_practice_id AND day_number = :day_number',
                                array(':provider_practice_id' => $providerPracticeId, ':day_number' => $dayNumber)
                            );
                            $arrProviderPracticeSh->hours = $formattedHours;
                            if (!$arrProviderPracticeSh->save()) {
                                return [
                                    'success' => false,
                                    'error' => $arrProviderPracticeSh->getErrors()
                                ];
                            }
                            // previous schedule processed
                            $prevSchedule[$dayNumber] = 2;
                        } else {

                            // insert
                            $providerPracticeSchedule = new ProviderPracticeSchedule;
                            $providerPracticeSchedule->day_number = $dayNumber;
                            $providerPracticeSchedule->hours = $formattedHours;
                            $providerPracticeSchedule->provider_practice_id = $providerPracticeId;
                            if (!$providerPracticeSchedule->save()) {
                                return [
                                    'success' => false,
                                    'error' => $providerPracticeSchedule->getErrors()
                                ];
                            }
                        }
                    }
                }
            }

            // previous schedule
            foreach ($prevSchedule as $key => $value) {

                // without process
                if ($value == 1) {

                    $arrProviderPracticeSh = ProviderPracticeSchedule::model()->find(
                        'provider_practice_id = :provider_practice_id AND day_number = :day_number',
                        array(':provider_practice_id' => $providerPracticeId, ':day_number' => $key)
                    );
                    // delete
                    if (!$arrProviderPracticeSh->delete()) {
                        return [
                            'success' => false,
                            'error' => $arrProviderPracticeSh->getErrors()
                        ];
                    }
                }
            }

            // Look for provider practice data
            $providerPractice = ProviderPractice::model()->findByPK($providerPracticeId);

            // Update capybara cache
            if ($providerPractice) {
                CapybaraCache::loadSchedule($providerPractice->practice_id, $providerPractice->provider_id);
            }
            return $scheduleDays;
        }
    }

    /**
     * Get the schedule for a provider_practice id
     * @param id $providerPracticeId
     * @return array
     */
    public static function getSchedule($providerPracticeId = 0)
    {
        $sql = sprintf(
            "SELECT * FROM provider_practice_schedule
            WHERE provider_practice_id = %d
            ORDER BY day_number ASC",
            $providerPracticeId
        );
        $days = Yii::app()->db->createCommand($sql)->query();

        $hours = array();
        $dayNames = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        foreach ($days as $d) {
            $strFrom = date('Y-m-d') . ' ' . substr($d['hours'], 0, 8);
            $from = date_create_from_format('Y-m-d H:i:s', $strFrom);

            $strTo = date('Y-m-d') . ' ' . substr($d['hours'], 11, 8);
            $to = date_create_from_format('Y-m-d H:i:s', $strTo);

            if ($from && $to) {
                $hours[$d['day_number']] = array(
                    'name' => $dayNames[$d['day_number']],
                    'from' => date_format($from, 'g:i A'),
                    'to' => date_format($to, 'g:i A'),
                    'hours_from' => date_format($from, 'g'),
                    'mins_from' => date_format($from, 'i'),
                    'ampm_from' => date_format($from, 'A'),
                    'hours_to' => date_format($to, 'g'),
                    'mins_to' => date_format($to, 'i'),
                    'ampm_to' => date_format($to, 'A')
                );
            }
        }
        return $hours;
    }

    /**
     * Save provider practices hour
     * @param string $postData
     * @param int $providerId
     * @param int $accountId
     * @return array $results
     */
    public static function saveData($providerId = 0, $postData = '', $accountId = 0)
    {

        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";
        $results['data'] = "";

        if (empty($postData)) {
            return $results;
        }
        $arrRankOrder = !empty($postData['rankOrder']) ? $postData['rankOrder'] : array();
        $practicesSelected = !empty($postData['practice']) ? $postData['practice'] : array();
        $arrContactInformation = !empty($postData['practices']) ? $postData['practices'] : array();

        if (count($practicesSelected) > 0) {

            foreach ($practicesSelected as $practiceId => $isSelected) {
                $auditAction = '';
                if ($isSelected == 1) {

                    $newData = $arrContactInformation['contact'][$practiceId];
                    $newData['rank_order'] = isset($arrRankOrder[$practiceId]) ? $arrRankOrder[$practiceId] : null;
                    $providerPracticeId = ProviderPractice::setProviderPractice($providerId, $practiceId, $newData);

                    $scheduleDays = array();
                    if (isset($arrContactInformation['days'][$practiceId])) {
                        $scheduleDays = $arrContactInformation['days'][$practiceId];
                    }
                    $auditAction = 'U';
                    if (!empty($scheduleDays)) {
                        foreach ($scheduleDays as $day => $value) {
                            $scheduleDays[$day]['selected'] = !empty($value) ? 1 : 0;
                        }
                    }
                    ProviderPracticeSchedule::saveOfficeHours($providerPracticeId, $scheduleDays);
                } else {
                    $scheduleDays = [];
                    // delete ProviderPractice
                    $arrProviderPractice = ProviderPractice::model()->findAll(
                        'provider_id = :provider_id AND practice_id = :practice_id',
                        array(':provider_id' => $providerId, ':practice_id' => $practiceId)
                    );
                    $results['cpnValidationError'] = false;
                    if (!empty($arrProviderPractice)) {
                        foreach ($arrProviderPractice as $providerPractice) {
                            if (!$providerPractice->delete()) {
                                $results['success'] = false;
                                $results['error'] = "ERROR_DELETING_PROVIDER_PRACTICE";
                                $errorDetected = $providerPractice->getErrors();
                                $results['data'] = $errorDetected;
                                $results['provider_practice_id'] = $providerPractice->id;
                                if (isset($errorDetected['cpn_validation'])) {
                                    $results['cpnValidationError'] = true;
                                }
                                return $results;
                            }
                        }
                    }
                    $auditAction = 'D';
                }
                if (!empty($auditAction)) {
                    $auditSection = 'ProviderPracticeSchedule #' . $providerId . ' (practice_id:' . $practiceId . ')';
                    $notes = '';
                    if (!empty($scheduleDays)) {
                        $notes = json_encode($scheduleDays);
                    }
                    AuditLog::create($auditAction, $auditSection, 'provider_practice_schedule', $notes, false);
                }
            }
            // Sync EMR data
            SqsComponent::sendMessage('importAccountEmr', $accountId);
        }

        // Update all rank order, this is for practices unlinked too
        ProviderPractice::updateRankOrder($providerId);

        return $results;
    }

    /**
     * Save Provider Practice Schedule by PracticeId
     * @param string $postData
     * @param int $practiceId
     * @param int $accountId
     * @return array $results
     */
    public static function saveDataByPractice($postData = '', $practiceId = 0, $accountId = 0)
    {
        if ($practiceId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";
        $results['data'] = "";

        if (empty($postData)) {
            return $results;
        }

        $providersSelected = !empty($postData['providers']) ? $postData['providers'] : array();
        $arrProviderPracticeSchedule = !empty($postData['provider_practice_schedule'])
            ? $postData['provider_practice_schedule']
            : array();

        foreach ($providersSelected as $providerId => $isSelected) {

            if ($isSelected == 1) {

                $providerPracticeId = ProviderPractice::setProviderPractice($providerId, $practiceId);

                $scheduleDays = array();
                if (!empty($arrProviderPracticeSchedule[$providerId])) {
                    $scheduleDays = $arrProviderPracticeSchedule[$providerId];
                }

                // Checked by default from Mon to Fri
                foreach ($scheduleDays as $day => $value) {
                    $scheduleDays[$day]['selected'] = 0;
                    if (!empty($value) && ($day >= 0 && $day <= 6)) {
                        $scheduleDays[$day]['selected'] = 1;
                    }
                }

                ProviderPracticeSchedule::saveOfficeHours($providerPracticeId, $scheduleDays);
            } else {

                // delete ProviderPractice
                $arrProviderPractice = ProviderPractice::model()->findAll(
                    'provider_id = :provider_id AND practice_id = :practice_id',
                    array(':provider_id' => $providerId, ':practice_id' => $practiceId)
                );
                $results['cpnValidationError'] = false;
                if (!empty($arrProviderPractice)) {
                    foreach ($arrProviderPractice as $providerPractice) {
                        if (!$providerPractice->delete()) {
                            $results['success'] = false;
                            $results['error'] = "ERROR_DELETING_PROVIDER_PRACTICE";
                            $errorDetected = $providerPractice->getErrors();
                            $results['data'] = $errorDetected;
                            $results['provider_practice_id'] = $providerPractice->id;
                            if (isset($errorDetected['cpn_validation'])) {
                                $results['cpnValidationError'] = true;
                            }
                            return $results;
                        }
                    }
                }
            }
        }
        // Sync EMR data
        SqsComponent::sendMessage('importAccountEmr', $accountId);

        return $results;
    }

    /**
     * PADM > Save the schedule for Telemedicine Booking (internal)
     *
     * @param array $providerTelemedicine
     * @param array $params
     * @return bool
     */
    public static function saveTelemedicineSchedule($providerTelemedicine, $params)
    {

        $results = [];

        if (empty($params['provider_id'])) {
            return false;
        }

        if ($providerTelemedicine['enabled'] == 1) {
            // telemedicine enabled
            $providerPractice = ProviderPractice::getOrCreateTelemedicinePractice(
                $params['provider_id'],
                $params['account_id'],
                $providerTelemedicine
            );
            $providerDetails = ProviderDetails::model()->find(
                'provider_id=:providerId',
                [':providerId' => $params['provider_id']]
            );
            if (!empty($providerDetails)) {
                $providerDetails->schedule_mail_enabled = 1;
                $providerDetails->save();
            }
            if (!empty($providerPractice)) {

                // we have a schedule, create it or update it
                if (!empty($providerTelemedicine['schedule'])) {
                    foreach ($providerTelemedicine['schedule'] as $day => $value) {
                        $scheduleDays[$day] = $value;
                        $scheduleDays[$day]['selected'] = !empty($value) ? 1 : 0;
                    }
                }
                $results['success'] = true;

                // take formatted schedule and create it or update it
                if (!empty($scheduleDays)) {
                    $result = ProviderPracticeSchedule::saveTelemedicineOfficeHours(
                        $providerPractice['id'],
                        $scheduleDays
                    );

                    // audit
                    AuditLog::create(
                        'C',
                        'Telemedicine',
                        null,
                        'Enabling telemedicine booking for provider #' . $params['provider_id'],
                        false
                    );

                    $results['success'] = (bool)$result;
                }

                // we didn't have a schedule or formatted schedule, so clear schedule for the provider-at-practice
                if (empty($providerTelemedicine['schedule'])) {
                    $result = ProviderPracticeSchedule::saveTelemedicineOfficeHours($providerPractice['id'], '');

                    // audit
                    AuditLog::create(
                        'D',
                        'Telemedicine',
                        null,
                        'Auto-removed telemedicine schedule for provider #' . $params['provider_id'],
                        false
                    );

                    $results['success'] = (bool)$result;
                }
            }
        } else {

            // telemedicine disabled, try to remove everything automatically
            $providerPractice = ProviderPractice::getTelemedicinePractice($params['provider_id']);
            if (!empty($providerPractice)) {
                // remove schedule
                $pps = ProviderPracticeSchedule::model()->findAll(
                    'provider_practice_id=:providerPracticeId',
                    [':providerPracticeId' => $providerPractice['id']]
                );
                if (!empty($pps)) {
                    foreach ($pps as $schedule) {
                        $schedule->delete();
                    }
                }
                // remove insurance
                $ppi = ProviderPracticeInsurance::model()->findAll(
                    'practice_id=:practiceId',
                    [':practiceId' => $providerPractice['practice_id']]
                );
                if (!empty($ppi)) {
                    foreach ($ppi as $insurance) {
                        $insurance->delete();
                    }
                }
                // remove link to provider
                $ppx = ProviderPractice::model()->findByPk($providerPractice['id']);
                if (!empty($ppx)) {
                    $ppx->delete();
                }

                // remove account-device
                $ad = AccountDevice::model()->findAll(
                    'practice_id=:practiceId',
                    [':practiceId' => $providerPractice['practice_id']]
                );
                if (!empty($ad)) {
                    foreach ($ad as $device) {
                        // remove providers
                        $adp = AccountDeviceProvider::model()->findAll(
                            'account_device_id=:accountDeviceId',
                            [':accountDeviceId' => $device->id]
                        );
                        if (!empty($adp)) {
                            foreach ($adp as $provider) {
                                $provider->delete();
                            }
                        }
                        $device->delete();
                    }
                }

                // remove reviews
                $pr = ProviderRating::model()->findAll(
                    'practice_id=:practiceId',
                    [':practiceId' => $providerPractice['practice_id']]
                );
                if (!empty($pr)) {
                    foreach ($pr as $rating) {
                        // remove responses
                        $prr = ProviderRatingResponse::model()->findAll(
                            'provider_rating_id=:providerRatingId',
                            [':providerRatingId' => $rating->id]
                        );
                        if (!empty($prr)) {
                            foreach ($prr as $response) {
                                $response->delete();
                            }
                        }
                        $rating->delete();
                    }
                }

                // remove account-practice
                $ap = AccountPractice::model()->findAll(
                    'practice_id=:practiceId',
                    [':practiceId' => $providerPractice['practice_id']]
                );
                if (!empty($ap)) {
                    foreach ($ap as $account) {
                        // remove emr
                        $ape = AccountPracticeEmr::model()->findAll(
                            'practice_id=:practiceId AND account_id=:accountId',
                            [':practiceId' => $account->practice_id, ':accountId' => $account->account_id]
                        );
                        if (!empty($ape)) {
                            foreach ($ape as $emr) {
                                $emr->delete();
                            }
                        }
                        $account->delete();
                    }
                }

                // remove practice details
                $pd = PracticeDetails::model()->findAll(
                    'practice_id=:practiceId',
                    [':practiceId' => $providerPractice['practice_id']]
                );
                if (!empty($pd)) {
                    foreach ($pd as $details) {
                        $details->delete();
                    }
                }

                // remove appointments
                $sm = SchedulingMail::model()->findAll(
                    'practice_id=:practiceId',
                    [':practiceId' => $providerPractice['practice_id']]
                );
                if (!empty($sm)) {
                    foreach ($sm as $scheduling) {
                        // remove comments
                        $smc = SchedulingMailComment::model()->findAll(
                            'scheduling_mail_id=:schedulingMailId',
                            [':schedulingMailId' => $scheduling->id]
                        );
                        if (!empty($smc)) {
                            foreach ($smc as $comment) {
                                $comment->delete();
                            }
                        }

                        // remove history
                        $smh = SchedulingMailHistory::model()->findAll(
                            'scheduling_mail_id=:schedulingMailId',
                            [':schedulingMailId' => $scheduling->id]
                        );
                        if (!empty($smh)) {
                            foreach ($smh as $history) {
                                $history->delete();
                            }
                        }

                        // remove patient
                        $smp = SchedulingMailEhrPatient::model()->findAll(
                            'scheduling_mail_id=:schedulingMailId',
                            [':schedulingMailId' => $scheduling->id]
                        );
                        if (!empty($smp)) {
                            foreach ($smp as $patient) {
                                $patient->delete();
                            }
                        }

                        // delete appointment
                        $scheduling->delete();
                    }
                }

                // remove practice
                $px = Practice::model()->findByPk($providerPractice['practice_id']);
                if (!empty($px)) {
                    $px->delete();
                }
            }

            // update provider details
            $providerDetails = ProviderDetails::model()->find(
                'provider_id=:providerId',
                [':providerId' => $params['provider_id']]
            );
            if (!empty($providerDetails)) {
                $providerDetails->telemedicine_enabled = 0;
                $providerDetails->telemedicine_accept_new_patients = 0;
                $providerDetails->telemedicine_disabled_by_account_id = $params['account_id'];
                $sqlaccid = sprintf(
                    "SELECT account_id, emr_user_data
                    FROM account_provider_emr
                    WHERE provider_id = %d
                    AND emr_user_data  IS NOT NULL ",
                    $params['provider_id']
                );
                $providerAccountId = Yii::app()->db->createCommand($sqlaccid)->queryColumn();
                if (count($providerAccountId) == 0) {
                    $providerDetails->schedule_mail_enabled = 0;
                }
                $providerDetails->save();
            }

            // audit
            AuditLog::create(
                'D',
                'Telemedicine',
                null,
                'Disabling telemedicine booking for provider #' . $params['provider_id'],
                false
            );

            $results['success'] = true;
        }


        $results['error'] = "";
        $results['data'] = "";
        return $results;
    }

    /**
     * inheritPracticeSchedules
     * After the claim process, if the provider has no schedule, asign the practice_office_hours
     *
     * @param integer $providerId
     * @param integer $practiceId
     */
    public static function inheritPracticeSchedules($providerId = 0, $practiceId = 0)
    {
        $results['success'] = false;
        $results['error'] = "";
        $results['data'] = array();
        // assign the default
        $defaultHours = array (
            array('day' => 1, 'hours' => '09:00:00 - 17:00:00'),
            array('day' => 2, 'hours' => '09:00:00 - 17:00:00'),
            array('day' => 3, 'hours' => '09:00:00 - 17:00:00'),
            array('day' => 4, 'hours' => '09:00:00 - 17:00:00'),
            array('day' => 5, 'hours' => '09:00:00 - 17:00:00')
        );
        if (empty($providerId) || empty($practiceId)) {
            $results['error'] = 'Provider and practices are empty';
            return $results;
        }

        // Does this practice have office_hour?
        $practiceOfficeHour = PracticeOfficeHour::model()->findAll(
            'practice_id = :practice_id',
            array(':practice_id' => $practiceId)
        );

        // provider account is linked to this practice?
        $providerPractice = ProviderPractice::model()->find(
            'provider_id=:provider_id AND practice_id=:practice_id',
            array(':provider_id' => $providerId, ':practice_id' => $practiceId)
        );
        if (empty($providerPractice)) {
            // Not linked
            $results['error'] = 'Provider and practice are not linked (empty provider_practice)';
            return $results;
        }

        // Does this provider have a schedule with this practice?
        $providerPracticeShedule = ProviderPracticeSchedule::model()->findAll(
            'provider_practice_id = :provider_practice_id',
            array(':provider_practice_id' => $providerPractice->id)
        );
        if (!empty($providerPracticeShedule)) {
            // Yes, he have an schedule, so do nothing...
            $results['error'] = 'We have records in provider_practice_schedule';
            return $results;
        }

        // Ok, $providerPracticeSchedule is empty, and we have $practiceOfficeHour
        // start the importation
        $scheduleDays = array();
        if (!empty($practiceOfficeHour)) {
            foreach ($practiceOfficeHour as $poh) {
                $providerPracticeSchedule = new ProviderPracticeSchedule;
                $providerPracticeSchedule->day_number = $poh->day_number;
                $providerPracticeSchedule->hours = $poh->hours;
                $providerPracticeSchedule->provider_practice_id = $providerPractice->id;
                $providerPracticeSchedule->is_generated = 1;
                $providerPracticeSchedule->save();
                $scheduleDays[$poh->day_number] = $providerPracticeSchedule;
            }
        } else {
            foreach ($defaultHours as $dd) {
                $providerPracticeSchedule = new ProviderPracticeSchedule;
                $providerPracticeSchedule->day_number = $dd['day'];
                $providerPracticeSchedule->hours = $dd['hours'];
                $providerPracticeSchedule->provider_practice_id = $providerPractice->id;
                $providerPracticeSchedule->is_generated = 1;
                $providerPracticeSchedule->save();
                $ddDay = $dd['day'];
                $scheduleDays[$ddDay] = $providerPracticeSchedule;
            }
        }
        CapybaraCache::loadSchedule($practiceId, $providerId);

        $results['success'] = true;
        $results['error'] = "";
        $results['data'] = $scheduleDays;

        return $results;
    }

    /**
     * getPrimaryPracticeSchedule
     * After the claim process, get provider has primary practice schedule
     * to assign as default schedule for telemed
     * @param integer $providerId
     * @param integer $accountId
     */
    public static function getPrimaryPracticeSchedule($providerId = 0, $accountId = 0)
    {
        $scheduledays = array();
        $results['success'] = false;
        $results['error'] = "";
        $results['data'] = array();
        if (empty($providerId) || empty($accountId)) {
            $results['error'] = 'Provider and Account are empty';
            return $results;
        }

        // find the primary practice
        $sql = sprintf(
            "SELECT p.id, p.provider_id, p.practice_id, p.primary_location
            FROM provider_practice p
            INNER JOIN account_practice a
            ON p.practice_id = a.practice_id
            WHERE p.primary_location = 1
            AND p.provider_id = %d
            AND a.account_id = %d",
            $providerId,
            $accountId
        );
        $primaryPractice = Yii::app()->db->createCommand($sql)->queryRow();
        if (!empty($primaryPractice)) {
            // get the schedule
            $providerPracticeId = $primaryPractice['id'];
            //get the schedule of the practice_id
            $practiceSql = sprintf(
                "SELECT day_number,hours FROM provider_practice_schedule
                WHERE provider_practice_id = %d
                ORDER BY day_number ASC",
                $providerPracticeId
            );
            $providerPracticeShedule = Yii::app()->db->createCommand($practiceSql)->query();
            $dayNames = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
            if (!empty($providerPracticeShedule)) {
                foreach ($providerPracticeShedule as $d) {
                    $strFrom = date('Y-m-d') . ' ' . substr($d['hours'], 0, 8);
                    $from = date_create_from_format('Y-m-d H:i:s', $strFrom);

                    $strTo = date('Y-m-d') . ' ' . substr($d['hours'], 11, 8);
                    $to = date_create_from_format('Y-m-d H:i:s', $strTo);
                    $dayName = $dayNames[$d['day_number']];
                    if ($from && $to) {
                        $hours = array("start" => date_format($from, 'g:i A'), "end" => date_format($to, 'g:i A'));
                        $scheduledays[$dayName]['hours'] = $hours;
                        $scheduledays[$dayName]['selected'] = 1;
                    }
                }
            }
        }
        $results['success'] = true;
        $results['error'] = "";
        $results['data'] = $scheduledays;
        return $results;
    }

}
