<?php

Yii::import('application.modules.core_models.models._base.BaseInsuranceFile');

class InsuranceFile extends BaseInsuranceFile
{
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function Ifslink($fileid, $status, $value)
    {
        return (
            '<a target="_
            blank" href="/insuranceFileStats/admin?fileid=' . $fileid . '&status=' .$status. '">' . $value . '</a>'
        );
    }

    public function getInsuranceFileStatsLink()
    {
        return self::Ifslink($this->id, "All", $this->file_name);
    }

    public function getInsuranceFileStatsErrorLink()
    {
        return self::Ifslink($this->id, "Error", $this->count_error);
    }


    public function getInsuranceFileStatsSkippedLink()
    {
        return self::Ifslink($this->id, "Skipped", $this->count_skipped);
    }
}