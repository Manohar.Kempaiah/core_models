<?php

Yii::import('application.modules.core_models.models._base.BaseIdpSpecialtyMapping');

class IdpSpecialtyMapping extends BaseIdpSpecialtyMapping
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeValidate()
    {
        if (empty($this->id)) {
            $this->date_added = date('Y-m-d H:i:s');
        }

        return parent::beforeValidate();
    }

    /**
     * {@inheritdoc}
     */
    public function search()
    {
        $dataProvider = parent::search();

        // default order by the specialty text
        $sort = new CSort();
        $sort->defaultOrder = ['specialty_text' => CSort::SORT_ASC];
        $dataProvider->setSort($sort);

        return $dataProvider;
    }

}
