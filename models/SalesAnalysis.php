<?php

Yii::import('application.modules.core_models.models._base.BaseSalesAnalysis');

class SalesAnalysis extends BaseSalesAnalysis
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Ensure account id for any new record.
     *
     * @return boolean
     */
    public function beforeValidate()
    {
        if (empty($this->id) && empty($this->account_id)) {
            $this->account_id = Yii::app()->user->id;
        }

        return parent::beforeValidate();
    }

}
