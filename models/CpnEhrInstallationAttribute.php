<?php

Yii::import('application.modules.core_models.models._base.BaseCpnEhrInstallationAttribute');

class CpnEhrInstallationAttribute extends BaseCpnEhrInstallationAttribute
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Find Installation Attribute by CpnEhrInstallationID-AttributeTypeID
     * @param $cpnEhrInstallationId
     * @param $attributeTypeId
     * @return mixed
     */
    public function findByAttributeTypeId($cpnEhrInstallationId, $attributeTypeId)
    {
        $sql = sprintf(
            "SELECT * FROM cpn_ehr_installation_attribute "
            . "WHERE cpn_ehr_installation_id = '%s' AND cpn_ehr_attribute_type_id = '%s'AND enabled_flag = 1;",
            $cpnEhrInstallationId,
            $attributeTypeId
        );

        return self::model()->findBySql($sql);
    }

    /**
     * Returns BaseQueueFolder value for cpn installation
     * @param $cpnInstallationId
     * @return string|null
     */
    public static function getBaseQueueFolderValue($cpnInstallationId)
    {
        //find CPnEhrInstallation
        $cpnEhrInstallation = CpnEhrInstallation::model()->cache(Yii::app()->params['cache_long'])->find(
            'cpn_installation_id = :cpn_installation_id AND del_flag = 0 ORDER BY id ASC',
            array(':cpn_installation_id' => $cpnInstallationId)
        );

        if ($cpnEhrInstallation) {
            // Set baseQueueFolder, looks for a custom value
            $cpnEhrInstallationAttribute = CpnEhrInstallationAttribute::model()
                ->findByAttributeTypeId($cpnEhrInstallation->id, CpnEhrAttributeType::BASE_QUEUE_FOLDER);

            if ($cpnEhrInstallationAttribute) {
                return $cpnEhrInstallationAttribute->attribute_value;
            }
        }

        return null;
    }
}
