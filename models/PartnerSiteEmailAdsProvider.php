<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteEmailAdsProvider');

class PartnerSiteEmailAdsProvider extends BasePartnerSiteEmailAdsProvider
{
    public static $EMAIL_ADS_TARGET = 'PROVIDER';
    
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}