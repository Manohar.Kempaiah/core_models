<?php

Yii::import('application.modules.core_models.models._base.BaseTwilioLog');

class TwilioLog extends BaseTwilioLog
{
    public $organizationId;
    public $organizationName;
    public $salesforceId;
    public $organizationStatus;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        $labels ['organizationId'] = Yii::t('app', 'Org. ID');
        $labels ['organizationName'] = Yii::t('app', 'Org. Name');
        $labels ['salesforceId'] = Yii::t('app', 'SF ID');
        $labels ['organizationStatus'] = Yii::t('app', 'Org. Status');

        return $labels;
    }

    /**
     * Check if we have to send SQS a request to update SF data
     * This happens if we're logging a new qualified call
     * @return boolean
     */
    public function beforeSave()
    {
        // see if this practice belong to an organization?
        $arrOrganizationIds = Practice::belongsToOrganization($this->practice_id);

        if (empty($arrOrganizationIds)) {
            // nope, just leave
            return parent::beforeSave();
        }

        // check if we're updating a call
        if (!$this->isNewRecord) {

            //this means the call has ended and wasn't marked as qualified before but it is now: update salesforce
            if (!empty($this->oldRecord) && !$this->oldRecord->is_qualified && $this->is_qualified) {
                foreach ($arrOrganizationIds as $organizationId) {
                    SqsComponent::sendMessage('updateSFOrganizationCalls', $organizationId);
                }

                // new call, prepare SQS command to send account-contact alerts
                SqsComponent::sendMessage('alertTrackedCall', $this->id);
            }

            // is this a completed call?
            // yes
            if ($this->tw_call_status == 'completed') {
                // Completed call, prepare SQS command to send account-contact alerts
                SqsComponent::sendMessage('alertTrackedCall', $this->id);

                // Insert Notification in cpn_communication_queue
                $this->addNotification();
            }
        }

        // We want to display all the recordings for calls lasting more than 2 seconds
        $audioStatus = 0;
        if ($this->tw_recording_duration > 2) {
            $audioStatus = 1; // active
        }
        $this->audio_verified_in = date("Y-m-d H:i:s");
        $this->audio_status = $audioStatus;

        return parent::beforeSave();
    }

    /**
     * Add a notification message in the CpnCommunicationQueue
     * @return void
     */
    public function addNotification()
    {
        // create our SQL to grab the installation information
        $sql = sprintf(
            "SELECT
                  DISTINCT cpn_installation.id
                FROM cpn_installation
                LEFT JOIN cpn_installation_practice
                  ON cpn_installation_practice.cpn_installation_id = cpn_installation.id
                INNER JOIN  provider_practice
                  ON (
                    provider_practice.practice_id = cpn_installation.practice_id OR
                    provider_practice.practice_id = cpn_installation_practice.practice_id
                  )
                INNER JOIN cpn_installation_provider
                  ON cpn_installation_provider.cpn_installation_id = cpn_installation.id AND
                  cpn_installation_provider.provider_id = provider_practice.provider_id
                WHERE
                  cpn_installation.enabled_flag = 1 AND
                  cpn_installation.del_flag = 0 AND
                  cpn_installation.allow_installation_flag = 0 AND
                  cpn_installation.installation_identifier IS NOT NULL AND
                  (
                    cpn_installation.practice_id = '%s' OR
                    cpn_installation_practice.practice_id = '%s'
                  ) AND
                  cpn_installation_provider.twilio_log_notification = 1;",
            $this->practice_id,
            $this->practice_id
        );

        // run the SQL
        $arrCpnInstallation = Yii::app()->db->createCommand($sql)->queryColumn();

        // grab the message type from the model
        $cpnMessageType = CpnMessageType::model()->find("id = :id", array(":id" => '3'));

        // do we have a message type?
        // no
        if (!$cpnMessageType) {
            // return a false to indicate it was not found
            return false;
        }

        // iterate through the one installation
        foreach ($arrCpnInstallation as $cpnInstallationId) {
            //create the new queue item
            $cpnCommunicationQueue = new CpnCommunicationQueue();
            $cpnCommunicationQueue->cpn_installation_id = $cpnInstallationId;
            $cpnCommunicationQueue->guid = Yii::app()->db->createCommand("SELECT uuid()")->queryScalar();
            $cpnCommunicationQueue->cpn_message_type_id = 3; // CALLS
            $cpnCommunicationQueue->message = $cpnMessageType->name;
            $cpnCommunicationQueue->url = 'https://' .
                                          Yii::app()->params['servers']['dr_pro_hn'] .
                                          '/app/calls/inbound-call?id=' . $this->id;
            $cpnCommunicationQueue->title = 'Doctor.com ' . $cpnMessageType->name;
            $cpnCommunicationQueue->seconds = '5'; // the length of time in seconds to show the alert in the companion
            $cpnCommunicationQueue->date_to_publish = date('Y-m-d H:i:s');
            $cpnCommunicationQueue->date_expires = date(
                'Y-m-d H:i:s',
                strtotime($cpnCommunicationQueue->date_to_publish . ' +1 day')
            ); //expire it in one day
            $cpnCommunicationQueue->added_by_account_id = '1'; // Tech

            //set up our message json here for a appointment
            //{
            //  "messageIdentifier":"blah",
            //  "messageType":blah,
            //  "url":"blah",
            //  "urlTitle":"blah",
            //  "seconds":blah,
            //  "notificationDate":"blah",
            //  "expiresDate":"blah",
            //  "call":
            //  {
            //      "date":"blah",
            //      "from":blah,
            //      "to":blah
            //  }
            //}
            // create our message_json array
            $messageJson = array();
            $messageJson['messageIdentifier'] = $cpnCommunicationQueue->guid;
            $messageJson['messageType'] = $cpnCommunicationQueue->cpn_message_type_id;
            $messageJson['url'] = $cpnCommunicationQueue->url;
            $messageJson['urlTitle'] = $cpnCommunicationQueue->title;
            $messageJson['seconds'] = $cpnCommunicationQueue->seconds;
            $messageJson['notificationDate'] = $cpnCommunicationQueue->date_to_publish;
            $messageJson['expiresDate'] = $cpnCommunicationQueue->date_expires;

            // create our call array
            $call = array();
            $call['date'] = $cpnCommunicationQueue->date_to_publish;
            $call['from'] = $this->tw_from;
            $call['to'] = $this->tw_to;

            // add the call array
            $messageJson['call'] = $call;

            // encode the JSON into our message_json field
            $cpnCommunicationQueue->message_json = json_encode($messageJson);

            // save it to the database
            $cpnCommunicationQueue->save();
        }
    }

    /**
     * Called on rendering the column for each row
     * @return string link to tooltip
     */
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id . '" '
            . 'rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice . '">'
            . $this->practice . '</a>');
    }

    /**
     * Called on rendering the column for each row
     * @return string link to tooltip
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id . '" '
            . 'rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">'
            . $this->provider . '</a>');
    }

    /**
     * Converts dates from EST (server timezone) to the account's timezone.
     * @param int $account_id
     * @param date $time_started
     * @param date $time_ended [Optional]
     * @return array $time_converted['time_started', 'time_ended', 'timezone'] -- 'time_started' and 'time_ended' are
     * DateTime objects, 'timezone' is a string
     */
    public static function convertAccountTimezone($account_id, $time_started, $time_ended = null)
    {
        $time_converted = array();
        $timezone = Yii::app()->db->createCommand()
            ->selectDistinct('time_zone')
            ->from('timezone')
            ->join('state', 'state.abbreviation = timezone.state_code')
            ->join('city', 'city.state_id = state.id')
            ->join('location', 'location.city_id = city.id')
            ->join('account', 'account.location_id = location.id')
            ->where('account.id = :account_id', array(':account_id' => $account_id))
            ->queryScalar();

        // Get a valid php time zone
        $timezone = Timezone::convertToPhpTimeZone($timezone);

        $time_started_converted = new DateTime($time_started, new DateTimeZone('EST'));
        if (!empty($timezone)) {
            $time_started_converted->setTimezone(new DateTimeZone($timezone));
        }
        $time_converted['time_started'] = $time_started_converted;
        $time_converted['timezone'] = $timezone;
        if (!is_null($time_ended)) {
            $time_ended_converted = new DateTime($time_ended);
            $time_ended_converted->setTimezone(new DateTimeZone($timezone));
            $time_converted['time_ended'] = $time_ended_converted;
        }

        return $time_converted;
    }

    /**
     * Determines if the given call is a qualified new patient call and
     * thus should be indicated as such in various reports.
     *
     * A qualified new patient call is defined as satisfying both:
     *
     * (a) REAL-PATIENT - Caller is not on our "blocked" list (for
     * spammers/solicitors), or our "approved business" list (for labs, etc.).
     * Note that blocked numbers often are added AFTER a call took place, so
     * we may need to recompute what is qualified and what is not accordingly.
     * (b) CONNECTED - Call is "connected". This means call status "Completed",
     * "Busy", or "In Progress", as well as "Hangup" ONLY if they hung up after
     * pressing 1 when the caller interface is on.
     * (c) FIRST-QUALIFIED-OF-THE-MONTH - Caller has not had a qualified call
     * to that practice/provider in the past "calendar month" days,
     * e.g. 28-31 days, depending on the month.
     * (d) CALLER-PRESSED-ONE (interface only) - If call interface is on,
     * caller pressed "1" to indicate they are new.
     */
    public function isQualifiedNewPatientCall()
    {

        // return false if called wasn't "CONNECTED"
        switch ($this->tw_call_status) {
            case 'completed':
            case 'busy':
            case 'in-progress':
                break;
            case 'hangup':
                if ($this->caller_option != 'N') {
                    return false;
                }
                break;
            default:
                return false;
        }

        // Return false if CALLER-PRESSED-ONE (interface only) is false
        if ($this->caller_option !== null && $this->caller_option != 'N') {
            return false;
        }

        // Return false if call wasn't "REAL-PATIENT" (e.g. is on
        // twilio blocked numbers or twilio ignore numbers list)
        if (TwilioBlockedNumbers::model()->exists("number = :number", array(
                    ':number' => $this->tw_from,
                )) || TwilioIgnoreNumber::model()->exists("number = :number", array(
                    ':number' => $this->tw_from,
                ))) {
            return false;
        }

        // Return true only if last criteria satisfied,
        // namely that the call is "FIRST-QUALIFIED-OF-THE-MONTH"
        // from that number for the provider/practice
        if (TwilioLog::model()->exists(
            "tw_from = :tw_from AND
            provider_id = :provider_id AND
            practice_id = :practice_id AND
            time_started > DATE_SUB(date(:time_started_1),
                INTERVAL 1 MONTH) AND
            time_started < :time_started_2 AND
            (is_qualified = 1)",
            array(
                ':tw_from' => $this->tw_from,
                ':provider_id' => $this->provider_id,
                ':practice_id' => $this->practice_id,
                ':time_started_1' => $this->time_started,
                ':time_started_2' => $this->time_started
                ))) {

            // this provider/practice has already had
            // a qualified call from this number in the past month
            return false;
        } else {

            // this provider/practice doesn't yet have
            // a qualified call from this number in the past month
            return true;
        }
    }

    /**
     * Match partner IDs to a source
     * @param int-string $partnerId
     * @return string
     */
    public static function getPartnerSource($partnerId)
    {
        $partnerId = (string) $partnerId;

        $sourcesShowcase = array(
            "38", // Showcase Page (AdWords)
            "37", // Showcase Page (Ask)
            "36", // Showcase Page (YourDoctor)
            "35", // Showcase Page (Doctor)
            "34", // Showcase Page (Direct || Unknown)
            "29", // Showcase 360 - Bonus3
            "28", // Showcase 360 - Bonus2
            "17", // Showcase 360
        );
        $sourcesClickToCall = array(
            "40", // Soleo Test
            "31", // AdWords (NYC)
            "30", // AdWords (Manhattan)
            "3"   // AdWords
        );
        $sourcesPremium = array(
            "33", // Ask
            "32", // Healthline
            "14", // Vitals
            "13", // HealthGrades
        );

        if (array_search($partnerId, $sourcesShowcase) !== false) {
            return "Showcase Page";
        } elseif (array_search($partnerId, $sourcesClickToCall) !== false) {
            return "Click-To-Call";
        } elseif (array_search($partnerId, $sourcesPremium) !== false) {
            return "Premium Partner";
        } else {
            return "Internet Number";
        }
    }

    /**
     * PADM: Get a tracked call's details
     * @param int $accountId
     * @param int $twilioLogId
     * @return array
     */
    public static function getTrackedCallDetails($accountId, $twilioLogId)
    {
        $sql = sprintf(
            "SELECT twilio_log.*, account_practice.account_id
            FROM twilio_log
            LEFT JOIN account_practice
            ON account_practice.practice_id = twilio_log.practice_id
            WHERE twilio_log.id = %d
            AND (account_id = %d OR account_id IS NULL);",
            $twilioLogId,
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get a practice's list of tracked calls
     * @param int $accountId
     * @param string $condition
     * @param string $sqlOrder
     * @param string $sqlSort
     * @return array
     */
    public static function getTrackedPracticeCalls($accountId, $condition, $sqlOrder, $sqlSort)
    {

        $sql = sprintf(
            "SELECT l.id AS id_log, l.twilio_number_id, l.provider_id,
            l.practice_id, l.tw_call_sid, l.tw_account_sid,
            l.tw_from, l.tw_to, l.tw_call_status,
            l.tw_api_version, l.tw_direction, l.tw_forwarded_from,
            l.tw_from_city, l.tw_from_state, l.tw_from_zip,
            l.tw_from_country, l.tw_to_city, l.tw_to_state,
            l.tw_to_zip, l.tw_to_country, l.tw_call_duration,
            l.tw_recording_url, l.tw_recording_sid, l.tw_recording_duration,
            l.time_started, l.time_ended,
            l.caller_option, l.is_qualified, l.twilio_partner_id,
            pra.name, twilio_blocked_numbers.id,
            twilio_partner.name AS partner
            FROM twilio_log AS l
            INNER JOIN account_practice AS ap
            ON l.practice_id = ap.practice_id
            INNER JOIN account AS a
            ON ap.account_id = a.id
            INNER JOIN practice AS pra
            ON l.practice_id = pra.id
            LEFT JOIN twilio_number
            ON l.twilio_number_id = twilio_number.id
            LEFT JOIN twilio_blocked_numbers
            ON (l.tw_from = twilio_blocked_numbers.number OR l.tw_from = CONCAT('+1', twilio_blocked_numbers.number))
            AND (twilio_number.twilio_partner_id = twilio_blocked_numbers.twilio_partner_id
            OR twilio_blocked_numbers.twilio_partner_id IS NULL)
            LEFT JOIN twilio_partner
            ON l.twilio_partner_id = twilio_partner.id
            WHERE a.id = %d
            AND SUBSTRING(l.tw_from, 0, 5) != '+1800'
            AND SUBSTRING(l.tw_from, 0, 5) != '+1888'
            AND SUBSTRING(l.tw_from, 0, 5) != '+1877'
            AND SUBSTRING(l.tw_from, 0, 5) != '+1866'
            AND SUBSTRING(l.tw_from, 0, 5) != '+1855'
            HAVING twilio_blocked_numbers.id IS NULL
            %s
            ORDER BY %s %s ;",
            $accountId,
            $condition,
            $sqlOrder,
            $sqlSort
        );
        return Yii::app()->db->createCommand($sql)->query();
    }

    /**
     * Get a list of tracked calls to this account
     * @param int $accountId
     * @return array
     */
    public static function getAccountCalls($accountId)
    {

        $sql = sprintf(
            "SELECT l.id AS id_log, l.twilio_number_id, l.provider_id,
            l.practice_id,
            l.tw_from, l.tw_to, l.tw_call_status,
            l.tw_direction, l.tw_forwarded_from,
            l.tw_from_city, l.tw_from_state, l.tw_from_zip,
            l.tw_from_country, l.tw_to_city, l.tw_to_state,
            l.tw_to_zip, l.tw_to_country, l.tw_call_duration,
            l.tw_recording_url, l.tw_recording_sid, l.tw_recording_duration,
            l.audio_status, l.audio_verified_in,
            l.time_started, l.time_ended,
            l.caller_option, l.is_qualified, l.twilio_partner_id,
            twilio_blocked_numbers.id,
            twilio_partner.name AS partner
            FROM twilio_log AS l
            INNER JOIN account_practice AS ap ON l.practice_id = ap.practice_id
            INNER JOIN account AS a ON ap.account_id = a.id
            INNER JOIN practice AS pra ON l.practice_id = pra.id
            LEFT JOIN twilio_number ON l.twilio_number_id = twilio_number.id
            LEFT JOIN twilio_blocked_numbers
              ON (l.tw_from = twilio_blocked_numbers.number OR l.tw_from = CONCAT('+1', twilio_blocked_numbers.number))
              AND (twilio_number.twilio_partner_id = twilio_blocked_numbers.twilio_partner_id
              OR twilio_blocked_numbers.twilio_partner_id IS NULL)
            LEFT JOIN twilio_partner ON l.twilio_partner_id = twilio_partner.id
            WHERE a.id = %d
            HAVING twilio_blocked_numbers.id IS NULL
            ORDER BY time_started DESC ;",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Command: Gets the total calls for the performance by account report
     * @return array
     */
    public static function getTotalCallsPerPartner()
    {
        $sql = sprintf("SELECT account_id, twilio_number.twilio_partner_id, YEAR(twilio_log.time_started) AS call_year,
            MONTH(twilio_log.time_started) AS call_month, COUNT(1) AS call_count
            FROM twilio_log
            INNER JOIN account_practice
            ON (account_practice.practice_id = twilio_log.practice_id)
            LEFT JOIN twilio_number
            ON twilio_log.twilio_number_id = twilio_number.id
            LEFT JOIN twilio_blocked_numbers
            ON (twilio_log.tw_from = twilio_blocked_numbers.number OR twilio_log.tw_from = CONCAT('+1',
            twilio_blocked_numbers.number))
            AND (twilio_number.twilio_partner_id = twilio_blocked_numbers.twilio_partner_id
            OR twilio_blocked_numbers.twilio_partner_id IS NULL)
            WHERE twilio_blocked_numbers.id IS NULL
            AND (twilio_log.time_started > date_sub(NOW(), INTERVAL 12 MONTH)
            AND twilio_log.time_started <= NOW())
            GROUP BY account_id, twilio_number.twilio_partner_id, call_year, call_month;");
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Command: Gets the qualified calls for the performance by account report
     * @return array
     */
    public static function getQualifiedCallsPerPartner()
    {
        $sql = sprintf("SELECT account_id, twilio_number.twilio_partner_id, YEAR(twilio_log.time_started) AS call_year,
            MONTH(twilio_log.time_started) AS call_month, COUNT(1) AS call_count
            FROM twilio_log
            INNER JOIN account_practice
            ON (account_practice.practice_id = twilio_log.practice_id)
            LEFT JOIN twilio_number
            ON twilio_log.twilio_number_id = twilio_number.id
            LEFT JOIN twilio_blocked_numbers
            ON (twilio_log.tw_from = twilio_blocked_numbers.number OR twilio_log.tw_from = CONCAT('+1',
            twilio_blocked_numbers.number))
            AND (twilio_number.twilio_partner_id = twilio_blocked_numbers.twilio_partner_id
            OR twilio_blocked_numbers.twilio_partner_id IS NULL)
            WHERE is_qualified
            AND twilio_blocked_numbers.id IS NULL
            AND (twilio_log.time_started > date_sub(NOW(), INTERVAL 12 MONTH)
            AND twilio_log.time_started <= NOW())
            GROUP BY account_id, twilio_number.twilio_partner_id, call_year, call_month;");
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get number of calls an organization has, to update SalesForce:
     * DDC_Tracked_Calls__c
     * @param int $organizationId
     * @return mixed
     */
    public static function updateSFOrganizationCalls($organizationId)
    {

        $organization = Organization::model()->findByPk($organizationId);
        if (!$organization || empty($organization->salesforce_id)) {
            return false;
        }

        $value = self::getOrganizationCalls($organizationId);

        return Yii::app()->salesforce->updateAccountField($organization->salesforce_id, 'DDC_Tracked_Calls__c', $value);
    }

    /**
     * Count the number of calls an organization has received
     * @param int $organizationId
     * @return int
     */
    public static function getOrganizationCalls($organizationId)
    {

        $sql = sprintf(
            "SELECT COUNT(1) AS call_count
            FROM organization
            INNER JOIN organization_account
            ON organization.id = organization_account.organization_id
            AND `connection` = 'O'
            INNER JOIN account_practice
            ON organization_account.account_id = account_practice.account_id
            INNER JOIN twilio_log
            ON account_practice.practice_id = twilio_log.practice_id
            AND tw_call_duration > 0
            WHERE organization.id = %d;",
            $organizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

     /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical use case:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->compare('t.id', $this->id, true);
        $criteria->compare('t.tw_to', preg_replace('/\D/', '', $this->twilio_number_id), true);
        $criteria->compare('caller_option', $this->caller_option, true);
        $criteria->compare('t.twilio_partner_id', $this->twilio_partner_id, false);
        $criteria->compare('t.tw_from', preg_replace('/\D/', '', $this->tw_from), true);
        $criteria->compare('CONCAT(t.tw_from_city, " - ", t.tw_from_state)', $this->tw_from_city, true);
        $criteria->compare('t.tw_from_zip', $this->tw_from_zip, false);
        $criteria->compare('t.is_qualified', $this->is_qualified, false);
        $criteria->compare('t.tw_call_status', $this->tw_call_status, false);
        $criteria->compare('t.time_started', $this->time_started, true);

        if (strlen($this->tw_recording_sid)) {
            if ($this->tw_recording_sid) {
                $criteria->addCondition('t.tw_recording_sid IS NOT NULL');
            } else {
                $criteria->addCondition('t.tw_recording_sid IS NULL');
            }
        }

        if (!intval($this->provider_id) && is_string($this->provider_id) && strlen($this->provider_id) > 0) {
            $criteria->compare('provider.first_last', $this->provider_id, true);
        } elseif (intval($this->provider_id)) {
            $criteria->compare('t.provider_id', $this->provider_id, true);
        }
        if (!intval($this->practice_id) && is_string($this->practice_id) && strlen($this->practice_id) > 0) {
            $criteria->compare('practice.name', $this->practice_id, true);
        } elseif (intval($this->practice_id)) {
            $criteria->compare('t.practice_id', $this->practice_id, true);
        }

        $criteria->join = 'LEFT JOIN ' . ProviderPractice::model()->tablename() . '
            ON provider_practice.practice_id = t.practice_id
            AND (t.provider_id = provider_practice.provider_id OR t.provider_id IS NULL OR t.provider_id = 0)
            LEFT JOIN practice ON practice.id = t.practice_id
            LEFT JOIN provider ON provider.id = t.provider_id
            LEFT JOIN account_provider
            ON account_provider.provider_id = t.provider_id
            LEFT JOIN organization_account
            ON organization_account.account_id = account_provider.account_id
            LEFT JOIN organization
            ON organization.id = organization_account.organization_id';

        $criteria->select = 'DISTINCT t.tw_call_duration,
            t.tw_recording_duration,
            t.id,
            t.twilio_number_id,
            t.provider_id,
            t.practice_id,
            t.twilio_partner_id,
            t.caller_option,
            t.is_qualified,
            t.caller,
            t.tw_call_sid,
            t.tw_account_sid,
            t.tw_from,
            t.tw_to,
            t.tw_call_status,
            t.tw_api_version,
            t.tw_direction,
            t.tw_forwarded_from,
            t.tw_from_city,
            t.tw_from_state,
            t.tw_from_zip,
            t.tw_from_country,
            t.tw_to_city,
            t.tw_to_state,
            t.tw_to_zip,
            t.tw_to_country,
            t.tw_recording_url,
            t.tw_recording_sid,
            t.time_started,
            t.time_ended,
            organization.id AS organizationId,
            organization.organization_name AS organizationName,
            organization.salesforce_id AS salesforceId,
            IF (
                organization.status IS NULL,
                "",
                CASE WHEN organization.status = "C" THEN "Canceled"
                WHEN organization.status = "A" THEN "Active"
                ELSE "Paused" END
            ) AS organizationStatus';

        $criteria->group = 't.id';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100)
        ));
    }

    /**
     * process and send SMS/Mail alert when save new provider rating
     * alertType = alert_tracked_call
     * @param int $twilioLogId
     */
    public static function sendAlertTrackedCall($twilioLogId = 0)
    {
        if ($twilioLogId == 0) {
            return false;
        }
        // find scheduling record
        $sql = sprintf(
            "SELECT tw.id, tw.provider_id, tw.practice_id, target_number,
            pro.first_last AS provider_name, pra.name AS practice_name,
            pra.address_full AS practice_address, pra.phone_number AS practice_phone
            FROM twilio_log AS tw
            LEFT JOIN practice AS pra ON tw.practice_id = pra.id
            LEFT JOIN provider AS pro ON tw.provider_id = pro.id
            WHERE tw.id = %d LIMIT 1;",
            $twilioLogId
        );
        $tw = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryRow();

        if (!empty($tw)) {
            $practiceId = $tw['practice_id'];
            $providerId = $tw['provider_id'];

            $alertsTargetSMS = AccountContactSetting::model()->getAlertsTarget(
                'SMS',
                'alert_tracked_call',
                $practiceId,
                $providerId
            );
            if (!empty($alertsTargetSMS)) {
                // send alerts...
                foreach ($alertsTargetSMS as $alert) {
                    $to = $alert['mobile_phone'];
                    $body = 'New Call at ' . $tw['target_number'] . ' for: ' . $tw['practice_name'];
                    $body.= '- Login for details https://goo.gl/td7udu';
                    $locationId = $alert['location_id'];
                    SmsComponent::sendSms($to, $body, false, $locationId);
                }
            }
            return true;
        }
    }

    /**
     * Counts the number of calls associated with a practice
     * @return int
     */
    public static function countCallsByPractice()
    {
        $sql = 'SELECT SQL_NO_CACHE COUNT(1) AS total
            FROM twilio_log
            LEFT JOIN practice ON twilio_log.practice_id=practice.id
            LEFT JOIN twilio_partner ON twilio_log.twilio_partner_id=twilio_partner.id
            LEFT JOIN account_provider ON account_provider.provider_id = twilio_log.provider_id
            LEFT JOIN organization_account ON organization_account.account_id = account_provider.account_id
            LEFT JOIN organization ON (organization.id =organization_account.organization_id);';
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get the twilio call list for all practice between two limits
     * @param int $limitFrom
     * @param int $limitTo
     * @return array
     */
    public static function getCallsByPractice($limitFrom, $limitTo)
    {
        $sql = sprintf(
            "SELECT SQL_NO_CACHE
            twilio_log.twilio_number_id,
            twilio_partner.name AS twilio_partner_name,
            twilio_log.provider_id,
            twilio_log.practice_id,
            twilio_log.tw_from,
            twilio_log.tw_from_zip,
            twilio_log.tw_from_city,
            twilio_log.tw_to,
            twilio_log.tw_to_zip,
            twilio_log.tw_to_city,
            twilio_log.tw_call_status,
            twilio_log.tw_call_duration,
            twilio_log.time_started,
            practice.name as pract_name,
            twilio_log.is_qualified,
            twilio_log.caller_option,
            organization.id AS organization_id,
            organization.organization_name AS organization_name,
            CONCAT('" . Yii::app()->salesforce->applicationUri . "', organization.salesforce_id) AS salesforce_id,
            IF (
                organization.id IS NULL,
                '',
                CASE WHEN organization.status = 'C' THEN 'Canceled'
                WHEN organization.status = 'A' THEN 'Active'
                ELSE 'Paused' END
            ) AS status
            FROM twilio_log
            LEFT JOIN practice ON twilio_log.practice_id=practice.id
            LEFT JOIN twilio_partner ON twilio_log.twilio_partner_id=twilio_partner.id
            LEFT JOIN account_provider ON account_provider.provider_id = twilio_log.provider_id
            LEFT JOIN organization_account ON organization_account.account_id = account_provider.account_id
            LEFT JOIN organization ON (organization.id = organization_account.organization_id)
            LIMIT %d, %d;",
            $limitFrom,
            $limitTo
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }

    /**
     * Get the twilio call list for a practice since a particular date and time
     * @param int $installationId
     * @param date $dateSince
     * @return array
     */
    public static function getCallsByPracticeSinceDate($installationId, $dateSince)
    {
        //create our SQL
        $sql = sprintf(
            "SELECT    DISTINCT
                    tl.id,
                    tl.time_started,
                    tl.tw_from,
                    tl.tw_from_city,
                    tl.tw_from_state,
                    tl.caller_option
            FROM      twilio_log tl
            INNER JOIN cpn_installation i on tl.practice_id = i.practice_id
            WHERE     tl.tw_from IS NOT NULL
              AND   tl.caller_option IS NOT NULL
              AND   i.id = %d
              AND   tl.time_started > '%s'
            ORDER BY  tl.time_started;",
            $installationId,
            $dateSince->format('Y-m-d H:i:s')
        );

        //return our results
        return Yii::app()->dbRO->createCommand($sql)->queryAll();
    }

    /**
     * get twilio log for export on organization tool
     * @param int $organizationId
     * @return array
     */
    public static function getOrganizationExport($organizationId)
    {

        $sql = "SELECT account_id FROM organization_account
            WHERE `connection`= 'O' AND organization_id = :organizationId";

        $ownerAccountId = Yii::app()->db->createCommand($sql)->
            bindParam(":organizationId", $organizationId, PDO::PARAM_INT)->queryScalar();

        if ($ownerAccountId) {
            $sql = sprintf(
                "SELECT DISTINCT
                twilio_log.id as TwilioCallId,
                practice.name as PracticeName,
                practice.address_full as PracticeAddress,
                practice.phone_number as PracticePrivatePhoneNumber,
                twilio_partner.name as TwilioPartner,
                twilio_number.twilio_number as TwilioNumber,
                SUBSTRING(tw_from,3) as PatientPhone,
                DATE_FORMAT(twilio_log.time_started,'%%m/%%d/%%Y') as CallTimeStart,
                DATE_FORMAT(twilio_log.time_ended, '%%m/%%d/%%Y') as CallTimeEnd,
                CASE caller_option
                WHEN 'N' THEN 'New Patient'
                WHEN 'R' THEN 'Returning Patient'
                WHEN 'B' THEN 'Business Office'
                WHEN 'D' THEN 'Disabled Twilio Number'
                END as CallOption,
                tw_call_status as CallStatus,
                IF(tw_call_duration='0','-',tw_call_duration) as CallDuration,
                tw_recording_url as CallRecordingUrl
                FROM twilio_number
                INNER JOIN twilio_log
                ON twilio_log.twilio_number_id= twilio_number.id
                INNER JOIN twilio_partner
                ON twilio_partner.id= twilio_number.twilio_partner_id
                INNER JOIN practice
                ON practice.id= twilio_number.practice_id
                INNER JOIN account_practice
                ON account_practice.practice_id= practice.id
                WHERE account_practice.account_id=%s
                ORDER BY twilio_log.time_started;",
                $ownerAccountId
            );
            return Yii::app()->db->createCommand($sql)->queryAll();
        } else {
            return array();
        }
    }

    /**
     * Return the number of calls per call type for last month for a given organization
     * @param int $organizationId
     * @return array
     */
    public static function getMRRecentCalls($organizationId)
    {
        $sql = sprintf(
            "SELECT caller_option, COUNT(DISTINCT(twilio_log.id)) AS total
            FROM twilio_log
            INNER JOIN account_practice
            ON twilio_log.practice_id = account_practice.practice_id
            LEFT JOIN account_provider
            ON account_provider.account_id = account_practice.account_id
            AND (account_provider.provider_id = twilio_log.provider_id
            OR twilio_log.provider_id IS NULL)
            INNER JOIN organization_account
            ON account_practice.account_id = organization_account.account_id
            AND `connection` = 'O'
            WHERE organization_id = %d
            AND twilio_log.time_started >= '%s'
            AND caller_option IN ('N','R')
            GROUP BY caller_option;",
            $organizationId,
            date('Y-m-01', strtotime('last month'))
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get calls from Twilio and store the results in the DB
     * This can be called from the command line or from GADM
     *
     * This won't work in non-production environments unless production credentials are used
     *
     * @param string $environment
     * @return boolean
     *
     * @see TwilioCommand::actionRestoreCallLog
     * @see modules/providers/TwilioLogController.php
     */
    public static function restoreCallLog($environment)
    {

        set_time_limit(0);

        // first, check which was the last page we processed
        $lastPage = GlobalParameter::model()->find('`key` = "twilio_api_call_log_last_page"');

        if (empty($lastPage)) {
            // save an initial record of what we're processing
            $gp = new GlobalParameter;
            $gp->key = 'twilio_api_call_log_last_page';
            $gp->value = '/2010-04-01/Accounts/ACc3b13f8a009cf6e8ddd7783bace6ffcf/Calls.xml?PageSize=1000&Page=0';
            $gp->save();
            $lastPage = GlobalParameter::model()->find('`key` = "twilio_api_call_log_last_page"');
        }
        $uri = $lastPage->value;

        $domain = 'https://api.twilio.com';

        echo 'Starting process from URI ' . $uri . PHP_EOL;

        while ($uri != '') {

            echo 'Init...';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $domain . $uri);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            /**
             * Unfortunately from https://www.twilio.com/docs/api/rest/test-credentials
             * > Your test credentials can currently be used to interact with the following three resources:
             * > Buying phone numbers - Sending SMS messages - Making calls
             * > Requests to any other resource with test credentials will receive a 403 Forbidden response.
             * > In the future, we may enable these resources for testing as well
             */
            $userPwd = Yii::app()->params['twilio_account_sid'] . ':' . Yii::app()->params['twilio_auth_token'];
            if ($environment == 'providers.doctor.com') {
                // only use prod credentials in production
                $userPwd = Yii::app()->params['twilio_account_sid_prod'] . ':' .
                    Yii::app()->params['twilio_auth_token_prod'];
            }

            curl_setopt($ch, CURLOPT_USERPWD, $userPwd);

            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            $output = curl_exec($ch);
            curl_close($ch);
            echo 'Received' . PHP_EOL;

            if (!empty($output)) {
                $calls = new SimpleXMLElement($output);

                $uri = $calls->Calls['nextpageuri'];

                if ($uri != $lastPage->value && !empty($uri)) {
                    // update the last page we checked
                    $lastPage->value = $uri;
                    $lastPage->save();
                }

                $calls = $calls->Calls->Call;

                if (!$calls) {
                    // probably a 403
                    return false;
                }

                foreach ($calls as $call) {

                    echo 'Creating ' . $call->Sid[0] . '...';

                    $callExists = TwilioApiCallLog::model()->find('sid=:sid', array(':sid' => $call->Sid[0]));
                    if (!empty($callExists)) {
                        // we already logged this call
                        echo 'Skipped' . PHP_EOL;
                    }

                    $apiCall = new TwilioApiCallLog();
                    $apiCall->sid = $call->Sid[0];
                    $apiCall->date_created = date('Y-m-d H:i:s', strtotime($call->DateCreated[0]));
                    $apiCall->date_updated = date('Y-m-d H:i:s', strtotime($call->DateUpdated[0]));
                    $apiCall->parent_call_sid = $call->ParentCallSid[0];
                    $apiCall->account_sid = $call->AccountSid[0];
                    $apiCall->to = $call->To[0];
                    $apiCall->to_formatted = $call->ToFormatted[0];
                    $apiCall->from = $call->From[0];
                    $apiCall->from_formatted = $call->FromFormatted[0];
                    $apiCall->phone_number_sid = $call->PhoneNumberSid[0];
                    $apiCall->status = $call->Status[0];
                    $apiCall->start_time = date('Y-m-d H:i:s', strtotime($call->StartTime[0]));
                    $apiCall->end_time = date('Y-m-d H:i:s', strtotime($call->EndTime[0]));
                    $apiCall->duration = $call->Duration[0];
                    $apiCall->price = $call->Price[0];
                    $apiCall->price_unit = $call->PriceUnit[0];
                    $apiCall->direction = $call->Direction[0];
                    $apiCall->answered_by = json_encode($call->AnsweredBy[0]);
                    $apiCall->annotation = json_encode($call->annotation[0]);
                    $apiCall->api_version = $call->ApiVersion[0];
                    $apiCall->forwarded_from = $call->ForwardedFrom[0];
                    $apiCall->group_sid = json_encode($call->GroupSid[0]);
                    $apiCall->caller_name = json_encode($call->CallerName[0]);
                    $apiCall->uri = $call->Uri[0];
                    $apiCall->subresource_uris = json_encode($call->SubresourceUris[0]);
                    $apiCall->save();

                    echo 'Saved' . PHP_EOL;
                }
                echo PHP_EOL . 'Next page...' . PHP_EOL;

            }
        }
        echo 'All done' . PHP_EOL;
    }

    /**
     * Checks a record's ownership
     * @param int $twilioLogId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($twilioLogId = 0, $accountId = 0)
    {
        if ($twilioLogId < 1 || $accountId < 1) {
            return false;
        }
        $sql = sprintf(
            "SELECT account_id
            FROM twilio_log
            INNER JOIN account_practice
            ON twilio_log.practice_id = account_practice.practice_id
            WHERE twilio_log.id = %d
            AND account_id = %d LIMIT 1;",
            $twilioLogId,
            $accountId
        );
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        return !empty($result) ? true : false;
    }

    /**
     * Get the recording of a call from S3
     * @param string $sid
     */
    public static function getRecording($sid)
    {

        // get aws library
        Yii::import('application.vendors.2amigos.yii-aws.components.*');

        // bucket credentials
        $bucketName = Yii::app()->params['twilio_bucket_name'];
        $bucketKey = Yii::app()->params['twilio_bucket_key'];
        $bucketSecret = Yii::app()->params['twilio_bucket_secret'];

        // instantiate s3
        $s3 = new A2S3(array(
            'key' => $bucketKey,
            'secret' => $bucketSecret,
        ));

        try {
            // Get the object.
            $result = $s3->getObject([
                'Bucket' => $bucketName,
                'Key' => $sid . '.mp3'
            ]);

            // Display the object in the browser.
            header("Content-Type: audio/mpeg");
            header("Accept-Ranges: none");
            header('Content-Disposition: filename="' . $sid . '.mp3"');

            return (string) $result['Body'];
        } catch (S3Exception $e) {
            // throw exception
            return $e->getMessage() . PHP_EOL;
        }

    }

}
