<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPatient');

class ProviderPatient extends BaseProviderPatient
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
