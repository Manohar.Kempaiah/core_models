<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeMatchingRules');

class ProviderPracticeMatchingRules extends BaseProviderPracticeMatchingRules
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
