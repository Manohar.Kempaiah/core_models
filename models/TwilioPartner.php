<?php

Yii::import('application.modules.core_models.models._base.BaseTwilioPartner');

class TwilioPartner extends BaseTwilioPartner
{
    public const GOOGLE_PLACES = 8;

    public static function model($className = __CLASS__)
    {

        return parent::model($className);
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        $relations = parent::relations();
        $relations['partnerSite'] = [
            self::BELONGS_TO,
            'PartnerSite',
            'partner_site_id'
        ];

        return $relations;
    }

    /**
     * Clean phone format
     * @return boolean
     */
    public function beforeSave()
    {

        $this->phone_number = preg_replace('/[^0-9]/', '', $this->phone_number);

        return parent::beforeSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // the partner site "1" is Doctor.com -- we don't want it to be deleted since most records in the DB link to it
        if ($this->id == 1) {
            // no way
            $this->addError("id", "Can't delete Doctor.com.");
            return false;
        }

        // delete records from twilio_number before delete this twilio_partner id
        $arrTwilioNumber = TwilioNumber::model()->findAll(
            "twilio_partner_id = :twilio_partner_id",
            array(
                ":twilio_partner_id" => $this->id
            )
        );
        foreach ($arrTwilioNumber as $twilioNumber) {
            if (!$twilioNumber->delete()) {
                $this->addError("id", "Couldn't delete phone number.");
                return false;
            }
        }

        return parent::beforeDelete();
    }

}
