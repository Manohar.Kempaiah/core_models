<?php

Yii::import('application.modules.core_models.models._base.BaseAccountProvider');

class AccountProvider extends BaseAccountProvider
{

    public $account_full_name;

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        $accountId = $this->account_id;
        $providerId = $this->provider_id;

        // CPN Validation
        $sql = sprintf(
            "SELECT cpn_installation.id
            FROM cpn_installation
            INNER JOIN cpn_installation_provider
                ON cpn_installation_provider.cpn_installation_id = cpn_installation.id
            INNER JOIN cpn_ehr_installation
                ON cpn_installation.id = cpn_ehr_installation.cpn_installation_id
            WHERE cpn_installation_provider.provider_id= %d
                AND cpn_installation.account_id = %d
                AND cpn_installation.enabled_flag = 1
                AND cpn_installation.del_flag = 0
                AND cpn_ehr_installation.enabled_flag = 1
                AND cpn_ehr_installation.del_flag = 0
                AND cpn_installation_user_id IS NOT NULL; ",
            $providerId,
            $accountId
        );
        $existsCpn = Yii::app()->db->createCommand($sql)->queryScalar();
        if ($existsCpn) {
            $error = "This provider has an active Companion installation. "
                . "Please deactivate it before unlinking the provider.";
            $this->addError('provider_id', $error);
            if (Yii::app()->request->IsAjaxRequest) {
                header("HTTP/1.0 409 Conflict");
            } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                Yii::app()->user->setFlash('error', $error);
            }
            return false;
        }

        // owner Validation
        $doOwnerValidation = OrganizationAccount::isOwnerAccount($accountId);
        if ($doOwnerValidation) {

            // Has many accounts?
            $exists = AccountProvider::model()->exists(
                'provider_id = :provider_id AND account_id <> :account_id',
                array(':provider_id' => $providerId, ':account_id' => $accountId)
            );
            if (!$exists) {
                // Has pending appointment?
                $exists = SchedulingMail::model()->exists(
                    'provider_id=:providerId AND status=:status',
                    array(':providerId' => $providerId, ':status' => 'P')
                );
                if ($exists) {
                    $error = "This provider has pending appointment requests. "
                        . "Please either approve or reject them before proceeding.";
                    $this->addError('provider_id', $error);
                    if (Yii::app()->request->IsAjaxRequest) {
                        header("HTTP/1.0 409 Conflict");
                    } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                        Yii::app()->user->setFlash('error', $error);
                    }
                    return false;
                }
            }

            // remove all digital review hub related to the provider and the account
            AccountDeviceProvider::removeAccountDeviceProvider($accountId, $providerId, 7);

            // Has ReviewHub ?
            $arrAccountDeviceProvider = AccountDeviceProvider::getDeviceByAccountProvider($accountId, $providerId);
            if (count($arrAccountDeviceProvider)) {
                $error = "This provider has ReviewHub associations. "
                    . "Please remove the provider from the ReviewHub and try again.";
                $this->addError('provider_id', $error);
                if (Yii::app()->request->IsAjaxRequest) {
                    header("HTTP/1.0 409 Conflict");
                } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                    Yii::app()->user->setFlash(
                        'error',
                        "<strong>ERROR: </strong>This account and provider can't be "
                            . "unlinked: <a data-who='provider' data-id='$providerId' id='showErrorProviderPracticeUnlink' "
                            . "href='#'> See details</a>"
                    );
                }
                return false;
            }

            // delete associated widgets to practices of same account related to the provider
            $this->removeAssociatedWidgets();
        }

        // delete from EmrScheduleException
        $arrESE = EmrScheduleException::model()->findAll(
            'provider_id = :provider_id',
            array(':provider_id' => $this->provider_id)
        );
        if (!empty($arrESE)) {
            foreach ($arrESE as $eachRecord) {
                $eachRecord->delete();
            }
        }

        // delete from AccountProviderEMR
        $apEmr = AccountProviderEmr::model()->findAll(
            'provider_id = :provider_id AND account_id = :account_id',
            array(':provider_id' => $this->provider_id, ':account_id' => $this->account_id)
        );
        if (!empty($apEmr)) {
            foreach ($apEmr as $eachRecord) {
                $eachRecord->delete();
            }
        }

        // delete from ProviderPracticeSocialEnhance
        $providerPracticeSocialEnhance = ProviderPracticeSocialEnhance::model()->findAll(
            'account_id=:account_id AND provider_id=:provider_id',
            array(':account_id' => $this->account_id, ':provider_id' => $this->provider_id)
        );
        foreach ($providerPracticeSocialEnhance as $ppse) {
            $ppse->delete();
        }

        // delete from provider_practice_gmb
        $arrGmb = ProviderPracticeGmb::model()->findAll(
            'provider_id = :providerId AND account_id = :accountId',
            array(':providerId' => $providerId, ':accountId' => $accountId)
        );

        if (!empty($arrGmb)) {
            foreach ($arrGmb as $gmb) {
                $gmb->delete();
            }
        }

        // We cannot delete this record, so mark as disabled
        // set as enabled = false for waiting_room_setting
        $waitingRoomSetting = WaitingRoomSetting::model()->findAll(
            'account_id=:account_id AND provider_id=:provider_id AND enabled = 1',
            array(':account_id' => $this->account_id, ':provider_id' => $this->provider_id)
        );
        foreach ($waitingRoomSetting as $wrs) {
            $wrs->enabled = 0;
            $wrs->save();
        }

        // check if telemedicine was generated by the account we're removing from the provider
        // if so, disable telemedicine
        $sql = sprintf(
            "SELECT COUNT(1) AS total
                FROM provider_practice
                INNER JOIN practice_details
                ON provider_practice.practice_id = practice_details.practice_id
                AND practice_type_id = 5
                INNER JOIN account_practice
                ON provider_practice.practice_id = account_practice.practice_id
                AND account_practice.account_id = %d
                INNER JOIN account_provider
                ON provider_practice.provider_id = account_provider.provider_id
                AND account_provider.account_id = %d
                WHERE provider_practice.provider_id = %d;",
            $this->account_id,
            $this->account_id,
            $this->provider_id
        );
        $accountOwnsTelemedicinePractice = Yii::app()->db->createCommand($sql)->queryScalar();
        // did this account we're removing from the provider own the provider's telemedicine practice?
        if ($accountOwnsTelemedicinePractice) {
            // yes, then remove telemedicine as well, as we can't have booking without an account
            ProviderPractice::removeTelemedicinePractice($this->provider_id);
        }

        return parent::beforeDelete();
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        return '<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id . '" '
            . 'rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">'
            . $this->provider . '</a>';
    }

    /**
     * Remove features provided by account's organization if necesary
     * @return mixed
     */
    public function afterDelete()
    {
        // save event to the audit log
        AuditLog::create(
            'D',
            'Delete record',
            'account_provider',
            'ID: ' . $this->id . ', Provider ID: ' . $this->provider_id . ', Account ID: ' . $this->account_id,
            false,
            $this->account_id
        );

        // It is owner or manager account delete AccountProvider for all
        $currentAccountOrg = OrganizationAccount::model()->find(
            'account_id=:account_id',
            array(':account_id' => $this->account_id)
        );

        if (!empty($currentAccountOrg)) {
            if ($currentAccountOrg->connection == 'O' || $currentAccountOrg->connection == 'M') {
                $allAccounts = OrganizationAccount::model()->findAll(
                    'organization_id = :organization_id AND account_id != :account_id',
                    array(':organization_id' => $currentAccountOrg->organization_id, ':account_id' => $this->account_id)
                );

                if (!empty($allAccounts)) {
                    foreach ($allAccounts as $allAccount) {
                        $allAccountProvider = AccountProvider::model()->find(
                            'provider_id = :provider_id AND account_id = :account_id',
                            array(':provider_id' => $this->provider_id, ':account_id' => $allAccount->account_id)
                        );
                        if (!empty($allAccountProvider)) {
                            $allAccountProvider->delete();
                        }
                    }
                }
            }
        }

        $accountOrg = OrganizationAccount::model()->find(
            'account_id = :accountId AND connection = :connection',
            array(':accountId' => $this->account_id, ':connection' => "O")
        );
        if ($accountOrg) {
            $organizationFeatures = OrganizationFeature::model()->findAll(
                'organization_id = :organizationId AND provider_id = :providerId',
                array(':organizationId' => $accountOrg->organization_id, ':providerId' => $this->provider_id)
            );
            foreach ($organizationFeatures as $orgFeature) {
                $orgFeature->deleteFeature();
            }
        }

        // update claimed and nasp
        self::updateClaimed($this->provider_id);

        if (AccountProvider::model()->countByAttributes(array("provider_id" => $this->provider_id)) == 0) {
            // Set Online Scheduling as Inactive when provider is not associated to an account
            $providerDetailsUpdate = ProviderDetails::model()->find(
                'provider_id=:provider_id',
                array(':provider_id' => $this->provider_id)
            );
            if (!empty($providerDetailsUpdate)) {
                $providerDetailsUpdate->schedule_mail_enabled = 0;
                $providerDetailsUpdate->save();
            }
        }

        SqsComponent::sendMessage('updateLeadSpecialties', $this->account_id);

        SqsComponent::sendMessage('importAccountEmr', $this->account_id);

        // queue updating Number_of_Providers__c in salesforce if necessary
        $organizationId = Account::belongsToOrganization($this->account_id);
        if ($organizationId) {
            SqsComponent::sendMessage('updateSFOrganizationFeatures', $organizationId);
        }

        return parent::afterDelete();
    }

    /**
     * Validate account-provider is unique
     * Update salesforce
     * @return boolean
     */
    public function beforeSave()
    {

        $providerId = isset($this->provider_id) ? $this->provider_id : 0;
        $accountId = isset($this->account_id) ? $this->account_id : 0;

        if (empty($providerId) || empty($accountId)) {
            return false;
        }

        $this->date_added = TimeComponent::validateDateAdded($this->date_added);

        // check if association already exists
        $associationChanged = false;
        if (!$this->isNewRecord) {
            // on update, check if account_id or provider_id changed
            if (!empty($this->oldRecord->account_id) && !empty($this->oldRecord->provider_id)) {
                if (
                    $this->oldRecord->account_id != $this->account_id
                    ||
                    $this->oldRecord->provider_id != $this->provider_id
                ) {
                    $associationChanged = true;
                }
            }
        }

        if ($this->isNewRecord) {
            $this->research_status = 'Published';
        }

        if ($this->isNewRecord || $associationChanged) {

            // check if association already exists
            $exists = AccountProvider::model()->exists(
                'provider_id=:provider_id AND account_id=:account_id',
                array(':provider_id' => $providerId, ':account_id' => $accountId)
            );
            if ($exists) {
                // it does, reject
                $this->addError('provider_id_lookup', 'This provider is already associated to this account.');
                return false;
            }

            // queue updating Number_of_Providers__c in salesforce if necessary
            $organizationId = Account::belongsToOrganization($this->account_id);
            if ($organizationId) {
                SqsComponent::sendMessage('updateSFOrganizationFeatures', $organizationId);
            }
        }

        return parent::beforeSave();
    }

    /**
     * Once an account_provider entry is created, mark the provider_id as claimed
     * @return mixed
     */
    public function afterSave()
    {
        // save insert event to the audit log
        if ($this->isNewRecord) {
            AuditLog::create(
                'C',
                'Create record',
                'account_provider',
                'ID: ' . $this->id . ', Provider ID: ' . $this->provider_id . ', Account ID: ' . $this->account_id,
                false,
                $this->account_id
            );

            // set claimed value, this is not neccesary on updates
            self::updateClaimed($this->provider_id);
        } else {
            AuditLog::create(
                'U',
                'Update record',
                'account_provider',
                'ID: ' . $this->id . ', Provider ID: ' . $this->provider_id . ', Account ID: ' . $this->account_id,
                false,
                $this->account_id
            );
        }

        // Create account-provider for the managers
        AccountProvider::associateAccountProvider($this->account_id, $this->provider_id);

        SqsComponent::sendMessage('updateLeadSpecialties', $this->account_id);

        // emr_importation_disabled is set by ExternalDataImporter import process
        if (empty(Yii::app()->params['emr_importation_disabled'])) {
            SqsComponent::sendMessage('importAccountEmr', $this->account_id);
        }

        return parent::afterSave();
    }

    /**
     * Create relation between account->provider
     * @param int $accountId
     * @param int $providerId
     */
    public static function associateAccountProvider($accountId = 0, $providerId = 0)
    {

        if ($accountId == 0 || $providerId == 0) {
            return false;
        }

        $arrRelatedAccounts = OrganizationAccount::getManagers($accountId);
        foreach ($arrRelatedAccounts as $account) {
            // Add the provider to the owner and to all managers
            if ($account['connection_type'] == 'O' || $account['connection_type'] == 'M') {

                $accountProvider = new AccountProvider();
                $accountProvider->account_id = $account['id'];
                $accountProvider->provider_id = $providerId;
                // The beforeSave() check if exists or not
                $accountProvider->save();
            }
        }
    }

    /**
     * Update the salesforce lead associated with the account
     * with the specialty info for the associated providers.
     * @param int $accountId
     */
    public static function updateLeadSpecialties($accountId)
    {

        $account = Account::model()->findByPk($accountId);

        if (!empty($account->salesforce_lead_id)) {
            // fetch specialty info
            $sql = sprintf(
                "SELECT
                IF(COUNT(DISTINCT sub_specialty.generic_category)>1, 'Other', sub_specialty.generic_category) AS
                Specialty__c,
                GROUP_CONCAT(DISTINCT CONCAT(specialty.name,' (',sub_specialty.generic_category,')')) AS
                Specialty_Details__c
                FROM account_provider
                INNER JOIN provider_specialty
                    ON provider_specialty.provider_id= account_provider.provider_id
                INNER JOIN sub_specialty
                    ON sub_specialty.id= provider_specialty.sub_specialty_id
                INNER JOIN specialty
                    ON specialty.id = sub_specialty.specialty_id
                WHERE account_provider.account_id = %d;",
                $account->id
            );
            $specialtyInfo = Yii::app()->db->createCommand($sql)->queryRow();

            // cut specialty details info if it is longer than 255 characters.
            if (strlen($specialtyInfo["Specialty_Details__c"]) > 255) {
                $specialtyInfo["Specialty_Details__c"] =
                    StringComponent::truncate($specialtyInfo["Specialty_Details__c"], 251);
            }

            if (!empty($specialtyInfo['Specialty__c']) && !empty($specialtyInfo['Specialty_Details__c'])) {
                // update lead
                $result = Yii::app()->salesforce->updateLead($account->salesforce_lead_id, $specialtyInfo);
                if (isset($result['error'])) {
                    Yii::log($result['error'], "warning");
                }
            }
        }
    }

    /**
     * Get names of accounts other than $accountId that own $providerId
     * @param int $providerId
     * @param int $accountId
     * @return string
     */
    public function getOtherOwners($providerId, $accountId)
    {

        $sql = sprintf(
            "SELECT GROUP_CONCAT(CONCAT(account.first_name, ' ', account.last_name) SEPARATOR ', ')
            FROM account_provider
            INNER JOIN account
                ON account_provider.account_id = account.id
            WHERE provider_id = %d
                AND account_id != %d;",
            $providerId,
            $accountId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * This account has access to this provider?
     * @param integer $accountId
     * @param integer $providerId
     *
     * @return bool
     */
    public static function canAccess($accountId, $providerId)
    {
        $exists = AccountProvider::model()->cache(Yii::app()->params['cache_medium'])->find(
            "account_id=:account_id AND provider_id=:provider_id",
            [":account_id" => $accountId, ":provider_id" => $providerId]
        );
        if (!empty($exists)) {
            return true;
        }
        return false;
    }

    /**
     * Get an account's providers
     * @param int $accountId
     * @param array $accountInfo
     * @return array
     */
    public static function getAccountProviders($accountId = 0, $accountInfo = array())
    {

        // This code was replaced by this new method
        $data = [
            'account_id' => $accountId,
            'page' => 1,
            'page_size' => 1000,
        ];
        $result = AccountProvider::getProviders($data);
        return $result['accountProviders'];
    }

    /**
     * Get providers in the given account with flags that indicate whether they:
     * - have a scheduling telemedicine practice
     * - have office hours in that practice
     *
     * @param int $accountId
     * @return array
     */
    public static function getAccountTelemedicineProviders($accountId)
    {

        $sql = sprintf(
            "SELECT provider.id, provider.prefix, provider.first_name, provider.last_name,
            ppl.url as third_party_booking_url,orf.active as third_party_url_feature,
            IF(MAX(practice_details.practice_id) IS NOT NULL, true, false) AS telemedicine_provider,
            IF (MAX(practice_details.practice_id) IS NOT NULL
                AND MAX(pps.id) IS NOT NULL, true, false) AS telemedicine_schedule
            FROM provider
            INNER JOIN account_provider
            ON account_provider.provider_id = provider.id
            LEFT JOIN provider_practice
            ON provider.id = provider_practice.provider_id

            LEFT JOIN practice_details
            ON provider_practice.practice_id = practice_details.practice_id
            AND practice_type_id = 5

            LEFT JOIN provider_practice_schedule AS pps
            ON provider_practice.id = pps.provider_practice_id

            LEFT JOIN (SELECT * FROM provider_practice_listing WHERE listing_type_id IN (156,157)) ppl
                    ON provider.id = ppl.provider_id
            LEFT JOIN (SELECT * FROM organization_feature WHERE feature_id = 26 AND active = 1) orf
                    ON provider.id = orf.provider_id

            WHERE account_id = %d
            GROUP BY provider.id
            ORDER BY first_name, last_name, practice_details.id IS NOT NULL;",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get an account's provider research status
     * @param int $accountId
     * @param int $providerId
     * @param bool $useCache
     * @return array
     */
    public static function getProviderResearchStatus($accountId = 0, $providerId = 0, $useCache = false)
    {
        if ($accountId == 0 || $providerId == 0) {
            return false;
        }

        $sql = sprintf(
            "SELECT research_status
            FROM account_provider
            WHERE account_id = %d
            AND provider_id = %d
            LIMIT 1;",
            $accountId,
            $providerId
        );

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_medium'] : 0;
        $result = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryRow();
        return !empty($result['research_status']) ? $result['research_status'] : '';
    }

    /**
     * Get an account's providers basic data
     * @param array $data
     * [
     *    'account_id' => 0,
     *    'use_cache' => 1,
     *    'page' => 1,
     *    'page_size' => 50
     * ]
     * @return array
     */
    public static function getProviders($data = array())
    {
        // Process params
        $accountId = !empty($data['account_id']) ? $data['account_id'] : 0;
        $useCache = !empty($data['use_cache']) ? $data['use_cache'] : false;
        $page = !empty($data['page']) ? $data['page'] : 1;
        $full = isset($data['full']) ? $data['full'] : 1;

        // page has a negative number?
        $page = $page < 1 ? 1 : $page;

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;

        // count the providers related to this account
        $totalProviders = AccountProvider::model()
            ->cache($cacheTime)
            ->count("account_id=:account_id", [":account_id" => $accountId]);

        // Prepare the offset
        $pageSize = !empty($data['page_size']) ? $data['page_size'] : $totalProviders;
        $offset = ($page - 1) * $pageSize;

        if ($full == 1) {
            $sql = sprintf(
                "SELECT p.id, p.id as provider_id, p.localization_id, p.country_id,
                p.first_last, p.first_m_last, p.first_middle_last,
                p.npi_id, p.gender, p.credential_id, p.prefix, p.first_name, p.middle_name, p.last_name,
                CONCAT(p.prefix, ' ', p.first_name, ' ', p.last_name) AS `name`,
                p.suffix, p.docscore, p.accepts_new_patients, p.year_began_practicing, p.vanity_specialty,
                p.additional_specialty_information, p.brief_bio, p.bio, p.fees, p.avg_rating, p.friendly_url,
                p.photo_path, p.date_added, p.date_updated, p.umd_id, p.date_umd_updated, p.featured, p.claimed,
                p.suspended, account_provider.research_status,
                IF(provider_details.schedule_mail_enabled IS NULL, 0, provider_details.schedule_mail_enabled)
                    AS schedule_mail_enabled,
                GROUP_CONCAT(
                    DISTINCT CONCAT(pp.practice_id,'-',pp.primary_location,'-',pp.rank_order)
                    ORDER BY pp.rank_order ASC SEPARATOR ',') AS at_practices,
                account_provider.account_id, account_provider.all_practices,
                account_provider.id AS account_provider_id
                FROM `provider` AS p
                LEFT JOIN account_provider
                    ON account_provider.provider_id = p.id
                LEFT JOIN provider_details
                    ON p.id = provider_details.provider_id
                LEFT JOIN provider_practice AS pp
                    ON p.id = pp.provider_id
                WHERE account_provider.account_id = %d
                GROUP BY p.id
                ORDER BY last_name ASC, first_name ASC
                LIMIT %d, %d;",
                $accountId,
                $offset,
                $pageSize
            );
        } else {
            $sql = sprintf(
                "SELECT p.id, p.id as provider_id,
                p.gender, p.credential_id, p.prefix, p.first_name, p.middle_name, p.last_name,
                CONCAT(p.prefix, ' ', p.first_name, ' ', p.last_name) AS `name`,
                p.suffix, p.suspended
                FROM `account_provider`
                LEFT JOIN `provider` AS p
                    ON account_provider.provider_id = p.id
                WHERE account_provider.account_id = %d
                ORDER BY last_name ASC, first_name ASC",
                $accountId
            );
        }
        $accountProviders = array();
        $accountData['has_online_scheduling'] = 0;
        $tmpAccountProviders = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();

        if (!empty($tmpAccountProviders)) {
            foreach ($tmpAccountProviders as $value) {

                if ($full == 1) {
                    // Adding profile pic
                    $value['real_logo_path'] = AssetsComponent::generateImgSrc(
                        $value['photo_path'],
                        'providers',
                        'L',
                        true
                    );

                    // Adding provider specialties
                    $value['provider_specialties'] = ProviderSpecialty::getDetails($value['id']);

                    // Check if online scheduling is enabled for at least one provider
                    if ($value['schedule_mail_enabled'] == 1) {
                        $accountData['has_online_scheduling'] = 1;
                    }
                }

                $accountProviders[$value['id']] = $value;
            }
        }
        unset($tmpAccountProviders);

        if ($full == 0) {
            return $accountProviders;
        }

        return [
            'has_online_scheduling' => $accountData['has_online_scheduling'],
            'accountProviders' => $accountProviders,
            'provider_current_page' => $page,
            'provider_page_size' => $pageSize,
            'provider_total_records' => $totalProviders
        ];
    }

    /**
     * Get an account's providers grouped in a single field
     * @param int $accountId
     * @return string
     */
    public static function getGroupedAccountProviders($accountId)
    {
        $sql = sprintf(
            "SELECT SQL_NO_CACHE GROUP_CONCAT(provider_id) AS providers
            FROM account_provider
            WHERE account_id = %d;",
            $accountId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get an account's providers practices
     * @param int $accountId
     * @return array
     */
    public static function getAccountProvidersPractices($accountId)
    {
        $sql = sprintf(
            "SELECT provider_practice.provider_id AS provider_id, practice_id AS id
            FROM provider_practice
            INNER JOIN account_provider
                ON provider_practice.provider_id = account_provider.provider_id
            WHERE account_id = %d;",
            $accountId
        );
        return Yii::app()->dbRO->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get an account's providers practices for API
     * @param int $accountId
     * @return array
     */
    public static function getAccountProvidersPracticesAPI($accountId)
    {
        $sql = sprintf(
            "SELECT pp.id, pp.provider_id, pp.practice_id,
            pp.primary_location, pp.phone_number, pp.fax_number, pp.email, pp.rank_order, pp.location_details,
            provider.first_m_last AS provider_name, practice.name AS practice_name
            FROM provider_practice AS pp
            INNER JOIN account_provider AS apro
                ON pp.provider_id = apro.provider_id AND apro.account_id = %d
            INNER JOIN account_practice AS apra
                ON pp.practice_id = apra.practice_id  AND apra.account_id = %d
            LEFT JOIN provider
                ON provider.id = pp.provider_id
            LEFT JOIN practice
                ON practice.id = pp.practice_id
            LEFT JOIN practice_details
                ON practice.id = practice_details.practice_id
            WHERE practice_type_id != 5;",
            $accountId,
            $accountId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Get an account's providers practices
     * @param int $accountId
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function existsAccountProvidersPractices($accountId, $providerId, $practiceId)
    {
        $sql = sprintf(
            "SELECT pp.id
            FROM provider_practice AS pp
            INNER JOIN account_provider AS apro
                ON pp.provider_id = apro.provider_id AND apro.account_id = %d
            INNER JOIN account_practice AS apra
                ON pp.practice_id = apra.practice_id  AND apra.account_id = %d
            WHERE apro.provider_id = %d
            AND apra.practice_id = %d;",
            $accountId,
            $accountId,
            $providerId,
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * PADM: Get an account's providers as long as they have ratings
     * @param int $accountId
     * @return array
     */
    public static function getAccountProvidersWithRatings($accountId = 0)
    {

        if (empty($accountId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT DISTINCT(ap.provider_id)
            FROM account_provider AS ap
            INNER JOIN provider_rating AS pr
                ON ap.provider_id = pr.provider_id
            WHERE ap.account_id = %d
                AND pr.status IN ('A', 'P')
            ORDER BY ap.provider_id;",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Gets an account's providers as used in Checkout
     * @param int $accountId
     * @return array
     */
    public static function getAccountProvidersForCheckout($accountId)
    {
        $sql = sprintf(
            "SELECT IF(provider_practice.id IS NULL,'1','0') AS practice_order,
            IF(provider_practice.id IS NULL, 'Without Practice',practice.name) AS practice,
            provider.prefix, provider.first_name, provider.last_name, credential.title,
            provider.id, provider.featured
            FROM provider
            INNER JOIN credential
                ON credential.id = provider.credential_id
            INNER JOIN account_provider
                ON provider.id = account_provider.provider_id
            LEFT JOIN provider_practice
                ON provider_practice.provider_id = provider.id
                AND provider_practice.primary_location = 1
            LEFT JOIN practice
                ON provider_practice.practice_id = practice.id
            WHERE account_id = %d
            ORDER BY practice_order, practice;",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get all account linked to a provider
     * @param int $providerId
     * @return array
     */
    public static function getAccountsByProvider($providerId = 0)
    {
        if ($providerId == 0) {
            return array();
        }
        $sql = sprintf(
            "SELECT DISTINCT account_id
            FROM account_provider
            WHERE provider_id = %d;",
            $providerId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get email of accounts related to given provider
     * @param int $providerId
     * @param bool $usecache
     * @return arr
     */
    public static function getProviderAccountEmail($providerId = 0, $useCache = true)
    {
        if ($providerId == 0) {
            return false;
        }

        $cacheTime = ($useCache) ? Yii::app()->params['cache_medium'] : 0;

        $sql = sprintf(
            "SELECT ap.account_id, a.email
            FROM account_provider AS ap
            INNER JOIN account a
                ON ap.account_id = a.id
            WHERE ap.provider_id = %d and a.email != ''; ",
            $providerId
        );
        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Return the list of providers for this account
     * @param int $accountId
     * @return array
     */
    public static function getList($accountId = 0)
    {
        $sql = sprintf("SELECT provider_id FROM account_provider WHERE account_id = %d;", $accountId);
        $arrProviders = Yii::app()->db->createCommand($sql)->queryAll();
        $providersList = '';
        if (!empty($arrProviders)) {
            foreach ($arrProviders as $provider) {
                $providersList .= $provider['provider_id'] . ',';
            }
        }
        $providersList = rtrim($providersList, ',');

        return $providersList;
    }

    /**
     * Get an account's providers
     * @param int $accountId
     * @param int $research
     * @return array
     */
    public static function getAccountProvidersResearch($accountId, $research)
    {
        $researchStr = "";

        if ($research == 1) {
            $researchStr = " AND (
                account_provider.research_status IN ('Pending', 'Rejected')
            ) ";
        }

        $sql = sprintf(
            "SELECT provider.id, provider.featured,
            CONCAT(provider.prefix, ' ', provider.first_name, ' ', provider.last_name) AS name,
            account_provider.research_status
            FROM account_provider
            INNER JOIN provider
                ON provider.id = account_provider.provider_id
            WHERE account_provider.account_id = %d
                %s
            GROUP BY provider_id
            ORDER BY name;",
            $accountId,
            $researchStr
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get all providers for this account
     * @param int $accountId
     * @return object
     */
    public static function getAllByAccountId($accountId = 0)
    {
        $ap = AccountProvider::model()->findAll(
            "account_id = :account_id",
            array(":account_id" => $accountId)
        );
        return $ap;
    }

    /**
     * Get Account/EMR configuration
     *
     * @deprecated No longer used by internal code and not recommended.
     *
     * @param int $accountId
     * @return array
     */
    public static function getAccountEmrConfig($accountId = 0)
    {
        $accountEmr = AccountEmr::getByAccountId($accountId);

        $options['origin_db'] = '';
        if (!empty($accountEmr->options)) {
            $options = json_decode($accountEmr->options, true);
        }

        $arrEmrTypes = EmrType::model()->getAllEmrTypes();

        // import EMR settings into the ingestion server
        if (isset($settingsSaved)) {
            AccountEmr::importAccountEmr($accountId);
        }

        $accountProviders = AccountProvider::getAccountProviders($accountId);

        $accountProviderEmr = AccountProviderEmr::getAllByAccountId($accountId);

        $accountPractices = AccountPractice::getAccountPractices($accountId);
        $accountPracticeEmr = AccountPracticeEmr::getAllByAccountId($accountId);

        // Check Facebook Integration
        $seResult = SocialEnhanceComponent::checkFacebookIntegration(
            $accountId,
            $accountProviderEmr,
            $accountPracticeEmr,
            true
        );
        $hasFacebookIntegration = $seResult[0];
        $accountProviderEmr = $seResult[1];
        $accountPracticeEmr = $seResult[2];

        // Is involved in a Widget?
        $arrWidget = Widget::model()->getWidgetsByAccountId($accountId);
        foreach ($arrWidget as $v) {
            foreach ($accountProviderEmr as $key => $value) {
                $accountProviderEmr[$key]['is_in_widget'] = 0;
                if ($value['provider_id'] == $v['provider_id']) {
                    $accountProviderEmr[$key]['is_in_widget'] = 1;
                    break;
                }
            }
            foreach ($accountPracticeEmr as $key => $value) {
                $accountPracticeEmr[$key]['is_in_widget'] = 0;
                if ($value['practice_id'] == $v['practice_id']) {
                    $accountPracticeEmr[$key]['is_in_widget'] = 1;
                    break;
                }
            }
        }


        // has Mi7Integration ?
        $mi7Integration = $readOnlyIntegration = $syncScheduleType = $allowOverbooking = false;
        $emrTypeId = $accountEmr['emr_type_id'];
        $btnSubmitCaption = "Enable integration";
        $regressionCounter = 0;
        $aditionalInfo = '';

        if ($emrTypeId > 0) {
            foreach ($arrEmrTypes as $eachEmr) {
                if ($eachEmr['id'] == $emrTypeId) {
                    $readOnlyIntegration = ($eachEmr['read_only_integration'] == 1) ? true : false;
                    $mi7Integration = ($eachEmr['mi7_integration'] == 1) ? true : false;
                    $syncScheduleType = ($eachEmr['sync_provider_practice_schedule'] == 1) ? true : false;
                    $allowOverbooking = ($eachEmr['overbooking'] == 1) ? true : false;
                    break;
                }
            }

            // Submit Text
            $cnt = AccountPracticeEmr::countAccountPractices($accountId);
            if ($cnt > 0) {
                $cnt1 = AccountPracticeEmr::countPractices($accountId, $accountEmr->id);
                if ($cnt1 > 0) {
                    $cnt1 = AccountProviderEmr::countProviders($accountId, $accountEmr->id);
                    if ($cnt1 > 0) {
                        $btnSubmitCaption = "Save Settings";
                    } else {
                        $regressionCounter = EhrCommunicationQueue::waitingProviderList($accountEmr->id);
                        if ($regressionCounter > 0) {
                            $regressionCounter = 10; // reload after 10 seconds
                            $btnSubmitCaption = "Waiting for providers list, checking again in 5 seconds";
                        } else {
                            $btnSubmitCaption = "Get providers list from EHR";
                        }
                    }
                } else {

                    if ($mi7Integration) {
                        $aditionalInfo = 'display_activate_base_info';
                    }
                    $btnSubmitCaption = "Match practices and providers";
                }
            } else {
                $btnSubmitCaption = "Initialize";
                if ($mi7Integration) {
                    $aditionalInfo = 'display_activate_base_info';
                    // check if we're an admin only if we're not in cli
                    if (php_sapi_name() != 'cli' && !empty(Yii::app()->session['admin_account_id'])) {
                        $cnt = EhrCommunicationQueue::model()->count(
                            "emr_id = :emr_id
                                AND (message_type = :message_providers OR message_type = :message_practices)
                                AND processed = :processed",
                            array(
                                ":emr_id" => $accountEmr->id,
                                ":message_providers" => Mi7Importer::$GET_PROVIDER_LIST,
                                ":message_practices" => Mi7Importer::$GET_PRACTICE_LIST,
                                ":processed" => 0
                            )
                        );

                        if ($cnt) {
                            $regressionCounter = 10; // reload after 10 seconds
                            $btnSubmitCaption = "Initialization in progress, checking again in 10 seconds";
                        }
                    } else {
                        $btnSubmitCaption = "Match practices and providers";
                    }
                }
            }
        }

        // Create array with practice for providers
        $providersForPractices = array();
        foreach ($accountProviders as $ap) {
            $providerId = $ap['provider_id'];
            $providersPractice = ProviderPractice::getProvidersPractices($providerId, $accountId, 'workingOn');
            if (!empty($providersPractice)) {
                foreach ($providersPractice as $eachPractice) {
                    $providersForPractices[$eachPractice['id']][$providerId] = $eachPractice['provider_name'];
                }
            }
        }
    }


    /**
     * Check if the provider or related account are in research mode
     * @param int $providerId
     * @return bool
     */
    public static function isInResearchMode($providerId = null)
    {
        if (is_null($providerId)) {
            return false;
        }
        $sql = sprintf(
            "SELECT account_id, research_mode, research_status
            FROM account_provider
            LEFT JOIN account
                ON account_provider.account_id = account.id
            WHERE provider_id = %d",
            $providerId
        );
        $data = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();

        $return = false;
        if (!empty($data)) {
            foreach ($data as $v) {
                if ($v['research_mode'] != 0 || $v['research_status'] != 'Published') {
                    $return = true;
                    break;
                }
            }
        }
        return $return;
    }

    /**
     * Unlink a provider from an account (account_provider)
     * @param int $accountId
     * @param int $providerId
     * @param int $practiceId
     * @return bool
     */
    public static function unlinkProvider($accountId = 0, $providerId = 0, $force = false, $practiceId = 0)
    {
        $result['success'] = true;
        $result['error'] = array();

        ETLComponent::startTransaction();
        $ownerAccountId = 0;
        $lstManagers = '';
        $canUnlink = false;
        $settingsSaved = true;
        Yii::app()->user->id = $accountId;
        $arrManagers = OrganizationAccount::getManagers($accountId);
        foreach ($arrManagers as $manager) {
            $lstManagers .= $manager['id'] . ',';
            if ($manager['id'] == $accountId) {
                if ($manager['connection_type'] == 'M' || $manager['connection_type'] == 'O') {
                    $canUnlink = true;
                }
            }
            if ($manager['connection_type'] == 'O') {
                $ownerAccountId = $manager['id'];
            }
        }
        $lstManagers = rtrim($lstManagers, ',');

        // if the logged user, is an staff account, return false
        if (!Common::isTrue($canUnlink)) {
            $result['success'] = false;
            $result['error'][]['provider_id'][] = "ERROR: You don't have permissions to unlink a provider";
            Yii::app()->user->setFlash(
                'error',
                "<strong>ERROR: </strong>You don't have permissions to unlink a provider"
            );
            $settingsSaved = false;
            return $result;
        }

        $accountPractices = array();
        if ($practiceId > 0) {
            // Unlink for this practice
            $accountPractices = AccountPractice::model()->findAll(
                'account_id IN (:lstManagers) AND practice_id =:practice_id',
                array(':lstManagers' => $lstManagers, ':practice_id' => $practiceId)
            );
        }

        // Unlink for the account
        if (Common::isTrue($force)) {
            ProviderPracticeWidget::deleteWidget($ownerAccountId, $providerId, $practiceId);
            AccountDevice::deleteReviewHub($ownerAccountId, $providerId, true);
            AccountProviderEmr::deleteRecord($ownerAccountId, $providerId);
        } else {

            $arrWidget = ProviderPracticeWidget::getWidgetFromProviderAccount(
                $ownerAccountId,
                $providerId,
                true,
                'ALL'
            );

            if ($arrWidget) {
                $error = "This provider has associated widgets. Please remove the widgets and try again.";
                $result['success'] = false;
                $result['error'][]['provider_id'][] = $error;
                $settingsSaved = false;
                return $result;
            }
        }

        // delete provider_practice
        $cpnValidationError = false;
        if (!empty($accountPractices)) {
            foreach ($accountPractices as $accountPractice) {
                $providerPractice = ProviderPractice::model()->find(
                    'practice_id=:practice_id AND provider_id=:provider_id',
                    array(':practice_id' => $accountPractice->practice_id, ':provider_id' => $providerId)
                );

                if (!empty($providerPractice)) {
                    if (!$providerPractice->delete()) {
                        $errorDetected = $providerPractice->getErrors();
                        $result['error'][] = $errorDetected;
                        if (isset($errorDetected['cpn_validation'])) {
                            $cpnValidationError = true;
                        }
                        $settingsSaved = false;
                    }
                }
            }
        }

        if (Common::isTrue($cpnValidationError)) {
            $errorTxt = "This provider has an active Companion installation. "
                . "Please deactivate it before unlinking the provider.";
            Yii::app()->user->setFlash('error', $errorTxt);
        } elseif (!$settingsSaved) {
            Yii::app()->user->setFlash(
                'error',
                "<strong>ERROR: </strong>This account and practice can't be
                unlinked: <a data-who='provider' data-id='" . $providerId . "'
                id='showErrorProviderPracticeUnlink' href='#'> See details</a>"
            );
        }
        $result['success'] = $settingsSaved;

        if ($practiceId == 0 && $settingsSaved) {

            foreach ($arrManagers as $manager) {
                // Unlink the provider for the account
                $accountProvider = AccountProvider::model()->find(
                    'account_id = :accountId AND provider_id = :provider_id',
                    array(':accountId' => $manager['id'], ':provider_id' => $providerId)
                );

                if (!empty($accountProvider)) {
                    // delete account_provider
                    if ($accountProvider->delete()) {
                        // save to audit log and notify changes to phabricator
                        $objProvider = Provider::model()
                            ->cache(Yii::app()->params['cache_medium'])
                            ->findByPk($providerId);

                        AuditLog::create(
                            'D',
                            'Unlinked provider #' . $providerId
                                . ' (' . $objProvider->first_name . ' ' . $objProvider->last_name . ') from account',
                            true
                        );
                    } else {
                        $result['success'] = false;
                        $result['error'][]['provider_id'] = $accountProvider->getErrors();
                    }
                }
            }
            SqsComponent::sendMessage('importAccountEmr', $ownerAccountId);
        }

        ETLComponent::commitTransaction();
        return $result;
    }

    /**
     * Get others providers associated to a list of practices that are not managed by a given account
     * @param array $arrPractices
     * @param int $accountId
     * @return string
     */
    public static function getOtherProviders($arrPractices = null, $accountId = null)
    {
        if (empty($arrPractices) || empty($accountId)) {
            return '';
        }

        $arrOtherProviders = array();

        $tmpOtherProviders = Provider::getProvidersAssociatedToOtherAccounts($accountId, $arrPractices, true);
        reset($tmpOtherProviders);
        $key = key($tmpOtherProviders);
        unset($tmpOtherProviders[$key]);
        if (!empty($tmpOtherProviders)) {
            foreach ($tmpOtherProviders as $tmp) {
                $providerId = $tmp->id;
                $arrOtherProviders[$providerId] = $tmp->attributes;

                // Adding related practices
                $arrOtherProviders[$providerId]['practices'] = '';
                $practices = ProviderPractice::model()->findAll(
                    'provider_id=:provider_id',
                    array(':provider_id' => $providerId)
                );
                if (!empty($practices)) {
                    $dataPractice = '';
                    foreach ($practices as $p) {
                        if ($dataPractice == '') {
                            $dataPractice = $p->practice_id;
                        } else {
                            $dataPractice .= ',' . $p->practice_id;
                        }
                    }
                    $arrOtherProviders[$providerId]['practices'] = $dataPractice;
                }

                if (!empty($tmp->photo_path)) {
                    $arrOtherProviders[$providerId]['photo_path'] = AssetsComponent::generateImgSrc(
                        $tmp->photo_path,
                        'providers',
                        'M',
                        true,
                        true
                    );
                }
                $arrOtherProviders[$providerId]['specialty'] = Provider::model()->getSpecialtySubspecialty($providerId);
            }
        }
        return $arrOtherProviders;
    }

    /**
     * This will update the claimed value on the specified provider, also will set the nasp_date to NULL if claimed
     * is different than Provider::CLAIMED_PAYING_ACCOUNT
     *
     * Return the new claimed value or false if the provider was not found.
     *
     * @param int $providerId
     * @return int
     */
    public static function updateClaimed($providerId)
    {
        // try to find the provider
        $provider = Provider::model()->findByPk($providerId);

        // if the provider wasn't found return false
        if (empty($provider)) {
            return false;
        }

        // default
        $claimed = null;

        // find if the provider is related at least to one account
        $hasAccount = AccountProvider::model()->exists('provider_id = :provider_id', [':provider_id' => $providerId]);

        if (empty($hasAccount)) {
            // the provider is not related to any account
            $claimed = Provider::CLAIMED_NO_ACCOUNT;
        } else {
            // check if the provider is related at least to an active or paused organization
            $query = Yii::app()->db->createCommand(
                'SELECT ap.id
                FROM account_provider ap
                JOIN organization_account oa ON oa.account_id = ap.account_id
                JOIN organization o ON o.id = oa.organization_id AND oa.connection = "O"
                WHERE
                    ap.provider_id = :provider_id
                    AND (o.status = "A" OR o.status = "P")
                LIMIT 1'
            );
            $query->bindValue(':provider_id', $providerId);
            $row = $query->queryRow();

            // set claimed
            if ($row) {
                $claimed = Provider::CLAIMED_PAYING_ACCOUNT;
            } else {
                $claimed = Provider::CLAIMED_FREEMIUM_ACCOUNT;
            }
        }

        // flag to determine if must save the provider
        $mustSave = false;

        // if claimed is different update it
        if ($provider->claimed != $claimed) {
            $provider->claimed = $claimed;
            $mustSave = true;
        }

        // nasp date should be null if claimed is other than CLAIMED_PAYING_ACCOUNT
        if ($claimed != Provider::CLAIMED_PAYING_ACCOUNT && $provider->nasp_date != null) {
            $provider->nasp_date = null;
            $mustSave = true;
        }

        // update the record only if were changes for performance
        if ($mustSave) {
            $provider->save();
        }

        // returns the new claimed value
        return $claimed;
    }

    /**
     * Get providers for Reputation scans by account ID
     *
     * @params int $providerId
     *
     * @return array
     */
    public function getProvidersForRepinsights($accountId, $providerId = null)
    {
        if ($providerId) {
            $accountProviders = AccountProvider::model()->findAll(
                'account_id = :account_id AND provider_id = :provider_id',
                [
                    ':account_id' => $accountId,
                    ':provider_id' => $providerId
                ]
            );
        } else {
            $accountProviders = AccountProvider::model()->findAll(
                'account_id = :account_id',
                [':account_id' => $accountId]
            );
        }

        $result = [];
        foreach ($accountProviders as $accountProvider) {
            $provider = $accountProvider->provider;
            $account = $accountProvider->account;

            $item = [];
            $item['NPI'] = $provider->npi_id;
            if ($account) {
                $item['PDI_PartnerID'] = $account->partner_site_id;
            }
            $item['PDI_ProviderID'] = $provider->id;
            $item['FamilyName'] = $provider->last_name;
            $item['GivenName'] = $provider->first_name;
            $item['MiddleName'] = $provider->middle_name;
            $item['GenerationalSuffix'] = '';
            $item['Degree'] = $provider->credential->title;

            $item['Specialties'] = [];
            $specialties = ProviderSpecialty::getDetails($provider->id);
            foreach ($specialties as $specialty) {
                $item['Specialties'][] = ['Specialty' => $specialty['specialty']];
            }

            // add additional specialty from NPPES
            $nppesSpecialty = ProviderSpecialty::getFromNppes($provider->npi_id);
            if ($nppesSpecialty) {
                foreach ($nppesSpecialty as $nps) {
                    $item['Specialties'][] = ['Specialty' => $nps['specialty']];
                }
            }

            // remove duplicate values and guarantee index cardinality
            $item['Specialties'] = array_values(array_unique($item['Specialties'], SORT_REGULAR));

            $item['Addresses'] = [];
            $practiceId = Provider::getPrimaryPractice($provider->id);

            if (!empty($practiceId)) {
                $practice = Practice::model()->findByPk($practiceId);

                $address = [];

                $address['AddressLabel'] = $practice->name;
                $address['Line1'] = $practice->address;
                $address['Line2'] = $practice->address_2;
                $address['Line3'] = '';
                $address['Line4'] = '';

                $location = $practice->location;
                if ($location) {
                    $address['City'] = $location->city->name;
                    $address['State'] = $location->city->state->abbreviation;
                    $address['Zip'] = $location->zipcode;
                    $address['LocationID'] = $practice->id;
                }

                $item['Addresses'][] = $address;
            }

            $result[] = $item;
        }

        return $result;
    }

    /**
     * Update profile population status
     *
     * @params integer $providerId
     * @params string $newStatus
     * @params integer $accountId
     * @params integer $adminAccountId
     */
    public static function updateProfilePopulationStatus($providerId, $newStatus, $accountId, $adminAccountId)
    {
        $accountProvider = AccountProvider::model()->find(
            'account_id=:account_id AND provider_id=:providerId',
            array(':account_id' => $accountId, ':providerId' => $providerId)
        );

        $prevStatus = $accountProvider->research_status;
        $accountProvider->research_status = $newStatus;
        $result = $accountProvider->save();

        // save in researh mode log
        AuditLog::saveResearchModeLog(
            $accountId,
            'provider',
            $providerId,
            $newStatus,
            $prevStatus,
            'Profile Population Mode',
            $adminAccountId
        );

        return $result;
    }
    public function getProviderAccount($providerId)
    {
        $result = $this->with('account', 'provider')
            ->find('provider_id = :provider_id', [':provider_id' => $providerId]);

        if ($result) {
            return $result->account;
        }

        return null;
    }

    /**
     * Remove associated widgets to practices of same account related to a given provider.
     * @return void
     */
    protected function removeAssociatedWidgets()
    {
        // get practices of same account where the providers works
        $arrPractices = ProviderPractice::getProvidersPractices($this->provider_id, $this->account_id, 'workingOn');

        // nothing found? exit
        if (empty($arrPractices)) {
            return;
        }

        // loop by each practice
        foreach ($arrPractices as $eachPractice) {
            $practiceId = $eachPractice['id'];

            // this relations has any widget?
            Widget::clearDraftWidgets($this->account_id, $this->provider_id, $practiceId);

            // get widgets
            $arrProviderPracticeWidget = ProviderPracticeWidget::getWidgetFromProviderPracticeAccount(
                $this->provider_id,
                $practiceId,
                $this->account_id
            );

            // nothing found? continue with next practice
            if (empty($arrProviderPracticeWidget)) {
                continue;
            }

            // remove each widget
            foreach ($arrProviderPracticeWidget as $ppw) {
                // load provider_practice_widget
                $widget = ProviderPracticeWidget::model()->findByPk($ppw['id']);

                // widget not found? continue with next
                if (empty($widget)) {
                    continue;
                }

                AuditLog::create(
                    'D',
                    'Removed provider_practice_widget #' . $widget->id,
                    'provider_practice_widget',
                    'widget_id: ' . $widget->provider_id . ', ' . 'provider_id: ' . $widget->provider_id . ', ' .
                        'practice_id: ' . $widget->practice_id
                );

                // delete it
                $widget->delete();
            }
        }
    }
    /**
     * Get all providers linked to an account
     * @param int $accountId
     * @return array
     */
    public static function getProvidersConcatByAccount($accountId = 0)
    {
        if ($accountId == 0) {
            return array();
        }
        // extend the maximum of characters in group_concat
        Yii::app()->db->createCommand('SET SESSION group_concat_max_len = 1000000;')->execute();
        Yii::app()->db->createCommand('SET @@group_concat_max_len = 1000000;')->execute();

        $sql = sprintf(
            "SELECT GROUP_CONCAT(provider_id) AS providers
            FROM account_provider
            WHERE account_id = %d;",
            $accountId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
    }
}
