<?php

Yii::import('application.modules.core_models.models._base.BaseAccountDirectory');

class AccountDirectory extends BaseAccountDirectory
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getAccountDirectoryByToken($token)
    {
        // try to find a directory setting by their name
        return AccountDirectory::model()->find(
            "access_token = :access_token",
            [
                ':access_token' => $token
            ]
        );
    }
}
