<?php

Yii::import('application.modules.core_models.models._base.BaseAccountDeviceProvider');

class AccountDeviceProvider extends BaseAccountDeviceProvider
{

    public $account;
    public $device;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Declares the validation rules.
     * @return array
     */
    public function rules()
    {
        return array(
            array('account_device_id, provider_id', 'required'),
            array('account_device_id', 'numerical', 'integerOnly' => true),
            array('id, account_device_id, provider_id, accountDevice, device, account', 'safe', 'on' => 'search')
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'account_device_id' => null,
            'provider_id' => Yii::t('app', 'Provider'),
            'accountDevice' => null,
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->with = array('provider', 'accountDevice', 'accountDevice.device', 'accountDevice.account');

        if (!intval($this->account_device_id) && is_string($this->account_device_id) &&
            strlen($this->account_device_id) > 0) {
            $criteria->compare('accountDevice.program_title', $this->account_device_id, true);
        } elseif (intval($this->account_device_id)) {
            $criteria->compare('account_device_id', $this->account_device_id);
        }

        if (!intval($this->provider_id) && is_string($this->provider_id) && strlen($this->provider_id) > 0) {
            $criteria->compare('provider.first_last', $this->provider_id, true);
        } elseif (intval($this->provider_id)) {
            $criteria->compare('provider_id', $this->provider_id);
        }

        if (!intval($this->account) && is_string($this->account) && strlen($this->account) > 0) {
            $criteria->addSearchCondition('concat(account.first_name, " ", account.last_name)', $this->account);
        } elseif (intval($this->account)) {
            $criteria->compare('account_id', $this->account);
        }

        if (!intval($this->device) && is_string($this->device) && strlen($this->device) > 0) {
            $criteria->compare('device.nickname', $this->device, true);
        } elseif (intval($this->device)) {
            $criteria->compare('device_id', $this->device);
        }

        $criteria->compare('accountDevice.program_title', $this->accountDevice, true);

        $sort = new CSort();
        $sort->attributes = array(
            'device' => array(
                'asc' => 'device.nickname',
                'desc' => 'device.nickname desc',
            ),
            'account' => array(
                'asc' => 'account.first_name',
                'desc' => 'account.first_name desc',
            ),
        );
        $sort->defaultOrder = 'account.first_name';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100),
            'sort' => $sort,
        ));
    }

    /**
     * GADM: Display all the providers that are already associated with an AccountDevice in the multi-select control
     * @return array $arrProviders
     */
    public function checkSelectedProviders()
    {
        $accountDevices = $this->findAll(array('order' => 'id', 'condition' => 'account_device_id=:account_device_id',
            'params' => array(':account_device_id' => $this->account_device_id)));

        $listProviders = CHtml::listData($accountDevices, 'id', 'provider_id');
        $arrProviders = array();

        foreach ($listProviders as $provider) {
            $arrProviders[$provider] = array("selected" => true);
        }

        return $arrProviders;
    }

    /**
     * Checks if associated provider/s belongs to another account
     * @param  array $arrProviders
     * @return boolean
     */
    public function hasOtherProviders($arrProviders)
    {
        $account_id = AccountDevice::getAccountId($this->account_device_id);

        foreach ($arrProviders as $id => $provider) {

            $accountProvider = AccountProvider::model()->find('provider_id = :provider_id', array(':provider_id' =>
                $id));

            if ((isset($accountProvider['account_id'])) &&  ($accountProvider['account_id']!= $account_id)) {
                // the provider belongs to other account.
                return true;
            }
        }
        return false;
    }

    /**
     * Kiosk: Gets providers linked to an account_device in the order they were added
     * @param int $accountDeviceId
     * @param int $practiceId
     * @param boolean $asArray
     * @return array
     */
    public static function getAccountDeviceProviders($accountDeviceId, $practiceId = 0, $asArray = false)
    {
        if ($asArray) {
            $sql = sprintf(
                "SELECT provider_id
                FROM account_device_provider
                WHERE account_device_id = %d;",
                $accountDeviceId
            );
            return Yii::app()->db->createCommand($sql)->queryColumn();

        } else {
            if ($practiceId > 0) {
                $sql = sprintf(
                    "SELECT GROUP_CONCAT(adp.provider_id ORDER BY adp.id) AS provider_ids
                    FROM account_device_provider AS adp
                    INNER JOIN account_device AS ad ON adp.account_device_id = ad.id
                    LEFT JOIN account_practice AS apra ON ad.account_id = apra.account_id
                    LEFT JOIN provider_practice AS pp ON adp.provider_id = pp.provider_id
                    AND apra.practice_id = pp.practice_id
                    WHERE adp.account_device_id = %d AND apra.practice_id = %d  AND pp.id IS NOT null",
                    $accountDeviceId,
                    $practiceId
                );
            } else {
                $sql = sprintf(
                    "SELECT GROUP_CONCAT(provider_id ORDER BY account_device_provider.id) AS provider_ids
                    FROM account_device_provider
                    WHERE account_device_id = %d;",
                    $accountDeviceId
                );
            }
            return Yii::app()->db->createCommand($sql)->queryScalar();
        }
    }

    /**
     * Get all the account_device_provider records for this provider - account
     * @param int $accountId
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function getDeviceByAccountProvider($accountId = 0, $providerId = 0, $practiceId = 0)
    {
        $practiceCondition = '';
        if ($practiceId > 0) {
            $practiceCondition = sprintf(" AND ad.practice_id = %d ", $practiceId);
        }

        $sql = sprintf(
            "SELECT adp.id, d.nickname AS name
            FROM account_device_provider AS adp
                LEFT JOIN account_device AS ad ON ad.id = adp.account_device_id
                LEFT JOIN device AS d ON d.id = ad.device_id
            WHERE adp.provider_id = %d
                %s
                AND ad.account_id = %d
                AND ad.status IN ('P', 'T', 'A');",
            $providerId,
            $practiceCondition,
            $accountId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Removes all account devices related to the given provider, account and device model.
     *
     * @param int $accountId
     * @param int $providerId
     * @param int $deviceModelId
     * @return void
     */
    public static function removeAccountDeviceProvider($accountId, $providerId, $deviceModelId)
    {
        // get all records matching the criteria
        $adp = static::model()->findAllBySql(
            'SELECT
                account_device_provider.*
            FROM account_device_provider
                JOIN account_device ON account_device.id = account_device_provider.account_device_id
                JOIN device ON device.id = account_device.device_id
            WHERE
                account_device_provider.provider_id = :provider_id
                AND account_device.account_id = :account_id
                AND account_device.status IN ("P", "T", "A")
                AND device.device_model_id = :device_model_id',
            [
                ':account_id' => $accountId,
                ':provider_id' => $providerId,
                ':device_model_id' => $deviceModelId
            ]
        );

        // nothing to cancel? exit
        if (empty($adp)) {
            return;
        }

        // remove each record
        foreach ($adp as $record) {
            $record->delete();
        }
    }

    /**
     * Get Account Device nickname
     * @param  int $account_device_id
     * @return string
     */
    public static function getNickname($id)
    {

        $sql = sprintf(
            "SELECT nickname FROM device
            INNER JOIN account_device ON account_device.device_id = device.id
            WHERE account_device.id = %s;",
            $id
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }
}
