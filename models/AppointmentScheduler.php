<?php

Yii::import('application.modules.providers.protected.components.BaseImporter');
Yii::import('application.modules.providers.protected.components.Mi7Importer');
Yii::import('application.modules.providers.protected.components.NoEmrImporter');
Yii::import('application.modules.providers.protected.components.CapybaraCache');

class AppointmentScheduler
{
    const SLOT_DURATION = 30;

    private $providerId;
    private $practiceId;
    private $duration;

    /**
     * AppointmentScheduler constructor.
     *
     * @param $providerId
     * @param $practiceId
     */
    public function __construct($providerId, $practiceId)
    {
        $this->providerId = $providerId;
        $this->practiceId = $practiceId;
        $this->duration = 60;
    }

    /**
     * Set Appointment duration
     * @param $duration
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;
    }

    /**
     * Gets week availability around the given date
     * If $extendedCheck = true, return a whole month of availability
     * @param string $date
     * @param bool $extendedCheck
     * @return array
     */
    public function getWeekAvailability($date, $extendedCheck = false)
    {

        if ($extendedCheck) {
            ini_set('memory_limit', '512M');
        }

        // Look for data in cache, otherwise build it up
        if (!CapybaraCache::isLoaded($this->practiceId, $this->providerId)) {
            CapybaraCache::loadPracticeProvider($this->practiceId, $this->providerId);
        }

        // get availability
        $diff = CapybaraCache::getAvoidAppointmentsWithin($this->practiceId);

        $minDateToGetApp = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        while ($diff >= 24) {
            $minDateToGetApp += (24 * 3600);
            if (in_array(date('w', $minDateToGetApp), array(1, 2, 3, 4, 5))) {
                $diff -= 24;
            }
        }

        // fix date format
        $date = date('Y-m-d', strtotime($date));

        // for each day we're showing
        $arrTimes = array();
        for ($i = 0; $i <= ($extendedCheck ? 30 : 6); $i++) {

            $dt = new DateTime;
            $dt->setDate(date('Y', strtotime($date)), date('m', strtotime($date)), date('d', strtotime($date)));
            $dt->modify('+ '.$i.' day');

            $day = $dt->getTimestamp();

            $dateTimes = array();
            if (($day) >= $minDateToGetApp) {
                // only query future dates
                $dateTimes = $this->getDateAvailability(date('Y-m-d', $day));
            }

            $arrTimes[date('m/d/Y', $day)] = array(
                'times' => $dateTimes,
                'date' => date('m/d/Y', $day),
                'strDate' => date('l F j, Y', $day),
                'dateShort' => date('m/d/y', $day),
                'month' => date('n', $day),
                'weekday' => date('N', $day),
                'dayNumber' => date('j', $day),
                'year' => date('Y', $day),
                'extendedCheck' => (bool)$extendedCheck
            );
        }
        return $arrTimes;
    }

    /**
     * Get availability for the given date
     * @param $date
     * @return array|null
     */
    private function getDateAvailability($date)
    {
        $arrAvailablePeriods = array();

        $schedule = CapybaraCache::getSchedule($this->practiceId, $this->providerId);

        $dayOfWeek = date('w', strtotime($date));
        if ($dayOfWeek == 0) {
            $dayOfWeek = 7;
        }

        if (isset($schedule['availability'][$dayOfWeek])) {
            $arrAvailablePeriods [] = array($schedule['availability'][$dayOfWeek]['time_start'], $schedule['availability'][$dayOfWeek]['time_end']);
        }

        $arrBusyPeriods = array();
        $appointments = CapybaraCache::getAppointmentsForDate($this->practiceId, $this->providerId, $date);
        foreach ($appointments as $item) {
            $arrBusyPeriods[] = array($item['time_start'], $item['time_end']);
        }

        $schedulingMails = CapybaraCache::getSchedulingMailsForDate($this->practiceId, $this->providerId, $date);
        foreach ($schedulingMails as $item) {
            $arrBusyPeriods[] = array($item['time_start'], $item['time_end']);
        }

        // Get schedule exceptions (Blocked dates, hours, vacations, etc...)
        $scheduleExceptions = CapybaraCache::getScheduleExceptions($this->practiceId, $this->providerId);

        foreach ($scheduleExceptions['items'] as $scheduleException) {

            // Check if the exception is for a particular date eg: 2018-11-02
            if (empty($scheduleException['day_of_week'])) {

                // If the given date is between exception date_start and date_end
                if ($date >= $scheduleException['date_start'] && $date <= $scheduleException['date_end']) {
                    // If the given date is after the exception starting date it means it starts by midnight
                    if ($date > $scheduleException['date_start']) {
                        $scheduleException['time_start'] = '00:00:00';
                    }
                    // If the given date is before the exception ending date it means it ends by midnight
                    if ($date < $scheduleException['date_end']) {
                        $scheduleException['date_end'] = '23:59:59';
                    }
                    // Add schedule exception
                    $arrBusyPeriods[] = array($scheduleException['time_start'], $scheduleException['time_end']);
                }

            } else {
                // In case the exception is for a particular day eg: Mondays

                // check if the given day matches with an exception
                if ($dayOfWeek == $scheduleException['day_of_week']) {
                    // Add schedule exception
                    $arrBusyPeriods[] = array($scheduleException['time_start'], $scheduleException['time_end']);
                }

            }

        }

        // Get operatories at Practice-Provider level
        $operatories = CapybaraCache::getProviderPracticeOperatories($this->practiceId, $this->providerId);

        // If there are no operatories set at Practice-Provider level check at Practice level
        if (!$operatories) {
            $operatories = CapybaraCache::getPracticeOperatories($this->practiceId);

            // If there's operatories set at Practice level
            if ($operatories) {

                // Load all providers for the given pratice
                $sql = sprintf(
                    "SELECT
                      provider_id
                    FROM provider_practice
                    WHERE practice_id = %d",
                    $this->practiceId
                );
                $providerIds = Yii::app()->db->createCommand($sql)->queryColumn();

                // Get all appointments from all related providers
                foreach ($providerIds as $providerId) {
                    $appointments = CapybaraCache::getAppointmentsForDate($this->practiceId, $providerId, $date);
                    foreach ($appointments as $item) {
                        $arrBusyPeriods[] = array($item['time_start'], $item['time_end']);
                    }
                }

            }
        }

        // If there are no operatories set at Practice level (use default = 1)
        if (!$operatories) {
            $operatories = 1;
        }

        $availablePeriods = $this->getAvailablePeriods($arrAvailablePeriods, $arrBusyPeriods, $operatories);
        $availablePeriods = $this->arrayFlatten($availablePeriods);
        $availablePeriods = $this->arrayUnique($availablePeriods);

        foreach ($availablePeriods as $apsk => $apsv) {
            foreach ($apsv as $apk => $apv) {
                $ap = strtotime($date . ' ' . $apv);
                $ap = date('h:i a', $ap);
                $apsv[$apk] = $ap;
            }
            $availablePeriods[$apsk] = $apsv;
        }

        // If is setting appointment for the same day, remove past hours
        if ($date == date("Y-m-d")) {
            foreach ($availablePeriods as $key => $value) {
                $toDateTime = strtotime($date . ' ' . $value[1]);
                if ($toDateTime < (time() + 3600)) {
                    unset($availablePeriods[$key]);
                }
            }
        }
        $availablePeriods = array_values($availablePeriods);
        return $availablePeriods;
    }

    /**
     * @param $arrAvail
     * @param $arrBusy
     * @param $operatories
     * @return array|null
     */
    private function getAvailablePeriods($arrAvail, $arrBusy, $operatories = 1)
    {
        // Appointment duration in minutes
        $apptDuration = (int)$this->duration;
        $slotDuration = (int)static::SLOT_DURATION;

        if ($slotDuration > $apptDuration) {
            $slotDuration = $apptDuration;
        }

        $base = mktime(0, 0, 0, date('m'), date('d'), date('Y'));
        $dayMinutes = array_fill(0, 1440, null);

        // Set what minutes are available using schedule
        if (!empty($arrAvail)) {
            foreach ($arrAvail as $avail) {

                $start = (strtotime($avail[0]) - $base) / 60;
                $end = (strtotime($avail[1]) - $base) / 60;

                for ($i = $start; $i <= $end; $i++) {
                    $dayMinutes[$i] = 0;
                }
            }
        }

        // Set number of appointments that every minute have
        if (!empty($arrBusy)) {
            foreach ($arrBusy as $busy) {
                $start = (strtotime($busy[0]) - $base) / 60;
                $end = (strtotime($busy[1]) - $base) / 60;

                for ($i = $start + 1; $i < $end; $i++) {
                    $dayMinutes[$i]++;
                }
            }
        }

        $availablePeriods = null;
        $minutesFree = 1;
        reset($dayMinutes);
        $lastMinuteKey = key($dayMinutes);

        $dayLength = sizeof($dayMinutes);
        for ($j = 0; $j < $dayLength; $j++) {

            // Get the number of appointments for the minute
            $appointmentsCount = $dayMinutes[$j];
            $minuteKey = $j;

            // Check if there's room for a new appointment
            if (!is_null($appointmentsCount) && $appointmentsCount < $operatories) {

                if ($minuteKey == $lastMinuteKey + 1) {
                    $minutesFree++;
                } else {
                    $minutesFree = 0;
                }

                if ($minutesFree == $apptDuration) {
                    $from = $minuteKey - ($apptDuration);
                    $to = $minuteKey;

                    $from = date('H:i', $base + ($from * 60));
                    $to = date('H:i', $base + ($to * 60));

                    $availablePeriods[] = array($from, $to);
                    $minutesFree = 0;
                    // changing this last number changes the slots duration (was "4", then ($duration - 1) now
                    // ($slotDuration - 1) which can be set from PB)
                    $j = $j - $apptDuration + ($slotDuration - 1);
                }

                $lastMinuteKey = $minuteKey;
            }
        }

        return $availablePeriods;
    }

    /**
     * flatten associative multi dimension array recursive
     * @param array $array
     * @param bool $preserve
     * @param array $r
     * @return array
     */
    private function arrayFlatten($array, $preserve = false, $r = array())
    {
        $tmp = null;
        unset($tmp);

        if (!empty($array)) {
            foreach ($array as $key => $value) {
                if (is_array($value)) {
                    foreach ($value as $k => $v) {
                        if (is_array($v)) {
                            $tmp = $v;
                            unset($value [$k]);
                        }
                    }
                    if ($preserve) {
                        $r [$key] = $value;
                    } else {
                        $r [] = $value;
                    }
                }
                // this is correct
                $r = isset($tmp) ? static::_arrayFlatten($tmp, $preserve, $r) : $r;
            }
        }
        // wrong spot:
        // $r = isset($tmp) ? array_flatten($tmp, $preserve, $r) : $r;
        return $r;
    }

    /**
     * Get unique values from an array
     * @param array $myArray
     * @return array
     */
    private function arrayUnique($myArray)
    {
        if (!is_array($myArray)) {
            return $myArray;
        }

        foreach ($myArray as &$myvalue) {
            $myvalue = serialize($myvalue);
        }
        unset($myvalue);

        $myArray = array_unique($myArray);

        foreach ($myArray as &$myvalue) {
            $myvalue = unserialize($myvalue);
        }

        return $myArray;
    }

}
