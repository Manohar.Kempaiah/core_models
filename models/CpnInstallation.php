<?php

Yii::import('application.modules.core_models.models._base.BaseCpnInstallation');

class CpnInstallation extends BaseCpnInstallation
{

    /**
     * Update EHR integration status in Salesforce after deleting
     *
     * @return boolean
     */
    public function afterDelete()
    {
        if (!empty($this->account_id)) {
            $organizations = OrganizationAccount::model()->findAll('account_id=:accountId', array(
                ':accountId' => $this->account_id
            ));
            if (!empty($organizations)) {
                foreach ($organizations as $organization) {
                    SqsComponent::sendMessage('updateSFEHR', $organization->organization_id);
                }
            }
        }

        return parent::afterDelete();
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get Installation
     *
     * @param str $ownerAccountId
     * @param int $cpnInstallationId
     * @return array
     */
    public function getInstallation($ownerAccountId, $cpnInstallationId)
    {
        // Query for the installation
        $sql = sprintf(
            "SELECT      cpn_installation.id,
                            cpn_installation.account_id,
                            cpn_installation.practice_id,
                            cpn_installation.machine_nickname,
                            cpn_installation.enabled_flag,
                            practice.name AS practice_name,
                            cpn_ehr_installation.cpn_installation_id,
                            IF(SUM(scheduling_mail_notification)>0,'1','0') as scheduling_mail_notification,
                            IF(SUM(twilio_log_notification)>0,'1','0') as twilio_log_notification,
                            IF(SUM(provider_rating_notification)>0,'1','0') as provider_rating_notification,
                            cpn_software_installation.installed_flag as software_installation_installed_flag
                FROM        cpn_installation
                INNER JOIN  practice ON practice.id = cpn_installation.practice_id
                LEFT JOIN   cpn_ehr_installation ON cpn_installation.id = cpn_ehr_installation.cpn_installation_id
                  AND cpn_ehr_installation.enabled_flag = '1' AND cpn_ehr_installation.del_flag = '0'
                LEFT JOIN   cpn_installation_provider
                  ON cpn_installation_provider.cpn_installation_id = cpn_installation.id
                LEFT JOIN   cpn_software_installation
                  ON cpn_software_installation.cpn_installation_id = cpn_installation.id AND cpn_software_id = 1
                WHERE account_id = '%s' AND cpn_installation.del_flag = '0' ",
            $ownerAccountId
        );

        //do we have the installation id?
        //yes
        if (!empty($cpnInstallationId)) {
            //add the installation id as a sanity check
            $sql .= sprintf("AND cpn_installation.id = %s ", $cpnInstallationId);
        }

        //add our group and order by
        $sql .= "GROUP BY cpn_installation.id
                 ORDER BY cpn_installation.machine_nickname, practice.name;";

        //do we have our installation?
        //yes
        if (!empty($cpnInstallationId)) {
            // Return the result
            return Yii::app()->db->createCommand($sql)->queryRow();

        //do we have our installation?
        //no
        } else {
            // Return all the result
            return Yii::app()->db->createCommand($sql)->queryAll();
        }
    }

    /**
     * Get Services
     *
     * @param str $secret
     * @param str $installationIdentifier
     * @return array
     */
    public function getServices($secret, $installationIdentifier)
    {
        // Query
        $sql = sprintf(
            "SELECT
                ss.guid AS identifier,
                ss.service_name AS name,
                s.installation_folder AS installationFolder,
                s.file_name AS exeName
                FROM cpn_software_installation si
                    INNER JOIN cpn_installation i on si.cpn_installation_id = i.id
                    INNER JOIN cpn_software s on si.cpn_software_id = s.id
                    INNER JOIN cpn_software_service ss on s.id = ss.cpn_software_id
                WHERE i.installation_identifier = '%s'
                    AND i.secret = '%s'
                    AND i.enabled_flag = 1
                    AND i.del_flag = 0
                    AND s.del_flag = 0
                    AND s.enabled_flag = 1
                    AND si.del_flag = 0
                    AND si.enabled_flag = 1;",
            $installationIdentifier,
            $secret
        );

        // Return all the result
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }

    /**
     * Finalize Installation
     *
     * @param str $installedFlag
     * @param str $passphrase
     * @param str $machineName
     * @param str $passphrase
     * @param str $operatingSystem
     * @param int $installationId
     * @return bool
     */
    public function finalizeInstallation($installedFlag, $passphrase, $machineName, $operatingSystem, $installationId)
    {
        // Query to cpn_installation
        $cpnI = CpnInstallation::model()->find(
            'id = :id AND enabled_flag =1 AND del_flag = 0',
            array(':id' => $installationId)
        );

        if ($cpnI) {
            $cpnI->allow_installation_flag = ($installedFlag ? 0 : 1);
            $cpnI->passphrase = $passphrase;
            $cpnI->machine_name = $machineName;
            $cpnI->operating_system = $operatingSystem;
            $cpnI->date_updated = date("Y-m-d H:i:s");
            $res = $cpnI->save();

            if ($res) {
                return true;
            }
        }

        return false;
    }

    /**
     * Mark Uninstalled
     *
     * @param int $installationId
     * @return bool
     */
    public function markUninstalled($installationId)
    {
        //mark our software installations as uninstalled
        return CpnSoftwareInstallation::model()->confirmUninstallation($installationId);
    }

    /*
 * Is some EHR integration enabled?
 *
 * @return boolean
 */
    public static function isEhrEnabled($accountId)
    {
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);
        //grab the installations based on account id
        $arrCpnInstallation = CpnInstallation::model()->findAll(
            'account_id = :account_id',
            array(':account_id' => $ownerAccountId)
        );

        //do we have any installations?
        //yes
        if (!empty($arrCpnInstallation)) {
            //iterate through our list of installations
            foreach ($arrCpnInstallation as $cpnInstallation) {
                //check to see if teh ehr installation exists
                $ehrEnabled = CpnEhrInstallation::model()->exists(
                    'cpn_installation_id = :cpn_installation_id AND enabled_flag = 1 AND del_flag = 0',
                    array(':cpn_installation_id' => $cpnInstallation->id)
                );

                //do we have ehr enabled?
                //yes
                if ($ehrEnabled) {
                    //return yes
                    return true;
                }
            }
        }

        //return no
        return false;
    }

    /**
     * Is some notification integration enabled?
     * @param $accountId
     * @return boolean
     */
    public static function isNotificationEnabled($accountId)
    {
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);
        //grab the installation based on the account id
        $arrCpnInsNot = CpnInstallation::model()->getInstallation($ownerAccountId, null);

        //iterate through the results
        foreach ($arrCpnInsNot as $cpnInsNot) {
            //do we have the enabled flag to true and at least one notifications?
            //yes
            if ($cpnInsNot['enabled_flag'] == 1 &&
                (
                    $cpnInsNot['scheduling_mail_notification'] == 1 ||
                    $cpnInsNot['twilio_log_notification'] == 1 ||
                    $cpnInsNot['provider_rating_notification'] == 1
                )
            ) {
                //return yes
                return true;
            }
        }

        //return no
        return false;
    }

    /**
     * Flag an installation as deleted
     *
     * @return array
     */
    public function deleteInstallation()
    {
        $this->del_flag = 1;
        if ($this->save()) {
            $cpnEhrInstallation = CpnEhrInstallation::model()->find(
                'cpn_installation_id=:cpn_installation_id',
                array(':cpn_installation_id' => $this->id)
            );

            if ($cpnEhrInstallation) {
                $cpnEhrInstallation->del_flag = 1;
                $cpnEhrInstallation->save();
            }

            return (array) $this->attributes;
        }
    }

    /**
     * Get an installation code
     */
    public static function generateInstallationCode()
    {
        $installationCode = rand(100000, 999999);

        $cpnInstallation = CpnInstallation::model()->exists(
            'installation_code=:installation_code',
            array(':installation_code' => $installationCode)
        );

        if ($cpnInstallation) {
            return self::generateInstallationCode();
        }

        return $installationCode;
    }

    /**
     * Set EHR installation type and configuration
     *
     * @param array $ehrData
     * @param integer $accountId
     */
    public function setEhrInstallation($ehrData, $accountId)
    {
        // Update CpnEhrInstallation
        $cpnEhrInstallation = CpnEhrInstallation::model()->find(
            'cpn_installation_id = :cpn_installation_id AND enabled_flag = 1 AND del_flag = 0',
            array(':cpn_installation_id' => $this->id)
        );

        if ($cpnEhrInstallation) {
            // Previous EhrType
            $prevEhrType = $cpnEhrInstallation->cpn_ehr_id;

            $cpnEhrInstallation->cpn_ehr_id = $ehrData['type'];
            $cpnEhrInstallation->updated_by_account_id = $accountId;
            $cpnEhrInstallation->enabled_flag = 1;
            $cpnEhrInstallation->del_flag = 0;

        } else {
            // New CpnEhrInstallation
            $prevEhrType = $ehrData['type'];

            $cpnEhrInstallation = new CpnEhrInstallation();
            $cpnEhrInstallation->guid = Yii::app()->db->createCommand("SELECT uuid()")->queryScalar();
            $cpnEhrInstallation->cpn_ehr_id = $ehrData['type'];
            $cpnEhrInstallation->cpn_installation_id = $this->id;
            $cpnEhrInstallation->added_by_account_id = $accountId;
        }

        if ($cpnEhrInstallation->save()) {

            // If EHR Type has changed remove all attributes from previous configuration
            if ($prevEhrType != $ehrData['type']) {

                // Get all attributes for this installation
                $ehrAttributes = CpnEhrInstallationAttribute::model()->findAll(
                    'cpn_ehr_installation_id = :cpn_ehr_installation_id',
                    array(':cpn_ehr_installation_id' => $cpnEhrInstallation->id)
                );

                // Delete them
                if ($ehrAttributes) {
                    foreach ($ehrAttributes as $cpnEhrInsAttribute) {
                        $cpnEhrInsAttribute->delete();
                    }
                }
            }


            // Set EHR Attributes
            foreach ($ehrData['attributes'] as $key => $attributeValue) {

                // Search for existing attribute
                $ehrAttribute = CpnEhrInstallationAttribute::model()->find(
                    'cpn_ehr_installation_id=:cpn_ehr_installation_id
                            AND cpn_ehr_attribute_type_id=:cpn_ehr_attribute_type_id',
                    array(
                        ':cpn_ehr_installation_id' => $cpnEhrInstallation->id,
                        ':cpn_ehr_attribute_type_id' => $key
                    )
                );

                if ($ehrAttribute) {

                    $ehrAttribute->attribute_value = $attributeValue;
                    $ehrAttribute->enabled_flag = 1;
                    $ehrAttribute->del_flag = 0;
                    $ehrAttribute->updated_by_account_id = $accountId;
                    $ehrAttribute->save();
                } else {
                    // New CpnEhrInstallationAttribute
                    $ehrAttribute = new CpnEhrInstallationAttribute();
                    $ehrAttribute->guid = Yii::app()->db->createCommand("SELECT uuid()")->queryScalar();
                    $ehrAttribute->cpn_ehr_attribute_type_id = $key;
                    $ehrAttribute->cpn_ehr_installation_id = $cpnEhrInstallation->id;
                    $ehrAttribute->attribute_value = $attributeValue;
                    $ehrAttribute->added_by_account_id = $accountId;
                    $ehrAttribute->save();
                }

            }

        }

    }

    /**
     * Delete EHR installation for the current companion installation
     *
     * @param integer $accountId
     */
    public function deleteEhrInstallation($accountId)
    {
        // Update CpnEhrInstallation
        $cpnEhrInstallation = CpnEhrInstallation::model()->find(
            'cpn_installation_id = :cpn_installation_id AND enabled_flag = 1 AND del_flag = 0',
            array(':cpn_installation_id' => $this->id)
        );

        /**
         * Delete EhrInstallation
         */
        if ($cpnEhrInstallation) {

            $iAttribute = CpnEhrInstallationAttribute::model()->findAll(
                'cpn_ehr_installation_id=:cpn_ehr_installation_id',
                array(':cpn_ehr_installation_id' => $cpnEhrInstallation->id)
            );

            if ($iAttribute) {
                foreach ($iAttribute as $cpnEhrInsAttribute) {
                    $cpnEhrInsAttribute->enabled_flag = 0;
                    $cpnEhrInsAttribute->del_flag = 1;
                    $cpnEhrInsAttribute->updated_by_account_id = $accountId;
                    $cpnEhrInsAttribute->save();
                }
            }
            $cpnEhrInstallation->enabled_flag = 0;
            $cpnEhrInstallation->del_flag = 1;
            $cpnEhrInstallation->updated_by_account_id = $accountId;
            $cpnEhrInstallation->save();
        }

    }

    /**
     * Set EHR Location <--> DDC Practice associations
     *
     * @param array $practices
     * @param integer $accountId
     */
    public function setEhrLocations($practices, $accountId)
    {
        foreach ($practices as $practice) {
            // Search for existing practice associations
            $cpnInstallationPractice = CpnInstallationPractice::model()->find(
                'cpn_installation_id = :cpn_installation_id AND practice_id = :practice_id',
                array(
                    ':cpn_installation_id' => $this->id,
                    ':practice_id' => $practice['id']
                )
            );

            // Check if the EHR location is set, otherwise delete the entry from the DB
            if (!empty($practice['cpn_location_id'])) {
                // If not found, create a new entry
                if (!$cpnInstallationPractice) {
                    $cpnInstallationPractice = new CpnInstallationPractice();
                    $cpnInstallationPractice->cpn_installation_id = $this->id;
                    $cpnInstallationPractice->practice_id = $practice['id'];
                    $cpnInstallationPractice->added_by_account_id = $accountId;
                }
                $cpnInstallationPractice->cpn_installation_location_id = $practice['cpn_location_id'];
                $cpnInstallationPractice->save();
            } else {
                if ($cpnInstallationPractice) {
                    // Delete the association
                    $cpnInstallationPractice->delete();
                }
            }
        }

    }

    /**
     * Set EHR Provider <--> DDC Provider associations
     *
     * @param array $providers
     * @param integer $accountId
     */
    public function setEhrProviders($providers, $accountId)
    {
        $iProvider = CpnInstallationProvider::model()->findAll(
            'cpn_installation_id=:cpn_installation_id',
            array(':cpn_installation_id' => $this->id)
        );

        if ($iProvider) {

            foreach ($iProvider as $cpnInsProvider) {
                $cpnInsProvider->cpn_installation_user_id = null;
                $cpnInsProvider->updated_by_account_id = $accountId;
                $cpnInsProvider->save();
            }
        }

        foreach ($providers as $provider) {
            $iProvider = CpnInstallationProvider::model()->find(
                'cpn_installation_id = :cpn_installation_id AND provider_id = :provider_id',
                array(
                    ':cpn_installation_id' => $this->id,
                    ':provider_id' => $provider['id']
                )
            );

            if ($iProvider) {
                $iProvider->cpn_installation_user_id = $provider['cpn_installation_user_id'];
                $iProvider->updated_by_account_id = $accountId;
                $iProvider->save();
            } else {
                $iProvider = new CpnInstallationProvider();
                $iProvider->cpn_installation_id = $this->id;
                $iProvider->cpn_installation_user_id =  $provider['cpn_installation_user_id'];
                $iProvider->provider_id = $provider['id'];
                $iProvider->added_by_account_id = $accountId;
                $iProvider->save();
            }
        }
    }

    /**
     * Set operatories for practice and provider-practice (only used by Dentrix)
     *
     * @param array $data
     */
    public function setOperatories($data)
    {
        foreach ($data['practices'] as $oPractice) {
            // Save operatories at practice level
            $practice = Practice::model()->findByPk($oPractice['id']);

            if ($practice) {
                $maxOperatories = null;
                if (!empty($oPractice['operatories'])) {
                    $maxOperatories = (int) $oPractice['operatories'];
                }

                $practice->operatories = $maxOperatories;
                $practice->save();
            }

            foreach ($oPractice['providers'] as $provider) {
                // Save operatories at provider-practice level
                $providerPractice = ProviderPractice::model()->find(
                    'provider_id = :provider_id AND practice_id = :practice_id',
                    array(':provider_id' => $provider['id'], ':practice_id' => $practice['id'])
                );

                if ($providerPractice) {
                    $maxOperatories = null;
                    if (!empty($provider['operatories'])) {
                        $maxOperatories = (int) $provider['operatories'];
                    }

                    $providerPractice->operatories = $maxOperatories;
                    $providerPractice->save();
                }
            }
        }
    }

    /**
     * Set notifications for installation providers
     *
     * @param array $notification
     * @param integer $accountId
     */
    public function setNotifications($notification, $accountId)
    {
        $cpnProviders = CpnInstallationProvider::model()->findAll(
            'cpn_installation_id=:cpn_installation_id',
            array(':cpn_installation_id' => $this->id)
        );

        /*
         * Blank Notification
         */
        if ($cpnProviders) {
            foreach ($cpnProviders as $cpnProvider) {
                $cpnProvider->scheduling_mail_notification = 0;
                $cpnProvider->twilio_log_notification = 0;
                $cpnProvider->provider_rating_notification = 0;
                $cpnProvider->updated_by_account_id = $accountId;
                $cpnProvider->save();
            }
        }

        if (isset($notification['providers'])) {

            foreach ($notification['providers'] as $providerId => $pNotification) {

                $cpnProvider = CpnInstallationProvider::model()->find(
                    'cpn_installation_id = :cpn_installation_id AND provider_id = :provider_id',
                    array(':cpn_installation_id' => $this->id, ':provider_id' => $providerId)
                );

                if ($cpnProvider) {
                    $cpnProvider->scheduling_mail_notification = (int)$pNotification['scheduling_mail_notification'];
                    $cpnProvider->twilio_log_notification = (int)$pNotification['twilio_log_notification'];
                    $cpnProvider->provider_rating_notification = (int)$pNotification['provider_rating_notification'];
                    $cpnProvider->updated_by_account_id = $accountId;
                    $cpnProvider->save();
                } else {
                    $cpnProvider = new CpnInstallationProvider();
                    $cpnProvider->cpn_installation_id = $this->id;
                    $cpnProvider->provider_id = $providerId;
                    $cpnProvider->scheduling_mail_notification = (int)$pNotification['scheduling_mail_notification'];
                    $cpnProvider->twilio_log_notification = (int)$pNotification['twilio_log_notification'];
                    $cpnProvider->provider_rating_notification = (int)$pNotification['provider_rating_notification'];
                    $cpnProvider->added_by_account_id = $accountId;
                    $cpnProvider->save();
                }
            }
        }
    }
}
