<?php

use function GuzzleHttp\json_decode;

Yii::import('application.modules.core_models.models._base.BaseWaitingRoomInvite');

class WaitingRoomInvite extends BaseWaitingRoomInvite
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Prepare data to create a review_request entry, used in:
     *  - telemedicine/callEnded
     *  - telemedicine/callRating
     *
     * @param array $params
     *      $params['guid'] -> waiting_room_invite->guid
     *
     * @param obj $waitingRoomInvite
     * @param obj $waitingRoomSetting
     * @param obj $waitingRoomGuest
     *
     * @return array $result
     */
    public static function createReviewRequest(
        $params = array(),
        $waitingRoomInvite = null,
        $waitingRoomSetting = null,
        $waitingRoomGuest = null
    ) {

        // find the waiting room invite
        if (empty($waitingRoomInvite->waiting_room_setting_id)) {
            if (!empty($params['guid'])) {
                $waitingRoomInvite = WaitingRoomInvite::model()
                    ->find('guid=:guid', array(':guid' => $params['guid']));
            }
            if (empty($waitingRoomInvite->waiting_room_setting_id)) {
                return array(
                    'success' => false,
                    'error' => true,
                    'data' => 'Missing Waiting Room Invite'
                );
            }
        }

        // Can we send a RR?
        if ($waitingRoomInvite->send_review_request == 0) {
            return array(
                'success' => false,
                'error' => true,
                'data' => 'There is no ReviewRequest configured.'
            );
        }

        if (
            (empty($waitingRoomGuest->id) && !empty($waitingRoomInvite->review_request_id))
            ||
            !empty($waitingRoomGuest->review_request_id)
        ) {
            // for the main patient
            return array(
                'success' => false,
                'error' => true,
                'data' => 'The ReviewRequest was sent before.'
            );
        }

        if (empty($waitingRoomSetting)) {
            // Check waitingRoomSetting
            $waitingRoomSetting = WaitingRoomSetting::model()
                ->cache(Yii::app()->params['cache_short'])
                ->findByPK($waitingRoomInvite->waiting_room_setting_id);

            if (empty($waitingRoomSetting)) {
                return array(
                    'success' => false,
                    'error' => true,
                    'data' => 'Missing Waiting Setting'
                );
            }
        }

        if (
            empty($waitingRoomSetting->custom_url)
            && empty($waitingRoomSetting->provider_practice_listing_id)
            && empty($waitingRoomSetting->account_device_id)
        ) {
            return array(
                'success' => false,
                'error' => true,
                'data' => 'No destination is configured in waiting_room'
            );
        }

        // Custom URL
        $wrsCustomUrl = $waitingRoomSetting->custom_url;
        $freemiumGoogle = false;
        if (!empty($wrsCustomUrl)) {
            $isJson = StringComponent::isJson($wrsCustomUrl);
            if ($isJson) {
                $custom = json_decode($wrsCustomUrl, true);
                $baseUrl = 'https://search.google.com/local/writereview?placeid=';
                if (!empty($custom['place_id'])) {
                    $wrsCustomUrl = $baseUrl . $custom['place_id'];
                    $freemiumGoogle = true;
                }
            }
        }

        // Define an empty array, to send to the ReviewRequest process
        if (!empty($waitingRoomGuest->id)) {
            $patientEmail = $waitingRoomGuest->email;
            $patientPhone = $waitingRoomGuest->phone_number;
            $patientFirstName = $waitingRoomGuest->first_name;
            $patientLastName = $waitingRoomGuest->last_name;
            $overallRating = $waitingRoomGuest->overall_rating;
        } else {
            $patientEmail = $waitingRoomInvite->patient_email;
            $patientPhone = $waitingRoomInvite->patient_phone_number;
            $patientFirstName = $waitingRoomInvite->patient_first_name;
            $patientLastName = $waitingRoomInvite->patient_last_name;
            $overallRating = $waitingRoomInvite->overall_rating;
        }

        $postData = array(
            'accountId' => $waitingRoomSetting->account_id,
            'providerId' => $waitingRoomSetting->provider_id,
            'patientFirstName' => $patientFirstName,
            'patientLastName' => $patientLastName,
            'patientEmail' => $patientEmail,
            'patientMobilePhone' => $patientPhone,
            'reviewRequestEmailSubject' => null,
            'reviewRequestEmailTitle' => null,
            'reviewRequestEmailBody' => null,
            'reviewRequestSmsBody' => null,
            'googleReviewEmailTitle' => null,
            'googleReviewEmailBody' => null,
            'googleReviewSmsBody' => null,
            'sendTo' => '',
            'send_via' => (!empty($patientEmail) ? 'EMAIL' : (!empty($patientPhone) ? 'SMS' : '')),
            'coming_from' => !empty($params['coming_from']) ? $params['coming_from'] : '',
            'source' => 'VirtualVisit',
            'accountDeviceId' => $waitingRoomSetting->account_device_id,
            'providerPracticeListingId' => $waitingRoomSetting->provider_practice_listing_id,
            'customUrl' => $wrsCustomUrl,
            'overall_rating' => $overallRating,
            'waiting_room_invite_id' => $waitingRoomInvite->id,
            'waiting_room_guest_id' => !empty($waitingRoomGuest->id) ? $waitingRoomGuest->id : null,
            'participant' => !empty($params['participant']) ? $params['participant'] : 'mainPatient',
            'testEmail' => 0
        );

        if (!empty($waitingRoomSetting->provider_practice_listing_id)) {
            $ppl = ProviderPracticeListing::model()->findByPK($waitingRoomSetting->provider_practice_listing_id);
            if ($ppl->listing_type_id == ListingType::GOOGLE_PROFILE_ID) {
                $postData['sendTo'] = 'GOOGLE';
            } elseif ($ppl->listing_type_id == ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID) {
                $postData['sendTo'] = 'GOOGLE';
            } elseif ($ppl->listing_type_id == ListingType::YELP_PROFILE_ID) {
                $postData['sendTo'] = 'YELP';
            } elseif ($ppl->listing_type_id == ListingType::YELP_WRITEREVIEW_ID) {
                $postData['sendTo'] = 'YELP';
            }
        } elseif (!empty($waitingRoomSetting->account_device_id)) {
            $postData['sendTo'] = 'DDC';
        } elseif (!empty($waitingRoomSetting->custom_url)) {
            $postData['sendTo'] = $freemiumGoogle ? 'GOOGLE' : 'CUSTOM';
        } else {
            return array(
                'success' => false,
                'error' => true,
                'data' => 'There is no ReviewRequest configured.'
            );
        }

        return ReviewRequest::sendReviewRequest($postData);
    }

    /**
     * Get total call time in seconds
     */
    public function getCallTime()
    {

        $sql = sprintf(
            "SELECT TIME_TO_SEC(TIMEDIFF(NOW(), MAX(provider_connected))) AS time_diff,
            room_duration , room_sid, id, date_added
            FROM waiting_room_queue
            WHERE
                waiting_room_invite_id = %d
            GROUP BY room_sid
            ORDER BY time_diff DESC",
            $this->id
        );

        $sec = Yii::app()->db->createCommand($sql)->queryRow();
        return (!empty($sec['time_diff'])) ? (int) $sec['time_diff'] : 0;
    }

    /**
     * Return the particpants and the status
     */
    public function getParticipants()
    {
        $inviteId = $this->id;
        $waitingRoomSetting = $this->waitingRoomSetting;

        $room = TwilioApi::getRoom($this->guid);
        $room = Common::get($room, 'data', null);

        // Get the provider
        $provider = $waitingRoomSetting->provider;

        // Find the queue
        $waitingRoomQueue = WaitingRoomQueue::model()
            ->find(
                array(
                    "condition" => "waiting_room_invite_id=" . $inviteId,
                    "order" => "id DESC"
                )
            );


        $list = array();

        // Is the provider connected ?
        $startTime = $status = '';
        if (
            !empty($waitingRoomQueue->provider_connected)
            &&
            (empty($waitingRoomQueue->provider_disconnected)
                ||
                $waitingRoomQueue->provider_disconnected <= $waitingRoomQueue->provider_connected)
        ) {
            $startTime = $waitingRoomQueue->provider_connected;
        }
        $participantSid = '';
        if (!empty($room['participants'])) {
            foreach ($room['participants'] as $p) {
                if ($p['identity'] == $provider->first_last) {
                    $participantSid = $p['sid'];
                    break;
                }
            }
        }
        $connected = array(
            'status' => (!empty($participantSid) ? 'connected' : $status),
            'start_time' => $startTime,
            'identity' => $waitingRoomSetting->provider->first_last,
            'first_name' => $waitingRoomSetting->provider->first_name,
            'last_name' => $waitingRoomSetting->provider->last_name,
            'title' => $waitingRoomSetting->provider->prefix,
            'invite_sent' => '',
            'invite_via' => '',
            'call_link' => '',
            'participant_sid' => $participantSid,
            'participant_type' => 'provider'
        );
        // Add the provider to the list
        $list[] = $connected;

        //
        // Is the main patient connected ?
        //
        $callLink = '';
        if (empty($this->patient_email) && empty($this->patient_phone_number)) {
            // yes, because havent email and phone
            $callLink = WaitingRoomSetting::getUrl('visit')
                . "/" . $waitingRoomSetting->friendly_url
                . "/" . $this->guid;
            $inviteVia = 'Share Link';
        } elseif (!empty($this->patient_email)) {
            $inviteVia = $this->patient_email;
        } else {
            $inviteVia = StringComponent::formatPhone($this->patient_phone_number);
        }

        $startTime = $status = '';
        if (
            !empty($waitingRoomQueue->patient_connected)
            &&
            (empty($waitingRoomQueue->patient_disconnected)
                ||
                $waitingRoomQueue->patient_disconnected <= $waitingRoomQueue->patient_connected)
        ) {
            $startTime = $waitingRoomQueue->patient_connected;
            $status = 'prejoin';
        }

        $participantSid = '';
        if (!empty($room['participants'])) {
            foreach ($room['participants'] as $p) {
                if ($p['identity'] == $this->patient_nickname) {
                    $participantSid = $p['sid'];
                    $status = 'connected';
                    break;
                }
            }
        }

        $connected = array(
            'status' => (!empty($participantSid) ? 'connected' : $status),
            'start_time' => $startTime,
            'identity' => $this->patient_nickname,
            'first_name' => $this->patient_first_name,
            'last_name' => $this->patient_last_name,
            'title' => '',
            'invite_sent' => $this->invite_sent,
            'invite_via' => $inviteVia,
            'call_link' => $callLink,
            'participant_sid' => $participantSid,
            'participant_type' => (!empty($this->is_static) ? 'mainStaticPatient' : 'mainPatient')
        );
        // Add the main patient to the list
        $list[] = $connected;

        // Get the guests
        $waitingRoomGuest = WaitingRoomGuest::model()->findAll(
            "waiting_room_invite_id=:waiting_room_invite_id",
            [":waiting_room_invite_id" => $inviteId]
        );
        foreach ($waitingRoomGuest as $guest) {
            // If the user is a Patient, display only the other collegues
            $callLink = '';
            if (empty($guest->email) && empty($guest->phone_number)) {
                // yes, because havent email and phone
                $callLink = WaitingRoomSetting::getUrl('visit')
                    . "/" . $waitingRoomSetting->friendly_url
                    . "/" . $guest->guid;
                $inviteVia = 'Share link';
            } elseif (!empty($guest->email)) {
                $inviteVia = $guest->email;
            } else {
                $inviteVia = StringComponent::formatPhone($guest->phone_number);
            }

            // We need to search in the waiting_room_guest_queue
            if (!empty($waitingRoomQueue->id)) {
                $condition = sprintf(
                    "waiting_room_guest_id=%d AND waiting_room_queue_id=%d",
                    $guest->id,
                    $waitingRoomQueue->id
                );
                $waitingRoomGuestQueue = WaitingRoomGuestQueue::model()
                    ->find(array("condition" => $condition, "order" => "id DESC"));
            } else {
                $waitingRoomGuestQueue = new WaitingRoomGuestQueue();
            }

            $startTime = $status = '';
            if (
                !empty($waitingRoomGuestQueue->guest_connected)
                &&
                (empty($waitingRoomGuestQueue->guest_disconnected)
                    ||
                    $waitingRoomGuestQueue->guest_disconnected <= $waitingRoomGuestQueue->guest_connected)
            ) {
                $startTime = $waitingRoomGuestQueue->guest_connected;
                $status = 'prejoin';
            }

            $participantSid = '';
            if (!empty($room['participants'])) {
                foreach ($room['participants'] as $p) {
                    if ($p['identity'] == $guest->nickname) {
                        $participantSid = $p['sid'];
                        $status = 'connected';
                        break;
                    }
                }
            }
            $connected = array(
                'status' => (!empty($participantSid) ? 'connected' : $status),
                'start_time' => $startTime,
                'identity' => $guest->nickname,
                'first_name' => $guest->first_name,
                'last_name' => $guest->last_name,
                'title' => (string)$guest->title,
                'invite_sent' => $guest->invite_sent,
                'invite_via' => $inviteVia,
                'call_link' => $callLink,
                'participant_sid' => $participantSid,
                'participant_type' => $guest->type
            );
            // Add the guest to the list
            $list[] = $connected;
        }
        return array(
            'room' => !empty($room['room']) ? $room['room'] : null,
            'participants' => $list,
        );
    }


    /**
     * validate the checkIn
     * @param string $guid,
     *
     * @return array
     */
    public static function checkIn($guid = null)
    {

        if (empty($guid)) {
            return array(
                'success' => false,
                'error' => 'Empty guid',
                'data' => 'Empty guid'
            );
        }

        $waitingRoomGuest = $waitingRoomInvite = $waitingRoomSetting = null;
        $connected = $disconnected = null;

        $waitingRoomInvite = WaitingRoomInvite::model()->find("guid=:guid", [":guid" => $guid]);

        // Is the main Patient?
        if (empty($waitingRoomInvite)) {
            // Nop, search in the guest participants
            $waitingRoomGuest = WaitingRoomGuest::model()->find("guid=:guid", [":guid" => $guid]);
            if (!empty($waitingRoomGuest)) {
                $waitingRoomInvite = $waitingRoomGuest->waitingRoomInvite;
            }
        }

        if (empty($waitingRoomInvite)) {
            return array(
                'success' => false,
                'error' => 'Invalid waiting room invite',
                'data' => 'Invalid waiting room invite'
            );
        }

        $waitingRoomSetting = $waitingRoomInvite->waitingRoomSetting;
        if (empty($waitingRoomSetting)) {
            return array(
                'success' => false,
                'error' => 'Invalid waiting room',
                'data' => 'Invalid waiting room'
            );
        }

        $connected = $disconnected = null;
        // Is there any open queue for this $waitingRoomInvite->id ?
        $condition = sprintf("waiting_room_invite_id=%d AND room_ended IS NULL", $waitingRoomInvite->id);
        $waitingRoomQueue = WaitingRoomQueue::model()->find(
            ["condition" => $condition, "order" => "id DESC"]
        );

        // did we find it?
        if (empty($waitingRoomQueue)) {
            // no, maybe we're in the process of creating it give it some time to save
            sleep(2);

            // try to find an existing record for this room again
            $waitingRoomQueue = WaitingRoomQueue::model()->find(
                ["condition" => $condition, "order" => "id DESC"]
            );
        }

        if (empty($waitingRoomQueue)) {
            // Is a new call, create the queue
            $waitingRoomQueue = new WaitingRoomQueue();
            $waitingRoomQueue->waiting_room_invite_id = $waitingRoomInvite->id;
            $waitingRoomQueue->room_created = TimeComponent::getNow();
            $waitingRoomQueue->save();
        }

        // The connected person is the main Patient?
        if (empty($waitingRoomGuest)) {

            // Yes, the connected person is the main Patient
            if (empty($waitingRoomQueue->room_sid)) {
                $waitingRoomQueue->patient_connected = TimeComponent::getNow();
                // No body connected yet, reset the room_created time
                $waitingRoomQueue->room_created = TimeComponent::getNow();
            }
            if (!empty($waitingRoomQueue->room_sid)) {
                // Is the current call
                $waitingRoomQueue->patient_disconnected = null;
            }
            $waitingRoomQueue->save();

            $connected = $waitingRoomQueue->patient_connected;
        } else {
            // Is a guest try to find an existing record for this room again
            $condition = sprintf(
                "waiting_room_queue_id=%d AND waiting_room_guest_id=%d AND guest_disconnected IS NULL",
                $waitingRoomQueue->id,
                $waitingRoomGuest->id
            );
            $waitingRoomGuestQueue = WaitingRoomGuestQueue::model()->find(
                ["condition" => $condition, "order" => "id DESC"]
            );

            if (empty($waitingRoomGuestQueue)) {
                $waitingRoomGuestQueue = new WaitingRoomGuestQueue();
                $waitingRoomGuestQueue->waiting_room_queue_id = $waitingRoomQueue->id;
                $waitingRoomGuestQueue->waiting_room_guest_id = $waitingRoomGuest->id;
                $waitingRoomGuestQueue->guest_connected = TimeComponent::getNow();
            }
            $waitingRoomGuestQueue->save();
            $connected = $waitingRoomGuestQueue->guest_connected;
        }

        // Was the invited previously disabled? (removed from the waiting-room by the provider)
        if (!empty($invite) && $invite['enabled'] != 1) {
            // Yes was disabled. We need to reactivate it
            $waitingRoomInvite->enabled = 1;
            $waitingRoomInvite->save();
            $invite['enabled'] = 1;

            // Remove the cache
            $queueId = "telemedicine_" . $waitingRoomInvite->waiting_room_setting_id;
            Yii::app()->cache->set($queueId, null, Yii::app()->params['cache_long']);
        }

        $responseWaitingRoom = array(
            "id" => $waitingRoomSetting->id,
            "provider_id" => $waitingRoomSetting->provider_id,
            "name" => $waitingRoomSetting->provider->first_last,
            "friendly_url" => $waitingRoomSetting->friendly_url,
            "logo_path" => $waitingRoomSetting->logo_path,
            "welcome_message" => $waitingRoomSetting->welcome_message,
            "rate_review_title" => $waitingRoomSetting->rate_review_title,
            "patient_point_enabled" => $waitingRoomSetting->patient_point_enabled
        );

        $responseWaitingRoomGuest = $responseWaitingRoomInvite = [];
        if (!empty($waitingRoomGuest)) {
            $responseWaitingRoomGuest = array(
                "id" => $waitingRoomGuest->id,
                "guid" => $waitingRoomGuest->guid,
                "patient_id" => $waitingRoomGuest->patient_id,
                "provider_id" => $waitingRoomGuest->provider_id,
                "participant_type" => $waitingRoomGuest->type,
                "nickname" => $waitingRoomGuest->nickname,
                "first_name" => $waitingRoomGuest->first_name,
                "last_name" => $waitingRoomGuest->last_name,
                "phone_number" => $waitingRoomGuest->phone_number,
                "email" => $waitingRoomGuest->email,
                "connected" => $waitingRoomGuestQueue->guest_connected,
                "disconnected" => $waitingRoomGuestQueue->guest_disconnected
            );
        }

        $responseWaitingRoomInvite = array(
            "id" => $waitingRoomInvite->id,
            "guid" => $waitingRoomInvite->guid,
            "enabled" => $waitingRoomInvite->enabled,
            "is_static" => $waitingRoomInvite->is_static,
            "send_review_request" => $waitingRoomInvite->send_review_request,
            "patient_id" => $waitingRoomInvite->patient_id,
            "nickname" => $waitingRoomInvite->patient_nickname,
            "first_name" => $waitingRoomInvite->patient_first_name,
            "last_name" => $waitingRoomInvite->patient_last_name,
            "phone_number" => $waitingRoomInvite->patient_phone_number,
            "email" => $waitingRoomInvite->patient_email,
            "connected" => $waitingRoomQueue->patient_connected,
            "disconnected" => $waitingRoomQueue->patient_disconnected
        );

        // get Appoinment data from the patient, if any
        $visitReason = null;
        if (
            $waitingRoomInvite->patient_id != null ||
            $waitingRoomInvite->patient_phone_number != null ||
            $waitingRoomInvite->patient_email != null
        ) {

            $visitReason = self::getVisitReasonFromCapybara($waitingRoomInvite, $waitingRoomSetting);
        }

        // by default enable ads if feature is enabled in config file
        $patientPointEnabled = Yii::app()->params['patient_point_video_ads_enabled'];

        // also check if ads are enabled in the waiting room configuration (if paying client)
        if ($patientPointEnabled) {

            $organizationAccount = OrganizationAccount::model()
                ->cache(Yii::app()->params['cache_short'])
                ->find(
                    'account_id = :account_id AND connection = :connection',
                    array(':account_id' => $waitingRoomSetting->account_id, ':connection' => "O")
                );

            $patientPointEnabled =
                $patientPointEnabled
                &&
                (
                    empty($organizationAccount)
                    ||
                    $waitingRoomSetting->patient_point_enabled
                );

            // we're good so far
            if ($patientPointEnabled) {
                // also check if the provider has a specialty that's supported by patientpoint
                $patientPointEnabled = !empty(ProviderSpecialty::hasPatientPointSupportedSpecialty(
                    $waitingRoomSetting->provider_id
                ));
            }
        }

        return array(
            'success' => true,
            'error' => '',
            'data' => array(
                "waitingRoom" => $responseWaitingRoom,
                "waitingRoomInvite" => $responseWaitingRoomInvite,
                "waitingRoomGuest" => $responseWaitingRoomGuest,
                "waitingRoomQueue" => $waitingRoomQueue->attributes,
                "queue_id" =>  $waitingRoomQueue->id,
                "visitReason" => $visitReason,
                "connected" => $connected,
                "disconnected" => $disconnected,
                "ads_video_enabled" => $patientPointEnabled,
            )
        );
    }


    /*
     * Get the visit reason saved for the appointment
     * @param object $waitingRoomInvite
     * @param object $waitingRoomSetting
     *
     * @return int visitReason
     */
    private static function getVisitReasonFromCapybara($waitingRoomInvite, $waitingRoomSetting)
    {
        // get the appointment date and remove the time
        $waitingRoomInviteDateAdded = $waitingRoomInvite->date_added;
        $s = strtotime($waitingRoomInviteDateAdded);
        $waitingRoomInviteDate = date('Y-m-d', $s);

        $appointmentSql = '';

        // try to match by patient_id which is the most accurate field to match
        if (isset($waitingRoomInvite->patient_id)) {
            $appointmentSql = sprintf(
                "SELECT visit_reason_id FROM scheduling_mail sm WHERE
            sm.patient_id = %d AND sm.provider_id = %d AND sm.appointment_date = '%s'
            ORDER BY date_added DESC",
                $waitingRoomInvite->patient_id,
                $waitingRoomSetting->provider_id,
                $waitingRoomInviteDate
            );
            // else try to match by patient_email which is the 2nd most accurate field to match
        } elseif (isset($waitingRoomInvite->patient_email)) {

            // Search capybara appointments for this patient
            $appointmentSql = sprintf(
                "SELECT visit_reason_id FROM scheduling_mail sm
                WHERE sm.email = '%s' AND sm.provider_id = %d AND
                sm.appointment_date = '%s'
                ORDER BY date_added DESC",
                $waitingRoomInvite->patient_email,
                $waitingRoomSetting->provider_id,
                $waitingRoomInviteDate
            );
            // else try to match by patient_phone which is the 3rd most accurate field to match
        } elseif (isset($waitingRoomInvite->patient_phone_number)) {

            // Search capybara appointments for this patient
            $appointmentSql = sprintf(
                "SELECT visit_reason_id FROM scheduling_mail sm
                WHERE sm.cell_phone = '%s' AND sm.provider_id = %d AND
                sm.appointment_date = '%s'
                ORDER BY date_added DESC",
                $waitingRoomInvite->patient_phone_number,
                $waitingRoomSetting->provider_id,
                $waitingRoomInviteDate
            );
        }

        $capybaraAppointmentVisitReason = Yii::app()->db->createCommand($appointmentSql)->queryRow();

        if (is_array($capybaraAppointmentVisitReason)) {

            $visitReason = VisitReason::model()
                ->cache(Yii::app()->params['cache_long'])
                ->find("id=:id", [":id" => $capybaraAppointmentVisitReason['visit_reason_id']]);

            // it should never be null, we have integrity reference (FK)
            return $visitReason->name;
        } else {
            return null;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave()
    {
        // add reference to the scheduling mail
        if (empty($this->scheduling_mail_id)) {
            $partnerSiteAdServer = PartnerSiteAdServer::searchByWaitingRoomInviteId($this->id);

            if ($partnerSiteAdServer && $partnerSiteAdServer['sm_id']) {
                $waitingRoomInviteToUpdate = static::model()->findByPk($this->id);
                $waitingRoomInviteToUpdate->scheduling_mail_id = $partnerSiteAdServer['sm_id'];
                $waitingRoomInviteToUpdate->save();
            }
        }

        return parent::afterSave();
    }
}
