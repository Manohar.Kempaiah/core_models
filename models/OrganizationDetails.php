<?php

Yii::import('application.modules.core_models.models._base.BaseOrganizationDetails');

class OrganizationDetails extends BaseOrganizationDetails
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function representingColumn()
    {
        return 'id';
    }

    /**
     * Validate all required field are filled
     * @return boolean
     */
    public function beforeValidate()
    {
        if (empty($this->organization_id)) {
            $this->addError('organization_id', 'Organization is required.');
            return false;
        }

        if ($this->isNewRecord) {
            if (OrganizationDetails::model()->exists(
                'organization_id = :organizationId',
                array(':organizationId' => $this->organization_id))
            ) {
                $this->addError('organization_id', 'This organization already has a details entry.');
                return false;
            }
        } elseif (
                OrganizationDetails::model()->exists(
                    'organization_id = :organizationId AND id != :id',
                    array(':organizationId' => $this->organization_id, ':id' => $this->id)
                )
            ) {
            $this->addError('organization_id', 'This organization already has a details entry.');
            return false;
        }
        return parent::beforeValidate();
    }

    /**
     * Update SalesForce if the Client Support Tier changed
     * @return mixed
     */
    public function beforeSave()
    {
        // notify salesforce if necessary
        if (!$this->isNewRecord &&
            !empty($this->oldRecord) && $this->oldRecord->client_support_tier != $this->client_support_tier) {
            // we need to notify sf because the value changed, look up the org
            $organizationObj = Organization::model()->findByPk($this->organization_id);
            // could we find the org?
            if (!empty($organizationObj)) {
                // yes, build data
                $sfData = array(
                    'salesforce_id' => $organizationObj->salesforce_id,
                    'field' => 'Client_Support_Tier__c',
                    'value' => $this->client_support_tier
                );
                // put notifying sf in the sqs queue
                SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);
            }
        }
        return parent::beforeSave();
    }

    /**
     * Get all the organizations that have the Healthgrades import flags
     * and a sponsor code.
     *
     * @return array
     */
    public function getHealthgradesOrganizations()
    {
        return OrganizationDetails::model()
            ->with(
                array(
                    'organization' => array(
                        'together' => true,
                        'joinType' => 'INNER JOIN'
                    ),
                )
            )->findAll(
                'healthgrades_api_enabled = 1' .
                ' AND (healthgrades_api_code IS NOT NULL OR healthgrades_api_providers IS NOT NULL)'
            );
    }

    /**
     * Sets the BAA flag to 1, meaning the user has accepted the BAA
     *
     */
    public function acceptBaa()
    {
        // Set status to 1 (accepted)
        $this->baa_accepted = 1;
        return $this->save();
    }

    /**
     * Add additional search conditions.
     *
     * @return \CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        // $criteria->compare('organization_id', $this->organization_id); disabled, must be compared to org name
        $criteria->compare('send_monthly_report', $this->send_monthly_report);
        $criteria->compare('healthgrades_api_code', $this->healthgrades_api_code, true);
        $criteria->compare('healthgrades_api_enabled', $this->healthgrades_api_enabled);
        $criteria->compare('healthgrades_api_providers', $this->healthgrades_api_providers, true);
        $criteria->compare('additional_information', $this->additional_information, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('baa_first_email_date_sent', $this->baa_first_email_date_sent, true);
        $criteria->compare('baa_email_times_sent', $this->baa_email_times_sent);
        $criteria->compare('client_support_tier', $this->client_support_tier, true);
        $criteria->compare('baa_accepted', $this->baa_accepted);
        $criteria->compare('scan_template_type_id', $this->scan_template_type_id);
        $criteria->compare('practice_only_scan', $this->practice_only_scan);
        $criteria->compare('allow_mass_deletion', $this->allow_mass_deletion);

        $criteria->join .= 'LEFT JOIN organization ON organization.id = t.organization_id';
        $criteria->compare('organization.organization_name', $this->organization_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }
}
