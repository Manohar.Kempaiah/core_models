<?php

Yii::import('application.modules.core_models.models._base.BaseScanResultProvider');
Yii::import('application.modules.core_models.models._interfaces.GenericScanResult');

class ScanResultProvider extends BaseScanResultProvider implements GenericScanResult
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('scan_request_provider_id', 'required'),
            array('scan_request_provider_id, listing_type_id, is_featured, review_number, match_name, match_phone, match_address', 'numerical', 'integerOnly' => true),
            array('review_average', 'length', 'max' => 10),
            array('match_specialty', 'length', 'max' => 45),
            array('url, scraped_name, scraped_phone, scraped_address, scraped_specialty', 'length', 'max' => 255),
            array('listing_type_id, is_featured, review_number, review_average, match_name, match_phone, match_address, match_specialty, url, scraped_name, scraped_phone, scraped_address, scraped_specialty', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, scan_request_provider_id, listing_type_id, is_featured, review_number, review_average, match_name, match_phone, match_address, match_specialty, url, scraped_name, scraped_phone, scraped_address, scraped_specialty', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        $relations = parent::relations();
        $relations['listingType'] = [self::BELONGS_TO, 'ListingType', 'listing_type_id'];

        return $relations;
    }

    public function getScrapedName()
    {
        return $this->scraped_name;
    }

    public function getScrapedAddress()
    {
        return $this->scraped_phone;
    }

    public function getScrapedPhone()
    {
        return $this->scraped_phone;
    }

    /**
     * Determine if the type of the result is 'active' or not
     *
     * This is as convenience method to check if the current object
     * is an instance of 'ScanResultProvider' or not.
     * We are assuming that an instance of ScanResultProvider is
     * always an active type.
     *
     * @return boolean
     */
    public function isActive()
    {
        if (get_class($this) === 'ScanResultProvider') {
            return true;
        }

        return false;
    }
}
