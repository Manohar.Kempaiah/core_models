<?php

Yii::import('application.modules.core_models.models._base.BaseAccountPracticeEmr');

class AccountPracticeEmr extends BaseAccountPracticeEmr
{

    const DEFAULT_AVOID_APP_WITHIN = 168; // 168hs -> 7days

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function byAccount($accountId)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->getTableAlias(false) . '.account_id = :account_id',
            'params' => [':account_id' => $accountId],
        ]);
        return $this;
    }

    /**
     * Save AuditLog after delete
     */
    public function afterDelete()
    {
        AuditLog::create(
            'D',
            'Delete record',
            'account_practice_emr',
            'Practice: ' . $this->emr_location_data . ' (' . $this->practice_id . ')',
            false,
            $this->account_id,
            true
        );

        // update scheduling if necessary
        if (!empty($this->practice_id) || !empty($this->account_id)) {
            // async update
            SqsComponent::sendMessage('updateBooking', json_encode(['practice_id' => $this->practice_id ?? null, 'account_id' => $this->account_id ?? null]));
        }

        return parent::afterDelete();
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'account_id' => 'Account',
            'emr_location_id' => Yii::t('app', 'EMR Location'),
            'emr_location_data' => Yii::t('app', 'EMR Location Data'),
            'account_practice_id' => Yii::t('app', 'Account-Practice'),
            'practice_id' => 'Practice',
            'pre_appt_reminder_mail' => Yii::t('app', 'Pre-Appt. Reminder via Email'),
            'pre_appt_reminder_sms' => Yii::t('app', 'Pre-Appt. Reminder via SMS'),
            'pre_appt_reminder_autocall' => Yii::t('app', 'Pre-Appt. Reminder via Auto-call'),
            'pre_appt_reminder_staffcall' => Yii::t('app', 'Pre-Appt. Reminder via Staff Call'),
            'date_reminder_agreement' => Yii::t('app', 'Date Reminder Agreement'),
            'copy_reminder_agreement' => Yii::t('app', 'Copy Reminder Agreement'),
            'account_reminder_agreement' => Yii::t('app', 'Account Reminder Agreement'),
            'account' => 'Account',
            'practice' => 'Practice',
        );
    }

    /**
     * Checks that there are not active appointment widgets before disabling the emr_data for a provider.
     * @return boolean
     */
    public function beforeSave()
    {
        $accountId = $this->account_id;
        $cnt = 0;
        // are we un-setting the practice? (disabling)
        if (!empty($this->oldRecord->practice_id) && empty($this->practice_id)) {
            // yes
            // check for active widgets for that practice
            $sql = sprintf(
                "SELECT COUNT(1)
                FROM provider_practice_widget
                INNER JOIN widget
                ON provider_practice_widget.widget_id = widget.id
                WHERE
                (
                    widget.widget_type = 'GET_APPOINTMENT'
                    OR
                    (
                        widget.widget_type = 'MOBILE_ENHANCE'
                        AND
                        widget.options NOT LIKE '%%\"hide_appointment_button\":\"1\"%%'
                    )
                )
                AND widget.draft = 0
                AND widget.hidden = 0
                AND provider_practice_widget.practice_id = %d
                AND widget.account_id = %d ;",
                $this->oldRecord->practice_id,
                $accountId
            );

            $cnt = Yii::app()->db->createCommand($sql)->queryScalar();
            if ($cnt > 0) {
                $this->addError(
                    'practice_id',
                    'Please disable the appointment widgets for this practice before turning this feature off.'
                );
                return false;
            }
        }

        if ($this->isNewRecord) {
            if (empty($this->avoid_app_within)) {
                $this->avoid_app_within = AccountPracticeEmr::DEFAULT_AVOID_APP_WITHIN;
            }
        }

        return parent::beforeSave();
    }

    /**
     * Save changes to audit log
     *
     * @return void
     */
    public function afterSave()
    {

        if ($this->isNewRecord) {
            AuditLog::create('C', 'Account Practice EMR', 'account_practice_emr', 'Practice #' . $this->practice_id, false, $this->account_id, true);
        } else {
            AuditLog::create('U', 'Account Practice EMR', 'account_practice_emr', 'Practice #' . $this->practice_id, false, $this->account_id, true);
        }

        // update scheduling if necessary
        if ($this->isNewRecord
            ||
            (!empty($this->oldRecord) && $this->oldRecord->emr_location_id != $this->emr_location_id)
            ||
            (!empty($this->oldRecord) && $this->oldRecord->practice_id != $this->practice_id)
            ||
            (!empty($this->oldRecord) && $this->oldRecord->account_id != $this->account_id)
        ) {
            if (!empty($this->practice_id) || !empty($this->account_id)) {
                // async update
                SqsComponent::sendMessage('updateBooking', json_encode(['practice_id' => $this->practice_id ?? null, 'account_id' => $this->account_id ?? null]));
            }
            if ((!empty($this->oldRecord->practice_id) && $this->oldRecord->practice_id != $this->practice_id)
                ||
                (!empty($this->oldRecord->account_id) && $this->oldRecord->account_id != $this->account_id)) {
                // async update the previous practice_id
                SqsComponent::sendMessage('updateBooking', json_encode(['practice_id' => $this->oldRecord->practice_id, 'account_id' => $this->oldRecord->account_id]));
            }
        }

        return parent::afterSave();
    }

    /**
     * DA: Gets a provider and practice EMR data
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function getByProviderPractice($providerId, $practiceId)
    {
        $sql = sprintf(
            "SELECT emr_location_id, emr_user_id, avoid_app_within
            FROM account_practice_emr
            INNER JOIN provider_practice
            ON account_practice_emr.practice_id = provider_practice.practice_id
            INNER JOIN account_provider_emr
            ON account_provider_emr.provider_id = provider_practice.provider_id
            WHERE account_practice_emr.account_id = account_provider_emr.account_id
            AND account_practice_emr.practice_id = %d
            AND account_provider_emr.provider_id = %d;",
            $practiceId,
            $providerId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Gets practice EMR data related to this provider
     * @param int $providerId
     * @return array
     */
    public static function getByProvider($providerId)
    {
        $sql = sprintf(
            "SELECT SQL_NO_CACHE account_practice_emr.practice_id, practice.name
            FROM account_practice_emr
            INNER JOIN provider_practice
            ON account_practice_emr.practice_id = provider_practice.practice_id
            INNER JOIN account_provider_emr
            ON account_provider_emr.provider_id = provider_practice.provider_id
            INNER JOIN practice
            ON practice.id = account_practice_emr.practice_id
            WHERE account_practice_emr.account_id= account_provider_emr.account_id
            AND account_provider_emr.provider_id = %d;",
            $providerId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get all the practices associated with the given account and their EMR parameters if defined
     * @param int $accountId
     * @return array
     */
    public static function getByAccountId($accountId)
    {
        $sql = sprintf(
            "SELECT p.id, localization_id, country_id, verified, unpublished,
            is_test, `name`, practice_group_id, health_system_id, address_full, address, address_2,
            location_id, latitude, longitude, phone_number, fax_number, score, email, website,
            handicap_accesible, aesthetic_procedure, description, friendly_url, visits_number,
            parent_practice_id, logo_path, p.date_added, p.date_updated, date_umd_updated, phone_number_clean,
            out_of_network, facility_fee, npi_id, yelp_sync_approved, yext_location_id, yelp_business_id,
            dme_services, MAX(ape.id) AS ape_id, account_id, emr_location_id, emr_location_data,
            ape.practice_id, send_appointment_reminders, avoid_app_within, pre_appt_reminder_mail,
            pre_appt_reminder_sms, pre_appt_reminder_autocall, pre_appt_reminder_staffcall, fb_page_id,
            date_reminder_agreement, copy_reminder_agreement, account_reminder_agreement,
            fb_page_id  AS profile_name
            FROM practice p
            INNER JOIN account_practice_emr ape ON p.id = ape.practice_id
            LEFT JOIN practice_details ON p.id = practice_details.practice_id
            WHERE ape.account_id = %d
            AND practice_details.practice_type_id != 5
            GROUP BY p.id;",
            $accountId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get Appointment Reminders
     * @param int $ownerAccountId
     * @param bool $useCache
     * @return array
     */
    public static function getAppointmentReminders($ownerAccountId, $useCache = true)
    {
        $sql = sprintf(
            "SELECT account_practice_emr.id, account_practice_emr.account_id,
            CAST(emr_location_id AS UNSIGNED) AS emr_location_id, emr_location_data,
            practice_id, send_appointment_reminders,
            avoid_app_within, pre_appt_reminder_mail, pre_appt_reminder_sms, pre_appt_reminder_autocall,
            pre_appt_reminder_staffcall, fb_page_id, date_reminder_agreement, copy_reminder_agreement,
            account_reminder_agreement, address_full, practice.name AS practice_name
            FROM account_practice_emr
            left join practice ON account_practice_emr.practice_id = practice.id
            WHERE account_id = %d",
            $ownerAccountId
        );

        $cacheTime = $useCache ? Yii::app()->params['cache_short'] : 0;

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Get all the practices associated with the given account and their EMR parameters if defined
     * @param int $accountId
     * @return array
     */
    public static function getPractices($accountId)
    {
        $sql = sprintf(
            "SELECT account_practice_emr.id, account_id, CAST(emr_location_id AS UNSIGNED) AS emr_location_id,
            emr_location_data, account_practice_emr.practice_id, send_appointment_reminders, avoid_app_within,
            pre_appt_reminder_mail,
            pre_appt_reminder_sms, pre_appt_reminder_autocall, pre_appt_reminder_staffcall, fb_page_id,
            date_reminder_agreement, copy_reminder_agreement, account_reminder_agreement,
            address_full, practice.name AS practice_name
            FROM account_practice_emr
            LEFT join practice
            ON account_practice_emr.practice_id = practice.id
            LEFT JOIN practice_details
            ON practice.id = practice_details.practice_id
            WHERE account_id = %d
            AND (practice_type_id != 5 OR practice.id IS NULL);",
            $accountId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Find the practice associated with this facebook page id
     * @param string $fbPageId
     * @return int $providerId
     */
    public static function getByFacebookPageId($fbPageId = '')
    {
        $practiceId = 0;
        $fbPageId = trim($fbPageId);
        if (!empty($fbPageId)) {
            $sql = sprintf(
                "SELECT practice_id
                FROM account_practice_emr ape
                WHERE ape.fb_page_id = '%s'",
                $fbPageId
            );

            $practiceId = Yii::app()->db->createCommand($sql)->queryScalar();
        }
        return $practiceId;
    }

    /**
     * Get all practices that belong to a given account and are linked to providers
     * @param int $accountId
     * @return arr
     */
    public static function getAccountPracticesWithProvidersEmr($accountId = 0)
    {
        if ($accountId > 0) {
            $sql = sprintf(
                "SELECT DISTINCT practice.id AS practice_id, practice.name AS practice_name,
                practice.address AS practice_address
                FROM account_practice_emr
                INNER JOIN practice ON account_practice_emr.practice_id = practice.id
                INNER JOIN provider_practice ON practice.id = provider_practice.practice_id
                INNER JOIN account_provider_emr ON provider_practice.provider_id = account_provider_emr.provider_id
                WHERE account_practice_emr.account_id = %d
                ORDER BY practice_name;",
                $accountId
            );

            return Yii::app()->db->createCommand($sql)->queryAll();
        }
        return false;
    }

    /**
     * Count How Many Practices has this account in this location
     * @param int $accountId
     * @param int $emrId
     * @return int
     */
    public static function countPractices($accountId = 0, $emrId = 0)
    {
        $cnt = 0;
        if ($accountId > 0 && $emrId > 0) {

            $sql = sprintf(
                "SELECT COUNT(*) AS cnt"
                    . " FROM account_practice_emr"
                    . " WHERE account_id = '%d' AND emr_location_id != '' AND emr_location_id IS NOT NULL "
                    . "AND practice_id IS NOT NULL",
                $accountId,
                $emrId
            );

            $cnt = (int)Yii::app()->db->createCommand($sql)->queryScalar();
        }
        return $cnt;
    }

    /**
     * Count How Many Practices has this account
     * @param int $accountId
     * @return int
     */
    public static function countAccountPractices($accountId = 0)
    {
        $cnt = 0;
        if ($accountId > 0) {

            $sql = sprintf(
                "SELECT COUNT(*) AS cnt"
                    . " FROM account_practice_emr"
                    . " WHERE account_id = '%d'",
                $accountId
            );

            $cnt = (int)Yii::app()->db->createCommand($sql)->queryScalar();
        }
        return $cnt;
    }

    /**
     * Save appointment Reminders
     * @param array $postData
     * @param int $ownerAccountId
     * @return array
     * @throws Exception
     */
    public static function saveData($postData = array(), $ownerAccountId = 0)
    {
        $results['success'] = true;
        $results['error'] = '';
        $results['data'] = [];

        if (empty($postData) || empty($ownerAccountId)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        foreach ($postData as $value) {
            $data = array();
            if (!empty($value['id']) && $value['id'] > 0) {
                $data = AccountPracticeEmr::model()->findByPk($value['id']);
            } else {
                $data = AccountPracticeEmr::model()->find(
                    'account_id=:accountId AND practice_id=:practiceId',
                    array(':accountId' => $ownerAccountId, ':practiceId' => $value['practice_id'])
                );
            }

            if (empty($data)) {
                $data = new AccountPracticeEmr();
                $data->practice_id = $value['practice_id'];
                $data->account_id = $ownerAccountId;
            }

            $data->avoid_app_within = $value['avoid_app_within'];
            $data->pre_appt_reminder_mail = !empty($value['pre_appt_reminder_mail_enabled']) ?
                $value['pre_appt_reminder_mail'] : 0;
            $data->pre_appt_reminder_sms = !empty($value['pre_appt_reminder_sms_enabled']) ?
                $value['pre_appt_reminder_sms'] : 0;
            $data->pre_appt_reminder_autocall = !empty($value['pre_appt_reminder_autocall_enabled']) ?
                $value['pre_appt_reminder_autocall'] : 0;
            $data->pre_appt_reminder_staffcall = !empty($value['pre_appt_reminder_staffcall_enabled']) ?
                $value['pre_appt_reminder_staffcall'] : 0;

            if (!isset($value['send_appointment_reminders'])) {
                $data->send_appointment_reminders = 0;
            }
            if ($data->send_appointment_reminders == 1 && $data->date_reminder_agreement == null) {
                $globalParameter = GlobalParameter::model()->find('`key`="ehr_reminder_agreement"')->value;
                $data->date_reminder_agreement = date("Y-m-d");
                $data->copy_reminder_agreement = $globalParameter;
                $data->account_reminder_agreement = $ownerAccountId;
            } else {
                $data->date_reminder_agreement = null;
                $data->copy_reminder_agreement = null;
                $data->account_reminder_agreement = null;
            }

            if ($data->save()) {

                // Since we want to have the same settings in all records, we will look for all other records that exist
                // and set the same settings as in the one we just saved
                $otherData = AccountPracticeEmr::model()->findAll(
                    'account_id<>:accountId AND practice_id=:practiceId',
                    array(':accountId' => $data->account_id, ':practiceId' => $data->practice_id)
                );

                if (!empty($otherData)) {
                    // Loop through the object and set the same settings
                    foreach ($otherData as $setting) {
                        $setting->pre_appt_reminder_mail = $data->pre_appt_reminder_mail;
                        $setting->pre_appt_reminder_sms = $data->pre_appt_reminder_sms;
                        $setting->pre_appt_reminder_autocall = $data->pre_appt_reminder_autocall;
                        $setting->pre_appt_reminder_staffcall = $data->pre_appt_reminder_staffcall;
                        $setting->save();
                    }
                }

                // Save new values to the CapybaraCache
                CapybaraCache::setAvoidAppointmentWithin($data->practice_id, $data->avoid_app_within);

                $results['success'] = true;
                $results['error'] = "";
                $resultsData = (array)$data->attributes;

                $sql = sprintf(
                    "SELECT practice.name AS practice_name, practice.address_full AS address_full
                    FROM practice
                    WHERE id = %d",
                    $value['practice_id']
                );
                $practice = Yii::app()->dbRO->createCommand($sql)->queryRow();
                $resultsData['practice_name'] = $practice['practice_name'];
                $resultsData['address_full'] = $practice['address_full'];

                $results['data'][] = $resultsData;
            } else {
                $results['success'] = false;
                $results['error'] = "ERROR_SAVING_APPOINTMENT_REMINDER";
                $results['data'] = $data->getErrors();
                return $results;
            }
        }
        return $results;
    }

    /**
     * get all Practices for this account
     * @param int $accountId
     * @return object
     */
    public static function getAllByAccountId($accountId = 0)
    {
        return  AccountPracticeEmr::model()->findAll(
            "account_id = :account_id",
            array(":account_id" => $accountId)
        );
    }
}
