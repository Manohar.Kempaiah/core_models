<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPractice');

class ProviderPractice extends BaseProviderPractice
{

    /**
     * @var bool value to do device and widget validation in beforeDelete()
     */
    public $accountDeviceWidget = true;

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        $providerId = $this->provider_id;
        $practiceId = $this->practice_id;

        $redisKey = 'provider_practice_beforeDelete:' . $providerId . '-' . $practiceId;
        Yii::app()->redis->exec('DEL', array($redisKey));

        $accountId = (php_sapi_name() != "cli" && !empty(Yii::app()->user->id)) ? Yii::app()->user->id : 1;

        if (empty($accountId)) {
            $getOwner = true; // return the owner_account_id
            $accountId = Account::getAccountBasedOnProviderAndPractice($providerId, $practiceId, $getOwner);
        }

        ini_set('max_execution_time', 0);

        // If account_id exists
        if (!empty($accountId)) {

            // CPN Validation
            $sql = sprintf(
                "SELECT cpn_installation.id
                FROM cpn_installation
                INNER JOIN cpn_installation_provider
                    ON cpn_installation_provider.cpn_installation_id = cpn_installation.id
                INNER JOIN cpn_ehr_installation ON cpn_installation.id = cpn_ehr_installation.cpn_installation_id
                WHERE cpn_installation_provider.provider_id= %d
                    AND cpn_installation.account_id = %d
                    AND cpn_installation.practice_id = %d
                    AND cpn_installation.enabled_flag = 1
                    AND cpn_installation.del_flag = 0
                    AND cpn_ehr_installation.enabled_flag = 1
                    AND cpn_ehr_installation.del_flag = 0
                    AND cpn_installation_user_id IS NOT NULL; ",
                $providerId,
                $accountId,
                $practiceId
            );

            $existsCpn = Yii::app()->db->createCommand($sql)->queryScalar();
            if (!empty($existsCpn)) {
                $errorTxt = "This provider has an active Companion installation. "
                    . "Please deactivate it before unlinking the provider.";

                $this->addError("id", $errorTxt);
                $this->addError("provider_id", $errorTxt);
                $this->addError("practice_id", $errorTxt);
                $this->addError("cpn_validation", true);
                if (php_sapi_name() != 'cli') {
                    Yii::app()->user->setFlash('error', $errorTxt);
                }
                if (Yii::app()->request->IsAjaxRequest) {
                    header("HTTP/1.0 409 Conflict");
                } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                    Yii::app()->user->setFlash('error', $errorTxt);
                }
                return false;
            }

            // If is the unique practice for this provider, make some validations
            $arrPractices = $this->getProvidersPractices($providerId, $accountId, 'workingOn');

            if (count($arrPractices) == 1) {
                // This provider has fb_page_id?
                $hasFacebookPage = AccountProviderEmr::model()->exists(
                    '(fb_page_id IS NOT NULL OR fb_page_id > 0) AND account_id=:accountId AND provider_id=:providerId',
                    array(':accountId' => $accountId, ':providerId' => $providerId)
                );
                if (!empty($hasFacebookPage)) {

                    $errorTxt = "This provider has Social Publishing enabled. "
                        . "Please disconnect any social media accounts and try again.";
                    Yii::app()->redis->exec('SET', array(
                        $redisKey,
                        "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                    ));
                    $this->addError("id", $errorTxt);
                    $this->addError("provider_id", $errorTxt);
                    $this->addError("practice_id", $errorTxt);

                    if (Yii::app()->request->IsAjaxRequest) {
                        header("HTTP/1.0 409 Conflict");
                    } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                        Yii::app()->user->setFlash(
                            'error',
                            "<strong>ERROR: </strong>This provider has an active Facebook
                            Integration and this is the only active Practice for him. Please
                            <a href='/myaccount/accountFacebookIntegration'>deactivate Facebook</a>
                            before deleting this."
                        );
                    }
                    return false;
                }
            }

            // If is the unique provider for this practice, make some validations
            $q = ProviderPractice::model()->count('practice_id=:practiceId', array(':practiceId' => $practiceId));
            if ($q == 1) {
                // This practice has fb_page_id?
                $hasFacebookPage = AccountPracticeEmr::model()->exists(
                    '(fb_page_id IS NOT NULL OR fb_page_id > 0) AND account_id=:accountId AND practice_id=:practiceId',
                    array(':accountId' => $accountId, ':practiceId' => $practiceId)
                );
                if (!empty($hasFacebookPage)) {

                    $errorTxt = "This practice has Social Publishing enabled. "
                        . "Please disconnect any social media accounts and try again.";
                    Yii::app()->redis->exec('SET', array(
                        $redisKey,
                        "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                    ));
                    $this->addError("id", $errorTxt);
                    $this->addError("provider_id", $errorTxt);
                    $this->addError("practice_id", $errorTxt);

                    if (Yii::app()->request->IsAjaxRequest) {
                        header("HTTP/1.0 409 Conflict");
                    } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                        Yii::app()->user->setFlash(
                            'error',
                            "<strong>ERROR: </strong>This practice has an active Facebook
                            Integration and this is the only active Provider. Please
                            <a href='/myaccount/accountFacebookIntegration'>deactivate Facebook</a>
                            before deleting this."
                        );
                    }
                    return false;
                }
            }
        }

        // first, remove digital RRs automatically
        $whereCondition = '';
        if (!empty($accountId)) {
            $whereCondition = sprintf(" AND account_device.account_id = %d", $accountId);
        }

        $sql = sprintf(
            "SELECT account_device.id
            FROM provider_practice
            INNER JOIN account_device
                ON provider_practice.practice_id = account_device.practice_id
            INNER JOIN account_device_provider
                ON account_device_provider.provider_id = provider_practice.provider_id
                AND account_device_provider.account_device_id = account_device.id
            LEFT JOIN device ON account_device.device_id = device.id
            LEFT JOIN provider_rating
                ON provider_rating.account_device_id = account_device.id
            WHERE device_model_id = 7
                %s
                AND provider_practice.provider_id = %d
                AND provider_practice.practice_id = %d
                AND provider_rating.id IS NULL;",
            $whereCondition,
            $providerId,
            $practiceId
        );
        $accountDevices = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($accountDevices)) {
            foreach ($accountDevices as $accountDevice) {
                // delete from account_device_provider before deleting from account_device
                $adps = AccountDeviceProvider::model()->findAll(
                    'account_device_id=:accountDeviceId',
                    [':accountDeviceId' => $accountDevice['id']]
                );
                if (!empty($adps)) {
                    foreach ($adps as $adp) {
                        $adp->delete();
                    }
                }
                // delete from patientemail before deleting from account_device
                $pes = Patientemail::model()->findAll(
                    'account_device_id=:accountDeviceId',
                    [':accountDeviceId' => $accountDevice['id']]
                );
                if (!empty($pes)) {
                    foreach ($pes as $pe) {
                        $pe->delete();
                    }
                }

                $accountDeviceToDelete = AccountDevice::model()->findByPk($accountDevice['id']);
                if (!empty($accountDeviceToDelete) && !$accountDeviceToDelete->delete()) {
                    $this->addError("id", "Couldn't delete RR.");
                    return false;
                }
            }
        }

        // now, do we still have devices linked to the practice<->provider ?
        $sql = sprintf(
            "SELECT account_device.id, provider_practice.id as pp_id, device_model_id
            FROM provider_practice
            INNER JOIN account_device
                ON provider_practice.practice_id = account_device.practice_id
            INNER JOIN account_device_provider
                ON account_device_provider.provider_id = provider_practice.provider_id
                AND account_device_provider.account_device_id = account_device.id
            LEFT JOIN device ON account_device.device_id = device.id
            WHERE account_device.status = 'A'
                %s
                AND provider_practice.provider_id = %d
                AND provider_practice.practice_id = %d;",
            $whereCondition,
            $providerId,
            $practiceId
        );
        $accountDevice = Yii::app()->db->createCommand($sql)->queryRow();

        if (!empty($accountDevice['id'])) {
            $deviceType = 'ReviewHub';
            if ($accountDevice['device_model_id'] == DeviceModel::REVIEW_REQUEST_ID) {
                $deviceType = 'ReviewRequest';
            }

            $errorTxt = "This provider/practice has " . $deviceType . " associations. "
                . "Please remove the provider from " . $deviceType . " and try again.";

            Yii::app()->redis->exec('SET', array(
                $redisKey,
                "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
            ));
            $this->addError("id", $errorTxt);
            $this->addError("provider_id", $errorTxt);
            $this->addError("practice_id", $errorTxt);
            if (Yii::app()->request->IsAjaxRequest) {
                header("HTTP/1.0 409 Conflict");
            } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {

                Yii::app()->user->setFlash(
                    'error',
                    "<strong>ERROR: </strong>Devices are linked to this practice<->provider."
                );
            }
            return false;
        }

        if (Common::isTrue($this->accountDeviceWidget)) {
            // This relations has any widget?
            Widget::clearDraftWidgets($accountId, $providerId, $practiceId);
            $arrProviderPracticeWidget = ProviderPracticeWidget::getWidgetFromProviderPracticeAccount(
                $providerId,
                $practiceId,
                $accountId
            );

            if (!empty($arrProviderPracticeWidget)) {

                $errorTxt = "This provider/practice is associated with an appointment widget. "
                    . "Please remove the widget and try again.";

                Yii::app()->redis->exec('SET', array(
                    $redisKey,
                    "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                ));
                $this->addError("id", $errorTxt);
                $this->addError("provider_id", $errorTxt);
                $this->addError("practice_id", $errorTxt);

                if (Yii::app()->request->IsAjaxRequest) {
                    header("HTTP/1.0 409 Conflict");
                } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                    Yii::app()->user->setFlash(
                        'error',
                        "<strong>ERROR: </strong>This relationship (practice<->provider)"
                            . " is involved in a widget: <a data-who='practice' data-id='$practiceId'"
                            . " id='showErrorProviderPracticeUnlink' href='#'>See details</a>"
                    );
                }
                return false;
            }
        }

        // If account_id exists
        if (!empty($accountId)) {

            // Find all account related to this account
            $accountList = OrganizationAccount::getAllAccountId($accountId, 0, true);

            // Force delete widget for all related account
            $sql = sprintf(
                "SELECT widget.id
                FROM provider_practice_widget AS ppw
                LEFT JOIN widget ON widget.id = ppw.widget_id
                WHERE ppw.provider_id = %d
                    AND ppw.practice_id = %d
                    AND widget.account_id IN (%s);",
                $providerId,
                $practiceId,
                $accountList
            );

            $hiddenWidgets = Yii::app()->db->createCommand($sql)->queryAll();
            foreach ($hiddenWidgets as $hiddenWidget) {
                $widget = Widget::model()->findByPk($hiddenWidget['id']);
                if (!empty($widget) && !$widget->delete()) {
                    $errorTxt = "Couldn't delete Widget id: " . $hiddenWidget['id'];
                    Yii::app()->redis->exec(
                        'SET',
                        array(
                            $redisKey,
                            "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                        )
                    );
                    $this->addError("id", $errorTxt);
                    $this->addError("provider_id", $errorTxt);
                    $this->addError("practice_id", $errorTxt);
                    return false;
                }
            }
        }

        // Delete twilio numbers
        $sql = sprintf(
            "SELECT partner_site_id
            FROM partner_site_provider_featured
            WHERE provider_id=%d
                AND active='1'; ",
            $providerId
        );
        $providerPSPF = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($providerPSPF as $pspf) {
            $partnerSiteId = $pspf['partner_site_id'];
            if (!PartnerSiteProviderFeatured::model()->deleteTwilioNumbers($partnerSiteId, $providerId, $practiceId)) {
                $this->addError("id", "Couldn't delete Twilio number.");
                return false;
            }
        }

        // Be sure to delete twilio numbers
        $arrTwilioNumbers = TwilioNumber::model()->findAll(
            "provider_id = :pro_id AND practice_id=:pra_id",
            array(":pro_id" => $providerId, ":pra_id" => $practiceId)
        );
        foreach ($arrTwilioNumbers as $twilioNumber) {
            if (!$twilioNumber->delete()) {
                $errorTxt = "Couldn't delete Twilio number.";
                Yii::app()->redis->exec('SET', array(
                    $redisKey,
                    "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                ));
                $this->addError("id", $errorTxt);
                $this->addError("provider_id", $errorTxt);
                $this->addError("practice_id", $errorTxt);
                return false;
            }
        }

        Yii::app()->redis->exec('SET', array(
            $redisKey,
            "Provider:" . $providerId . ", Practice:" . $practiceId . ": Couldn't delete schedule"
        ));
        // Delete records from provider_practice_listing before delete this provider_practice id
        $arrProviderPracticeListing = ProviderPracticeListing::model()->findAll(
            "provider_id = :pro_id AND practice_id=:pra_id",
            array(":pro_id" => $providerId, ":pra_id" => $practiceId)
        );
        foreach ($arrProviderPracticeListing as $providerPracticeListing) {
            if (!$providerPracticeListing->delete()) {
                $errorTxt = "Couldn't delete links.";
                Yii::app()->redis->exec('SET', array(
                    $redisKey,
                    "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                ));
                $this->addError("id", $errorTxt);
                $this->addError("provider_id", $errorTxt);
                $this->addError("practice_id", $errorTxt);
                return false;
            }
        }

        // Delete records from provider_practice_insurance before delete this provider_practice id
        $arrProviderPracticeInsurance = ProviderPracticeInsurance::model()->findAll(
            "provider_id = :pro_id AND practice_id=:pra_id",
            array(":pro_id" => $providerId, ":pra_id" => $practiceId)
        );
        foreach ($arrProviderPracticeInsurance as $providerPracticeInsurance) {
            if (!$providerPracticeInsurance->delete()) {
                $errorTxt = "Couldn't delete insurance.";
                Yii::app()->redis->exec('SET', array(
                    $redisKey,
                    "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                ));
                $this->addError("id", $errorTxt);
                $this->addError("provider_id", $errorTxt);
                $this->addError("practice_id", $errorTxt);
                return false;
            }
        }

        // Delete records from provider_practice_visit_reason before delete this provider_practice id
        $arrProviderPracticeVisitReason = ProviderPracticeVisitReason::model()->findAll(
            "provider_id = :pro_id AND practice_id=:pra_id",
            array(":pro_id" => $providerId, ":pra_id" => $practiceId)
        );
        foreach ($arrProviderPracticeVisitReason as $providerPracticeVisitReason) {
            if (!$providerPracticeVisitReason->delete()) {
                $errorTxt = "Couldn't delete visit reasons.";
                Yii::app()->redis->exec('SET', array(
                    $redisKey,
                    "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                ));
                $this->addError("id", $errorTxt);
                $this->addError("provider_id", $errorTxt);
                $this->addError("practice_id", $errorTxt);
                return false;
            }
        }

        // delete from EmrScheduleException
        $arrESE = EmrScheduleException::model()->findAll(
            'provider_id = :provider_id AND practice_id = :practice_id',
            array(':provider_id' => $providerId, ':practice_id' => $practiceId)
        );
        if (!empty($arrESE)) {
            foreach ($arrESE as $eachRecord) {
                if (!$eachRecord->delete()) {
                    $errorTxt = "Couldn't delete schedule exceptions.";
                    Yii::app()->redis->exec('SET', array(
                        $redisKey,
                        "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                    ));
                    $this->addError("id", $errorTxt);
                    $this->addError("provider_id", $errorTxt);
                    $this->addError("practice_id", $errorTxt);
                    return false;
                }
            }
        }

        // Delete records from provider_practice_schedule before delete this provider_practice id
        $arrProviderPracticeSchedules = ProviderPracticeSchedule::model()->findAll(
            "provider_practice_id = :pp_id",
            array(":pp_id" => $this->id)
        );
        foreach ($arrProviderPracticeSchedules as $providerPracticeSchedule) {
            if (!$providerPracticeSchedule->delete()) {
                $errorTxt = "Couldn't delete schedule.";
                Yii::app()->redis->exec('SET', array(
                    $redisKey,
                    "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                ));
                $this->addError("id", $errorTxt);
                $this->addError("provider_id", $errorTxt);
                $this->addError("practice_id", $errorTxt);
                return false;
            }
        }

        // delete from ProviderPracticeSpecialty
        $arrPPS = ProviderPracticeSpecialty::model()->findAll(
            'provider_id = :provider_id AND practice_id = :practice_id',
            array(':provider_id' => $providerId, ':practice_id' => $practiceId)
        );
        if (!empty($arrPPS)) {
            foreach ($arrPPS as $eachRecord) {
                if (!$eachRecord->delete()) {
                    $errorTxt = "Couldn't delete specialty.";
                    Yii::app()->redis->exec('SET', array(
                        $redisKey,
                        "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                    ));
                    $this->addError("id", $errorTxt);
                    $this->addError("provider_id", $errorTxt);
                    $this->addError("practice_id", $errorTxt);
                    return false;
                }
            }
        }

        // delete from provider_practice_gmb
        $arrGmb = ProviderPracticeGmb::model()->findAll(
            'provider_id = :providerId AND practice_id = :practiceId',
            array(':providerId' => $providerId, ':practiceId' => $practiceId)
        );

        if (!empty($arrGmb)) {
            foreach ($arrGmb as $gmb) {
                $gmb->delete();
            }
        }

        $sql = sprintf(
            "SELECT id
            FROM organization_feature
            WHERE practice_id = %s
                AND provider_id <> %s
                AND feature_id IN ('4','14','16'); ",
            $practiceId,
            $providerId
        );
        $organizationFeature = Yii::app()->db->createCommand($sql)->queryRow();

        if (!isset($organizationFeature['id'])) {

            $arrTwilioNumbers = TwilioNumber::model()->findAll(
                'provider_id = :provider_id AND practice_id=:practice_id',
                array(':provider_id' => $providerId, ':practice_id' => $practiceId)
            );
            foreach ($arrTwilioNumbers as $twilioNumber) {
                if (!$twilioNumber->delete()) {
                    $errorTxt = "Couldn't delete phone number.";
                    Yii::app()->redis->exec('SET', array(
                        $redisKey,
                        "Provider:" . $providerId . ", Practice:" . $practiceId . ": " . $errorTxt
                    ));
                    $this->addError("id", $errorTxt);
                    $this->addError("provider_id", $errorTxt);
                    $this->addError("practice_id", $errorTxt);
                    return false;
                }
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Fix primary location if necessary, update twilio
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->isNewRecord) {
            $this->date_added = date('Y-m-d H:i:s');
            // It is the first record for this provider?
            $existProviderInPractice = $this->exists(
                'provider_id = :provider_id',
                array(':provider_id' => $this->provider_id)
            );

            if (!$existProviderInPractice) {
                $this->primary_location = 1;
            } else {
                // This provider is already associated to this practice ?
                $exist = $this->find(
                    'provider_id = :provider_id AND practice_id = :practice_id',
                    array(':provider_id' => $this->provider_id, ':practice_id' => $this->practice_id)
                );
                if ($exist) {
                    $this->addError('provider_id_lookup', 'This provider is already associated to this practice.');
                    return false;
                }
            }
        } else {
            $this->date_updated = date('Y-m-d H:i:s');
        }

        // only for new records
        if ($this->isNewRecord && $this->getScenario() == 'telemedicine') {
            // telemedicine practice always will have rank_order = 0 and primary_location = 0
            $this->rank_order = 0;
            $this->primary_location = 0;
        }

        $this->phone_number = preg_replace('/[^0-9]/', '', $this->phone_number);

        if (!$this->isNewRecord && empty($this->phone_number)) {
            // We can't allow emptying the phone number if we active twilio numbers
            if (TwilioNumber::model()->exists(
                'practice_id=:practiceId AND provider_id=:providerId AND enabled = 1',
                array(':practiceId' => $this->practice_id, ':providerId' => $this->provider_id)
            )) {
                $this->addError('phone_number', 'Please specify the practice\'s phone number.');
                return false;
            }
        }

        return parent::beforeSave();
    }

    /**
     * Return provider's Specialties from a given practice
     * @param int $practiceId
     * @return arr
     */
    public static function getProviderPracticeSpecialties($practiceId = 0)
    {
        $sql = sprintf(
            "SELECT DISTINCT
                specialty.name AS specialty_name, sub_specialty.name AS sub_specialty_name
            FROM specialty
            INNER JOIN sub_specialty
                ON specialty.id = sub_specialty.specialty_id
            INNER JOIN provider_specialty
                ON provider_specialty.sub_specialty_id= sub_specialty.id
            INNER JOIN provider_practice
                ON provider_practice.provider_id= provider_specialty.provider_id
            WHERE provider_practice.practice_id= %d
            ORDER BY specialty.name, sub_specialty.name;",
            $practiceId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    // Called on rendering the column for each row
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id . '" '
            . 'rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice . '">'
            . $this->practice . '</a>');
    }

    // Called on rendering the column for each row
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id . '" '
            . 'rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">'
            . $this->provider . '</a>');
    }

    /**
     * @todo document
     * @return string
     */
    public function getFullProviderPractice()
    {

        $provider = Provider::model()->findByPk($this->provider_id);
        $practice = Practice::model()->findByPk($this->practice_id);

        return $provider->prefix . ' ' . $provider->first_name . ' ' . $provider->last_name . ' at ' . $practice->name;
    }

    /**
     * Get details about a provider's practice
     *
     * Note: $unpublished argument can be 0 (published), 1 (unpublished) or null (both)
     *
     * @param int $providerId
     * @param int $networkId
     * @param int $unpublished
     * @return array
     */
    public function getDetails($providerId = 0, $networkId = 0, $unpublished = null)
    {

        if ($unpublished !== null) {
            if ($unpublished) {
                $sqlUnpublished = 'AND practice.unpublished = 1';
            } else {
                $sqlUnpublished = 'AND practice.unpublished = 0';
            }
        } else {
            $sqlUnpublished = '';
        }

        $sql = sprintf(
            "SELECT DISTINCT practice.*, practice.name AS practice, city.name AS city,
            state.name AS state, location.zipcode AS zipcode,
            state.abbreviation AS abbreviation,
            provider_practice.email, provider_practice.fax_number,
            provider_practice.id AS provider_practice_id,
            IF(twilio_number.twilio_number IS NOT NULL, twilio_number.twilio_number, practice.phone_number) AS pr_phone,
            practice.email AS pr_mail, practice.fax_number AS pr_fax,
            IF (twilio_number.twilio_number IS NOT NULL, '1', '0') AS is_tracked_phone_number,
            practice_type_id, coverage_location_id, coverage_radius,
            coverage_state_id, city.friendly_url AS city_friendly_url,
            state.friendly_url AS state_friendly_url
            FROM provider_practice
            LEFT JOIN practice
            ON provider_practice.practice_id = practice.id
            LEFT JOIN practice_details
            ON practice.id = practice_details.practice_id
            LEFT JOIN location
            ON practice.location_id = location.id
            LEFT JOIN city
            ON location.city_id = city.id
            LEFT JOIN state
            ON city.state_id = state.id
            LEFT JOIN twilio_number
            ON practice.id = twilio_number.practice_id
            AND twilio_number.provider_id IS NULL
            AND twilio_number.twilio_partner_id = 1
            WHERE provider_practice.provider_id = %d
            %s
            ORDER BY provider_practice.primary_location DESC;",
            $providerId,
            $sqlUnpublished
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Get Provider's practices
     * $state condition:
     *      all -> get all practice relates to this provider
     *      workingOn -> get only practices related to logged account
     * @param int $providerId
     * @param int $accountId
     * @param string $state = all || workingOn
     * @param int $cache (Seconds for query cache)
     * @return array
     */
    public static function getProvidersPractices(
        $providerId = 0,
        $accountId = 0,
        $state = 'all',
        $pager = [],
        $cache = 0
    )
    {

        if ($providerId == 0) {
            return false;
        }

        if ($accountId > 0) {
            if ($state == 'all') {
                $selectValues = "
                    practice.id as practice_id,
                    practice.name,
                    practice.location_id, practice.address, practice.address_2,
                    provider_practice.primary_location, practice.logo_path,
                    provider_practice.provider_id,
                    IF(provider_practice.id IS NULL, false, true) AS checked,
                    provider_practice.email AS pp_email,
                    provider_practice.phone_number AS pp_phone_number,
                    provider_practice.fax_number AS pp_fax_number,
                    provider_practice.id AS provider_practice_id,
                    provider_practice.rank_order,
                    provider_practice.location_details";

                $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);

                $limitClause = '';
                if ($pager) {
                    $limitClause = 'LIMIT ' . $pager['limit'];
                }

                $sqlPractices = sprintf(
                    "(SELECT %s
                    FROM practice
                    LEFT JOIN provider_practice
                        ON provider_practice.practice_id = practice.id
                        AND provider_practice.provider_id = %d
                    INNER JOIN account_practice
                        ON account_practice.practice_id = practice.id
                    LEFT JOIN practice_details
                        ON practice.id = practice_details.practice_id
                        AND practice_details.practice_type_id != 5
                    WHERE account_practice.account_id = %d
                    GROUP BY practice.id)
                    UNION ALL
                        (SELECT %s
                        FROM practice
                        LEFT JOIN provider_practice
                            ON provider_practice.practice_id = practice.id
                            AND provider_practice.provider_id = %d
                        INNER JOIN account_practice
                            ON provider_practice.practice_id = account_practice.practice_id
                        INNER JOIN account_provider
                            ON provider_practice.provider_id = account_provider.provider_id
                            AND account_practice.account_id = %d
                        LEFT JOIN practice_details
                            ON practice.id = practice_details.practice_id
                            AND practice_details.practice_type_id != 5
                        WHERE account_provider.account_id = %d
                        AND account_provider.all_practices = 1)
                    ORDER BY primary_location DESC, ISNULL(rank_order), rank_order;",
                    $selectValues,
                    $providerId,
                    $accountId,
                    $selectValues,
                    $providerId,
                    $ownerAccountId,
                    $accountId,
                    $limitClause
                );

                $arrPractices = Yii::app()->db->createCommand($sqlPractices)->queryAll();
            } elseif ($state == 'workingOn') {

                $sqlPractices = sprintf(
                    "SELECT practice.id, practice.id as practice_id, provider.first_m_last AS provider_name,
                        pp.primary_location,
                        pp.phone_number AS provider_practice_phone_number,
                        pp.fax_number AS provider_practice_fax_number,
                        pp.email AS provider_practice_email,
                        pp.location_details AS provider_practice_location_details,
                        pp.rank_order, pp.location_details, pp.id as provider_practice_id,
                        pp.operatories,
                        practice.localization_id, practice.country_id, practice.name, practice_group_id,
                        practice.health_system_id, practice.address_full, practice.address, practice.address_2,
                        practice.location_id, practice.latitude, practice.longitude, practice.phone_number,
                        practice.fax_number, practice.email, practice.website, practice.handicap_accesible,
                        practice.aesthetic_procedure, practice.description, practice.friendly_url,
                        practice.visits_number, practice.parent_practice_id, practice.logo_path,
                        practice.phone_number_clean, practice.out_of_network, practice.facility_fee, practice.npi_id,
                        practice.yelp_sync_approved, practice.yext_location_id, practice.yelp_business_id,
                        practice.dme_services
                    FROM practice
                    LEFT JOIN provider_practice AS pp
                        ON pp.practice_id = practice.id
                    LEFT JOIN provider
                        ON pp.provider_id = provider.id
                    INNER JOIN account_practice
                        ON account_practice.practice_id = practice.id
                    LEFT JOIN practice_details
                        ON practice.id = practice_details.practice_id
                        AND practice_details.practice_type_id != 5
                    WHERE pp.provider_id = %d
                        AND account_practice.account_id = %d
                    GROUP BY practice.id
                    ORDER BY primary_location DESC, ISNULL(pp.rank_order), pp.rank_order;",
                    $providerId,
                    $accountId
                );

                $arrPractices = Yii::app()->db
                    ->cache($cache)
                    ->createCommand($sqlPractices)
                    ->queryAll();
            } else {
                return false;
            }
        } else {
            $sqlPractices = sprintf(
                "SELECT provider_practice.*, practice.name AS practice_name,
                    provider.first_m_last AS provider_name,
                    provider_practice.id AS provider_practice_id
                FROM provider_practice
                LEFT JOIN practice
                    ON provider_practice.practice_id = practice.id
                LEFT JOIN provider
                    ON provider_practice.provider_id = provider.id
                LEFT JOIN practice_details
                    ON practice.id = practice_details.practice_id
                    AND practice_details.practice_type_id != 5
                WHERE provider_id = %d
                ORDER BY provider_practice.rank_order;",
                $providerId
            );
            $arrPractices = Yii::app()->db->createCommand($sqlPractices)->queryAll();
        }

        // Search for a practice city/state
        if (!empty($arrPractices)) {
            foreach ($arrPractices as $k => $v) {
                $arrPractices[$k]['city_state'] = 'Unknown';
                if (!empty($v['location_id'])) {
                    // Search for a practice city/state
                    $location = Location::model()
                        ->cache(Yii::app()->params['cache_medium'])
                        ->find('id = :location_id', array(':location_id' => $v['location_id']));

                    if (!empty($location)) {
                        $arrPractices[$k]['city_state'] = $location->getFullLocation(false);
                    }
                }
            }
        }

        return $arrPractices;
    }

    /**
     * Get all account<->providers, linked or not to this practice
     * @param int $practiceId
     * @param int $accountId
     * @param bool $useCache
     * @param bool $mustBeLinked
     * @return array
     */
    public static function getPracticeProviders($practiceId, $accountId, $useCache = true, $mustBeLinked = false)
    {

        $cacheTime = $useCache ? Yii::app()->params['cache_short'] : 0;

        $linkedCondition = '';
        if (Common::isTrue($mustBeLinked)) {
            $linkedCondition = ' AND provider_practice.practice_id IS NOT NULL ';
        }

        $selectValues = "provider_practice.practice_id, provider.gender,
            provider.id AS provider_id, provider.photo_path, provider.first_middle_last,
            provider.prefix, provider.first_name, provider.last_name,
            IF(provider_practice.id is null, FALSE, TRUE) AS checked,
            provider_practice.id AS provider_practice_id";

        $sql = sprintf(
            "SELECT %s
            FROM provider
            LEFT JOIN provider_practice
                ON provider_practice.provider_id = provider.id
            AND provider_practice.practice_id = %d
            INNER JOIN account_provider
                ON account_provider.provider_id = provider.id
            WHERE account_provider.account_id = %d
            %s
            GROUP BY provider.id;",
            $selectValues,
            $practiceId,
            $accountId,
            $linkedCondition
        );

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * API: Get full data of providers for this practice
     * @param int $practiceId
     * @param int $accountId
     * @param bool $brief
     * @return array
     */
    public static function getProvidersFullData($practiceId = 0, $accountId = 0, $brief = false)
    {
        if ($brief) {
            $selectValues = "DISTINCT p.id AS id, p.first_name, p.last_name, p.first_m_last, p.suspended";
        } else {
            $selectValues = "DISTINCT p.id AS id, p.localization_id, p.country_id, p.first_last, p.first_m_last,
                p.first_middle_last, p.npi_id, p.gender, p.credential_id, p.prefix, p.first_name, p.middle_name,
                p.last_name, p.suffix, p.docscore, p.accepts_new_patients, p.year_began_practicing, p.vanity_specialty,
                p.additional_specialty_information, p.brief_bio, p.bio, p.fees, p.avg_rating, p.friendly_url,
                p.photo_path, p.date_added, p.date_updated, p.umd_id, p.date_umd_updated, p.featured, p.claimed,
                p.suspended, p.partner_site_id, pp.id AS provider_practice_id";
        }

        $sql = sprintf(
            "SELECT %s
            FROM provider_practice AS pp
            INNER JOIN provider AS p
                ON pp.provider_id = p.id
            INNER JOIN account_provider AS apro
                ON apro.provider_id = p.id
            WHERE pp.practice_id = %d
                AND apro.account_id = %d;",
            $selectValues,
            $practiceId,
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * create options for Select Practices when Claim Provider
     * @param int $providerId
     * @param int $accountId
     * @return arr $optionPractices
     */
    public static function createComboPracticesClaimProvider($providerId = 0, $accountId = 0)
    {

        $arrPractices = array();
        if ($providerId > 0) {
            $sqlPractices = sprintf(
                "SELECT provider_practice.practice_id, practice.name AS practice_name,
                    practice.address_full AS practice_address, practice.phone_number AS practice_phone
                FROM provider_practice
                LEFT JOIN practice
                    ON provider_practice.practice_id = practice.id
                WHERE provider_id = '%s'
                GROUP BY provider_practice.id
                ORDER BY practice.name ASC;",
                $providerId
            );
            $providerPractices = Yii::app()->db->createCommand($sqlPractices)->queryAll();

            if ($providerPractices) {
                foreach ($providerPractices as $pc) {
                    $practiceId = $pc['practice_id'];
                    $haveThisPractice = AccountPractice::model()->exists(
                        'account_id=:accountId AND practice_id=:practiceId',
                        array(':accountId' => $accountId, ':practiceId' => $practiceId)
                    );
                    if (!$haveThisPractice) {
                        $arrPractices['provider'][$pc['practice_id']]['practiceId'] = $practiceId;
                        $arrPractices['provider'][$pc['practice_id']]['name'] = $pc['practice_name'] . ' - Phone:' .
                            $pc['practice_phone'];
                    }
                }
            }
        }
        return $arrPractices;
    }

    /**
     * Get Providers Practice Hours
     * @todo Combine with ProviderPractice::getHours
     * @param $provider_practice_id
     * @return array
     */
    public static function getProvidersPracticesHours($provider_practice_id = 0)
    {
        if ($provider_practice_id == 0) {
            return false;
        }
        $sqlHours = sprintf(
            "SELECT provider_practice_schedule.id, hours, day_number
            FROM provider_practice_schedule
            WHERE provider_practice_id = '%s';",
            $provider_practice_id
        );

        $arrHours = Yii::app()->db->createCommand($sqlHours)->queryAll();
        $schedule = array();
        if ($arrHours) {
            foreach ($arrHours as $value) {
                if (!empty($value['hours'])) {
                    $x['id'] = $value['id'];
                    $x['hours'] = $value['hours'];
                    $schedule[$value['day_number']] = $x;
                }
            }
        }
        return $schedule;
    }

    /**
     * Get details about a provider's working hours in a practice given their IDs
     * @todo Combine with ProviderPractice::getProvidersPracticesHours()
     * @param int $provider_practice_id
     * @return array
     */
    public function getHours($provider_practice_id)
    {

        $sql = sprintf(
            "SELECT CASE day_number
                WHEN 1 THEN 'Mon'
                WHEN 2 THEN 'Tue'
                WHEN 3 THEN 'Wed'
                WHEN 4 THEN 'Thu'
                WHEN 5 THEN 'Fri'
                WHEN 6 THEN 'Sat'
                WHEN 0 THEN 'Sun'
                ELSE 'Unknown'
                END AS day,
                hours
            FROM provider_practice_schedule
            WHERE provider_practice_id = %d
                AND hours IS NOT NULL
            ORDER BY day_number ASC;",
            $provider_practice_id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Simulate practice office hours by combining every provider's office hours
     * @param int $practiceId
     * @param int $providerId
     * @return array
     */
    public function getCombinedHours($practiceId, $providerId = false)
    {

        $sql = sprintf(
            "SELECT CASE day_number
                WHEN 1 THEN 'Mon'
                WHEN 2 THEN 'Tue'
                WHEN 3 THEN 'Wed'
                WHEN 4 THEN 'Thu'
                WHEN 5 THEN 'Fri'
                WHEN 6 THEN 'Sat'
                WHEN 0 THEN 'Sun'
                ELSE 'Unknown'
                END AS day,
                hours, day_number
            FROM provider_practice
            INNER JOIN provider_practice_schedule
                ON provider_practice.id = provider_practice_schedule.provider_practice_id
            WHERE provider_practice.practice_id = %d %s
                AND hours IS NOT NULL
            ORDER BY day_number ASC;",
            $practiceId,
            empty($providerId) ? '' : 'AND provider_practice.provider_id = ' . $providerId
        );

        $arrProviderHours = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();

        if (empty($arrProviderHours)) {
            return '';
        }

        // Combine hours from all providers into a single day
        $arrHours = array();
        foreach ($arrProviderHours as $providerHour) {
            $hours = explode(' - ', $providerHour['hours']);
            if (!isset($arrHours[$providerHour['day']])) {
                $arrHours[$providerHour['day']] = array();
            }
            if (
                !isset($arrHours[$providerHour['day']]['from'])
                || strtotime($hours[0]) < (strtotime($arrHours[$providerHour['day']]['from']))
            ) {
                $arrHours[$providerHour['day']]['from'] = $hours[0];
            }
            if (
                !isset($arrHours[$providerHour['day']]['to'])
                || (strtotime($arrHours[$providerHour['day']]['to'])) < strtotime($hours[1])
            ) {
                $arrHours[$providerHour['day']]['to'] = $hours[1];
            }
            $arrHours[$providerHour['day']]['day_number'] = $providerHour['day_number'];
        }

        // Build final array
        $index = 0;
        $dayNames = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
        $arrProviderHours = array();

        foreach ($arrHours as $hoursVal) {
            $arrProviderHours[$index]['id'] = $hoursVal['day_number'];
            $arrProviderHours[$index]['day_number'] = $hoursVal['day_number'];
            $arrProviderHours[$index]['day'] = $dayNames[$hoursVal['day_number']];
            $arrProviderHours[$index]['hours'] = $hoursVal['from'] . ' - ' . $hoursVal['to'];

            $timeFrom = strtotime(date('Y-m-d ' . $hoursVal['from']));
            $timeTo = strtotime(date('Y-m-d ' . $hoursVal['to']));
            $timeRange = date('h:iA', $timeFrom) . ' - ' . date('h:iA', $timeTo);
            $arrProviderHours[$index]['timeRange'] = $timeRange;

            $arrProviderHours[$index]['selected'] = 0;
            $arrProviderHours[$index]['hoursFrom'] = date('h', $timeFrom);
            $arrProviderHours[$index]['minsFrom'] = date('i', $timeFrom);
            $arrProviderHours[$index]['amFrom'] = date('A', $timeFrom) == 'AM' ? 'selected' : '';
            $arrProviderHours[$index]['pmFrom'] = date('A', $timeFrom) == 'PM' ? 'selected' : '';

            $arrProviderHours[$index]['hoursTo'] = date('h', $timeTo);
            $arrProviderHours[$index]['minsTo'] = date('i', $timeTo);
            $arrProviderHours[$index]['amTo'] = date('A', $timeTo) == 'AM' ? 'selected' : '';
            $arrProviderHours[$index]['pmTo'] = date('A', $timeTo) == 'PM' ? 'selected' : '';

            $index++;
        }

        usort($arrProviderHours, function ($a, $b) {
            return $a['day_number'] - $b['day_number'];
        });

        return $arrProviderHours;
    }

    /**
     * Create Twilio numbers if necessary and clean up un-needed practices
     * @return boolean
     */
    public function afterSave()
    {

        ini_set('max_execution_time', 0);

        if ($this->isNewRecord && !self::researchMode()) {

            // Send twilio numbers generation to a queue
            $queueBody = array(
                'provider_practice_id' => $this->id
            );
            SqsComponent::sendMessage('providerPracticeGenerateTwilioNumbers', $queueBody);

            // Duplicate provider-practice-Insurances for new practice
            ProviderPracticeInsurance::model()->populateNewPractice($this->provider_id, $this->practice_id);
        }

        // Delete records from provider_practice called like 'Practice At'
        $accountId = $this->getAccountId($this->practice_id);
        $arrProviderPractice = ProviderPractice::getPracticeDetailsForProviders(
            $this->provider_id,
            '',
            $accountId,
            'LEFT'
        );

        // can we clean up practices?
        $hasOtherValidPractices = self::countActualPractices($this->provider_id, $this->practice_id, $accountId);

        if (!empty($arrProviderPractice) && count($arrProviderPractice) > 1 && $hasOtherValidPractices) {

            foreach ($arrProviderPractice as $providerPractice) {

                if (substr($providerPractice['name'], 0, 11) == "Practice At") {
                    $practiceObj = Practice::model()->findByPk($providerPractice['id']);
                    if (!empty($practiceObj) && $practiceObj->claimed == 0) {
                        // Look up for the provider<->practice
                        $existsWithAnotherProvider = ProviderPractice::model()->exists(
                            "practice_id = :practice_id AND provider_id != :provider_id",
                            array(":practice_id" => $providerPractice['id'], ":provider_id" => $this->provider_id)
                        );
                        // This practice is related to another provider?
                        if (empty($existsWithAnotherProvider)) {
                            // No
                            // Look up for the provider<->practice relationship
                            $providerPracticeId = $providerPractice['provider_practice_id'];
                            $recProviderPractice = ProviderPractice::model()->findByPk($providerPracticeId);
                            if (!empty($recProviderPractice)) {
                                $recProviderPractice->delete();
                            }
                        }
                    }
                }
            }
        }

        // actions that should be made only on inserts
        if ($this->isNewRecord) {
            // update rank order and primary_location
            ProviderPractice::updateRankOrder($this->provider_id);

            // update practice size
            PracticeDetails::setPracticeSize($this->practice_id);
        }

        // update scheduling if necessary
        if ($this->isNewRecord
            ||
            (!empty($this->oldRecord) && $this->oldRecord->practice_id != $this->practice_id)
            ||
            (!empty($this->oldRecord) && $this->oldRecord->provider_id != $this->provider_id)
        ) {
            if (!empty($this->provider_id)) {
                // async update
                SqsComponent::sendMessage('updateProviderBooking', $this->provider_id);
            }
            if (
                !empty($this->oldRecord)
                &&
                !empty($this->provider_id)
                &&
                $this->oldRecord->provider_id != $this->provider_id
            ) {
                // async update the previous provider_id
                SqsComponent::sendMessage('updateProviderBooking', $this->oldRecord->provider_id);
            }
        }

        return parent::afterSave();
    }

    /**
     * Check if a provider has valid practices linked to him so we can clean up
     * @param int $providerId
     * @param int $practiceId
     * @param int $accountId
     * @return int
     */
    public static function countActualPractices($providerId, $practiceId, $accountId)
    {

        if (empty($providerId) || empty($practiceId) || empty($accountId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT COUNT(1) AS total
            FROM provider_practice
            INNER JOIN practice
            ON provider_practice.practice_id = practice.id
            INNER JOIN account_practice
            ON account_practice.practice_id = practice.id
            INNER JOIN account_provider
            ON account_provider.account_id = account_practice.account_id
            AND account_provider.provider_id = provider_practice.provider_id
            LEFT JOIN practice_details
            ON provider_practice.practice_id = practice_details.practice_id
            WHERE provider_practice.provider_id = %d
            AND practice.name NOT LIKE 'Practice At%%'
            AND practice.id != %d
            AND account_practice.account_id = %d
            AND practice_details.practice_type_id != 5;",
            $providerId,
            $practiceId,
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Update rank, clean up practices, clean up partner_site_entity_match
     */
    public function afterDelete()
    {

        $practiceId = $this->practice_id;

        // Update rank ordering for all practices after deletion, this also will set the first as the primary if
        // there isn't any primary location
        ProviderPractice::updateRankOrder($this->provider_id);

        // We need to Remove the practice
        // if it is not associated with any other provider
        // and start with 'Practice At...'
        $practice = Practice::model()->findByPk($this->practice_id);
        if (!empty($practice) && substr($practice->name, 0, 11) == "Practice At") {

            $existsProviderPractice = ProviderPractice::model()->exists(
                "practice_id = :practice_id",
                array(":practice_id" => $this->practice_id)
            );
            // This practice is related to a provider?
            if (empty($existsProviderPractice)) {
                // No, so save the previous relationship and then delete
                $this->saveProviderPreviousPractice($this->provider_id, $practice);
                $practice->delete();
            }
        }

        // update practice size
        PracticeDetails::setPracticeSize($practiceId);

        // remove locations suggested for novartis that matchs with this provider-practice
        $matches = PartnerSiteEntityMatch::model()->findAll(
            'partner_site_id = 76 AND match_column_name = "location_id" AND internal_column_value = :id',
            [':id' => $this->id]
        );

        // found some suggestions for novartis?
        if ($matches) {
            // remove all suggestions for this provider-practice
            foreach ($matches as $m) {
                $m->delete();
            }

            // if the practice is not suggested with other provider, check if there is a twilio purchased for the
            // practice and remove it
            $sql = 'SELECT 1
                FROM partner_site_entity_match
                WHERE partner_site_id = 76
                    AND match_column_name = "location_id"
                    AND internal_column_value IN (
                        SELECT provider_practice.id
                        FROM provider_practice
                        WHERE provider_practice.practice_id = :practice_id
                    )
                LIMIT 1';
            $exist = Yii::app()->db->createCommand($sql)->queryScalar([':practice_id' => $practiceId]);

            if (!$exist) {
                // find twilio partner id
                $sql = "SELECT id FROM twilio_partner WHERE partner_site_id = 76 AND for_showcase = 0";
                $twilioPartnerId = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)
                    ->queryScalar();

                $twilioNumber = TwilioNumber::model()->find(
                    'practice_id = :practice_id AND twilio_partner_id = :twilio_partner_id',
                    [':practice_id' => $practiceId, ':twilio_partner_id' => $twilioPartnerId]
                );

                if ($twilioNumber) {
                    $twilioNumber->delete();
                }
            }
        }

        // remove all provider_practice_ref values in partner_site_entity_match associated to the giver p@p
        $this->removeProviderPracticeRef($this->id);

        return parent::afterDelete();
    }

    /**
     * @todo document
     * @param array $attributes
     * @param string $condition
     * @param array $params
     * @return int
     */
    public function updateAll($attributes, $condition = '', $params = array())
    {

        $numRows = parent::updateAll($attributes, $condition, $params);

        if ($numRows > 0) {

            $arrCondition = array();
            foreach ($params as $key => $value) {
                $arrCondition[substr($key, 1)] = $params[$key];
            }
        }

        return $numRows;
    }

    /**
     * Get some details about providers working in a given practice
     * @param str $listPracticesIds
     * @param int $providerId
     * @return array
     */
    public static function getPracticeColleagues($listPracticesIds = '', $providerId = 0)
    {

        if (!empty($listPracticesIds) && $providerId > 0) {

            $sql = sprintf(
                "SELECT provider.id AS provider_id, provider_practice.practice_id AS practice_id,
                provider.prefix AS prefix, provider.vanity_specialty AS vanity_specialty,
                provider.first_name AS first_name, provider.last_name AS last_name,
                CONCAT(IF(provider.prefix IS NOT NULL AND provider.prefix != '', CONCAT(provider.prefix, ' '), ''),
                provider.first_last) AS provider_name, provider.avg_rating, credential.title AS credential,
                provider.friendly_url AS friendly_url, provider.photo_path, provider.gender,
                GROUP_CONCAT(DISTINCT(specialty.name) SEPARATOR ', ') AS specialties
                FROM provider
                INNER JOIN provider_practice
                ON provider.id = provider_practice.provider_id
                AND primary_location = 1
                LEFT JOIN credential
                ON provider.credential_id = credential.id
                LEFT JOIN provider_specialty
                ON provider.id = provider_specialty.provider_id
                LEFT JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
                LEFT JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
                WHERE provider_practice.practice_id IN (%s)
                AND provider.id != %d
                AND provider.suspended = 0
                GROUP BY provider.id
                ORDER BY RAND()
                LIMIT 10;",
                $listPracticesIds,
                $providerId
            );
            return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
        }
        return false;
    }

    /**
     * Returns whether a provider is working at a practice or not.
     * @todo change * for COUNT
     * @param int $provider_id
     * @param int $practice_id
     * @return boolean
     */
    public static function isWorkingAtPractice($provider_id, $practice_id)
    {
        $sql = sprintf(
            "SELECT *
            FROM provider_practice
            WHERE provider_id = %d
            AND practice_id = %d;",
            $provider_id,
            $practice_id
        );
        $results = Yii::app()->db->createCommand($sql)->queryAll();

        return sizeof($results) ? true : false;
    }

    /**
     * PADM: Gets providers working at a set of practices
     * @param int $practiceIds
     * @param int $ownerAccountId
     * @return array
     */
    public static function getProvidersAtPractices($practiceIds = '', $ownerAccountId = 0)
    {
        if (empty($practiceIds)) {
            return false;
        }

        if ($ownerAccountId == 0 && (php_sapi_name() != "cli" && !empty(Yii::app()->user->id))) {
            $ownerAccountId = OrganizationAccount::switchOwnerAccountID(Yii::app()->user->id);
        }

        $sql = sprintf(
            'SELECT DISTINCT provider.id AS provider_id, provider.first_name AS first_name,
                provider.last_name AS last_name, provider_details.schedule_mail_enabled AS schedule_mail_enabled
            FROM provider_practice
            INNER JOIN provider
                ON provider_practice.provider_id = provider.id
            INNER JOIN account_provider
                ON provider_practice.provider_id = account_provider.provider_id
                AND account_provider.account_id = %d
            INNER JOIN provider_details
                ON provider.id = provider_details.provider_id
            WHERE practice_id IN (%s)
            ORDER BY first_name, last_name;',
            $ownerAccountId,
            $practiceIds
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * DA: Get provider name, specialty and photo for providers working in a given practice
     * @param int $practiceId
     * @return array
     */
    public static function getProvidersAtPracticeDetails($practiceId)
    {
        $sql = sprintf(
            'SELECT DISTINCT provider.id AS provID, CONCAT(first_name, " ", last_name) AS name,
                provider.photo_path, provider.gender,
                GROUP_CONCAT(
                    CONCAT( specialty.name, " - ", sub_specialty.name )
                    ORDER BY primary_specialty SEPARATOR ", "
                    ) AS specialty, provider_details.no_insurance_reason
            FROM provider_practice
            INNER JOIN provider
                ON provider_practice.provider_id = provider.id
            LEFT JOIN provider_specialty
                ON provider.id = provider_specialty.provider_id
            LEFT JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
            LEFT JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
            INNER JOIN provider_details
                ON provider_details.provider_id = provider.id
            WHERE practice_id = %d
                AND provider_details.schedule_mail_enabled = TRUE
                AND provider.suspended != 1
            GROUP BY provider.id
            ORDER BY first_name, last_name;',
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * DR: Get provider name, specialty and photo for providers working in a given practice
     * @param int $practiceId
     * @param int $accountId
     * @return array
     */
    public static function getProvidersAtPracticeDetailsForDirectReview($practiceId = 0, $accountId = 0)
    {
        $condAccount = '';
        if ($accountId > 0) {
            $condAccount = sprintf(" AND account_provider.account_id = %d", $accountId);
        }

        $sql = sprintf(
            'SELECT DISTINCT provider.id AS provID, CONCAT(first_name, " ", last_name) AS name,
                provider.photo_path, GROUP_CONCAT(DISTINCT(specialty.name) SEPARATOR ", ") AS specialty,
                provider.gender
            FROM provider_practice
            INNER JOIN provider
                ON provider_practice.provider_id = provider.id
            LEFT JOIN provider_specialty
                ON provider.id = provider_specialty.provider_id
            LEFT JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
            LEFT JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
            INNER JOIN provider_details
                ON provider_details.provider_id = provider.id
            LEFT JOIN account_provider
                ON account_provider.provider_id = provider.id
            WHERE practice_id = %d
                AND provider.suspended != 1
                %s
            GROUP BY provider.id
            ORDER BY first_name, last_name;',
            $practiceId,
            $condAccount
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * DA: Get practices for a given provider
     * @param int $providerId
     * @param int $twilioId
     * @param int $accountId
     * @param string $joinAccountPracticeEmr (INNER || LEFT)
     * @return array
     */
    public static function getPracticeDetailsForProviders(
        $providerId = 0,
        $twilioId = '',
        $accountId = 0,
        $joinAccountPracticeEmr = 'INNER'
    ) {
        $condAccount = '';
        if ($accountId > 0) {
            $condAccount = " AND account_practice.account_id = :account_id"; //$accountId
        }

        if ($twilioId) {
            $sql = sprintf(
                "SELECT practice.id, practice.name, address, address_2,
                    city_name, logo_path AS photo_path, IF(twilio_number.twilio_number IS NULL,
                    practice.phone_number, twilio_number.twilio_number) as phone_number,
                    zipcode, state.abbreviation AS state, provider_practice.id AS provider_practice_id,
                    IF (practice_details.practice_type_id = 5, true, false) AS is_telemedicine_scheduling
                FROM practice
                INNER JOIN provider_practice
                    ON provider_practice.practice_id = practice.id
                %s JOIN account_practice
                    ON account_practice.practice_id = practice.id
                %s JOIN account_practice_emr
                    ON account_practice_emr.practice_id = practice.id
                %s JOIN account_provider_emr
                    ON account_provider_emr.provider_id = provider_practice.provider_id
                    AND account_provider_emr.account_id = account_practice.account_id
                LEFT JOIN twilio_number
                    ON twilio_number.practice_id = practice.id
                    AND twilio_number.provider_id IS NULL AND twilio_partner_id = :twilio_partner_id
                LEFT JOIN location
                    ON location.id = practice.location_id
                LEFT JOIN city
                    ON location.city_id = city.id
                LEFT JOIN state
                    ON city.state_id = state.id
                LEFT JOIN practice_details
                    ON practice.id = practice_details.practice_id
                WHERE provider_practice.provider_id = :provider_id
                    %s
                GROUP BY practice.id
                ORDER BY provider_practice.primary_location DESC, practice.name ASC;",
                $joinAccountPracticeEmr,
                $joinAccountPracticeEmr,
                $joinAccountPracticeEmr,
                $condAccount
            );

            $query = Yii::app()->db->createCommand($sql)
                ->bindValue(':provider_id', $providerId)
                ->bindValue(':twilio_partner_id', $twilioId);

            if ($accountId) {
                $query->bindValue(':account_id', $accountId);
            }

            return $query->queryAll();
        } else {
            $sql = sprintf(
                'SELECT practice.id, practice.name, address, address_2,
                    city_name, logo_path AS photo_path, practice.phone_number,
                    zipcode, state.abbreviation AS state, provider_practice.id AS provider_practice_id,
                    IF (practice_details.practice_type_id = 5, true, false) AS is_telemedicine_scheduling
                FROM practice
                INNER JOIN provider_practice
                    ON provider_practice.practice_id = practice.id
                %s JOIN account_practice
                    ON account_practice.practice_id = practice.id
                %s JOIN account_practice_emr
                    ON account_practice_emr.practice_id = practice.id
                %s JOIN account_provider_emr
                    ON account_provider_emr.provider_id = provider_practice.provider_id
                    AND account_provider_emr.account_id = account_practice.account_id
                LEFT JOIN location
                    ON location.id = practice.location_id
                LEFT JOIN city
                    ON location.city_id = city.id
                LEFT JOIN state
                    ON city.state_id = state.id
                LEFT JOIN practice_details
                    ON practice.id = practice_details.practice_id
                WHERE provider_practice.provider_id = :provider_id
                    %s
                GROUP BY practice.id
                ORDER BY provider_practice.primary_location DESC, practice.name ASC;',
                $joinAccountPracticeEmr,
                $joinAccountPracticeEmr,
                $joinAccountPracticeEmr,
                $condAccount
            );

            $query = Yii::app()->db->createCommand($sql)->bindValue(':provider_id', $providerId);
            if ($accountId) {
                $query->bindValue(':account_id', $accountId);
            }

            return $query->queryAll();
        }
    }

    /**
     * Return 1 or 0 if this practice has providers
     * @param int $accountId
     * @param int $practiceId
     * @return bool 0|1
     */
    public static function hasProviders($accountId = 0, $practiceId = 0)
    {
        $sql = sprintf(
            "SELECT pp.id
            FROM provider_practice AS pp
            LEFT JOIN account_provider AS ac ON pp.provider_id = ac.provider_id
            WHERE ac.account_id = %d AND pp.practice_id = %d",
            $accountId,
            $practiceId
        );
        $resultQuery = Yii::app()->db->createCommand($sql)->queryScalar();
        return !empty($resultQuery) ? 1 : 0;
    }

    /**
     * update or insert relation in ProviderPractice
     * @param int $providerId
     * @param int $practiceId
     * @param arr $newData
     * $newData['email']
     * $newData['phone_numnber']
     * $newData['fax_numnber']
     * $newData['rank_order']
     * @return int $providerPracticeID
     */
    public static function setProviderPractice($providerId = 0, $practiceId = 0, $newData = array())
    {

        if ($providerId == 0 || $practiceId == 0) {
            return 0;
        }

        // Is this a valid practice?
        $practice = Practice::model()->findByPk($practiceId);
        if (empty($practice) || $practice->suspended == 1) {
            return 0;
        }

        // Is this a valid provider?
        $provider = Provider::model()->findByPk($providerId);
        if (empty($provider) || $provider->suspended == 1) {
            return 0;
        }

        // Find association
        $providerPractice = ProviderPractice::model()->find(
            'provider_id = :provider_id AND practice_id = :practice_id',
            array(':provider_id' => $providerId, ':practice_id' => $practiceId)
        );

        if (empty($providerPractice)) {
            // Insert new association
            $providerPractice = new ProviderPractice();
            $providerPractice->provider_id = $providerId;
            $providerPractice->practice_id = $practiceId;
            $providerPractice->primary_location = 0;
        }

        // Primary Location
        $isPrimaryLocation = $providerPractice->primary_location;
        if (!empty($newData)) {
            $providerPractice->email = isset($newData['email']) ? trim($newData['email']) : '';
            $providerPractice->phone_number = isset($newData['phone_number']) ? trim($newData['phone_number']) : '';
            $providerPractice->fax_number = isset($newData['fax_number']) ? $newData['fax_number'] : '';
            $providerPractice->rank_order = isset($newData['rank_order']) ? $newData['rank_order'] : '';
            $providerPractice->location_details =
                isset($newData['location_details']) ? $newData['location_details'] : '';

            // Set primary location for first rank_order
            if (isset($newData['rank_order']) && $newData['rank_order'] !== null && $newData['rank_order'] == 0) {
                $isPrimaryLocation = 1;
            }
        }

        if ($isPrimaryLocation == 0) {
            // check for other associated practices
            $assocPractices = ProviderPractice::model()->count(
                'provider_id=:providerId AND primary_location = 1',
                array(':providerId' => $providerId)
            );
            // 1 if the prov was not already assoc to a primary location, 0 otherwise
            $isPrimaryLocation = ($assocPractices == 0) ? 1 : 0;
        }

        $providerPractice->primary_location = $isPrimaryLocation;

        // Save ProviderPractice
        $providerPractice->save();
        return $providerPractice->id;
    }

    /**
     * If the name of deleted practice is like 'Practice At', it is saving in provider_previous_practice table
     * @param int $idProvider
     * @param Practice $practice
     */
    private function saveProviderPreviousPractice($idProvider, $practice)
    {

        $accountId = $this->getAccountId($practice->id);

        if ($accountId != 0) {

            // It's exist record? do not save
            $existProviderPreviousPractice = ProviderPreviousPractice::model()->exists(
                'provider_id=:provider_id AND account_id=:account_id',
                array(':provider_id' => $idProvider, ':account_id' => $accountId)
            );

            // Save record en provider_previous_practice
            if (!$existProviderPreviousPractice) {
                $providerPreviousPractice = new ProviderPreviousPractice();

                $providerPreviousPractice->provider_id = $idProvider;
                $providerPreviousPractice->account_id = $accountId;
                $providerPreviousPractice->practice_name = $practice->name;
                $providerPreviousPractice->practice_group_id = $practice->practice_group_id;
                $providerPreviousPractice->address = $practice->address;
                $providerPreviousPractice->address_2 = $practice->address_2;
                $providerPreviousPractice->location_id = $practice->location_id;
                $providerPreviousPractice->latitude = $practice->latitude;
                $providerPreviousPractice->longitude = $practice->longitude;
                $providerPreviousPractice->phone_number = $practice->phone_number;
                $providerPreviousPractice->fax_number = $practice->fax_number;
                $providerPreviousPractice->email = $practice->email;
                $providerPreviousPractice->website = $practice->website;
                $providerPreviousPractice->handicap_accesible = $practice->handicap_accesible;
                $providerPreviousPractice->aesthetic_procedure = $practice->aesthetic_procedure;
                $providerPreviousPractice->date_deleted = date('Y-m-d H:i:s');
                $providerPreviousPractice->phone_number_clean = $practice->phone_number_clean;
                $providerPreviousPractice->out_of_network = $practice->out_of_network;
                $providerPreviousPractice->facility_fee = $practice->facility_fee;
                $providerPreviousPractice->npi_id = $practice->npi_id;
                $providerPreviousPractice->yext_location_id = $practice->yext_location_id;
                $providerPreviousPractice->dme_services = $practice->dme_services;
                $providerPreviousPractice->save();
            }
        }
    }

    /**
     * Search the account_id from user logged or from account_practice and return that value
     * @param int $idPractice
     * @return int
     */
    public function getAccountId($idPractice)
    {

        $accountId = 0;

        if (php_sapi_name() != "cli" && !empty(Yii::app()->user->id)) {
            $accountId = Yii::app()->user->id;
        } else {
            $accountPractice = AccountPractice::model()->cache(Yii::app()->params['cache_medium'])
                ->count('practice_id=:practice_id', array(':practice_id' => $idPractice));
            if ($accountPractice == 1) {
                $accountPractice = AccountPractice::model()->find(
                    'practice_id=:practice_id',
                    array(':practice_id' => $idPractice)
                );
                if ($accountPractice) {
                    $accountId = $accountPractice->account_id;
                }
            }
        }

        return $accountId;
    }

    /**
     * MSE: Get Treatment for a given provider
     * @param int $practiceId
     * @param str $providerIdStr
     * @return array
     */
    public static function getProviderPracticeTreatments($practiceId, $providerIdStr)
    {
        $res = array();
        if ($providerIdStr != "") {
            $sql = sprintf(
                'SELECT GROUP_CONCAT(provider_id SEPARATOR ", ") AS concat_provider
                FROM provider_practice WHERE practice_id = %s
                AND provider_id IN (%s); ',
                $practiceId,
                $providerIdStr
            );
            $resultQuery = Yii::app()->db->createCommand($sql)->queryScalar();

            if ($resultQuery != "") {
                $sql = sprintf(
                    'SELECT DISTINCT treatment.name
                    FROM provider_treatment
                    INNER JOIN treatment ON treatment.id = provider_treatment.treatment_id
                    WHERE provider_id IN (%s)
                    ORDER BY treatment.name; ',
                    $resultQuery
                );
                $res = Yii::app()->db->createCommand($sql)->queryAll();
            }
        }

        return $res;
    }

    /**
     * get next rank order for a given provider
     * @param int $providerId
     * @return int
     */
    public static function getNextRankOrder($providerId)
    {

        $sql = sprintf(
            'SELECT MAX(provider_practice.rank_order) + 1
            AS rank_order
            FROM provider_practice
            WHERE provider_id = %s;',
            $providerId
        );
        $res = Yii::app()->db->createCommand($sql)->queryScalar();

        // cast NULL to zero for first provider-practice
        return (int)$res;
    }

    /**
     * update all practices rank order of a provider
     * @param int $providerId
     */
    public static function updateRankOrder($providerId)
    {
        // search for provider practices with the correct order, telemedicine practices will be ignored
        $providerPractices = ProviderPractice::model()->with('practice.practiceDetails')->findAll([
            'condition' => 'provider_id = :provider_id ' .
                'AND (practiceDetails.practice_type_id IS NULL OR practiceDetails.practice_type_id <> 5)',
            'params' => [':provider_id' => $providerId],
            'order' => 'primary_location DESC, ISNULL(rank_order), rank_order'
        ]);

        $rankOrder = 0;
        $primaryLocation = 1;
        foreach ($providerPractices as $providerPractice) {
            if ($rankOrder == 1) {
                $primaryLocation = 0;
            }

            // update only if there is a difference between the values or if rank_order is null
            if (
                $providerPractice->rank_order != $rankOrder ||
                $providerPractice->primary_location != $primaryLocation ||
                $providerPractice->rank_order === null
            ) {
                $providerPractice->primary_location = $primaryLocation;
                $providerPractice->rank_order = $rankOrder;
                $providerPractice->save();
            }

            ++$rankOrder;
        }

        // check if all telemedicine practices has the right values
        $providerPractices = ProviderPractice::model()->with('practice.practiceDetails')->findAll([
            'condition' => 'provider_id = :provider_id AND practiceDetails.practice_type_id = 5 ' .
                'AND (rank_order <> 0 OR primary_location = 1)',
            'params' => [':provider_id' => $providerId]
        ]);

        // set right values for found records
        foreach ($providerPractices as $pp) {
            $pp->primary_location = 0;
            $pp->rank_order = 0;
            $pp->save();
        }
    }

    /**
     * Validate if the provider can be published. Validate practices asociated status.
     * @param int $providerId
     * @param int $accountId
     * @param bool $getPracticeIds
     * @return array
     */
    public static function canProviderBePublished($providerId, $accountId, $getPracticeIds)
    {

        $sql = sprintf(
            "SELECT pp.id, pp.provider_id, pp.practice_id, apra.account_id AS apra_id,
                apro.account_id AS apro_id, apra.research_status AS apra_research_status,
                apro.research_status AS apro_research_status
            FROM provider_practice AS pp
            LEFT JOIN account_practice AS apra
                ON pp.practice_id = apra.practice_id
            LEFT JOIN account_provider AS apro
                ON pp.provider_id = apro.provider_id
            WHERE apra.account_id = apro.account_id
                AND pp.provider_id = %d
                AND apro.account_id = %d",
            $providerId,
            $accountId
        );
        $result = Yii::app()->db->createCommand($sql)->query();

        $arrayPractices = array();
        $canBePublished = false;
        $providerStatus = '';

        if (!empty($result)) {
            foreach ($result as $value) {
                $providerStatus = $value['apro_research_status'];
                if (
                    $value['apro_research_status'] == 'Ready to Publish'
                    && ($value['apra_research_status'] == 'Ready to Publish'
                        || $value['apra_research_status'] == 'Published')
                ) {
                    $canBePublished = true;
                    if (!$getPracticeIds) {
                        break;
                    } else {
                        if ($value['apra_research_status'] == 'Ready to Publish') {
                            $arrayPractices[] = $value['practice_id'];
                        }
                    }
                }
            }
        }

        return array($canBePublished, $providerStatus, $arrayPractices);
    }

    /**
     * Validate if the practice can be published. Validate providers asociated status.
     * @param int $practiceId
     * @param int $accountId
     * @param bool $getProviderIds
     * @return array
     */
    public static function canPracticeBePublished($practiceId = 0, $accountId = 0, $getProviderIds = false)
    {

        $sql = sprintf(
            "SELECT pp.id, pp.provider_id, pp.practice_id, apra.account_id AS apra_id,
                apro.account_id AS apro_id, apra.research_status AS apra_research_status,
                apro.research_status AS apro_research_status
            FROM provider_practice AS pp
            LEFT JOIN account_practice AS apra
                ON pp.practice_id = apra.practice_id
            LEFT JOIN account_provider AS apro
                ON pp.provider_id = apro.provider_id
            WHERE apra.account_id = apro.account_id
                AND pp.practice_id = %d
                AND apro.account_id = %d",
            $practiceId,
            $accountId
        );
        $result = Yii::app()->db->createCommand($sql)->query();

        $arrayProviders = array();
        $canBePublished = false;
        $practiceStatus = '';

        if (!empty($result)) {
            foreach ($result as $value) {
                $practiceStatus = $value['apra_research_status'];
                if (
                    $value['apra_research_status'] == 'Ready to Publish'
                    && ($value['apro_research_status'] == 'Ready to Publish'
                        || $value['apro_research_status'] == 'Published')
                ) {
                    $canBePublished = true;
                    if (!$getProviderIds) {
                        break;
                    } else {
                        if ($value['apro_research_status'] == 'Ready to Publish') {
                            $arrayProviders[] = $value['provider_id'];
                        }
                    }
                }
            }
        }

        return array($canBePublished, $practiceStatus, $arrayProviders);
    }

    /**
     * Get operatories value for a given Practice-Provider
     * @param $practiceId
     * @param $providerId
     * @return mixed
     */
    public static function getOperatories($practiceId, $providerId)
    {
        $sql = sprintf(
            "SELECT operatories FROM provider_practice
                  WHERE practice_id = %d AND provider_id = %d
                  LIMIT 1",
            $practiceId,
            $providerId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Generate twilio numbers using the provider_id
     */
    public function generateTwilioNumbers()
    {
        // Insert twilio numbers
        $providerPSPF = PartnerSiteProviderFeatured::model()->findAll(
            'provider_id = :provider_id AND active = :active',
            array(':provider_id' => $this->provider_id, ':active' => "1")
        );

        foreach ($providerPSPF as $pspf) {
            $modelPSPF = PartnerSiteProviderFeatured::model()->findByPk($pspf->id);
            $modelPSPF->generateTwilioNumbers();
        }
    }

    public function getPhones($providerId, $practiceId)
    {
        $partnerSitePhones = $this->getPartnerSitePhones($providerId, $practiceId);
        $twilioPhones = $this->getTwilioPhones($providerId, $practiceId);
        $providerPracticePhones = $this->getProviderPracticePhones($providerId, $practiceId);
        $practicePhones = $this->getPracticePhones($practiceId);

        return array_merge(
            $partnerSitePhones,
            $twilioPhones,
            $providerPracticePhones,
            $practicePhones
        );
    }

    protected function getPartnerSitePhones($providerId, $practiceId)
    {
        $phones = [];
        $psp = [];

        if ($providerId && $practiceId) {
            $psp = PartnerSiteParams::model()->cache(Yii::app()->params['cache_long'])->findAllByAttributes(
                ['provider_id' => $providerId, 'practice_id' => $practiceId],
                "param_name LIKE 'custom_tracking_numbers_%'"
            );

            if (!$psp) {
                $psp = PartnerSiteParams::model()->cache(Yii::app()->params['cache_long'])->findAllByAttributes(
                    ['practice_id' => $practiceId],
                    "param_name LIKE 'custom_tracking_numbers_%'"
                );
            }

        } elseif ($practiceId) {
            $psp = PartnerSiteParams::model()->cache(Yii::app()->params['cache_long'])->findAllByAttributes(
                ['practice_id' => $practiceId],
                "param_name LIKE 'custom_tracking_numbers_%' AND provider_id IS NULL"
            );
        } elseif ($providerId) {
            $psp = PartnerSiteParams::model()->cache(Yii::app()->params['cache_long'])->findAllByAttributes(
                ['provider_id' => $providerId],
                "param_name LIKE 'custom_tracking_numbers_%' AND practice_id IS NULL"
            );
        }

        if ($psp) {
            foreach ($psp as $param) {
                $phone = [
                    'number' => $param->param_value,
                    'type' => 'partner_site_param',
                    'data' => [
                        'param_name' => $param->param_name,
                        'code' => str_replace('custom_tracking_numbers_', '', $param->param_name),
                        'practice_id' => $practiceId,
                        'provider_id' => $providerId
                    ]
                ];

                $phones[] = $phone;
            }
        }

        return $phones;
    }

    protected function getTwilioPhones($providerId, $practiceId)
    {
        $phones = [];
        $tn = [];

        if ($providerId && $practiceId) {
            $tn = TwilioNumber::model()->cache(Yii::app()->params['cache_long'])->findAllByAttributes(
                ['provider_id' => $providerId, 'practice_id' => $practiceId]
            );

            if (!$tn) {
                $tn = TwilioNumber::model()->cache(Yii::app()->params['cache_long'])->findAllByAttributes(
                    ['practice_id' => $practiceId],
                    'provider_id IS NULL'
                );
            }
        } elseif ($practiceId) {
            $tn = TwilioNumber::model()->cache(Yii::app()->params['cache_long'])->findAllByAttributes(
                ['practice_id' => $practiceId],
                'provider_id IS NULL'
            );
        } elseif ($providerId) {
            $tn = TwilioNumber::model()->cache(Yii::app()->params['cache_long'])->findAllByAttributes(
                ['provider_id' => $providerId],
                'practice_id IS NULL'
            );
        }

        if ($tn) {
            foreach ($tn as $twilioNumber) {
                $phone = [
                    'number' => $twilioNumber->twilio_number,
                    'type' => 'twilio',
                    'data' => [
                        'practice_id' => $practiceId,
                        'provider_id' => $providerId
                    ]
                ];

                $phones[] = $phone;
            }
        }

        return $phones;
    }

    protected function getProviderPracticePhones($providerId, $practiceId)
    {
        $phones = [];
        $ppp = [];

        if ($providerId && $practiceId) {
            $ppp = ProviderPractice::model()->cache(Yii::app()->params['cache_long'])->findAllByAttributes(
                ['provider_id' => $providerId, 'practice_id' => $practiceId]
            );
        }

        if ($ppp) {
            foreach ($ppp as $providerPractice) {
                $phone = [
                    'number' => $providerPractice->phone_number,
                    'type' => 'provider_practice',
                    'data' => [
                        'practice_id' => $practiceId,
                        'provider_id' => $providerId
                    ]
                ];

                $phones[] = $phone;
            }
        }

        return $phones;
    }

    protected function getPracticePhones($practiceId)
    {
        $practice = Practice::model()->cache(Yii::app()->params['cache_long'])->findByPk($practiceId);

        if ($practice) {
            $phone = [
                'number' => $practice->phone_number,
                'type' => 'practice',
                'data' => []
            ];

            // Returning array for consistency with
            // similar methods.
            return [$phone];
        }

        return [];
    }

    /**
     * Get the Telemedicine Booking (internal) practice for a given provider (one by provider)
     *
     * @param int $providerId
     * @return mixed
     */
    public static function getTelemedicinePractice($providerId)
    {

        // validate provider id
        if (empty($providerId)) {
            return false;
        }

        // look up an existing telemedicine practice
        $sql = sprintf(
            "SELECT provider_practice.id AS id, provider_practice.practice_id AS practice_id,
            provider_practice.provider_id AS provider_id, provider_details.telemedicine_accept_new_patients
            FROM provider_practice
            INNER JOIN practice_details
            ON provider_practice.practice_id = practice_details.practice_id
            AND practice_details.practice_type_id = 5
            LEFT JOIN provider_details
            ON provider_practice.provider_id = provider_details.provider_id
            WHERE provider_practice.provider_id = %d
            LIMIT 1;",
            $providerId
        );

        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get the Telemedicine Booking (internal) practice for a given provider (one by provider) or create it if it
     * doesn't exist
     *
     * @param int $providerId
     * @param int $accountId
     * @param array $arrData
     * @return mixed
     */
    public static function getOrCreateTelemedicinePractice($providerId, $accountId, $arrData = null)
    {

        // validate ids
        if (empty($providerId) || empty($accountId)) {
            Yii::log('Telemedicine: Did not receive providerId or accountId', CLogger::LEVEL_WARNING);
            return false;
        }

        $providerPractice = self::getTelemedicinePractice($providerId);

        // do we need to update provider_details?
        if (!empty($arrData)) {
            $providerDetails = ProviderDetails::model()->find(
                'provider_id=:providerId',
                [':providerId' => $providerId]
            );
            $needToSave = false;
            if (empty($providerDetails)) {
                $needToSave = true;
                $providerDetails = new ProviderDetails;
                $providerDetails->provider_id = $providerId;
            }
            if ($providerDetails->telemedicine_enabled != 1) {
                $needToSave = true;
                $providerDetails->telemedicine_enabled = 1;
                $providerDetails->telemedicine_enabled_by_account_id = $accountId;
            }
            if (
                !empty($providerPractice)
                &&
                $arrData['accept_new_patients'] != $providerPractice['telemedicine_accept_new_patients']
            ) {
                $needToSave = true;
                $providerDetails->telemedicine_accept_new_patients = $arrData['accept_new_patients'];
            }
            if ($needToSave) {
                $providerDetails->save();
            }
        }

        if (!empty($providerPractice)) {
            // already has telemedicine
            // return the existing id
            return $providerPractice;
        }

        // none found, create it
        $provider = Provider::model()->findByPk($providerId);
        if (empty($provider)) {
            Yii::log('Telemedicine: Could not find ' . $providerId, CLogger::LEVEL_WARNING);
            return false;
        }

        // create practice
        $practice = new Practice;
        $practice->name = 'Telemedicine practice for ' . (!empty($provider->prefix) ? $provider->prefix . ' ' : '')
            . $provider->first_last;
        if (!$practice->save()) {
            Yii::log(
                'Telemedicine: Could not save practice because: ' . json_encode($practice->getErrors()),
                CLogger::LEVEL_WARNING
            );
            return false;
        }

        // create entry in audit log
        AuditLog::create(
            'C',
            'Telemedicine practice being generated',
            'provider_practice',
            'Provider #' . $provider->id,
            ' Practice #' . $practice->id,
            false,
            $accountId
        );

        // create account-practice
        $ap = new AccountPractice;
        $ap->practice_id = $practice->id;
        $ap->account_id = $accountId;
        $ap->save();


        $needToCreateAppointment = true;
        // check if the appointment request is set
        if (isset($arrData['appointment_request'])) {
            // create the EMR
            $needToCreateAppointment = (!empty($arrData['appointment_request'])) ? $arrData['appointment_request'] : 0;
        }
        if ($needToCreateAppointment) {
            // automatically create account emr if needed
            $hasEMR = AccountEmr::model()->exists('account_id=:accountId', [':accountId' => $accountId]);
            if (!$hasEMR) {
                $ae = new AccountEmr;
                $ae->account_id = $accountId;
                $ae->emr_type_id = 3; // doctor.com
                $ae->save();
            }

            // create account_practice_emr
            $ape = new AccountPracticeEmr;
            $ape->practice_id = $practice->id;
            $ape->emr_location_id = $practice->id;
            $ape->account_id = $accountId;
            $ape->save();

            // create account_provider_emr
            $ape = AccountProviderEmr::model()->find(
                'account_id=:accountId AND provider_id=:providerId',
                [':accountId' => $accountId, ':providerId' => $providerId]
            );
            if (empty($ape)) {
                $ape = new AccountProviderEmr;
                $ape->provider_id = $providerId;
                $ape->emr_user_id = $providerId;
                $ape->account_id = $accountId;
                $ape->save();
            }
        }


        // create or update practice_details
        $practiceDetails = PracticeDetails::model()->find('practice_id=:practiceId', [':practiceId' => $practice->id]);
        if (empty($practiceDetails)) {
            $practiceDetails = new PracticeDetails;
            $practiceDetails->practice_id = $practice->id;
        }
        $practiceDetails->practice_type_id = 5; // Telemedicine Booking (internal)
        if (!$practiceDetails->save()) {
            Yii::log(
                'Telemedicine: Could not save practice details because: ' . json_encode($practiceDetails->getErrors()),
                CLogger::LEVEL_WARNING
            );
            return false;
        }

        // create provider_practice - set scenario to telemedicine so rank_order and primary_practice aren't set
        $providerPractice = new ProviderPractice('telemedicine');
        $providerPractice->provider_id = $providerId;
        $providerPractice->practice_id = $practice->id;
        if (!$providerPractice->save()) {
            Yii::log(
                'Telemedicine: Could not save provider_practice details because: ' .
                    json_encode($providerPractice->getErrors()),
                CLogger::LEVEL_WARNING
            );
            return false;
        }

        // update provider details, creating it if necessary
        $providerDetails = ProviderDetails::model()->find(
            'provider_id=:providerId',
            [':providerId' => $providerId]
        );
        if (empty($providerDetails)) {
            $providerDetails = new ProviderDetails;
            $providerDetails->provider_id = $providerId;
        }
        $providerDetails->telemedicine_enabled = 1;
        $providerDetails->telemedicine_accept_new_patients =
            (!empty($arrData) ? (bool)$arrData['accept_new_patients'] : 1);
        $providerDetails->telemedicine_enabled_by_account_id = $accountId;
        $providerDetails->save();

        $providerPractice = [
            'id' => $providerPractice->id,
            'provider_id' => $providerId,
            'practice_id' => $practice->id
        ];

        return $providerPractice;
    }

    /**
     * Remove a provider's telemedicine practice
     * @param int $providerId
     * @return bool
     */
    public static function removeTelemedicinePractice($providerId)
    {

        if (empty($providerId)) {
            return false;
        }

        // look up telemedicine practice
        $pp = self::getTelemedicinePractice($providerId);
        if (!empty($pp)) {

            // found, delete schedule
            $pps = ProviderPracticeSchedule::model()->findAll(
                'provider_practice_id=:providerPracticeId',
                [
                    ':providerPracticeId' => $pp['id']
                ]
            );
            if (!empty($pps)) {
                foreach ($pps as $s) {
                    $s->delete();
                }
            }

            // found, delete insurances
            $ppi = ProviderPracticeInsurance::model()->findAll(
                'provider_id=:providerId AND practice_id=:practiceId',
                [':providerId' => $providerId, ':practiceId' => $pp['practice_id']]
            );
            if (!empty($ppi)) {
                foreach ($ppi as $i) {
                    $i->delete();
                }
            }

            // found, delete devices
            $ads = AccountDevice::model()->findAll(
                'practice_id=:practiceId',
                [':practiceId' => $pp['practice_id']]
            );
            if (!empty($ads)) {
                foreach ($ads as $ad) {

                    // try to remove external rr
                    $rres = ReviewRequestExternal::model()->findAll(
                        'account_device_id=:accountDeviceId',
                        [':accountDeviceId' => $ad->id]
                    );
                    if (!empty($rres)) {
                        foreach ($rres as $rre) {
                            $rre->delete();
                        }
                    }

                    $ad->delete();
                }
            }

            // delete practice details
            $pds = PracticeDetails::model()->findAll(
                'practice_id=:practiceId',
                [':practiceId' => $pp['practice_id']]
            );
            if (!empty($pds)) {
                foreach ($pds as $pd) {
                    $pd->delete();
                }
            }

            // set null all SchedulingMail for this practice
            $sm = SchedulingMail::model()->findAll(
                'practice_id=:practiceId',
                [':practiceId' => $pp['practice_id']]
            );
            if (!empty($sm)) {
                foreach ($sm as $appointment) {
                    $appointment->practice_id = null;
                    $appointment->save();
                }
            }

            // delete link to provider
            $providerPractice = ProviderPractice::model()->findByPk($pp['id']);
            if ($providerPractice) {
                $providerPractice->delete();
            }

            // delete actual practice
            $practice = Practice::model()->findByPk($pp['practice_id']);
            if ($practice) {
                return $practice->delete();
            }
            return true;
        }
    }

    /**
     * Removes each provider practice ref relation from partner site entity match table.
     *
     * @param int $providerPracticeId
     * @return boolean
     */
    protected function removeProviderPracticeRef($providerPracticeId)
    {
        // try to find each relation
        $pse = PartnerSiteEntityMatch::model()->findAll(
            'internal_column_value = :id AND retrieved_entity_type = "provider_practice" AND ' .
                'retrieved_column_name = "provider_practice_ref"',
            [':id' => $providerPracticeId]
        );

        // nothing to remove
        if (empty($pse)) {
            return true;
        }

        // delete each record
        foreach ($pse as $p) {
            $p->delete();
        }

        return true;
    }
}
