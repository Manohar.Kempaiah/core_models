<?php

Yii::import('application.modules.core_models.models._base.BaseEmrScheduleException');

class EmrScheduleException extends BaseEmrScheduleException
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function scopes()
    {
        return array(
            'active' => array(
                'condition'=> $this->tableAlias . '.enabled_flag = 1 AND  ' .$this->tableAlias . '.del_flag = 0',
            ),
            'exceptions' => array(
                'condition'=> $this->tableAlias . '.day_number != 0',
            ),
            'vacations' => array(
                'condition'=> $this->tableAlias . '.day_number = 0',
            ),
        );
    }

    public function byPractice($practiceId)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $this->tableAlias . '.practice_id = '. (int) $practiceId,
        ));
        return $this;
    }

    public function byProvider($providerId)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $this->tableAlias . '.provider_id = '. (int) $providerId,
        ));
        return $this;
    }

    public function byDayNumber($dayNumber)
    {
        $this->getDbCriteria()->mergeWith(array(
            'condition' => $this->tableAlias . '.day_number = '. (int) $dayNumber,
        ));
        return $this;
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // delete from EmrScheduleExceptionHour
        $arrESEH = EmrScheduleExceptionHour::model()->findAll(
            'emr_schedule_exception_id = :id',
            array(':id' => $this->id)
        );
        if (!empty($arrESEH)) {
            foreach ($arrESEH as $eachRecord) {
                $eachRecord->delete();
            }
        }
        return parent::beforeDelete();
    }

    /**
     * Validate date_start and date_end
     * @return bool
     */
    public function beforeSave()
    {

        if ($this->id) {
            $this->date_updated = date('Y-m-d H:i:s');
        } else {
            $this->date_added = date('Y-m-d H:i:s');
            $this->date_updated = date('Y-m-d H:i:s');
        }

        if (!empty($this->date_start) && !empty($this->date_end)) {
            $start = date_create_from_format("Y-m-d", $this->date_start);
            $end = date_create_from_format("Y-m-d", $this->date_end);
            if ($start > $end) {
                $this->addError('date_start', 'Start date must be earlier than or equal to end date.');
                return false;
            }
        } elseif (empty($this->date_start) && !empty($this->date_end)) {
            $this->addError('date_start', 'Start date is required for vacations.');
            return false;
        } elseif (!empty($this->date_start) && empty($this->date_end)) {
            $this->addError('date_end', 'End date is required for vacations.');
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Get the provider-practice pairs having an associated EMR for an account
     * @param int $accountId Account id
     * @return array
     */
    public static function getEMRProviderPractice($accountId)
    {

        $sql = sprintf(
            "SELECT *,
            IF(emr_schedule_exception.id IS NULL, FALSE, TRUE) AS checked,
            provider_practice.provider_id AS provider_id,
            provider_practice.practice_id AS practice_id,
            provider_practice.email AS pp_email,
            provider_practice.phone_number AS pp_phone_number,
            provider_practice.fax_number AS pp_fax_number,
            provider_practice.id AS provider_practice_id
            FROM practice
            INNER JOIN provider_practice
            ON provider_practice.practice_id = practice.id
            INNER JOIN provider
            ON provider.id = provider_practice.provider_id
            INNER JOIN account_practice_emr
            ON account_practice_emr.practice_id = practice.id
            INNER JOIN account_provider_emr
            ON account_provider_emr.provider_id = provider.id
            LEFT JOIN emr_schedule_exception
            ON emr_schedule_exception.provider_id = provider.id
            AND emr_schedule_exception.practice_id = practice.id
            LEFT JOIN practice_details ON practice.id = practice_details.practice_id
            WHERE account_practice_emr.account_id = %d
            AND account_provider_emr.account_id = %d
            AND practice_details.practice_type_id != 5
            GROUP BY provider.id, practice.id
            ORDER BY practice.name;",
            $accountId,
            $accountId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * API - Get the provider-practice pairs having an associated EMR for an account
     * @param int $accountId Account id
     * @return array
     */
    public static function getScheduleExceptionForApi($accountId)
    {

        $sql = sprintf(
            "SELECT ese.id AS schedule_exception_id, ese.provider_id, provider.first_middle_last AS provider_name,
                provider.last_name, provider.first_name,
                ese.practice_id, practice.name AS practice_name, ese.day_number, ese.date_start, ese.date_end,
                eseh.time_start, eseh.time_end, eseh.id AS schedule_exception_hour_id
                FROM emr_schedule_exception AS ese
                INNER JOIN provider_practice
                ON provider_practice.practice_id = ese.practice_id
                AND provider_practice.provider_id = ese.provider_id
                INNER JOIN provider
                ON provider.id = ese.provider_id
                INNER JOIN practice
                ON practice.id = ese.practice_id
                INNER JOIN account_practice_emr
                ON account_practice_emr.practice_id = ese.practice_id
                INNER JOIN account_provider_emr
                ON account_provider_emr.provider_id = ese.provider_id
                LEFT JOIN emr_schedule_exception_hour AS eseh
                ON ese.id = eseh.emr_schedule_exception_id AND eseh.enabled_flag = 1 AND eseh.del_flag = 0
                WHERE account_practice_emr.account_id = %d AND account_provider_emr.account_id = %d
                AND ese.enabled_flag = 1 AND ese.del_flag = 0
                GROUP BY schedule_exception_id;",
            $accountId,
            $accountId
        );

        $tempResult = Yii::app()->db->createCommand($sql)->queryAll();
        $result = array();
        if (!empty($tempResult)) {
            foreach ($tempResult as $tr) {

                if ($tr['day_number'] == 0) {
                    $result['vacations'][] = $tr;

                } else {

                    // This is to get exceptions for the same day at different hours
                    $criteria = new CDbCriteria;
                    $criteria->compare('emr_schedule_exception_id', $tr['schedule_exception_id']);
                    $criteria->compare('enabled_flag', 1);
                    $criteria->compare('del_flag', 0);
                    $criteria->order = 'time_start ASC';

                    $hours = EmrScheduleExceptionHour::model()->findAll($criteria);
                    if (!empty($hours)) {
                        foreach ($hours as $h) {
                            $tr['time_start'] = $h->time_start;
                            $tr['time_end'] = $h->time_end;
                            $tr['schedule_exception_hour_id'] = $h->id;
                            $result['schedule_exceptions'][] = $tr;
                        }
                    } else {
                        $result['schedule_exceptions'][] = $tr;
                    }

                }
            }
        }
        return $result;
    }

    /**
     * Retrieve the vacations registered for a provider in a practice
     * @param int $providerId Provider id
     * @param int $practiceId Practice id
     * @return array
     */
    public static function getVacations($providerId, $practiceId)
    {
        return EmrScheduleException::model()->findAll(
            "provider_id = :provider_id AND practice_id = :practice_id AND
                date_start IS NOT NULL AND enabled_flag = 1 AND del_flag = 0",
            array(
                ":provider_id" => $providerId,
                ":practice_id" => $practiceId
            )
        );
    }

    /**
     * Find duplicated vacations
     * @param obj $emrScheduleExceptionModel
     * @return obj $emrScheduleExceptionModel
     */
    public static function getDuplicatedVacations($emrScheduleExceptionModel)
    {

        return EmrScheduleException::model()->exists(
            "provider_id = :provider_id
            AND practice_id = :practice_id
            AND day_number = :day_number
            AND date_start = :date_start
            AND date_end = :date_end
            AND id != :id
            AND enabled_flag = 1
            AND del_flag = 0",
            array(
                ":provider_id" => $emrScheduleExceptionModel->provider_id,
                ":practice_id" => $emrScheduleExceptionModel->practice_id,
                ":day_number" => 0,
                ":date_start" => $emrScheduleExceptionModel->date_start,
                ":date_end" => $emrScheduleExceptionModel->date_end,
                ":id" => $emrScheduleExceptionModel->id ? $emrScheduleExceptionModel->id : 0
            )
        );

    }

    /**
     * Retrieve the EMR schedule exceptions of a provider at a practice
     * @param int $providerId Provider id
     * @param int $practiceId Practice id
     * @return array
     */
    public static function getExceptions($providerId, $practiceId)
    {
        $exceptions = EmrScheduleException::model()->findAll(
            "provider_id = :provider_id AND practice_id = :practice_id AND
                date_start IS NULL AND enabled_flag = 1 AND del_flag = 0",
            array(
                ":provider_id" => $providerId,
                ":practice_id" => $practiceId
            )
        );

        $result = array();
        foreach ($exceptions as $e) {

            $criteria = new CDbCriteria;
            $criteria->compare('emr_schedule_exception_id', $e->id);
            $criteria->order = 'time_start ASC';

            $result[$e->day_number] = EmrScheduleExceptionHour::model()->findAll($criteria);
        }

        return $result;
    }



    /**
     * Find if this new appointment is inside schedule exception hours range
     * @param array () $appData
     * $appData['schedulingMailId'] = Yii::app()->request->getParam('schedulingMailId', false);
     * $appData['newStatus'] = Yii::app()->request->getParam('newStatus', false);
     * $appData['newDate'] = Yii::app()->request->getParam('newDate', false);
     * $appData['startHour'] = Yii::app()->request->getParam('startHour', false);
     * $appData['startMin'] = Yii::app()->request->getParam('startMin', false);
     * $appData['startAmPm'] = Yii::app()->request->getParam('startAmPm', false);
     * $appData['endHour'] = Yii::app()->request->getParam('startHour', false);
     * $appData['endMin'] = Yii::app()->request->getParam('startMin', false);
     * $appData['endAmPm'] = Yii::app()->request->getParam('startAmPm', false);
     * $appData['overBooking'] = Yii::app()->request->getParam('overBooking', 0);
     * $appData['practiceId'] = Yii::app()->request->getParam('practiceId', 0);
     * @return bool $isFree
     */
    public static function isInsideScheduledException($appData = '')
    {
        // Possible values for $isFree
        // 0: This appointment is in use
        // 1: Is free for use
        // 2: Is an overBooking
        // 3: Out of practice office hours
        // 4: Invalid Date
        // 5: In Vacation
        // 6: In Scheduled Exception
        $isFree = 1;

        if ($appData['newStatus'] == 'RE' && !empty($appData['practiceId'])) {
            $schedulingMail = SchedulingMail::model()->findByPk($appData['schedulingMailId']);

            // Get exceptions for this practice and provider
            $exceptionModels = EmrScheduleException::model()->findAll(
                "provider_id = :provider_id AND practice_id = :practice_id AND
                day_number IS NOT NULL AND enabled_flag = 1 AND del_flag = 0",
                array(
                    ":provider_id" => $schedulingMail->provider_id,
                    ":practice_id" => $appData['practiceId']
                )
            );

            if (!empty($exceptionModels)) {
                // Prepare appointment data to be compared
                list($year, $month, $day) = explode('-', $appData['newDate']);
                $dayOfWeek = date('w', mktime(0, 0, 0, $month, $day, $year));
                $appointmentHour = date(
                    "H:i",
                    strtotime($appData['startHour'] . ':' . $appData['startMin'] . ' ' . $appData['startAmPm'])
                );

                // Go through each and check whether the schedule falls within the range of any
                // First, check if the schedule corresponds to the day where we are trying to save this
                foreach ($exceptionModels as $day) {
                    // First check whether an exception was already been found and break if that's the case
                    if ($isFree == 6) {
                        break;
                    }

                    // We check whether the current exception is for the day we are checking
                    $dayNumber = $day['day_number'];
                    if ($dayNumber == $dayOfWeek) {
                        // If it is between, check if the appointment hour falls within the exception hours
                        $exceptionHours = EmrScheduleExceptionHour::model()->findAll(
                            "emr_schedule_exception_id = :emr_schedule_exception_id",
                            array(
                                ":emr_schedule_exception_id" => $day['id']
                            )
                        );

                        foreach ($exceptionHours as $hour) {
                            // Cycle through the exceptions to check hour ranges
                            $exceptionStartHour = date(
                                "H:i",
                                strtotime($hour['time_start'])
                            );
                            $exceptionEndHour = date(
                                "H:i",
                                strtotime($hour['time_end'])
                            );
                            if ($appointmentHour <= $exceptionEndHour && $appointmentHour >= $exceptionStartHour) {
                                // New time is within an exception
                                $isFree = 6;
                                break;
                            }

                        }
                    }
                }
            }
        }
        return $isFree;
    }

    /**
     * Delete all the schedule exceptions defined for an account
     * @param int $accountId
     */
    public static function deleteByAccountId($accountId)
    {
        $sql = sprintf(
            "SELECT provider_practice.provider_id, provider_practice.practice_id
            FROM provider_practice
            INNER JOIN account_practice_emr
                ON provider_practice.practice_id = account_practice_emr.practice_id
            INNER JOIN account_provider_emr
                ON provider_practice.provider_id = account_provider_emr.provider_id
            WHERE account_practice_emr.account_id = %d
            AND account_provider_emr.account_id = %d",
            $accountId,
            $accountId
        );
        $pps = Yii::app()->db->createCommand($sql)->query();

        foreach ($pps as $pp) {
            $eses = EmrScheduleException::model()->findAll(
                "provider_id = :provider_id AND practice_id = :practice_id
                    AND enabled_flag = 1 AND del_flag = 0",
                array(":provider_id" => $pp['provider_id'], ":practice_id" => $pp['practice_id']))
            ;
            foreach ($eses as $ese) {
                $esehs = $ese->emrScheduleExceptionHours;
                foreach ($esehs as $eseh) {
                    if (!$eseh->delete()) {
                        return false;
                    }
                }
                if (!$ese->delete()) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * MI7 - Blocked Schedule
     *
     * @param array $schedule
     * @param int $accountId
     * @param int $aEmrId
     * @param int $practiceId
     * @return array
     */
    public static function mi7BlockedSchedule($schedule, $accountId, $aEmrId, $practiceId)
    {
        $result = array();

        $resultProvider = AccountProviderEmr::mi7FindProviderEmr($schedule, $accountId, $aEmrId);

        if ($resultProvider['results']) {
            $providerId = $resultProvider['providerId'];
        } else {
            return $resultProvider;
        }

        $dateSch = explode("T", $schedule['StartDate']);
        $scheduleStart = $dateSch[0];

        $startTime = explode("Z", $dateSch[1]);
        $startTime = $startTime[0];

        $dateSch = explode("T", $schedule['EndDate']);
        $scheduleEnd = $dateSch[0];

        $endTime = explode("Z", $dateSch[1]);
        $endTime = $endTime[0];

        $ese = new EmrScheduleException;
        $ese->provider_id = $providerId;
        $ese->practice_id = $practiceId;
        $ese->day_number = 0;
        $ese->date_start = $scheduleStart;
        $ese->date_end = $scheduleEnd;
        $ese->enabled_flag = 1;
        $ese->del_flag = 0;
        $ese->date_added = date("Y-m-d H:i:s");
        if (!$ese->save()) {
            $result['result'] = false;
            $result['results'] = $ese->getErrors();
            return $result;
        }

        if ($scheduleStart == $scheduleEnd) {

            $eseh = new EmrScheduleExceptionHour;
            $eseh->emr_schedule_exception_id = $ese->id;
            $eseh->time_start = $startTime;
            $eseh->time_end = $endTime;
            $ese->enabled_flag = 1;
            $ese->del_flag = 0;
            $eseh->save();
        }

        $result['result'] = true;
        $result['results'] = "success";

        return $result;
    }

    /**
     * MI7 - Unblocked Schedule
     *
     * @param array $schedule
     * @param int $accountId
     * @param int $aEmrId
     * @param int $practiceId
     * @return array
     */
    public static function mi7UnblockedSchedule($schedule, $accountId, $aEmrId, $practiceId)
    {
        $result = array();

        $resultProvider = AccountProviderEmr::mi7FindProviderEmr($schedule, $accountId, $aEmrId);

        if ($resultProvider['result']) {
            $providerId = $resultProvider['providerId'];
        } else {
            return $resultProvider;
        }

        $dateSch = explode("T", $schedule['StartDate']);
        $scheduleStart = $dateSch[0];

        $startTime = explode("Z", $dateSch[1]);
        $startTime = $startTime[0];

        $dateSch = explode("T", $schedule['EndDate']);
        $scheduleEnd = $dateSch[0];

        $endTime = explode("Z", $dateSch[1]);
        $endTime = $endTime[0];

        $ese = EmrScheduleException::model()->findAll(
            "provider_id = :provider_id AND practice_id = :practice_id
                AND date_start = :date_start AND date_end = :date_end
                AND enabled_flag = 1 AND del_flag = 0",
            array(
                ":provider_id" => $providerId,
                ":practice_id" => $practiceId,
                ":date_start" => $scheduleStart,
                ":date_end" => $scheduleEnd
            ));

        if ($ese) {
            foreach ($ese as $scheduleException) {
                $eseh = EmrScheduleExceptionHour::model()->find(
                    "emr_schedule_exception_id = :emr_schedule_exception_id",
                    array(
                        ":emr_schedule_exception_id" => $scheduleException->id
                    )
                );

                if ($eseh) {
                    $eseh->delete();
                }

                $scheduleException->delete();
            }

            $result['result'] = true;
            $result['results'] = "success";
        } else {
            $result['result'] = false;
            $result['results'] = "Schedule doesn't exist";
        }

        return $result;
    }

    /**
     * Save Vacations
     * @param array $postData
     * @param int $accountId
     * @return array
     */
    public static function saveVacations($postData = array(), $accountId = 0)
    {
        $results['success'] = true;
        $results['error'] = '';
        $results['data'] = [];

        if (empty($postData) || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        $accountProvidersPractices = AccountProvider::getAccountProvidersPracticesAPI($accountId);

        foreach ($postData as $value) {

            $practiceId = $value['practice_id'];
            $providerId = $value['provider_id'];
            $allowAccess = false;
            foreach ($accountProvidersPractices as $v) {
                if ($v['practice_id'] == $practiceId && $v['provider_id'] == $providerId) {
                    $allowAccess = true;
                    break;
                }
            }
            if (!$allowAccess) {
                $results['success'] = false;
                $results['error'] = "ACCESS_DENIED";
                return $results;
            }

            $scheduleMailId = !empty($value['id']) ? $value['id'] : 0;
            $scheduleMailId = !empty($value['schedule_exception_id']) ?
                $value['schedule_exception_id'] : $scheduleMailId;
            $backendAction = !empty($value['backend_action']) ? $value['backend_action'] : '';

            $vacationsModel = false;
            if (!empty($scheduleMailId)) {
                $vacationsModel = EmrScheduleException::model()->findByPk($scheduleMailId);

                if (!empty($vacationsModel) && $backendAction == 'delete') {
                    if (!$vacationsModel->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_APPOINTMENT_VACATIONS";
                        $results['data'] = $vacationsModel->getErrors();
                        return $results;
                    }
                    continue;
                }
            }
            if (!$vacationsModel) {
                $vacationsModel = new EmrScheduleException();
            }
            $vacationsModel->practice_id = $practiceId;
            $vacationsModel->provider_id = $providerId;
            $vacationsModel->enabled_flag = 1;
            $vacationsModel->del_flag = 0;
            $vacationsModel->date_added = date('Y-m-d H:i:s');

            // Format date from m/d/Y to Y-m-d
            if (!empty($value['date_start']) && !empty($value['date_end'])) {
                $vacationsModel->date_start = date("Y-m-d", strtotime($value['date_start']));
                $vacationsModel->date_end = date("Y-m-d", strtotime($value['date_end']));
            }

            // look for duplicate vacations
            $isDuplicate = EmrScheduleException::getDuplicatedVacations($vacationsModel);

            if (empty($isDuplicate)) {

                if ($vacationsModel->save()) {
                    $results['success'] = true;
                    $results['error'] = "";
                    $value['status'] = 'SAVED';
                    $value['id'] = $vacationsModel->id;
                    $value['schedule_exception_id'] = $vacationsModel->id;
                    $results['data'][] = $value;
                } else {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_APPOINTMENT_VACATIONS";
                    $results['data'] = $vacationsModel->getErrors();
                    return $results;
                }

            } else {

                $results['success'] = true;
                $results['error'] = "DUPLICATED";
                $results['data'][] = $value;

            }

            // Reload cache
            CapybaraCache::loadScheduleExceptions($practiceId, $providerId);
        }
        return $results;
    }

    /**
     * Save ScheduleExceptions
     * @param array $postData
     * @param int $accountId
     * @return array
     */
    public static function saveScheduleExceptions($postData = array(), $accountId = 0)
    {
        $results['success'] = true;
        $results['error'] = '';
        $results['data'] = [];

        if (empty($postData) || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        $accountProvidersPractices = AccountProvider::getAccountProvidersPracticesAPI($accountId);

        foreach ($postData as $key => $value) {

            // Expected data:
            // array (
            //      'schedule_exception_id' => '278106'
            //      'schedule_exception_hour_id' => '1234'
            //      'provider_id' => '3346049'
            //      'practice_id' => '3659772'
            //      'day_number' => '7'
            //      'date_start' => null
            //      'date_end' => null
            //      'time_start' => '09:00:00'
            //      'time_end' => '17:00:00'
            //      'backend_action' => 'delete'
            // )

            $practiceId = $value['practice_id'];
            $providerId = $value['provider_id'];
            $backendAction = !empty($value['backend_action']) ? trim($value['backend_action']) : '';
            $scheduleExceptionId = !empty($value['schedule_exception_id']) ? (int) $value['schedule_exception_id'] : 0;
            $scheduleExceptionHourId = !empty($value['schedule_exception_hour_id']) ?
                    (int) $value['schedule_exception_hour_id'] : 0;
            $timeStart = !empty($value['time_start']) ? $value['time_start'] : '';
            $timeEnd = !empty($value['time_end']) ? $value['time_end'] : '';

            $allowAccess = false;
            foreach ($accountProvidersPractices as $v) {
                if ($v['practice_id'] == $practiceId && $v['provider_id'] == $providerId) {
                    $allowAccess = true;
                    break;
                }
            }
            if (!$allowAccess) {
                $results['success'] = false;
                $results['error'] = "ACCESS_DENIED";
                return $results;
            }

            if ($scheduleExceptionId > 0 && ($backendAction == 'delete' || empty($timeStart) || empty($timeEnd))) {

                // Find the parent model
                $exceptionsModel = EmrScheduleException::model()->find(
                    "id = :id",
                    array(
                        ":id" => $scheduleExceptionId
                    )
                );

                // Looking for hour model
                if (!empty($exceptionsModel) && !empty($scheduleExceptionHourId)) {

                    // We have $scheduleExceptionHourId, look for the model
                    $exceptionHourModel = EmrScheduleExceptionHour::model()->find(
                        "emr_schedule_exception_id = :emr_schedule_exception_id AND id = :schedule_exception_hour_id",
                        array(
                            ":emr_schedule_exception_id" => $scheduleExceptionId,
                            ":schedule_exception_hour_id" => $scheduleExceptionHourId
                        )
                    );
                    if (!empty($exceptionHourModel) && !$exceptionHourModel->delete()) {
                        // We have hour model, delete it
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_SCHEDULING_EXCEPTIONS";
                        $results['data'][$key] = $exceptionHourModel->getErrors();
                        return $results;
                    }

                    // After deleting, check if there are any childs left
                    $exceptionHoursModels = EmrScheduleExceptionHour::model()->findAll(
                        "emr_schedule_exception_id = :emr_schedule_exception_id",
                        array(":emr_schedule_exception_id" => $scheduleExceptionId)
                    );

                    // If there are no $exceptionHoursModels left for this $exceptionsModel, we should delete it
                    if (empty($exceptionHoursModels) && !$exceptionsModel->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_SCHEDULING_EXCEPTIONS";
                        $results['data'][$key] = $exceptionsModel->getErrors();
                        return $results;
                    }
                }

                continue;
            }

            $exceptionsModel = EmrScheduleException::model()->find("id = :id", array(":id" => $scheduleExceptionId));
            if (empty($exceptionsModel)) {
                $exceptionsModel = EmrScheduleException::model()->find(
                    "provider_id = :provider_id AND practice_id = :practice_id AND day_number = :day_number
                    AND enabled_flag = 1 AND del_flag = 0",
                    array(
                        ":provider_id" => $providerId,
                        ":practice_id" => $practiceId,
                        ":day_number" => $value['day_number'],
                    )
                );
            }
            if (empty($exceptionsModel)) {
                $exceptionsModel = new EmrScheduleException();
            }
            $exceptionsModel->provider_id = $providerId;
            $exceptionsModel->practice_id = $practiceId;
            $exceptionsModel->day_number = $value['day_number'];
            $exceptionsModel->date_start = null;
            $exceptionsModel->date_end = null;
            $exceptionsModel->enabled_flag = 1;
            $exceptionsModel->del_flag = 0;
            $exceptionsModel->date_added = date('Y-m-d H:i:s');

            if ($exceptionsModel->save()) {
                $scheduleExceptionId = $exceptionsModel->id;
                $value['schedule_exception_id'] = $scheduleExceptionId;

                // looking for hours
                $exceptionHoursModel = EmrScheduleExceptionHour::model()->find(
                    "emr_schedule_exception_id = :emr_schedule_exception_id AND id = :schedule_exception_hour_id",
                    array(
                        ":emr_schedule_exception_id" => $scheduleExceptionId,
                        ":schedule_exception_hour_id" => $scheduleExceptionHourId
                    )
                );
                if (!$exceptionHoursModel) {
                    $exceptionHoursModel = new EmrScheduleExceptionHour();
                    $exceptionHoursModel->emr_schedule_exception_id = $scheduleExceptionId;
                }
                $exceptionHoursModel->time_start = $timeStart;
                $exceptionHoursModel->time_end = $timeEnd;
                $exceptionHoursModel->enabled_flag = 1;
                $exceptionHoursModel->del_flag = 0;

                if ($exceptionHoursModel->save()) {
                    $value['status'] = 'SAVED';
                    $value['schedule_exception_hour_id'] = $exceptionHoursModel->id;
                    $results['success'] = true;
                    $results['error'] = "";
                    $results['data'][$key] = $value;
                } else {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_SCHEDULE_EXCEPTIONS_HOURS";
                    $results['data'][$key] = $exceptionHoursModel->getErrors();
                    return $results;
                }
            } else {
                $results['success'] = false;
                $results['error'] = "ERROR_SAVING_SCHEDULE_EXCEPTIONS";
                $results['data'][$key] = $exceptionsModel->getErrors();
                return $results;
            }

            // Reload cache
            CapybaraCache::loadScheduleExceptions($practiceId, $providerId);
        }

        return $results;
    }
}
