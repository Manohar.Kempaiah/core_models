<?php

Yii::import('application.modules.core_models.models._base.BaseTrackingActivity');

class TrackingActivity extends BaseTrackingActivity
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Generates SQL for Tracking Metrics
     * @param str $orgString
     * @return array
     */
    public static function getTrackingMetricsOrganization($orgString)
    {

        $sql = sprintf(
            "SELECT DISTINCT organization.organization_name, widget.account_id
            FROM prd01.organization AS organization
            INNER JOIN prd01.organization_account AS organization_account
            ON organization_account.organization_id = organization.id
            INNER JOIN prd01.widget AS widget
            ON widget.account_id = organization_account.account_id
            WHERE organization.status = 'A'
            AND organization_account.connection = 'O'
            AND widget.draft = 0
            %s
            ORDER BY organization.organization_name
            LIMIT 100;",
            $orgString
        );

        return Yii::app()->analyticsRO->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Generates SQL for Tracking Metrics
     * @param str $trackingParameter
     * @return array
     */
    public static function getTrackingActivity($trackingParameter)
    {
        $sql = sprintf(
            "SELECT
            COUNT(tracking_activity.tracking_activity_type_id) AS total,
            tracking_activity.tracking_activity_type_id,
            tracking_activity_parameter.value
            FROM prd01.tracking_activity AS tracking_activity
            INNER JOIN prd01.tracking_activity_parameter AS tracking_activity_parameter
            ON tracking_activity_parameter.tracking_activity_id = tracking_activity.id
            WHERE tracking_activity.tracking_activity_type_id IN (11, 12, 14)
            %s
            GROUP BY tracking_activity.tracking_activity_type_id, tracking_activity_parameter.value;",
            $trackingParameter
        );

        return Yii::app()->analyticsRO->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Generates SQL for Tracking Widget
     * @param int $accountId
     * @return array
     */
    public static function getTrackingWidget($accountId)
    {
        $sql = sprintf(
            "SELECT
            key_code
            FROM prd01.widget AS widget
            WHERE widget.account_id = %d
            AND widget.draft = 0;",
            $accountId
        );

        return Yii::app()->analyticsRO->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }
}
