<?php

Yii::import('application.modules.core_models.models._base.BaseScanErrorStatus');

class ScanErrorStatus extends BaseScanErrorStatus
{
    const PENDING = 1;
    const IN_PROGRESS = 2;
    const ADDRESSED = 3;
    const RESOLVED = 4;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
