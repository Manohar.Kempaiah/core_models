<?php

Yii::import('application.modules.core_models.models._base.BaseFelixAssociation');

class FelixAssociation extends BaseFelixAssociation
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'felix_locationid';
    }

    /**
     * Declares the validation rules.
     * @return array
     */
    public function rules()
    {
        return array(
            array('practice_id, association_type, date_updated', 'required'),
            array('practice_id, disabled', 'numerical', 'integerOnly' => true),
            array('association_type', 'length', 'max' => 30),
            array('disabled', 'default', 'setOnEmpty' => true, 'value' => null),
            array('felix_locationid, practice_id, association_type, date_updated, disabled', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        return array(
            'practice' => array(self::BELONGS_TO, 'Practice', 'practice_id'),
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'felix_locationid' => Yii::t('app', 'Felix ID'),
            'practice_id' => Yii::t('app', 'Practice'),
            'association_type' => Yii::t('app', 'Association Type'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'disabled' => Yii::t('app', 'Disabled'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->with = array('practice');

        $criteria->compare('felix_locationid', $this->felix_locationid);
        //$criteria->compare('practice_id', $this->practice_id);
        if (!intval($this->practice_id) && is_string($this->practice_id) && strlen($this->practice_id) > 0) {
            $criteria->compare('practice.name', $this->practice_id, true);
        } elseif (intval($this->practice_id)) {
            $criteria->compare('practice_id', $this->practice_id);
        }

        $criteria->compare('association_type', $this->association_type, false);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('disabled', $this->disabled);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100)
        ));
    }

    //called on rendering the column for each row
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id . '" rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice . '">' . $this->practice . '</a>');
    }

}
