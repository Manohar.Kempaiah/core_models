<?php

interface GenericScanResult
{
    public function getScrapedName();
    public function getScrapedAddress();
    public function getScrapedPhone();

}
