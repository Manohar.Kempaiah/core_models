<?php

Yii::import('application.modules.core_models.models._base.BaseDataSourceEntityMatch');

class DataSourceEntityMatch extends BaseDataSourceEntityMatch
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
