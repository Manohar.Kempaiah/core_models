<?php

Yii::import('application.modules.core_models.models._base.BaseProviderSchedule');

class ProviderSchedule extends BaseProviderSchedule
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_provider
            INNER JOIN provider_schedule
            ON account_provider.provider_id = provider_schedule.provider_id
            WHERE provider_schedule.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

}
