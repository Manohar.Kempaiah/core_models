<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteEmailAds');

class PartnerSiteEmailAds extends BasePartnerSiteEmailAds
{
    public static $EMAIL_ADS_TARGET = 'PATIENT';

    /*
     * Model
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Process beforeSave
     */
    public function beforeSave()
    {

        if (empty($this->title)) {
            return false;
        }
        if (empty($this->url)) {
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * get Partners
     * @return array
     */
    public static function getPartners()
    {
        $sql = "SELECT partner_site.id, partner_site.display_name
            FROM partner_site
            WHERE partner_site.id in (
                select distinct partner_site_id from scheduling_mail where partner_site_id != 1
            ) AND partner_site.partner_id > 1
            ORDER BY partner_site.display_name;";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * get All Partners
     * @return array
     */
    public static function getAllPartners()
    {
        $sql = "SELECT partner_site.id, partner_site.name, partner_site.display_name
            FROM partner_site
            WHERE partner_site.partner_id > 1 AND partner_site.display_name != ''
            ORDER BY partner_site.display_name;";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * get Ads
     * @param integer $partnerSiteId
     * @return array
     */
    public static function getAds($partnerSiteId)
    {

        $ad = PartnerSiteEmailAds::model()->find(
            'partner_site_id=:partner_site_id AND active = 1',
            [':partner_site_id' => $partnerSiteId]
        );
        $results = [];
        if (!empty($ad)) {
            $results = array(
                'title' => $ad->title,
                'text' => $ad->style,
                'id' => $ad->id,
                'url' => $ad->url,
            );
        }

        return $results;
    }

    /**
     * get the Active Ad for the partner site id
     * @param integer $partnerSiteId
     * @return object
     */
    public static function getActiveAd($partnerSiteId)
    {

        return PartnerSiteEmailAds::model()->find(
            'partner_site_id=:partner_site_id AND active = 1',
            [':partner_site_id' => $partnerSiteId]
        );
    }

}
