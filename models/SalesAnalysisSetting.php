<?php

Yii::import('application.modules.core_models.models._base.BaseSalesAnalysisSetting');

use application\components\Prospect\Reputation as ReputationComponent;
use application\components\Prospect\Scan as ScanComponent;

class SalesAnalysisSetting extends BaseSalesAnalysisSetting
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave()
    {
        // avoid duplicated records on inserts
        if ($this->isNewRecord) {

            // check for duplicated record
            $isDuplicated = $this->preventDuplicated();

            // stop saving the sales analysis when a duplication is detected
            if ($isDuplicated) {
                return false;
            }

            // set base if no analysis setting exists for this analysis
            $haveBase = $this->haveBase();

            if ($haveBase == false) {
                $this->base = 1;
            }
        }

        return parent::beforeSave();
    }

    /**
     * {@inheritdoc}
     */
    public function afterDelete()
    {
        // set base if no analysis setting exists for this analysis
        $haveBase = $this->haveBase();

        // to ensure having at least one sales analysis setting with base flag, get any of the existent and set the flag
        if ($haveBase == false) {
            $as = static::model()->find(
                'sales_analysis_id = :sales_analysis_id',
                [':sales_analysis_id' => $this->sales_analysis_id]
            );

            if ($as) {
                $as->base = 1;
                $as->save();
            }
        }

        // remove stats from analytics
        ReputationComponent::removeDashboardData($this->id);
        ScanComponent::removeDashboardData($this->id);

        return parent::afterDelete();
    }

    /**
     * Returns a boolean value indicating if the record that is trying to be inserted could be duplicated.
     *
     * @return boolean
     */
    protected function preventDuplicated()
    {
        // check for duplicated prospect
        if ($this->prospect_id) {
            // check if the prospect id was already added
            $exists = self::model()->find(
                'sales_analysis_id = :sales_analysis_id AND prospect_id = :prospect_id',
                [
                    ':sales_analysis_id' => $this->sales_analysis_id,
                    ':prospect_id' => $this->prospect_id,
                ]
            );

            if ($exists) {
                $this->addError('prospect_id', 'Prospect already added to this sales analysis');
                return true;
            }

            // not duplicated
            return false;
        }

        // check for duplicated organization
        if ($this->organization_id) {
            // check if the organization id was already added
            $exists = self::model()->find(
                'sales_analysis_id = :sales_analysis_id AND organization_id = :organization_id',
                [
                    ':sales_analysis_id' => $this->sales_analysis_id,
                    ':organization_id' => $this->organization_id,
                ]
            );

            if ($exists) {
                $this->addError('organization_id', 'Organization already added to this sales analysis');
                return true;
            }

            // not duplicated
            return false;
        }

        // this never should be reached
        return false;
    }

    /**
     * Returns true if the sales analysis have at least one record with base flag active.
     *
     * @return boolean
     */
    protected function haveBase()
    {
        return (boolean) self::model()->find(
            'sales_analysis_id = :sales_analysis_id AND base = 1',
            [
                ':sales_analysis_id' => $this->sales_analysis_id,
            ]
        );
    }

}
