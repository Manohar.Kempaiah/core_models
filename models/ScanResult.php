<?php

Yii::import('application.modules.core_models.models._base.BaseScanResult');

class ScanResult extends BaseScanResult
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Query the last scan for a practice's average rating score in Google and Yelp
     * @param int $practiceId
     * @return array
     */
    public static function getPracticeRating($practiceId = null)
    {

        if (empty($practiceId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT review_number
            FROM scan_result
            INNER JOIN scan ON scan_result.scan_id = scan.id
            WHERE practice_id = %d AND code = 'google' AND `year_month` = DATE_FORMAT(CURDATE(), '%%Y-%%c');",
            $practiceId
         );
        $googleRatings = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();

        $sql = sprintf(
            "SELECT review_number
            FROM scan_result
            INNER JOIN scan ON scan_result.scan_id = scan.id
            WHERE practice_id = %d AND code = 'yelp' AND `year_month` = DATE_FORMAT(CURDATE(), '%%Y-%%c');",
            $practiceId
        );
        $yelpRatings = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();

        return array($googleRatings, $yelpRatings);
    }

    /**
     * Get Scan Results
     * @param int $scanId
     * @return array
     */
    public static function getScanResults($scanId = null)
    {
        if (!$scanId) {
            return false;
        }
        $sql = sprintf(
            "SELECT
                id, scan_id, `error`, match_name, match_address, match_phone, `code`,
                review_number, review_average, url, scraped_name, scraped_address, scraped_phone, date_added,
                (match_name + match_address + match_phone) / 3 AS match_score
            FROM
                scan_result
            WHERE
                scan_id = %d;",
            $scanId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

}
