<?php

class ElsScan
{

    /**
     * Query to elastic for the monthly report for a given organizationId and return data as it
     * @param int $organizationId
     * @param int $year
     * @param int $month
     * @return array
     */
    private static function getHeatmap($organizationId, $year = null, $month = null)
    {

        if ($year == null) {
            $year = date('Y');
        }

        if ($month == null) {
            $month = date('n');
        }

        $scan = Scan::model()->find(
            '`organization_id`=:organizationId AND `year_month`=:yearMonth',
            array(
                ':organizationId' => $organizationId,
                ':yearMonth' => $year . '-' . $month
            )
        );

        if ($scan) {
            // $scan->year_month has format Y-n but we want Y-m
            $yearMonth = date_create_from_format('Y-n', $scan->year_month);
            $scanKey = md5($organizationId . '-' . date('Y', $yearMonth->getTimestamp()) . '-' .
                    date('m', $yearMonth->getTimestamp()));
        } else {
            return array();
        }

        $condition = [
            'query' => [
                'term' => [
                    'scan_key' => $scanKey
                ]
            ]
        ];

        // prepare elasticsearch connection
        $server = Yii::app()->params->servers['els_server'];
        $client = \Elasticsearch\ClientBuilder::create()->setHosts($server['hosts'])->build();

        // get data
        $params = [
            'index' => Yii::app()->params->elasticPrefixIndex . 'scan',
            'type' => 'report',
            'body' => [
                'query' => [
                    'filtered' => $condition
                ],
                "sort" => [
                    "practice_name" => [
                        "order" => "asc"
                    ],
                    "provider_name" => [
                        "order" => "asc"
                    ]
                ]
            ],
            'from' => 0,
            'size' => 10000
        ];

        try {

            $data = $client->search($params);
            $data = $data['hits']['hits'];

            if ($data) {
                // force sort by practice name
                foreach ($data as $key => $row) {
                    $mid[$key] = $row['_source']['practice_name'];
                }

                array_multisort($mid, SORT_ASC, $data);
            }
        } catch (Exception $e) {
            return array();
        }

        return $data;
    }

    /**
     * Get the providers monthly report for a given organizationId
     * @param int $organizationId
     * @param int $year
     * @param int $month
     * @return array
     */
    public static function getProvidersHeatmap($organizationId, $year = null, $month = null)
    {

        // backup memory limit and set to as much as possible
        $memoryLimit = ini_get('memory_limit');
        ini_set('memory_limit', -1);

        $data = self::getHeatmap($organizationId, $year, $month);

        if (empty($data)) {
            return array();
        }

        /**
         * @important hardcoded for https://dev.doctor.com/T33851
         *
         * @todo remove after the demo takes place (or we stop using yext)
         */
        $drOnly = false;
        $yextSites = array(
            'WHITEPAGES',
            'MAPQUEST',
            'SUPERPAGES',
            'CITYSEARCH',
            'LOCALCOM',
            'MERCHANTCIRCLE',
            'EZLOCAL',
            'LOCALDATABASE',
            'SHOWMELOCAL',
            'TOPIX',
            'CITYSQUARES',
            'LOCALPAGES',
            'MYLOCALSERVICES',
            'MOJOPAGES',
            'YELLOWISE',
            'YELLOWMOXIE',
            'AVANTAR',
            'COPILOT',
            'CYLEX',
            'WHERETO',
            'AROUNDME',
            'CREDIBILITYCOM',
            'DEXKNOWS',
            'INSIDERPAGES',
            'NAVMII',
            'YPCOM',
            'CHAMBEROFCOMMERCECOM',
            'USCITYNET',
            'YELLOWPAGECITYCOM',
            '8COUPONS',
            'ELOCAL',
            'FACTUAL',
            'YASABE',
            'CITYMAPS',
            'CITYBOT',
            'YELLOWPAGESGOESGREEN',
            'POINTCOM',
            'IGLOBAL',
            'ABLOCAL',
            'OPENDI',
            'VOTEFORTHEBEST',
            'N49CA',
            '2FINDLOCAL',
            'AMERICANTOWNSCOM',
            'GETFAVE',
            'GOLOCAL247',
            'BROWNBOOKNET',
            'CREDIBILITYREVIEW',
            'IBEGIN',
            'LOCALSTACK',
            'TUPALO'
        );

        $providerListings = [];
        $providers = [];

        foreach ($data as $record) {
            if (isset($record['_source']['provider_name'])) {
                $provider = array();

                // header
                foreach ($record['_source']['results'] as $result) {
                    if (!isset($providerListings[$result['code']])) {
                        $providerListings[$result['code']] = array(
                            "code" => $result['code'],
                            "name" => StringComponent::formatShortListingNames($result['code']),
                            "url" => $result['url']
                        );
                    }
                }

                $provider['scan_id'] = $record['_id'];
                $provider['name'] = $record['_source']['provider_name'];
                $provider['address'] = $record['_source']['address'];

                $provider['results'] = array();

                // add each result
                foreach ($record['_source']['results'] as $result) {
                    $score = (round(($result['match_phone'] + $result['match_name'] + $result['match_address']) / 3, 1))
                            * 100;

                    $provider['results'][$result['code']] = array(
                        "score" => $score,
                        "code" => $result['code'],
                        "url" => $result['url'] // needed to have each listing's url
                    );
                }

                $providers[] = $provider;
            }
        }

        foreach ($providerListings as $listing) {
            if ($drOnly
                &&
                (array_search($listing['code'], $yextSites) !== false
                ||
                array_search(StringComponent::formatShortListingNames($listing['code']), $yextSites) !== false)
                ) {
                    // skip if it's a yext site and we don't want to show them
                    unset($providerListings[$listing['code']]);
            }
        }

        // get formatted scan date
        $graphTitle = date('F Y', strtotime($data[0]['_source']['date_added']));
        $organizationName = $data[0]['_source']['organization'];

        // prepare elasticsearch connection
        $server = Yii::app()->params->servers['els_server'];
        $client = \Elasticsearch\ClientBuilder::create()->setHosts($server['hosts'])->build();
        $availableMonths = self::getAvailableMonths($organizationId, $client);

        $return = array(
            'providers' => $providers,
            'providerListings' => $providerListings,
            'drOnly' => $drOnly,
            'empty' => false,
            'graphTitle' => $graphTitle,
            'organizationName' => $organizationName,
            'orgID' => $organizationId,
            'availableMonths' => $availableMonths
        );

        // restore memory limit
        ini_set('memory_limit', $memoryLimit);

        return $return;
    }

    /** Get the practices monthly report for a given organizationId
     * @param int $organizationId
     * @param int $year
     * @param int $month
     * @return array
     */
    public static function getPracticesHeatmap($organizationId, $year = null, $month = null)
    {

        // backup memory limit and set to as much as possible
        $memoryLimit = ini_get('memory_limit');
        ini_set('memory_limit', -1);

        $data = self::getHeatmap($organizationId, $year, $month);

        if (empty($data)) {
            return array();
        }

        /**
         * @important hardcoded for https://dev.doctor.com/T33851
         *
         * @todo remove after the demo takes place (or we stop using yext)
         */
        $drOnly = false;
        $yextSites = array(
            'WHITEPAGES',
            'MAPQUEST',
            'SUPERPAGES',
            'CITYSEARCH',
            'LOCALCOM',
            'MERCHANTCIRCLE',
            'EZLOCAL',
            'LOCALDATABASE',
            'SHOWMELOCAL',
            'TOPIX',
            'CITYSQUARES',
            'LOCALPAGES',
            'MYLOCALSERVICES',
            'MOJOPAGES',
            'YELLOWISE',
            'YELLOWMOXIE',
            'AVANTAR',
            'COPILOT',
            'CYLEX',
            'WHERETO',
            'AROUNDME',
            'CREDIBILITYCOM',
            'DEXKNOWS',
            'INSIDERPAGES',
            'NAVMII',
            'YPCOM',
            'CHAMBEROFCOMMERCECOM',
            'USCITYNET',
            'YELLOWPAGECITYCOM',
            '8COUPONS',
            'ELOCAL',
            'FACTUAL',
            'YASABE',
            'CITYMAPS',
            'CITYBOT',
            'YELLOWPAGESGOESGREEN',
            'POINTCOM',
            'IGLOBAL',
            'ABLOCAL',
            'OPENDI',
            'VOTEFORTHEBEST',
            'N49CA',
            '2FINDLOCAL',
            'AMERICANTOWNSCOM',
            'GETFAVE',
            'GOLOCAL247',
            'BROWNBOOKNET',
            'CREDIBILITYREVIEW',
            'IBEGIN',
            'LOCALSTACK',
            'TUPALO'
        );

        $locationListings = [];
        $locations = [];

        foreach ($data as $record) {
            if (!isset($record['_source']['provider_name'])) {
                $location = array();

                // header
                foreach ($record['_source']['results'] as $result) {
                    if (!isset($locationListings[$result['code']])) {
                        $locationListings[$result['code']] = array(
                            "code" => $result['code'],
                            "name" => StringComponent::formatShortListingNames($result['code']),
                            "url" => $result['url'],
                        );
                    }
                }

                $location['scan_id'] = $record['_id'];
                $location['name'] = $record['_source']['practice_name'];
                $location['address'] = $record['_source']['address'];

                $location['results'] = array();

                // add each result
                foreach ($record['_source']['results'] as $result) {
                    $score = (round(($result['match_phone'] + $result['match_name'] + $result['match_address']) / 3, 1))
                            * 100;

                    $location['results'][$result['code']] = array(
                        "score" => $score,
                        "code" => $result['code'],
                        "url" => $result['url'] // needed to have each listing's url
                    );
                }

                $locations[] = $location;
            }
        }

        foreach ($locationListings as $listing) {
            if ($drOnly
                &&
                (
                    array_search($listing['code'], $yextSites) !== false
                    ||
                    array_search(StringComponent::formatShortListingNames($listing['code']), $yextSites) !== false)
                ) {
                    // skip if it's a yext site and we don't want to show them
                    unset($locationListings[$listing['code']]);
            }
        }

        // get formatted scan date
        $graphTitle = date('F Y', strtotime($data[0]['_source']['date_added']));
        $organizationName = $data[0]['_source']['organization'];

        $server = Yii::app()->params->servers['els_server'];
        $client = \Elasticsearch\ClientBuilder::create()->setHosts($server['hosts'])->build();
        $availableMonths = self::getAvailableMonths($organizationId, $client);

        $return = array(
            'locations' => $locations,
            'locationListings' => $locationListings,
            'drOnly' => $drOnly,
            'graphTitle' => $graphTitle,
            'organizationName' => $organizationName,
            'orgID' => $organizationId,
            'availableMonths' => $availableMonths
        );

        // restore memory limit
        ini_set('memory_limit', $memoryLimit);

        return $return;
    }

    /**
     * Gets months that has reports for the desired organization
     * @param int $orgID
     * @return array
     */
    private static function getAvailableMonths($orgID, $client)
    {

        if (!$orgID || !$client) {
            return false;
        }

        $found = 0;
        $date = mktime(0, 0, 0, date("m"), 1);
        $lastDate = mktime(0, 0, 0, 8, 1, 2016); //We don't have report information previous to this date
        $counter = 0;
        $availableMonths = [];

        while ($found <= 5 && $date >= $lastDate) {
            $scanKey = md5($orgID . '-' . date('Y', $date) . '-' . date('m', $date));
            $condition = [
                'query' => [
                    'term' => [
                        'scan_key' => $scanKey
                    ]
                ]
            ];
            $params = [
                'index' => Yii::app()->params->elasticPrefixIndex . 'scan',
                'type' => 'report',
                'body' => [
                    'query' => [
                        'filtered' => $condition
                    ]
                ],
                'from' => 0,
                'size' => 1
            ];

            $data = $client->search($params);
            if ($data['hits']['total'] > 0) {
                $availableMonths[] = $date;
                $found++;
            }

            $counter++;
            $date = mktime(0, 0, 0, date("m") - $counter, 1);
        }

        return $availableMonths;
    }

}
