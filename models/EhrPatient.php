<?php

Yii::import('application.modules.core_models.models._base.BaseEhrPatient');

class EhrPatient extends BaseEhrPatient
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function newPatient($installationId, $patient)
    {
        if (empty($patient)) {
            header("HTTP/1.0 500 Conflict");
            // The process ends
            Yii::app()->end();
        }

        $sql = sprintf(
            "select cpnehr.name from cpn_ehr_installation cehri "
            . "join cpn_ehr cpnehr on cehri.cpn_ehr_id = cpnehr.id where cehri.cpn_installation_id = '%d'",
            $installationId
        );
        $name = Yii::app()->db->createCommand($sql)->queryScalar();

        if (!isset($patient->homeEmail)) {
            $patient->homeEmail = 'nomail+' . $name . '_' . $installationId . '_' .
                    $patient->identifierInternal.'@corp.doctor.com';
        }

        $newPatientId = $this->savePatientData($patient);

        if (!empty($newPatientId)) {
            $patientEhrId = $this->saveEhrPatientData($installationId, $patient, $newPatientId);
            if ($patientEhrId) {
                return $newPatientId;
            }
        }
    }

    /**
     * Search a patient by primary Key and use internal updatePatientData function to Updates the Patient Data
     * @param JSON Object $patient
     * @param string $doctorPatientId
     * @return bool result
     */
    public function updatePatient($patient, $doctorPatientId)
    {
        if (empty($patient)) {
            header("HTTP/1.0 500 Conflict");
            // The process ends
            Yii::app()->end();
        }

        $ddcPatient = Patient::model()->findByPk($doctorPatientId);

        // If unexistant patient, return without saving data
        if (!$ddcPatient) {
            return false;
        }
        // Update EhrPatient data
        return (bool)$this->updatePatientData($ddcPatient, $patient);
    }

    /**
     * Get the id of the patient in the EHR
     * @param int $schedulingMailId
     * @param int $schedulingMail
     */
    public static function getMatchedPatientData($schedulingMailId, $patientId = 0)
    {
        if ($patientId == 0) {
            $scheduling = SchedulingMail::model()->findByPk($schedulingMailId);
            $patientId = $scheduling->patient_id;
        }

        $sql = sprintf(
            "SELECT cei.id "
            . "FROM cpn_ehr_installation cei "
            . "INNER JOIN cpn_installation ci ON ci.id = cei.cpn_installation_id "
            . "INNER JOIN account_provider ap ON ci.account_id = ap.account_id "
            . "INNER JOIN account_practice aq ON ci.account_id = aq.account_id "
            . "INNER JOIN scheduling_mail sm ON sm.provider_id = ap.provider_id "
            . " AND sm.practice_id = aq.practice_id "
            . "WHERE sm.id = '%d' AND ap.account_id = aq.account_id",
            $schedulingMailId
        );
        $cpnEhrInstallationId = Yii::app()->db->createCommand($sql)->queryScalar();

        // ehr match patient
        $ehrPatient = Patient::model()->findByPk($patientId);
        $smEhrPatient = null;

        if (!empty($cpnEhrInstallationId)) {
            $sql = sprintf(
                "SELECT
                    patient.id,
                    patient.first_name,
                    patient.last_name,
                    patient.mobile_phone,
                    patient.day_phone,
                    patient.night_phone,
                    patient.date_of_birth,
                    ehr_patient.ehr_patient_id
                  FROM patient
                  INNER JOIN ehr_patient ON ehr_patient.patient_id = patient.id
                  WHERE patient.id = %d AND ehr_patient.cpn_ehr_installation_id = %d",
                $patientId,
                $cpnEhrInstallationId
            );
            $smEhrPatient = Yii::app()->db->createCommand($sql)->queryAll();
        }

        return array("ehrPatient" => $ehrPatient, "smEhrPatient" => $smEhrPatient);
    }

    /**
     * Get the current New York DateTime
     * @return DateTime $ny_datetime
     */
    public function getCurrentNewYorkDateTime()
    {
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('America/New_York'));
        return $date->format('Y-m-d H:i:s');
    }

    /**
     * Save the Patient in the DB
     * @param JSON Object $patient
     * @return object PatientId/false
     */
    private function savePatientData($patient)
    {
        // Save Patient data
        $newpatient = new Patient();

        if (isset($patient->gender)) {
            $gender = substr($patient->gender, 0, 1);
            if (!empty($gender)) {
                $newpatient->gender = $gender;
            }
        }

        if (isset($patient->firstName)) {
            $newpatient->first_name = $patient->firstName;
        }

        if (isset($patient->lastName)) {
            $newpatient->last_name = $patient->lastName;
        }

        if (isset($patient->ethnicity)) {
            $newpatient->ethnicity = '';
            $first = true;
            foreach ($patient->ethnicity as $value) {
                if ($first) {
                    $newpatient->ethnicity = $value;
                    $first = false;
                } else {
                    $newpatient->ethnicity = $newpatient->ethnicity.', '.$value;
                }
            }
        }

        if (isset($patient->race)) {
            $newpatient->race = '';
            $first = true;
            foreach ($patient->race as $value) {
                if ($first) {
                    $newpatient->race = $value;
                    $first = false;
                } else {
                    $newpatient->race = $newpatient->race.', '.$value;
                }
            }
        }

        if (isset($patient->dateOfBirth)) {
            $newpatient->date_of_birth = $patient->dateOfBirth;
        }

        $newpatient->date_added = date("Y-m-d H:i:s");
        $newpatient->enabled = '0';

        if (isset($patient->homeEmail)) {
            $newpatient->email = $patient->homeEmail;
        } else {
            if (isset($patient->workEmail)) {
                $newpatient->email = $patient->workEmail;
            }
        }

        // email address is empty or invalid
        if (empty($newpatient->email) || !filter_var($newpatient->email, FILTER_VALIDATE_EMAIL)) {
            // generate username from first and last name
            $patientUsername = preg_replace(
                '/[^A-Za-z0-9\-]/',
                '_',
                $newpatient->first_name . ' ' . $newpatient->last_name
            );
            // append cell phone number to it
            $patientMobilePhone = '';
            if (!empty($patient->cellPhone)) {
                $patientMobilePhone = preg_replace('/[^A-Za-z0-9\-() ]/', '_', $patient->cellPhone);
            }

            // create fake email address @corp.doctor
            $patientEmail = $patientUsername . '+' . StringComponent::normalizePhoneNumber($patientMobilePhone, true) .
                '@corp.doctor.com';
            // use it
            $newpatient->email = $patientEmail;
        } else {
            // the email is valid
            $patientEmail = $newpatient->email;
        }

        // Set the Address (home address)
        $newpatient->address = '';
        if (isset($patient->homeAddress->line1)) {
            $newpatient->address = $patient->homeAddress->line1;
        }

        if (isset($patient->homeAddress->postal)) {
            $location = Location::model()->find(
                "zipcode = :zipcode",
                array(":zipcode" => $patient->homeAddress->postal)
            );
            if ($location) {
                // only set zipcode if found in the location table
                $newpatient->zipcode = $patient->homeAddress->postal;
            }
        }

        if (isset($patient->homeAddress->city)) {
            $newpatient->address = $newpatient->address.', '.$patient->homeAddress->city;
        }

        if (isset($patient->homeAddress->state)) {
            $newpatient->address = $newpatient->address.', '.$patient->homeAddress->state;
        }

        if (isset($patient->homeAddress->postal)) {
            $newpatient->address = $newpatient->address.', '.$patient->homeAddress->postal;
        }

        if (isset($patient->homeAddress->country)) {
            $newpatient->address = $newpatient->address.', '.$patient->homeAddress->country;
        }

        // Set the Address_2 (work address)
        $newpatient->address_2 = '';
        if (isset($patient->workAddress->line1)) {
            $newpatient->address_2 = $patient->workAddress->line1;
        }

        if (isset($patient->workAddress->city)) {
            $newpatient->address_2 = $newpatient->address_2.', '.$patient->workAddress->city;
        }

        if (isset($patient->workAddress->state)) {
            $newpatient->address_2 = $newpatient->address_2.', '.$patient->work_address->state;
        }

        if (isset($patient->workAddress->postal)) {
            $newpatient->address_2 = $newpatient->address_2.', '.$patient->work_address->postal;
        }

        if (isset($patient->workAddress->country)) {
            $newpatient->address_2 = $newpatient->address_2.', '.$patient->work_address->country;
        }

        if (isset($patient->homePhone)) {
            $newpatient->night_phone = $patient->homePhone;
        }

        if (isset($patient->workPhone)) {
            $newpatient->day_phone = $patient->workPhone;
        }

        if (isset($patient->cellPhone)) {
            $newpatient->mobile_phone = $patient->cellPhone;
        }

        $newpatient->date_signed_in = $this->getCurrentNewYorkDateTime();
        $newpatient->localization_id = '1'; // ask

        // Password cannot be null in Patient Tables
        $newpatient->assignRandomPassword();

        if ($newpatient->validate()) {
            // try to create the patient
            if ($newpatient->save()) {
                // return its id
                return $newpatient->id;
            }
        } elseif (!empty($patientEmail)) {
            // we could not save the patient, maybe he or she already existed - look up by email addres we may have
            // generated before or the one received in the json string
            $oldpatient = Patient::model()->find('email=:email', array(':email' => $patientEmail));
            if ($oldpatient) {
                // found, return id
                return $oldpatient->id;
            }
        }

        // could not create or find existing patient
        return false;
    }

    /**
     * Save the EhrPatient Data in the DB
     * @param int $installationId
     * @param JSON Object $patient
     * @param string $newPatientId
     * @return object PatientId/false
     */
    private function saveEHRPatientData($installationId, $patient, $newPatientId)
    {
        $cpnEhrInstallation = CpnEhrInstallation::model()->cache(Yii::app()->params['cache_long'])->find(
            'cpn_installation_id = :cpn_installation_id ORDER BY id ASC',
            array(':cpn_installation_id' => $installationId)
        );

        // Save EHRPatient data
        $cpnEhrPatient = new EhrPatient();
        $cpnEhrPatient->ehr_patient_id = $patient->identifierInternal;
        $cpnEhrPatient->cpn_ehr_installation_id = $cpnEhrInstallation->id;
        $cpnEhrPatient->patient_id = $newPatientId;
        $cpnEhrPatient->date_added = date("Y-m-d H:i:s");
        $cpnEhrPatient->added_by_account_id = '1';

        if ($cpnEhrPatient->validate()) {
            $cpnEhrPatient->save();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Updates the Patient Data
     * @param Patient $newpatient
     * @param JSON Object $patient
     * @return bool result
     */
    private function updatePatientData($newpatient, $patient)
    {
        if (isset($patient->gender)) {
            $newpatient->gender = $patient->gender;
        }

        if (isset($patient->firstName)) {
            $newpatient->first_name = $patient->firstName;
        }

        if (isset($patient->lastName)) {
            $newpatient->last_name = $patient->lastName;
        }

        if (isset($patient->ethnicity)) {
            $newpatient->ethnicity = '';
            $first = true;
            foreach ($patient->ethnicity as $value) {
                if ($first) {
                    $newpatient->ethnicity = $value;
                    $first = false;
                } else {
                    $newpatient->ethnicity = $newpatient->ethnicity.', '.$value;
                }
            }
        }

        if (isset($patient->race)) {
            $newpatient->race = '';
            $first = true;
            foreach ($patient->race as $value) {
                if ($first) {
                    $newpatient->race = $value;
                    $first = false;
                } else {
                    $newpatient->race = $newpatient->race.', '.$value;
                }
            }
        }

        if (isset($patient->dateOfBirth)) {
            $newpatient->date_of_birth = $patient->dateOfBirth;
        }

        $newpatient->date_added = date("Y-m-d H:i:s");
        $newpatient->enabled = '0';

        if (isset($patient->homeEmail)) {
            $newpatient->email = $patient->homeEmail;
        } else {
            if (isset($patient->workEmail)) {
                $newpatient->email = $patient->workEmail;
            } else {
                // Email cannot be null in Patient Table
                $newpatient->email = ' ';
            }
        }

        // Set the Address (home address)
        $newpatient->address = '';

        if (isset($patient->homeAddress->line1)) {
            $newpatient->address = $patient->homeAddress->line1;
        }

        if (isset($patient->homeAddress->postal)) {
            $newpatient->zipcode = $patient->homeAddress->postal;
        }

        if (isset($patient->homeAddress->city)) {
            $newpatient->address = $newpatient->address.', '.$patient->homeAddress->city;
        }

        if (isset($patient->homeAddress->state)) {
            $newpatient->address = $newpatient->address.', '.$patient->homeAddress->state;
        }

        if (isset($patient->homeAddress->postal)) {
            $newpatient->address = $newpatient->address.', '.$patient->homeAddress->postal;
        }

        if (isset($patient->homeAddress->country)) {
            $newpatient->address = $newpatient->address.', '.$patient->homeAddress->country;
        }

        // Set the Address_2 (work address)
        $newpatient->address_2 = '';

        if (isset($patient->workAddress->line1)) {
            $newpatient->address_2 = $patient->workAddress->line1;
        }

        if (isset($patient->workAddress->city)) {
            $newpatient->address_2 = $newpatient->address_2.', '.$patient->workAddress->city;
        }

        if (isset($patient->workAddress->state)) {
            $newpatient->address_2 = $newpatient->address_2.', '.$patient->work_address->state;
        }

        if (isset($patient->workAddress->postal)) {
            $newpatient->address_2 = $newpatient->address_2.', '.$patient->work_address->postal;
        }

        if (isset($patient->workAddress->country)) {
            $newpatient->address_2 = $newpatient->address_2.', '.$patient->work_address->country;
        }

        if (isset($patient->homePhone)) {
            $newpatient->night_phone = $patient->homePhone;
        }

        if (isset($patient->workPhone)) {
            $newpatient->day_phone = $patient->workPhone;
        }

        if (isset($patient->cellPhone)) {
            $newpatient->mobile_phone = $patient->cellPhone;
        }

        $newpatient->date_signed_in = $this->getCurrentNewYorkDateTime();
        $newpatient->localization_id = '1'; // ask

        // If this is a new record, create a password
        if (empty($newpatient->id)) {
            $newpatient->assignRandomPassword();
        }

        if ($newpatient->validate()) {
            return $newpatient->save();
        }
        return false;
    }
}
