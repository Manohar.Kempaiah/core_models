<?php

Yii::import('application.modules.core_models.models._base.BaseDirectorySetting');

class DirectorySetting extends BaseDirectorySetting
{

    public $account_full_name;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Whether the directory belongs to a churned organization or not.
     * @return bool
     */
    public static function belongsToChurnedOrganization($key)
    {
        $churned = null;

        $json = Yii::app()->cache->get("D" . $key . "_churned");
        if ($json) {
            $data = json_decode($json);
            $churned = (bool) $data->churned;
        } else {
            $directory = DirectorySetting::load($key);
            $churned = (bool) Account::model()->belongsToChurnedOrganization($directory->account_id);

            $data = array(
                "churned" => $churned
            );
            Yii::app()->cache->set("D" . $key . "_churned", json_encode($data), Yii::app()->params['cache_long']);
        }
        return $churned;
    }


    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // delete all related records
        $relatedRecords = ProviderPracticeDirectory::model()->findAll(
            'directory_setting_id = :directory_setting_id',
            [':directory_setting_id' => $this->id]
        );

        foreach ($relatedRecords as $record) {
            $record->delete();
        }

        $layouts = DirectoryPartnerLayout::model()->findAll(
            'directory_setting_id = :directory_setting_id',
            [':directory_setting_id' => $this->id]
        );

        foreach ($layouts as $record) {
            $record->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * beforeValidate
     */
    public function beforeValidate()
    {
        // By default set options as an empty JSON
        if (empty($this->options)) {
            $this->options = '{}';
        }
        return parent::beforeValidate();
    }

    /**
     * Get the providers associated with a widget
     * @param bool $demoMode Whether we're in preview mode
     * @return array The list of associated providers
     */
    public function getProviders()
    {
        $sql = sprintf(
            "SELECT DISTINCT p.*
            FROM directory_setting ds
            INNER JOIN provider_practice_directory ppd
                ON ppd.directory_setting_id = ds.id
            INNER JOIN `provider` p
                ON ppd.provider_id = p.id
            WHERE ds.key_code = '%s';",
            $this->key_code
        );

        return DirectorySetting::model()->findAllBySql($sql);
    }


    /**
     * Get the practices associated with a widget
     * @param int providerId Optional provider id
     * @return array The list of associated practices
     */
    public function getPractices($providerId = null)
    {
        $sql = sprintf(
            "SELECT DISTINCT p.*
            FROM directory_setting AS ds
            INNER JOIN provider_practice_directory AS ppd
                ON ppd.directory_setting_id = ds.id
            INNER JOIN practice p
                ON ppd.practice_id = p.id
            WHERE ds.key_code = '%s' %s;",
            $this->key_code,
            isset($providerId) ? "AND provider_id = " . $providerId : ""
        );
        return DirectorySetting::model()->findAllBySql($sql);
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM directory_setting
            WHERE id = %d;",
            $recordId,
            $accountId
        );
    }

    public function getOrigin($partnerSiteId)
    {
        try {
            $setting = DirectorySetting::model()->find([
                'condition' => 'partner_site_id=:psi',
                'params' => ['psi' => $partnerSiteId]
            ]);

            if (empty($setting)) {
                return null;
            }

            $enabled = $setting->cors_enabled;
            $domain = $setting->cors_domain;
            if (!$enabled || empty($domain)) {
                return "*";
            }

            $search = ["http://", "https://"];
            $host = str_replace($search, "", $domain);
            $parts = explode('/', $host);
            $host = $parts[0];
            $pattern = '|' . $host . '$|';

            if (isset($_SERVER['HTTP_ORIGIN']) && preg_match($pattern, $_SERVER['HTTP_ORIGIN'])) {
                return $_SERVER['HTTP_ORIGIN'];
            } else {
                throw new CHttpException(500, '500 - General Error');
            }
        } catch (\Exception $e) {
            throw new CHttpException(500, '500 - General Error');
        }
    }

    /**
     * Read the config setting based on the environment
     */
    public function findBySiteIdAndEnv($partnerSiteId)
    {
        $env = strtolower(Yii::app()->params['environment']);
        $settings =  DirectorySetting::model()->findAll("partner_site_id=:psi", array('psi' => $partnerSiteId));
        if (empty($settings)) {
            return null;
        }

        $genericSetting = $findedSetting = null;
        foreach ($settings as $setting) {
            if (empty($setting->environment)) {
                $genericSetting = $setting;
            } else {
                $environments = explode(',', $setting->environment);
                if (in_array($env, $environments)) {
                    $findedSetting = $setting;
                    break;
                }
            }
        }

        return (!empty($findedSetting) ? $findedSetting : $genericSetting);
    }

    /**
     * Return a directory setting by their name and version.
     *
     * @param string $name
     * @param int $version
     * @return array
     */
    public function getDirectorySettingByName($name, $version)
    {
        // try to find a directory setting by their name
        return  DirectorySetting::model()->find(
            "name = :name AND version = :version AND FIND_IN_SET(:environment, environment)",
            [
                ':name' => $name,
                ':version' => $version,
                ':environment' => strtolower(Yii::app()->params['environment'])
            ]
        );
    }

    /**
     * Get all layouts of a desired directory setting.
     *
     * @param int $id
     * @return array
     */
    public static function getLayouts($id)
    {
        $sql = '
            SELECT directory_partner_layout.directory_component_template_id,
                directory_setting.config_front,
                directory_component_section.name AS section_name,
                directory_partner_layout.layout_components,
                directory_partner_layout.style_variables,
                directory_partner_layout.texts_and_placeholders
            FROM directory_setting
                JOIN directory_partner_layout
                ON directory_partner_layout.directory_setting_id = directory_setting.id
                JOIN directory_component_section
                ON directory_component_section.id = directory_partner_layout.component_section_id
            WHERE directory_setting.id = :id
            ORDER BY directory_partner_layout.`order`';

        return Yii::app()->db->createCommand($sql)->queryAll(true, [':id' => $id]);
    }

    /**
     * Clone directory settings and all their partner layout data.
     * Return true if success.
     *
     * @param mixed $from partner_site_id or name
     * @param string $name new name
     * @param int $partnerSiteId new partner_site_id (optional)
     * @param string $fromEnvironment to select which environment must be cloned
     * @param string $toEnvironment to select where must be cloned
     * @throws Exception in case of any error
     * @return boolean
     */
    public static function cloneDirectory(
        $from,
        $name,
        $partnerSiteId = null,
        $fromEnvironment = 'all',
        $toEnvironment = 'all'
    )
    {
        // filtering criteria
        $criteria = new CDbCriteria();

        // filter by from_environment was specified
        if ($fromEnvironment != 'all') {
            $criteria->addCondition('FIND_IN_SET(:environment, environment)');
            $criteria->params[':environment'] = $fromEnvironment;
        }

        $criteria->addCondition('partner_site_id = :from OR name = :from');
        $criteria->params[':from'] = $from;

        // try to find the source data
        $source = DirectorySetting::model()->findAll($criteria);

        // throw an error if not found
        if (empty($source)) {
            throw new Exception(
                'Directoy setting "' . $from . '" not found, please set a right name or partner site id.'
            );
        }

        // check if partner site exists to avoid FK issues
        if ($partnerSiteId) {
            $exists = PartnerSite::model()->exists('id = :partner_site_id', [':partner_site_id' => $partnerSiteId]);

            if (!$exists) {
                throw new Exception(
                    'Partner site "' . $partnerSiteId .
                        '" not found, please set a valid partner site for the new directory.'
                );
            }
        }

        // check if the directory already exists in the environment, do this in all sources before start cloning
        foreach ($source as $s) {
            if ($toEnvironment == 'all') {
                // special case for all environments, must test each environment in the list
                foreach (explode(',', $s->environment) as $env) {
                    $exists = static::directoryExists($partnerSiteId, $name, trim($env));

                    if ($exists) {
                        throw new Exception(
                            'A directory already exists in the environment specified, can\'t continue.'
                        );
                    }
                }
            } else {
                // test only the dest environment
                $exists = static::directoryExists($partnerSiteId, $name, $toEnvironment);

                if ($exists) {
                    throw new Exception('A directory already exists in the environment specified, can\'t continue.');
                }
            }
        }

        // loop by each directory setting found and clone them
        foreach ($source as $s) {
            // get the id of the directory setting to duplicate
            $id = $s->id;

            // tell the model that is a new record to duplicate the directory setting
            $s->isNewRecord = true;

            // set the id to null to avoid duplicate key issues
            $s->id = null;

            // set the other attributes and save it
            $s->partner_site_id = $partnerSiteId;
            $s->name = $name;
            $s->key_code = $name;
            $s->cors_enabled = 0;
            $s->cors_domain = null;
            // set the environment if was specified
            if ($toEnvironment != 'all') {
                $s->environment = $toEnvironment;
            }
            $success = $s->save();

            // could not continue
            if (!$success) {
                throw new Exception(print_r($s->getErrors(), true));
            }

            // get all layouts
            $layouts = DirectoryPartnerLayout::model()->findAll('directory_setting_id = :id', [':id' => $id]);

            // loop by each layout
            foreach ($layouts as $l) {
                // set the layout as new record and save it
                $l->isNewRecord = true;
                $l->id = null;
                $l->directory_setting_id = $s->id;
                $success = $l->save();

                // in case of error, stop here
                if (!$success) {
                    throw new Exception(print_r($s->getErrors(), true));
                }
            }
        }

        // done
        return true;
    }

    /**
     * Return true if a directory already exists with the specified parameters.
     * @param int $partnerSiteId
     * @param string $name
     * @param string $environment
     */
    protected static function directoryExists($partnerSiteId, $name, $environment)
    {
        // check duplicated partner site id
        if ($partnerSiteId) {
            $exists = DirectorySetting::model()->exists(
                'partner_site_id = :partner_site_id AND FIND_IN_SET(:environment, environment)',
                [
                    ':partner_site_id' => $partnerSiteId,
                    ':environment' => $environment
                ]
            );

            if ($exists) {
                return true;
            }
        }

        // check duplicated name
        return DirectorySetting::model()->exists(
            'name = :name AND FIND_IN_SET(:environment, environment)',
            [
                ':name' => $name,
                ':environment' => $environment
            ]
        );
    }

    /**
     * Returns the settings by the provided data.
     * This method accepts the partner_site_id or the partner name, if both are provided, the partner_site_id will take
     * preference.
     *
     * @param int $partnerSiteId
     * @param string $partner
     * @param string $environment
     * @param int $version
     * @return DirectorySetting
     */
    public static function getPartnerConfig($partnerSiteId, $partner, $environment, $version = null)
    {
        // create a criteria to filter data dinamically
        $criteria = new CDbCriteria();

        // limit to enabled directories
        $criteria->addCondition('enabled = 1');

        // add version filter
        if ($version) {
            $criteria->addCondition('version = :version');
            $criteria->params[':version'] = $version;
        } else {
            // order by version to get only the major version
            $criteria->order = 'version DESC';
        }

        // filter by environment
        $criteria->addCondition('FIND_IN_SET(:environment, environment)');
        $criteria->params[':environment'] = $environment;

        // filter by partner site id if was provided
        if ($partnerSiteId) {
            $criteria->addCondition('partner_site_id = :partner_site_id');
            $criteria->params[':partner_site_id'] = $partnerSiteId;
        }

        // filter by partner name if was provided
        if ($partner) {
            $criteria->addCondition('name = :name');
            $criteria->params[':name'] = $partner;
        }

        // get settings
        return DirectorySetting::model()->find($criteria);
    }

    /**
     * Return all directory settings that matches with specified parameters.
     *
     * @param string $environment
     * @param int $version
     * @param int $enabled
     * @return array
     */
    public static function getDirectorySetting($environment, $version = null, $enabled = null)
    {
        // create a criteria to filter data dinamically
        $criteria = new CDbCriteria();

        // limit to enabled directories
        if ($enabled !== null) {
            $criteria->addCondition('enabled = :enabled');
            $criteria->params[':enabled'] = $enabled ? 1 : 0;
        }

        // add version filter
        if (strlen($version) != 0) {
            $criteria->addCondition('version = :version');
            $criteria->params[':version'] = $version;
        } else {
            // if no version is specified, skip all directories with version = 0, useful to filter unwanted directories
            $criteria->addCondition('version <> "0"');
            // order by version to get only the major version
            $criteria->order = 'version DESC';
        }

        // filter by environment
        $criteria->addCondition('FIND_IN_SET(:environment, environment)');
        $criteria->params[':environment'] = $environment;

        return DirectorySetting::model()->findAll($criteria);
    }

    /**
     * Generate the frontend JSON of the specified directory.
     *
     * @param DirectorySetting $setting
     * @return array
     */
    public static function generateJson(DirectorySetting $setting)
    {
        // get config front data
        $json = json_decode($setting->config_front);

        // couldn't continue
        if (empty($json)) {
            return [];
        }

        // add empty values
        $json->layoutComponents = [];
        $json->styleVariables = [];
        $json->textsAndPlaceholders = [];

        // add fixed values from settings
        static::addFixedValues($json, $setting);

        // get all partner site layouts
        $layouts = static::getLayouts($setting->id);

        // exit here if the isn't any layout
        if (empty($layouts)) {
            return $json;
        }

        // add all json style columns
        $json = static::addLayouts($json, $layouts);

        return $json;
    }

    /**
     * Generate the backend JSON of the specified directory.
     *
     * @param DirectorySetting $setting
     * @return array
     */
    public static function generateBackendJson(DirectorySetting $setting)
    {
        // get config front data
        $json = json_decode($setting->config_back);

        // couldn't continue
        if (empty($json)) {
            return [];
        }

        return $json;
    }

    /**
     * Add fixed values to a generated JSON.
     *
     * @param array $json
     * @param DirectorySetting $setting
     * @return array
     */
    protected static function addFixedValues($json, DirectorySetting $setting)
    {
        // add fixed root values
        $json->directorySettingId = $setting->id;
        $json->version = $setting->version;
        $json->name = $setting->name;
        $json->editable = (bool) $setting->editable;

        // add properties
        if (!isset($json->properties)) {
            $json->properties = new stdClass();
        }

        $json->properties->partnerSiteId = $setting->partner_site_id;

        // add tracking
        if (!isset($json->properties->tracking)) {
            $json->properties->tracking = new stdClass();
        }

        if (empty($json->properties->tracking->trackingName)) {
            $json->properties->tracking->trackingName = $setting->name;
        }

        // done
        return $json;
    }

    /**
     * Add layouts to a generated JSON.
     *
     * @param array $json
     * @param array $layouts
     * @return array
     */
    protected static function addLayouts($json, $layouts)
    {
        // loop by each layout and add it to the json in a different required format
        foreach ($layouts as $l) {
            // add the layout components data
            $layoutComponents = json_decode($l['layout_components']);
            $templateId = json_decode($l['directory_component_template_id']);
            if ($layoutComponents) {
                $layoutComponents->templateId = $templateId;
                $json->layoutComponents[] = $layoutComponents;
            }

            // add the style data
            if ($l['style_variables']) {
                $styleVariables = json_decode($l['style_variables']);

                if ($styleVariables) {
                    foreach ($styleVariables as $sv) {
                        $json->styleVariables[strtolower($l['section_name'])][] = $sv;
                    }
                }
            }

            // add the text and placeholders data
            if ($l['texts_and_placeholders']) {
                $textAndPlaceholders = json_decode($l['texts_and_placeholders']);

                if ($textAndPlaceholders) {
                    foreach ($textAndPlaceholders as $tp) {
                        $json->textsAndPlaceholders[strtolower($l['section_name'])][] = $tp;
                    }
                }
            }
        }

        return $json;
    }
}
