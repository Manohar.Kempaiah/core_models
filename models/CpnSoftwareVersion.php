<?php

Yii::import('application.modules.core_models.models._base.BaseCpnSoftwareVersion');

class CpnSoftwareVersion extends BaseCpnSoftwareVersion
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get Software Version
     *
     * @param int $major
     * @param int $minor
     * @param int $build
     * @param str $softwareIdentifier
     * @param str $secret
     * @param str $installationIdentifier
     * @return array
     */
    public function getSoftwareVersion($major, $minor, $build, $softwareIdentifier, $secret, $installationIdentifier)
    {
        $csv = null;

        // Query to cpn_software_installation
        $sql = "SELECT si.id
            FROM cpn_software_installation si
            INNER JOIN cpn_software s on si.cpn_software_id = s.id
            INNER JOIN cpn_installation i on si.cpn_installation_id = i.id
            WHERE i.installation_identifier = :installation_identifier
            AND i.secret = :secret
            AND s.guid = :guid;";

        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":installation_identifier", $installationIdentifier, PDO::PARAM_STR);
        $command->bindParam(":secret", $secret, PDO::PARAM_STR);
        $command->bindParam(":guid", $softwareIdentifier, PDO::PARAM_STR);
        $csiId = $command->queryScalar();

        if ($csiId) {

            $date = new DateTime();
            $releaseDate = $date->format('Y-m-d H:i:s');

            // Query to cpn_software_version
            $sql = sprintf(
                "SELECT
                v.guid AS identifier,
                v.major,
                v.minor,
                v.build,
                v.checksum,
                v.guid as filename,
                v.cpn_software_id
                FROM cpn_software_version v
                    INNER JOIN cpn_software s on v.cpn_software_id = s.id
                WHERE s.enabled_flag = 1
                    AND v.enabled_flag = 1
                    AND s.del_flag = 0
                    AND s.guid = '%s'
                    AND (v.cpn_software_installation_id = '0' OR v.cpn_software_installation_id is null
                            OR v.cpn_software_installation_id = '%s')
                    AND v.del_flag = 0
                    AND v.date_to_release <= '%s'
                    AND (
                        (v.major > %s) OR (v.major = %s AND v.minor > %s) OR
                        (v.major = %s AND v.minor = %s AND v.build > %s) OR
                        (v.major = 100 AND v.minor = 100 AND v.build= 100)
                    )
                    ORDER BY v.id DESC;",
                $softwareIdentifier,
                $csiId,
                $releaseDate,
                $major,
                $major,
                $minor,
                $major,
                $minor,
                $build
            );

            // Return latest result
            $csv = Yii::app()->db->createCommand($sql)->queryRow();
        }

        return $csv;
    }
}
