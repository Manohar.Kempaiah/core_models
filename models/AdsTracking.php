<?php

Yii::import('application.modules.core_models.models._base.BaseAdsTracking');

class AdsTracking extends BaseAdsTracking
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('partner_id, provider_id, provider_specialty, visit_reason', 'required'),
            array('partner_id, waiting_room_invite_id, provider_id', 'numerical', 'integerOnly'=>true),
            array('provider_specialty', 'length', 'max'=>250),
            array('visit_reason, confirm', 'length', 'max'=>50),
            array('waiting_room_invite_id, confirm', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, partner_id, waiting_room_invite_id, provider_id, provider_specialty, visit_reason, date_added, confirm', 'safe', 'on'=>'search'),
        );
    }
}
