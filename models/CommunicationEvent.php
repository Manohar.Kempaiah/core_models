<?php

Yii::import('application.modules.core_models.models._base.BaseCommunicationEvent');

class CommunicationEvent extends BaseCommunicationEvent
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
