<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeSpecialty');

class ProviderPracticeSpecialty extends BaseProviderPracticeSpecialty
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Clean phone format
     * @return boolean
     */
    public function beforeSave()
    {

        $this->phone_number = preg_replace('/[^0-9]/', '', $this->phone_number);

        return parent::beforeSave();
    }

}
