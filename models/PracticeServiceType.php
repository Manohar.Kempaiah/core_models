<?php

Yii::import('application.modules.core_models.models._base.BasePracticeServiceType');

class PracticeServiceType extends BasePracticeServiceType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Clean phone number
     * @return bool
     */
    public function beforeSave()
    {

        $this->phone_number = preg_replace('/[^0-9]/', '', $this->phone_number);

        return parent::beforeSave();
    }

}
