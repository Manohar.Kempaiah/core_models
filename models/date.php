<?php

namespace Ddc\CoreModels;

final class date
{
    public function displayCurrentDateTime()
    {
        // Get the current date and time using the DateTime class
        $currentDateTime = new \DateTime();
        
        // Format the date and time as a string
        $formattedDateTime = $currentDateTime->format('Y-m-d H:i:s');
        
        echo 'Current Date and Time: ' . $formattedDateTime;
    }
}
