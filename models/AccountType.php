<?php

Yii::import('application.modules.core_models.models._base.BaseAccountType');

class AccountType extends BaseAccountType
{

    // NOTE: these types MUST match the ids in the database
    // ADMIN-LEVEL TYPES

    const ADMINISTRATOR = 1;
    const ACCOUNT_MANAGER = 11;
    const RESEARCHER = 12;
    const MODERATOR = 13;
    // NORMAL USER TYPES
    const PATIENT_LEAD_PROGRAM = 2;
    const MARKETING_AND_REPUTATION_MONITORING = 3;
    const CUSTOM_BUILD_WEBSITE_FOR_PRACTICE = 4;
    const INTEGRATED_PRACTICE_MANAGEMENT = 5;
    const AUTOMATED_PATIENT_MESSAGING = 6;
    const FREE_ACCOUNT = 7;
    const FEATURE_AND_SYNDICATE = 9;
    const PATIENT = 10;
    const CHANNEL_CLIENT = 14;
    const CHANNEL_PARTNER = 15;
    // id 8 was deleted (if we delete more in the future within
    // range of LOWEST_DEFINED_ID and HIGHEST_DEFINED_ID, we
    // should make this a list OR just make a list for all valid entries
    // and check those in the functions below
    const DELETED_ID = 8;
    // must match lowest/highest defined IDs in database, respectively,
    // for use in functions below
    const LOWEST_DEFINED_ID = 1;
    const HIGHEST_DEFINED_ID = 15;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        return new CActiveDataProvider(
            get_class($this),
            array('criteria' => $criteria, 'pagination' => array('pageSize' => 100))
        );
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from account_type table if associated records exist in account table
        $has_associated_records = Account::model()->exists(
            'account_type_id=:account_type_id',
            array(':account_type_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError(
                "id",
                "This account type can't be deleted because there are accounts with that account type."
            );
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * returns true if account type is any type of internal administrator type
     *
     * @param $id Account type id
     * @return bool
     */
    public static function isAdminLevel($id)
    {
        return ($id == self::ADMINISTRATOR ||
                $id == self::ACCOUNT_MANAGER ||
                $id == self::RESEARCHER ||
                $id == self::MODERATOR);
    }

    /**
     * returns true if account type is any type of provider
     *
     * @param $id Account type id
     * @return bool
     */
    public static function isProvider($id)
    {
        if (in_array($id, self::getProviderAccountTypes())) {
            return true;
        }

        return false;
    }

    /**
     * Return array with Provider account type ids
     * @return array
     */
    public static function getProviderAccountTypes()
    {
        return [
            self::PATIENT_LEAD_PROGRAM,
            self::MARKETING_AND_REPUTATION_MONITORING,
            self::CUSTOM_BUILD_WEBSITE_FOR_PRACTICE,
            self::INTEGRATED_PRACTICE_MANAGEMENT,
            self::AUTOMATED_PATIENT_MESSAGING,
            self::FREE_ACCOUNT,
            self::FEATURE_AND_SYNDICATE,
            self::ACCOUNT_MANAGER,
            self::CHANNEL_PARTNER
        ];
    }

    /**
     * Check if the account is a channel partner admin
     *
     * @param $id
     * @return bool
     */
    public static function isChannelPartner($id)
    {
        if (self::CHANNEL_PARTNER == $id) {
            return true;
        }

        return false;
    }

    /**
     * returns true if account type is one defined in our database
     *
     * @param $id Account type id
     * @return bool
     */
    public static function isValid($id)
    {
        return ($id >= self::LOWEST_DEFINED_ID &&
                $id <= self::HIGHEST_DEFINED_ID &&
                $id != self::DELETED_ID);
    }

    /**
     * expression to use in controller access rules when checking if user has admin-level privileges
     *
     * @param $strId Id variable to use in expression
     * @param bool $isAdmin Expression checks if variable is admin true or false based on $isAdmin
     * @return string
     */
    public static function getAccessRuleExpressionIsAdminLevel($strId, $isAdmin = true)
    {

        if ($isAdmin) {
            return '(isset(' . $strId . ') && (' .
                    $strId . ' == ' . self::ADMINISTRATOR . ' || ' .
                    $strId . ' == ' . self::ACCOUNT_MANAGER . ' || ' .
                    $strId . ' == ' . self::RESEARCHER . ' || ' .
                    $strId . ' == ' . self::MODERATOR . '))';
        } else {
            return '(' .
                    $strId . ' != ' . self::ADMINISTRATOR . ' && ' .
                    $strId . ' != ' . self::ACCOUNT_MANAGER . ' && ' .
                    $strId . ' != ' . self::RESEARCHER . ' && ' .
                    $strId . ' != ' . self::MODERATOR . ')';
        }
    }

    /**
     * expression to use in controller access rules when checking if user is a valid value
     *
     * @param $strId Id variable to use in expression
     * @param bool $isValid Expression checks if variable is valid
     * @return string
     */
    public static function getAccessRuleExpressionIsValid($strId, $isValid = true)
    {

        if ($isValid) {
            return '(isset(' . $strId . ') && ' .
                    $strId . ' >= ' . self::LOWEST_DEFINED_ID . ' && ' .
                    $strId . ' <= ' . self::HIGHEST_DEFINED_ID . ' && ' .
                    $strId . ' != ' . self::DELETED_ID . ')';
        } else {
            return '(!isset(' . $strId . ') || ' .
                    $strId . ' < ' . self::LOWEST_DEFINED_ID . ' || ' .
                    $strId . ' > ' . self::HIGHEST_DEFINED_ID . ' || ' .
                    $strId . ' == ' . self::DELETED_ID . ')';
        }
    }

    /**
     * a string to be used in SQL statements to check if a user is an admin
     *
     * @param $strId Id variable to use in expression
     * @param bool $shouldBeAdmin If true, must be admin
     * @param bool $mustBeValid If true, must be valid
     * @return string
     */
    public static function getSqlIsAdmin($strId, $shouldBeAdmin = true, $mustBeValid = false)
    {
        if ($shouldBeAdmin) {
            return '(' . $strId . ' = ' . self::ADMINISTRATOR . ' OR '
                    . $strId . ' = ' . self::ACCOUNT_MANAGER . ' OR '
                    . $strId . ' = ' . self::RESEARCHER . ' OR '
                    . $strId . ' = ' . self::MODERATOR . ')';
        } else {
            if ($mustBeValid) {
                return '(' . $strId . ' >= ' . self::LOWEST_DEFINED_ID . ' AND ' .
                        $strId . ' != ' . self::ADMINISTRATOR . ' AND ' .
                        $strId . ' != ' . self::ACCOUNT_MANAGER . ' AND ' .
                        $strId . ' != ' . self::RESEARCHER . ' AND ' .
                        $strId . ' != ' . self::MODERATOR . ' AND ' .
                        $strId . ' != ' . self::DELETED_ID . ' AND ' .
                        $strId . ' <= ' . self::HIGHEST_DEFINED_ID . ')';
            } else {
                return '(' . $strId . ' != ' . self::ADMINISTRATOR . ' AND ' .
                        $strId . ' != ' . self::ACCOUNT_MANAGER . ' AND ' .
                        $strId . ' != ' . self::RESEARCHER . ' AND ' .
                        $strId . ' != ' . self::MODERATOR . ')';
            }
        }
    }

    /**
     * Get the selectable account types.
     * @return array
     */
    public function selectableAccountTypes()
    {
        return self::model()->findAll("id != :id", array(":id" => AccountType::PATIENT));
    }

}
