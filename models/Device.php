<?php

Yii::import('application.modules.core_models.models._base.BaseDevice');

class Device extends BaseDevice
{

    public $account_device_status;
    public $deviceModelName;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'nickname';
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'status' => Yii::t('app', 'Status'),
            'device_model_id' => null,
            'nickname' => Yii::t('app', 'Nickname'),
            'serial_number' => Yii::t('app', 'Serial Number'),
            'date_added' => Yii::t('app', 'Date Added'),
            'notes' => Yii::t('app', 'Notes'),
            'accountDevices' => null,
            'deviceModel' => null,
            'deviceHistories' => null,
        );
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('device_model_id, nickname, serial_number', 'required'),
            array('status', 'length', 'max' => 1),
            array('device_model_id', 'length', 'max' => 11),
            array('nickname', 'length', 'max' => 100),
            array('serial_number', 'length', 'max' => 50),
            array('date_added, notes', 'safe'),
            array('status, date_added, notes', 'default', 'setOnEmpty' => true, 'value' => null),
            array(
                'id, status, device_model_id, nickname, serial_number, date_added, notes, account_device_status',
                'safe', 'on' => 'search'
            ),
        );
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from device if associated records exist in account_device table
        $has_associated_records = AccountDevice::model()->exists(
            'device_id=:device_id',
            array(':device_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError('id', 'Device cannot be because there are accounts linked to it.');
            return false;
        }

        // remove historic records before deleting from device table
        $arrDeviceHistory = DeviceHistory::model()->findAll('device_id=:device_id', array(':device_id' => $this->id));
        if (!empty($arrDeviceHistory)) {
            foreach ($arrDeviceHistory as $history) {
                if (!$history->delete()) {
                    $this->addError("id", "Could not delete device's history");
                    return false;
                }
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Prevent duplicate nicknames
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->isNewRecord) {
            $this->date_added = date("Y-m-d");

            // check if nickname exists
            $nicknameExists = Device::model()->exists('nickname=:nickname', array(':nickname' => $this->nickname));
            if ($nicknameExists) {
                $this->addError('nickname', 'This nickname already exists');
                return false;
            }
        } else {
            // check if nickname exists
            $nicknameExists = Device::model()->exists(
                'nickname=:nickname AND id!=:id',
                array(':nickname' => $this->nickname, ':id' => $this->id)
            );
            if ($nicknameExists) {
                $this->addError('nickname', 'This nickname already exists for another device');
                return false;
            }
        }

        return parent::beforeSave();
    }

    /**
     * Create a history record if necessary when a device is created or has its status changed
     * @return mixed
     */
    public function afterSave()
    {

        if ($this->isNewRecord) {
            // if creating, just create a history record
            DeviceHistory::log($this->id, null, $this->status, 'M');
        } else {
            // if updating, query the latest status from history
            $sql = sprintf(
                "SELECT new_status
                FROM device_history
                WHERE device_id = %d
                ORDER BY id DESC
                LIMIT 1;",
                $this->id
            );
            $prevStatus = Yii::app()->db->createCommand($sql)->queryScalar();
            if ($prevStatus != $this->status) {
                // create a history record only if the status changed
                DeviceHistory::log($this->id, $prevStatus, $this->status, 'M');
            }
        }

        return parent::afterSave();
    }

    /**
     * Main search function, overrides the one in BaseDevice.php
     * @return \CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->join = " LEFT JOIN
            (SELECT IF(MAX(active_ac_id) IS NOT NULL, MAX(active_ac_id), MAX(inactive_ac_id)) AS last_ac_id,
            device_id
            FROM (
                SELECT IF(status = 'A', id, NULL) AS active_ac_id, IF(status <> 'A', id, NULL) AS inactive_ac_id,
                device_id
                FROM account_device
            ) AS aa
            GROUP BY device_id) AS ad
            ON t.id = ad.device_id
            LEFT JOIN account_device
            ON account_device.id = ad.last_ac_id
            LEFT JOIN device_model
            ON t.device_model_id = device_model.id";

        $criteria->select = 't.id, t.status, t.nickname, device_model.name AS deviceModelName, t.serial_number, '
            . 't.date_added, account_device.status AS account_device_status';

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.status', $this->status, true);
        $criteria->compare('nickname', $this->nickname, true);
        $criteria->compare('device_model_id', $this->device_model_id);
        $criteria->compare('serial_number', $this->serial_number, true);
        if ($this->account_device_status != '') {
            $criteria->compare('account_device.status', $this->account_device_status, true);
        }

        $sort = new CSort();
        $sort->defaultOrder = 'id desc';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100),
            'sort' => $sort,
        ));
    }

    /**
     * Get status full name
     * @return string
     */
    public function getStatusName()
    {
        switch ($this->status) {
            case 'A':
                return 'Available';
            case 'B':
                return 'Broken or permanently stranded';
            case 'D':
                return 'Demo';
            case 'R':
                return 'Repairing';
            case 'S':
                return 'Stranded';
            case 'U':
                return 'Unavailable';
            default:
                return 'Unknown';
        }
    }

    /**
     * Given a device id, find Active linked accounts (if any), otherwise all linked
     * @return string
     */
    public function getAccountDeviceStatus()
    {

        // first look for an active account
        $sql = sprintf(
            "SELECT IF(status = 'A', CONCAT(first_name, ' ', last_name), CONCAT(first_name, ' ', last_name, ' (',
            CASE `status`
                WHEN 'P' THEN 'Pre-delivery'
                WHEN 'T' THEN 'In transit to client'
                WHEN 'B' THEN 'Return requested'
                WHEN 'S' THEN 'Suspended'
                WHEN 'D' THEN 'Stranded'
                WHEN 'C' THEN 'In transit from client'
                WHEN 'R' THEN 'Returned'
                ELSE 'Unknown'
            END
            , ')'))
            FROM account
            INNER JOIN account_device
            ON account.id = account_device.account_id
            LEFT JOIN account_device_history
            ON account_device.id = account_device_history.account_device_id
            WHERE device_id = %d
            ORDER BY account_device.status='A' DESC, account_device_history.id DESC
            LIMIT 1;",
            $this->id
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Change device status
     * @param int $deviceId
     * @param string $newStatus
     * @return bool
     */
    public static function changeStatus($deviceId = 0, $newStatus = '')
    {
        if ($deviceId > 0 && !empty($newStatus)) {
            $device = Device::model()->findByPk($deviceId);
            if ($device) {
                $device->status = $newStatus;
                if ($device->save()) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Create new digital device
     *
     * @return object
     */
    public static function createNewDigitalDevice()
    {
        // get the las created device
        $lastDevice = Device::model()->findBySql("SELECT MAX(id) as id FROM device LIMIT 1;");
        $newDeviceId = $lastDevice->id + 1;

        $device = new Device();
        $device->status = 'A';
        $device->device_model_id = 7; // Digital ReviewRequest
        $device->nickname = 'RR-' . $newDeviceId;
        $device->serial_number = 'DOCTOR' . time();
        $device->date_added = date('Y-m-d');
        $device->save();
        return $device;
    }

    /**
     * Get ReviewHub URL
     * @param array $key array object (I.E: uid, partner_site_id)
     *
     * @return string
     */
    public function getReviewHubUrl($key = null)
    {
        if ($this->device_model_id == DeviceModel::REVIEW_REQUEST_ID) {
            // It is a digital device, it doesn't need the kiosk_mode parameter
            $ddcLink = 'https://' . Yii::app()->params->servers['dr_fee_hn']
                . '?sid=' . $this->getSID();
        } else {
            $ddcLink = 'https://' . Yii::app()->params->servers['dr_kio_hn']
                . '?sid=' . $this->getSID() . '&kiosk_mode=1';
        }
        $args = '';
        if (!empty($key)) {
            $args = '&key=' . base64_encode(json_encode($key));
        } else {
            if ($this->device_model_id == DeviceModel::REVIEW_REQUEST_ID) {
                $key = ['uid' => StringComponent::generateGUID()];
                $args = '&key=' . base64_encode(json_encode($key));
            }
        }

        return $ddcLink . $args;
    }

    /**
     * Generate a secure hash of a device ID
     * @param int $deviceId
     * @return string
     */
    public static function getSidByDeviceId($deviceId = '')
    {
        return hash('sha512', 'Doctor.com ReviewHub-' . Yii::app()->params['rh_secret'] . '-' . $deviceId);
    }

    /**
     * Generate a secure hash of a device ID
     *
     * @return string
     */
    public function getSID()
    {
        return hash('sha512', 'Doctor.com ReviewHub-' . Yii::app()->params['rh_secret'] . '-' . $this->id);
    }
}
