<?php

Yii::import('application.modules.core_models.models._base.BasePracticeDetails');

class PracticeDetails extends BasePracticeDetails
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = array('email_prefix', 'match', 'pattern' => '/^[a-zA-Z0-9\-\_]+$/', 'message' =>
        'Only characters a-z, numbers, dashes and underscores are allowed');
        $rules[] = array('email_prefix', 'unique', 'message' => "Email address already in use");

        return $rules;
    }

    /**
     * Validate coverage_location_id and coverage_state_id, since they're FKs that can be null
     */
    public function beforeSave()
    {

        if (((int) $this->coverage_location_id > 0) === false) {
            $this->coverage_location_id = null;
        }
        if (((int) $this->coverage_state_id > 0) === false) {
            $this->coverage_state_id = null;
        }
        if ($this->isNewRecord) {
            // default is 100
            $this->review_request_auto_daily_limit = 100;
        }

        if (!empty($this->review_request_auto_daily_limit) && !is_numeric($this->review_request_auto_daily_limit)) {
            $this->addError('review_request_auto_daily_limit', 'Only numbers are allowed');
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Update EHR integration status in Salesforce after saving
     * @return bool
     */
    public function afterSave()
    {

        if (!empty($this->practice_id)) {
            $organizations = OrganizationAccount::getPracticeOwners($this->practice_id);
            if (!empty($organizations)) {
                foreach ($organizations as $organization) {
                    SqsComponent::sendMessage('updateSFEHR', $organization['organization_id']);
                }
            }
        }

        // update scheduling if necessary
        if ($this->isNewRecord
            ||
            (!empty($this->oldRecord) && $this->oldRecord->practice_type_id != $this->practice_type_id)
            ||
            (!empty($this->oldRecord) && $this->oldRecord->practice_id != $this->practice_id)
        ) {
            if (!empty($this->practice_id)) {
                // async update
                SqsComponent::sendMessage('updateBooking', json_encode(['practice_id' => $this->practice_id]));
            }
            if (!empty($this->oldRecord->practice_id) && $this->oldRecord->practice_id != $this->practice_id) {
                // async update the previous practice_id
                SqsComponent::sendMessage(
                    'updateBooking',
                    json_encode(['practice_id' => $this->oldRecord->practice_id])
                );
            }
        }

        return parent::afterSave();
    }

    /**
     * Update EHR integration status in Salesforce after deleting
     * @return boolean
     */
    public function afterDelete()
    {

        if (!empty($this->practice_id)) {
            $organizations = OrganizationAccount::getPracticeOwners($this->practice_id);
            if (!empty($organizations)) {
                foreach ($organizations as $organization) {
                    SqsComponent::sendMessage('updateSFEHR', $organization['organization_id']);
                }
            }
        }

        // update scheduling if necessary
        if (!empty($this->practice_id)) {
            // async update
            SqsComponent::sendMessage('updateBooking', json_encode(['practice_id' => $this->practice_id]));
        }

        return parent::afterDelete();
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id .
            '" rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice . '">' .
            $this->practice . '</a>');
    }

    /**
     * Get a practice's size
     * @return string
     */
    public function getPracticeSize()
    {

        switch ($this->practice_size) {
            case 1:
                return 'Single Provider Practice';
            case 2:
                return 'Group Practice (2-5 Providers)';
            case 3:
                return 'Group Practice (6-10 Providers)';
            case 4:
                return 'Group Practice (11-20 Providers)';
            case 5:
                return 'Group Practice (21-50 Providers)';
            case 6:
                return 'Group Practice (51+ Providers)';
            case 7:
                return 'Hospital';
            default:
                return 'Unknown';
        }
    }

    /**
     * Set PracticeSize automatically counting providers from specified practice.
     * @param int $practiceId
     * @return boolean
     */
    public static function setPracticeSize($practiceId)
    {
        // determine the practice size
        $practiceDetails = PracticeDetails::model()->find(
            'practice_id = :practice_id',
            [':practice_id' => $practiceId]
        );

        if ($practiceDetails == null) {
            return false;
        }

        // if practice_size is Hospital, we can't update the field
        if ($practiceDetails->practice_size != 7) {
            // definition of practice limits, see practices_details.practice_size for definition
            $arrThreshold = [
                6 => 50, // 51+
                5 => 20, // 21~50
                4 => 10, // 11~20
                3 => 5, // 6~10
                2 => 1, // 2~5
                1 => 0, // 1
                8 => -1, // 0
            ];

            $count = ProviderPractice::model()->count('practice_id = :practice_id', [':practice_id' => $practiceId]);

            // set the correct practice size, 6: > 50, 5: 20 > n < 51...
            foreach ($arrThreshold as $practiceSize => $gate) {
                if ($count > $gate) {
                    $practiceDetails->practice_size = $practiceSize;
                    $practiceDetails->save();
                    break;
                }
            }
        }

        return true;
    }

    /**
     * Get the covered location's full name
     * @return boolean
     */
    public function getFullLocation()
    {
        $location = Location::model()->findByPk($this->coverage_location_id);
        if ($location) {
            return $location->fullLocation;
        }
        return false;
    }

    /**
     * Get Messaging Settings
     * @param int $practiceId
     * @param int $ownerAccountId
     * @param int $providerId
     * @return mixed, array or object
     */
    public static function getMessagingSetting($practiceId = 0, $ownerAccountId = 0, $providerId = 0)
    {
        if (empty($practiceId)) {
            return false;
        }

        $tmpResults = PracticeDetails::getReviewRequestCustomizedData($practiceId, $providerId);

        if ($tmpResults['success']) {
            $pd = (array) $tmpResults['data'][0]->attributes;
            $practiceLogo = $tmpResults['data'][1];
            $reviewLinks = $tmpResults['data'][2];
            $practiceRating = $tmpResults['data'][3];

            $data['practice_details'] = $pd;
            $data['practice_logo'] = $practiceLogo;
            $data['review_links'] = $reviewLinks;
            $data['average_rating'] = $practiceRating;

            // Get all devices related to the account-practice. Sending $ownerAccountId to be able to get them
            $data['practice_devices'] = AccountDevice::getAccountDevices($ownerAccountId, $practiceId, true);
            // Check if the device has providers associated, otherwise remove devices from the list
            if (!empty($data['practice_devices'])) {
                foreach ($data['practice_devices'] as $key => $device) {
                    if (empty($device['providers'])) {
                        unset($data['practice_devices'][$key]);
                    }
                }
            }

            // Do we have to filter for this provider only??
            if ($providerId > 0) {
                // Yes
                foreach ($data['practice_devices'] as $k => $v) {
                    if ($v['device_model_id'] == DeviceModel::REVIEW_REQUEST_ID) {
                        // This provider is in this RR?
                        $exists = AccountDeviceProvider::model()->find(
                            "account_device_id=:account_device_id AND provider_id=:provider_id",
                            [":account_device_id" => $v['id'], ":provider_id" => $providerId]
                        );
                        if (empty($exists->id)) {
                            // Nop
                            unset($data['practice_devices'][$k]);
                        }
                    }
                }
            }
            return $data;
        }
        return false;
    }

    /**
     * Get Full Details
     * @param int $practiceId
     * @return mixed, array or object
     */
    public static function getDetails($practiceId = 0, $returnArray = false)
    {
        if ($practiceId == 0) {
            return array();
        }
        $arrPracticeDetails = PracticeDetails::model()->find(
            'practice_id=:practice_id',
            array(':practice_id' => $practiceId)
        );

        $practiceDetails = '';
        if (!empty($arrPracticeDetails)) {
            if ($returnArray) {
                $practiceDetails = (array) $arrPracticeDetails->attributes;
            } else {
                $practiceDetails = $arrPracticeDetails;
            }
        }
        return $practiceDetails;
    }

    /**
     * Update EHR configuration in Salesforce
     * EHR__c
     * EHR_Integrated__c
     *
     * @param int $organizationId
     * @return mixed
     */
    public static function updateSFEHR($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);
        if (!$organization || empty($organization->salesforce_id)) {
            return false;
        }

        // get this organization's integrated ehrs
        $sql = sprintf(
            "SELECT GROUP_CONCAT(DISTINCT(emr_type.name) SEPARATOR ', ')
            FROM emr_type
            INNER JOIN account_emr
            ON account_emr.emr_type_id = emr_type.id
            INNER JOIN organization_account
            ON account_emr.account_id = organization_account.account_id
            WHERE organization_account.organization_id = %d;",
            $organizationId
        );
        $ehrIntegrated = Yii::app()->db->createCommand($sql)->queryScalar();

        // get every practice's ehr
        $ehr = Emr::getOrganizationEhrs($organizationId);

        $data = array(
            'EHR__c' => substr($ehr ? $ehr : '', 0, 255),
            'EHR_Integrated__c' => substr($ehrIntegrated ? $ehrIntegrated : '', 0, 50)
        );

        return Yii::app()->salesforce->updateAccount($organization->salesforce_id, $data);
    }

    /**
     * Save PracticeDetails Data
     * @param array $postData
     * @param int $accountId
     * @param int $practiceId
     * @return array
     */
    public static function saveData($postData = '', $accountId = 0, $practiceId = 0)
    {
        if (empty($postData) || $accountId == 0 || $practiceId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }

        $pdId = !empty($postData['id']) ? $postData['id'] : 0;
        // check if practiceDetails exists
        if ($pdId == 0) {
            $practiceDetails = new PracticeDetails();
            $practiceDetails->practice_id = $practiceId;
        } else {
            $practiceDetails = PracticeDetails::model()->find(
                'id=:id AND practice_id=:practice_id',
                array(':id' => $pdId, ':practice_id' => $practiceId)
            );
        }
        $practiceDetails->attributes = $postData;

        if ($practiceDetails->save()) {
            $results['success'] = true;
            $results['error'] = "";
            $results['data'] = (array) $practiceDetails->attributes;
        } else {
            $results['success'] = false;
            $results['error'] = "ERROR_SAVING_PRACTICE_DETAILS";
            $results['data'] = $practiceDetails->getErrors();
        }
        return $results;
    }

    /**
     * Save PracticeDetails Data
     * customizing email subject and body and sms body of review requests sent to patients
     * @param array $postData
     * @param int $practiceId
     * @return array
     */
    public static function updateReviewRequestManualCustomize($postData = array(), $practiceId = 0)
    {
        if (empty($postData) || $practiceId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }

        // Get the practice_details
        $pd = PracticeDetails::model()->find('practice_id=:practiceId', array(':practiceId' => $practiceId));


        // DDC review custom fields
        $defaultEmailSubject = trim(StringComponent::globalMessenger('PADM', 'reviewRequestEmailSubject'));
        $defaultEmailTitle = trim(StringComponent::globalMessenger('PADM', 'reviewRequestEmailTitle'));
        $defaultEmailBody = trim(StringComponent::globalMessenger('PADM', 'reviewRequestEmailBody'));
        $defaultSmsBody = trim(StringComponent::globalMessenger('PADM', 'reviewRequestSmsBody'));

        if (isset($postData['review_request_email_subject'])) {
            $rrEmailSubject = Common::get($postData, 'review_request_email_subject', $defaultEmailSubject);
            if (trim($rrEmailSubject) == trim($defaultEmailSubject)) {
                $rrEmailSubject = null;
            }
            $pd->review_request_email_subject = $rrEmailSubject;
        }
        if (isset($postData['review_request_email_title'])) {
            $rrEmailTitle = Common::get($postData, 'review_request_email_title', $defaultEmailTitle);
            if (trim($rrEmailTitle) == trim($defaultEmailTitle)) {
                $rrEmailTitle = null;
            }
            $pd->review_request_email_title = $rrEmailTitle;
        }
        if (isset($postData['review_request_email_body'])) {
            $rrEmailBody = Common::get($postData, 'review_request_email_body', $defaultEmailBody);
            if (trim($rrEmailBody) == trim($defaultEmailBody)) {
                $rrEmailBody = null;
            }
            $pd->review_request_email_body = $rrEmailBody;
        }
        if (isset($postData['review_request_sms_body'])) {
            $rrSmsBody = Common::get($postData, 'review_request_sms_body', $defaultSmsBody);
            if (trim($rrSmsBody) == trim($defaultSmsBody)) {
                $rrSmsBody = null;
            }
            $pd->review_request_sms_body = $rrSmsBody;
        }

        if (isset($postData['review_request_service_text'])) {
            $pd->review_request_service_text = Common::get($postData, 'review_request_service_text', null);
        }
        if (isset($postData['review_request_service_link'])) {
            $pd->review_request_service_link = Common::get($postData, 'review_request_service_link', null);
        }

        // Google review custom fields
        $defaultGrEmailTitle = trim(StringComponent::globalMessenger('PADM', 'googleReviewtEmailTitle'));
        $defaultGrEmailBody = trim(StringComponent::globalMessenger('PADM', 'googleReviewEmailBody'));
        $defaultGrSmsBody = trim(StringComponent::globalMessenger('PADM', 'googleReviewSmsBody'));

        if (isset($postData['google_review_email_title'])) {
            $grEmailTitle = Common::get($postData, 'google_review_email_title', $defaultGrEmailTitle);
            if (trim($grEmailTitle) == trim($defaultGrEmailTitle)) {
                $grEmailTitle = null;
            }
            $pd->google_review_email_title = $grEmailTitle;
        }
        if (isset($postData['google_review_email_body'])) {
            $grEmailBody = Common::get($postData, 'google_review_email_body', $defaultGrEmailBody);
            if (trim($grEmailBody) == trim($defaultGrEmailBody)) {
                $grEmailBody = null;
            }
            $pd->google_review_email_body = $grEmailBody;
        }
        if (isset($postData['google_review_sms_body'])) {
            $grSmsBody = Common::get($postData, 'google_review_sms_body', $defaultGrSmsBody);
            if (trim($grSmsBody) == trim($defaultGrSmsBody)) {
                $grSmsBody = null;
            }
        }
        // Yelp review custom fields
        $defaultYrEmailTitle = StringComponent::globalMessenger('PADM', 'yelpReviewtEmailTitle');
        $defaultYrEmailBody = StringComponent::globalMessenger('PADM', 'yelpReviewEmailBody');
        $defaultYrSmsBody = StringComponent::globalMessenger('PADM', 'yelpReviewSmsBody');

        $yrEmailTitle = null;
        if (isset($postData['yelp_review_email_title'])) {
            $yrEmailTitle = $postData['yelp_review_email_title'];
        }
        $yrEmailBody = null;
        if (isset($postData['yelp_review_email_body'])) {
            $yrEmailBody = $postData['yelp_review_email_body'];
        }
        $yrSmsBody = null;
        if (isset($postData['yelp_review_sms_body'])) {
            $yrSmsBody = $postData['yelp_review_sms_body'];
        }

        if (trim($yrEmailTitle) == trim($defaultYrEmailTitle)) {
            $yrEmailTitle = null;
        }
        if (trim($yrEmailBody) == trim($defaultYrEmailBody)) {
            $yrEmailBody = null;
        }
        if (trim($yrSmsBody) == trim($defaultYrSmsBody)) {
            $yrSmsBody = null;
        }

        // Yelp review custom fields
        $defaultYrEmailTitle = StringComponent::globalMessenger('PADM', 'yelpReviewtEmailTitle');
        $defaultYrEmailBody = StringComponent::globalMessenger('PADM', 'yelpReviewEmailBody');
        $defaultYrSmsBody = StringComponent::globalMessenger('PADM', 'yelpReviewSmsBody');

        $yrEmailTitle = null;
        if (isset($postData['yelp_review_email_title'])) {
            $yrEmailTitle = $postData['yelp_review_email_title'];
        }
        $yrEmailBody = null;
        if (isset($postData['yelp_review_email_body'])) {
            $yrEmailBody = $postData['yelp_review_email_body'];
        }
        $yrSmsBody = null;
        if (isset($postData['yelp_review_sms_body'])) {
            $yrSmsBody = $postData['yelp_review_sms_body'];
        }

        if (trim($yrEmailTitle) == trim($defaultYrEmailTitle)) {
            $yrEmailTitle = null;
        }
        if (trim($yrEmailBody) == trim($defaultYrEmailBody)) {
            $yrEmailBody = null;
        }
        if (trim($yrSmsBody) == trim($defaultYrSmsBody)) {
            $yrSmsBody = null;
        }

        $pd->review_request_email_subject = $rrEmailSubject;
        $pd->review_request_email_title = $rrEmailTitle;
        $pd->review_request_email_body = $rrEmailBody;
        $pd->review_request_sms_body = $rrSmsBody;
        $pd->google_review_email_title = $grEmailTitle;
        $pd->google_review_email_body = $grEmailBody;
        $pd->google_review_sms_body = $grSmsBody;
        $pd->yelp_review_email_title = $yrEmailTitle;
        $pd->yelp_review_email_body = $yrEmailBody;
        $pd->yelp_review_sms_body = $yrSmsBody;

        if ($pd->save()) {
            $results['success'] = true;
            $results['error'] = "";
        } else {
            $results['success'] = false;
            $results['error'] = "ERROR_SAVING_PRACTICE_DETAILS";
            $results['data'] = $pd->getErrors();
        }
        return $results;
    }

    /**
     * Get Review Request customized data
     * @param int $practiceId
     * @param int $providerId
     * @return array
     */
    public static function getReviewRequestCustomizedData($practiceId = 0, $providerId = 0)
    {
        if ($practiceId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }

        // get practice logo
        $practice = Practice::model()->findByPk($practiceId);
        $practiceLogo = '';
        if (!empty($practice->logo_path)) {
            $practiceLogo = AssetsComponent::generateImgSrc(
                $practice->logo_path,
                'practices',
                'M',
                true,
                false,
                false
            );
        }

        if (empty($providerId)) {
            // get social links
            $arrReviewLinks = ProviderPracticeListing::getAllReviewLinks($practiceId);
        } else {
            // get social links
            $listingTypes = array(
                ListingType::GOOGLE_PROFILE_ID,
                ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID,
                ListingType::YELP_PROFILE_ID,
                ListingType::YELP_PROVIDER_ID
            );

            $arrReviewLinks = ProviderPracticeListing::getReviewsLink($providerId, $practiceId, $listingTypes);

            if (empty($arrReviewLinks['yelp'])) {
                $arrReviewLinks['yelp'] = '';
                $arrReviewLinks['yelp_long_url'] = '';
                $arrReviewLinks['yelp_id'] = '';
                $arrReviewLinks['yelp_source'] = '';
                $arrReviewLinks['yelp_custom_link'] = '';
                $arrReviewLinks['yelp_provider_id'] = '';
                $arrReviewLinks['yelp_provider_name'] = '';
                $arrReviewLinks['yelp_listing_type_id'] = '';
            }
            if (empty($arrReviewLinks['google+'])) {
                $arrReviewLinks['google+'] = '';
                $arrReviewLinks['google+_long_url'] = '';
                $arrReviewLinks['google+_id'] = '';
                $arrReviewLinks['google+_source'] = '';
                $arrReviewLinks['google+_custom_link'] = '';
                $arrReviewLinks['google+_provider_id'] = '';
                $arrReviewLinks['google+_provider_name'] = '';
                $arrReviewLinks['google+_listing_type_id'] = '';
            }
            if (empty($arrReviewLinks['google_pp'])) {
                $arrReviewLinks['google_pp'] = '';
                $arrReviewLinks['google_pp_long_url'] = '';
                $arrReviewLinks['google_id_pp'] = '';
                $arrReviewLinks['google_pp_source'] = '';
                $arrReviewLinks['google_pp_custom_link'] = '';
                $arrReviewLinks['google_pp_provider_id'] = '';
                $arrReviewLinks['google_pp_provider_name'] = '';
                $arrReviewLinks['google_pp_listing_type_id'] = '';
            }
        }

        // get practice details
        $pd = PracticeDetails::model()->find('practice_id=:practiceId', array(':practiceId' => $practiceId));

        // generate them if necessary
        if (!$pd) {
            $pd = new PracticeDetails;
            $pd->practice_id = $practiceId;
            $pd->reviewhub_sharing = 'BOTH';
            $pd->save();
        }
        if ($pd->review_request_auto_daily_limit == 99) {
            $pd->review_request_auto_daily_limit = 100;
        }

        // Get default text for email and sms
        if (empty($pd->review_request_email_subject)) {
            $pd->review_request_email_subject = StringComponent::globalMessenger('PADM', 'reviewRequestEmailSubject');
        }
        if (empty($pd->review_request_email_title)) {
            $pd->review_request_email_title = StringComponent::globalMessenger('PADM', 'reviewRequestEmailTitle');
        }
        if (empty($pd->review_request_email_body)) {
            $pd->review_request_email_body = StringComponent::globalMessenger('PADM', 'reviewRequestEmailBody');
        }
        if (empty($pd->review_request_sms_body)) {
            $pd->review_request_sms_body = StringComponent::globalMessenger('PADM', 'reviewRequestSmsBody');
        }

        if (empty($pd->google_review_email_title)) {
            $pd->google_review_email_title = StringComponent::globalMessenger('PADM', 'googleReviewEmailTitle');
        }
        if (empty($pd->google_review_email_body)) {
            $pd->google_review_email_body = StringComponent::globalMessenger('PADM', 'googleReviewEmailBody');
        }
        if (empty($pd->google_review_sms_body)) {
            $pd->google_review_sms_body = StringComponent::globalMessenger('PADM', 'googleReviewSmsBody');
        }

        if (empty($pd->yelp_review_email_title)) {
            $pd->yelp_review_email_title = StringComponent::globalMessenger('PADM', 'yelpReviewEmailTitle');
        }
        if (empty($pd->yelp_review_email_body)) {
            $pd->yelp_review_email_body = StringComponent::globalMessenger('PADM', 'yelpReviewEmailBody');
        }
        if (empty($pd->yelp_review_sms_body)) {
            $pd->yelp_review_sms_body = StringComponent::globalMessenger('PADM', 'yelpReviewSmsBody');
        }

        $pd->review_request_auto_body = StringComponent::globalMessenger('PADM', 'reviewRequestAutoBody');
        $pd->review_request_auto_subject = StringComponent::globalMessenger('PADM', 'reviewRequestAutoSubject');

        $practiceRating = ProviderRating::getPracticeRatingAverage($practiceId);

        $results['success'] = true;
        $results['error'] = "";
        $results['data'] = array($pd, $practiceLogo, $arrReviewLinks, $practiceRating);
        return $results;
    }
}
