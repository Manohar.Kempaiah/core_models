<?php

Yii::import('application.modules.core_models.models._base.BasePartner');

class Partner extends BasePartner
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get the partners from the database as html list data
     *
     * @return array("id" => "name", [...]);
     */
    public static function getHtmlList()
    {
        return CHtml::listData(
            self::model()->findAll(
                array(
                    'order' => 'display_name'
                )
            ),
            'id',
            'display_name'
        );
    }

    /**
     * Get a specific partner given a partner_site_id
     * @param int $partnerSiteId
     * @return string
     */
    public static function getPartnerNameByPartnerSiteId($partnerSiteId)
    {

        if (isset($partnerSiteId) && $partnerSiteId > 0) {
            $sql = sprintf(
                'SELECT partner.*
                FROM partner, partner_site
                WHERE partner.id = partner_site.partner_id
                AND partner_site.id = %d;',
                $partnerSiteId
            );
            $partner = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryRow();

            if (isset($partner) && isset($partner['display_name'])) {
                return $partner['display_name'];
            }
        }

        return '';
    }

}
