<?php

Yii::import('application.modules.core_models.models._base.BaseDirectoryBaseComponentType');

class DirectoryBaseComponentType extends BaseDirectoryBaseComponentType
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
