<?php

Yii::import('application.modules.core_models.models._base.BasePdiModel');

class PdiModel extends BasePdiModel
{

    /**
     * Return model instance
     * @param string $className
     * @return PdiModel
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check if a modelName belongs to a PDI model
     * @param type $modelName
     * @return type
     */
    public static function isPdiModel($modelName)
    {
        // find only the models that are being synced through SQS
        return PdiModel::model()->cache(Yii::app()->params['cache_long'])->
                findAll("model = :model AND sync_sqs = 1", array(':model' => $modelName));
    }
}
