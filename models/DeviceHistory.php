<?php

Yii::import('application.modules.core_models.models._base.BaseDeviceHistory');

class DeviceHistory extends BaseDeviceHistory
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Create an entry in device_history when a device changes
     * @param int $device_id
     * @param string $prev_status
     * @param string $new_status
     * @param string $change_type
     * @return mixed
     */
    public static function log($device_id, $prev_status, $new_status, $change_type)
    {

        $accountDeviceHistory = new DeviceHistory();
        $accountDeviceHistory->device_id = $device_id;
        $accountDeviceHistory->prev_status = $prev_status;
        $accountDeviceHistory->new_status = $new_status;
        $accountDeviceHistory->change_type = $change_type;
        if (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
            $accountDeviceHistory->account_id = Yii::app()->user->id;
        }
        $accountDeviceHistory->date_added = date('Y-m-d H:i:s');
        return $accountDeviceHistory->save();
    }

}
