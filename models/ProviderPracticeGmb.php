<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeGmb');

class ProviderPracticeGmb extends BaseProviderPracticeGmb
{

    // used by gmb queue filter
    public $organization_name;
    public $organization_type;
    public $practice_name;
    public $provider_name;
    public $provider_npi;

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Programmatically generate entries in provider_practice_listing based on provider_practice_gmb
     * @return boolean
     */
    public function afterSave()
    {
        // If this is a new entry
        if ($this->isNewRecord) {

            // Create a location state record
            $locationState = new ProviderPracticeGmbLocationState();
            $locationState->provider_practice_gmb_id = $this->id;
            $locationState->save();

            $this->addRelatedRecord('providerPracticeGmbLocationState', $locationState, false);

            // Check if data points exists, otherwise creates them
            foreach (GmbDataPoint::model()->findAll() as $dataPoint) {
                if (!$this->getDataPoint($dataPoint->id)) {

                    // List of enabled DPs by default
                    $enabledDps = [
                        GmbDataPoint::BOOKING_URL,
                        GmbDataPoint::DESCRIPTION,
                        GmbDataPoint::NAME,
                        GmbDataPoint::COVER,
                        GmbDataPoint::ADDRESS,
                        GmbDataPoint::WEBSITE,
                        GmbDataPoint::PHONE,
                        GmbDataPoint::OFFICE_HOURS,
                        GmbDataPoint::ADDITIONAL_PHOTOS
                    ];

                    if (in_array($dataPoint->id, $enabledDps)) {
                        $this->setDataPoint($dataPoint->id, 1);
                    } else {
                        $this->setDataPoint($dataPoint->id);
                    }
                }
            }
        }

        if ($this->is_claimed_by == 'DDC') {

            $providerId = $this->provider_id;
            $practiceId = $this->practice_id;
            $placeId = $this->place_id;
            $url = 'https://accounts.google.com/ServiceLogin?continue=' .
                'https://search.google.com/local/writereview?placeid=';

            if (!empty($providerId)) {
                $listingTypeId = ListingType::GOOGLE_PROVIDERATPRACTICE_PROFILE_ID;
                // Search by practice<->provider
                $exists = ProviderPracticeListing::model()->find(
                    "practice_id=:practice_id AND provider_id=:provider_id AND listing_type_id=:listing_type_id",
                    array(
                        ":practice_id" => $practiceId,
                        ":provider_id" => $providerId,
                        ":listing_type_id" => $listingTypeId,
                    )
                );
            } else {
                $listingTypeId = ListingType::GOOGLE_PROFILE_ID;
                // Search by practice
                $exists = ProviderPracticeListing::model()->find(
                    "practice_id=:practice_id AND provider_id IS NULL AND listing_type_id=:listing_type_id",
                    array(
                        ":practice_id" => $practiceId,
                        ":listing_type_id" => $listingTypeId,
                    )
                );
            }
            if (!$exists) {

                // Add new provider_practice_listing record
                $ppl = new ProviderPracticeListing();

                $ppl->listing_type_id = $listingTypeId;
                $ppl->practice_id = $practiceId;
                $ppl->provider_id = $providerId;
                $ppl->date_added = date("Y-m-d H:i:s");
                $ppl->date_updated = date("Y-m-d H:i:s");
                $ppl->url = $url . $placeId;
                $ppl->source = "GMB Tool";

                if ($ppl->save()) {
                    // Update provider_practice_listing_id
                    $this->provider_practice_listing_id = $ppl->id;
                    $this->save();
                }
            }
        }

        // Check if we need to create/remove Twilio Tracking Number
        TwilioNumber::checkGmbTrackingNumber($this);

        return parent::afterSave();
    }

    /**
     * Make sure the place Id is unique
     * @return boolean
     */
    public function beforeSave()
    {
        // is this a new record? (we only check this case to avoid breaking existing duplicates)
        if ($this->isNewRecord) {
            $auditLogNotes = "Provider Practice GMB beforeSave";
            $this->createAuditLog('C', $auditLogNotes);

            // yes, then check: does the place Id already exist?
            $exists = ProviderPracticeGmb::model()->exists(
                'place_id = :placeId',
                array(':placeId' => $this->place_id)
            );
            // does it?
            if ($exists) {
                // yes, avoid saving
                $auditLogNotes = "Provider Practice GMB beforeSave - Duplicated place id";
                $this->createAuditLog('F', $auditLogNotes);
                $this->addError('place_id', 'This place Id is already linked to a practice or provider at practice.');
                return false;
            }
        } else {
            $auditLogNotes = "Provider Practice GMB beforeSave - Updating record #" . $this->id;
            $datapointsCheck = $this->createAuditLog('U', $auditLogNotes);
            if ($datapointsCheck == 'datapoint_empty') {
                return false;
            }
        }
        return parent::beforeSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // delete records from gmb_location_stats
        $arrStats = GmbLocationStats::model()->findAll(
            'provider_practice_gmb_id = :providerPracticeGmbId',
            array(':providerPracticeGmbId' => $this->id)
        );
        foreach ($arrStats as $stat) {
            if (!$stat->delete()) {
                $auditLogNotes = "Provider Practice GMB beforeDelete - #" . $this->id;
                $this->createAuditLog('F', $auditLogNotes, false);
                $this->addError('id', 'This relation cannot be deleted because stats are attached to it.');
                return false;
            }
        }

        // Delete all related entries
        foreach ($this->providerPracticeGmbDataPoints as $dp) {
            $dp->delete();
        }
        foreach ($this->providerPracticeGmbVerifications as $vf) {
            $vf->delete();
        }

        if ($this->providerPracticeGmbLocationState) {
            $this->providerPracticeGmbLocationState->delete();
        }

        // Remove Gmb location associations with this entity (DO NOT DELETE IT)
        foreach ($this->gmbLocations as $gl) {
            $gl->provider_practice_gmb_id = null;
            $gl->save();
        }

        $auditLogNotes = "Provider Practice GMB beforeDelete - #" . $this->id;
        $this->createAuditLog('D', $auditLogNotes, false);

        $ppGmbR = ProviderPracticeGmbRemoved::model()->findByPk($this->id);
        if (empty($ppGmbR)) {
            $ppGmbR = new ProviderPracticeGmbRemoved();
        }
        $ppGmbR->setAttributes($this->getAttributes());
        $ppGmbR->save();

        return parent::beforeDelete();
    }

    /**
     * Find pages associated with 'O'-level Account
     * @param int $ownerAccountId
     * @param bool $order
     * @param int $offset
     * @param int $recordsPerPage
     * @param string $searchTerm
     * @param int $displayItems
     * @param int $cacheDuration
     * @return array
     */
    public static function getOwnerPages(
        $ownerAccountId,
        $order = false,
        $offset = 0,
        $recordsPerPage = 50,
        $searchTerm = '',
        $displayItems = 0,
        $cacheDuration = 0
    ) {

        // do we want to order the results?
        if ($order) {
            // yes
            $order = 'ORDER BY practice_name, provider_name';
        } else {
            // no
            $order = 'ORDER BY practice_id';
        }

        // do we have a search term?
        $condition = '';
        if (!empty($searchTerm)) {
            $condition = sprintf(
                " AND (
                    practice.name LIKE '%%%s%%'
                    OR (
                        `provider`.first_name LIKE '%%%s%%'
                        OR `provider`.last_name LIKE '%%%s%%'
                        OR `provider`.first_last LIKE '%%%s%%'
                    )
                    OR place_id = '%s'
                )",
                addslashes($searchTerm),
                addslashes($searchTerm),
                addslashes($searchTerm),
                addslashes($searchTerm),
                addslashes($searchTerm)
            );
        }

        // do we want to show only a specific type of item?
        if (!empty($displayItems)) {
            // yes, which?
            if ($displayItems == '1') {
                // practice level only
                $condition .= 'AND provider_id IS NULL';
            } elseif ($displayItems == '2') {
                // provider-at-practice level only
                $condition .= 'AND provider_id IS NOT NULL';
            }
        }

        // count total records
        $sql = sprintf(
            "SELECT COUNT(1)
            FROM provider_practice_gmb
            LEFT JOIN practice
                ON provider_practice_gmb.practice_id = practice.id
            LEFT JOIN `provider`
                ON provider_practice_gmb.provider_id = `provider`.id
            WHERE account_id = %d
            %s;",
            $ownerAccountId,
            $condition
        );
        $count = Yii::app()->db->createCommand($sql)->queryScalar();

        // limit
        $limit = '';
        if ($offset > 0 || $recordsPerPage > 0) {
            // set limit
            $limit = sprintf("LIMIT %d, %d", $offset, $recordsPerPage);
        }

        $sql = sprintf(
            "SELECT provider_practice_gmb.id AS ppgID, practice_id, provider_id,
            IF(is_read_only = 0, FALSE, TRUE) AS is_read_only,
            IF(sync_tracking_number = 0, FALSE, TRUE) AS sync_tracking_number,
            IF(apply_dr_booking_url = 0, FALSE, TRUE) AS apply_dr_booking_url,
            IF(has_sync_issues = 0, FALSE, TRUE) AS has_sync_issues,
            IF(is_claimed_by = '3RDPARTY', TRUE, FALSE) AS alreadyClaimed,
            escalation, location_name_override, place_id, sync_cover,
            category_1, category_2, category_3,
            practice.name AS practice_name, practice.address, `provider`.first_last AS provider_name,
            link_verified, is_claimed_by, sync_issues_details,
            apply_dr_description, has_localposts_api, date_last_sync, sync_address, sync_website
            FROM provider_practice_gmb
            LEFT JOIN practice
                ON provider_practice_gmb.practice_id = practice.id
            LEFT JOIN `provider`
                ON provider_practice_gmb.provider_id = `provider`.id
            WHERE account_id = %d
            %s
            %s
            %s;",
            $ownerAccountId,
            $condition,
            $order,
            $limit
        );

        // query all pages
        $pages = Yii::app()->db->createCommand($sql)->queryAll();
        // return pages and total count
        return [$pages, $count];
    }

    /**
     * Check if we already have a provider_practice_gmb entry for the specified place Id, and if the p@p link is valid
     * @param string $placeId
     * @param int $practiceId
     * @param int $providerId
     * @return array
     */
    public static function checkInputs($placeId, $practiceId, $providerId = null)
    {
        $error = false;
        $errorMessage = "";

        $existing = ProviderPracticeGmb::model()->find('place_id = :placeId', array(':placeId' => $placeId));

        if ($existing) {
            $error = true;
            $errorMessage .= "This Place ID is already associated. ";
        }

        if (!empty($providerId)) {
            $associated = ProviderPractice::model()->find(
                'practice_id = :practiceId AND provider_id = :providerId',
                array(':practiceId' => $practiceId, ':providerId' => $providerId)
            );

            if (!$associated) {
                $error = true;
                $errorMessage .= "According to our database, that provider doesn't work at that practice. ";
            }
        }

        return array(
            "error" => $error,
            "errorMessage" => $errorMessage,
        );
    }

    /**
     * Find the place id from the provider_practice_gmb table
     *
     * @param integer $practiceId
     * @param integer $providerId
     * @return mixed
     */
    public function findForScan($practiceId, $providerId = null)
    {
        $values = [
            ':practice_id' => $practiceId,
        ];

        $providerCondition = 'AND provider_id IS NULL';
        if ($providerId) {
            $providerCondition = "AND provider_id = :provider_id";
            $values[':provider_id'] = $providerId;
        }

        $sql = "SELECT place_id, location_name_override
                FROM provider_practice_gmb
                WHERE practice_id = :practice_id
                %s
                AND escalation IS NULL
                ORDER BY is_claimed_by = 'DDC' DESC
                LIMIT 1";
        $sql = sprintf($sql, $providerCondition);

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValues($values)
            ->queryRow();
    }

    /**
     * Return array with GMB categories
     * Updated 2021-03-29
     * @return array
     */
    public static function getCategories()
    {
        $sql = 'SELECT gcid as code,name FROM gmb_category WHERE is_active=1';
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Looks for a category by code
     *
     * @param $code
     * @return mixed|string[]|null
     */
    public static function getCategory($code)
    {
        foreach (static::getCategories() as $category) {
            if ($category['code'] == $code) {
                return $category;
            }
        }
        return null;
    }


    /**
     * Query queue of unlinked PPL entries
     * @param array $arrSearch
     * @return void
     */
    public static function getQueue($arrSearch = null)
    {
        // increase memory and time limit to avoid cutting the proces
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 1200);
        set_time_limit(1200);

        // extend the maximum of characters in group_concat
        $sql = 'SET SESSION group_concat_max_len = 1000000;';
        Yii::app()->db->createCommand($sql)->execute();

        // build condition
        $conditionP = ''; // practice query
        $conditionPP = ''; // provider-practice query
        if (!empty($arrSearch['organization_name'])) {

            // get all organization name in a string
            $sql = 'SELECT GROUP_CONCAT(id) FROM organization WHERE organization_name LIKE :organization_name';
            $query = Yii::app()->db->createCommand($sql);
            $query->bindValue(
                ':organization_name',
                "%" . addslashes($arrSearch['organization_name']) . "%",
                PDO::PARAM_STR
            );
            $organizationName = $query->queryScalar();

            $orgCond = '';
            if ($organizationName) {
                $orgCond = sprintf(
                    "AND organization.id IN (%s) ",
                    $organizationName
                );
            }
            $conditionP .= $orgCond;
            $conditionPP .= $orgCond;
        }

        // Begin Organization Type
        $condOrganizationType = "(
                        (organization.type != 'Healthcare Institution' AND organization_feature.id IS NOT NULL)
                        OR
                        (
                            organization.type = 'Healthcare Institution'
                            AND (listings = 'Implementation' OR listings = 'Maintenance')
                        )
                    )";

        $condOrganizationTypeForPractice = "(
                        (organization.type != 'Healthcare Institution')
                        OR
                        (
                            organization.type = 'Healthcare Institution'
                            AND (listings = 'Implementation' OR listings = 'Maintenance')
                        )
                    )";

        if (!empty($arrSearch['organization_type'])) {
            $orgCond = sprintf(
                "AND (
                    organization.type LIKE '%%%s%%'
                ) ",
                addslashes($arrSearch['organization_type'])
            );

            $orgCondForPractice = sprintf(
                "AND (
                    organization.type LIKE '%%%s%%'
                ) ",
                addslashes($arrSearch['organization_type'])
            );
        } else {
            $orgCond = sprintf(
                "AND  %s ",
                $condOrganizationType
            );
            $orgCondForPractice = sprintf(
                "AND  %s ",
                $condOrganizationTypeForPractice
            );
        }
        $conditionP .= $orgCondForPractice;
        $conditionPP .= $orgCond;
        // End Organization Type

        if (!empty($arrSearch['practice_name']) && empty($arrSearch['practice_id'])) {

            // get all practice name in a string
            $sql = 'SELECT GROUP_CONCAT(id) FROM practice WHERE name LIKE :name';
            $query = Yii::app()->db->createCommand($sql);
            $query->bindValue(
                ':name',
                "%" . addslashes($arrSearch['practice_name']) . "%",
                PDO::PARAM_STR
            );
            $practiceName = $query->queryScalar();

            $practCond = '';
            if ($practiceName) {
                $practCond = sprintf(
                    "AND practice.id IN (%s) ",
                    $practiceName
                );
            }
            $conditionP .= $practCond;
            $conditionPP .= $practCond;
        }
        if (!empty($arrSearch['practice_id'])) {
            $practCond = sprintf(
                "AND practice.id = %d ",
                $arrSearch['practice_id']
            );
            $conditionP .= $practCond;
            $conditionPP .= $practCond;
        }
        if (!empty($arrSearch['provider_name']) && empty($arrSearch['provider_id'])) {

            // get all provider name in a string
            $sql = 'SELECT GROUP_CONCAT(id) FROM provider WHERE first_middle_last LIKE :first_middle_last';
            $query = Yii::app()->db->createCommand($sql);
            $query->bindValue(
                ':first_middle_last',
                "%" . addslashes($arrSearch['provider_name']) . "%",
                PDO::PARAM_STR
            );
            $providerName = $query->queryScalar();

            $provCon = '';
            if ($providerName) {
                $provCon = sprintf(
                    "AND provider.id IN (%s) ",
                    $providerName
                );
            }
            $conditionPP .= $provCon;
        }
        if (!empty($arrSearch['provider_id'])) {
            $provCon = sprintf(
                "AND provider.id = %d ",
                $arrSearch['provider_id']
            );
            $conditionPP .= $provCon;
        }
        if (!empty($arrSearch['provider_npi'])) {
            $provCon = sprintf(
                "AND provider.npi_id = %d ",
                $arrSearch['provider_npi']
            );
            $conditionPP .= $provCon;
        }

        $baseQuery = sprintf(
            "FROM
            (
                (
                    SELECT DISTINCT
                    CONCAT(organization.id, '-', provider_practice.id, '-', practice.id) AS id,
                    organization.id AS organization_id,
                    organization.organization_name,
                    organization.type AS organization_type,
                    practice.id AS practice_id,
                    practice.name AS practice_name,
                    address_full AS practice_address,
                    provider.id AS provider_id,
                    provider.npi_id AS provider_npi,
                    provider.first_middle_last AS provider_name,
                    organization_feature.date_added AS date_added,
                    IF(
                        provider_practice.phone_number != '' AND provider_practice.phone_number IS NOT NULL,
                        provider_practice.phone_number,
                        practice.phone_number
                    ) AS phone_number
                    FROM organization
                    INNER JOIN organization_account
                        ON organization_account.organization_id = organization.id
                        AND `connection` = 'O'
                    INNER JOIN account_provider
                        ON account_provider.account_id = organization_account.account_id
                    INNER JOIN provider
                        ON provider.id = account_provider.provider_id
                    INNER JOIN provider_practice
                        ON provider.id = provider_practice.provider_id
                    INNER JOIN practice
                        ON provider_practice.practice_id = practice.id
                    INNER JOIN account_practice
                        ON account_practice.account_id = account_provider.account_id
                    INNER JOIN organization_feature
                        ON organization.id = organization_feature.organization_id
                        AND feature_id = 10
                        AND active = 1
                        AND provider.id = organization_feature.provider_id
                    LEFT JOIN provider_practice_gmb
                        ON provider_practice_gmb.practice_id = practice.id
                        AND provider_practice_gmb.provider_id = provider.id
                    WHERE organization.status != 'C'
                        AND provider_practice_gmb.id IS NULL
                        AND practice.nasp_date IS NOT NULL
                        %s
                    LIMIT 5000
                )

                UNION ALL

                (
                    SELECT DISTINCT
                    CONCAT(organization.id, '-', 0, '-', practice.id) AS id,
                    organization.id AS organization_id,
                    organization.organization_name,
                    organization.type AS organization_type,
                    practice.id AS practice_id,
                    practice.name AS practice_name,
                    address_full AS practice_address,
                    provider_practice_gmb.provider_id AS provider_id,
                    null AS provider_npi,
                    null AS provider_name,
                    organization_feature.date_added AS date_added,
                    practice.phone_number AS phone_number
                    FROM organization
                    INNER JOIN organization_account
                        ON organization_account.organization_id = organization.id
                        AND `connection` = 'O'
                    INNER JOIN account_practice
                        ON account_practice.account_id = organization_account.account_id
                    INNER JOIN practice
                        ON practice.id = account_practice.practice_id
                    LEFT JOIN organization_feature
                        ON organization.id = organization_feature.organization_id
                        AND feature_id = 12
                        AND active = 1
                        AND practice.id = organization_feature.practice_id
                    LEFT JOIN provider_practice_gmb
                        ON provider_practice_gmb.practice_id = practice.id
                        AND provider_practice_gmb.provider_id IS NULL
                    WHERE provider_practice_gmb.id IS NULL
                        AND organization.status != 'C'
                        AND practice.nasp_date IS NOT NULL
                        %s
                    LIMIT 5000
                )
            ) AS unlinked",
            $conditionPP,
            $conditionP
        );

        // count records
        $countSql = sprintf('SELECT COUNT(1) %s', $baseQuery);
        $count = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($countSql)->queryScalar();
        /*
        https://providers.local.doctor.com/gmb/queue?
        ProviderPracticeGmb%5Borganization_name%5D=
        &ProviderPracticeGmb%5Borganization_type%5D=smp
        &ProviderPracticeGmb%5Bpractice_id%5D=
        &ProviderPracticeGmb%5Bpractice_name%5D=
        &ProviderPracticeGmb%5Bprovider_id%5D=
        &ProviderPracticeGmb%5Bprovider_npi%5D=
        &ProviderPracticeGmb%5Bprovider_name%5D=
        &ajax=queue-grid&page=1
         */
        // query them
        $sql = sprintf(
            "SELECT id, organization_id, organization_name,
            organization_type, practice_id, practice_name,
            practice_address, provider_id,  provider_npi,
            provider_name, date_added, phone_number
            %s",
            $baseQuery
        );

        return new CSqlDataProvider(
            $sql,
            array(
                'totalItemCount' => $count,
                'sort' => array(
                    'attributes' => array(
                        'organization_name',
                        'organization_type',
                        'practice_name',
                        'practice_address',
                        'provider_npi',
                        'provider_name',
                        'date_added',
                    ),
                ),
                'pagination' => array(
                    'pageSize' => 100,
                ),
            )
        );
    }

    /**
     * Unclaimed GMB Queue for all organizations
     * @param array $arrSearch
     * @return void
     */
    public static function getUnclaimedQueue($arrSearch = null)
    {

        // build condition
        $conditionPP = ''; // provider-practice query
        if (!empty($arrSearch['organization_name'])) {
            $orgCond = sprintf(
                "AND organization_name LIKE '%%%s%%' ",
                addslashes($arrSearch['organization_name'])
            );
            $conditionPP .= $orgCond;
        }

        // Begin Organization Type
        $condOrganizationType = "(
                        (organization.type != 'Healthcare Institution' AND organization_feature.id IS NOT NULL)
                        OR
                        (
                            organization.type = 'Healthcare Institution'
                            AND (listings = 'Implementation' OR listings = 'Maintenance')
                        )
                    )";

        if (!empty($arrSearch['organization_type'])) {
            $orgCond = sprintf(
                "AND (
                    organization.type LIKE '%%%s%%'
                    AND
                    %s
                ) ",
                addslashes($arrSearch['organization_type']),
                $condOrganizationType
            );
        } else {
            $orgCond = sprintf(
                "AND  %s ",
                $condOrganizationType
            );
        }
        $conditionPP .= $orgCond;
        // End Organization Type

        if (!empty($arrSearch['practice_name'])) {
            $practCond = sprintf(
                "AND pra.name LIKE '%%%s%%' ",
                addslashes($arrSearch['practice_name'])
            );
            $conditionPP .= $practCond;
        }
        if (!empty($arrSearch['practice_id'])) {
            $practCond = sprintf(
                "AND pra.id = %d ",
                $arrSearch['practice_id']
            );
            $conditionPP .= $practCond;
        }
        if (!empty($arrSearch['provider_name'])) {
            $provCon = sprintf(
                "AND pro.first_middle_last LIKE '%%%s%%' ",
                addslashes($arrSearch['provider_name'])
            );
            $conditionPP .= $provCon;
        }
        if (!empty($arrSearch['provider_id'])) {
            $provCon = sprintf(
                "AND pro.id = %d ",
                $arrSearch['provider_id']
            );
            $conditionPP .= $provCon;
        }
        if (!empty($arrSearch['provider_npi'])) {
            $provCon = sprintf(
                "AND pro.npi_id = %d ",
                $arrSearch['provider_npi']
            );
            $conditionPP .= $provCon;
        }

        $sql = sprintf(
            "SELECT DISTINCT
                ppg.id, ppg.account_id, organization.id AS organization_id,
                organization.organization_name, organization.`status`,
                organization.`type` AS organization_type, ppg.place_id,
                ppg.practice_id, pra.`name` AS practice_name, pra.address AS practice_address,
                ppg.provider_id, pro.first_middle_last AS provider_name, pro.npi_id,
                ppg.is_claimed_by, ppg.date_added, ppg.place_id,
                ppg.category_1, ppg.category_2, ppg.category_3, ppg.link_verified
            FROM provider_practice_gmb AS ppg
            INNER JOIN organization_account AS oa
                ON oa.account_id = ppg.account_id AND `connection` = 'O'
            INNER JOIN organization
                ON organization.id = oa.organization_id AND organization.`status` != 'C'
            INNER JOIN organization_feature
                ON organization.id = organization_feature.organization_id
            INNER JOIN practice AS pra
                ON pra.id = ppg.practice_id
            LEFT JOIN provider AS pro
                ON pro.id = ppg.provider_id
            WHERE is_claimed_by != 'DDC'
                %s
            ORDER BY organization_name, practice_name
            LIMIT 100;",
            $conditionPP
        );

        // count records
        $countSql = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
        $count = count($countSql);

        // query them
        return new CSqlDataProvider(
            $sql,
            array(
                'totalItemCount' => $count,
                'sort' => array(
                    'attributes' => array(
                        'organization_name',
                        'organization_type',
                        'is_claimed_by',
                        'practice_name',
                        'practice_address',
                        'provider_npi',
                        'provider_name',
                        'date_added',
                    ),
                ),
                'pagination' => false,
            )
        );
    }

    /**
     * Create an audit log for this model.
     *
     * @param string $type
     * @param string $notes
     * @param boolean $withDetails
     *
     * @return void
     */

    public function createAuditLog($type, $notes, $withDetails = true)
    {
        $updateSection = null;
        if ($withDetails) {
            $diff = Common::modifiedAttributesPretty($this);
            $notes = $notes . "\n" . $diff;
        }

        if ($type == 'U') {
            if (Yii::app()->getRequest()->getIsPostRequest()) {
                $request = Yii::app()->request->getJSON();
                $updateSection = $this->getGMBproviderpracitceFlagInfo($request);
                $updateNoteSection = $this->getGMBproviderpracitceNoteInfo($request);
                $newNote = "Provider Practice GMB beforeSave - Updating record updated for . #" . $this->id;
                $fields = [
                    'skip', 'integrate', 'create', 'claim', 'request_ownership', 'verify', 'merge', 'close',
                    'is_read_only', 'category_1', 'category_2', 'category_3', 'place_id', 'sync_tracking_number'
                ];

                $fieldPoints = [
                    'Additional Photos', 'Booking URL', 'Description', 'Name', 'Cover', 'Address', 'Website', 'Phone',
                    'Office Hours', 'Logo', 'Insurances'
                ];
                $dateUpdateFlag = array('date_updated' => date('Y-m-d  h:i:s'));
                $dataFlag = array('flag' => $updateNoteSection);
                $dataFlagu = $dateUpdateFlag + $dataFlag;

                if (in_array($updateNoteSection, $fields)) {
                    $auditLogNotesNew =  print_r($dataFlagu, true);
                    $auditLogNotesNew = str_replace('Array', '', $auditLogNotesNew);
                }
                $dataPointUpdate = array('datapoint' => $updateNoteSection);
                $dataPointNote = $dateUpdateFlag + $dataPointUpdate;

                if (in_array($updateNoteSection, $fieldPoints)) {
                    $auditLogNotesNew =  print_r($dataPointNote, true);
                    $auditLogNotesNew = str_replace('Array', '', $auditLogNotesNew);
                }

                $dataCheck = implode('', $dataFlag);
                // validate fields for empty
                if (empty($dataCheck)) {
                    return 'datapoint_empty';
                }
                $dataCheck = implode('', $dataPointUpdate);
                // validate fieldPoints for empty
                if (empty($dataCheck)) {
                    return 'datapoint_empty';
                }

                if ($updateSection == 'panel_integrated  place_id edited') {
                    $sectionNames = ['panel integrated', 'place_id edited'];
                    foreach ($sectionNames as $sectionNamed) {
                        AuditLog::create(
                            $type,
                            $sectionNamed,
                            'provider_practice_gmb',
                            $newNote . "\n" . $auditLogNotesNew,
                            false,
                            0,
                            false,
                            $this->id,
                            $this->account_id
                        );
                    }
                } else {
                    AuditLog::create(
                        $type,
                        $updateSection,
                        'provider_practice_gmb',
                        $newNote . "\n" . $auditLogNotesNew,
                        false,
                        0,
                        false,
                        $this->id,
                        $this->account_id
                    );
                }
            }
        } elseif ($type == 'C') {
            $updateSection = 'entity created';
            AuditLog::create(
                $type,
                $updateSection,
                'provider_practice_gmb',
                $notes,
                false,
                0,
                false,
                $this->id,
                $this->account_id
            );
        } elseif ($type == 'D') {
            $updateSection = 'entity deleted';
            AuditLog::create(
                $type,
                $updateSection,
                'provider_practice_gmb',
                $notes,
                false,
                0,
                false,
                $this->id,
                $this->account_id
            );
        }
    }


    /**
     * Fetch the claim status for practice and provider at practice
     *
     * @param string $practiceId
     * @param string $providerId
     *
     * @return array
     */
    public function getClaimStatus($practiceId, $providerId = null)
    {
        $practiceClaimed = false;
        $providerPracticeClaimed = false;

        $providerAtPractice = null;
        $practiceOnly = $this->findAllByAttributes(['practice_id' => $practiceId]);

        if ($practiceOnly) {
            foreach ($practiceOnly as $gmb) {
                if ($gmb->is_claimed_by && $gmb->is_claimed_by !== 'NONE') {
                    $practiceClaimed = true;
                    break;
                }
            }

            if ($providerId) {
                $providerAtPractice = $this->findAllByAttributes([
                    'practice_id' => $practiceId,
                    'provider_id' => $providerId
                ]);

                if ($providerAtPractice) {
                    foreach ($providerAtPractice as $gmb) {
                        if ($gmb->is_claimed_by && $gmb->is_claimed_by !== 'NONE') {
                            $providerPracticeClaimed = true;
                            break;
                        }
                    }
                }
            }
        }

        return [
            'practice_claimed' => $practiceClaimed,
            'provider_practice_claimed' => $providerPracticeClaimed
        ];
    }

    /**
     * Get by provider and practice ids. If no provider id
     * is passed, then only searches for the practice record.
     *
     * @param string $practiceId
     * @param string $providerId
     *
     * @return ProviderPracticeGmb
     */
    public function getByPracticeAndProvider($practiceId, $providerId = null)
    {
        if ($providerId) {
            $gmb = $this->findByAttributes([
                'practice_id' => $practiceId,
                'provider_id' => $providerId
            ]);
        } else {
            $gmb = $this->findByAttributes(
                ['practice_id' => $practiceId],
                'provider_id IS NULL'
            );
        }

        return $gmb;
    }

    public function getByProvider($providerId)
    {
        return $this->findAllByAttributes([
            'provider_id' => $providerId
        ]);
    }

    /**
     * @return bool
     */
    public function isProviderAtPractice()
    {
        if ($this->provider_id) {
            return true;
        }
        return false;
    }


    /**
     * returns list of organization features
     * return $list
     */
    public function getFeatures()
    {
        $list = [];
        if (!empty($this->account->organization) && !empty($this->account->organization->organizationFeatures)) {
            foreach ($this->account->organization->organizationFeatures as $feature) {
                if (
                    $feature->practice_id == $this->practice_id
                    ||
                    ($this->provider_id && $feature->provider_id == $this->provider_id)
                ) {
                    switch ($feature->feature_id) {
                        case Feature::GMB:
                            if (!in_array('GMB Listing', $list)) {
                                $list[] = 'GMB Listing';
                            }
                            break;
                        case  Feature::REP_INSIGHTS:
                            if (!in_array('Rep Insight', $list)) {
                                $list[] = 'Rep Insight';
                            }
                            break;
                        case Feature::REVIEWHUB:
                        case Feature::REVIEW_REQUEST:
                            if (!in_array('Review Request', $list)) {
                                $list[] = 'Review Request';
                            }
                            break;
                        default:
                    }
                }
            }
        }
        return $list;
    }

    /**
     * @param $dataPointId
     * @return bool
     */
    public function hasDataPointEnabled($dataPointId)
    {
        $dp = $this->getDataPoint($dataPointId);
        if ($dp->enabled == 1) {
            return true;
        }
        return false;
    }

    /**
     * @param $dataPointId
     * @return ProviderPracticeGmbDataPoint|null
     */
    public function getDataPoint($dataPointId)
    {
        foreach ($this->providerPracticeGmbDataPoints as $dp) {
            if ($dp->gmb_data_point_id == $dataPointId) {
                return $dp;
            }
        }
        return null;
    }

    /**
     * Set data point
     * @param $dataPointId
     * @param int $enabled
     * @param null $override
     */
    public function setDataPoint($dataPointId, $enabled = 0, $override = null)
    {
        $isNew = false;
        $dataPoint = $this->getDataPoint($dataPointId);

        // If the data point for this entity was not found, create a new one
        if (!$dataPoint) {
            $dataPoint = new ProviderPracticeGmbDataPoint();
            $dataPoint->provider_practice_gmb_id = $this->id;
            $dataPoint->gmb_data_point_id = $dataPointId;
            $isNew = true;
        }

        // Update data point
        $dataPoint->enabled = $enabled;
        $dataPoint->override = $override;
        $dataPoint->save();

        // If this data point is new attach it to the model
        if ($isNew) {
            $index = count($this->providerPracticeGmbDataPoints);
            $this->addRelatedRecord('providerPracticeGmbDataPoints', $dataPoint, $index);
        }

        return $dataPoint;
    }

    public function isVerified()
    {
        if ($this->providerPracticeGmbLocationState && $this->providerPracticeGmbLocationState->is_verified) {
            return true;
        }
        return false;
    }

    public function getOrganization()
    {
        if ($this->account) {
            return $this->account->organization;
        }
        return null;
    }

    public function textSearch($text)
    {
        $conditions = [
            't.place_id IS NOT NULL'
        ];

        $conditionsSql = implode(' AND ', $conditions);

        $orConditions = [
            'provider.first_m_last LIKE :text_like_1',
            'provider.last_name LIKE :text_like_2',
            'practice.name LIKE :text_like_3',
        ];

        $orConditionsSql = implode(' OR ', $orConditions);
        $textLike = '%' . $text . '%';

        $where = $conditionsSql . ' AND (' . $orConditionsSql . ')';

        $criteria = new CDbCriteria;
        $criteria->condition = $where;
        $criteria->limit = 5;
        $criteria->params = [
            ':text_like_1' => $textLike,
            ':text_like_2' => $textLike,
            ':text_like_3' => $textLike,
        ];
        $criteria->with = [
            'provider',
            'practice'
        ];

        return ProviderPracticeGmb::model()->findAll($criteria);
    }


    /**
     *  returning name of the columns which are updating
     *  @param: $request
     */
    public function getGMBproviderpracitceFlagInfo($request)
    {
        $jsonEncode = json_encode($request);
        $jsonDecode = json_decode($jsonEncode, true);
        $keyFlag = array_keys($jsonDecode);
        $valueFlag = array_values($jsonDecode);
        if ($keyFlag[1] == 'data_points') {
            //if override name is empty it returns value as "name"
            $returnFlag = isset($valueFlag[1][0]["name"]) ? $valueFlag[1][0]["name"] : 'name';
            //checks if enabled is true or false
            $statusFlag = $valueFlag[1][0]["enabled"] ? 'enabled' : 'disabled';
            if (strlen($valueFlag[1][0]["override"]) > 0) {
                //there is value in override text input,add it
                $override = $valueFlag[1][0]["override"];
                return "data_point_{$returnFlag}_{$statusFlag}_override_$override";
            }

            //if no value in override text input place "none" as
            return "data_point_{$returnFlag}_{$statusFlag}_override_none";
        } elseif ($keyFlag[1] == 'category_1' || $keyFlag[1] == 'category_2' || $keyFlag[1] == 'category_3') {
            return "$keyFlag[1]_edited_$valueFlag[1]";
        } elseif ($keyFlag[1] == 'place_id' && $this->link_verified == 1 && $this->integrate == 1) {
            if (ProviderPracticeGmb::model()->exists('place_id = :place_id', ['place_id' => $this->place_id])) {
                return 'panel integrated';
            } else {
                return "panel_integrated  place_id_edited_$valueFlag[1]";
            }
        } elseif ($keyFlag[1] == 'place_id' && $this->integrate == 0) {
            return "place_id_edited_$valueFlag[1]";
        } elseif ($keyFlag[1] == 'is_read_only') {
            // in code , value returing from card.php by clicking Sync Data is for is_read_only and value is opposite of
            // checkbox
            // If it is is enabled = 0 , disabled = 1,that behaviour is overriden only for audit log,also name would be
            // sync data instead of is_read_only.
            $statusFlag = $valueFlag[1] == 1 ? 'disabled' : 'enabled';
            return "sync_data_edited_$statusFlag";
        } else {
            //conditions where integrate = 1
            if ($keyFlag[1] == 'place_id') {
                return "place_id_edited_$valueFlag[1]";
            }
            //checks every other flags except place id,data point and category
            if ($valueFlag[1] == 1) {
                $statusFlag = 'enabled';
            } else {
                $statusFlag = 'disabled';
            }
            return "$keyFlag[1]_edited_$statusFlag";
        }
    }

    /**
     *  checking name of the columns which are updating
     *  @param: $request
     */
    public function getGMBproviderpracitceNoteInfo($request)
    {

        $dataPid = null;
        $jsonEncode = json_encode($request);
        $jsonDecode = json_decode($jsonEncode, true);
        $keyFlag = array_keys($jsonDecode);

        if ($keyFlag[1] == 'data_points') {
            $dataPid = $jsonDecode['data_points'][0]['id'];
        }
        $dataNames = GmbDataPoint::model()->find(
            'id = :id',
            array(':id' => $dataPid)
        );
        $fields = [
            'skip', 'integrate', 'create', 'claim', 'request_ownership', 'verify', 'merge', 'close',
            'is_read_only', 'category_1', 'category_2', 'category_3', 'place_id', 'sync_tracking_number'
        ];

        foreach ($fields as $field) {
            if (isset($request->{$field})) {
                return $field;
            }
        }
        if ($keyFlag[1] == 'data_points') {
            return $dataNames->name;
        }
    }
}
