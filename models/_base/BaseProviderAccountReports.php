<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

abstract class BaseProviderAccountReports extends BaseModel
{

    public $account_email;
    public $account_first_name;
    public $account_last_name;
    public $account_type;
    public $phone_number;
    public $state_name;
    public $city_name;
    public $zipcode;
    public $docscore;
    public $new_reviews;
    public $online_appointments;
    public $search_views;
    public $profile_views;
    public $tracked_calls;
    public $qualified_tracked_calls;
    public $clicks_to_call;
    public $clicks_to_fax;
    public $clicks_to_website;
    public $clicks_to_social_media;
    public $date_created;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'account_provider';
    }

    public function rules()
    {
        return array(
            array(
                'account_email, account_first_name, account_last_name, account_type, phone_number, state_name,
                city_name, zipcode, docscore, online_appointments, new_reviews, search_views, profile_views,
                tracked_calls, qualified_tracked_calls, clicks_to_call, clicks_to_fax, clicks_to_website,
                clicks_to_social_media, date_added',
                'required'
            ),
            array(
                'account_email, account_first_name, account_last_name, account_type, phone_number, state_name,
                city_name, zipcode, docscore, online_appointments, new_reviews, search_views, profile_views,
                tracked_calls, qualified_tracked_calls, clicks_to_call, clicks_to_fax, clicks_to_website,
                clicks_to_social_media, date_added',
                'safe',
                'on' => 'search'
            ),
        );
    }

    public function relations()
    {
        return array(
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'provider' => array(self::BELONGS_TO, 'Provider', 'provider_id'),
        );
    }

    public function search()
    {

        $criteria = new CDbCriteria;

        $criteria->with = array('account.accountType', 'provider', 'account.location.cities.state');

        // Filter for partners
        if (isset($_GET['Partners']) && $_GET['Partners'] != '') {
            $criteria->addInCondition('t.id', $this->getFilterId($_GET['Partners']));
        }

        // Filter for sites
        if (isset($_GET['PartnerSites']) && $_GET['PartnerSites'] != '') {
            $criteria->addInCondition('t.id', $this->getPartnerSiteFilterId($_GET['PartnerSites']));
        }

        // Filter for date
        $where = '';
        if (isset($_GET['Date'])) {
            $date = $_GET['Date'];
        } else {
            $date = 8;
        }
        $date = PartnerFilters::getDate($date);
        if ($date != '') {
            $aux = split('-', $date);
            if (count($aux) > 1) {
                $where .= 't.date_added >= "' . $date . '"';
            } else {
                $where .= ' YEAR(t.date_added) >= "' . $date . '"';
            }
            $criteria->addCondition($where);
        }

        // Filters for the CGridView
        $criteria->compare('account.email', $this->account_email, true);
        $criteria->compare('account.first_name', $this->account_first_name, true);
        $criteria->compare('account.last_name', $this->account_last_name, true);
        $criteria->compare('accountType.name', $this->account_type, true);
        $criteria->compare('account.phone_number', $this->phone_number, true);
        $criteria->compare('state.name', $this->state_name, true);
        $criteria->compare('cities.name', $this->city_name, true);
        $criteria->compare('location.zipcode', $this->zipcode, true);
        $criteria->compare('provider.docscore', $this->docscore, true);
        $criteria->compare('online_appointments', $this->online_appointments);
        $criteria->compare('new_reviews', $this->new_reviews);
        $criteria->compare('search_views', $this->search_views);
        $criteria->compare('profile_views', $this->profile_views);
        $criteria->compare('tracked_calls', $this->tracked_calls);
        $criteria->compare('qualified_tracked_calls', $this->qualified_tracked_calls);
        $criteria->compare('clicks_to_call', $this->clicks_to_call);
        $criteria->compare('clicks_to_fax', $this->clicks_to_fax);
        $criteria->compare('clicks_to_website', $this->clicks_to_website);
        $criteria->compare('clicks_to_social_media', $this->clicks_to_social_media);
        $criteria->compare('t.date_added', $this->date_added, true);

        $criteria->join = 'LEFT JOIN (SELECT provider_id, COUNT(id) AS online_appointments
            FROM scheduling_mail
            GROUP BY 1) online_appointments_table
            ON online_appointments_table.provider_id = t.provider_id
            LEFT JOIN (SELECT provider_id, COUNT(provider_rating.id) AS new_reviews
            FROM provider_rating
            GROUP BY 1) new_reviews_table
            ON new_reviews_table.provider_id = t.provider_id
            LEFT JOIN (SELECT COUNT(tracking_activity_parameter.id) AS search_views, provider.id AS provider_id,
                tracking_activity_type.id AS type
                FROM provider
                INNER JOIN account_provider
                ON provider.id = account_provider.provider_id
                INNER JOIN account
                ON account.id = account_provider.account_id
                INNER JOIN partner_site
                ON partner_site.id = account.partner_site_id
                INNER JOIN tracking_activity
                ON tracking_activity.site_id = partner_site.id
                INNER JOIN tracking_activity_parameter
                ON tracking_activity_parameter.tracking_activity_id = tracking_activity.id
                INNER JOIN tracking_activity_type
                ON tracking_activity_type.id = 1)
                search_views
            ON search_views.provider_id = t.provider_id
            LEFT JOIN (SELECT COUNT(tracking_activity_parameter.id) AS profile_views, provider.id AS provider_id,
                tracking_activity_type.id AS type
                FROM provider
                INNER JOIN account_provider
                ON provider.id = account_provider.provider_id
                INNER JOIN account
                ON account.id = account_provider.account_id
                INNER JOIN partner_site
                ON partner_site.id = account.partner_site_id
                INNER JOIN tracking_activity
                ON tracking_activity.site_id = partner_site.id
                INNER JOIN tracking_activity_parameter
                ON tracking_activity_parameter.tracking_activity_id = tracking_activity.id
                INNER JOIN tracking_activity_type
                ON tracking_activity_type.id = 2)
                profile_views
            ON profile_views.provider_id = t.provider_id
            LEFT JOIN (SELECT COUNT(twilio_log.id) AS tracked_calls, twilio_log.provider_id AS provider_id
                FROM twilio_log) twilio_log
            ON twilio_log.provider_id = t.provider_id
            LEFT JOIN (SELECT COUNT(twilio_log.id) AS qualified_tracked_calls, twilio_log.provider_id AS provider_id
                FROM twilio_log
                WHERE twilio_log.tw_call_duration > 40) qualified_twilio_log
            ON qualified_twilio_log.provider_id = t.provider_id
            LEFT JOIN (SELECT COUNT(tracking_activity.id) AS clicks_to_call, account.id AS account_id
                FROM account
                INNER JOIN partner_site
                ON partner_site.id = account.partner_site_id
                INNER JOIN tracking_activity
                ON partner_site.id = tracking_activity.site_id
                INNER JOIN tracking_activity_type
                ON tracking_activity_type.id = tracking_activity.tracking_activity_type_id
                WHERE tracking_activity.tracking_activity_type_id = 3) table_tracking_activity
            ON table_tracking_activity.account_id = t.account_id
            LEFT JOIN (SELECT COUNT(tracking_activity.id) AS clicks_to_fax, account.id AS account_id
                FROM account
                INNER JOIN partner_site
                ON partner_site.id = account.partner_site_id
                INNER JOIN tracking_activity
                ON partner_site.id = tracking_activity.site_id
                INNER JOIN tracking_activity_type
                ON tracking_activity_type.id = tracking_activity.tracking_activity_type_id
                WHERE tracking_activity.tracking_activity_type_id = 4) tracking_activity_fax
            ON table_tracking_activity.account_id = t.account_id
            LEFT JOIN (SELECT COUNT(tracking_activity.id) AS clicks_to_website, account.id AS account_id
                FROM account
                INNER JOIN partner_site
                ON partner_site.id = account.partner_site_id
                INNER JOIN tracking_activity
                ON partner_site.id = tracking_activity.site_id
                INNER JOIN tracking_activity_type
                ON tracking_activity_type.id = tracking_activity.tracking_activity_type_id
                WHERE tracking_activity.tracking_activity_type_id = 5) tracking_activity_website
            ON table_tracking_activity.account_id = t.account_id
            LEFT JOIN (SELECT COUNT(tracking_activity.id) AS clicks_to_social_media, account.id AS account_id
                FROM account
                INNER JOIN partner_site
                ON partner_site.id = account.partner_site_id
                INNER JOIN tracking_activity
                ON partner_site.id = tracking_activity.site_id
                INNER JOIN tracking_activity_type
                ON tracking_activity_type.id = tracking_activity.tracking_activity_type_id
                WHERE tracking_activity.tracking_activity_type_id = 7) tracking_activity_social_media
            ON table_tracking_activity.account_id = t.account_id';

        $criteria->select = 't.*,
            online_appointments_table.online_appointments AS online_appointments,
            new_reviews_table.new_reviews AS new_reviews,
            search_views.search_views AS search_views,
            profile_views.profile_views AS profile_views,
            twilio_log.tracked_calls AS tracked_calls,
            qualified_twilio_log.qualified_tracked_calls AS qualified_tracked_calls,
            table_tracking_activity.clicks_to_call AS clicks_to_call,
            tracking_activity_fax.clicks_to_fax AS clicks_to_fax,
            tracking_activity_website.clicks_to_website AS clicks_to_website,
            tracking_activity_social_media.clicks_to_social_media AS clicks_to_social_media';

        if (isset($_GET['export'])) {
            return new CActiveDataProvider(
                get_class($this),
                array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => 500)
                    )
            );
        } else {
            return new CActiveDataProvider(
                get_class($this),
                array(
                    'criteria' => $criteria,
                    'pagination' => array('pageSize' => 100)
                    )
            );
        }
    }

    /**
     * @todo document
     * @param array $array
     * @return array
     */
    public function getFilterId($array)
    {

        $sql = 'SELECT account_provider.id
            FROM partner_site
            INNER JOIN provider
            ON provider.partner_site_id = partner_site.id
            INNER JOIN account_provider
            ON account_provider.provider_id = provider.id
            ' . $this->getWhere($array);

        $aux = Yii::app()->db->createCommand($sql)->queryAll();

        $array2 = array();
        foreach ($aux as $id) {
            $array2[] = $id['id'];
        }

        return $array2;
    }

    /**
     * @todo document
     * @param array $array
     * @return array
     */
    public function getPartnerSiteFilterId($array)
    {
        $sql = 'SELECT account_provider.id
            FROM partner_site
            INNER JOIN account
            ON account.partner_site_id = partner_site.id
            INNER JOIN account_provider
            ON account_provider.account_id = account.id
            ' . $this->getWhere($array);

        $aux = Yii::app()->db->createCommand($sql)->queryAll();

        $array2 = array();
        foreach ($aux as $id) {
            $array2[] = $id['id'];
        }

        return $array2;
    }

    /**
     * @todo document
     * @param array $array
     * @return string
     */
    public function getWhere($array)
    {
        $where = '';
        if (isset($array) && $array != null) {
            foreach ($array as $partnerId) {
                if ($where == '') {
                    $where = 'WHERE partner_site.partner_id IN (' . $partnerId;
                } else {
                    $where .= ', ' . $partnerId;
                }
            }
            $where .= ')';
        }

        return $where;
    }
}
