<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "template".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Template".
 *
 * Columns in table "template" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property string $lang_id
 * @property string $internal_type
 * @property string $name
 * @property string $subject
 * @property string $frame_template_name
 * @property string $content
 * @property string $content_plain
 *
 */
abstract class BaseTemplate extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'template';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Template|Templates', $n);
    }

    public static function representingColumn()
    {
        return 'name';
    }

    public function rules()
    {
        return array(
            array('internal_type, name, subject, content, content_plain', 'required'),
            array('lang_id', 'length', 'max' => 6),
            array('internal_type', 'length', 'max' => 100),
            array('name, subject, frame_template_name', 'length', 'max' => 255),
            array('lang_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, lang_id, internal_type, name, subject, content, content_plain, frame_template_name', 'safe', 'on' => 'search'),
            array('content_plain', 'safe'),
            array('subject', 'safe'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'lang_id' => Yii::t('app', 'Language'),
            'internal_type' => Yii::t('app', 'Internal type'),
            'name' => Yii::t('app', 'Name'),
            'subject' => Yii::t('app', 'Subject'),
            'frame_template_name' => Yii::t('app', 'Frame Template Name'),
            'content' => Yii::t('app', 'Content'),
            'content_plain' => Yii::t('app', 'Plaintext content'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('lang_id', $this->lang_id, true);
        $criteria->compare('internal_type', $this->internal_type, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('subject', $this->subject, true);
        $criteria->compare('frame_template_name', $this->frame_template_name, true);
        $criteria->compare('content', $this->content, true);
        $criteria->compare('content_plain', $this->content_plain, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100)
        ));
    }

}
