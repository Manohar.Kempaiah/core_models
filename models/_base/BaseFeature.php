<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "feature".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Feature".
 *
 * Columns in table "feature" available as properties of the model,
 * followed by relations of table "feature" available as properties of the model.
 *
 * @property integer $id
 * @property string $per
 * @property string $name
 * @property string $description
 * @property string $date_added
 * @property string $date_updated
 *
 * @property OrganizationFeature[] $organizationFeatures
 * @property OrganizationHistory[] $organizationHistories
 */
class BaseFeature extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'feature';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Feature|Features', $n);
    }

    public static function representingColumn()
    {
        return 'name';
    }

    public function rules()
    {
        return array(
            array('name, description', 'required'),
            array('per', 'length', 'max' => 1),
            array('name', 'length', 'max' => 254),
            array('description', 'length', 'max' => 512),
            array('date_added, date_updated', 'safe'),
            array('per, date_added, date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, per, name, description, date_added, date_updated', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'organizationFeatures' => array(self::HAS_MANY, 'OrganizationFeature', 'feature_id'),
            'organizationHistories' => array(self::HAS_MANY, 'OrganizationHistory', 'feature_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'per' => Yii::t('app', 'Per'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'date_added' => Yii::t('app', 'Date Added'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'organizationFeatures' => null,
            'organizationHistories' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('per', $this->per, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
