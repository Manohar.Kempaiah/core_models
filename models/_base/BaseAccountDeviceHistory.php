<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "account_device_history".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AccountDeviceHistory".
 *
 * Columns in table "account_device_history" available as properties of the model,
 * followed by relations of table "account_device_history" available as properties of the model.
 *
 * @property string $id
 * @property integer $account_device_id
 * @property string $prev_status
 * @property string $new_status
 * @property string $change_type
 * @property integer $account_id
 * @property string $date_added
 *
 * @property AccountDevice $accountDevice
 * @property Account $account
 */
abstract class BaseAccountDeviceHistory extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'account_device_history';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'AccountDeviceHistory|AccountDeviceHistories', $n);
    }

    public static function representingColumn()
    {
        return 'new_status';
    }

    public function rules()
    {
        return array(
            array('account_device_id, new_status, date_added', 'required'),
            array('account_device_id, account_id', 'numerical', 'integerOnly' => true),
            array('prev_status, new_status, change_type', 'length', 'max' => 1),
            array('prev_status, change_type, account_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array(
                'id, account_device_id, prev_status, new_status, change_type, account_id, date_added',
                'safe',
                'on' => 'search'
            ),
        );
    }

    public function relations()
    {
        return array(
            'accountDevice' => array(self::BELONGS_TO, 'AccountDevice', 'account_device_id'),
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'account_device_id' => null,
            'prev_status' => Yii::t('app', 'Prev Status'),
            'new_status' => Yii::t('app', 'New Status'),
            'change_type' => Yii::t('app', 'Change Type'),
            'account_id' => null,
            'date_added' => Yii::t('app', 'Date Added'),
            'accountDevice' => null,
            'account' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('account_device_id', $this->account_device_id);
        $criteria->compare('prev_status', $this->prev_status, true);
        $criteria->compare('new_status', $this->new_status, true);
        $criteria->compare('change_type', $this->change_type, true);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('date_added', $this->date_added, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
