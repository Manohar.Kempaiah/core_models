<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for tables where we use the 'secret' password hash container
 * Includes authentication and hashing functionality
 */
abstract class BasePasswordModel extends BaseModel
{

    public static $allowablePasswordSymbols = "!#$%&'()*+,-.:;<=>?@[]^_`{|}~";
    public static $passwordLongevityDays = 90;
    public static $passwordMinimumLength = 8;

    public static $passwordCanExpire = true;
    /**
     * Encode password if needed
     * @internal Remember to call parent::beforeValidate() when overriding beforeValidate
     */
    public function beforeValidate()
    {
        $changePassword = false;
        $checkPasswordConfirm = false;

        if ($this->getScenario() == 'skipPassword') {
            return parent::beforeValidate();
        }

        if ($this->isNewRecord) {
            if (empty($this->password) || empty($this->password_confirm)) {
                // send back both validation errors if necessary
                if (empty($this->password)) {
                    $this->addError('password', 'Please fill the Password.');
                }
                if (empty($this->password_confirm)) {
                    $this->addError('password_confirm', 'Confirm Password must be the same as the Password.');
                }
                return false;

            } elseif (isset($this->password)  && !empty($this->password)
                && isset($this->password_confirm) && !empty($this->password_confirm)
                && empty($this->secret)
                ) {
                    $checkPasswordConfirm = true;
                    $changePassword = true;
            } elseif (isset($this->password) && !empty($this->password) && empty($this->secret)) {
                $this->password_confirm = $this->password;
                $changePassword = true;
            }
        } elseif (!empty($this->password_confirm) && !empty($this->password)) {
            // if password and password_confirm are filled, it's a password change
            $checkPasswordConfirm = true;
            $changePassword = true;
        }

        if ($checkPasswordConfirm) {
            $passwordAndConfirmPasswordAreEqual = $this->password === $this->password_confirm;

            if (!$passwordAndConfirmPasswordAreEqual) {
                $this->addError('password_confirm', 'Password and Confirm Password must be the same.');
                return false;
            }
        }

        if ($changePassword) {
            $issues = self::validateNewPassword($this->password);

            $passesPasswordRequirements = empty($issues);

            foreach ($issues as $issueDescription) {
                $this->addError('password', $issueDescription);
            }

            if ($passesPasswordRequirements) {

                $pastPassword = new AccountPasswordHistory;
                $pastPassword->entity_id = $this->id;
                $pastPassword->entity_type = AccountPasswordHistory::getEntityType(get_class($this));
                $pastPassword->secret = $this->secret;

                if (!empty(Yii::app()->session['admin_account_id'])) {
                    $adminAccount = Account::model()->find(
                        'MD5(id)=:adminAccountId',
                        array(
                            ':adminAccountId' => Yii::app()->session['admin_account_id']
                        )
                    );
                    if ($adminAccount) {
                        $pastPassword->added_by_account_id = $adminAccount->id;
                    }
                } elseif (!empty(Yii::app()->user->account_type_id)
                        && AccountType::isAdminLevel(Yii::app()->user->account_type_id)) {
                    $pastPassword->added_by_account_id = Yii::app()->user->id;
                }

                $pastPassword->save();

                $this->secret = self::encodePassword($this->password);
                $this->last_password_change = date('Y-m-d H:i:s');
            } else {
                return false;
            }
        }

        return parent::beforeValidate();
    }

    /**
     * Validate data beforeSave()
     * @return boolean
     */
    public function beforeSave()
    {
        // wipe password variable to make sure we don't try and save it anywhere
        if (!empty($this->password)) {
            $this->password = '';
        }

        return parent::beforeSave();
    }

    /**
     * Check whether the password is correct
     * @param string $password
     * @return boolean
     */
    public function authenticate($password)
    {
        return password_verify(md5($password), $this->secret);
    }

    /**
     * Generates a random password and sets both password and password_confirm
     * so that there is no validation error on saving.
     *
     * Returns the UNENCODED plaintext password in case it needs to be
     * printed to the user in an email.
     *
     * @return string
     */
    public function assignRandomPassword()
    {
        // To ensure required character sets are included, we add random-ish
        // selections from said sets to safely-generated random strings.
        $numberOfDigitsOrSymbols = mt_rand(1, 3);
        $numberOfUppercaseLetters = mt_rand(1, 3);
        $numberOfLowercaseLetters = mt_rand(1, 3);

        $digitsOrSymbols = self::createRandomizedSubstring(
            "0123456789" . self::$allowablePasswordSymbols,
            $numberOfDigitsOrSymbols
        );
        $uppercaseLetters = self::createRandomizedSubstring("ABCDEFGHIJKLMNOPQRSTUVWXYZ", $numberOfUppercaseLetters);
        $lowercaseLetters = self::createRandomizedSubstring("abcdefghijklmnopqrstuvwxyz", $numberOfLowercaseLetters);

        $combinedRequirements = $digitsOrSymbols . $uppercaseLetters . $lowercaseLetters;
        // str_shuffle is insufficiently random on its own, but both the length
        // and the contents of $combinedRequirements have been generated with mt_rand()
        $combinedRequirements = str_shuffle($combinedRequirements);

        // minimum length characters of password is generated via safe method
        $safeStringLength = intval(ceil(self::$passwordMinimumLength / 2));

        $mixinRandomString = Yii::app()->getSecurityManager()->generateRandomString($safeStringLength, true);
        $stableRandomString = Yii::app()->getSecurityManager()->generateRandomString($safeStringLength, true);

        // we are using both a random string order unaffected by further functions, as
        // well as a somewhat-random order of strings otherwise generated
        $plaintextPassword = $stableRandomString . str_shuffle($mixinRandomString . $combinedRequirements);

        $this->password         = $plaintextPassword;
        $this->password_confirm = $plaintextPassword;
        $encodedPassword = self::encodePassword($plaintextPassword);
        $this->secret = $encodedPassword;
        return $plaintextPassword;
    }

    /**
     * Internal method for taking a set of required characters, and choosing n of them in a random order
     * A slight improvement over substr(str_shuffle(str_repeat($definitionString, $length)), 0, $length);
     *
     * @param string $definitionString
     * @param int $length
     * @return string
     */
    private static function createRandomizedSubstring($definitionString, $length)
    {
        $resultString = "";

        for ($i = 0; $i < $length; $i++) {
            $resultString .= $definitionString[mt_rand(0, strlen($definitionString) - 1)];
        }

        return $resultString;
    }

    /**
     * Checks the validity of a new password against our rule set.
     *
     * @param string $newPassword
     * @return string[]
     */
    public function validateNewPassword($newPassword)
    {
        $issues = array();

        $meetsLengthRequirement = strlen($newPassword) >= self::$passwordMinimumLength;
        $containsUppercase = 1 === preg_match('/\p{Lu}/u', $newPassword);
        $containsLowercase = 1 === preg_match('/\p{Ll}/u', $newPassword);
        $containsNumber = 1 === preg_match('/\d/', $newPassword);
        $containsSymbol = false != strpbrk($newPassword, self::$allowablePasswordSymbols);
        $containsNumberOrSymbol = $containsNumber || $containsSymbol;

        if (!$meetsLengthRequirement) {
            $issues['LENGTH'] = 'The password must be at least ' . self::$passwordMinimumLength . ' characters long.';
        }
        if (!$containsUppercase) {
            $issues['UPPERCASE'] = 'The password must contain at least one uppercase letter.';
        }
        if (!$containsLowercase) {
            $issues['LOWERCASE'] = 'The password must contain at least one lowercase letter.';
        }
        if (!$containsNumberOrSymbol) {
            $issues['NUMBER_OR_SYMBOL'] = 'The password must contain at least one number or a '
                    . 'symbol from this list: ' . self::$allowablePasswordSymbols;
        }

        $sameAsCurrentPassword = password_verify(md5($newPassword), $this->secret);

        $entityType = AccountPasswordHistory::getEntityType(get_class($this));

        $pastPasswords = AccountPasswordHistory::model()->findAll(
            'entity_id=:entityId AND entity_type=:entityType',
            array(':entityId' => $this->id, ':entityType' => $entityType)
        );

        $sameAsHistoricalPassword = false;

        foreach ($pastPasswords as $pastPassword) {
            $exists = password_verify(md5($newPassword), $pastPassword->secret);

            if ($exists) {
                $sameAsHistoricalPassword = true;
                break; // already found one past instance, password_verify() is expensive computationally
            }
        }

        if ($sameAsCurrentPassword || $sameAsHistoricalPassword) {
            $issues['PAST'] = 'The password cannot be the same as any of your past passwords.';
        }

        return $issues;
    }

    /**
     * How many days have passed since the last password change?
     *
     * @return integer
     */
    public function daysSincePasswordChange()
    {
        if (empty($this->last_password_change)) {
            return self::$passwordLongevityDays;
        }

        $currentDate = new DateTime();
        $mostRecentPasswordChangeDate = new DateTime($this->last_password_change);

        return intval($mostRecentPasswordChangeDate->diff($currentDate)->format("%a"));
    }

    /**
     * Has the set amount of time passed since the user's password was last updated?
     *
     * @return boolean
     */
    public function passwordExpired()
    {
        if (self::$passwordCanExpire === false) {
            return false;
        }

        return (self::daysSincePasswordChange() >= self::$passwordLongevityDays);
    }

    /**
     * How many days are left before the password expires?
     *
     * @return integer
     */
    public function passwordLongevityDaysRemaining()
    {
        if (self::$passwordCanExpire === false) {
            return self::$passwordLongevityDays;
        }

        return self::$passwordLongevityDays - self::daysSincePasswordChange();
    }

    /**
     * Encodes the given password for secure storage in our database
     *
     * @param string $pwd
     * @return string
     */
    private static function encodePassword($pwd)
    {
        // set bcrypt cost to 12 based on current literature recommendations
        $options = [
            'cost' => 12,
        ];

        $intermediate = md5($pwd);

        return password_hash($intermediate, PASSWORD_BCRYPT, $options);
    }

}
