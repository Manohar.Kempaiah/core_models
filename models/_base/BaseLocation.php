<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "location".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Location".
 *
 * Columns in table "location" available as properties of the model,
 * followed by relations of table "location" available as properties of the model.
 *
 * @property integer $id
 * @property integer $city_id
 * @property integer $is_pending_approval
 * @property string $city_name
 * @property string $zipcode
 * @property double $latitude
 * @property double $longitude
 * @property string $zipcode_type
 * @property integer $dma_id
 *
 * @property Account[] $accounts
 * @property Dma $dma
 * @property City $city
 * @property ManualValidation[] $manualValidations
 * @property Patient[] $patients
 * @property Practice[] $practices
 * @property PracticeDetails[] $practiceDetails
 * @property PracticeRemoved[] $practiceRemoveds
 * @property ProviderPreviousPractice[] $providerPreviousPractices
 */
abstract class BaseLocation extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'location';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Location|Locations', $n);
    }

    public static function representingColumn()
    {
        return 'city_name';
    }

    public function rules()
    {
        return array(
            array('city_id, city_name, zipcode, dma_id', 'required'),
            array('city_id, is_pending_approval, dma_id', 'numerical', 'integerOnly' => true),
            array('latitude, longitude', 'numerical'),
            array('city_name', 'length', 'max' => 50),
            array('zipcode, zipcode_type', 'length', 'max' => 10),
            array('is_pending_approval, latitude, longitude, zipcode_type', 'default',
                'setOnEmpty' => true, 'value' => null),
            array('id, city_id, is_pending_approval, city_name, zipcode, latitude, longitude, zipcode_type, dma_id',
                'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'accounts' => array(self::HAS_MANY, 'Account', 'location_id'),
            'dma' => array(self::BELONGS_TO, 'Dma', 'dma_id'),
            'city' => array(self::BELONGS_TO, 'City', 'city_id'),
            'manualValidations' => array(self::HAS_MANY, 'ManualValidation', 'location_id'),
            'patients' => array(self::HAS_MANY, 'Patient', 'location_id'),
            'practices' => array(self::HAS_MANY, 'Practice', 'location_id'),
            'practiceDetails' => array(self::HAS_MANY, 'PracticeDetails', 'coverage_location_id'),
            'practiceRemoveds' => array(self::HAS_MANY, 'PracticeRemoved', 'location_id'),
            'providerPreviousPractices' => array(self::HAS_MANY, 'ProviderPreviousPractice', 'location_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'city_id' => null,
            'is_pending_approval' => Yii::t('app', 'Is Pending Approval'),
            'city_name' => Yii::t('app', 'City Name'),
            'zipcode' => Yii::t('app', 'Zipcode'),
            'latitude' => Yii::t('app', 'Latitude'),
            'longitude' => Yii::t('app', 'Longitude'),
            'zipcode_type' => Yii::t('app', 'Zipcode Type'),
            'dma_id' => Yii::t('app', 'Dma'),
            'accounts' => null,
            'dma' => Yii::t('app', 'Dma'),
            'city' => null,
            'manualValidations' => null,
            'patients' => null,
            'practices' => null,
            'practiceDetails' => null,
            'practiceRemoveds' => null,
            'providerPreviousPractices' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('city_id', $this->city_id);
        $criteria->compare('is_pending_approval', $this->is_pending_approval);
        $criteria->compare('city_name', $this->city_name, true);
        $criteria->compare('zipcode', $this->zipcode, true);
        $criteria->compare('latitude', $this->latitude);
        $criteria->compare('longitude', $this->longitude);
        $criteria->compare('zipcode_type', $this->zipcode_type, true);
        $criteria->compare('dma_id', $this->dma_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
