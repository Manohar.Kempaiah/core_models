<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "type_group".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "TypeGroup".
 *
 * Columns in table "type_group" available as properties of the model,
 * followed by relations of table "type_group" available as properties of the model.
 *
 * @property string $id
 * @property string $name
 * @property string $code
 * @property string $description
 *
 * @property Type[] $types
 */
abstract class BaseTypeGroup extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'type_group';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'TypeGroup|TypeGroups', $n);
    }

    public static function representingColumn()
    {
        return 'name';
    }

    public function rules()
    {
        return array(
            array('name, code', 'required'),
            array('name', 'length', 'max'=>100),
            array('code', 'length', 'max'=>45),
            array('description', 'safe'),
            array('description', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, code, description', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'types' => array(self::HAS_MANY, 'Type', 'type_group_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'description' => Yii::t('app', 'Description'),
            'types' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
