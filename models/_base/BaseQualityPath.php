<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "quality_path".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "QualityPath".
 *
 * Columns in table "quality_path" available as properties of the model,
 * followed by relations of table "quality_path" available as properties of the model.
 *
 * @property integer $id
 * @property integer $practice_id
 * @property integer $provider_id
 * @property string $start_date
 * @property string $end_date
 * @property integer $quality_area_id
 * @property string $phone_number
 * @property string $url
 *
 * @property Practice $practice
 * @property Provider $provider
 * @property QualityArea $qualityArea
 */
abstract class BaseQualityPath extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'quality_path';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'QualityPath|QualityPaths', $n);
    }

    public static function representingColumn()
    {
        return 'start_date';
    }

    public function rules()
    {
        return array(
            array('practice_id, provider_id, start_date, quality_area_id', 'required'),
            array('practice_id, provider_id, quality_area_id', 'numerical', 'integerOnly' => true),
            array('phone_number', 'length', 'max' => 15),
            array('url', 'length', 'max' => 255),
            array('end_date', 'safe'),
            array('end_date, phone_number, url', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, practice_id, provider_id, start_date, end_date, quality_area_id, phone_number, url',
                'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'practice' => array(self::BELONGS_TO, 'Practice', 'practice_id'),
            'provider' => array(self::BELONGS_TO, 'Provider', 'provider_id'),
            'qualityArea' => array(self::BELONGS_TO, 'QualityArea', 'quality_area_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'practice_id' => null,
            'provider_id' => null,
            'start_date' => Yii::t('app', 'Start Date'),
            'end_date' => Yii::t('app', 'End Date'),
            'quality_area_id' => null,
            'phone_number' => Yii::t('app', 'Phone Number'),
            'url' => Yii::t('app', 'Url'),
            'practice' => null,
            'provider' => null,
            'qualityArea' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('practice_id', $this->practice_id);
        $criteria->compare('provider_id', $this->provider_id);
        $criteria->compare('start_date', $this->start_date, true);
        $criteria->compare('end_date', $this->end_date, true);
        $criteria->compare('quality_area_id', $this->quality_area_id);
        $criteria->compare('phone_number', $this->phone_number, true);
        $criteria->compare('url', $this->url, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
