<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "cpn_ehr_attribute_type".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CpnEhrAttributeType".
 *
 * Columns in table "cpn_ehr_attribute_type" available as properties of the model,
 * followed by relations of table "cpn_ehr_attribute_type" available as properties of the model.
 *
 * @property integer $id
 * @property string $guid
 * @property integer $cpn_ehr_id
 * @property integer $sockets_flag
 * @property integer $folder_flag
 * @property integer $sftp_flag
 * @property integer $encrypted_flag
 * @property integer $custom_flag
 * @property integer $enabled_flag
 * @property integer $del_flag
 * @property string $name
 * @property string $description
 * @property string $date_added
 * @property integer $added_by_account_id
 * @property string $date_updated
 * @property integer $updated_by_account_id
 *
 * @property Account $addedByAccount
 * @property CpnEhrInstallationAttribute[] $cpnEhrInstallationAttributes
 */
abstract class BaseCpnEhrAttributeType extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'cpn_ehr_attribute_type';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'CpnEhrAttributeType|CpnEhrAttributeTypes', $n);
    }

    public static function representingColumn()
    {
        return 'guid';
    }

    public function rules()
    {
        return array(
            array('guid, name, added_by_account_id', 'required'),
            array('cpn_ehr_id, sockets_flag, folder_flag, sftp_flag, custom_flag, encrypted_flag, enabled_flag, del_flag, added_by_account_id, updated_by_account_id', 'numerical', 'integerOnly' => true),
            array('guid', 'length', 'max' => 255),
            array('name', 'length', 'max' => 100),
            array('description', 'length', 'max' => 500),
            array('date_added, date_updated', 'safe'),
            array('cpn_ehr_id, sockets_flag, folder_flag, sftp_flag, custom_flag, encrypted_flag, enabled_flag, del_flag, description, date_added, date_updated, updated_by_account_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, guid, cpn_ehr_id, sockets_flag, folder_flag, sftp_flag, custom_flag, encrypted_flag, enabled_flag, del_flag, name, description, date_added, added_by_account_id, date_updated, updated_by_account_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'addedByAccount' => array(self::BELONGS_TO, 'Account', 'added_by_account_id'),
            'cpnEhrInstallationAttributes' => array(self::HAS_MANY, 'CpnEhrInstallationAttribute', 'cpn_ehr_attribute_type_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'guid' => Yii::t('app', 'Guid'),
            'cpn_ehr_id' => Yii::t('app', 'Cpn Ehr'),
            'sockets_flag' => Yii::t('app', 'Sockets Flag'),
            'folder_flag' => Yii::t('app', 'Folder Flag'),
            'sftp_flag' => Yii::t('app', 'Sftp Flag'),
            'custom_flag' => Yii::t('app', 'Custom Flag'),
            'encrypted_flag' => Yii::t('app', 'Encrypted Flag'),
            'enabled_flag' => Yii::t('app', 'Enabled Flag'),
            'del_flag' => Yii::t('app', 'Del Flag'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'date_added' => Yii::t('app', 'Date Added'),
            'added_by_account_id' => null,
            'date_updated' => Yii::t('app', 'Date Updated'),
            'updated_by_account_id' => Yii::t('app', 'Updated By Account'),
            'addedByAccount' => null,
            'cpnEhrInstallationAttributes' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('guid', $this->guid, true);
        $criteria->compare('cpn_ehr_id', $this->cpn_ehr_id);
        $criteria->compare('sockets_flag', $this->sockets_flag);
        $criteria->compare('folder_flag', $this->folder_flag);
        $criteria->compare('sftp_flag', $this->sftp_flag);
        $criteria->compare('custom_flag', $this->custom_flag);
        $criteria->compare('encrypted_flag', $this->encrypted_flag);
        $criteria->compare('enabled_flag', $this->enabled_flag);
        $criteria->compare('del_flag', $this->del_flag);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('added_by_account_id', $this->added_by_account_id);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('updated_by_account_id', $this->updated_by_account_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
