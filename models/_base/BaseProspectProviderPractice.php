<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "prospect_provider_practice".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProspectProviderPractice".
 *
 * Columns in table "prospect_provider_practice" available as properties of the model,
 * followed by relations of table "prospect_provider_practice" available as properties of the model.
 *
 * @property integer $id
 * @property integer $prospect_id
 * @property integer $prospect_provider_id
 * @property integer $prospect_practice_id
 * @property integer $is_primary
 *
 * @property ProspectListingScan[] $prospectListingScans
 * @property Prospect $prospect
 * @property ProspectPractice $prospectPractice
 * @property ProspectProvider $prospectProvider
 * @property ProspectReputationScan[] $prospectReputationScans
 */
abstract class BaseProspectProviderPractice extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'prospect_provider_practice';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ProspectProviderPractice|ProspectProviderPractices', $n);
    }

    public static function representingColumn()
    {
        return 'id';
    }

    public function rules()
    {
        return array(
            array('prospect_id, prospect_practice_id', 'required'),
            array('prospect_id, prospect_provider_id, prospect_practice_id, is_primary', 'numerical', 'integerOnly'=>true),
            array('prospect_provider_id, is_primary', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, prospect_id, prospect_provider_id, prospect_practice_id, is_primary', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'prospectListingScans' => array(self::HAS_MANY, 'ProspectListingScan', 'prospect_provider_practice_id'),
            'prospect' => array(self::BELONGS_TO, 'Prospect', 'prospect_id'),
            'prospectPractice' => array(self::BELONGS_TO, 'ProspectPractice', 'prospect_practice_id'),
            'prospectProvider' => array(self::BELONGS_TO, 'ProspectProvider', 'prospect_provider_id'),
            'prospectReputationScans' => array(self::HAS_MANY, 'ProspectReputationScan', 'prospect_provider_practice_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'prospect_id' => null,
            'prospect_provider_id' => null,
            'prospect_practice_id' => null,
            'is_primary' => Yii::t('app', 'Is Primary'),
            'prospectListingScans' => null,
            'prospect' => null,
            'prospectPractice' => null,
            'prospectProvider' => null,
            'prospectReputationScans' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('prospect_id', $this->prospect_id);
        $criteria->compare('prospect_provider_id', $this->prospect_provider_id);
        $criteria->compare('prospect_practice_id', $this->prospect_practice_id);
        $criteria->compare('is_primary', $this->is_primary);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
