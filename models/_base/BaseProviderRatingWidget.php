<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "provider_rating_widget".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProviderRatingWidget".
 *
 * Columns in table "provider_rating_widget" available as properties of the model,
 * followed by relations of table "provider_rating_widget" available as properties of the model.
 *
 * @property integer $id
 * @property integer $provider_rating_id
 * @property integer $widget_id
 * @property integer $account_id
 *
 * @property ProviderRating $providerRating
 * @property Widget $widget
 * @property Account $account
 */
abstract class BaseProviderRatingWidget extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'provider_rating_widget';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ProviderRatingWidget|ProviderRatingWidgets', $n);
    }

    public static function representingColumn()
    {
        return 'id';
    }

    public function rules()
    {
        return array(
            array('provider_rating_id, widget_id, account_id', 'numerical', 'integerOnly' => true),
            array('provider_rating_id, widget_id, account_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, provider_rating_id, widget_id, account_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'providerRating' => array(self::BELONGS_TO, 'ProviderRating', 'provider_rating_id'),
            'widget' => array(self::BELONGS_TO, 'Widget', 'widget_id'),
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'provider_rating_id' => null,
            'widget_id' => null,
            'account_id' => null,
            'providerRating' => null,
            'widget' => null,
            'account' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('provider_rating_id', $this->provider_rating_id);
        $criteria->compare('widget_id', $this->widget_id);
        $criteria->compare('account_id', $this->account_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
