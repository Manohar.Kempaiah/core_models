<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

abstract class BaseProviderAccountReport extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'account_provider';
    }

    public function relations()
    {
        return array(
            'account_email' => array(self::BELONGS_TO, 'Account', 'email'),
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->select = 'account_email';

        return new CActiveDataProvider(
            get_class($this),
            array(
                'criteria' => $criteria,
                'pagination' => array('pageSize' => 100)
            )
        );
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Account|Accounts', $n);
    }

}
