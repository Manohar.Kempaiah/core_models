<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "provider_language".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProviderLanguage".
 *
 * Columns in table "provider_language" available as properties of the model,
 * followed by relations of table "provider_language" available as properties of the model.
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $language_id
 *
 * @property Language $language
 * @property Provider $provider
 */
abstract class BaseProviderLanguage extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'provider_language';
    }

    public static function representingColumn()
    {
        return 'id';
    }

    public function rules()
    {
        return array(
            array('provider_id, language_id', 'required'),
            array('provider_id, language_id', 'numerical', 'integerOnly' => true),
            array('id, provider_id, language', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'language' => array(self::BELONGS_TO, 'Language', 'language_id'),
            'provider' => array(self::BELONGS_TO, 'Provider', 'provider_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Provider'),
            'language_id' => Yii::t('app', 'Language'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        //this is to enable eager loading - looks like it's no good to add provider here
        $criteria->with = array('language', 'provider');

        if (!intval($this->provider_id) && is_string($this->provider_id) && strlen($this->provider_id) > 0) {
            $criteria->compare('provider.first_last', $this->provider_id, true);
        } elseif (intval($this->provider_id)) {
            $criteria->compare('provider_id', $this->provider_id);
        }
        if (!intval($this->language_id) && is_string($this->language_id) && strlen($this->language_id) > 0) {
            $criteria->compare('language.name', $this->language_id, true);
        } elseif (intval($this->language_id)) {
            $criteria->compare('language_id', $this->language_id);
        }

        return new CActiveDataProvider(
            get_class($this),
            array(
                'criteria' => $criteria,
                'pagination' => array('pageSize' => 100)
            )
        );
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ProviderLanguage|ProviderLanguages', $n);
    }

}
