<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "idp_specialty_mapping".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "IdpSpecialtyMapping".
 *
 * Columns in table "idp_specialty_mapping" available as properties of the model,
 * followed by relations of table "idp_specialty_mapping" available as properties of the model.
 *
 * @property integer $id
 * @property string $specialty_text
 * @property integer $sub_specialty_id
 * @property string $credential
 * @property integer $idp_provision_id
 * @property string $date_added
 * @property string $date_updated
 *
 * @property SubSpecialty $subSpecialty
 */
abstract class BaseIdpSpecialtyMapping extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'idp_specialty_mapping';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'IdpSpecialtyMapping|IdpSpecialtyMappings', $n);
    }

    public static function representingColumn()
    {
        return 'date_added';
    }

    public function rules()
    {
        return array(
            array('date_added, date_updated', 'required'),
            array('sub_specialty_id, idp_provision_id', 'numerical', 'integerOnly'=>true),
            array('specialty_text', 'length', 'max'=>255),
            array('credential', 'length', 'max'=>250),
            array('specialty_text, sub_specialty_id, credential, idp_provision_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, specialty_text, sub_specialty_id, credential, idp_provision_id, date_added, date_updated', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'subSpecialty' => array(self::BELONGS_TO, 'SubSpecialty', 'sub_specialty_id'),
            'idpProvision' => array(self::BELONGS_TO, 'IdpProvision', 'idp_provision_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'specialty_text' => Yii::t('app', 'Specialty Text'),
            'sub_specialty_id' => null,
            'credential' => Yii::t('app', 'Credential'),
            'idp_provision_id' => Yii::t('app', 'Idp Provision'),
            'date_added' => Yii::t('app', 'Date Added'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'subSpecialty' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('specialty_text', $this->specialty_text, true);
        $criteria->compare('sub_specialty_id', $this->sub_specialty_id);
        $criteria->compare('credential', $this->credential, true);
        $criteria->compare('idp_provision_id', $this->idp_provision_id);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
