<?php

/**
 * This is the base Model class with common methods that should be extended by the BaseModels
 */
abstract class BaseModel extends MyActiveRecord
{

    public $basePath;
    public $oldRecord;

    /**
     * Retrieve the model name based on the table name
     * @param string $modelName Model name
     * @return string
     */
    public static function getTableFromModel($modelName)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $modelName, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    /**
     * Gets the name of the model associated with a given table
     * @param string $tableName Name of the table
     * @return mixed The model name of false if no model is associated with the table
     */
    public static function getModelNameFromTable($tableName)
    {
        $modelName = self::tableToModelName($tableName);

        $files = glob(Yii::app()->basePath . '/modules/core_models/models/' . $modelName . '.php');
        foreach ($files as $theModel) {
            if ($theModel) {
                $model = $modelName::model();
                if ($model->tableName() == $tableName) {
                    return $modelName;
                }
            }
        }

        return false;
    }

    /**
     * Get the model schema (columns)
     * @return array
     */
    public function getSchema()
    {
        $objSchema = $this->getMetaData()->columns;
        $schema = array();
        if (!empty($objSchema)) {
            foreach ($objSchema as $fieldName => $value) {
                if ($fieldName == 'email') {
                    $schema[$fieldName] = 'email';
                } else {
                    $schema[$fieldName] = $value->type;
                }
            }
        }
        return $schema;
    }

    /**
     * Gets a class name with base on a table name
     * @param type $str
     * @return type
     */
    protected static function tableToModelName($str)
    {
        // Split string in words.
        $words = explode('_', strtolower($str));

        $return = '';
        foreach ($words as $word) {
            $return .= ucfirst(trim($word));
        }

        return $return;
    }

    /**
     * Initializes the model with the correct basePath according to the active module.
     * @internal Avoid using __construct. Use init() instead.
     *  if you still need to use it, call parent::__construct($scenario). Read yii documentation.
     */
    public function init()
    {
        $activeModule = (isset(Yii::app()->controller->module->id) ? Yii::app()->controller->module->id : false);
        if ($activeModule) {
            $activeModuleObject = Yii::app()->getModule($activeModule);
            if ($activeModuleObject) {
                $this->basePath = $activeModuleObject->getBasePath();
                return;
            }
        }

        // no base path set, so set to base path of app itself
        // (note: this is needed for unit testing, for example, to work)
        $this->basePath = Yii::app()->getBasePath();
    }

    /**
     * Encode password if needed
     * @internal Remember to call parent::beforeValidate() when overriding beforeValidate
     */
    public function beforeValidate()
    {

        // if has Email, validate them
        $hasEmail = property_exists(get_called_class(), 'email') || $this->hasAttribute('email') || isset($this->email);
        if ($hasEmail && !empty($this->email)) {
            $this->email = StringComponent::validateEmail($this->email);
            if (empty($this->email)) {
                $this->addError('email', 'Incorrect Email Format.');
                return false;
            }
        }

        // set date_updated if that field exists
        if ($this->hasAttribute('date_updated') || property_exists(get_called_class(), 'date_updated')) {
            $this->date_updated = date('Y-m-d H:i:s');
        }

        return parent::beforeValidate();
    }

    /**
     * Automatically generate creation date if necessary.
     * @internal Remember to call parent::beforeSave() when overriding beforeSave
     */
    public function beforeSave()
    {

        $currentDate = date('Y-m-d H:i:s');

        // set date_added if that field exists and this is a new record
        if (
            $this->isNewRecord &&
            ($this->hasAttribute('date_added') || property_exists(get_called_class(), 'date_added'))
        ) {

            if (
                empty($this->date_added)
                || $this->date_added == '0000-00-00 00:00:00'
                || $this->date_added == 'CURRENT_TIMESTAMP'
            ) {
                $this->date_added = $currentDate;
            }
        }

        // set date_updated if that field exists
        if ($this->hasAttribute('date_updated') || property_exists(get_called_class(), 'date_updated')) {
            if (empty($this->date_updated) || $this->date_updated == '0000-00-00 00:00:00') {
                $this->date_updated = $currentDate;
            }
        }

        // validate date_birth attribute
        if ($this->hasAttribute('date_birth') || property_exists(get_called_class(), 'date_birth')) {
            if ($this->date_birth == '0000-00-00') {
                $this->date_birth = null;
            }
        }

        return parent::beforeSave();
    }

    /**
     * Submits change notifications to the PDI
     * @return boolean
     */
    public function afterSave()
    {
        if (!self::researchMode() && Yii::app()->params["pdi_notification"]) {
            ETLComponent::commitChanges($this, $this->isNewRecord ? "INSERT" : "UPDATE");
        }

        return parent::afterSave();
    }

    /**
     * Submits deletion notifications to the PDI
     * @return boolean
     */
    public function afterDelete()
    {
        if (!self::researchMode() && Yii::app()->params["pdi_notification"]) {
            ETLComponent::commitChanges($this, "DELETE");
        }

        return parent::afterDelete();
    }

    /**
     * Updates records with the specified condition and pushes the performed
     * changes to the PDI.
     * @param array $attributes list of attributes (name=>$value) to be updated
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return integer the number of rows being updated
     */
    public function updateAll($attributes, $condition = '', $params = array())
    {
        $updated = parent::updateAll($attributes, $condition, $params);

        if (!self::researchMode() && Yii::app()->params["pdi_notification"]) {
            if ($updated > 0) {
                //If records updated, find them and push their new column values in a transaction.
                $updatedModels = $this->findAll($condition, $params);
                ETLComponent::startTransaction();
                foreach ($updatedModels as $model) {
                    ETLComponent::commitChanges($model, "UPDATE");
                }
                ETLComponent::commitTransaction();
            }
        }

        return $updated;
    }

    /**
     * Deletes rows with the specified condition and submits the changes to the PDI.
     * @param mixed $condition query condition or criteria.
     * @param array $params parameters to be bound to an SQL statement.
     * @return integer the number of rows deleted
     */
    public function deleteAll($condition = '', $params = array())
    {
        if (!self::researchMode() && Yii::app()->params["pdi_notification"]) {
            //Get the retrieved records and open a PDI transaction
            $retrievedModels = $this->findAll($condition, $params);

            ETLComponent::startTransaction();
            foreach ($retrievedModels as $model) {
                ETLComponent::commitChanges($model, "DELETE");
            }
            $countRetrieved = count($retrievedModels);
        }

        $countDeleted = parent::deleteAll($condition, $params);

        if (!self::researchMode() && Yii::app()->params["pdi_notification"]) {
            //Submit changes only if the delete operation succeeded for all the
            //retrieved records.
            if ($countRetrieved == $countDeleted) {
                ETLComponent::commitTransaction();
            } else {
                ETLComponent::cancelTransaction();
            }
        }

        return $countDeleted;
    }

    /**
     * Implements cascade deletion of records
     * @param array $relations Aarray of relations to navigate
     */
    public function deleteRecursive($relations = array())
    {
        if (count($relations) == 0) {
            $relations = array_keys($this->relations());
        }

        foreach ($relations as $relation) {
            if (is_array($this->$relation)) {
                foreach ($this->$relation as $relation_item) {
                    $relation_item->deleteRecursive();
                }
            } else {
                $this->$relation->deleteRecursive();
            }
        }
        $this->delete();
    }

    /**
     * Implements cascade update of records
     * @param array $relations Aarray of relations to navigate
     */
    public function updateRecursive($relations = array())
    {
        if (count($relations) == 0) {
            $relations = array_keys($this->relations());
        }

        foreach ($relations as $relation) {
            if (is_array($this->$relation)) {
                foreach ($this->$relation as $relation_item) {
                    $relation_item->updateRecursive();
                }
            } else {
                $this->$relation->updateRecursive();
            }
        }
        $this->save();
    }

    /**
     * Override the afterFind method of AR to clone the model and keep the old values in the virtual property oldRecord.
     * @return parent
     */
    public function afterFind()
    {
        $this->oldRecord = clone $this;
        parent::afterFind();
    }

    /**
     * Returns formatted date based on the following spec:
     * When space is a concern the format should be: MM/DD/YYYY at HH:MM[am/pm],
     * when more space is available (such as in this Review Detail view) the
     * format should be: [Month full name] DD, YYYY at HH:MM[am/pm].
     *
     * @param boolean $isShort
     * @return string
     */
    public function getPrettyDateAdded($isShort = true)
    {

        // must use date_added field
        if (!$this->date_added) {
            return '';
        }

        $timestamp = strtotime($this->date_added);

        if ($isShort) {
            $output = date('n/j/Y', $timestamp);
        } else {
            $output = date('F j, Y', $timestamp);
        }

        $output .= ' at ' . date('g:ia', $timestamp);
        return $output;
    }

    /**
     * Generates a user-friendly error message based on all errors encountered
     *
     * Note: this should be inherited for any model-specific error message
     * displaying
     */
    public function getUserFriendlyErrorMessage()
    {

        return 'An error occurred';
    }

    /**
     * Removes special characters from a string parameter.
     * Mimics mysqli_real_escape_string
     * @param string $inp
     * @return string
     */
    public static function sanitizeString($inp)
    {
        if (is_array($inp)) {
            return array_map(__METHOD__, $inp);
        }

        if (!empty($inp) && is_string($inp)) {
            return str_replace(
                array('\\', "\0", "\n", "\r", "'", '"', "\x1a"),
                array('\\\\', '\\0', '\\n', '\\r', "\\'", '\\"', '\\Z'),
                $inp
            );
        }

        return $inp;
    }

    /**
     * Returns a MySQL REPLACE statement for multiple values
     * @param string $search
     * @param array $arrReplace
     * @return string
     */
    public static function replaceMultiple($search, $arrReplace = null)
    {

        //If no array of replacements is suplied, use the default
        if ($arrReplace == null) {
            $arrReplace = array(
                ","  => "",
                "."  => "",
                "-"  => "",
                "\'" => "",
                " "  => "",
                ":"  => "",
                ";"  => "",
                "("  => "",
                ")"  => "",
                "{"  => "",
                "}"  => ""
            );
        }

        $replace = "";
        foreach ($arrReplace as $old => $new) {
            if ($replace == "") {
                $replace = "REPLACE(" . $search . ", \"" . $old . "\", \"" . $new . "\")";
            } else {
                $replace = "REPLACE(" . $replace . ", \"" . $old . "\", \"" . $new . "\")";
            }
        }

        return $replace;
    }
}
