<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "status".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Status".
 *
 * Columns in table "status" available as properties of the model,
 * followed by relations of table "status" available as properties of the model.
 *
 * @property string $id
 * @property string $name
 * @property string $code
 * @property string $description
 * @property string $status_group_id
 *
 * @property ScanError[] $scanErrors
 * @property ScanErrorDetail[] $scanErrorDetails
 * @property StatusGroup $statusGroup
 */
abstract class BaseStatus extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'status';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Status|Statuses', $n);
    }

    public static function representingColumn()
    {
        return 'name';
    }

    public function rules()
    {
        return array(
            array('name, code, status_group_id', 'required'),
            array('name', 'length', 'max'=>100),
            array('code', 'length', 'max'=>45),
            array('status_group_id', 'length', 'max'=>10),
            array('description', 'safe'),
            array('description', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, code, description, status_group_id', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'scanErrors' => array(self::HAS_MANY, 'ScanError', 'status_id'),
            'scanErrorDetails' => array(self::HAS_MANY, 'ScanErrorDetail', 'status_id'),
            'statusGroup' => array(self::BELONGS_TO, 'StatusGroup', 'status_group_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'code' => Yii::t('app', 'Code'),
            'description' => Yii::t('app', 'Description'),
            'status_group_id' => null,
            'scanErrors' => null,
            'scanErrorDetails' => null,
            'statusGroup' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('status_group_id', $this->status_group_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
