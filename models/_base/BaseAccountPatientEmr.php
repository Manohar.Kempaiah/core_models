<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "account_patient_emr".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AccountPatientEmr".
 *
 * Columns in table "account_patient_emr" available as properties of the model,
 * followed by relations of table "account_patient_emr" available as properties of the model.
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $patient_id
 * @property string $emr_patient_id
 * @property string $mi7_patient_id
 *
 * @property AccountEmr $account
 * @property Patient $patient
 */
abstract class BaseAccountPatientEmr extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'account_patient_emr';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'AccountPatientEmr|AccountPatientEmrs', $n);
    }

    public static function representingColumn()
    {
        return 'emr_patient_id';
    }

    public function rules()
    {
        return array(
            array('account_id', 'required'),
            array('account_id, patient_id', 'numerical', 'integerOnly' => true),
            array('emr_patient_id, mi7_patient_id', 'length', 'max' => 25),
            array('patient_id, emr_patient_id, mi7_patient_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, account_id, patient_id, emr_patient_id, mi7_patient_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'account' => array(self::BELONGS_TO, 'AccountEmr', 'account_id'),
            'patient' => array(self::BELONGS_TO, 'Patient', 'patient_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'account_id' => null,
            'patient_id' => null,
            'emr_patient_id' => Yii::t('app', 'Emr Paient'),
            'mi7_patient_id' => Yii::t('app', 'Mi7 Patient'),
            'account' => null,
            'patient' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('patient_id', $this->patient_id);
        $criteria->compare('emr_patient_id', $this->emr_patient_id, true);
        $criteria->compare('mi7_patient_id', $this->mi7_patient_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
