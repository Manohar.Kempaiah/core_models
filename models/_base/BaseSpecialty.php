<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "specialty".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Specialty".
 *
 * Columns in table "specialty" available as properties of the model,
 * followed by relations of table "specialty" available as properties of the model.
 *
 * @property integer $id
 * @property string $name
 * @property string $name_plural
 * @property string $name_ads
 * @property string $api_abreviature
 * @property string $description
 * @property string $friendly_url
 * @property integer $visits_number
 * @property integer $enabled
 *
 * @property SubSpecialty[] $subSpecialties
 */
abstract class BaseSpecialty extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'specialty';
    }

    public static function representingColumn()
    {
        return 'name';
    }

    public function rules()
    {
        return array(
            array('name, name_plural', 'required'),
            array('visits_number, enabled', 'numerical', 'integerOnly' => true),
            array('name, name_plural', 'length', 'max' => 100),
            array('name_ads, friendly_url', 'length', 'max' => 255),
            array('api_abreviature', 'length', 'max' => 5),
            array('description', 'safe'),
            array(
                'name_ads, description, visits_number, level, enabled',
                'default',
                'setOnEmpty' => true,
                'value' => null
            ),
            array('id, name, name_plural, name_ads, api_abreviature, description, friendly_url, visits_number, '
                . 'level, enabled', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'subSpecialties' => array(self::HAS_MANY, 'SubSpecialty', 'specialty_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'name_plural' => Yii::t('app', 'Name Plural'),
            'name_ads' => Yii::t('app', 'Name Ads'),
            'api_abreviature' => Yii::t('app', 'API Abbreviation'),
            'description' => Yii::t('app', 'Description'),
            'friendly_url' => Yii::t('app', 'Friendly Url'),
            'visits_number' => Yii::t('app', 'Visits Number'),
            'enabled' => Yii::t('app', 'Enabled'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('name_plural', $this->name_plural, true);
        $criteria->compare('name_ads', $this->name_ads, true);
        $criteria->compare('api_abreviature', $this->api_abreviature, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('friendly_url', $this->friendly_url, true);
        $criteria->compare('visits_number', $this->visits_number);
        $criteria->compare('enabled', $this->enabled);
        $criteria->compare('level', $this->level);

        return new CActiveDataProvider(
            get_class($this),
            array(
                'criteria' => $criteria,
                'pagination' => array('pageSize' => 100)
            )
        );
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Specialty|Specialtys', $n);
    }

}
