<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "partner_data_feed".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PartnerDataFeed".
 *
 * Columns in table "partner_data_feed" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property string $timestamp
 * @property string $entity_type
 * @property string $data_object
 * @property string $operation
 * @property string $partner_site_id
 * @property string $pre_data_object
 * @property string $origin_query
 * @property integer $provider_id
 * @property integer $practice_id
 * @property integer $account_id
 *
 */
abstract class BasePartnerDataFeed extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'partner_data_feed';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'PartnerDataFeed|PartnerDataFeeds', $n);
    }

    public static function representingColumn()
    {
        return 'timestamp';
    }

    public function rules()
    {
        return array(
            array('timestamp, entity_type, data_object, operation', 'required'),
            array('provider_id, practice_id, account_id', 'numerical', 'integerOnly'=>true),
            array('entity_type, operation', 'length', 'max'=>64),
            array('partner_site_id', 'length', 'max'=>20),
            array('pre_data_object, origin_query', 'safe'),
            array('partner_site_id, pre_data_object, origin_query, provider_id, practice_id, account_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, timestamp, entity_type, data_object, operation, partner_site_id, pre_data_object, origin_query, provider_id, practice_id, account_id', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'timestamp' => Yii::t('app', 'Timestamp'),
            'entity_type' => Yii::t('app', 'Entity Type'),
            'data_object' => Yii::t('app', 'Data Object'),
            'operation' => Yii::t('app', 'Operation'),
            'partner_site_id' => Yii::t('app', 'Partner Site'),
            'pre_data_object' => Yii::t('app', 'Pre Data Object'),
            'origin_query' => Yii::t('app', 'Origin Query'),
            'provider_id' => Yii::t('app', 'Provider'),
            'practice_id' => Yii::t('app', 'Practice'),
            'account_id' => Yii::t('app', 'Account'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('timestamp', $this->timestamp, true);
        $criteria->compare('entity_type', $this->entity_type, true);
        $criteria->compare('data_object', $this->data_object, true);
        $criteria->compare('operation', $this->operation, true);
        $criteria->compare('partner_site_id', $this->partner_site_id, true);
        $criteria->compare('pre_data_object', $this->pre_data_object, true);
        $criteria->compare('origin_query', $this->origin_query, true);
        $criteria->compare('provider_id', $this->provider_id);
        $criteria->compare('practice_id', $this->practice_id);
        $criteria->compare('account_id', $this->account_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
