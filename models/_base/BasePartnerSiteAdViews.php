<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "partner_site_ad_views".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PartnerSiteAdViews".
 *
 * Columns in table "partner_site_ad_views" available as properties of the model,
 * followed by relations of table "partner_site_ad_views" available as properties of the model.
 *
 * @property integer $id
 * @property integer $partner_site_id
 * @property integer $provider_id
 * @property integer $partner_site_ad_id
 * @property integer $patient_id
 * @property string $date_added
 *
 */

abstract class BasePartnerSiteAdViews extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'partner_site_ad_views';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'PartnerSiteAdView|PartnerSiteAdViews', $n);
    }

    public static function representingColumn()
    {
        return 'partner_site_ad_id';
    }

    public function rules()
    {
        return array(
            array('partner_site_id, provider_id, patient_id, partner_site_ad_id', 'required'),
            array('partner_site_id, provider_id, patient_id, partner_site_ad_id', 'numerical', 'integerOnly' => true),
            array('date_added, partner_site_id, provider_id, patient_id, partner_site_ad_id', 'safe'),
            array('date_added, partner_site_id, provider_id, patient_id, partner_site_ad_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('date_added, partner_site_id, provider_id, patient_id, partner_site_ad_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array();
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'partner_site_id' => null,
            'partner_site_ad_id' => null,
            'provider_id' => null,
            'patient_id' => null,
            'date_added' => Yii::t('app', 'Date Added'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('partner_site_id', $this->partner_site_id);
        $criteria->compare('partner_site_ad_id', $this->partner_site_id);
        $criteria->compare('provider_id', $this->partner_site_id);
        $criteria->compare('patient_id', $this->partner_site_id);
        $criteria->compare('date_added', $this->date_added, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
