<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "emr_schedule_exeption_hour".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "EmrScheduleExceptionHour".
 *
 * Columns in table "emr_schedule_exeption_hour" available as properties of the model,
 * followed by relations of table "emr_schedule_exeption_hour" available as properties of the model.
 *
 * @property string $id
 * @property string $emr_schedule_exception_id
 * @property string $time_start
 * @property string $time_end
 * @property string $placer_id
 * @property int $enabled_flag
 * @property int $del_flag
 * @property string $date_added
 * @property string $date_updated
 *
 * @property EmrScheduleException $emrScheduleException
 */
abstract class BaseEmrScheduleExceptionHour extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'emr_schedule_exception_hour';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'EmrScheduleExceptionHour|EmrScheduleExceptionHours', $n);
    }

    public static function representingColumn()
    {
        return 'time_start';
    }

    public function rules()
    {
        return array(
            array('emr_schedule_exception_id', 'required'),
            array('emr_schedule_exception_id', 'length', 'max' => 10),
            array('time_start, time_end', 'safe'),
            array('time_start, time_end', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, emr_schedule_exception_id, time_start, time_end', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'emrScheduleException' => array(self::BELONGS_TO, 'EmrScheduleException', 'emr_schedule_exception_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'emr_schedule_exception_id' => null,
            'time_start' => Yii::t('app', 'Time Start'),
            'time_end' => Yii::t('app', 'Time End'),
            'emrScheduleException' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('emr_schedule_exception_id', $this->emr_schedule_exception_id);
        $criteria->compare('time_start', $this->time_start, true);
        $criteria->compare('time_end', $this->time_end, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
