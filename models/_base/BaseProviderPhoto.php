<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "provider_photo".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProviderPhoto".
 *
 * Columns in table "provider_photo" available as properties of the model,
 * followed by relations of table "provider_photo" available as properties of the model.
 *
 * @property integer $id
 * @property integer $provider_id
 * @property string $photo_path
 * @property string $photo_name
 * @property string $sort_order
 * @property integer $cover
 * @property integer $width
 * @property integer $height
 * @property string $caption
 * @property string $date_added
 * @property Provider $provider
 */
abstract class BaseProviderPhoto extends BaseModel
{

    public $uploadPath;

    public function init()
    {
        parent::init();
        if (
                (isset(Yii::app()->controller) && Yii::app()->controller->module->id == 'api') ||
                php_sapi_name() == "cli"
        ) {
            $this->uploadPath = Yii::getPathOfAlias('api') . '/../providers/' . Yii::app()->params['upload_providers'];
        } else {
            $this->uploadPath = $this->basePath . Yii::app()->params['upload_providers'];
        }
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'provider_photo';
    }

    public static function representingColumn()
    {
        return 'photo_path';
    }

    public function rules()
    {
        return array(
            array('provider_id, photo_path, photo_name', 'required'),
            array('provider_id, cover', 'numerical', 'integerOnly' => true),
            array('photo_path', 'length', 'max' => 255),
            array('photo_path', 'ext.CustomValidators.CheckDirectory', 'uploadPath' => $this->uploadPath),
            array('photo_name, caption', 'length', 'max' => 100),
            array('sort_order', 'length', 'max' => 10),
            array('width, height', 'length', 'max' => 5),
            array('caption', 'length', 'max' => 100),
            array('sort_order, cover, caption, width, height', 'default', 'setOnEmpty' => true, 'value' => null),
            array(
                'id, provider_id, photo_path, photo_name, sort_order, cover, caption, width, height, date_added',
                'safe',
                'on' => 'search'
            ),
        );
    }

    public function relations()
    {
        return array(
            'provider' => array(self::BELONGS_TO, 'Provider', 'provider_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Provider'),
            'photo_path' => Yii::t('app', 'Photo Path'),
            'photo_name' => Yii::t('app', 'Photo Name'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'cover' => Yii::t('app', 'Cover'),
            'width' => Yii::t('app', 'Width'),
            'height' => Yii::t('app', 'Height'),
            'caption' => Yii::t('app', 'Caption'),
            'date_added' => 'Date Added',
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        //$criteria->compare('provider.first_last', $this->provider_id, true);
        //it will search by id or provider.first_last
        if (!intval($this->provider_id) && is_string($this->provider_id) && strlen($this->provider_id) > 0) {
            $criteria->with[] = 'provider';
            $criteria->addSearchCondition("provider.first_last", $this->provider_id);
        } else {
            $criteria->compare('provider_id', $this->provider_id);
        }
        $criteria->compare('t.photo_path', $this->photo_path, true);
        $criteria->compare('photo_name', $this->photo_name, true);
        $criteria->compare('sort_order', $this->sort_order, true);
        $criteria->compare('cover', $this->cover);
        $criteria->compare('width', $this->width);
        $criteria->compare('height', $this->height);
        $criteria->compare('caption', $this->caption, true);
        $criteria->compare('t.date_added', $this->date_added, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100)
        ));
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ProviderPhoto|ProviderPhotos', $n);
    }

}
