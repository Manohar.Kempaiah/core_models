<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "yext_specialty_mapping".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "YextSpecialtyMapping".
 *
 * Columns in table "yext_specialty_mapping" available as properties of the model,
 * followed by relations of table "yext_specialty_mapping" available as properties of the model.
 *
 * @property integer $id
 * @property integer $sub_specialty_id
 * @property integer $yext_category_id
 *
 * @property SubSpecialty $subSpecialty
 */
abstract class BaseYextSpecialtyMapping extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'yext_specialty_mapping';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'YextSpecialtyMapping|YextSpecialtyMappings', $n);
    }

    public static function representingColumn()
    {
        return 'id';
    }

    public function rules()
    {
        return array(
            array('sub_specialty_id, yext_category_id', 'required'),
            array('sub_specialty_id, yext_category_id', 'numerical', 'integerOnly' => true),
            array('id, sub_specialty_id, yext_category_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'subSpecialty' => array(self::BELONGS_TO, 'SubSpecialty', 'sub_specialty_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'sub_specialty_id' => null,
            'yext_category_id' => Yii::t('app', 'Yext Category'),
            'subSpecialty' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('sub_specialty_id', $this->sub_specialty_id);
        $criteria->compare('yext_category_id', $this->yext_category_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
