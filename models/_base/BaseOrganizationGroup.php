<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model class for table "organization_group".
 *
 * The followings are the available columns in table 'organization_group':
 * @property integer $id
 * @property string $name
 * @property string $contact_email
 * @property string $contact_first_name
 * @property string $contact_last_name
 * @property string $contact_address
 * @property string $contact_address_2
 * @property string $contact_phone_number
 * @property string $contact_fax_number
 * @property string $date_added
 * @property string $date_updated
 *
 * The followings are the available model relations:
 * @property Organization[] $organizations
 */
class BaseOrganizationGroup extends BaseModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'organization_group';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('name, contact_email, contact_first_name, contact_last_name', 'required'),
            array('name, contact_last_name, contact_address, contact_address_2', 'length', 'max' => 80),
            array('contact_email', 'length', 'max' => 254),
            array('contact_first_name', 'length', 'max' => 50),
            array('contact_phone_number, contact_fax_number', 'length', 'max' => 14),
            array('date_added, date_updated', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, contact_email, contact_first_name, contact_last_name, contact_address, contact_address_2,'
                . ' contact_phone_number, contact_fax_number, date_added, date_updated', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'organizations' => array(self::HAS_MANY, 'Organization', 'organization_group_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'contact_email' => 'Contact Email',
            'contact_first_name' => 'Contact First Name',
            'contact_last_name' => 'Contact Last Name',
            'contact_address' => 'Contact Address',
            'contact_address_2' => 'Contact Address 2',
            'contact_phone_number' => 'Contact Phone Number',
            'contact_fax_number' => 'Contact Fax Number',
            'date_added' => 'Date Added',
            'date_updated' => 'Date Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('contact_email', $this->contact_email, true);
        $criteria->compare('contact_first_name', $this->contact_first_name, true);
        $criteria->compare('contact_last_name', $this->contact_last_name, true);
        $criteria->compare('contact_address', $this->contact_address, true);
        $criteria->compare('contact_address_2', $this->contact_address_2, true);
        $criteria->compare('contact_phone_number', $this->contact_phone_number, true);
        $criteria->compare('contact_fax_number', $this->contact_fax_number, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BaseOrganizationGroup the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
