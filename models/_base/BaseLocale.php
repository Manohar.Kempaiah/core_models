<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "locale".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Locale".
 *
 * Columns in table "locale" available as properties of the model,
 * followed by relations of table "locale" available as properties of the model.
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 *
 * @property Account[] $accounts
 * @property AccountDetails[] $accountDetails
 * @property AccountDevice[] $accountDevices
 * @property AccountDeviceHistory[] $accountDeviceHistories
 * @property AccountDeviceProvider[] $accountDeviceProviders
 * @property AccountDeviceTranslations[] $accountDeviceTranslations
 * @property Country[] $countries
 * @property Credential[] $credentials
 * @property DeviceHistory[] $deviceHistories
 * @property EducationName[] $educationNames
 * @property Emr[] $emrs
 * @property Hospital[] $hospitals
 * @property Insurance[] $insurances
 * @property InsuranceCompany[] $insuranceCompanies
 * @property Language[] $languages
 * @property ListingType[] $listingTypes
 * @property ManualValidation[] $manualValidations
 * @property Manufacturer[] $manufacturers
 * @property Medication[] $medications
 * @property MedicationManufacturer[] $medicationManufacturers
 * @property Organization[] $organizations
 * @property OrganizationAccount[] $organizationAccounts
 * @property OrganizationFeature[] $organizationFeatures
 * @property OrganizationGroup[] $organizationGroups
 * @property OrganizationHistory[] $organizationHistories
 * @property PartnerSiteProviderFeatured[] $partnerSiteProviderFeatureds
 * @property Practice[] $practices
 * @property PracticeDetails[] $practiceDetails
 * @property PracticeGroup[] $practiceGroups
 * @property PracticeLanguage[] $practiceLanguages
 * @property PracticeLatlongHistory[] $practiceLatlongHistories
 * @property PracticeOfficeHour[] $practiceOfficeHours
 * @property PracticePhoto[] $practicePhotos
 * @property PracticeServiceType[] $practiceServiceTypes
 * @property PracticeSpecialty[] $practiceSpecialties
 * @property PracticeTreatment[] $practiceTreatments
 * @property PracticeVideo[] $practiceVideos
 * @property Provider[] $providers
 * @property ProviderAward[] $providerAwards
 * @property ProviderCertification[] $providerCertifications
 * @property ProviderDetails[] $providerDetails
 * @property ProviderEducation[] $providerEducations
 * @property ProviderExperience[] $providerExperiences
 * @property ProviderForm[] $providerForms
 * @property ProviderGa[] $providerGas
 * @property ProviderHospital[] $providerHospitals
 * @property ProviderInsurance[] $providerInsurances
 * @property ProviderLanguage[] $providerLanguages
 * @property ProviderMembership[] $providerMemberships
 * @property ProviderPaymentType[] $providerPaymentTypes
 * @property ProviderPhoto[] $providerPhotos
 * @property ProviderPopulation[] $providerPopulations
 * @property ProviderPractice[] $providerPractices
 * @property ProviderPracticeInsurance[] $providerPracticeInsurances
 * @property ProviderPracticeListing[] $providerPracticeListings
 * @property ProviderPracticeSchedule[] $providerPracticeSchedules
 * @property ProviderPracticeSpecialty[] $providerPracticeSpecialties
 * @property ProviderPracticeWidget[] $providerPracticeWidgets
 * @property ProviderPublication[] $providerPublications
 * @property ProviderRating[] $providerRatings
 * @property ProviderRatingAbuse[] $providerRatingAbuses
 * @property ProviderRatingResponse[] $providerRatingResponses
 * @property ProviderRatingWidget[] $providerRatingWidgets
 * @property ProviderResidency[] $providerResidencies
 * @property ProviderSchedule[] $providerSchedules
 * @property ProviderSpecialty[] $providerSpecialties
 * @property ProviderStats[] $providerStats
 * @property ProviderSuspended[] $providerSuspendeds
 * @property ProviderSuspendedNpi[] $providerSuspendedNpis
 * @property ProviderTreatment[] $providerTreatments
 * @property ProviderTreatmentPerformed[] $providerTreatmentPerformeds
 * @property ProviderUrlHistory[] $providerUrlHistories
 * @property ProviderVideo[] $providerVideos
 * @property QualityPath[] $qualityPaths
 * @property SchedulingMail[] $schedulingMails
 * @property Treatment[] $treatments
 * @property TreatmentFriendly[] $treatmentFriendlies
 * @property TreatmentFriendlyTreatment[] $treatmentFriendlyTreatments
 * @property Widget[] $widgets
 * @property WidgetUrl[] $widgetUrls
 */
abstract class BaseLocale extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'locale';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Locale|Locales', $n);
    }

    public static function representingColumn()
    {
        return 'code';
    }

    public function rules()
    {
        return array(
            array('code, name', 'required'),
            array('code', 'length', 'max' => 5),
            array('name', 'length', 'max' => 50),
            array('id, code, name', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'accounts' => array(self::HAS_MANY, 'Account', 'locale_id'),
            'accountDetails' => array(self::HAS_MANY, 'AccountDetails', 'locale_id'),
            'accountDevices' => array(self::HAS_MANY, 'AccountDevice', 'locale_id'),
            'accountDeviceHistories' => array(self::HAS_MANY, 'AccountDeviceHistory', 'locale_id'),
            'accountDeviceProviders' => array(self::HAS_MANY, 'AccountDeviceProvider', 'locale_id'),
            'accountDeviceTranslations' => array(self::HAS_MANY, 'AccountDeviceTranslations', 'locale_id'),
            'countries' => array(self::HAS_MANY, 'Country', 'default_locale_id'),
            'credentials' => array(self::HAS_MANY, 'Credential', 'locale_id'),
            'deviceHistories' => array(self::HAS_MANY, 'DeviceHistory', 'locale_id'),
            'educationNames' => array(self::HAS_MANY, 'EducationName', 'locale_id'),
            'emrs' => array(self::HAS_MANY, 'Emr', 'locale_id'),
            'hospitals' => array(self::HAS_MANY, 'Hospital', 'locale_id'),
            'insurances' => array(self::HAS_MANY, 'Insurance', 'locale_id'),
            'insuranceCompanies' => array(self::HAS_MANY, 'InsuranceCompany', 'locale_id'),
            'languages' => array(self::HAS_MANY, 'Language', 'locale_id'),
            'listingTypes' => array(self::HAS_MANY, 'ListingType', 'locale_id'),
            'manualValidations' => array(self::HAS_MANY, 'ManualValidation', 'locale_id'),
            'manufacturers' => array(self::HAS_MANY, 'Manufacturer', 'locale_id'),
            'medications' => array(self::HAS_MANY, 'Medication', 'locale_id'),
            'medicationManufacturers' => array(self::HAS_MANY, 'MedicationManufacturer', 'locale_id'),
            'organizations' => array(self::HAS_MANY, 'Organization', 'locale_id'),
            'organizationAccounts' => array(self::HAS_MANY, 'OrganizationAccount', 'locale_id'),
            'organizationFeatures' => array(self::HAS_MANY, 'OrganizationFeature', 'locale_id'),
            'organizationGroups' => array(self::HAS_MANY, 'OrganizationGroup', 'locale_id'),
            'organizationHistories' => array(self::HAS_MANY, 'OrganizationHistory', 'locale_id'),
            'partnerSiteProviderFeatureds' => array(self::HAS_MANY, 'PartnerSiteProviderFeatured', 'locale_id'),
            'practices' => array(self::HAS_MANY, 'Practice', 'locale_id'),
            'practiceDetails' => array(self::HAS_MANY, 'PracticeDetails', 'locale_id'),
            'practiceGroups' => array(self::HAS_MANY, 'PracticeGroup', 'locale_id'),
            'practiceLanguages' => array(self::HAS_MANY, 'PracticeLanguage', 'locale_id'),
            'practiceLatlongHistories' => array(self::HAS_MANY, 'PracticeLatlongHistory', 'locale_id'),
            'practiceOfficeHours' => array(self::HAS_MANY, 'PracticeOfficeHour', 'locale_id'),
            'practicePhotos' => array(self::HAS_MANY, 'PracticePhoto', 'locale_id'),
            'practiceServiceTypes' => array(self::HAS_MANY, 'PracticeServiceType', 'locale_id'),
            'practiceSpecialties' => array(self::HAS_MANY, 'PracticeSpecialty', 'locale_id'),
            'practiceTreatments' => array(self::HAS_MANY, 'PracticeTreatment', 'locale_id'),
            'practiceVideos' => array(self::HAS_MANY, 'PracticeVideo', 'locale_id'),
            'providers' => array(self::HAS_MANY, 'Provider', 'locale_id'),
            'providerAwards' => array(self::HAS_MANY, 'ProviderAward', 'locale_id'),
            'providerCertifications' => array(self::HAS_MANY, 'ProviderCertification', 'locale_id'),
            'providerDetails' => array(self::HAS_MANY, 'ProviderDetails', 'locale_id'),
            'providerEducations' => array(self::HAS_MANY, 'ProviderEducation', 'locale_id'),
            'providerExperiences' => array(self::HAS_MANY, 'ProviderExperience', 'locale_id'),
            'providerForms' => array(self::HAS_MANY, 'ProviderForm', 'locale_id'),
            'providerGas' => array(self::HAS_MANY, 'ProviderGa', 'locale_id'),
            'providerHospitals' => array(self::HAS_MANY, 'ProviderHospital', 'locale_id'),
            'providerInsurances' => array(self::HAS_MANY, 'ProviderInsurance', 'locale_id'),
            'providerLanguages' => array(self::HAS_MANY, 'ProviderLanguage', 'locale_id'),
            'providerMemberships' => array(self::HAS_MANY, 'ProviderMembership', 'locale_id'),
            'providerPaymentTypes' => array(self::HAS_MANY, 'ProviderPaymentType', 'locale_id'),
            'providerPhotos' => array(self::HAS_MANY, 'ProviderPhoto', 'locale_id'),
            'providerPopulations' => array(self::HAS_MANY, 'ProviderPopulation', 'locale_id'),
            'providerPractices' => array(self::HAS_MANY, 'ProviderPractice', 'locale_id'),
            'providerPracticeInsurances' => array(self::HAS_MANY, 'ProviderPracticeInsurance', 'locale_id'),
            'providerPracticeListings' => array(self::HAS_MANY, 'ProviderPracticeListing', 'locale_id'),
            'providerPracticeSchedules' => array(self::HAS_MANY, 'ProviderPracticeSchedule', 'locale_id'),
            'providerPracticeSpecialties' => array(self::HAS_MANY, 'ProviderPracticeSpecialty', 'locale_id'),
            'providerPracticeWidgets' => array(self::HAS_MANY, 'ProviderPracticeWidget', 'locale_id'),
            'providerPublications' => array(self::HAS_MANY, 'ProviderPublication', 'locale_id'),
            'providerRatings' => array(self::HAS_MANY, 'ProviderRating', 'locale_id'),
            'providerRatingAbuses' => array(self::HAS_MANY, 'ProviderRatingAbuse', 'locale_id'),
            'providerRatingResponses' => array(self::HAS_MANY, 'ProviderRatingResponse', 'locale_id'),
            'providerRatingWidgets' => array(self::HAS_MANY, 'ProviderRatingWidget', 'locale_id'),
            'providerResidencies' => array(self::HAS_MANY, 'ProviderResidency', 'locale_id'),
            'providerSchedules' => array(self::HAS_MANY, 'ProviderSchedule', 'locale_id'),
            'providerSpecialties' => array(self::HAS_MANY, 'ProviderSpecialty', 'locale_id'),
            'providerStats' => array(self::HAS_MANY, 'ProviderStats', 'locale_id'),
            'providerSuspendeds' => array(self::HAS_MANY, 'ProviderSuspended', 'locale_id'),
            'providerSuspendedNpis' => array(self::HAS_MANY, 'ProviderSuspendedNpi', 'locale_id'),
            'providerTreatments' => array(self::HAS_MANY, 'ProviderTreatment', 'locale_id'),
            'providerTreatmentPerformeds' => array(self::HAS_MANY, 'ProviderTreatmentPerformed', 'locale_id'),
            'providerUrlHistories' => array(self::HAS_MANY, 'ProviderUrlHistory', 'locale_id'),
            'providerVideos' => array(self::HAS_MANY, 'ProviderVideo', 'locale_id'),
            'qualityPaths' => array(self::HAS_MANY, 'QualityPath', 'locale_id'),
            'schedulingMails' => array(self::HAS_MANY, 'SchedulingMail', 'locale_id'),
            'treatments' => array(self::HAS_MANY, 'Treatment', 'locale_id'),
            'treatmentFriendlies' => array(self::HAS_MANY, 'TreatmentFriendly', 'locale_id'),
            'treatmentFriendlyTreatments' => array(self::HAS_MANY, 'TreatmentFriendlyTreatment', 'locale_id'),
            'widgets' => array(self::HAS_MANY, 'Widget', 'locale_id'),
            'widgetUrls' => array(self::HAS_MANY, 'WidgetUrl', 'locale_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'accounts' => null,
            'accountDetails' => null,
            'accountDevices' => null,
            'accountDeviceHistories' => null,
            'accountDeviceProviders' => null,
            'accountDeviceTranslations' => null,
            'countries' => null,
            'credentials' => null,
            'deviceHistories' => null,
            'educationNames' => null,
            'emrs' => null,
            'hospitals' => null,
            'insurances' => null,
            'insuranceCompanies' => null,
            'languages' => null,
            'listingTypes' => null,
            'manualValidations' => null,
            'manufacturers' => null,
            'medications' => null,
            'medicationManufacturers' => null,
            'organizations' => null,
            'organizationAccounts' => null,
            'organizationFeatures' => null,
            'organizationGroups' => null,
            'organizationHistories' => null,
            'partnerSiteProviderFeatureds' => null,
            'practices' => null,
            'practiceDetails' => null,
            'practiceGroups' => null,
            'practiceLanguages' => null,
            'practiceLatlongHistories' => null,
            'practiceOfficeHours' => null,
            'practicePhotos' => null,
            'practiceServiceTypes' => null,
            'practiceSpecialties' => null,
            'practiceTreatments' => null,
            'practiceVideos' => null,
            'providers' => null,
            'providerAwards' => null,
            'providerCertifications' => null,
            'providerDetails' => null,
            'providerEducations' => null,
            'providerExperiences' => null,
            'providerForms' => null,
            'providerGas' => null,
            'providerHospitals' => null,
            'providerInsurances' => null,
            'providerLanguages' => null,
            'providerMemberships' => null,
            'providerPaymentTypes' => null,
            'providerPhotos' => null,
            'providerPopulations' => null,
            'providerPractices' => null,
            'providerPracticeInsurances' => null,
            'providerPracticeListings' => null,
            'providerPracticeSchedules' => null,
            'providerPracticeSpecialties' => null,
            'providerPracticeWidgets' => null,
            'providerPublications' => null,
            'providerRatings' => null,
            'providerRatingAbuses' => null,
            'providerRatingResponses' => null,
            'providerRatingWidgets' => null,
            'providerResidencies' => null,
            'providerSchedules' => null,
            'providerSpecialties' => null,
            'providerStats' => null,
            'providerSuspendeds' => null,
            'providerSuspendedNpis' => null,
            'providerTreatments' => null,
            'providerTreatmentPerformeds' => null,
            'providerUrlHistories' => null,
            'providerVideos' => null,
            'qualityPaths' => null,
            'schedulingMails' => null,
            'treatments' => null,
            'treatmentFriendlies' => null,
            'treatmentFriendlyTreatments' => null,
            'widgets' => null,
            'widgetUrls' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
