<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "scan_error_detail".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ScanErrorDetail".
 *
 * Columns in table "scan_error_detail" available as properties of the model,
 * followed by relations of table "scan_error_detail" available as properties of the model.
 *
 * @property string $id
 * @property integer $scan_error_id
 * @property string $type_id
 * @property string $field_type_id
 *
 * @property Type $fieldType
 * @property ScanError $scanError
 * @property Type $type
 * @property ScanErrorLog[] $scanErrorLogs
 * @property ScanErrorTask[] $scanErrorTasks
 */
abstract class BaseScanErrorDetail extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'scan_error_detail';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ScanErrorDetail|ScanErrorDetails', $n);
    }

    public static function representingColumn()
    {
        return 'id';
    }

    public function rules()
    {
        return array(
            array('id, scan_error_id', 'required'),
            array('scan_error_id', 'numerical', 'integerOnly'=>true),
            array('id, type_id, field_type_id', 'length', 'max'=>10),
            array('type_id, field_type_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, scan_error_id, type_id, field_type_id', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'fieldType' => array(self::BELONGS_TO, 'Type', 'field_type_id'),
            'scanError' => array(self::BELONGS_TO, 'ScanError', 'scan_error_id'),
            'type' => array(self::BELONGS_TO, 'Type', 'type_id'),
            'scanErrorLogs' => array(self::HAS_MANY, 'ScanErrorLog', 'scan_error_detail_id'),
            'scanErrorTasks' => array(self::HAS_MANY, 'ScanErrorTask', 'scan_error_detail_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'scan_error_id' => null,
            'type_id' => null,
            'field_type_id' => null,
            'fieldType' => null,
            'scanError' => null,
            'type' => null,
            'scanErrorLogs' => null,
            'scanErrorTasks' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('scan_error_id', $this->scan_error_id);
        $criteria->compare('type_id', $this->type_id);
        $criteria->compare('field_type_id', $this->field_type_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
