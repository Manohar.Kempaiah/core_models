<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "cpn_installation_location".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CpnInstallationLocation".
 *
 * Columns in table "cpn_installation_location" available as properties of the model,
 * followed by relations of table "cpn_installation_location" available as properties of the model.
 *
 * @property integer $id
 * @property integer $cpn_installation_id
 * @property string $ehr_location_id
 * @property string $ehr_location_data
 * @property string $date_added
 * @property integer $added_by_account_id
 * @property string $date_updated
 * @property integer $updated_by_account_id
 *
 * @property Account $addedByAccount
 * @property CpnInstallation $cpnInstallation
 */
abstract class BaseCpnInstallationLocation extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'cpn_installation_location';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'CpnInstallationLocation|CpnInstallationLocations', $n);
    }

    public static function representingColumn()
    {
        return 'ehr_location_id';
    }

    public function rules()
    {
        return array(
            array('cpn_installation_id, ehr_location_id, ehr_location_data, added_by_account_id', 'required'),
            array('cpn_installation_id, added_by_account_id, updated_by_account_id', 'numerical', 'integerOnly' => true),
            array('ehr_location_id', 'length', 'max' => 25),
            array('ehr_location_data', 'length', 'max' => 254),
            array('date_added, date_updated', 'safe'),
            array('date_added, date_updated, updated_by_account_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, cpn_installation_id, ehr_location_id, ehr_location_data, date_added, added_by_account_id, date_updated, updated_by_account_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'addedByAccount' => array(self::BELONGS_TO, 'Account', 'added_by_account_id'),
            'cpnInstallation' => array(self::BELONGS_TO, 'CpnInstallation', 'cpn_installation_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'cpn_installation_id' => null,
            'ehr_location_id' => Yii::t('app', 'Ehr Location'),
            'ehr_location_data' => Yii::t('app', 'Ehr Location Data'),
            'date_added' => Yii::t('app', 'Date Added'),
            'added_by_account_id' => null,
            'date_updated' => Yii::t('app', 'Date Updated'),
            'updated_by_account_id' => Yii::t('app', 'Updated By Account'),
            'addedByAccount' => null,
            'cpnInstallation' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('cpn_installation_id', $this->cpn_installation_id);
        $criteria->compare('ehr_location_id', $this->ehr_location_id, true);
        $criteria->compare('ehr_location_data', $this->ehr_location_data, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('added_by_account_id', $this->added_by_account_id);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('updated_by_account_id', $this->updated_by_account_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
