<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "gmb_category".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "GmbCategory".
 *
 * Columns in table "gmb_category" available as properties of the model.
 *
 * @property integer $id
 * @property integer $gcid
 * @property string $name
 */
abstract class BaseGmbCategory extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'gmb_category';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'GmbCategory|GmbCategory', $n);
    }

    public function rules()
    {
        return array(
            array('gcid,name', 'required'),
            array('id', 'safe','on'=>'search')
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'gcid' => Yii::t('app', 'GCID'),
            'name' => Yii::t('app', 'GMB Category')
        );
    }

    /**
     * Filters the GMB Category
     * @return \CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('gcid', $this->gcid, true);
        $criteria->compare('name', $this->name, true);
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array(
                        'pageSize' => 100,
                    )
        ));
    }

}
