<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "credential".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Credential".
 *
 * Columns in table "credential" available as properties of the model,
 * followed by relations of table "credential" available as properties of the model.
 *
 * @property integer $id
 * @property integer $localization_id
 * @property string $title
 * @property string $refered_as
 * @property string $definition
 *
 * @property Localization $localization
 * @property Provider[] $providers
 */
abstract class BaseCredential extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'credential';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Credential|Credentials', $n);
    }

    public static function representingColumn()
    {
        return 'title';
    }

    public function rules()
    {
        return array(
            array('title', 'required'),
            array('localization_id', 'numerical', 'integerOnly' => true),
            array('title, refered_as, definition', 'length', 'max' => 50),
            array('localization_id, refered_as, definition', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, localization_id, title, refered_as, definition', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'localization' => array(self::BELONGS_TO, 'Localization', 'localization_id'),
            'providers' => array(self::HAS_MANY, 'Provider', 'credential_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'localization_id' => null,
            'title' => Yii::t('app', 'Title'),
            'refered_as' => Yii::t('app', 'Refered As'),
            'definition' => Yii::t('app', 'Definition'),
            'localization' => null,
            'providers' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('localization_id', $this->localization_id);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('refered_as', $this->refered_as, true);
        $criteria->compare('definition', $this->definition, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
