<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "research_mode_log".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ResearchModeLog".
 *
 * Columns in table "research_mode_log" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property integer $owner_account_id
 * @property integer $provider_id
 * @property integer $practice_id
 * @property string $prev_status
 * @property string $new_status
 * @property string $note
 * @property integer $user_account_id
 * @property string $date_added
 * @property integer $note_read_by_account_id
 * @property string $note_read_date
 *
 */
abstract class BaseResearchModeLog extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'research_mode_log';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ResearchModeLog|ResearchModeLogs', $n);
    }

    public static function representingColumn()
    {
        return 'prev_status';
    }

    public function rules()
    {
        return array(
            array('owner_account_id', 'required'),
            array('owner_account_id, provider_id, practice_id, user_account_id, note_read_by_account_id', 'numerical', 'integerOnly' => true),
            array('prev_status, new_status', 'length', 'max' => 16),
            array('note, date_added, note_read_date', 'safe'),
            array('provider_id, practice_id, note, section, user_account_id, date_added, note_read_by_account_id, note_read_date, prev_status, new_status', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, owner_account_id, provider_id, practice_id, prev_status, new_status, note, user_account_id, date_added, note_read_by_account_id, note_read_date', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'owner_account_id' => Yii::t('app', 'Owner Account'),
            'provider_id' => Yii::t('app', 'Provider'),
            'practice_id' => Yii::t('app', 'Practice'),
            'prev_status' => Yii::t('app', 'Prev Status'),
            'new_status' => Yii::t('app', 'New Status'),
            'note' => Yii::t('app', 'Note'),
            'user_account_id' => Yii::t('app', 'User Account'),
            'date_added' => Yii::t('app', 'Date Added'),
            'note_read_by_account_id' => Yii::t('app', 'Note Read By Account'),
            'note_read_date' => Yii::t('app', 'Note Read Date'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('owner_account_id', $this->owner_account_id);
        $criteria->compare('provider_id', $this->provider_id);
        $criteria->compare('practice_id', $this->practice_id);
        $criteria->compare('prev_status', $this->prev_status, true);
        $criteria->compare('new_status', $this->new_status, true);
        $criteria->compare('note', $this->note, true);
        $criteria->compare('user_account_id', $this->user_account_id);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('note_read_by_account_id', $this->note_read_by_account_id);
        $criteria->compare('note_read_date', $this->note_read_date, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
