<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "cpn_software_installation".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CpnSoftwareInstallation".
 *
 * Columns in table "cpn_software_installation" available as properties of the model,
 * followed by relations of table "cpn_software_installation" available as properties of the model.
 *
 * @property integer $id
 * @property string $guid
 * @property integer $cpn_software_id
 * @property integer $cpn_installation_id
 * @property integer $install_software_flag
 * @property integer $requested_install_id
 * @property string $date_to_install
 * @property integer $installed_flag
 * @property string $date_installed
 * @property string $ip_address
 * @property integer $seconds_to_install
 * @property integer $uninstall_software_flag
 * @property integer $requested_uninstall_id
 * @property string $date_to_uninstall
 * @property integer $uninstalled_flag
 * @property string $date_uninstalled
 * @property integer $seconds_to_uninstall
 * @property string $last_heartbeat
 * @property integer $enabled_flag
 * @property integer $del_flag
 * @property string $date_added
 * @property integer $added_by_account_id
 * @property string $date_updated
 * @property integer $updated_by_account_id
 *
 * @property Account $addedByAccount
 * @property CpnInstallation $cpnInstallation
 * @property CpnSoftware $cpnSoftware
 * @property Account $updatedByAccount
 * @property CpnSoftwareVersion[] $cpnSoftwareVersions
 */
abstract class BaseCpnSoftwareInstallation extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'cpn_software_installation';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'CpnSoftwareInstallation|CpnSoftwareInstallations', $n);
    }

    public static function representingColumn()
    {
        return 'date_to_install';
    }

    public function rules()
    {
        return array(
            array('cpn_software_id, cpn_installation_id, added_by_account_id', 'required'),
            array('cpn_software_id, cpn_installation_id, install_software_flag, requested_install_id, '
                . 'installed_flag, seconds_to_install, uninstall_software_flag, requested_uninstall_id, '
                . 'uninstalled_flag, seconds_to_uninstall, enabled_flag, del_flag, added_by_account_id, '
                . 'updated_by_account_id', 'numerical', 'integerOnly' => true),
            array('guid', 'length', 'max' => 255),
            array('ip_address', 'length', 'max' => 15),
            array('date_to_install, date_installed, date_to_uninstall, date_uninstalled, last_heartbeat, '
                . 'date_added, date_updated', 'safe'),
            array('guid, install_software_flag, requested_install_id, date_to_install, installed_flag, '
                . 'date_installed, ip_address, seconds_to_install, uninstall_software_flag, requested_uninstall_id, '
                . 'date_to_uninstall, uninstalled_flag, date_uninstalled, seconds_to_uninstall, last_heartbeat, '
                . 'enabled_flag, del_flag, date_added, date_updated, updated_by_account_id', 'default',
                'setOnEmpty' => true, 'value' => null),
            array('id, guid, cpn_software_id, cpn_installation_id, install_software_flag, requested_install_id, '
                . 'date_to_install, installed_flag, date_installed, ip_address, seconds_to_install, '
                . 'uninstall_software_flag, requested_uninstall_id, date_to_uninstall, uninstalled_flag, '
                . 'date_uninstalled, seconds_to_uninstall, last_heartbeat, enabled_flag, del_flag, date_added, '
                . 'added_by_account_id, date_updated, updated_by_account_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'addedByAccount' => array(self::BELONGS_TO, 'Account', 'added_by_account_id'),
            'cpnInstallation' => array(self::BELONGS_TO, 'CpnInstallation', 'cpn_installation_id'),
            'cpnSoftware' => array(self::BELONGS_TO, 'CpnSoftware', 'cpn_software_id'),
            'updatedByAccount' => array(self::BELONGS_TO, 'Account', 'updated_by_account_id'),
            'cpnSoftwareVersions' => array(self::HAS_MANY, 'CpnSoftwareVersion', 'cpn_software_installation_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'guid' => Yii::t('app', 'Guid'),
            'cpn_software_id' => null,
            'cpn_installation_id' => null,
            'install_software_flag' => Yii::t('app', 'Install Software Flag'),
            'requested_install_id' => Yii::t('app', 'Requested Install'),
            'date_to_install' => Yii::t('app', 'Date To Install'),
            'installed_flag' => Yii::t('app', 'Installed Flag'),
            'date_installed' => Yii::t('app', 'Date Installed'),
            'ip_address' => Yii::t('app', 'Ip Address'),
            'seconds_to_install' => Yii::t('app', 'Seconds To Install'),
            'uninstall_software_flag' => Yii::t('app', 'Uninstall Software Flag'),
            'requested_uninstall_id' => Yii::t('app', 'Requested Uninstall'),
            'date_to_uninstall' => Yii::t('app', 'Date To Uninstall'),
            'uninstalled_flag' => Yii::t('app', 'Uninstalled Flag'),
            'date_uninstalled' => Yii::t('app', 'Date Uninstalled'),
            'seconds_to_uninstall' => Yii::t('app', 'Seconds To Uninstall'),
            'last_heartbeat' => Yii::t('app', 'Last Heartbeat'),
            'enabled_flag' => Yii::t('app', 'Enabled Flag'),
            'del_flag' => Yii::t('app', 'Del Flag'),
            'date_added' => Yii::t('app', 'Date Added'),
            'added_by_account_id' => null,
            'date_updated' => Yii::t('app', 'Date Updated'),
            'updated_by_account_id' => null,
            'addedByAccount' => null,
            'cpnInstallation' => null,
            'cpnSoftware' => null,
            'updatedByAccount' => null,
            'cpnSoftwareVersions' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('guid', $this->guid, true);
        $criteria->compare('cpn_software_id', $this->cpn_software_id);
        $criteria->compare('cpn_installation_id', $this->cpn_installation_id);
        $criteria->compare('install_software_flag', $this->install_software_flag);
        $criteria->compare('requested_install_id', $this->requested_install_id);
        $criteria->compare('date_to_install', $this->date_to_install, true);
        $criteria->compare('installed_flag', $this->installed_flag);
        $criteria->compare('date_installed', $this->date_installed, true);
        $criteria->compare('ip_address', $this->ip_address, true);
        $criteria->compare('seconds_to_install', $this->seconds_to_install);
        $criteria->compare('uninstall_software_flag', $this->uninstall_software_flag);
        $criteria->compare('requested_uninstall_id', $this->requested_uninstall_id);
        $criteria->compare('date_to_uninstall', $this->date_to_uninstall, true);
        $criteria->compare('uninstalled_flag', $this->uninstalled_flag);
        $criteria->compare('date_uninstalled', $this->date_uninstalled, true);
        $criteria->compare('seconds_to_uninstall', $this->seconds_to_uninstall);
        $criteria->compare('last_heartbeat', $this->last_heartbeat, true);
        $criteria->compare('enabled_flag', $this->enabled_flag);
        $criteria->compare('del_flag', $this->del_flag);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('added_by_account_id', $this->added_by_account_id);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('updated_by_account_id', $this->updated_by_account_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
