<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "scan_error_type".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ScanErrorType".
 *
 * Columns in table "scan_error_type" available as properties of the model,
 * followed by relations of table "scan_error_type" available as properties of the model.
 *
 * @property integer $id
 * @property string $name
 *
 * @property ScanError[] $scanErrors
 */
abstract class BaseScanErrorType extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'scan_error_type';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ScanErrorType|ScanErrorTypes', $n);
    }

    public static function representingColumn()
    {
        return 'name';
    }

    public function rules()
    {
        return array(
            array('name', 'length', 'max' => 40),
            array('name', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'scanErrors' => array(self::HAS_MANY, 'ScanError', 'scan_error_type_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'scanErrors' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
