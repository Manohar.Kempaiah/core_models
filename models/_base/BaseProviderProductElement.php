<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "provider_product_element".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProviderProductElement".
 *
 * Columns in table "provider_product_element" available as properties of the model,
 * and there are no model relations.
 *
 * @property integer $id
 * @property integer $provider_id
 * @property integer $product_element_id
 * @property integer $data_source_id
 * @property integer $confidence_score
 * @property string $last_date_updated
 * @property string $snapshot_date_added
 *
 */
abstract class BaseProviderProductElement extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'provider_product_element';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ProviderProductElement|ProviderProductElements', $n);
    }

    public static function representingColumn()
    {
        return 'last_date_updated';
    }

    public function rules()
    {
        return array(
            array('product_element_id, data_source_id, confidence_score', 'required'),
            array('provider_id, product_element_id, data_source_id, confidence_score', 'numerical', 'integerOnly'=>true),
            array('last_date_updated, snapshot_date_added', 'safe'),
            array('provider_id, last_date_updated, snapshot_date_added', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, provider_id, product_element_id, data_source_id, confidence_score, last_date_updated, snapshot_date_added', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Provider'),
            'product_element_id' => Yii::t('app', 'Product Element'),
            'data_source_id' => Yii::t('app', 'Data Source'),
            'confidence_score' => Yii::t('app', 'Confidence Score'),
            'last_date_updated' => Yii::t('app', 'Last Date Updated'),
            'snapshot_date_added' => Yii::t('app', 'Snapshot Date Added'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('provider_id', $this->provider_id);
        $criteria->compare('product_element_id', $this->product_element_id);
        $criteria->compare('data_source_id', $this->data_source_id);
        $criteria->compare('confidence_score', $this->confidence_score);
        $criteria->compare('last_date_updated', $this->last_date_updated, true);
        $criteria->compare('snapshot_date_added', $this->snapshot_date_added, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
