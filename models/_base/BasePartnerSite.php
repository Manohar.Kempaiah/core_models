<?php

Yii::import('application.modules.core_models.models._base.BasePasswordModel');

/**
 * This is the model base class for the table "partner_site".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PartnerSite".
 *
 * Columns in table "partner_site" available as properties of the model,
 * followed by relations of table "partner_site" available as properties of the model.
 *
 * @property integer $id
 * @property integer $partner_id
 * @property integer $listing_site_id
 * @property integer $site_id
 * @property integer $pdi_partner
 * @property integer $active
 * @property integer $set_providers_featured
 * @property integer $set_book_appointment
 * @property integer $shares_reviews
 * @property integer $set_review
 * @property string $update_method
 * @property string $dumpfile_frequency
 * @property string $output_encoding
 * @property string $engine
 * @property integer $strict_type
 * @property integer $salesforce_sync
 * @property integer $sort_order
 * @property string $name
 * @property string $display_name
 * @property string $alias_name
 * @property string $url
 * @property string $felix_url
 * @property integer $any_localization
 * @property string $email
 * @property string $secret
 * @property string $last_password_change
 * @property integer $see_general_entries
 * @property integer $listing_type_id
 * @property string $custom_provider
 * @property string $custom_provider_featured
 * @property string $custom_provider_export
 * @property string $custom_practice_export
 * @property string $custom_provider_non_export
 * @property string $internal_notes
 * @property integer $base_network
 * @property integer $partner_site_alias_id
 * @property string $logo_path
 * @property integer $scan_template_type_id
 * @property integer $is_channel_partner
 * @property string $salesforce_id
 * @property string $force_twilio_enabled
 * @property string $server_alias
 *
 * @property Account[] $accounts
 * @property Oauth2Clients[] $oauth2Clients
 * @property Organization[] $organizations
 * @property ScanTemplateType $scanTemplateType
 * @property ListingType $listingType
 * @property Partner $partner
 * @property PartnerSiteEntity[] $partnerSiteEntities
 * @property PartnerSiteEntityMatch[] $partnerSiteEntityMatches
 * @property PartnerSiteParams[] $partnerSiteParams
 * @property PartnerSitePdiTable[] $partnerSitePdiTables
 * @property PartnerSiteProviderFeatured[] $partnerSiteProviderFeatureds
 * @property PartnerSiteQueue[] $partnerSiteQueues
 * @property PartnerSiteReviewResponse[] $partnerSiteReviewResponses
 * @property ProviderRatingImportLog[] $providerRatingImportLogs
 * @property ScanRequest[] $scanRequests
 */
abstract class BasePartnerSite extends BasePasswordModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'partner_site';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'PartnerSite|PartnerSites', $n);
    }

    public static function representingColumn()
    {
        return 'name';
    }

    /**
     * Rules
     */
    public function rules()
    {
        return array(
            array('partner_id, name, display_name', 'required'),
            array(
                'partner_id, listing_site_id, site_id, pdi_partner, active, set_providers_featured, '
                    . 'set_book_appointment, shares_reviews, set_review, strict_type, salesforce_sync, sort_order, '
                    . 'any_localization, see_general_entries, listing_type_id, base_network, partner_site_alias_id, '
                    . 'scan_template_type_id, is_channel_partner',
                'numerical',
                'integerOnly' => true
            ),
            array('update_method, dumpfile_frequency', 'length', 'max' => 11),
            array('output_encoding', 'length', 'max' => 8),
            array('name', 'length', 'max' => 45),
            array('display_name', 'length', 'max' => 127),
            array('server_alias', 'length', 'max' => 127),
            array('alias_name, url, felix_url, email, logo_path', 'length', 'max' => 255),
            array('secret', 'length', 'max' => 60),
            array('salesforce_id', 'length', 'max' => 18),
            array('last_password_change, custom_provider, custom_provider_featured, custom_provider_export, custom_practice_export, '
                . 'custom_provider_non_export, internal_notes', 'safe'),
            array('listing_site_id, site_id, pdi_partner, active, set_providers_featured, set_book_appointment, '
                . 'shares_reviews, set_review, update_method, dumpfile_frequency, output_encoding, strict_type, '
                . 'salesforce_sync, sort_order, alias_name, felix_url, any_localization, email, secret, '
                . 'see_general_entries, listing_type_id, custom_provider, custom_provider_featured, '
                . 'custom_provider_export, custom_practice_export, custom_provider_non_export, '
                . 'internal_notes, base_network, partner_site_alias_id, logo_path, scan_template_type_id, '
                . 'is_channel_partner, salesforce_id, force_twilio_enabled, engine, mr_co_branded', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, partner_id, listing_site_id, site_id, pdi_partner, active, set_providers_featured, '
                . 'set_book_appointment, shares_reviews, set_review, update_method, dumpfile_frequency, '
                . 'output_encoding, strict_type, salesforce_sync, sort_order, name, display_name, alias_name, '
                . 'url, felix_url, any_localization, email, secret, last_password_change, see_general_entries, listing_type_id, '
                . 'custom_provider, custom_provider_featured, custom_provider_export, custom_practice_export, '
                . 'custom_provider_non_export, internal_notes, base_network, partner_site_alias_id, logo_path, '
                . 'scan_template_type_id, is_channel_partner, salesforce_id, force_twilio_enabled, engine, mr_co_branded', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Relations
     */
    public function relations()
    {
        return array(
            'accounts' => array(self::HAS_MANY, 'Account', 'partner_site_id'),
            'oauth2Clients' => array(self::HAS_MANY, 'Oauth2Clients', 'partner_site_id'),
            'organizations' => array(self::HAS_MANY, 'Organization', 'partner_site_id'),
            'scanTemplateType' => array(self::BELONGS_TO, 'ScanTemplateType', 'scan_template_type_id'),
            'listingType' => array(self::BELONGS_TO, 'ListingType', 'listing_type_id'),
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'partnerSiteEntities' => array(self::HAS_MANY, 'PartnerSiteEntity', 'partner_site_id'),
            'partnerSiteEntityMatches' => array(self::HAS_MANY, 'PartnerSiteEntityMatch', 'partner_site_id'),
            'partnerSiteParams' => array(self::HAS_MANY, 'PartnerSiteParams', 'partner_site_id'),
            'partnerSitePdiTables' => array(self::HAS_MANY, 'PartnerSitePdiTable', 'partner_site_id'),
            'partnerSiteProviderFeatureds' => array(self::HAS_MANY, 'PartnerSiteProviderFeatured', 'partner_site_id'),
            'partnerSiteQueues' => array(self::HAS_MANY, 'PartnerSiteQueue', 'partner_site_id'),
            'partnerSiteReviewResponses' => array(self::HAS_MANY, 'PartnerSiteReviewResponse', 'partner_site_id'),
            'providerRatingImportLogs' => array(self::HAS_MANY, 'ProviderRatingImportLog', 'partner_site_id'),
            'scanRequests' => array(self::HAS_MANY, 'ScanRequest', 'partner_site_id'),
        );
    }

    /**
     * Pivot Models
     */
    public function pivotModels()
    {
        return array();
    }

    /**
     * Attribute Labels
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'partner_id' => null,
            'listing_site_id' => Yii::t('app', 'Listing Site'),
            'site_id' => Yii::t('app', 'Site'),
            'pdi_partner' => Yii::t('app', 'Pdi Partner'),
            'active' => Yii::t('app', 'Active'),
            'set_providers_featured' => Yii::t('app', 'Set Providers Featured'),
            'set_book_appointment' => Yii::t('app', 'Set Book Appointment'),
            'shares_reviews' => Yii::t('app', 'Shares Reviews'),
            'set_review' => Yii::t('app', 'Set Review'),
            'update_method' => Yii::t('app', 'Update Method'),
            'dumpfile_frequency' => Yii::t('app', 'Dumpfile Frequency'),
            'output_encoding' => Yii::t('app', 'Output Encoding'),
            'engine' => Yii::t('app', 'Engine'),
            'server_alias' => Yii::t('app', 'Server Alias'),
            'strict_type' => Yii::t('app', 'Strict Type'),
            'salesforce_sync' => Yii::t('app', 'Salesforce Sync'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'name' => Yii::t('app', 'Name'),
            'display_name' => Yii::t('app', 'Display Name'),
            'alias_name' => Yii::t('app', 'Alias Name'),
            'url' => Yii::t('app', 'Url'),
            'felix_url' => Yii::t('app', 'Felix Url'),
            'any_localization' => Yii::t('app', 'Any Localization'),
            'email' => Yii::t('app', 'Email'),
            'secret' => Yii::t('app', 'Secret'),
            'last_password_change' => Yii::t('app', 'Last Password Change'),
            'see_general_entries' => Yii::t('app', 'See General Entries'),
            'listing_type_id' => null,
            'custom_provider' => Yii::t('app', 'Custom Provider'),
            'custom_provider_featured' => Yii::t('app', 'Custom Provider Featured'),
            'custom_provider_export' => Yii::t('app', 'Custom Provider Export'),
            'custom_practice_export' => Yii::t('app', 'Custom Practice Export'),
            'custom_provider_non_export' => Yii::t('app', 'Custom Provider Non Export'),
            'internal_notes' => Yii::t('app', 'Internal Notes'),
            'base_network' => Yii::t('app', 'Base Network'),
            'partner_site_alias_id' => Yii::t('app', 'Partner Site Alias'),
            'logo_path' => Yii::t('app', 'Logo Path'),
            'scan_template_type_id' => null,
            'is_channel_partner' => Yii::t('app', 'Is Channel Partner'),
            'salesforce_id' => Yii::t('app', 'Salesforce'),
            'force_twilio_enabled' =>  Yii::t('app', 'Force Twilio Enabled'),
            'mr_co_branded' => Yii::t('app', 'Co branded'),
            'accounts' => null,
            'oauth2Clients' => null,
            'organizations' => null,
            'scanTemplateType' => null,
            'listingType' => null,
            'partner' => null,
            'partnerSiteEntities' => null,
            'partnerSiteEntityMatches' => null,
            'partnerSiteParams' => null,
            'partnerSitePdiTables' => null,
            'partnerSiteProviderFeatureds' => null,
            'partnerSiteQueues' => null,
            'partnerSiteReviewResponses' => null,
            'providerRatingImportLogs' => null,
            'scanRequests' => null,
        );
    }

    /**
     * Search
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('partner_id', $this->partner_id);
        $criteria->compare('listing_site_id', $this->listing_site_id);
        $criteria->compare('site_id', $this->site_id);
        $criteria->compare('pdi_partner', $this->pdi_partner);
        $criteria->compare('active', $this->active);
        $criteria->compare('set_providers_featured', $this->set_providers_featured);
        $criteria->compare('set_book_appointment', $this->set_book_appointment);
        $criteria->compare('shares_reviews', $this->shares_reviews);
        $criteria->compare('set_review', $this->set_review);
        $criteria->compare('update_method', $this->update_method, true);
        $criteria->compare('dumpfile_frequency', $this->dumpfile_frequency, true);
        $criteria->compare('output_encoding', $this->output_encoding, true);
        $criteria->compare('enginge', $this->engine, true);
        $criteria->compare('strict_type', $this->strict_type, true);
        $criteria->compare('salesforce_sync', $this->salesforce_sync, true);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('display_name', $this->display_name, true);
        $criteria->compare('alias_name', $this->alias_name, true);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('felix_url', $this->felix_url, true);
        $criteria->compare('any_localization', $this->any_localization);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('secret', $this->secret, true);
        $criteria->compare('last_password_change', $this->last_password_change, true);
        $criteria->compare('see_general_entries', $this->see_general_entries);
        $criteria->compare('listing_type_id', $this->listing_type_id);
        $criteria->compare('custom_provider', $this->custom_provider, true);
        $criteria->compare('custom_provider_featured', $this->custom_provider_featured, true);
        $criteria->compare('custom_provider_export', $this->custom_provider_export, true);
        $criteria->compare('custom_practice_export', $this->custom_practice_export, true);
        $criteria->compare('custom_provider_non_export', $this->custom_provider_non_export, true);
        $criteria->compare('internal_notes', $this->internal_notes, true);
        $criteria->compare('base_network', $this->base_network);
        $criteria->compare('partner_site_alias_id', $this->partner_site_alias_id);
        $criteria->compare('logo_path', $this->logo_path, true);
        $criteria->compare('scan_template_type_id', $this->scan_template_type_id);
        $criteria->compare('is_channel_partner', $this->is_channel_partner);
        $criteria->compare('mr_co_branded', $this->mr_co_branded);
        $criteria->compare('salesforce_id', $this->salesforce_id, true);
        $criteria->compare('force_twilio_enabled', $this->force_twilio_enabled, true);
        $criteria->order = 't.display_name ASC';
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100),
        ));
    }
}
