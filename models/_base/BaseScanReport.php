<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "scan_report".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ScanReport".
 *
 * Columns in table "scan_report" available as properties of the model,
 * followed by relations of table "scan_report" available as properties of the model.
 *
 * @property integer $id
 * @property string $report_type
 * @property string $datetime_start
 * @property string $datetime_end
 * @property integer $report_year
 * @property integer $report_month
 * @property string $date_added
 *
 * @property ScanRequest[] $scanRequests
 */
abstract class BaseScanReport extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'scan_report';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ScanReport|ScanReports', $n);
    }

    public static function representingColumn()
    {
        return 'datetime_start';
    }

    public function rules()
    {
        return array(
            array('id, datetime_start, datetime_end, report_month', 'required'),
            array('id, report_year, report_month', 'numerical', 'integerOnly' => true),
            array('report_type', 'length', 'max' => 14),
            array('date_added', 'safe'),
            array('report_type, report_year, date_added', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, report_type, datetime_start, datetime_end, report_year, report_month, date_added', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'scanRequests' => array(self::HAS_MANY, 'ScanRequest', 'scan_report_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'report_type' => Yii::t('app', 'Report Type'),
            'datetime_start' => Yii::t('app', 'Datetime Start'),
            'datetime_end' => Yii::t('app', 'Datetime End'),
            'report_year' => Yii::t('app', 'Report Year'),
            'report_month' => Yii::t('app', 'Report Month'),
            'date_added' => Yii::t('app', 'Date Added'),
            'scanRequests' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('report_type', $this->report_type, true);
        $criteria->compare('datetime_start', $this->datetime_start, true);
        $criteria->compare('datetime_end', $this->datetime_end, true);
        $criteria->compare('report_year', $this->report_year);
        $criteria->compare('report_month', $this->report_month);
        $criteria->compare('date_added', $this->date_added, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
