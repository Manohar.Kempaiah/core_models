<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "cpn_hl7_version".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CpnHl7Version".
 *
 * Columns in table "cpn_hl7_version" available as properties of the model,
 * followed by relations of table "cpn_hl7_version" available as properties of the model.
 *
 * @property integer $id
 * @property string $guid
 * @property integer $enabled_flag
 * @property integer $del_flag
 * @property string $name
 * @property string $date_added
 * @property integer $added_by_account_id
 * @property string $date_updated
 * @property integer $updated_by_account_id
 *
 * @property Account $addedByAccount
 * @property Account $updatedByAccount
 */
abstract class BaseCpnHl7Version extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'cpn_hl7_version';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'CpnHl7Version|CpnHl7Versions', $n);
    }

    public static function representingColumn()
    {
        return 'guid';
    }

    public function rules()
    {
        return array(
            array('guid, name, added_by_account_id', 'required'),
            array('enabled_flag, del_flag, added_by_account_id, updated_by_account_id', 'numerical', 'integerOnly' => true),
            array('guid', 'length', 'max' => 255),
            array('name', 'length', 'max' => 100),
            array('date_added, date_updated', 'safe'),
            array('enabled_flag, del_flag, date_added, date_updated, updated_by_account_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, guid, enabled_flag, del_flag, name, date_added, added_by_account_id, date_updated, updated_by_account_id', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'addedByAccount' => array(self::BELONGS_TO, 'Account', 'added_by_account_id'),
            'updatedByAccount' => array(self::BELONGS_TO, 'Account', 'updated_by_account_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'guid' => Yii::t('app', 'Guid'),
            'enabled_flag' => Yii::t('app', 'Enabled Flag'),
            'del_flag' => Yii::t('app', 'Del Flag'),
            'name' => Yii::t('app', 'Name'),
            'date_added' => Yii::t('app', 'Date Added'),
            'added_by_account_id' => null,
            'date_updated' => Yii::t('app', 'Date Updated'),
            'updated_by_account_id' => null,
            'addedByAccount' => null,
            'updatedByAccount' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('guid', $this->guid, true);
        $criteria->compare('enabled_flag', $this->enabled_flag);
        $criteria->compare('del_flag', $this->del_flag);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('added_by_account_id', $this->added_by_account_id);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('updated_by_account_id', $this->updated_by_account_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
