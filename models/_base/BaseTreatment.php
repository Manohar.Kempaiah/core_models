<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model class for table "treatment".
 *
 * The followings are the available columns in table 'treatment':
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $maximum_cost
 * @property integer $minimum_cost
 * @property integer $provider_level_search
 * @property integer $common_treatment
 *
 * The followings are the available model relations:
 * @property PracticeTreatment[] $practiceTreatments
 * @property ProviderTreatment[] $providerTreatments
 * @property ProviderTreatmentPerformed[] $providerTreatmentPerformeds
 * @property SpecialtyTreatment[] $specialtyTreatments
 * @property SpecialtyTreatmentCondition[] $specialtyTreatmentConditions
 */
abstract class BaseTreatment extends BaseModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'treatment';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('maximum_cost, minimum_cost, provider_level_search, common_treatment', 'numerical',
                'integerOnly' => true),
            array('name', 'length', 'max' => 250),
            array('code', 'length', 'max' => 5),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, name, code, maximum_cost, minimum_cost, provider_level_search, common_treatment', 'safe',
                'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'practiceTreatments' => array(self::HAS_MANY, 'PracticeTreatment', 'treatment_id'),
            'providerTreatments' => array(self::HAS_MANY, 'ProviderTreatment', 'treatment_id'),
            'providerTreatmentPerformeds' => array(self::HAS_MANY, 'ProviderTreatmentPerformed', 'treatment_id'),
            'specialtyTreatments' => array(self::HAS_MANY, 'SpecialtyTreatment', 'treatment_id'),
            'specialtyTreatmentConditions' => array(self::HAS_MANY, 'SpecialtyTreatmentCondition', 'treatment_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
            'maximum_cost' => 'Maximum Cost',
            'minimum_cost' => 'Minimum Cost',
            'provider_level_search' => 'Provider Level Search',
            'common_treatment' => 'Common Treatment',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('maximum_cost', $this->maximum_cost);
        $criteria->compare('minimum_cost', $this->minimum_cost);
        $criteria->compare('provider_level_search', $this->provider_level_search);
        $criteria->compare('common_treatment', $this->common_treatment);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BaseTreatment the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
