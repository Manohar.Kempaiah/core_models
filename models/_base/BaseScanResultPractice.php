<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "scan_result_practice".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ScanResultPractice".
 *
 * Columns in table "scan_result_practice" available as properties of the model,
 * followed by relations of table "scan_result_practice" available as properties of the model.
 *
 * @property integer $id
 * @property integer $listing_type_id
 * @property integer $match_name
 * @property integer $match_address
 * @property integer $match_zipcode
 * @property integer $match_city
 * @property double $distance
 * @property integer $match_near_location
 * @property integer $match_state
 * @property integer $match_phone
 * @property string $scraped_name
 * @property string $scraped_phone_number
 * @property string $scraped_address
 * @property string $scraped_zipcode
 * @property string $scraped_city
 * @property string $scraped_state
 * @property string $scraped_state_abbreviation
 * @property double $scraped_latitude
 * @property double $scraped_longitude
 * @property string $url
 * @property integer $scan_request_practice_id
 *
 * @property ScanError[] $scanErrors
 * @property ScanRequestPractice $scanRequestPractice
 * @property ScanSourcePractice[] $scanSourcePractices
 */
abstract class BaseScanResultPractice extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'scan_result_practice';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ScanResultPractice|ScanResultPractices', $n);
    }

    public static function representingColumn()
    {
        return 'scraped_name';
    }

    public function rules()
    {
        return array(
            array('scan_request_practice_id', 'required'),
            array('listing_type_id, match_name, match_address, match_zipcode, match_city, match_near_location, match_state, match_phone, scan_request_practice_id', 'numerical', 'integerOnly'=>true),
            array('distance, scraped_latitude, scraped_longitude', 'numerical'),
            array('scraped_name, scraped_phone_number, scraped_address', 'length', 'max'=>255),
            array('scraped_zipcode', 'length', 'max'=>10),
            array('scraped_city, scraped_state, scraped_state_abbreviation', 'length', 'max'=>50),
            array('url', 'safe'),
            array('listing_type_id, match_name, match_address, match_zipcode, match_city, distance, match_near_location, match_state, match_phone, scraped_name, scraped_phone_number, scraped_address, scraped_zipcode, scraped_city, scraped_state, scraped_state_abbreviation, scraped_latitude, scraped_longitude, url', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, listing_type_id, match_name, match_address, match_zipcode, match_city, distance, match_near_location, match_state, match_phone, scraped_name, scraped_phone_number, scraped_address, scraped_zipcode, scraped_city, scraped_state, scraped_state_abbreviation, scraped_latitude, scraped_longitude, url, scan_request_practice_id', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'scanErrors' => array(self::HAS_MANY, 'ScanError', 'scan_result_practice_id'),
            'scanRequestPractice' => array(self::BELONGS_TO, 'ScanRequestPractice', 'scan_request_practice_id'),
            'scanSourcePractices' => array(self::HAS_MANY, 'ScanSourcePractice', 'scan_result_practice_id'),
            'listingType' => array(self::BELONGS_TO, 'ListingType', 'listing_type_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'listing_type_id' => Yii::t('app', 'Listing Type'),
            'match_name' => Yii::t('app', 'Match Name'),
            'match_address' => Yii::t('app', 'Match Address'),
            'match_zipcode' => Yii::t('app', 'Match Zipcode'),
            'match_city' => Yii::t('app', 'Match City'),
            'distance' => Yii::t('app', 'Distance'),
            'match_near_location' => Yii::t('app', 'Match Near Location'),
            'match_state' => Yii::t('app', 'Match State'),
            'match_phone' => Yii::t('app', 'Match Phone'),
            'scraped_name' => Yii::t('app', 'Scraped Name'),
            'scraped_phone_number' => Yii::t('app', 'Scraped Phone Number'),
            'scraped_address' => Yii::t('app', 'Scraped Address'),
            'scraped_zipcode' => Yii::t('app', 'Scraped Zipcode'),
            'scraped_city' => Yii::t('app', 'Scraped City'),
            'scraped_state' => Yii::t('app', 'Scraped State'),
            'scraped_state_abbreviation' => Yii::t('app', 'Scraped State Abbreviation'),
            'scraped_latitude' => Yii::t('app', 'Scraped Latitude'),
            'scraped_longitude' => Yii::t('app', 'Scraped Longitude'),
            'url' => Yii::t('app', 'Url'),
            'scan_request_practice_id' => null,
            'scanErrors' => null,
            'scanRequestPractice' => null,
            'scanSourcePractices' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('listing_type_id', $this->listing_type_id);
        $criteria->compare('match_name', $this->match_name);
        $criteria->compare('match_address', $this->match_address);
        $criteria->compare('match_zipcode', $this->match_zipcode);
        $criteria->compare('match_city', $this->match_city);
        $criteria->compare('distance', $this->distance);
        $criteria->compare('match_near_location', $this->match_near_location);
        $criteria->compare('match_state', $this->match_state);
        $criteria->compare('match_phone', $this->match_phone);
        $criteria->compare('scraped_name', $this->scraped_name, true);
        $criteria->compare('scraped_phone_number', $this->scraped_phone_number, true);
        $criteria->compare('scraped_address', $this->scraped_address, true);
        $criteria->compare('scraped_zipcode', $this->scraped_zipcode, true);
        $criteria->compare('scraped_city', $this->scraped_city, true);
        $criteria->compare('scraped_state', $this->scraped_state, true);
        $criteria->compare('scraped_state_abbreviation', $this->scraped_state_abbreviation, true);
        $criteria->compare('scraped_latitude', $this->scraped_latitude);
        $criteria->compare('scraped_longitude', $this->scraped_longitude);
        $criteria->compare('url', $this->url, true);
        $criteria->compare('scan_request_practice_id', $this->scan_request_practice_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
