<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

abstract class BasePatientAccountReports extends BaseModel
{

    public $insurance_company;
    public $origin_partner;
    public $origin_site;
    public $online_appointment_period;
    public $total_online_appointment;
    public $reviews_period;
    public $total_reviews;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'patient';
    }

    public function relations()
    {
        return array(
            'insurance' => array(self::BELONGS_TO, 'Insurance', 'insurance_id'),
            'partnerSite' => array(self::BELONGS_TO, 'PartnerSite', 'partner_site_id'),
        );
    }

    public function rules()
    {
        return array(
            array('id, first_name, last_name, email, zipcode, insurance_company, insurance, origin_partner, '
                . 'origin_site, online_appointment_period, total_online_appointment, reviews_period, total_reviews, '
                . 'date_added', 'required'),
            array('id, first_name, last_name, email, zipcode, insurance_company, insurance, origin_partner, '
                . 'origin_site, online_appointment_period, total_online_appointment, reviews_period, total_reviews, '
                . 'date_added', 'safe', 'on' => 'search'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->with = array('insurance.company', 'partnerSite.partner');

        // Filter for partners
        if (isset($_GET['Partners']) && $_GET['Partners'] != '') {
            $criteria->addInCondition('partnerSite.partner_id', $_GET['Partners']);
        }

        // Filter for sites
        if (isset($_GET['PartnerSites']) && $_GET['PartnerSites'] != '') {
            $criteria->addInCondition('t.partner_site_id', $_GET['PartnerSites']);
        }

        // FIlter for date
        $where = '';
        if (isset($_GET['Date'])) {
            $date = $_GET['Date'];
        } else {
            $date = 8;
        }
        $date = PartnerFilters::getDate($date);
        $date_period = '';
        if ($date != '') {
            $date_period = 'WHERE date_added > ' . $date;
            $aux = split('-', $date);
            if (count($aux) > 1) {
                $where .= 't.date_added >= "' . $date . '"';
            } else {
                $where .= ' year(t.date_added) >= "' . $date . '"';
            }
            $criteria->addCondition($where);
        }

        // Filters for the CGridView
        $criteria->compare('id', $this->id);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('zipcode', $this->zipcode, true);
        $criteria->compare('insurance_company', $this->insurance_company, true);
        $criteria->compare('insurance.name', $this->insurance, true);
        $criteria->compare('partner.name', $this->origin_partner, true);
        $criteria->compare('partnerSite.name', $this->origin_site, true);
        $criteria->compare('online_appointment_period', $this->online_appointment_period);
        $criteria->compare('total_online_appointment', $this->total_online_appointment);
        $criteria->compare('reviews_period', $this->reviews_period);
        $criteria->compare('total_reviews', $this->total_reviews);
        $criteria->compare('date_added', $this->date_added, true);

        $criteria->join = 'LEFT JOIN (SELECT COUNT(scheduling_mail.id) AS online_appointment_period,
            scheduling_mail.partner_site_id
            FROM scheduling_mail ' . $date_period . ') online_appointment_period_table
            ON online_appointment_period_table.partner_site_id = t.partner_site_id
            LEFT JOIN (SELECT COUNT(scheduling_mail.id) AS total_online_appointment,
            scheduling_mail.partner_site_id
            FROM scheduling_mail) total_online_appointment_table
            ON total_online_appointment_table.partner_site_id = t.partner_site_id
            LEFT JOIN (SELECT COUNT(provider_rating.id) AS reviews_period,
            provider_rating.partner_site_id AS partner_site_id
            FROM provider_rating ' . $date_period . ') reviews_period_table
            ON reviews_period_table.partner_site_id = t.partner_site_id
            LEFT JOIN (SELECT COUNT(provider_rating.id) AS total_reviews,
            provider_rating.partner_site_id AS partner_site_id
            FROM provider_rating) total_reviews_table
            ON total_reviews_table.partner_site_id = t.partner_site_id';

        $criteria->select = 't.*,
            online_appointment_period_table.online_appointment_period AS online_appointment_period,
            total_online_appointment_table.total_online_appointment AS total_online_appointment,
            reviews_period_table.reviews_period AS reviews_period,
            total_reviews_table.total_reviews AS total_reviews';

        return new CActiveDataProvider(get_class($this), array('criteria' => $criteria, 'pagination' => array('pageSize' => 100)));
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Patient|Patients', $n);
    }

}
