<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "localization".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Localization".
 *
 * Columns in table "localization" available as properties of the model,
 * followed by relations of table "localization" available as properties of the model.
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 *
 * @property Account[] $accounts
 * @property AccountDevice[] $accountDevices
 * @property Country[] $countries
 * @property Credential[] $credentials
 * @property EducationName[] $educationNames
 * @property Emr[] $emrs
 * @property Insurance[] $insurances
 * @property InsuranceCompany[] $insuranceCompanies
 * @property Patient[] $patients
 * @property Practice[] $practices
 * @property ProviderRating[] $providerRatings
 * @property SchedulingMail[] $schedulingMails
 */
abstract class BaseLocalization extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'localization';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Localization|Localizations', $n);
    }

    public static function representingColumn()
    {
        return 'code';
    }

    public function rules()
    {
        return array(
            array('code, name', 'required'),
            array('code', 'length', 'max' => 5),
            array('name', 'length', 'max' => 50),
            array('id, code, name', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'accounts' => array(self::HAS_MANY, 'Account', 'localization_id'),
            'accountDevices' => array(self::HAS_MANY, 'AccountDevice', 'localization_id'),
            'countries' => array(self::HAS_MANY, 'Country', 'default_localization_id'),
            'credentials' => array(self::HAS_MANY, 'Credential', 'localization_id'),
            'educationNames' => array(self::HAS_MANY, 'EducationName', 'localization_id'),
            'emrs' => array(self::HAS_MANY, 'Emr', 'localization_id'),
            'insurances' => array(self::HAS_MANY, 'Insurance', 'localization_id'),
            'insuranceCompanies' => array(self::HAS_MANY, 'InsuranceCompany', 'localization_id'),
            'patients' => array(self::HAS_MANY, 'Patient', 'localization_id'),
            'practices' => array(self::HAS_MANY, 'Practice', 'localization_id'),
            'providerRatings' => array(self::HAS_MANY, 'ProviderRating', 'localization_id'),
            'schedulingMails' => array(self::HAS_MANY, 'SchedulingMail', 'localization_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'code' => Yii::t('app', 'Code'),
            'name' => Yii::t('app', 'Name'),
            'accounts' => null,
            'accountDevices' => null,
            'countries' => null,
            'credentials' => null,
            'educationNames' => null,
            'emrs' => null,
            'insurances' => null,
            'insuranceCompanies' => null,
            'patients' => null,
            'practices' => null,
            'providerRatings' => null,
            'schedulingMails' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('code', $this->code, true);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
