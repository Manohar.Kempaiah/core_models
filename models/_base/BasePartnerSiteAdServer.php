<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "partner_site_ad_server".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PartnerSiteAdServer".
 *
 * Columns in table "partner_site_ad_server" available as properties of the model,
 * followed by relations of table "partner_site_ad_server" available as properties of the model.
 *
 * @property integer $id
 * @property integer $partner_site_id
 * @property string $name
 * @property string $type
 * @property string $file_path
 * @property integer $active
 * @property string $date_added
 * @property string $date_updated
 *
 * @property PartnerSite $partnerSite
 */

abstract class BasePartnerSiteAdServer extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'partner_site_ad_server';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'PartnerSiteAdServer|PartnerSiteAdServer', $n);
    }

    public static function representingColumn()
    {
        return 'file_path';
    }

    public function rules()
    {
        return array(
            array('partner_site_id, name', 'required'),
            array('partner_site_id, active', 'numerical', 'integerOnly' => true),
            array('date_added, date_updated, file_path, type, name', 'safe'),
            array('date_added, date_updated, file_path', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, partner_site_id, date_added, date_updated, file_path, type, active, name', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'partnerSite' => array(self::BELONGS_TO, 'PartnerSite', 'partner_site_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'partner_site_id' => null,
            'name' => Yii::t('app', 'Name'),
            'date_added' => Yii::t('app', 'Date Added'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'file_path' => Yii::t('app', 'File Path'),
            'type' => Yii::t('app', 'File Type'),
            'active' => Yii::t('app', 'Active'),
            'partnerSite' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('partner_site_id', $this->partner_site_id);
        $criteria->compare('name', $this->name);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('file_path', $this->file_path, true);
        $criteria->compare('type', $this->type, true);
        $criteria->compare('active', $this->active, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
