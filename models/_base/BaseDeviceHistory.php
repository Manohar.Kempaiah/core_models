<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model class for table "device_history".
 *
 * The followings are the available columns in table 'device_history':
 * @property string $id
 * @property integer $device_id
 * @property string $prev_status
 * @property string $new_status
 * @property string $change_type
 * @property integer $account_id
 * @property string $date_added
 *
 * The followings are the available model relations:
 * @property Device $device
 * @property Account $account
 */
abstract class BaseDeviceHistory extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'device_history';
    }

    public static function representingColumn()
    {
        return 'date_added';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('device_id, new_status, date_added', 'required'),
            array('device_id, account_id', 'numerical', 'integerOnly' => true),
            array('prev_status, new_status, change_type', 'length', 'max' => 1),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, device_id, prev_status, new_status, change_type, account_id, date_added', 'safe',
                'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'device' => array(self::BELONGS_TO, 'Device', 'device_id'),
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'device_id' => 'Device',
            'prev_status' => 'Previous Status',
            'new_status' => 'New Status',
            'change_type' => 'Change Type',
            'account_id' => 'Account',
            'date_added' => 'Date Added',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('device_id', $this->device_id);
        $criteria->compare('prev_status', $this->prev_status, true);
        $criteria->compare('new_status', $this->new_status, true);
        $criteria->compare('change_type', $this->change_type, true);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('date_added', $this->date_added, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Device History|Device Histories', $n);
    }

}
