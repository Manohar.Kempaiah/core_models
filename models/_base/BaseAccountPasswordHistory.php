<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model class for table "account_password_history".
 *
 * The followings are the available columns in table 'account_password_history':
 * @property integer $id
 * @property integer $entity_id
 * @property string $entity_type
 * @property string $secret
 * @property string $date_added
 * @property integer $added_by_account_id
 */
class BaseAccountPasswordHistory extends BaseModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'account_password_history';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'AccountPasswordHistory|AccountPasswordHistories', $n);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('entity_id, entity_type, secret', 'required'),
            array('entity_id, added_by_account_id', 'numerical', 'integerOnly' => true),
            array('entity_type', 'length', 'max' => 12),
            array('secret', 'length', 'max' => 60),
            array('date_added', 'safe'),
            array('id, entity_id, entity_type, date_added, added_by_account_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'entity_id' => 'Entity',
            'entity_type' => 'Entity Type',
            'secret' => 'Secret',
            'date_added' => 'Date Added',
            'added_by_account_id' => 'Added By Account',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('entity_id', $this->entity_id);
        $criteria->compare('entity_type', $this->entity_type, true);
        $criteria->compare('secret', $this->secret, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('added_by_account_id', $this->added_by_account_id);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BaseAccountPasswordHistory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
