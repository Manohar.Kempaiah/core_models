<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model class for table "organization_partner_graphics".
 *
 * The followings are the available columns in table 'organization_partner_graphics':
 * @property integer $id
 * @property integer $organization_id
 * @property integer $feature_id
 * @property integer $provider_id
 * @property integer $practice_id
 * @property integer $active
 * @property string $date_added
 * @property string $date_updated
 *
 * The followings are the available model relations:
 * @property Feature $feature
 * @property Organization $organization
 * @property Practice $practice
 * @property Provider $provider
 */
class BaseOrganizationPartnerGraphics extends BaseModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'organization_partner_graphics';
    }

    /**
     * Columns representing the schema
     * @return array
     */
    public static function representingColumn()
    {
        return array('organization_id', 'partner_id');
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('organization_id, partner_id, phab_task_id', 'required'),
            array('organization_id, partner_id', 'numerical', 'integerOnly' => true),
            array('assigned_to, date_added, date_updated', 'safe'),
            // The following rule is used by search().
            array('id, organization_id, partner_id, phab_task_id, assigned_to, date_added, date_updated',
                'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'partner' => array(self::BELONGS_TO, 'Partner', 'partner_id'),
            'organization' => array(self::BELONGS_TO, 'Organization', 'organization_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'organization_id' => 'Organization ID',
            'partner_id' => 'Partner',
            'phab_task_id' => 'Phabricator Task Id',
            'assigned_to' => 'Assigned To',
            'date_added' => 'Date Added',
            'date_updated' => 'Date Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);

        if (!intval($this->organization_id) && is_string($this->organization_id) && strlen($this->organization_id) > 0) {
            $criteria->with[] = 'organization';
            $criteria->addSearchCondition("organization.organization_name", $this->organization_id, true);
        } else {
            $criteria->compare('organization_id', $this->organization_id);
        }

        if (!intval($this->partner_id) && is_string($this->partner_id) && strlen($this->partner_id) > 0) {
            $criteria->with[] = 'partner';
            $criteria->addSearchCondition("partner.name", $this->partner_id, true);
        } else {
            $criteria->compare('partner_id', $this->partner_id);
        }

        $criteria->compare('t.phab_task_id', $this->phab_task_id, true);
        $criteria->compare('t.assigned_to', $this->assigned_to, true);
        $criteria->compare('t.date_added', $this->date_added, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->order = 't.date_added DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 20)
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BaseOrganizationFeature the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
