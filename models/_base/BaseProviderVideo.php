<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "provider_video".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProviderVideo".
 *
 * Columns in table "provider_video" available as properties of the model,
 * followed by relations of table "provider_video" available as properties of the model.
 *
 * @property integer $id
 * @property integer $provider_id
 * @property string $embed_code
 *
 * @property Provider $provider
 */
abstract class BaseProviderVideo extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'provider_video';
    }

    public static function representingColumn()
    {
        return 'embed_code';
    }

    public function rules()
    {
        return array(
            array('provider_id, embed_code', 'required'),
            array('provider_id', 'numerical', 'integerOnly' => true),
            array('id, provider_id, embed_code', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'provider' => array(self::BELONGS_TO, 'Provider', 'provider_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'provider_id' => Yii::t('app', 'Provider'),
            'embed_code' => Yii::t('app', 'Embed Code'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        //it will search by id or provider.first_last
        if (!intval($this->provider_id) && is_string($this->provider_id) && strlen($this->provider_id) > 0) {
            $criteria->with[] = 'provider';
            $criteria->addSearchCondition("provider.first_last", $this->provider_id);
        } else
            $criteria->compare('provider_id', $this->provider_id);

        $criteria->compare('embed_code', $this->embed_code, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ProviderVideo|ProviderVideos', $n);
    }

}
