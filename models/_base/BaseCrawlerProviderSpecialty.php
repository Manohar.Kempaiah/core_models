<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "crawler_provider_specialty".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CrawlerProviderSpecialty".
 *
 * Columns in table "crawler_provider_specialty" available as properties of the model,
 * followed by relations of table "crawler_provider_specialty" available as properties of the model.
 *
 * @property integer $id
 * @property integer $crawler_provider_id
 * @property string $specialty
 * @property integer $sort_order
 * @property string $date_added
 * @property string $date_updated
 *
 * @property CrawlerProvider $crawlerProvider
 */
abstract class BaseCrawlerProviderSpecialty extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'crawler_provider_specialty';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'CrawlerProviderSpecialty|CrawlerProviderSpecialties', $n);
    }

    public static function representingColumn()
    {
        return 'date_added';
    }

    public function rules()
    {
        return array(
            array('crawler_provider_id, date_added', 'required'),
            array('crawler_provider_id, sort_order', 'numerical', 'integerOnly' => true),
            array('specialty', 'length', 'max' => 100),
            array('date_updated', 'safe'),
            array('specialty, sort_order, date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, crawler_provider_id, specialty, sort_order, date_added, date_updated', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'crawlerProvider' => array(self::BELONGS_TO, 'CrawlerProvider', 'crawler_provider_id'),
        );
    }

    public function pivotModels()
    {
        return array();
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'crawler_provider_id' => null,
            'specialty' => Yii::t('app', 'Specialty'),
            'sort_order' => Yii::t('app', 'Sort Order'),
            'date_added' => Yii::t('app', 'Date Added'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'crawlerProvider' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('crawler_provider_id', $this->crawler_provider_id);
        $criteria->compare('specialty', $this->specialty, true);
        $criteria->compare('sort_order', $this->sort_order);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }
}
