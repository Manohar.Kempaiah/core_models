<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "practice_url_history".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "PracticeUrlHistory".
 *
 * Columns in table "practice_url_history" available as properties of the model,
 * followed by relations of table "practice_url_history" available as properties of the model.
 *
 * @property integer $id
 * @property integer $practice_id
 * @property string $friendly_url
 *
 * @property Practice $practice
 */
abstract class BasePracticeUrlHistory extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'practice_url_history';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'PracticeUrlHistory|PracticeUrlHistories', $n);
    }

    public static function representingColumn()
    {
        return 'friendly_url';
    }

    public function rules()
    {
        return array(
            array('practice_id, friendly_url', 'required'),
            array('practice_id', 'numerical', 'integerOnly' => true),
            array('friendly_url', 'length', 'max' => 255),
            array('id, practice_id, friendly_url', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'practice' => array(self::BELONGS_TO, 'Practice', 'practice_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'practice_id' => null,
            'friendly_url' => Yii::t('app', 'Friendly Url'),
            'practice' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('practice_id', $this->practice_id);
        $criteria->compare('friendly_url', $this->friendly_url, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
