<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "condition".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Condition".
 *
 * Columns in table "condition" available as properties of the model,
 * followed by relations of table "condition" available as properties of the model.
 *
 * @property integer $id
 * @property string $name
 *
 * @property SpecialtyCondition[] $specialtyConditions
 */
abstract class BaseCondition extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'condition';
    }

    public static function representingColumn()
    {
        return 'name';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max' => 50),
            array('id, name', 'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
            'specialtyConditions' => array(self::HAS_MANY, 'SpecialtyCondition', 'condition_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider(get_class($this), array('criteria' => $criteria, 'pagination' => array('pageSize' => 100)));
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Condition|Conditions', $n);
    }

}
