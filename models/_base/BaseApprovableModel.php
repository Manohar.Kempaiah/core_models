<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is a model that contains shared logic for objects that need
 * approvals/rejects and have logic around not sending emails after
 * the object is a certain age, etc.
 */
class BaseApprovableModel extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * If this is an old rating, we don't want to email anyone about this
     * rating because it will make us look bad (that we were that slow to
     * approve or reject it).
     */
    public function isTooOldToEmailAbout()
    {

        if (!$this->date_added) {
            return true;
        }

        // returns true if older than 30 days
        if (!ini_get('date.timezone')) {
            date_default_timezone_set('UTC');
        }
        return (strtotime($this->date_added) < strtotime('-30 days'));
    }

}
