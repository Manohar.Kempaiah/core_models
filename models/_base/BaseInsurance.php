<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "insurance".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "Insurance".
 *
 * Columns in table "insurance" available as properties of the model,
 * followed by relations of table "insurance" available as properties of the model.
 *
 * @property integer $id
 * @property integer $localization_id
 * @property integer $company_id
 * @property string $name
 * @property string $name_plural
 * @property string $name_ads
 * @property string $api_abbreviature
 * @property string $description
 * @property string $friendly_url
 * @property integer $visits_number
 * @property integer $enabled
 * @property string $gmb_network_id
 * @property string $date_added
 * @property string $marketplace_id
 *
 * @property InsuranceCompany $company
 * @property Localization $localization
 * @property Patient[] $patients
 * @property SchedulingMail[] $schedulingMails
 */
abstract class BaseInsurance extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'insurance';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Insurance|Insurances', $n);
    }

    public static function representingColumn()
    {
        return 'name';
    }

    public function rules()
    {
        return array(
            array('company_id, name, name_plural, api_abbreviature, friendly_url, date_added', 'required'),
            array('localization_id, company_id, visits_number, enabled', 'numerical', 'integerOnly'=>true),
            array('name, name_plural', 'length', 'max'=>100),
            array('name_ads, friendly_url, gmb_network_id', 'length', 'max'=>255),
            array('api_abbreviature', 'length', 'max'=>5),
            array('marketplace_id', 'length', 'max'=>14),
            array('description', 'safe'),
            array('localization_id, name_ads, description, visits_number, enabled, gmb_network_id, marketplace_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, localization_id, company_id, name, name_plural, name_ads, api_abbreviature, description, friendly_url, visits_number, enabled, gmb_network_id, date_added, marketplace_id', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'company' => array(self::BELONGS_TO, 'InsuranceCompany', 'company_id'),
            'localization' => array(self::BELONGS_TO, 'Localization', 'localization_id'),
            'patients' => array(self::HAS_MANY, 'Patient', 'insurance_id'),
            'schedulingMails' => array(self::HAS_MANY, 'SchedulingMail', 'insurance_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'localization_id' => null,
            'company_id' => null,
            'name' => Yii::t('app', 'Name'),
            'name_plural' => Yii::t('app', 'Name Plural'),
            'name_ads' => Yii::t('app', 'Name Ads'),
            'api_abbreviature' => Yii::t('app', 'Api Abbreviature'),
            'description' => Yii::t('app', 'Description'),
            'friendly_url' => Yii::t('app', 'Friendly Url'),
            'visits_number' => Yii::t('app', 'Visits Number'),
            'enabled' => Yii::t('app', 'Enabled'),
            'gmb_network_id' => Yii::t('app', 'Gmb Network'),
            'date_added' => Yii::t('app', 'Date Added'),
            'marketplace_id' => Yii::t('app', 'Marketplace'),
            'company' => null,
            'localization' => null,
            'patients' => null,
            'schedulingMails' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('localization_id', $this->localization_id);
        $criteria->compare('company_id', $this->company_id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('name_plural', $this->name_plural, true);
        $criteria->compare('name_ads', $this->name_ads, true);
        $criteria->compare('api_abbreviature', $this->api_abbreviature, true);
        $criteria->compare('description', $this->description, true);
        $criteria->compare('friendly_url', $this->friendly_url, true);
        $criteria->compare('visits_number', $this->visits_number);
        $criteria->compare('enabled', $this->enabled);
        $criteria->compare('gmb_network_id', $this->gmb_network_id, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('marketplace_id', $this->marketplace_id, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria
        ));
    }

}
