<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "cost_quality".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "CostQuality".
 *
 * Columns in table "cost_quality" available as properties of the model,
 * and there are no model relations.
 *
 * @property string $id
 * @property integer $is_excluded
 * @property integer $measure
 * @property integer $treatment_id
 * @property string $inpat_outpat_services
 * @property integer $treatment_friendly_id
 * @property integer $is_bilateral_procedure
 * @property integer $practice_group_id
 * @property integer $practice_id
 * @property integer $provider_id
 * @property integer $quality_area_id
 * @property integer $quality_factor_metric_id
 * @property integer $quality_factor_value_id
 * @property integer $number_of_episodes
 * @property double $aggregatecostprocedure
 * @property string $analysis_period_start
 * @property string $analysis_period_end
 * @property integer $cost_tier
 * @property integer $cost_factor_volume_id
 * @property integer $cost_factor_volatility_id
 * @property double $aggregatecostaverage
 * @property double $aggregatecoststandarddeviation
 * @property double $aggregatecostpercentile10th
 * @property double $aggregatecostpercentile25th
 * @property double $aggregatecostpercentile50th
 * @property double $aggregatecostpercentile75th
 * @property double $aggregatecostpercentile90th
 * @property double $facilitycostaverage
 * @property double $facilitycoststandarddeviation
 * @property double $facilitycostpercentile10th
 * @property double $facilitycostpercentile25th
 * @property double $facilitycostpercentile50th
 * @property double $facilitycostpercentile75th
 * @property double $facilitycostpercentile90th
 * @property double $anesthesiacostaverage
 * @property double $anesthesiacoststandarddeviation
 * @property double $anesthesiacostpercentile10th
 * @property double $anesthesiacostpercentile25th
 * @property double $anesthesiacostpercentile50th
 * @property double $anesthesiacostpercentile75th
 * @property double $anesthesiacostpercentile90th
 * @property double $radiologycostaverage
 * @property double $radiologycoststandarddeviation
 * @property double $radiologycostpercentile10th
 * @property double $radiologycostpercentile25th
 * @property double $radiologycostpercentile50th
 * @property double $radiologycostpercentile75th
 * @property double $radiologycostpercentile90th
 * @property double $pathologycostaverage
 * @property double $pathologycoststandarddeviation
 * @property double $pathologycostpercentile10th
 * @property double $pathologycostpercentile25th
 * @property double $pathologycostpercentile50th
 * @property double $pathologycostpercentile75th
 * @property double $pathologycostpercentile90th
 * @property double $professionalcostaverage
 * @property double $professionalcoststandarddeviation
 * @property double $professionalcostpercentile10th
 * @property double $professionalcostpercentile25th
 * @property double $professionalcostpercentile50th
 * @property double $professionalcostpercentile75th
 * @property double $professionalcostpercentile90th
 * @property double $ancillarycostaverage
 * @property double $ancillarycoststandarddeviation
 * @property double $ancillarycostpercentile10th
 * @property double $ancillarycostpercentile25th
 * @property double $ancillarycostpercentile50th
 * @property double $ancillarycostpercentile75th
 * @property double $ancillarycostpercentile90th
 * @property double $pharmacycostaverage
 * @property double $pharmacycoststandarddeviation
 * @property double $pharmacycostpercentile10th
 * @property double $pharmacycostpercentile25th
 * @property double $pharmacycostpercentile50th
 * @property double $pharmacycostpercentile75th
 * @property double $pharmacycostpercentile90th
 *
 */
abstract class BaseCostQuality extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'cost_quality';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'CostQuality|CostQualities', $n);
    }

    public static function representingColumn()
    {
        return 'analysis_period_start';
    }

    public function rules()
    {
        return array(
            array('is_excluded, measure, number_of_episodes, analysis_period_start, analysis_period_end, '
                . 'cost_tier', 'required'),
            array('is_excluded, measure, treatment_id, treatment_friendly_id, is_bilateral_procedure, '
                . 'practice_group_id, practice_id, provider_id, quality_area_id, quality_factor_metric_id, '
                . 'quality_factor_value_id, number_of_episodes, cost_tier, cost_factor_volume_id, '
                . 'cost_factor_volatility_id', 'numerical', 'integerOnly' => true),
            array('aggregatecostprocedure, aggregatecostaverage, aggregatecoststandarddeviation, '
                . 'aggregatecostpercentile10th, aggregatecostpercentile25th, aggregatecostpercentile50th, '
                . 'aggregatecostpercentile75th, aggregatecostpercentile90th, facilitycostaverage, '
                . 'facilitycoststandarddeviation, facilitycostpercentile10th, facilitycostpercentile25th, '
                . 'facilitycostpercentile50th, facilitycostpercentile75th, facilitycostpercentile90th, '
                . 'anesthesiacostaverage, anesthesiacoststandarddeviation, anesthesiacostpercentile10th, '
                . 'anesthesiacostpercentile25th, anesthesiacostpercentile50th, anesthesiacostpercentile75th, '
                . 'anesthesiacostpercentile90th, radiologycostaverage, radiologycoststandarddeviation, '
                . 'radiologycostpercentile10th, radiologycostpercentile25th, radiologycostpercentile50th, '
                . 'radiologycostpercentile75th, radiologycostpercentile90th, pathologycostaverage, '
                . 'pathologycoststandarddeviation, pathologycostpercentile10th, pathologycostpercentile25th, '
                . 'pathologycostpercentile50th, pathologycostpercentile75th, pathologycostpercentile90th, '
                . 'professionalcostaverage, professionalcoststandarddeviation, professionalcostpercentile10th, '
                . 'professionalcostpercentile25th, professionalcostpercentile50th, professionalcostpercentile75th, '
                . 'professionalcostpercentile90th, ancillarycostaverage, ancillarycoststandarddeviation, '
                . 'ancillarycostpercentile10th, ancillarycostpercentile25th, ancillarycostpercentile50th, '
                . 'ancillarycostpercentile75th, ancillarycostpercentile90th, pharmacycostaverage, '
                . 'pharmacycoststandarddeviation, pharmacycostpercentile10th, pharmacycostpercentile25th, '
                . 'pharmacycostpercentile50th, pharmacycostpercentile75th, pharmacycostpercentile90th', 'numerical'),
            array('inpat_outpat_services', 'length', 'max' => 3),
            array('treatment_id, inpat_outpat_services, treatment_friendly_id, is_bilateral_procedure, '
                . 'practice_group_id, practice_id, provider_id, quality_area_id, quality_factor_metric_id, '
                . 'quality_factor_value_id, aggregatecostprocedure, cost_factor_volume_id, cost_factor_volatility_id, '
                . 'aggregatecostaverage, aggregatecoststandarddeviation, aggregatecostpercentile10th, '
                . 'aggregatecostpercentile25th, aggregatecostpercentile50th, aggregatecostpercentile75th, '
                . 'aggregatecostpercentile90th, facilitycostaverage, facilitycoststandarddeviation, '
                . 'facilitycostpercentile10th, facilitycostpercentile25th, facilitycostpercentile50th, '
                . 'facilitycostpercentile75th, facilitycostpercentile90th, anesthesiacostaverage, '
                . 'anesthesiacoststandarddeviation, anesthesiacostpercentile10th, anesthesiacostpercentile25th, '
                . 'anesthesiacostpercentile50th, anesthesiacostpercentile75th, anesthesiacostpercentile90th, '
                . 'radiologycostaverage, radiologycoststandarddeviation, radiologycostpercentile10th, '
                . 'radiologycostpercentile25th, radiologycostpercentile50th, radiologycostpercentile75th, '
                . 'radiologycostpercentile90th, pathologycostaverage, pathologycoststandarddeviation, '
                . 'pathologycostpercentile10th, pathologycostpercentile25th, pathologycostpercentile50th, '
                . 'pathologycostpercentile75th, pathologycostpercentile90th, professionalcostaverage, '
                . 'professionalcoststandarddeviation, professionalcostpercentile10th, professionalcostpercentile25th, '
                . 'professionalcostpercentile50th, professionalcostpercentile75th, professionalcostpercentile90th, '
                . 'ancillarycostaverage, ancillarycoststandarddeviation, ancillarycostpercentile10th, '
                . 'ancillarycostpercentile25th, ancillarycostpercentile50th, ancillarycostpercentile75th, '
                . 'ancillarycostpercentile90th, pharmacycostaverage, pharmacycoststandarddeviation, '
                . 'pharmacycostpercentile10th, pharmacycostpercentile25th, pharmacycostpercentile50th, '
                . 'pharmacycostpercentile75th, pharmacycostpercentile90th', 'default', 'setOnEmpty' => true,
                'value' => null),
            array('id, is_excluded, measure, treatment_id, inpat_outpat_services, treatment_friendly_id, '
                . 'is_bilateral_procedure, practice_group_id, practice_id, provider_id, quality_area_id, '
                . 'quality_factor_metric_id, quality_factor_value_id, number_of_episodes, aggregatecostprocedure, '
                . 'analysis_period_start, analysis_period_end, cost_tier, cost_factor_volume_id, '
                . 'cost_factor_volatility_id, aggregatecostaverage, aggregatecoststandarddeviation, '
                . 'aggregatecostpercentile10th, aggregatecostpercentile25th, aggregatecostpercentile50th, '
                . 'aggregatecostpercentile75th, aggregatecostpercentile90th, facilitycostaverage, '
                . 'facilitycoststandarddeviation, facilitycostpercentile10th, facilitycostpercentile25th, '
                . 'facilitycostpercentile50th, facilitycostpercentile75th, facilitycostpercentile90th, '
                . 'anesthesiacostaverage, anesthesiacoststandarddeviation, anesthesiacostpercentile10th, '
                . 'anesthesiacostpercentile25th, anesthesiacostpercentile50th, anesthesiacostpercentile75th, '
                . 'anesthesiacostpercentile90th, radiologycostaverage, radiologycoststandarddeviation, '
                . 'radiologycostpercentile10th, radiologycostpercentile25th, radiologycostpercentile50th, '
                . 'radiologycostpercentile75th, radiologycostpercentile90th, pathologycostaverage, '
                . 'pathologycoststandarddeviation, pathologycostpercentile10th, pathologycostpercentile25th, '
                . 'pathologycostpercentile50th, pathologycostpercentile75th, pathologycostpercentile90th, '
                . 'professionalcostaverage, professionalcoststandarddeviation, professionalcostpercentile10th, '
                . 'professionalcostpercentile25th, professionalcostpercentile50th, professionalcostpercentile75th, '
                . 'professionalcostpercentile90th, ancillarycostaverage, ancillarycoststandarddeviation, '
                . 'ancillarycostpercentile10th, ancillarycostpercentile25th, ancillarycostpercentile50th,'
                . ' ancillarycostpercentile75th, ancillarycostpercentile90th, pharmacycostaverage, '
                . 'pharmacycoststandarddeviation, pharmacycostpercentile10th, pharmacycostpercentile25th, '
                . 'pharmacycostpercentile50th, pharmacycostpercentile75th, pharmacycostpercentile90th',
                'safe', 'on' => 'search'),
        );
    }

    public function relations()
    {
        return array(
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'is_excluded' => Yii::t('app', 'Is Excluded'),
            'measure' => Yii::t('app', 'Measure'),
            'treatment_id' => Yii::t('app', 'Treatment'),
            'inpat_outpat_services' => Yii::t('app', 'Inpat Outpat Services'),
            'treatment_friendly_id' => Yii::t('app', 'Treatment Friendly'),
            'is_bilateral_procedure' => Yii::t('app', 'Is Bilateral Procedure'),
            'practice_group_id' => Yii::t('app', 'Practice Group'),
            'practice_id' => Yii::t('app', 'Practice'),
            'provider_id' => Yii::t('app', 'Provider'),
            'quality_area_id' => Yii::t('app', 'Quality Area'),
            'quality_factor_metric_id' => Yii::t('app', 'Quality Factor Metric'),
            'quality_factor_value_id' => Yii::t('app', 'Quality Factor Value'),
            'number_of_episodes' => Yii::t('app', 'Number Of Episodes'),
            'aggregatecostprocedure' => Yii::t('app', 'Aggregatecostprocedure'),
            'analysis_period_start' => Yii::t('app', 'Analysis Period Start'),
            'analysis_period_end' => Yii::t('app', 'Analysis Period End'),
            'cost_tier' => Yii::t('app', 'Cost Tier'),
            'cost_factor_volume_id' => Yii::t('app', 'Cost Factor Volume'),
            'cost_factor_volatility_id' => Yii::t('app', 'Cost Factor Volatility'),
            'aggregatecostaverage' => Yii::t('app', 'Aggregatecostaverage'),
            'aggregatecoststandarddeviation' => Yii::t('app', 'Aggregatecoststandarddeviation'),
            'aggregatecostpercentile10th' => Yii::t('app', 'Aggregatecostpercentile10th'),
            'aggregatecostpercentile25th' => Yii::t('app', 'Aggregatecostpercentile25th'),
            'aggregatecostpercentile50th' => Yii::t('app', 'Aggregatecostpercentile50th'),
            'aggregatecostpercentile75th' => Yii::t('app', 'Aggregatecostpercentile75th'),
            'aggregatecostpercentile90th' => Yii::t('app', 'Aggregatecostpercentile90th'),
            'facilitycostaverage' => Yii::t('app', 'Facilitycostaverage'),
            'facilitycoststandarddeviation' => Yii::t('app', 'Facilitycoststandarddeviation'),
            'facilitycostpercentile10th' => Yii::t('app', 'Facilitycostpercentile10th'),
            'facilitycostpercentile25th' => Yii::t('app', 'Facilitycostpercentile25th'),
            'facilitycostpercentile50th' => Yii::t('app', 'Facilitycostpercentile50th'),
            'facilitycostpercentile75th' => Yii::t('app', 'Facilitycostpercentile75th'),
            'facilitycostpercentile90th' => Yii::t('app', 'Facilitycostpercentile90th'),
            'anesthesiacostaverage' => Yii::t('app', 'Anesthesiacostaverage'),
            'anesthesiacoststandarddeviation' => Yii::t('app', 'Anesthesiacoststandarddeviation'),
            'anesthesiacostpercentile10th' => Yii::t('app', 'Anesthesiacostpercentile10th'),
            'anesthesiacostpercentile25th' => Yii::t('app', 'Anesthesiacostpercentile25th'),
            'anesthesiacostpercentile50th' => Yii::t('app', 'Anesthesiacostpercentile50th'),
            'anesthesiacostpercentile75th' => Yii::t('app', 'Anesthesiacostpercentile75th'),
            'anesthesiacostpercentile90th' => Yii::t('app', 'Anesthesiacostpercentile90th'),
            'radiologycostaverage' => Yii::t('app', 'Radiologycostaverage'),
            'radiologycoststandarddeviation' => Yii::t('app', 'Radiologycoststandarddeviation'),
            'radiologycostpercentile10th' => Yii::t('app', 'Radiologycostpercentile10th'),
            'radiologycostpercentile25th' => Yii::t('app', 'Radiologycostpercentile25th'),
            'radiologycostpercentile50th' => Yii::t('app', 'Radiologycostpercentile50th'),
            'radiologycostpercentile75th' => Yii::t('app', 'Radiologycostpercentile75th'),
            'radiologycostpercentile90th' => Yii::t('app', 'Radiologycostpercentile90th'),
            'pathologycostaverage' => Yii::t('app', 'Pathologycostaverage'),
            'pathologycoststandarddeviation' => Yii::t('app', 'Pathologycoststandarddeviation'),
            'pathologycostpercentile10th' => Yii::t('app', 'Pathologycostpercentile10th'),
            'pathologycostpercentile25th' => Yii::t('app', 'Pathologycostpercentile25th'),
            'pathologycostpercentile50th' => Yii::t('app', 'Pathologycostpercentile50th'),
            'pathologycostpercentile75th' => Yii::t('app', 'Pathologycostpercentile75th'),
            'pathologycostpercentile90th' => Yii::t('app', 'Pathologycostpercentile90th'),
            'professionalcostaverage' => Yii::t('app', 'Professionalcostaverage'),
            'professionalcoststandarddeviation' => Yii::t('app', 'Professionalcoststandarddeviation'),
            'professionalcostpercentile10th' => Yii::t('app', 'Professionalcostpercentile10th'),
            'professionalcostpercentile25th' => Yii::t('app', 'Professionalcostpercentile25th'),
            'professionalcostpercentile50th' => Yii::t('app', 'Professionalcostpercentile50th'),
            'professionalcostpercentile75th' => Yii::t('app', 'Professionalcostpercentile75th'),
            'professionalcostpercentile90th' => Yii::t('app', 'Professionalcostpercentile90th'),
            'ancillarycostaverage' => Yii::t('app', 'Ancillarycostaverage'),
            'ancillarycoststandarddeviation' => Yii::t('app', 'Ancillarycoststandarddeviation'),
            'ancillarycostpercentile10th' => Yii::t('app', 'Ancillarycostpercentile10th'),
            'ancillarycostpercentile25th' => Yii::t('app', 'Ancillarycostpercentile25th'),
            'ancillarycostpercentile50th' => Yii::t('app', 'Ancillarycostpercentile50th'),
            'ancillarycostpercentile75th' => Yii::t('app', 'Ancillarycostpercentile75th'),
            'ancillarycostpercentile90th' => Yii::t('app', 'Ancillarycostpercentile90th'),
            'pharmacycostaverage' => Yii::t('app', 'Pharmacycostaverage'),
            'pharmacycoststandarddeviation' => Yii::t('app', 'Pharmacycoststandarddeviation'),
            'pharmacycostpercentile10th' => Yii::t('app', 'Pharmacycostpercentile10th'),
            'pharmacycostpercentile25th' => Yii::t('app', 'Pharmacycostpercentile25th'),
            'pharmacycostpercentile50th' => Yii::t('app', 'Pharmacycostpercentile50th'),
            'pharmacycostpercentile75th' => Yii::t('app', 'Pharmacycostpercentile75th'),
            'pharmacycostpercentile90th' => Yii::t('app', 'Pharmacycostpercentile90th'),
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('is_excluded', $this->is_excluded);
        $criteria->compare('measure', $this->measure);
        $criteria->compare('treatment_id', $this->treatment_id);
        $criteria->compare('inpat_outpat_services', $this->inpat_outpat_services, true);
        $criteria->compare('treatment_friendly_id', $this->treatment_friendly_id);
        $criteria->compare('is_bilateral_procedure', $this->is_bilateral_procedure);
        $criteria->compare('practice_group_id', $this->practice_group_id);
        $criteria->compare('practice_id', $this->practice_id);
        $criteria->compare('provider_id', $this->provider_id);
        $criteria->compare('quality_area_id', $this->quality_area_id);
        $criteria->compare('quality_factor_metric_id', $this->quality_factor_metric_id);
        $criteria->compare('quality_factor_value_id', $this->quality_factor_value_id);
        $criteria->compare('number_of_episodes', $this->number_of_episodes);
        $criteria->compare('aggregatecostprocedure', $this->aggregatecostprocedure);
        $criteria->compare('analysis_period_start', $this->analysis_period_start, true);
        $criteria->compare('analysis_period_end', $this->analysis_period_end, true);
        $criteria->compare('cost_tier', $this->cost_tier, true);
        $criteria->compare('cost_factor_volume_id', $this->cost_factor_volume_id);
        $criteria->compare('cost_factor_volatility_id', $this->cost_factor_volatility_id);
        $criteria->compare('aggregatecostaverage', $this->aggregatecostaverage);
        $criteria->compare('aggregatecoststandarddeviation', $this->aggregatecoststandarddeviation);
        $criteria->compare('aggregatecostpercentile10th', $this->aggregatecostpercentile10th);
        $criteria->compare('aggregatecostpercentile25th', $this->aggregatecostpercentile25th);
        $criteria->compare('aggregatecostpercentile50th', $this->aggregatecostpercentile50th);
        $criteria->compare('aggregatecostpercentile75th', $this->aggregatecostpercentile75th);
        $criteria->compare('aggregatecostpercentile90th', $this->aggregatecostpercentile90th);
        $criteria->compare('facilitycostaverage', $this->facilitycostaverage);
        $criteria->compare('facilitycoststandarddeviation', $this->facilitycoststandarddeviation);
        $criteria->compare('facilitycostpercentile10th', $this->facilitycostpercentile10th);
        $criteria->compare('facilitycostpercentile25th', $this->facilitycostpercentile25th);
        $criteria->compare('facilitycostpercentile50th', $this->facilitycostpercentile50th);
        $criteria->compare('facilitycostpercentile75th', $this->facilitycostpercentile75th);
        $criteria->compare('facilitycostpercentile90th', $this->facilitycostpercentile90th);
        $criteria->compare('anesthesiacostaverage', $this->anesthesiacostaverage);
        $criteria->compare('anesthesiacoststandarddeviation', $this->anesthesiacoststandarddeviation);
        $criteria->compare('anesthesiacostpercentile10th', $this->anesthesiacostpercentile10th);
        $criteria->compare('anesthesiacostpercentile25th', $this->anesthesiacostpercentile25th);
        $criteria->compare('anesthesiacostpercentile50th', $this->anesthesiacostpercentile50th);
        $criteria->compare('anesthesiacostpercentile75th', $this->anesthesiacostpercentile75th);
        $criteria->compare('anesthesiacostpercentile90th', $this->anesthesiacostpercentile90th);
        $criteria->compare('radiologycostaverage', $this->radiologycostaverage);
        $criteria->compare('radiologycoststandarddeviation', $this->radiologycoststandarddeviation);
        $criteria->compare('radiologycostpercentile10th', $this->radiologycostpercentile10th);
        $criteria->compare('radiologycostpercentile25th', $this->radiologycostpercentile25th);
        $criteria->compare('radiologycostpercentile50th', $this->radiologycostpercentile50th);
        $criteria->compare('radiologycostpercentile75th', $this->radiologycostpercentile75th);
        $criteria->compare('radiologycostpercentile90th', $this->radiologycostpercentile90th);
        $criteria->compare('pathologycostaverage', $this->pathologycostaverage);
        $criteria->compare('pathologycoststandarddeviation', $this->pathologycoststandarddeviation);
        $criteria->compare('pathologycostpercentile10th', $this->pathologycostpercentile10th);
        $criteria->compare('pathologycostpercentile25th', $this->pathologycostpercentile25th);
        $criteria->compare('pathologycostpercentile50th', $this->pathologycostpercentile50th);
        $criteria->compare('pathologycostpercentile75th', $this->pathologycostpercentile75th);
        $criteria->compare('pathologycostpercentile90th', $this->pathologycostpercentile90th);
        $criteria->compare('professionalcostaverage', $this->professionalcostaverage);
        $criteria->compare('professionalcoststandarddeviation', $this->professionalcoststandarddeviation);
        $criteria->compare('professionalcostpercentile10th', $this->professionalcostpercentile10th);
        $criteria->compare('professionalcostpercentile25th', $this->professionalcostpercentile25th);
        $criteria->compare('professionalcostpercentile50th', $this->professionalcostpercentile50th);
        $criteria->compare('professionalcostpercentile75th', $this->professionalcostpercentile75th);
        $criteria->compare('professionalcostpercentile90th', $this->professionalcostpercentile90th);
        $criteria->compare('ancillarycostaverage', $this->ancillarycostaverage);
        $criteria->compare('ancillarycoststandarddeviation', $this->ancillarycoststandarddeviation);
        $criteria->compare('ancillarycostpercentile10th', $this->ancillarycostpercentile10th);
        $criteria->compare('ancillarycostpercentile25th', $this->ancillarycostpercentile25th);
        $criteria->compare('ancillarycostpercentile50th', $this->ancillarycostpercentile50th);
        $criteria->compare('ancillarycostpercentile75th', $this->ancillarycostpercentile75th);
        $criteria->compare('ancillarycostpercentile90th', $this->ancillarycostpercentile90th);
        $criteria->compare('pharmacycostaverage', $this->pharmacycostaverage);
        $criteria->compare('pharmacycoststandarddeviation', $this->pharmacycoststandarddeviation);
        $criteria->compare('pharmacycostpercentile10th', $this->pharmacycostpercentile10th);
        $criteria->compare('pharmacycostpercentile25th', $this->pharmacycostpercentile25th);
        $criteria->compare('pharmacycostpercentile50th', $this->pharmacycostpercentile50th);
        $criteria->compare('pharmacycostpercentile75th', $this->pharmacycostpercentile75th);
        $criteria->compare('pharmacycostpercentile90th', $this->pharmacycostpercentile90th);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
