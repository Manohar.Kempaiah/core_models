<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "account_directory".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AccountDirectory".
 *
 * Columns in table "account_directory" available as properties of the model,
 * followed by relations of table "account_directory" available as properties of the model.
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $directory_role_id
 * @property string $access_token
 * @property string $last_access
 * @property string $date_added
 * @property string $date_updated
 *
 * @property Account $account
 * @property DirectoryRole $directoryRole
 */
abstract class BaseAccountDirectory extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getAccountId (): int
    {
        return $this->account_id;
    }

    public function tableName()
    {
        return 'account_directory';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'AccountDirectory|AccountDirectories', $n);
    }

    public static function representingColumn()
    {
        return 'access_token';
    }

    public function rules()
    {
        return array(
            array('account_id, directory_role_id', 'required'),
            array('account_id, directory_role_id', 'numerical', 'integerOnly'=>true),
            array('access_token', 'length', 'max'=>254),
            array('last_access, date_added, date_updated', 'safe'),
            array('access_token, last_access, date_added, date_updated', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, account_id, directory_role_id, access_token, last_access, date_added, date_updated', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'directoryRole' => array(self::BELONGS_TO, 'DirectoryRole', 'directory_role_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'account_id' => null,
            'directory_role_id' => null,
            'access_token' => Yii::t('app', 'Access Token'),
            'last_access' => Yii::t('app', 'Last Access'),
            'date_added' => Yii::t('app', 'Date Added'),
            'date_updated' => Yii::t('app', 'Date Updated'),
            'account' => null,
            'directoryRole' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('directory_role_id', $this->directory_role_id);
        $criteria->compare('access_token', $this->access_token, true);
        $criteria->compare('last_access', $this->last_access, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
