<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "directory_base_component_type".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "DirectoryBaseComponentType".
 *
 * Columns in table "directory_base_component_type" available as properties of the model,
 * followed by relations of table "directory_base_component_type" available as properties of the model.
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property DirectoryBaseComponentTemplate[] $directoryBaseComponentTemplates
 */
abstract class BaseDirectoryBaseComponentType extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'directory_base_component_type';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'DirectoryBaseComponentType|DirectoryBaseComponentTypes', $n);
    }

    public static function representingColumn()
    {
        return 'name';
    }

    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'length', 'max'=>20),
            array('description', 'length', 'max'=>100),
            array('description', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, name, description', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'directoryBaseComponentTemplates' => array(self::HAS_MANY, 'DirectoryBaseComponentTemplate', 'type_id'),
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'directoryBaseComponentTemplates' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('description', $this->description, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

}
