<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "provider_practice_directory".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "ProviderPracticeDirectory".
 *
 * Columns in table "provider_practice_directory" available as properties of the model,
 * followed by relations of table "provider_practice_directory" available as properties of the model.
 *
 * @property integer $id
 * @property integer $directory_setting_id
 * @property integer $provider_id
 * @property integer $practice_id
 * @property string $provider_bio
 * @property string $provider_photo_path
 * @property string $specialty
 * @property Provider $provider
 * @property Practice $practice
 *
 * @property DirectorySetting $directorySetting
 */
abstract class BaseProviderPracticeDirectory extends BaseModel
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'provider_practice_directory';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'ProviderPracticeDirectory|ProviderPracticeDirectories', $n);
    }

    public static function representingColumn()
    {
        return 'provider_bio';
    }

    public function rules()
    {
        return array(
            array('directory_setting_id', 'required'),
            array('directory_setting_id, provider_id, practice_id', 'numerical', 'integerOnly' => true),
            array('provider_photo_path', 'length', 'max' => 255),
            array('specialty', 'length', 'max' => 100),
            array('provider_bio', 'safe'),
            array(
                'provider_id, practice_id, provider_bio, provider_photo_path, specialty',
                'default',
                'setOnEmpty' => true,
                'value' => null
            ),
            array(
                'id, directory_setting_id, provider_id, practice_id, provider_bio, provider_photo_path',
                'safe',
                'on' => 'search'
            ),
        );
    }

    public function relations()
    {
        return array(
            'provider' => array(self::BELONGS_TO, 'Provider', 'provider_id'),
            'practice' => array(self::BELONGS_TO, 'Practice', 'practice_id'),
            'directorySetting' => array(self::BELONGS_TO, 'DirectorySetting', 'directory_setting_id'),
        );
    }

    public function pivotModels()
    {
        return array();
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'provider' => null,
            'practice' => null,
            'directory_setting_id' => null,
            'provider_id' => Yii::t('app', 'Provider'),
            'practice_id' => Yii::t('app', 'Practice'),
            'provider_bio' => Yii::t('app', 'Provider Bio'),
            'provider_photo_path' => Yii::t('app', 'Provider Photo Path'),
            'specialty' => Yii::t('app', 'Specialty'),
            'directorySetting' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('directory_setting_id', $this->directory_setting_id);
        $criteria->compare('provider_id', $this->provider_id);
        $criteria->compare('practice_id', $this->practice_id);
        $criteria->compare('provider_bio', $this->provider_bio, true);
        $criteria->compare('provider_photo_path', $this->provider_photo_path, true);
        $criteria->compare('specialty', $this->specialty, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }
}
