<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model base class for the table "account_practice".
 * DO NOT MODIFY THIS FILE! It is automatically generated by giix.
 * If any changes are necessary, you must set or override the required
 * property or method in class "AccountPractice".
 *
 * Columns in table "account_practice" available as properties of the model,
 * followed by relations of table "account_practice" available as properties of the model.
 *
 * @property integer $id
 * @property integer $account_id
 * @property integer $practice_id
 * @property integer $all_providers
 * @property string $date_added
 *
 * @property Account $account
 * @property Practice $practice
 */
abstract class BaseAccountPractice extends BaseModel
{
    public $account_full_name;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function tableName()
    {
        return 'account_practice';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'AccountPractice|AccountPractices', $n);
    }

    public static function representingColumn()
    {
        return 'date_added';
    }

    public function rules()
    {
        return array(
            array('account_id, practice_id', 'required'),
            array('account_id, practice_id, all_providers', 'numerical', 'integerOnly'=>true),
            array('date_added', 'safe'),
            array('all_providers, date_added, research_status', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, account_id, practice_id, all_providers, date_added, research_status', 'safe', 'on'=>'search'),
        );
    }

    public function relations()
    {
        return array(
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'practice' => array(self::BELONGS_TO, 'Practice', 'practice_id'),
        );
    }

    public function pivotModels()
    {
        return array();
    }

    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'account_id' => null,
            'practice_id' => null,
            'all_providers' => Yii::t('app', 'All Providers'),
            'date_added' => Yii::t('app', 'Date Added'),
            'research_status' => Yii::t('app', 'Research Status'),
            'account' => null,
            'practice' => null,
        );
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('practice_id', $this->practice_id);
        $criteria->compare('all_providers', $this->all_providers);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('research_status', $this->research_status);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }
}
