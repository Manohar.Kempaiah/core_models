<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model class for table "organization_history".
 *
 * The followings are the available columns in table 'organization_history':
 * @property integer $id
 * @property integer $organization_id
 * @property string $event
 * @property integer $feature_id
 * @property integer $account_id
 * @property integer $provider_id
 * @property integer $practice_id
 * @property string $date_added
 * @property string $date_updated
 *
 * The followings are the available model relations:
 * @property Account $account
 * @property Feature $feature
 * @property Organization $organization
 * @property Practice $practice
 * @property Provider $provider
 */
class BaseOrganizationHistory extends BaseModel
{

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'organization_history';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('organization_id', 'required'),
            array('organization_id, feature_id, account_id, provider_id, practice_id', 'numerical',
                'integerOnly' => true),
            array('event', 'length', 'max' => 2),
            array('date_added, date_updated', 'safe'),
            array('event, feature_id, account_id, provider_id, practice_id, date_added, date_updated',
                'default', 'setOnEmpty' => true, 'value' => null),
            array('id, organization_id, event, feature_id, account_id, provider_id, practice_id, date_added, '
                . 'date_updated', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'account' => array(self::BELONGS_TO, 'Account', 'account_id'),
            'feature' => array(self::BELONGS_TO, 'Feature', 'feature_id'),
            'organization' => array(self::BELONGS_TO, 'Organization', 'organization_id'),
            'practice' => array(self::BELONGS_TO, 'Practice', 'practice_id'),
            'provider' => array(self::BELONGS_TO, 'Provider', 'provider_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'organization_id' => 'Organization',
            'event' => 'Event',
            'feature_id' => 'Feature',
            'account_id' => 'Account',
            'provider_id' => 'Provider',
            'practice_id' => 'Practice',
            'date_added' => 'Date Added',
            'date_updated' => 'Date Updated',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('organization_id', $this->organization_id);
        $criteria->compare('event', $this->event, true);
        $criteria->compare('feature_id', $this->feature_id);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('provider_id', $this->provider_id);
        $criteria->compare('practice_id', $this->practice_id);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return BaseOrganizationHistory the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
