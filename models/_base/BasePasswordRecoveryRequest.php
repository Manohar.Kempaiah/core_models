<?php

Yii::import('application.modules.core_models.models._base.BaseModel');

/**
 * This is the model class for table "password_recovery_request".
 *
 * The followings are the available columns in table 'password_recovery_request':
 * @property integer $id
 * @property integer $account_id
 * @property string $account_type
 * @property string $hash
 * @property string $date_added
 * @property string $date_updated
 * @property integer $enabled
 */
abstract class BasePasswordRecoveryRequest extends BaseModel
{

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return PasswordRecoveryRequest the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'password_recovery_request';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'PasswordRecoveryRequest|PasswordRecoveryRequests', $n);
    }

    public static function representingColumn()
    {
        return 'hash';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('account_id, hash, date_added', 'required'),
            array('account_id, enabled', 'numerical', 'integerOnly' => true),
            array('account_type', 'length', 'max' => 10),
            array('hash', 'length', 'max' => 32),
            array('date_updated', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, account_id, account_type, hash, date_added, date_updated, enabled', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
        );
    }

    public function pivotModels()
    {
        return array(
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'account_id' => 'Account',
            'account_type' => 'Account Type',
            'hash' => 'Hash',
            'date_added' => 'Date Added',
            'date_updated' => 'Date Updated',
            'enabled' => 'Enabled',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('account_id', $this->account_id);
        $criteria->compare('account_type', $this->account_type, true);
        $criteria->compare('hash', $this->hash, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('enabled', $this->enabled);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

}
