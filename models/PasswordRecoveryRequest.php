<?php

Yii::import('application.modules.core_models.models._base.BasePasswordRecoveryRequest');

class PasswordRecoveryRequest extends BasePasswordRecoveryRequest
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Given an $account object or a $patient object, generate a hash to save to the DB and send via email for
     * recovering the object's password
     * @param mixed $object instanceof Patient or Account array
     * @return mixed
     */
    public static function generateHash($object = null)
    {

        if (!$object) {
            return false;
        }

        // find account data - careful, account isn't an instanceof Account because it's used for replacement
        // (see Mail::getAccount)
        if ($object instanceof Patient) {
            $accountType = 'patient';
        } else {
            $accountType = 'account';
        }

        $accountId = $object['id'];

        self::disableOldRequests($accountId, $accountType);

        // generate actual hash
        $hash = Yii::app()->getSecurityManager()->generateRandomString(32, true);

        // create new request
        $passwordRecoveryRequest = new PasswordRecoveryRequest;
        $passwordRecoveryRequest->account_id = $accountId;
        $passwordRecoveryRequest->account_type = $accountType;
        $passwordRecoveryRequest->date_added = date('Y-m-d H:i:s');
        $passwordRecoveryRequest->hash = $hash;
        $passwordRecoveryRequest->enabled = 1;
        if ($passwordRecoveryRequest->save()) {
            // return the hash
            return $hash;
        }
        return false;
    }

    /**
     * Mark all old requests as disabled
     * @param int $accountId
     * @param string $accountType
     * @return boolean
     */
    public static function disableOldRequests($accountId, $accountType)
    {

        $oldRequests = PasswordRecoveryRequest::model()->findAll(
            'account_id=:account_id AND account_type=:account_type AND enabled = 1',
            array(
                ':account_id' => $accountId,
                ':account_type' => $accountType
            )
        );
        foreach ($oldRequests as $oldRequest) {
            $oldRequest->enabled = 0;
            $oldRequest->date_updated = date('Y-m-d H:i:s');
            $oldRequest->save();
        }
        return true;
    }

}
