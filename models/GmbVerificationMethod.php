<?php

Yii::import('application.modules.core_models.models._base.BaseGmbVerificationMethod');

class GmbVerificationMethod extends BaseGmbVerificationMethod
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public static function getId($methodName)
    {
        $methods = static::model()->cache(Yii::app()->params['cache_long'])->findAll();

        foreach ($methods as $method) {
            if ($method->name == $methodName) {
                return $method->id;
            }
        }

        return null;
    }
}
