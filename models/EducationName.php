<?php

Yii::import('application.modules.core_models.models._base.BaseEducationName');

class EducationName extends BaseEducationName
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from education_name if associated records exist in provider_education table
        $has_associated_records = ProviderEducation::model()->exists(
            'education_name_id=:education_name_id',
            array(
                ':education_name_id' => $this->id
            )
        );
        if ($has_associated_records) {
            $this->addError("id", "Education name can't be deleted because there are providers linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Get the complete education name
     * @return string
     */
    public function getFullEducationName()
    {

        $education = EducationName::model()->findByPk($this->id);
        return addslashes($education->name);
    }

    public function rules()
    {
        $rules = parent::rules();
        $rules[] = array('name', 'unique');

        return $rules;
    }

}
