<?php

Yii::import('application.modules.core_models.models._base.BaseScanTemplateType');

class ScanTemplateType extends BaseScanTemplateType
{

    // defined here so we don't depend on the internal values all over the place
    const TYPE_SALES = "Sales";
    const TYPE_SALES_ID = 1;
    const TYPE_INTERNAL = "Internal";
    const TYPE_INTERNAL_ID = 2;
    const TYPE_PROSPECT = 'Prospect';
    const TYPE_PROSPECT_ID = 9;

    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from scan template type if associated records exist in scan template table
        $hasAssociatedRecords = ScanTemplate::model()->exists(
            'scan_template_type_id=:scan_template_type_id',
            array(':scan_template_type_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError(
                'id',
                "The Scan Template cannot be removed because it's linked to a Scan Templates."
            );
            return false;
        }

        // avoid deleting from scan template type if associated records exist in partner site table
        $hasAssociatedRecords = PartnerSite::model()->exists(
            'scan_template_type_id=:scan_template_type_id',
            array(':scan_template_type_id' => $this->id)
        );
        if ($hasAssociatedRecords) {
            $this->addError(
                'id',
                "The Scan Template cannot be removed because it's linked to a Partner Site."
            );
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Before Save
     * @return boolean
     */
    public function beforeSave()
    {

        // is this an existing record?
        if ($this->id > 0) {
            // yes, check if the name already exists
            $duplicateName = ScanTemplateType::model()->exists(
                'name = :name AND id != :id',
                array(
                    ':name' => $this->name,
                    ':id' => $this->id
                )
            );
        } else {
            // no, check if the name already exists
            $duplicateName = ScanTemplateType::model()->exists(
                'name = :name',
                array(
                    ':name' => $this->name
                )
            );
        }

        // was it a duplicate?
        if ($duplicateName) {
            // yes, exit
            $this->addError('name', 'Duplicate name');
            return false;
        }

        return parent::beforeSave();
    }
}
