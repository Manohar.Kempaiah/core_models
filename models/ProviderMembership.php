<?php

Yii::import('application.modules.core_models.models._base.BaseProviderMembership');

class ProviderMembership extends BaseProviderMembership
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id
                . '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="'
                . $this->provider . '">' . $this->provider . '</a>');
    }

    /**
     * Checks for valid dates before saving
     * @return boolean
     */
    public function beforeSave()
    {
        if ($this->from_date == 0 || $this->from_date == '') {
            $this->from_date = null;
        }
        if ($this->to_date == 0 || $this->to_date == '' || $this->to_date == 'Present' || $this->to_date == 'yyyy') {
            $this->to_date = null;
        }
        return parent::beforeSave();
    }

    /**
     * Gets details about a provider's memberships
     * @param int $providerId
     * @return array
     */
    public static function getDetails($providerId, $cache = false)
    {

        $sql = sprintf(
            "SELECT pm.id, pm.provider_id, pm.position, pm.organization,
            pm.from_date, pm.to_date, pm.description
            FROM provider_membership AS pm
            WHERE pm.provider_id = %d
            ORDER BY pm.from_date, pm.to_date DESC;",
            $providerId
        );

        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Save Data
     * @param string $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        foreach ($postData as $data) {

            if (empty($data['position']) || empty($data['organization'])) {
                continue;
            }

            if (isset($data['backend_action']) && $data['backend_action'] == 'delete' && $data['id'] > 0) {
                $model = ProviderMembership::model()->findByPk($data['id']);
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_MEMBERSHIP";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }

                    $auditSection = 'ProviderMembership #' . $providerId .' (#' . $model->organization . ')';
                    AuditLog::create('D', $auditSection, 'provider_membership', null, false);

                }
            } else {

                if ($data['id'] > 0) {
                    $model = ProviderMembership::model()->findByPk($data['id']);
                    $auditAction = 'U';
                } else {
                    $auditAction = 'C';
                    $model = new ProviderMembership;
                }
                $model->attributes = $data;
                $model->provider_id = $providerId;

                if (empty($model->from_date)) {
                    $model->to_date = null;
                }

                if (!$model->save()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_MEMBERSHIP";
                    $results['data'] = $model->getErrors();
                    return $results;
                } else {
                    if ($auditAction == 'C') {
                        // return the new item
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                }

                $auditSection = 'ProviderMembership #' . $providerId .' (#' . $model->organization . ')';
                AuditLog::create($auditAction, $auditSection, 'provider_membership', null, false);
            }

        }
        return $results;
    }

}
