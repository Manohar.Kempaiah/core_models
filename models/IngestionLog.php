<?php

Yii::import('application.modules.core_models.models._base.BaseIngestionLog');

class IngestionLog extends BaseIngestionLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Adds an error message to the ingestion log record
     * @param string $sourceModel
     * @param array $arrErrors
     * @param string $status
     */
    public function addErrorMessage($sourceModel, $arrErrors, $status = 'R')
    {
        // initialize array of existing errors
        $arrMessage = array();

        // previous errors exist for the record?
        // yes
        if (!empty($this->message)) {
            // deserialize errors into array
            $arrMessage = json_decode($this->message, true);
        }

        // add error to the list of messages
        $arrMessage[$sourceModel] = $arrErrors;

        // set the record to error status
        $this->status = $status;

        // change date_updated
        $this->date_updated = date('Y-m-d H:i:s');

        // save error messages
        $this->message = json_encode($arrMessage);

        // save record
        $this->save();
    }

}
