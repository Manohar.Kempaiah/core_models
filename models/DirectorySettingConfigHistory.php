<?php

Yii::import('application.modules.core_models.models._base.BaseDirectorySettingConfigHistory');

class DirectorySettingConfigHistory extends BaseDirectorySettingConfigHistory
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
