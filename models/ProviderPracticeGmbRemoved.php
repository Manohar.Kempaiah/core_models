<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeGmbRemoved');

class ProviderPracticeGmbRemoved extends BaseProviderPracticeGmbRemoved
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * GMB Link
     * @return raw HTML google link
     */
    public function getGmbLink()
    {
        return (
            '<a target="_blank" href="https://www.google.com/maps/place/?q=place_id:' . $this->place_id . '">' .
                $this->place_id . '</a>'
        );
    }

    /**
     * Provider Link
     * @return raw HTML Provider link
     */
    public function getProviderLink()
    {
        if ($this->provider) {
            return (
                '<a target="_blank" href="https://' . Yii::app()->params->servers['dr_web_hn'] . '/' .
                    $this->provider->friendly_url . '">' . $this->provider_id . '</a>'
            );
        }
        return $this->provider_id;
    }

    /**
     * Practice Link
     * @return raw HTML Practice link
     */
    public function getPracticeLink()
    {
        if ($this->practice) {
            return (
                '<a target="_blank" href="https://' . Yii::app()->params->servers['dr_web_hn'] .
                    $this->practice->friendly_url . '">' . $this->practice_id . '</a>'
            );
        }
        return $this->practice_id;
    }

    /**
     * Account Details
     * @return raw Account Details
     */
    public function getAccountDetails()
    {
        if ($this->account) {
            $account = $this->account;
            return (
                '<a class="commentTooltip" title="'
                    . '#' . $account->id . " " . $account->first_name . " " . $account->last_name . " (" . $account->email . ")" . '">'
                    . $account->id . '</a>'
            );
        }
        return $this->account_id;
    }
}
