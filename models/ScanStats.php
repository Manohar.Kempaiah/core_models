<?php

Yii::import('application.modules.core_models.models._base.BaseScanStats');

class ScanStats extends BaseScanStats
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function rules()
    {
        return array(
            array('scan_request_id', 'required'),
            array('scan_request_id, op_score, aggregate_accuracy, total_reviews, average_provider_accuracy, average_provider_reviews, data_elements, website_mobile, completed_scans, cnt_errors', 'numerical', 'integerOnly' => true),
            array('aggregate_rating', 'length', 'max' => 10),
            array('reviews_grade', 'length', 'max' => 5),
            array('date_added', 'safe'),
            array('op_score, aggregate_accuracy, aggregate_rating, total_reviews, average_provider_accuracy, average_provider_reviews, reviews_grade, data_elements, date_added, website_mobile, completed_scans, cnt_errors', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, scan_request_id, op_score, aggregate_accuracy, aggregate_rating, total_reviews, average_provider_accuracy, average_provider_reviews, reviews_grade, data_elements, date_added, website_mobile, completed_scans, cnt_errors', 'safe', 'on' => 'search'),
        );
    }
}
