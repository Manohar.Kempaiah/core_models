<?php

Yii::import('application.modules.core_models.models._base.BaseEmrType');

class EmrType extends BaseEmrType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Return list of emr_type to use in combo select
     * @return arr
     */
    public static function getAllEmrTypes()
    {
        $sql = "SELECT * FROM emr_type ORDER BY name ASC";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Return emr_type record
     * @param int $emrTypeId
     * @return arr
     */
    public static function getEmrType($emrTypeId = 0)
    {
        return EmrType::model()->find('id=:id', array(':id' => $emrTypeId));
    }

}
