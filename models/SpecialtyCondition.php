<?php

Yii::import('application.modules.core_models.models._base.BaseSpecialtyCondition');

class SpecialtyCondition extends BaseSpecialtyCondition
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Given an array of specialty IDs, return conditions that that specialty treats
     * @param array $arrSpecialtyIds
     * @param array $arrSubSpecialtyIds
     * @return boolean
     */
    public static function getBySpecialtyIds($arrSpecialtyIds, $arrSubSpecialtyIds = array())
    {
        // do we have sub-specialties?
        if (!empty($arrSubSpecialtyIds)) {
            // yes, check using them

            // build comma-separated list of ids
            $arrSubSpecialtyIds = implode(', ', $arrSubSpecialtyIds);

            $sql = sprintf(
                "SELECT DISTINCT(condition.name)
                FROM specialty_condition
                INNER JOIN `condition`
                ON specialty_condition.condition_id = condition.id
                WHERE sub_specialty_id IN (%s);",
                $arrSubSpecialtyIds
            );
            $result = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
            // did we have results?
            if (!empty($result)) {
                // yes, return them
                return $result;
            }
        }

        // do we have specialties?
        if (empty($arrSpecialtyIds)) {
            // no, leave
            return false;
        }
        // build comma-separated list of ids
        $arrSpecialtyIds = implode(', ', $arrSpecialtyIds);

        $sql = sprintf(
            "SELECT DISTINCT(condition.name)
            FROM specialty_condition
            INNER JOIN `condition`
            ON specialty_condition.condition_id = condition.id
            WHERE specialty_id IN (%s);",
            $arrSpecialtyIds
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }

}
