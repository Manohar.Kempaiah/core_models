<?php

Yii::import('application.modules.core_models.models._base.BaseStatus');

class Status extends BaseStatus
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    public function getAllByGroup($groupCode)
    {
        $params = [
            ':group_code' => $groupCode
        ];

        return $this->with('statusGroup')->findAll("statusGroup.code = :group_code", $params);
    }

    public function getByCodes($code, $groupCode)
    {
        $params = [
            ':code' => $code,
            ':group_code' => $groupCode
        ];

        return $this->with('statusGroup')->find("t.code = :code AND statusGroup.code = :group_code", $params);
    }

    /**
     * Fetch one scan error status by it's code
     *
     * @param string $code
     *
     * @return Status
     */
    public function getScanErrorStatusByCode($code)
    {
        return $this->getByCodes($code, 'scan_error_status_group');
    }

    /**
     * Get all the statuses that belong to the ScanError group
     *
     * @return Status[]
     */
    public function getScanErrorStatuses()
    {
        return $this->getAllByGroup('scan_error_status_group');
    }

    /**
     * Get a research competitor status by it's code
     *
     * @param string $code
     * @return Status
     */
    public function getCompetitorStatusByCode($code)
    {
        return $this->getByCodes($code, 'research_competitors_practice');
    }
}
