<?php

Yii::import('application.modules.core_models.models._base.BaseProviderRatingAbuse');

class ProviderRatingAbuse extends BaseProviderRatingAbuse
{

    public $localization_id;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        $rating = ProviderRating::model()->findByPk($this->rating_id);
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $rating->provider_id . '" '
            . 'rel="/administration/providerTooltip?id=' . $rating->provider_id . '" title="' . $rating->provider . '">'
            . $rating->provider . '</a>');
    }

    /**
     * Declares the validation rules.
     * @return array
     */
    public function rules()
    {
        return array(
            array('rating_id, abuse_report', 'required'),
            array('rating_id', 'numerical', 'integerOnly' => true),
            array('abuse_report, status_comments', 'length', 'max' => 255),
            array('status', 'length', 'max' => 1),
            array('date_added, date_updated', 'safe'),
            array('status, status_comments, date_added, date_updated, status_rejected_reason',
                'default', 'setOnEmpty' => true, 'value' => null),
            array('id, rating_id, abuse_report, status, date_added, date_updated, localization_id,'
                . ' status_rejected_reason, status_comments', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        //this is to enable eager loading
        $criteria->with = array('rating');

        $criteria->compare('rating_id', $this->rating_id);
        $criteria->compare('abuse_report', $this->abuse_report, true);
        $criteria->compare('t.status', $this->status, true);
        $criteria->compare('status_comments', $this->status_comments, true);
        $criteria->compare('t.date_added', $this->date_added, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('rating.localization_id', $this->localization_id, true);

        return new CActiveDataProvider(get_class($this), array('criteria' => $criteria, 'pagination' => array('pageSize'
            => 100)));
    }

     /**
     * Get the review report abuse
     * @param int $reviewId
     * @return array
     */
    public static function getReviewReportAbuse($reviewId = 0)
    {
        $sql = sprintf(
            "SELECT id, rating_id, abuse_report, `status`, status_comments, date_added, date_updated
            FROM provider_rating_abuse
            WHERE rating_id = %d
            ORDER BY date_added DESC;",
            $reviewId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Before Save
     * @return boolean
     */
    public function beforeSave()
    {
        // If status Approved or Pending
        if ($this->status != 'R') {
            $this->status_rejected_reason = null;
            $this->status_comments = null;
        }

        // If is Rejected
        if ($this->status == "R") {
            // Reason is Required
            if ($this->status_rejected_reason == "") {
                $this->addError('status_rejected_reason', 'Status Rejected Reason is Required.');
                return false;
            }
            // IF Reason is 'S' or 'O', Comments are Required
            if (($this->status_rejected_reason == "S" || $this->status_rejected_reason == "O") &&
                    $this->status_comments == "") {
                $this->addError('status_comments', 'Status Comments is Required.');
                return false;
            }
        }

        return parent::beforeSave();
    }
}
