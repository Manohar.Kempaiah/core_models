<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPaymentType');

class ProviderPaymentType extends BaseProviderPaymentType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id .
            '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">' .
            $this->provider . '</a>');
    }

    /**
     * get this provider's payment types
     * @param int $providerId
     * @return array
     */
    public static function getProviderPaymentTypes($providerId = 0)
    {

        $sql = sprintf(
            "SELECT payment_type.id AS id, :providerId as provider_id, payment_type.id AS payment_type_id,
            IF(provider_payment_type.id IS NOT NULL, true, false) AS checked,
            payment_type.name, payment_type.class_name
            FROM payment_type
            LEFT JOIN provider_payment_type
            ON payment_type.id = provider_payment_type.payment_type_id
            AND provider_id = :providerId"
        );

        $command = Yii::app()->db->createCommand($sql);
        $command->bindParam(":providerId", $providerId, PDO::PARAM_INT);
        $arrProviderPT = $command->queryAll();

        $noneSelected = true;
        foreach ($arrProviderPT as $key => $value) {
            if ($value['checked'] == 1) {
                $noneSelected = false;
            }
            $arrProviderPT[$key]['name'] = strip_tags($value['name']);
        }

        if ($noneSelected) {
            foreach ($arrProviderPT as $key => $value) {
                if ($value['id'] == 1) {
                    // if none selected, then check Cash
                    $arrProviderPT[$key]['checked'] = 1;
                    break;
                }
            }
        }
        return $arrProviderPT;
    }

    /**
     * Return payment type of the providers related to this practice
     * @param integer $practiceId
     * @return array
     */
    public static function getByPracticeId($practiceId = 0)
    {
        // get providers at practice
        $sql = sprintf("SELECT provider_id FROM provider_practice WHERE practice_id = %d", $practiceId);
        $arrProviders = Yii::app()->db->createCommand($sql)->queryAll();
        if (empty($arrProviders)) {
            return [];
        }

        // get providers payment types
        $xPayments = [];
        foreach ($arrProviders as $p) {
            $xPayments[] = self::getProviderPaymentTypes($p['provider_id']);
        }
        if (empty($xPayments)) {
            return [];
        }
        $payments = [];
        foreach ($xPayments as $ppay) {
            foreach ($ppay as $pay) {
                $payments[$pay['payment_type_id']] = $pay['name'];
            }
        }
        return $payments;
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";


        // drop provider payment types and re-create
        $prevPayment = array();

        // old payment types
        $paymentTypes = ProviderPaymentType::model()->findAll(
            'provider_id=:provider_id',
            array(':provider_id' => $providerId)
        );
        foreach ($paymentTypes as $paymentType) {
            $prevPayment[$paymentType->payment_type_id] = 1;
        }

        $auditSection = 'Provider Payment Type provider_id #' . $providerId;
        AuditLog::create('D', $auditSection, 'provider_language', null, false);

        $auditList = '';
        if (!empty($postData)) {
            foreach ($postData as $v) {
                if ($v['checked'] == 1) {

                    // update
                    if (isset($prevPayment[$v['payment_type_id']])) {

                        // previous payment processed
                        $prevPayment[$v['payment_type_id']] = 2;
                    } else {
                        // new payment
                        $pPT = new ProviderPaymentType();
                        $pPT->provider_id = $providerId;
                        $pPT->payment_type_id = $v['payment_type_id'];
                        if ($pPT->save()) {
                            $auditList .= $v['payment_type_id'] . ' - ';
                            $results['success'] = true;
                            $results['error'] = "";
                        } else {
                            $results['success'] = false;
                            $results['error'] = "ERROR_SAVING_PROVIDER_PAYMENT_TYPE";
                            $results['data'] = $pPT->getErrors();
                            return $results;
                        }
                    }
                }
            }
        }

        // previous payment
        foreach ($prevPayment as $key => $value) {

            // without process
            if ($value == 1) {
                $arrProviderPaymentType = ProviderPaymentType::model()->find(
                    'provider_id = :provider_id AND payment_type_id = :payment_type_id',
                    array(':provider_id' => $providerId, ':payment_type_id' => $key)
                );
                // delete
                $arrProviderPaymentType->delete();
            }
        }

        $auditSection = 'Provider Payment Type provider_id #' . $providerId . ' (' . $auditList . ')';
        AuditLog::create('C', $auditSection, 'provider_payment_type', null, false);

        return $results;
    }
}
