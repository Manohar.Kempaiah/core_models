<?php

Yii::import('application.modules.core_models.models._base.BaseCpnSoftwareVersionLog');

class CpnSoftwareVersionLog extends BaseCpnSoftwareVersionLog
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Mark Installed
     *
     * @param obj $installation
     * @param str $softwareVersionId
     * @param int $successFlag
     * @param str $guid
     * @return array
     */
    public function markInstalled($installation, $softwareVersionId, $successFlag)
    {
        // Query to cpn_software_version
        $cpnSV = CpnSoftwareVersion::model()->cache(
            Yii::app()->params['cache_long'])->find(
                'guid=:guid',
                array(':guid' => $softwareVersionId)
        );

        if ($cpnSV && $softwareVersionId) {

            // get the real IP address
            $ipAddress = (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != '' ?
                    $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']);

            // Create new CpnSoftwareVersionLog
            $csvl = new CpnSoftwareVersionLog();
            $csvl->guid = Yii::app()->db->createCommand("SELECT uuid()")->queryScalar();
            $csvl->cpn_software_version_id = $cpnSV->id;
            $csvl->cpn_installation_id = $installation->id;
            $csvl->success_flag = ($successFlag == "true" ? 1 : 0);
            $csvl->added_by_account_id = $installation->added_by_account_id;
            $csvl->ip_address = $ipAddress;
            $csvl->del_flag = 0;
            $res = $csvl->save();

            // did the record saved successfully?
            // yes
            if ($res) {
                return true;
            }
        }

        return false;
    }

}
