<?php

Yii::import('application.modules.core_models.models._base.BaseTwilioBlockedNumbers');

class TwilioBlockedNumbers extends BaseTwilioBlockedNumbers
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Normalizes phone number to +1XXXXXXXXXX format prior
     * to saving to database
     *
     * @return boolean
     */
    public function beforeSave()
    {

        if (!StringComponent::isValidPhoneNumber($this->number)) {
            $this->addError('number', 'Invalid number: ' . $this->number);
            return false;
        }

        // normalize phone number
        $unformattedNumber = $this->number;
        $this->number = StringComponent::normalizePhoneNumber($unformattedNumber);
        if (!$this->number) {
            $this->addError('number', 'Could not normalize phone number: ' . $unformattedNumber);
            return false;
        }

        return parent::beforeSave();
    }

}
