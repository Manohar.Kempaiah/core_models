<?php

Yii::import('application.modules.core_models.models._base.BasePage');

class Page extends BasePage
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'title';
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('meta_noindex', $this->meta_noindex);
        $criteria->compare('friendly_url', $this->friendly_url, true);
        $criteria->compare('title', $this->title, true);
        $criteria->compare('meta_description', $this->meta_description, true);
        $criteria->compare('meta_keywords', $this->meta_keywords, true);
        $criteria->compare('meta_robots', $this->meta_robots, true);
        $criteria->compare('head_tags', $this->head_tags, true);
        $criteria->compare('headline', $this->headline, true);
        $criteria->compare('sub_headline', $this->sub_headline, true);
        $criteria->compare('body', $this->body, true);

        return new CActiveDataProvider(get_class($this), array('criteria' => $criteria, 'pagination' =>
            array('pageSize' => 100)));
    }

}
