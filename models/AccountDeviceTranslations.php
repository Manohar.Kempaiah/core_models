<?php

Yii::import('application.modules.core_models.models._base.BaseAccountDeviceTranslations');

class AccountDeviceTranslations extends BaseAccountDeviceTranslations
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'id';
    }

    /**
     * Declares the validation rules.
     * @return array
     */
    public function rules()
    {
        return array(
            array('account_device_id, lang_id', 'required')
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'account_device_id' => Yii::t('app', 'Account Device Id'),
            'lang_id' => Yii::t('app', 'Language')
        );
    }

    /**
     * Get personalized translations of account-device
     * @param int $accountDeviceId
     * @return array
     */
    public static function getTranslations($accountDeviceId = 0, $languageId = '')
    {
        if (empty($languageId)) {
            $sql = sprintf(
                "SELECT SQL_NO_CACHE *
                FROM account_device_translations AS adt
                WHERE account_device_id = %d",
                $accountDeviceId
            );

            $arrLanguages = Yii::app()->db->createCommand($sql)->queryAll();

            $languages = array();
            if ($arrLanguages) {
                foreach ($arrLanguages as $al) {
                    $languages[$al['lang_id']]['initial_cta'] = $al['initial_cta'];
                    $languages[$al['lang_id']]['email_subject'] = $al['email_subject'];
                    $languages[$al['lang_id']]['email_body'] = $al['email_body'];
                }

            } else {
                // if empty, use all available languages
                $availableLanguages = Common::getAvailableLanguages();
                foreach ($availableLanguages as $key => $value) {
                    $languages[$key]['initial_cta'] = '';
                    $languages[$key]['email_subject'] = '';
                    $languages[$key]['email_body'] = '';
                }
            }
            return $languages;
        } else {
            $sql = sprintf(
                "SELECT *
                FROM account_device_translations AS adt
                WHERE account_device_id = %d
                AND lang_id='%s'",
                $accountDeviceId,
                $languageId
            );

            $arrLanguages = Yii::app()->db->createCommand($sql)->queryAll();
            if ($arrLanguages) {
                $languages['initial_cta'] = $arrLanguages[0]['initial_cta'];
                $languages['email_subject'] = $arrLanguages[0]['email_subject'];
                $languages['email_body'] = $arrLanguages[0]['email_body'];
            } else {
                // if have no translations, return empty array
                $languages['initial_cta'] = '';
                $languages['email_body'] = '';
                $languages['email_subject'] = '';
            }
            return $languages;
        }
    }

}
