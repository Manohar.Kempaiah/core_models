<?php

Yii::import('application.modules.core_models.models._base.BaseOrganizationFeature');

// autoload vendors class
Common::autoloadVendors();

class OrganizationFeature extends BaseOrganizationFeature
{

    public static function model($className = __class__)
    {
        return parent::model($className);
    }


    public function byOrganizationId($organizationId)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->getTableAlias(false) . '.organization_id = :organization_id',
            'params' => [':organization_id' => $organizationId],
        ]);
        return $this;
    }

    public function byFeatureId($featureId)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->getTableAlias(false) . '.feature_id = :feature_id',
            'params' => [':feature_id' => $featureId],
        ]);
        return $this;
    }

    public function byPracticeId($practiceId)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->getTableAlias(false) . '.practice_id = :practice_id',
            'params' => [':practice_id' => $practiceId],
        ]);
        return $this;
    }

    public function byProviderId($providerId)
    {
        $this->getDbCriteria()->mergeWith([
            'condition' => $this->getTableAlias(false) . '.provider_id = :provider_id',
            'params' => [':provider_id' => $providerId],
        ]);
        return $this;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);

        if (
            !intval($this->organization_id)
            &&
            is_string($this->organization_id)
            &&
            strlen($this->organization_id) > 0
        ) {
            $criteria->with[] = 'organization';
            $criteria->addSearchCondition("organization.organization_name", $this->organization_id, true);
        } else {
            $criteria->compare('organization_id', $this->organization_id);
        }

        if (!intval($this->feature_id) && is_string($this->feature_id) && strlen($this->feature_id) > 0) {
            $criteria->with[] = 'feature';
            $criteria->addSearchCondition("feature.name", $this->feature_id, true);
        } else {
            $criteria->compare('feature_id', $this->feature_id);
        }

        if (!intval($this->provider_id) && is_string($this->provider_id) && strlen($this->provider_id) > 0) {
            $criteria->with[] = 'provider';
            $criteria->addSearchCondition("provider.friendly_url", $this->provider_id, true);
        } else {
            $criteria->compare('provider_id', $this->provider_id);
        }

        if (!intval($this->practice_id) && is_string($this->practice_id) && strlen($this->practice_id) > 0) {
            $criteria->with[] = 'practice';
            $criteria->addSearchCondition("practice.name", $this->practice_id, true);
        } else {
            $criteria->compare('practice_id', $this->practice_id);
        }

        $criteria->compare('active', $this->active);
        $criteria->compare('t.date_added', $this->date_added, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->order = 't.date_added DESC';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 20)
        ));
    }

    /**
     * Update SalesForce if necessary:
     *
     * Number_of_Locations__c
     * Number_of_Providers__c
     * Number_of_ReviewHubs__c
     * Number_of_SiteEnhance_Mobiles__c
     * Number_of_Showcase_Pages__c
     * AdWords__c
     * Number_of_ProfilePromotes__c
     * Number_of_ProfileSyncs__c
     * Promoted_Provider_Names__c
     * ProfileSync_Provider_Names__c
     * Number_of_ProfilePromotes_Healthgrades__c
     * Number_of_Enhanced_Healthgrades__c
     * Number_of_ProfilePromotes_Vitals__c
     * Promoted_Healthgrades_Provider_Names__c
     * Enhanced_Healthgrades_Provider_Names__c
     * Promoted_Vitals_Provider_Names__c
     * VirtualVisit_Enabled__c
     *
     * @return mixed
     */
    public function beforeSave()
    {

        $statusChanged = false;

        // this is for prevent feature activation in canceled organizations
        // if the feature is active, first check if the org is not canceled
        if ($this->organization_id && $this->feature_id && $this->active) {
            $organization = Organization::model()->findByPk($this->organization_id);

            // the org is canceled so the feature can't be active
            if ($organization->status == 'C') {
                $this->addError('organization_id', 'Could not add or activate a feature in canceled organizations.');
                return false;
            }
        }

        if (!$this->isNewRecord) {
            // on update, check if active changed
            if (!empty($this->oldRecord->active) && $this->oldRecord->active != $this->active) {
                $statusChanged = true;
            }
        }

        if ($this->organization_id > 0 && $this->feature_id > 0 && ($this->isNewRecord || $statusChanged)) {

            if (
                $this->feature_id == 2 || $this->feature_id == 4 || $this->feature_id == 6  || $this->feature_id == 8 ||
                $this->feature_id == 9 || $this->feature_id == 10 || $this->feature_id == 14 || $this->feature_id == 15
                || $this->feature_id == 16 || $this->feature_id == 20
            ) {
                SqsComponent::sendMessage('updateSFOrganizationFeatures', $this->organization_id);
            }
        }

        return parent::beforeSave();
    }

    /**
     * deactivate the OrganizationFeature and propagate the process as necessary
     */
    public function deactivate()
    {

        $this->active = 0;

        if ($this->save()) {
            $currentOrganization = Organization::model()->findByPK($this->organization_id);
            if (!empty($currentOrganization->salesforce_id)) {
                $sfID = $currentOrganization->salesforce_id;
            } else {
                $sfID = null;
            }

            if ($this->feature_id == 2) {
                // REVIEW HUB

                // do nothing? in salesforce rachels can check when a laptop is returned or not returned.
                return true;
            } elseif ($this->feature_id == 3) {
                // LOCAL LISTINGS (YEXT)
            } elseif ($this->feature_id == 4) {
                // FEATURED PROVIDER

                $featuredSites = PartnerSiteProviderFeatured::model()->findAll(
                    'provider_id=:providerId',
                    array(':providerId' => $this->provider_id)
                );
                if ($featuredSites) {
                    foreach ($featuredSites as $featuredSite) {
                        $featuredSite->delete();
                    }
                }

                $currentProvider = Provider::model()->findByPK($this->provider_id);
                $currentProvider->featured = 0;
                return $currentProvider->save();
            } elseif ($this->feature_id == 6) {
                // ADWORDS

                // do nothing - we don't need to do anything for this provider
                return true;
            } elseif ($this->feature_id == 8) {
                // MOBILE SITEENHANCE

                // do nothing - script should automatically stop working
                return true;
            } elseif ($this->feature_id == 9) {
                // SHOWCASEPAGE

                // do nothing - script should automatically stop working
                return true;
            } elseif ($this->feature_id == 10) {
                // PROFILESYNC (PROVIDER)

                // do nothing - we don't need to do anything for this provider
                return true;
            } elseif ($this->feature_id == 11) {
                // ADENHANCE (PROVIDER)

                // Set devops manager vendor
                $vendor = DevOps::get()->vendor('phabricator');

                $currentProvider = Provider::model()->findByPK($this->provider_id);

                $newOrgName = $currentOrganization->organization_name
                    . " (Provider: " . $currentProvider->first_last . ")";

                $fixedSteps = "\n- Cancel adwords\n
                    - Deauthorize card, etc.\n\n
                    Link: https://providers.doctor.com/organization/modify?organization=" . $currentOrganization->id;

                // Create Task
                $ticket = DevOpsTicket::build([
                    'title' => $newOrgName . " - DEACTIVATE ADENHANCE",
                    'body' => "**remember to mark as 'Resolved'**" . $fixedSteps,
                    'body_format' => 'markdown',
                    'salesforce_id' => $sfID,
                    'assignedTo' => 'lyubomirp',
                    'extra_data' => ["auxiliary" => ["type" => 'DEACTIVATE_ADENHANCE']]
                ]);
                return $vendor->createTicket($ticket);
            } elseif ($this->feature_id == 17) {
                // GMB - twilio number
                // do nothing - we don't need to do anything for this provider
                return true;
            } elseif ($this->feature_id  == Feature::REVIEW_REQUEST) {
                // reviewRequest
                AccountDevice::removeDigitalDevice($this);
            }

            // temporary bypass
            return true;
        } else {
            return false;
        }
    }

    /**
     * activate the OrganizationFeature and propagate the process as necessary
     */
    public function activate()
    {

        set_time_limit(0);

        $this->active = 1;

        if ($this->save()) {
            $currentOrganization = Organization::model()->findByPK($this->organization_id);

            $sfID = null;
            $masterTask = null;

            if (!empty($currentOrganization->salesforce_id)) {
                $sfID = $currentOrganization->salesforce_id;
            }

            if (!empty($currentOrganization->onboarding_task_id)) {
                $masterTask = $currentOrganization->onboarding_task_id;
            }

            if ($this->feature_id == 2) {
                // REVIEW HUB
                // Rachel has a workflow view in SF so creating this task is redundant
            } elseif ($this->feature_id == 3) {
                // LOCAL LISTINGS (YEXT)
            } elseif ($this->feature_id == 4) {
                // Featured on Base Network

                $currentProvider = Provider::model()->findByPK($this->provider_id);
                $currentProvider->featured = 1;

                // automatically feature the provider in all partner sites
                $partnerSites = PartnerSite::model()->findAll(
                    'custom_provider IS NOT NULL AND set_providers_featured = 1 AND active = 1 AND base_network = 1'
                );

                foreach ($partnerSites as $partnerSite) {

                    $customProvider = $partnerSite->custom_provider;
                    $customProvider = str_replace('$$provider_id$$', $this->provider_id, $customProvider);
                    if ($customProvider == '') {
                        continue;
                    }
                    $featureProvider = Yii::app()->db->createCommand($customProvider)->queryScalar();
                    if ($featureProvider <= 0) {
                        continue;
                    }

                    self::featureProvider($partnerSite, $currentProvider);
                }
                return $currentProvider->save();
            } elseif ($this->feature_id == 6) {

                // ADWORDS
                $currentPractice = Practice::model()->findByPK($this->practice_id);

                $newOrgName = $currentOrganization->organization_name . " (Practice: " . $currentPractice->name . ")";

                // WARNING
                // actual AdWords task won't be created until Onboarding Tool is used

                // Set devops manager vendor
                $vendor = DevOps::get()->vendor('phabricator');

                // Create Task
                $ticket = DevOpsTicket::build([
                    'title' => $newOrgName . " - ADWORDS ACCOUNT CREATION",
                    'body' => "**remember to mark as 'Resolved'**",
                    'body_format' => 'markdown',
                    'salesforce_id' => $sfID,
                    'assignedTo' => 'raquelg',
                    'extra_data' => ["auxiliary" => ["type" => 'ADWORDS_CREATION']]
                ]);
                $result = $vendor->createTicket($ticket);
                // The ticket was created ok?
                if ($result->success()) {
                    // Yes
                    $taskId = $result->data()->getId();

                    if ($masterTask) {
                        $textToAdd = " - " . $taskId  . " - **ADWORDS** - " . $currentPractice->name . "\n";
                        $vendor->addTaskToOnboardingTicket($masterTask, $textToAdd, 'markdown');
                    }

                    return $taskId;
                }
                return null;
            } elseif ($this->feature_id == 8) {

                // MOBILE SITEENHANCE
                $newOrgName = $currentOrganization->organization_name;

                // Set devops manager vendor
                $vendor = DevOps::get()->vendor('phabricator');

                // Create Task
                $ticket = DevOpsTicket::build([
                    'title' => $newOrgName . " - MOBILE SITEENHANCE - Graphics",
                    'body' => "**remember to mark as 'Resolved'**",
                    'body_format' => 'markdown',
                    'salesforce_id' => $sfID,
                    'assignedTo' => 'sebastianb',
                    'extra_data' => ["auxiliary" => ["type" => 'MSE_GRAPHICS']]
                ]);
                $result = $vendor->createTicket($ticket);
                $taskMseGraphics = '';
                if ($result->success()) {
                    // Saved ok
                    $taskMseGraphics = $result->data()->getId();
                }

                // default assignee
                $assignee = 'taskmaster';
                // are we in the prod environment?
                if (Yii::app()->params['environment'] == 'prod') {
                    // yes: set actual asignee
                    $assignee = 'kims';
                }

                // Create Task
                $ticket = DevOpsTicket::build([
                    'title' => $newOrgName . " - MOBILE SITEENHANCE - Send Code",
                    'body' => $taskMseGraphics . " - Graphics\n" . "**remember to mark as 'Resolved'**",
                    'body_format' => 'markdown',
                    'salesforce_id' => $sfID,
                    'assignedTo' => $assignee,
                    'extra_data' => ["auxiliary" => ["type" => 'MSE_SENDCODE']]
                ]);
                $result = $vendor->createTicket($ticket);

                if ($result->success()) {
                    // Saved ok
                    $taskId = $result->data()->getId();

                    if ($masterTask) {
                        $content = " - " . $taskId  . " - **MOBILE SITEENHANCE** \n";
                        $vendor->addTaskToOnboardingTicket($masterTask, $content, 'markdown');
                    }

                    return $taskId;
                }
                return null;
            } elseif ($this->feature_id == 9) {

                // SHOWCASEPAGE
                // No actions required.
                return true;
            } elseif ($this->feature_id == 10) {
                // PROFILESYNC (PROVIDER)

            } elseif ($this->feature_id == 14) {
                // Featured Only on Healthgrades (Provider)

                // Send to Healthgrades
                if ($currentOrganization->status == 'A') {
                    // only if the organization is active
                    SqsComponent::sendMessage('sendSingleProviderToHealthgrades', $this->provider_id);
                }

                $currentProvider = Provider::model()->findByPK($this->provider_id);

                // 46 = Healthgrades
                $partnerSite = PartnerSite::model()->findByPk(46);
                $customProvider = $partnerSite->custom_provider;
                $customProvider = str_replace('$$provider_id$$', $this->provider_id, $customProvider);
                if ($customProvider == '') {
                    return true;
                }
                $featureProvider = Yii::app()->db->createCommand($customProvider)->queryScalar();
                if ($featureProvider <= 0) {
                    return true;
                }

                self::featureProvider($partnerSite, $currentProvider);
            } elseif ($this->feature_id == 15) {
                // Enhanced on Healthgrades

                // Send to Healthgrades
                if ($currentOrganization->status == 'A') {
                    // only if the organization is active
                    return SqsComponent::sendMessage('sendSingleProviderToHealthgrades', $this->provider_id);
                }

                return true;
            } elseif ($this->feature_id == 16) {
                // Featured Only on Vitals

                $currentProvider = Provider::model()->findByPK($this->provider_id);

                // 111 = Vitals
                $partnerSite = PartnerSite::model()->findByPk(111);
                $customProvider = $partnerSite->custom_provider;
                $customProvider = str_replace('$$provider_id$$', $this->provider_id, $customProvider);
                if ($customProvider == '') {
                    return true;
                }
                $featureProvider = Yii::app()->db->createCommand($customProvider)->queryScalar();
                if ($featureProvider <= 0) {
                    return true;
                }

                self::featureProvider($partnerSite, $currentProvider);
            } elseif ($this->feature_id == 17) {
                // GMB - twilio number
                // do nothing - we don't need to do anything for this provider
                return true;
            } elseif ($this->feature_id == Feature::REVIEW_REQUEST) {
                // reviewRequest
                AccountDevice::assignDigitalDevice($this);
                return true;
            } elseif ($this->feature_id  == Feature::TELEMEDICINE) {
                // TeleMedicine was activated
                if (!empty($sfID)) {
                    $sfData['VirtualVisit_Enabled__c'] = TimeComponent::createSalesforceDatetime();
                    AuditLog::create(
                        'C',
                        "Activate Organization Feature",
                        'organization_feature',
                        "Update VirtualVisit_Enabled__c SalesForce #" . $sfID
                            . ' - ' . $sfData['VirtualVisit_Enabled__c'],
                        false
                    );
                }
            }

            // temporary bypass
            return true;
        }
        return false;
    }

    /**
     * Feature a provider in a partner site
     * @param PartnerSite $partnerSite
     * @param Provider $currentProvider
     */
    private function featureProvider($partnerSite, $currentProvider)
    {

        PartnerSiteProviderFeatured::featureProvider($partnerSite->id, $currentProvider->id);
    }

    /**
     * Completely delete a provider's or a practice's feature: deactivate it first if not provided by other organization
     * Then write to the organization's history, and delete altogether from organization_feature
     */
    public function deleteFeature()
    {

        // does other organization provide this feature?
        $oFeat = '';
        if ($this->provider_id > 0 && $this->practice_id > 0) {
            // provider@practice based feature
            $oFeat = OrganizationFeature::model()->exists(
                'provider_id = :provider_id AND practice_id = :practice_id
                    feature_id = :feature_id AND organization_id != :organization_id',
                array(
                    ':feature_id' => $this->feature_id,
                    ':provider_id' => $this->provider_id,
                    ':practice_id' => $this->practice_id,
                    ':organization_id' => $this->organization_id
                )
            );
        } elseif ($this->provider_id > 0) {
            // provider-based feature
            $oFeat = OrganizationFeature::model()->exists(
                'feature_id = :featureId AND provider_id = :providerId AND organization_id != :organizationId',
                array(
                    ':featureId' => $this->feature_id,
                    ':providerId' => $this->provider_id,
                    ':organizationId' => $this->organization_id
                )
            );
        } else {
            // practice-based feature
            $oFeat = OrganizationFeature::model()->exists(
                'feature_id = :featureId AND practice_id = :practiceId AND organization_id != :organizationId',
                array(
                    ':featureId' => $this->feature_id,
                    ':practiceId' => $this->practice_id,
                    ':organizationId' => $this->organization_id
                )
            );
        }

        if (!$oFeat) {
            // nobody else provides this feature, so we can deactivate it
            $this->deactivate();
        }

        // one way or another, record this deletion
        $history = new OrganizationHistory();
        $history->organization_id = $this->organization_id;
        $history->event = 'R';
        $history->feature_id = $this->feature_id;
        // magic account_id = 1:
        // default to tech@corp.doctor.com for command-line processes that don't have a user id
        $history->account_id = php_sapi_name() != 'cli' && isset(Yii::app()->user->id) ? Yii::app()->user->id : 1;
        $history->provider_id = $this->provider_id;
        $history->practice_id = $this->practice_id;
        $history->date_added = date('Y-m-d H:i:s');
        $history->save();

        // delete the original feature
        $this->delete();
    }

    /**
     * When an account churns, buy a return label for each of its devices and send it to the Client and CS
     * Also update SalesForce
     * @param int $deviceId
     * @return boolean
     */
    public function deactivateReviewHubs($deviceId = 0)
    {

        set_time_limit(0);

        // get account details
        $currentOrganization = Organization::model()->findByPK($this->organization_id);
        $account = $currentOrganization->getOwnerAccount();
        if (!$account) {
            // account not found, leave
            return false;
        }

        if (!empty($deviceId) && intval($deviceId) > 0) {
            // find a specific device
            $accountDevices = AccountDevice::model()->findAll(
                'account_id=:account_id AND device_id=:device_id AND status IN ("A", "B", "S", "P", "T")',
                array(':account_id' => $account->id, ':device_id' => $deviceId)
            );
        } else {
            // find devices that are Active or return requested (B) or Suspended or Pre-delivery or in Transit to client
            $accountDevices = AccountDevice::model()->findAll(
                'account_id=:account_id AND status IN ("A", "B", "S", "P", "T")',
                array(':account_id' => $account->id)
            );
        }

        if (!$accountDevices) {
            // no linked devices, leave
            return false;
        }

        // init library
        require_once(Yii::app()->basePath . "/vendors/EasyPost/lib/easypost.php");

        \EasyPost\EasyPost::setApiKey(Yii::app()->params['easypost_api_key']);

        $trackingCodes = array();
        $imageURLs = array();

        foreach ($accountDevices as $accountDevice) {

            // find device data
            $errorDetails = ' for account "' . $account->organization_name . '"';
            $device = Device::model()->findByPk($accountDevice->device_id);
            if ($device) {
                $errorDetails .= ' and device "' . $device->nickname . '"';
            }

            // find practice data
            if ($accountDevice['practice_id'] <= 0) {
                $trackingCodes[] = 'No tracking code: no practice linked' . $errorDetails;
                $imageURLs[] = null;
                continue;
            }

            $practice = Practice::getDetails($accountDevice->practice_id);
            $practice = $practice[0];
            if (!$practice) {
                $trackingCodes[] = 'No tracking code: linked practice not found' . $errorDetails;
                $imageURLs[] = null;
                continue;
            }
            if (trim($practice['address_2']) == '') {
                $practice['address_2'] = '-';
            }

            // from address
            $fromAddress = \EasyPost\Address::create(array("company" => $practice['name'], "street1" =>
            $practice['address'], "street2" => $practice['address_2'], "city" => $practice['city'], "state" =>
            $practice['state'], "zip" => $practice['zipcode'], "phone" => $practice['phone_number']));

            // to address
            $toAddress = \EasyPost\Address::create(array(
                "name" => 'ATTN: Doctor.com', "street1" =>
                '85 5th Ave', "street2" => '7th Floor', "city" => 'New York', "state" => 'NY',
                "zip" => '10003', "phone" => '(888) 666-8161'
            ));

            // parcel
            $parcel = \EasyPost\Parcel::create(array("length" => 11, "width" => 13, "height" => 4, "weight" => 48));

            // set options
            $options = array('print_custom_1' => 'RETURN - ' . (!empty($account->organization_name) ?
                $account->organization_name : $practice['name']));

            // get rates
            $shipment = \EasyPost\Shipment::create(array(
                "to_address" => $toAddress, "from_address" => $fromAddress,
                "parcel" => $parcel, "options" => $options
            ));

            if (sizeof($shipment->rates) > 0) {
                $i = 0;
                foreach ($shipment->rates as $rate) {
                    if ($rate['service'] == 'Ground') {
                        // if we find a ground service (ups) buy it
                        if ($shipment->buy($shipment->rates[$i])) {
                            $trackingCodes[] = $shipment->tracking_code;
                            $imageURLs[] = $shipment->postage_label->label_url;

                            // save record in audit log
                            $auditLog = new AuditLog();
                            $auditLog->account_id = $account->id;
                            $auditLog->action = 'C';
                            $auditLog->table = 'account_device';
                            $auditLog->section = 'AUTO: Deactivating feature';
                            $auditLog->date_added = date('Y-m-d H:i:s');
                            // default to tech@corp.doctor.com for command-line processes that don't have a user id
                            if (php_sapi_name() != 'cli') {
                                $auditLog->admin_account_id = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
                            } else {
                                $auditLog->admin_account_id = 1;
                            }
                            $auditLog->notes = json_encode([
                                $shipment->tracking_code,
                                $shipment->postage_label->label_url
                            ]);
                            $auditLog->hidden = 1;
                            $auditLog->save();

                            $previousStatus = $accountDevice->status;

                            $accountDevice->shipment_tracking_number = $shipment->tracking_code;
                            $accountDevice->status = 'B'; // account-device status is now "Return Requested"

                            if ($accountDevice->save()) {
                                // log status change to B
                                if ($previousStatus != 'B') {
                                    AccountDeviceHistory::log($accountDevice->id, $previousStatus, 'B', 'A');
                                }
                            } else {
                                Yii::log(
                                    "Automatic churn error: Could not save account-device #" . $accountDevice->id
                                        . ": Error was: " . serialize($accountDevice->getErrors()),
                                    'error',
                                    'application.account.churn'
                                );
                            }
                        } else {
                            $trackingCodes[] = 'No tracking code: could\'t be bought' . $errorDetails;
                            $imageURLs[] = null;
                        }
                    }
                    $i++;
                }
                if (empty($trackingCodes)) {
                    $trackingCodes[] = 'No tracking code: no tracking offered by any rates' . $errorDetails;
                    $imageURLs[] = null;
                }
            } else {
                $trackingCodes[] = 'No tracking code: no rates offered by EasyPost' . $errorDetails;
                $imageURLs[] = null;
            }
        }

        if (empty($trackingCodes)) {
            // nothing to do
            return false;
        }

        // update field in sf
        try {
            $sfUpdate = $account->updateSalesforceAccountField(
                'ReviewHub_UPS_Return_Tracking__c',
                implode(', ', $trackingCodes)
            );
        } catch (Exception $e) {
            $sfUpdate = false;
        }

        // send email to the client if we've got actual labels to send
        $validImageURLs = array();
        if (!empty($imageURLs)) {
            foreach ($imageURLs as $imageURL) {
                if (!empty($imageURL)) {
                    // send only the valid ones
                    $validImageURLs[] = $imageURL;
                }
            }
        }

        // did we end up having valid labels?
        if (!empty($validImageURLs)) {

            // fetch template
            $template = Common::fetchValidTemplate('REVIEWHUB_RETURN_LABEL');

            $mailFrom = array('From' => 'clientservices@corp.doctor.com', 'From_Name' => 'Doctor.com Client Services');
            $mailTo = $account->email;
            $mailCC = $account->email_cc;
            $mailSubject = $template->subject;
            $mailAltBody = "Hello,\r\n
                \r\n
                Attached to this email is a pre-paid return label for your Doctor.com ReviewHub. Please print it off
                and attach it to the box with the laptop and charger inside.\r\n
                \r\n
                If you have any questions please contact Doctor.com Client Services at clientservices@corp.doctor.com
                or (888) 666-8161.\r\n
                \r\n
                Best,
                \r\n
                The Doctor.com Team";
            $mailBody = nl2br($mailAltBody);

            // generate attachments
            $arrAttachments = array();
            $i = 0;
            foreach ($validImageURLs as $imageURL) {
                $arrAttachments[] = array(
                    'file_contents' => base64_encode(file_get_contents($imageURL)),
                    'file_name' => 'Label ' . ($i + 1) . '.jpg',
                    'encoding' => 'base64',
                    'type' => 'image/jpg'
                );
                $i++;
            }

            MailComponent::sendEmail(
                $mailFrom,
                $mailTo,
                $mailCC,
                'lizt@corp.doctor.com',
                $mailSubject,
                $mailBody,
                $mailAltBody,
                $arrAttachments,
                false,
                'Attached to this email is a pre-paid return label'
            );
        }

        // send email to cs
        $mailContent = '';
        $i = 0;
        foreach ($trackingCodes as $trackingCode) {
            $mailContent .= "\r\n";
            $mailContent .= 'Tracking code #' . ($i + 1) . ': ' . $trackingCode;
            $mailContent .= "\r\n";
            $mailContent .= 'Label #' . ($i + 1) . ': ' . $imageURLs[$i];
            $mailContent .= "\r\n";
            $mailContent .= "\r\n";
            $i++;
        }
        $mailContent .= "\r\n";
        $mailContent .= "\r\n";
        if ($sfUpdate) {
            $mailContent .= 'SalesForce has been updated with those tracking numbers to ' .
                implode(', ', $trackingCodes);
        } else {
            $mailContent .= 'SalesForce COULD NOT be updated with those tracking numbers.';
        }

        $mailFrom = array('From' => 'no-reply@corp.doctor.com', 'From_Name' => 'Doctor.com');
        $mailTo = 'marak@corp.doctor.com';
        $mailCC = 'lizt@corp.doctor.com';
        $mailSubject = 'Automated churn of account "' . $account->organization_name . '"';
        $mailAltBody = "Hello Client Services Team,\r\n\r\nWe've generated the following return labels:\r\n" .
            $mailContent;
        $mailBody = nl2br($mailAltBody);

        return MailComponent::sendEmail(
            $mailFrom,
            $mailTo,
            $mailCC,
            false,
            $mailSubject,
            $mailBody,
            $mailAltBody,
            false,
            false,
            'Tracking code #1'
        );
    }

    /**
     * Create or update providerFeature
     */
    public static function addProviderFeature($organizationId, $providerId, $featureId)
    {
        // find the feature in the db
        $orgFeature = OrganizationFeature::model()->find(
            'organization_id = :organization_id AND provider_id = :provider_id AND feature_id = :feature_id',
            [':organization_id' => $organizationId, ':provider_id' => $providerId, ':feature_id' => $featureId]
        );

        // update the feature or insert a new one
        if ($orgFeature) {
            if ($orgFeature->active == 0) {
                $orgFeature->active = 1;
            }
        } else {
            $orgFeature = new OrganizationFeature();
            $orgFeature->organization_id = $organizationId;
            $orgFeature->provider_id = $providerId;
            $orgFeature->feature_id = $featureId;
            $orgFeature->active = 1;
        }

        return $orgFeature->save();
    }

    /**
     * Check if the provider has profile sync
     * @param $organizationId
     * @param $providerId
     * @return mixed
     */
    public static function hasProfileSync($organizationId, $providerId)
    {
        return OrganizationFeature::model()->exists(
            'organization_id = :organization_id AND provider_id = :provider_id AND feature_id = 10',
            [
                ':organization_id' => $organizationId,
                ':provider_id' => $providerId
            ]
        );
    }

    /**
     * Check if the provider is featured
     * @param $organizationId
     * @param $providerId
     * @return mixed
     */
    public static function isFeatured($organizationId, $providerId)
    {
        return OrganizationFeature::model()->exists(
            'organization_id = :organization_id AND provider_id = :provider_id AND feature_id IN (14, 15)',
            [
                ':organization_id' => $organizationId,
                ':provider_id' => $providerId
            ]
        );
    }

    /**
     * Get the Third party URL for syndication
     * @param $organizationId
     * @param $providerId
     * @param $practiceId
     * @return URL
     */
    public static function getThirdPartySyndicationUrl($organizationId, $practiceId, $providerId = null)
    {
        $providerFeature = false;
        $practiceFeature = false;
        if ($organizationId != null && $practiceId != null) {
            $sql = sprintf(
                'SELECT active FROM organization_feature
                WHERE feature_id = 27 AND organization_id = %d AND practice_id = %d;',
                $organizationId,
                $practiceId
            );
            $practiceFeature = Yii::app()->db->createCommand($sql)->queryScalar();
            if ($providerId != null) {
                $sqlProvider = sprintf(
                    'SELECT active FROM organization_feature
                    WHERE feature_id = 26 AND organization_id = %d AND provider_id = %d;',
                    $organizationId,
                    $providerId
                );
                $providerFeature = Yii::app()->db->createCommand($sqlProvider)->queryScalar();
            }
        }
        // get the third party booking url for the same
        $sqlUrl = null;
        $url = null;
        if ($practiceFeature || $providerFeature) {
            // get listing url for 156
            if ($providerId != null && $providerFeature) {
                $sqlUrl = sprintf(
                    'SELECT url FROM provider_practice_listing
                    WHERE listing_type_id = 157 AND provider_id = %d AND practice_id IS NULL;',
                    $providerId
                );
                $url = Yii::app()->db->createCommand($sqlUrl)->queryScalar();
            }
            if ($url == null && $practiceFeature && $practiceId != null) {
                // fetch for the practice URL
                $sqlUrlPractice = sprintf(
                    'SELECT url FROM provider_practice_listing
                    WHERE listing_type_id = 158 AND practice_id = %d AND provider_id IS NULL;',
                    $practiceId
                );
                $url = Yii::app()->db->createCommand($sqlUrlPractice)->queryScalar();
            }
        }
        return $url;
    }


    /**
     * Check if the provider/practice has third party booking url feature enabled
     * @param $organizationId
     * @param $providerId
     * @return boolean
     */
    public static function hasSyndClientUrl($organizationId, $practiceId, $providerId = null)
    {
        if ($organizationId == null || $practiceId == null) {
            return false;
        }
        $providerFeature = false;
        $practiceFeature = false;
        $sql = sprintf(
            'SELECT active FROM organization_feature
                WHERE feature_id = 27 AND organization_id = %d AND practice_id = %d;',
            $organizationId,
            $practiceId
        );
        $practiceFeature = Yii::app()->db->createCommand($sql)->queryScalar();
        if ($providerId != null) {
            $sqlProvider = sprintf(
                'SELECT active FROM organization_feature
                WHERE feature_id = 26 AND organization_id = %d AND provider_id = %d;',
                $organizationId,
                $providerId
            );
            $providerFeature = Yii::app()->db->createCommand($sqlProvider)->queryScalar();
        }
        return ($practiceFeature || $providerFeature);
    }
}
