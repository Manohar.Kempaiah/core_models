<?php

Yii::import('application.modules.core_models.models._base.BaseLocale');

class Locale extends BaseLocale
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
