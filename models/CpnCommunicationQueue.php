<?php

Yii::import('application.modules.core_models.models._base.BaseCpnCommunicationQueue');

class CpnCommunicationQueue extends BaseCpnCommunicationQueue
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get Notifications
     *
     * @param int $installationId
     * @return array
     */
    public function getNotifications($installationId)
    {
        // Convert current date to NY Time Zone
        $date = new DateTime();
        $date->setTimezone(new DateTimeZone('America/New_York'));
        $currentDate = $date->format('Y-m-d H:i:s');

        // Query to cpn_communication_queue
        $sql = sprintf(
            "SELECT      cq.message,
                        cq.guid AS messageIdentifier,
                        cq.cpn_message_type_id AS messageType,
                        cq.seconds,
                        cq.url,
                        cq.title
            FROM        cpn_communication_queue cq
            INNER JOIN  cpn_installation i on cq.cpn_installation_id = i.id
            WHERE       i.id = '%s'
                    AND i.enabled_flag = 1
                    AND i.del_flag = 0
                    AND cq.del_flag = 0
                    AND cq.processed_flag = 0
                    AND cq.date_expires > '%s'
                    AND cq.date_to_publish <= '%s'
            ORDER BY    cq.id;",
            $installationId,
            $currentDate,
            $currentDate
        );

        // Return all the results
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get Notifications Version 2
     *
     * This does not limit it based on the message expiration. It let's Companion figure that out.
     *
     * @param int $installationId
     * @return array
     */
    public function getNotificationsV2($installationId)
    {
        // Query to cpn_communication_queue without looking at the
        $sql = sprintf(
            "SELECT      cq.message,
                        cq.guid AS messageIdentifier,
                        cq.cpn_message_type_id AS messageType,
                        cq.message_json,
                        cq.seconds,
                        cq.url,
                        cq.title,
                        cq.date_expires
            FROM        cpn_communication_queue cq
            INNER JOIN  cpn_installation i on cq.cpn_installation_id = i.id
            WHERE       i.id = '%s'
                    AND i.enabled_flag = 1
                    AND i.del_flag = 0
                    AND cq.del_flag = 0
                    AND cq.processed_flag = 0
                    AND cq.date_to_publish <= UTC_TIMESTAMP()
            ORDER BY    cq.id;",
            $installationId
        );

        // Return all the results
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * GetPatientByPhoneNumber
     *
     * This gets the patient information from the database when we know about the phone number
     *
     * @param int $installationId - the installation id
     * @return int
     */
    public function getActiveReviewHubCount($installationId)
    {
        // Query the provider rating count
        $sql = sprintf(
            "SELECT      COUNT(ad.id) as ReviewHubCount
            FROM        account_device ad
            INNER JOIN  cpn_installation i on ad.practice_id = i.id
            WHERE       ad.status = 'A'
            AND         ad.date_delivered is not null
            AND         i.id = '%s';",
            $installationId
        );

        // grab the count
        $activeCounts = Yii::app()->db->createCommand($sql)->queryAll();

        //iterate through our counts
        foreach ($activeCounts as $activeCount) {
            //return the count
            return $activeCount['ReviewHubCount'];
        }

        //if we got here return a null
        return null;
    }

    /**
     * Get Review Count
     *
     * This checks to see how many reviews they got in the last 30 days
     *
     * @param int $installationId - the installation id
     * @param int $days - the number of days to go back
     * @return int
     */
    public function getReviewCount($installationId, $days)
    {
        // Query the provider rating count
        $sql = sprintf(
            "SELECT      COUNT(pr.id) as RatingCount
            FROM        provider_rating pr
            INNER JOIN  cpn_installation i on pr.practice_id = i.id
            WHERE       pr.date_added > DATE_SUB(NOW(), INTERVAL %s day)
            AND         i.id = '%s';",
            $days,
            $installationId
        );

        // grab the count
        $reviewCounts = Yii::app()->db->createCommand($sql)->queryAll();

        //iterate through our counts
        foreach ($reviewCounts as $reviewCount) {
            //return the count
            return $reviewCount['RatingCount'];
        }

        //if we got here return a null
        return null;
    }

    /**
     * NotificationACK
     *
     * @param int $installationId
     * @param str $messageIdentifier
     * @return bool
     */
    public function notificationACK($installationId, $messageIdentifier)
    {
        // Query to cpn_communication_queue
        $cpnCQ = CpnCommunicationQueue::model()->find(
            'guid = :guid AND processed_flag = :processed_flag AND cpn_installation_id = :cpn_installation_id',
            array(
                ':guid' => $messageIdentifier,
                ':processed_flag' => 0,
                ':cpn_installation_id' => $installationId
            )
        );
        //do we have the data?
        //yes
        if ($cpnCQ) {
            //set the processed flag to true
            $cpnCQ->processed_flag = 1;
            $cpnCQ->date_updated = date("Y-m-d H:i:s");
            //save the update
            $cpnCQ->save();

            //return success
            return true;
        }

        //return failure if we got here (means no data was found)
        return false;
    }

}
