<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class LoginForm extends CFormModel
{

    public $username;
    public $password;
    public $isPatient = false;
    // $rememberMe is kept for compatbility, but is unused
    public $rememberMe;
    private $_identity;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules()
    {
        return array(
            // username and password are required
            array('username, password', 'required'),
            // rememberMe needs to be a boolean
            array('rememberMe', 'boolean'),
            // password needs to be authenticated
            array('password', 'authenticate'),
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'rememberMe' => 'Remember me next time',
        );
    }

    /**
     * Authenticates the password.
     * This is the 'authenticate' validator as declared in rules().
     */
    public function authenticate($attribute, $params)
    {

        if (!$this->hasErrors()) {

            // create new identity if none exists
            if (!$this->_identity) {
                $this->_identity = new UserIdentity($this->username, $this->password, $this->isPatient);
            }

            $lang = isset(Yii::app()->session['lang']) ? Yii::app()->session['lang'] : 'en';

            if (!$this->_identity->authenticate()) {

                switch ($this->_identity->errorCode) {
                    case UserIdentity::ERROR_UNKNOWN_IDENTITY:
                    case UserIdentity::ERROR_USERNAME_INVALID:
                    case UserIdentity::ERROR_PASSWORD_INVALID:
                        $this->addError(
                            'password',
                            Yii::t(
                                'patients',
                                'svrErrIncorrectUsrOrPassw',
                                '',
                                'messages',
                                $lang
                            )
                        );
                        break;
                    case UserIdentity::ERROR_MULTIPLE_ACCOUNTS:
                        $this->addError(
                            'password',
                            'Multiple accounts are associated with this email address. '
                            . 'Please contact clientservices@corp.doctor.com to resolve this issue.'
                        );
                        break;
                    case UserIdentity::ERROR_ACCOUNT_DISABLED:
                        $this->addError(
                            'password',
                            'Your account has been disabled. '
                            . 'If you would like to re-enable it, please contact clientservices@corp.doctor.com.'
                        );
                        break;
                    case UserIdentity::ERROR_PASSWORD_EXPIRED:
                        $this->addError(
                            'password',
                            'Your password has expired, and needs to be changed.'
                                . ' Please use the "Forgot Your Password?" option.'
                        );
                        break;
                    case UserIdentity::ERROR_NONE:
                    default:
                        $this->addError('password', 'There was an unknown error. Please try again later');
                        break;
                }

                // audit the login error, show in audit log
                try {
                    $auditMessage = 'Error ' . $this->_identity->errorCode . ': Username: ' .
                            $this->_identity->username;
                    AuditLog::create('F', 'Login: Error', '', $auditMessage, false, 1, true);
                } catch (Exception $e) {
                    Yii::log("Failed to log: " . serialize($e));
                }
            }
        }
    }

    /**
     * Logs in the user using the given username and password in the model.
     * @return boolean whether login is successful
     */
    public function login()
    {

        // try logging in, setting any errors if this fails
        $this->authenticate(null, null);

        if ($this->_identity->errorCode === UserIdentity::ERROR_NONE) {
            $duration = Yii::app()->session->getTimeout();
            Yii::app()->user->login($this->_identity, $duration);

            if ($this->isPatient) {

                // audit the successful login
                try {
                    $auditMessage = 'Logged in: ' . $this->username;
                    AuditLog::create('S', 'Patient login: Logged in', '', $auditMessage, false, false);
                } catch (Exception $e) {
                    Yii::log("Failed to log: " . serialize($e));
                }

                // save the event in the patient's log
                $log = new PatientLog();
                $log->attributes = array(
                    "email" => $this->username,
                    "event_name" => "login",
                    "date" => date("Y-m-d H:i:s")
                );
                $log->save();
            }

            return true;
        } else {

            // audit the failed login
            try {
                $auditMessage = 'Error ' . $this->_identity->errorCode . ': Username: ' . $this->username;
                AuditLog::create('F', 'Login: Error', '', $auditMessage, false, 1, true);
            } catch (Exception $e) {
                Yii::log("Failed to log: " . serialize($e));
            }

            return false;
        }
    }

}
