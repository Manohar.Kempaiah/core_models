<?php

Yii::import('application.modules.core_models.models._base.BaseShippingLog');

class ShippingLog extends BaseShippingLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public static function representingColumn()
    {
        return 'tracking_number';
    }

}
