<?php

Yii::import('application.modules.core_models.models._base.BasePracticeVideo');

class PracticeVideo extends BasePracticeVideo
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    //called on rendering the column for each row
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id
            . '" rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice
            . '">' . $this->practice . '</a>');
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_practice
            INNER JOIN practice_video
            ON account_practice.practice_id = practice_video.practice_id
            WHERE practice_video.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * Get all video for this practice
     * @param int $practiceId
     * @return array
     */
    public static function getPracticeVideos($practiceId = 0)
    {
        if ($practiceId == 0) {
            return null;
        }

        $sql = sprintf(
            "SELECT id, practice_id, embed_code
            FROM practice_video
            WHERE practice_id = %d; ",
            $practiceId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $practiceId
     * @param int $accountId
     * @return array
     */
    public static function saveData($postData = '', $practiceId = 0, $accountId = 0)
    {

        $results['success'] = true;
        $results['error'] = "";

        if (empty($postData)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        if ($practiceId == 0 || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "PRACTICE_ID_ZERO";
            return $results;
        }

        // validate access
        ApiComponent::checkPermissions('Practice', $accountId, $practiceId);

        foreach ($postData as $data) {
            $model = null;
            $backendAction = !empty($data['backend_action']) ? trim($data['backend_action']) : '';
            $videoUrl = !empty(trim($data['embed_code'])) ? trim($data['embed_code']) : '';
            $data['id'] = isset($data['id']) ? $data['id'] : 0;

            if (!StringComponent::validateUrl($videoUrl)) {
                $results['success'] = false;
                $results['error'] = "ERROR_INVALID_URL";
                $results['data'] = $data;
                return $results;
            }

            if ($data['id'] > 0) {
                $model = PracticeVideo::model()->findByPk($data['id']);
            }

            if ($backendAction == 'delete' || empty($videoUrl)) {
                // delete the row
                if (!empty($model) && !$model->delete()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_DELETING_PRACTICE_VIDEO";
                    $results['data'] = $model->getErrors();
                    return $results;
                }

            } else {

                $auditAction = 'U';
                if (empty($model)) {
                    $model = new PracticeVideo;
                    $auditAction = 'C';
                }

                $model->embed_code = trim($data['embed_code']);
                $model->practice_id = $practiceId;

                if ($model->save()) {
                    $auditSection = 'Practice Video practice_id #' . $practiceId;
                    AuditLog::create($auditAction, $auditSection, 'practice_video', null, false);
                    $results['success'] = true;
                    $results['error'] = "";
                    if ($auditAction == 'C') {
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                } else {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PRACTICE_VIDEO";
                    $results['data'] = $model->getErrors();
                    return $results;
                }
            }
        }
        return $results;
    }

}
