<?php

class ReviewHubDashboard extends GxActiveRecord
{

    /**
     * Return the reviewHubs list for the logged account
     * @param int $accountId
     * @return array $result
     */
    public static function getList($accountId = 0)
    {

        $accountInfo = Account::getInfo($accountId, true, true);

        if ($accountInfo['connection_type'] == 'S') {
            // The staffer has no access to the RH list
            $arrAccountDevices = [];
        } else {
            $ownerAccountId = $accountInfo['owner_account_id'];
            $arrAccountDevices = AccountDevice::model()->getDevices($ownerAccountId);

            foreach ($arrAccountDevices as $key => $value) {
                $practicePhotos = PracticePhoto::model()->getPracticePhotos($value['practice_id']);
                $photoPath = '';
                if (!empty($practicePhotos)) {
                    $photoPath = $practicePhotos[0]['photo_path'];
                }
                $profileImage = AssetsComponent::generateImgSrc($photoPath, 'practices', 'M', true, true);
                $arrAccountDevices[$key]['practice_photo'] = $profileImage;
                $arrAccountDevices[$key]['concat_providers'] =
                    AccountDeviceProvider::getAccountDeviceProviders(
                        $value['account_device_id'],
                        $value['practice_id']
                    );
                $arrAccountDevices[$key]['device_model_id'] = null;
                $arrAccountDevices[$key]['kiosk_link'] = null;
                $arrAccountDevices[$key]['static_link'] = null;


                // find the device
                $device = Device::model()->cache(Yii::app()->params['cache_medium'])->find(
                    'id=:id',
                    array(':id' => $value['device_id'])
                );

                if ($device) {
                    $arrAccountDevices[$key]['device_model_id'] = $device->device_model_id;
                    $arrAccountDevices[$key]['kiosk_link'] = $device->getReviewHubUrl();
                    $keyData = array(
                        'uid' => StringComponent::generateGUID(),
                        'staticLink' => true
                    );
                    $arrAccountDevices[$key]['static_link'] = $device->getReviewHubUrl($keyData);
                }

                $practice = Practice::getDetails($value['practice_id']);
                if (!empty($practice[0])) {
                    if (trim($practice[0]['address_2']) == '') {
                        $practice[0]['address_2'] = '-';
                    }
                    $practice[0]['zipcode'] = " " . $practice[0]['zipcode'] . " "; // force to string
                    $arrAccountDevices[$key]['practice'] = $practice[0];
                }
            }
        }

        // Doctor.com info for returned label
        $doctorComInfo = array(
            'name' => 'ATTN: Luis Perdomo - LIFE STORAGE',
            'phone' => '(646) 553-6380',
            'address' => '280 Fairfield Avenue',
            'address2' => 'Unit 1118',
            'zip' => " 06902 ",
            'city' => 'Stamford',
            'state' => 'CT',
            'country_id' => 1,
        );

        //$accountInfo['practices'] and $accountInfo['providers'] are expected to be an array of ids
        $accountInfo['providers'] = array_column($accountInfo['providers'], 'id');
        // find providers that belong to this account
        $accountProvider = $accountInfo['providers'];

        $arrProviders = array();
        foreach ($accountProvider as $key => $ap) {
            $arrProviders[$key] = Provider::model()->findByPk($ap);
        }

        $saveResult = Yii::app()->request->getParam('ok', '');

        return array(
            'arrAccountDevices' => $arrAccountDevices,
            'arrProviders' => $arrProviders,
            'doctorcom_info' => $doctorComInfo,
            'saveResult' => $saveResult
        );
    }

    /**
     * ReviewHub Services - process changes to a given reviewhub (replacement, upgrade model, etc)
     * @param array $data
     * Eg:
     * $data['accountDeviceId'] = Yii::app()->request->getParam('id', 0);
     * $data['action'] = Yii::app()->request->getParam('action', 0);
     * $data['accessoryStr'] = Yii::app()->request->getParam('accessoryStr', '');
     * $data['providerStr'] = Yii::app()->request->getParam('providerStr', '');
     * $data['statusStr'] = Yii::app()->request->getParam('statusStr', '');
     * $data['sendEmailReview'] = Yii::app()->request->getPost('send_email_review_to_account');
     * @return array
     * @throws Exception
     */
    public static function saveServices($data = array())
    {
        $accountDeviceId = !empty($data['accountDeviceId']) ? $data['accountDeviceId'] : 0;
        $action = !empty($data['action']) ? $data['action'] : 0;
        $providerStr = !empty($data['providerStr']) ? $data['providerStr'] : '';
        $accessoryStr = !empty($data['accessoryStr']) ? $data['accessoryStr'] : '';
        $reasonStr = !empty($data['reasonStr']) ? $data['reasonStr'] : '';
        $notesStr = !empty($data['notesStr']) ? $data['notesStr'] : '';
        $statusStr = !empty($data['statusStr']) ? $data['statusStr'] : '';
        $followUpEmail = !empty($data['followUpEmail']) ? $data['followUpEmail'] : 0;
        $sendEmailReview = !empty($data['sendEmailReview']) ? $data['sendEmailReview'] : 0;
        $accountId = !empty($data['accountId']) ? $data['accountId'] : 0;

        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);
        ApiComponent::checkPermissions('AccountDevice', $ownerAccountId, $accountDeviceId);

        // Get account device and practice
        $accountDevice = AccountDevice::model()->findByPk($accountDeviceId);
        $practiceId = $accountDevice->practice_id;

        // Get master task for link in phabricator
        $organizationAccount = OrganizationAccount::model()->find(
            "account_id=:accountId AND connection='O'",
            array(":accountId" => $accountId)
        );

        if (!empty($organizationAccount)) {
            $organizationID = $organizationAccount->organization_id;
            $organization = Organization::model()->findByPK($organizationID);
        }

        $sfId = '';
        if (!empty($organization)) {
            $sfId = $organization->salesforce_id;
        }
        $result = array();

        if ($action == 1 || $action == 2 || $action == 3) {
            // (1)Send Accessory || (2)Replace broken device || (3)Upgrade model

            if ($action == 1) {
                // Send Accesory
                $dueDate = date('Y-m-d', strtotime('+5 days'));
                $subject = "Send Accessory (" . $accessoryStr . ") - " . $accountDevice->device_id;
                $description = "Send Accessory (" . $accessoryStr . ") - " . $accountDevice->device_id;
            } elseif ($action == 2) {
                // Replace broken device
                $subject = 'Replace broken device - ' . $accountDevice->device_id;
                $dueDate = date('Y-m-d', strtotime('+2 days'));
                $description = 'Replace broken device - ' . $accountDevice->device_id;
                $description .= ' Reason: ' . $reasonStr;
                $description .= ' Notes: ' . $notesStr;
            } else {
                // Upgrade model
                $dueDate = date('Y-m-d', strtotime('+5 days'));
                $subject = 'Upgrade Model - ' . $accountDevice->device_id;
                $description = 'Upgrade Model';
                $description .= ' Reason: ' . $reasonStr;
                $description .= ' Notes: ' . $notesStr;
            }

            // Create SF Task
            $data = array(
                'WhatId' => $sfId,
                'OwnerId' => '0051J000005qLFR', // Rachel Schenk
                'Status' => 'Not Started',
                'Priority' => 'Normal',
                'ActivityDate' => $dueDate,
                'Subject' => $subject,
                'Description' => $description
            );
            Yii::app()->salesforce->createTask($data);
        } elseif ($action == 4) {
            // Return label is generated outside of this action

        } elseif ($action == 5) {

            // Add/remove provider
            $accountId = $accountDevice->account_id;
            $accountInfo = Account::getInfo($accountId, true, false);
            $ownerAccountId = $accountInfo['owner_account_id'];

            $arrProviders = explode(",", $providerStr);
            $concat_providers = '';
            foreach ($arrProviders as $provider) {

                $arrProvider = explode("=", $provider);
                $providerId = !empty($arrProvider[0]) ? $arrProvider[0] : 0;
                $newStatus = (!empty($arrProvider[1]) && Common::isTrue($arrProvider[1])) ? true : false;
                $providerExists = AccountDeviceProvider::model()->exists(
                    'account_device_id=:accountDeviceId AND provider_id=:providerId',
                    array(':accountDeviceId' => $accountDeviceId, ':providerId' => $providerId)
                );

                if ($newStatus) {
                    $concat_providers = $concat_providers . $providerId . ',';
                }

                if (!$providerExists && $newStatus) {
                    // New Account Device Provider
                    $adp = new AccountDeviceProvider;
                    $adp->account_device_id = $accountDeviceId;
                    $adp->provider_id = $providerId;
                    $adp->save();
                } elseif ($providerExists && !$newStatus) {

                    // Delete Account Device Provider
                    $adp = AccountDeviceProvider::model()->find(
                        'account_device_id=:accountDeviceId AND provider_id=:providerId',
                        array(':accountDeviceId' => $accountDeviceId, ':providerId' => $providerId)
                    );
                    $adp->delete();
                }
            }

            // providers related
            $providersPractice = ProviderPractice::getProvidersFullData($practiceId, $ownerAccountId, true);

            $providersInRH = AccountDeviceProvider::model()->findAll(
                'account_device_id=:accountDeviceId',
                array(':accountDeviceId' => $accountDevice->id)
            );

            $providerNames = '';
            if ($providersInRH) {
                foreach ($providersPractice as $key => $pp) {
                    if (!isset($providersPractice[$key]['selected'])) {
                        $providersPractice[$key]['selected'] = false;
                    }

                    foreach ($providersInRH as $provider) {
                        if ($pp['id'] == $provider->provider_id) {
                            $providerNames .= $pp['first_m_last'] . ', ';
                            $providersPractice[$key]['selected'] = true;
                            break;
                        }
                    }
                }
                $providerNames = trim($providerNames);
                $providerNames = '(' . trim($providerNames, ',') . ')';
            }
            $result['providerNames'] = $providerNames;
            $result['providers_at_practice'] = $providersPractice;
        } elseif ($action == 6) {
            // Update RH Status
            $accountDevice->status = $statusStr;
            if ($accountDevice->save()) {
                $result['update_status'] = 'ok';
            } else {
                $result['update_status'] = 'error';
            }
        } elseif ($action == 7) {
            // Change FollowUp Email
            $result['followup_email'] = AccountDevice::updateFollowUpEmail(
                $accountDeviceId,
                $followUpEmail,
                $accountId
            );
        } elseif ($action == 8) {
            // Enable/Disable notifications
            $accountDevice->send_email_review_to_account = $sendEmailReview;
            if ($accountDevice->save()) {
                $result['update_status'] = 'ok';
            } else {
                $result['update_status'] = 'error';
            }
        }

        return $result;
    }

    /**
     * ReviewHub Save update
     * @param int $accountId
     * @param int $accountDeviceId
     * @param array $data
     * @return array
     */
    public static function saveUpdate($accountId = 0, $accountDeviceId = 0, $data = array())
    {
        $saveResult = "n";
        if ($accountDeviceId == 0) {
            return $saveResult;
        }
        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);

        ApiComponent::checkPermissions('AccountDevice', $ownerAccountId, $accountDeviceId);

        $pAD = !empty($data['post_account_device']) ? $data['post_account_device'] : '';
        $lang = !empty($data['lang']) ? $data['lang'] : '';
        $arrRhUrls = !empty($data['arr_rh_urls']) ? $data['arr_rh_urls'] : false;

        $accountDevice = AccountDevice::model()->find(
            "id=:id AND account_id=:account_id",
            array(":id" => $accountDeviceId, ":account_id" => $ownerAccountId)
        );
        $practice = Practice::model()->cache(Yii::app()->params['cache_medium'])->findByPk($accountDevice->practice_id);

        $arrLanguages = Common::getAvailableLanguages();

        $showProviderSpecialties = !empty($pAD['show_provider_specialties']) ? $pAD['show_provider_specialties'] : 0;

        // save audit
        $auditSection = 'ReviewHub #' . $accountDeviceId;
        AuditLog::create('U', $auditSection, 'account_device', null, false);

        // update denied languages
        $deniedLang = '';
        $defaultLang = '';
        foreach ($arrLanguages as $langCode => $value) {
            if (empty($lang[$langCode]['active'])) {
                $deniedLang .= $langCode . ',';
            }
            if (!empty($lang[$langCode]['default'])) {
                $defaultLang = $langCode;
            }
        }
        $deniedLang = trim($deniedLang, ',');
        $accountDevice->denied_languages = $deniedLang;

        $localizationId = 0;
        if (!empty($defaultLang)) {
            // Looking for localization_id
            $localizationId = Localization::getLocalizationId($defaultLang);
        }
        if (empty($localizationId)) {
            $localizationId = !empty($pAD['localization_id']) ? $pAD['localization_id'] : 0;
        }
        $accountDevice->localization_id = $localizationId;

        $accountDevice->show_provider_specialties = $showProviderSpecialties;

        if (!empty($lang[$defaultLang]['initial_cta'])) {
            $accountDevice->initial_cta = $lang[$defaultLang]['initial_cta'];
        }

        $accountDevice->save();

        // Update translations
        $accountDeviceTranslations = AccountDeviceTranslations::model()->findAll(
            "account_device_id=:account_device_id",
            array(":account_device_id" => $accountDeviceId)
        );
        foreach ($accountDeviceTranslations as $adt) {
            $adt->delete();
        }
        foreach ($lang as $key => $value) {
            $adt = new AccountDeviceTranslations();
            $adt->account_device_id = $accountDeviceId;
            $adt->lang_id = $key;
            $adt->initial_cta = $value['initial_cta'];
            $adt->save();
        }

        // process practice_details section
        $practiceId = $accountDevice->practice_id;
        $practiceDetails = PracticeDetails::model()->find(
            'practice_id=:practiceId',
            array(':practiceId' => $practiceId)
        );
        if (!$practiceDetails) {
            $practiceDetails = new PracticeDetails;
            $practiceDetails->practice_id = $practiceId;
        }

        // If sharing options are sent (PADM v1 support)
        if (
            isset($data['rh_sharing_dynamic']) ||
            isset($data['rh_sharing_google']) ||
            isset($data['rh_sharing_yelp'])
        ) {
            if (!empty($data['rh_sharing_dynamic'])) {
                $practiceDetails->reviewhub_sharing = 'DYNAMIC';
            } elseif (!empty($data['rh_sharing_google']) && !empty($data['rh_sharing_yelp'])) {
                $practiceDetails->reviewhub_sharing = 'BOTH';
            } elseif (!empty($data['rh_sharing_google'])) {
                $practiceDetails->reviewhub_sharing = 'GOOGLE+';
            } elseif (!empty($data['rh_sharing_yelp'])) {
                $practiceDetails->reviewhub_sharing = 'YELP!';
            } else {
                $practiceDetails->reviewhub_sharing = 'NONE';
            }
            $practiceDetails->save();
        }

        // save audit
        $auditSection = 'ReviewHub Sharing for practice #' . $practiceId . ' (' . $practice->name . ')';
        AuditLog::create('U', $auditSection, 'practice', null, false);

        // Check for arr_rh_urls if is not set it means it comes from PADM v2 (Links should not be updated or deleted)
        if (isset($data['arr_rh_urls'])) {

            // process reviewhub links section
            $googleUrl = !empty($arrRhUrls['google']) ? $arrRhUrls['google'] : '';
            $yelpUrl = !empty($arrRhUrls['yelp']) ? $arrRhUrls['yelp'] : '';
            $yelpWriteareviewUrl = !empty($arrRhUrls['yelp_writeareview']) ? $arrRhUrls['yelp_writeareview'] : '';

            // google
            $providerPracticeListing = ProviderPracticeListing::model()->find(
                'listing_type_id=:googleProfileId  AND provider_id IS NULL AND practice_id=:practiceId',
                array(':googleProfileId' => ListingType::GOOGLE_PROFILE_ID, ':practiceId' => $practiceId)
            );
            if (!empty($googleUrl)) {
                // update or create google link
                if (!$providerPracticeListing) {
                    $providerPracticeListing = new ProviderPracticeListing;
                    $providerPracticeListing->source = 'PADM / RH';
                    $providerPracticeListing->verified = 1;
                    $providerPracticeListing->practice_id = $practiceId;
                    $providerPracticeListing->listing_type_id = ListingType::GOOGLE_PROFILE_ID;
                    // save audit
                    $auditSection = 'Google link for practice #' . $practiceId . ' (' . $practice->name . ')';
                    AuditLog::create('C', $auditSection, 'provider_practice_listing', null, false);
                } else {
                    // save audit
                    $auditSection = 'Google link for practice #' . $practiceId . ' (' . $practice->name . ')';
                    AuditLog::create('U', $auditSection, 'provider_practice_listing', null, false);
                }
                $providerPracticeListing->url = $googleUrl;
                if ($providerPracticeListing->validate()) {
                    $providerPracticeListing->save();
                }
            } elseif ($providerPracticeListing) {
                // delete google link
                $providerPracticeListing->delete();
                // save audit
                $auditSection = 'Google link for practice #' . $practiceId . ' (' . $practice->name . ')';
                AuditLog::create('D', $auditSection, 'provider_practice_listing', null, false);
            }

            // yelp
            $providerPracticeListing = ProviderPracticeListing::model()->find(
                'listing_type_id=:yelpProfileId AND provider_id IS NULL AND practice_id=:practiceId',
                array(
                    ':yelpProfileId' => ListingType::YELP_PROFILE_ID,
                    ':practiceId' => $practiceId
                )
            );

            if (!empty($yelpUrl)) {
                // update or create yelp link
                if (!$providerPracticeListing) {
                    $providerPracticeListing = new ProviderPracticeListing;
                    $providerPracticeListing->source = 'PADM / RH';
                    $providerPracticeListing->verified = 1;
                    $providerPracticeListing->practice_id = $practiceId;
                    $providerPracticeListing->listing_type_id = ListingType::YELP_PROFILE_ID;
                    // save audit
                    $auditSection = 'Yelp link for practice #' . $practiceId . ' (' . $practice->name . ')';
                    AuditLog::create('C', $auditSection, 'provider_practice_listing', null, false);
                } else {
                    // save audit
                    $auditSection = 'Yelp link for practice #' . $practiceId . ' (' . $practice->name . ')';
                    AuditLog::create('U', $auditSection, 'provider_practice_listing', null, false);
                }
                $providerPracticeListing->url = $yelpUrl;
                if ($providerPracticeListing->validate()) {
                    $providerPracticeListing->save();
                }
            } elseif ($providerPracticeListing) {
                // delete yelp link
                $providerPracticeListing->delete();
                // save audit
                $auditSection = 'Yelp link for practice #' . $practiceId . ' (' . $practice->name . ')';
                AuditLog::create('D', $auditSection, 'provider_practice_listing', null, false);
            }

            // yelp - write a review
            $providerPracticeListing = ProviderPracticeListing::model()->find(
                'listing_type_id=:yelpWriteareviewId AND provider_id IS NULL AND practice_id=:practiceId',
                array(
                    ':practiceId' => $practiceId,
                    ':yelpWriteareviewId' =>
                    ListingType::YELP_WRITEREVIEW_ID
                )
            );
            if ($yelpWriteareviewUrl != '') {
                // update or create yelp's write a review link
                if (!$providerPracticeListing) {
                    $providerPracticeListing = new ProviderPracticeListing;
                    $providerPracticeListing->source = 'PADM / RH';
                    $providerPracticeListing->verified = 1;
                    $providerPracticeListing->practice_id = $practiceId;
                    $providerPracticeListing->listing_type_id = ListingType::YELP_WRITEREVIEW_ID;
                    // save audit
                    $auditSection = 'Yelp Write a review link for practice #' . $practiceId . ' (' . $practice->name .
                        ')';
                    AuditLog::create('C', $auditSection, 'provider_practice_listing', null, false);
                } else {
                    // save audit
                    $auditSection = 'Yelp Write a review link for practice #' . $practiceId . ' (' . $practice->name .
                        ')';
                    AuditLog::create('U', $auditSection, 'provider_practice_listing', null, false);
                }
                $providerPracticeListing->url = $yelpWriteareviewUrl;
                if ($providerPracticeListing->validate()) {
                    $providerPracticeListing->save();
                }
            } elseif ($providerPracticeListing) {
                // delete yelp write a review link
                $providerPracticeListing->delete();
                // save audit
                $auditSection = 'Yelp Write a review link for practice #' . $practiceId . ' (' . $practice->name . ')';
                AuditLog::create('D', $auditSection, 'provider_practice_listing', null, false);
            }
        }
        return "y";
    }

    /**
     * Delete image from ReviewHub (logo or hero)
     * @return boolean
     */
    public static function deleteFile($accountId = 0, $accountDeviceId = 0, $targetField = '')
    {
        if ($accountId == 0) {
            if (php_sapi_name() != 'cli') {
                $ownerAccountId = OrganizationAccount::switchOwnerAccountID(Yii::app()->user->id);
            }
        } else {
            $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);
        }
        if ($accountDeviceId == 0) {
            $accountDeviceId = Yii::app()->request->getParam('accountDeviceId', 0);
        }
        if (empty($targetField)) {
            $targetField = Yii::app()->request->getParam('targetField', '');
        }

        ApiComponent::checkPermissions('AccountDevice', $ownerAccountId, $accountDeviceId);

        $accountDevice = AccountDevice::model()->find(
            "id=:id AND account_id=:account_id",
            array(":id" => $accountDeviceId, ":account_id" => $ownerAccountId)
        );

        $results['success'] = false;
        $results['http_response_code'] = 200;
        $results['error'] = "ERROR_DELETING_PHOTO";
        $results['data'] = "Error deleting photo";

        if (!empty($targetField)) {
            $accountDevice->$targetField = '';
            if ($accountDevice->save()) {
                $auditSection = 'Image in ' . $targetField . ' for Device #' . $accountDeviceId;
                AuditLog::create('D', $auditSection, 'account_device', null, false);

                $results['success'] = true;
                $results['http_response_code'] = 200;
                $results['error'] = "";
                $results['data'] = "";
            } else {
                $results['data'] = $accountDevice->getErrors();
            }
        }

        return $results;
    }

    /**
     * Save upload image from ReviewHub
     * @return boolean
     */
    public static function saveFile($accountId = 0, $accountDeviceId = 0, $changeUploadPath = false)
    {
        if ($accountDeviceId == 0) {
            $accountDeviceId = Yii::app()->request->getParam('accountDeviceId', 0);
        }
        if ($accountId == 0) {
            if (php_sapi_name() != 'cli') {
                $ownerAccountId = OrganizationAccount::switchOwnerAccountID(Yii::app()->user->id);
            }
        } else {
            $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);
        }

        ApiComponent::checkPermissions('AccountDevice', $ownerAccountId, $accountDeviceId);

        $accountDevice = AccountDevice::model()->find(
            "id=:id AND account_id=:account_id",
            array(":id" => $accountDeviceId, ":account_id" => $ownerAccountId)
        );
        if ($changeUploadPath) {
            // change uploadPath
            $accountDevice->uploadPath = trim($accountDevice->basePath, '\api') . DIRECTORY_SEPARATOR .
                'providers' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR .
                'devices' . DIRECTORY_SEPARATOR;
        }

        $target = '';
        if (isset($_FILES['AccountDevice']['name']['logo_path'])) {
            $target = 'logo_path';
            $resultFileUpload = File::upload($accountDevice, $target);
            $accountDevice->logo_path = $resultFileUpload['file_name'];
        }
        if (isset($_FILES['AccountDevice']['name']['logo_hero_path'])) {
            $target = 'logo_hero_path';
            $resultFileUpload = File::upload($accountDevice, $target);
            $accountDevice->logo_hero_path = $resultFileUpload['file_name'];
        }
        if (isset($_FILES['AccountDevice']['name']['logo_office_staff_path'])) {
            $target = 'logo_office_staff_path';
            $resultFileUpload = File::upload($accountDevice, $target);
            $accountDevice->logo_office_staff_path = $resultFileUpload['file_name'];
        }
        $results['success'] = false;
        $results['http_response_code'] = 200;
        $results['error'] = "ERROR_UPLOADING_PHOTO";
        $results['data'] = "Error uploading photo";

        if ($accountDevice->save()) {
            // no need to notify phabricator here
            $results['success'] = true;
            $results['http_response_code'] = 200;
            $results['error'] = "";
            $results['data'] = [];
            $path = 'https://' . Yii::app()->params['servers']['dr_pro_hn'] . '/images/uploads/devices/';
            if (!empty($accountDevice->logo_path)) {
                $results['data']['accountDevice']['logo_path'] = $path . $accountDevice->logo_path;
            }
            if (!empty($accountDevice->logo_hero_path)) {
                $results['data']['accountDevice']['logo_hero_path'] = $path . $accountDevice->logo_hero_path;
            }
            if (!empty($accountDevice->logo_office_staff_path)) {
                $results['data']['accountDevice']['logo_office_staff_path'] = $path .
                    $accountDevice->logo_office_staff_path;
            }

            // save audit
            $auditSection = 'Device #' . $accountDeviceId . ' (' . $target . ')';
            AuditLog::create('U', $auditSection, 'account_device', null, false);
        } else {
            $results['data'] = $accountDevice->getErrors();
        }
        return $results;
    }

    /**
     * Validate Links for RH update
     * @param string $link
     * @param string $listingTypeId
     * @param string $practiceId
     */
    public static function validateLink($link = '', $listingTypeId = '', $practiceId = '')
    {
        // just a link validation
        // try to create the link, validate
        $ppl = new ProviderPracticeListing();
        $ppl->listing_type_id = $listingTypeId;
        $ppl->practice_id = $practiceId;
        $ppl->url = $link;
        $ppl->source = 'PADM / RH';
        $ppl->verified = 1;
        if (!$ppl->validate()) {
            // could not validate
            $validateResult = CJSON::encode(array(
                'error' => true,
                'message' => StringComponent::flatten($ppl->getErrors())
            ));
        } else {
            $validateResult = CJSON::encode(array('success' => true));
        }
        return $validateResult;
    }

    /**
     * Read the record, and related info to edit process
     * @param int $accountId
     * @param int $accountDeviceId
     * @param bool $convertToArray
     * @return array
     */
    public static function getUpdate($accountId = null, $accountDeviceId = null, $convertToArray = false)
    {
        ApiComponent::checkPermissions('AccountDevice', $accountId, $accountDeviceId);

        $accountInfo = Account::getInfo($accountId, true, false);
        $ownerAccountId = $accountInfo['owner_account_id'];

        $accountDevice = AccountDevice::model()->find(
            "id=:id AND account_id=:account_id",
            array(":id" => $accountDeviceId, ":account_id" => $accountId)
        );
        $practiceId = !empty($accountDevice->practice_id) ? $accountDevice->practice_id : 0;
        $concatProviders = AccountDeviceProvider::getAccountDeviceProviders($accountDevice['id'], $practiceId);

        $cacheMedium = Yii::app()->params['cache_medium'];
        $practice = Practice::model()->cache($cacheMedium)->findByPk($practiceId);

        $defaultLanguage = Localization::model()->getLocalizationCode($accountDevice->localization_id);
        $deniedLanguages = explode(',', $accountDevice->denied_languages);

        $arrLanguages = Common::getAvailableLanguages();
        $defaultCta = Language::loadKeyword($arrLanguages, array('def_initial_cta'));
        $arrTranslations = AccountDeviceTranslations::getTranslations($accountDeviceId);

        $lang = array();
        foreach ($arrLanguages as $key => $value) {
            $lang[$key]['name'] = $value;
            $lang[$key]['active'] = !in_array($key, $deniedLanguages) ? true : false;
            $lang[$key]['default'] = ($key == $defaultLanguage) ? true : false;
            $lang[$key]['initial_cta'] = (!empty($arrTranslations[$key]['initial_cta']))
                ? $arrTranslations[$key]['initial_cta'] : $defaultCta[$key]['def_initial_cta'];
            $lang[$key]['localization_id'] = Localization::model()->cache($cacheMedium)->getLocalizationId($key);
        }

        // get links to show at the bottom
        $googleUrl = ProviderPracticeListing::model()->find(
            'listing_type_id=:googleProfileId AND provider_id IS NULL AND practice_id=:practiceId',
            array(':googleProfileId' => ListingType::GOOGLE_PROFILE_ID, ':practiceId' => $practiceId)
        );
        $googleUrl = $googleUrl ? $googleUrl->url : '';

        $yelpUrl = ProviderPracticeListing::model()->find(
            'listing_type_id=:yelpProfileId AND provider_id IS NULL AND practice_id=:practiceId',
            array(':yelpProfileId' => ListingType::YELP_PROFILE_ID, ':practiceId' => $practiceId)
        );
        $yelpUrl = $yelpUrl ? $yelpUrl->url : '';

        $yelpWriteareviewUrl = ProviderPracticeListing::model()->find(
            'listing_type_id=:yelpWritereviewId AND provider_id IS NULL AND practice_id=:practiceId',
            array(':yelpWritereviewId' => ListingType::YELP_WRITEREVIEW_ID, ':practiceId' => $practiceId)
        );
        $yelpWriteareviewUrl = $yelpWriteareviewUrl ? $yelpWriteareviewUrl->url : '';

        // get attributes for the title
        $device = Device::model()->cache($cacheMedium)->findByPk($accountDevice->device_id);
        $model = DeviceModel::model()->cache($cacheMedium)->findByPk($device->device_model_id);

        // providers related
        $providersPractice = ProviderPractice::getProvidersFullData($practiceId, $ownerAccountId, true);

        $providersInRH = AccountDeviceProvider::model()->cache($cacheMedium)->findAll(
            'account_device_id=:accountDeviceId',
            array(':accountDeviceId' => $accountDevice->id)
        );

        $providerNames = '';
        if ($providersInRH) {
            foreach ($providersPractice as $key => $pp) {
                if (!isset($providersPractice[$key]['selected'])) {
                    $providersPractice[$key]['selected'] = false;
                }

                foreach ($providersInRH as $provider) {
                    if ($pp['id'] == $provider->provider_id) {
                        $providerNames .= $pp['first_m_last'] . ', ';
                        $providersPractice[$key]['selected'] = true;
                        break;
                    }
                }
            }
            $providerNames = trim($providerNames);
            $providerNames = '(' . trim($providerNames, ',') . ')';
        }

        $practiceDetails = PracticeDetails::model()->find(
            'practice_id=:practiceId',
            array(':practiceId' => $practiceId)
        );
        $rhSharing = $practiceDetails ? $practiceDetails->reviewhub_sharing : '';

        // to look up google link
        $location = Location::model()->cache($cacheMedium)->findByPk($practice->location_id);

        // only admins impersonating can update the links
        $canUpdateLinks = !empty(Yii::app()->session['admin_account_id']);

        if ($convertToArray) {
            $accountDevice = (array)$accountDevice->attributes;
            $accountDevice['concat_providers'] = explode(',', $concatProviders);
            $device = (array)$device->attributes;
            $model = (array)$model->attributes;
            $practice = (array)$practice->attributes;
            $practiceDetails = (array)$practiceDetails->attributes;
            $location = (array)$location->attributes;
        }

        $accountReviewHubUpdate['accountDevice'] = $accountDevice;
        $accountReviewHubUpdate['arrLanguages'] = $lang;
        $accountReviewHubUpdate['googleUrl'] = $googleUrl;
        $accountReviewHubUpdate['yelpUrl'] = $yelpUrl;
        $accountReviewHubUpdate['yelpWriteareviewUrl'] = $yelpWriteareviewUrl;
        $accountReviewHubUpdate['device'] = $device;
        $accountReviewHubUpdate['model'] = $model;
        $accountReviewHubUpdate['practice'] = $practice;
        $accountReviewHubUpdate['providerNames'] = $providerNames;
        $accountReviewHubUpdate['providers_at_practice'] = $providersPractice;
        $accountReviewHubUpdate['practiceDetails'] = $practiceDetails;
        $accountReviewHubUpdate['rhSharing'] = $rhSharing;
        $accountReviewHubUpdate['location'] = $location;
        $accountReviewHubUpdate['canUpdateLinks'] = $canUpdateLinks;

        return $accountReviewHubUpdate;
    }

    /**
     * Graph 1: Get daily device status, to be displayed in a graph in the review hub dashboard in GADM
     * This data isn't affected by the status or source filters
     * @return array
     */
    public static function getDevicesByStatus()
    {

        $arrDevices = null;
        $curDate = date('Y-m-d');
        $date = self::getFirstReview();

        // Available - Demo - Broken or permanently stranded - Repairing - Stranded - Unavailable (aka In use)
        $arrTmpTotals = array('A' => 0, 'D' => 0, 'B' => 0, 'R' => 0, 'S' => 0, 'U' => 0);

        // build an array with every date from the beginning of time until now and empty statuses
        while ($date < $curDate) {

            $arrDevices[$date] = $arrTmpTotals;

            $date = TimeComponent::getNextDate($date);
        }

        // query history data and fill in the array with values for the dates we have
        $sql = sprintf(
            "SELECT prev_history.prev_status, new_history.new_status, date_date_added AS date_added, device_count
            FROM (
                SELECT MIN(device_history.id) AS min_history_id, MAX(device_history.id) AS max_history_id,
                DATE(device_history.date_added) AS date_date_added, 1 AS device_count
                    FROM device_history
                    INNER JOIN device
                    ON device_history.device_id = device.id
                    GROUP BY device_id, date_date_added
                    ORDER BY device_history.date_added ASC
                ) AS subQuery
            LEFT JOIN device_history AS prev_history
                ON min_history_id = prev_history.id
            LEFT JOIN device_history AS new_history
                ON max_history_id = new_history.id;"
        );
        $array = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();

        foreach ($array as $device) {
            $date = $device['date_added'];

            // keep an array of status
            $arrTmpTotals[$device['new_status']] += $device['device_count'];
            if (!empty($device['prev_status'])) {
                $arrTmpTotals[$device['prev_status']] -= $device['device_count'];
                if ($arrTmpTotals[$device['prev_status']] < 0) {
                    $arrTmpTotals[$device['prev_status']] = 0;
                }
            }

            // populate date with new status
            foreach ($arrTmpTotals as $status => $value) {
                $arrDevices[$date][$status] = $value;
            }
        }

        // Available - Demo - Broken or permanently stranded - Repairing - Stranded - Unavailable (aka In use)
        $tmpA = $tmpD = $tmpB = $tmpR = $tmpS = $tmpU = 0;

        // populate each date with that array
        foreach ($arrDevices as $k => $device) {

            if ($device['U'] == 0) {

                $arrDevices[$k]['A'] = $tmpA;
                $arrDevices[$k]['D'] = $tmpD;
                $arrDevices[$k]['B'] = $tmpB;
                $arrDevices[$k]['R'] = $tmpR;
                $arrDevices[$k]['S'] = $tmpS;
                $arrDevices[$k]['U'] = $tmpU;
            }

            $tmpA = $arrDevices[$k]['A'];
            $tmpD = $arrDevices[$k]['D'];
            $tmpB = $arrDevices[$k]['B'];
            $tmpR = $arrDevices[$k]['R'];
            $tmpS = $arrDevices[$k]['S'];
            $tmpU = $arrDevices[$k]['U'];
        }

        return $arrDevices;
    }

    /**
     * Graph 2: Get average days passed until the first review, to be displayed in a graph in the review hub dashboard
     * in GADM
     * @param string $statusFilter
     * @return array
     */
    public static function getAverageDaysUntilFirstReview($statusFilter = '')
    {

        // build parameters
        $ratingStatus = self::buildStatusCondition($statusFilter);

        $arrRatings = array();
        $curDate = date('Y-m-d');
        $date = self::getFirstReview($ratingStatus);

        // build an array with every date from the beginning of time until now and empty diffs
        while ($date != $curDate) {
            $arrRatings[$date]['day_sum'] = null;
            $arrRatings[$date]['devices'] = null;

            $date = TimeComponent::getNextDate($date);
        }

        // base query: get first rating, difference from date delivered an device_id
        $sql = sprintf(
            "SELECT MIN(DATE(provider_rating.date_added)) AS `date`,
            DATEDIFF(MIN(provider_rating.date_added),
            IF(account_device.date_delivered IS NOT NULL, account_device.date_delivered, date_enrolled)) AS
            `diff`, account_device.device_id
            FROM account_device
            INNER JOIN device
                ON account_device.device_id = device.id
            LEFT JOIN shipping_log
                ON account_device.shipment_tracking_number = shipping_log.tracking_number
                AND account_device.date_delivered IS NOT NULL
            INNER JOIN provider_rating
                ON account_device.id = provider_rating.account_device_id
                AND provider_rating.date_added >= IF(account_device.date_delivered IS NOT NULL,
            account_device.date_delivered, date_enrolled)
            WHERE %s
                AND date_enrolled != '0000-00-00'
                AND provider_rating.date_added >= '%s'
                AND account_device.status = 'A'
            GROUP BY account_device.device_id
            ORDER BY date_enrolled",
            $ratingStatus,
            self::getFirstReview($ratingStatus)
        );
        $array = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();

        // this removes the top and bottom 2.5% from the result
        $remove = round(2.5 * sizeof($array) / 100);
        if (sizeof($array) > $remove * 2) {
            $array = array_slice($array, $remove, -$remove);
        }

        // sort the array by date
        usort($array, 'self::compareDate');

        // prepare output
        $arrRatings = array();
        foreach ($array as $rating) {
            if ($rating['diff'] > 0) {
                if (!isset($arrRatings[$rating['date']])) {
                    $arrRatings[$rating['date']] = array('day_sum' => 0, 'devices' => 0);
                }
                $arrRatings[$rating['date']]['day_sum'] += $rating['diff'];
                $arrRatings[$rating['date']]['devices']++;
            }
        }

        // average data
        foreach ($arrRatings as $rating_k => $rating) {
            if ($rating['devices'] !== null) {
                $lastRecord = round($rating['day_sum'] / $rating['devices'], 1);
                $arrRatings[$rating_k]['devices'] = $lastRecord;
            } else {
                $arrRatings[$rating_k]['devices'] = (isset($lastRecord) ? $lastRecord : null);
            }
            unset($rating['day_sum']);
        }

        return $arrRatings;
    }

    /**
     * Graph 3: Get average ratings to be displayed in a graph in the review hub dashboard in GADM
     * @param string $statusFilter
     * @param string $sourceFilter
     * @return array
     */
    public static function getAverageRating($statusFilter = '', $sourceFilter = '')
    {

        // build parameters
        $ratingStatus = self::buildStatusCondition($statusFilter);
        list($ratingSourceCondition, $ratingSourceJoin) = self::buildSourceCondition($sourceFilter);

        $arrRatings = null;
        $curDate = date('Y-m-d');
        $date = self::getFirstReview($ratingStatus);

        // build an array with every date from the beginning of time until now and empty ratings
        while ($date != $curDate) {
            $arrRatings[$date] = '0';
            $date = TimeComponent::getNextDate($date);
        }

        // query every rating and fill in the array with values for the dates we have
        $sql = sprintf(
            "SELECT rating AS rating, rating_getting_appt AS rating_getting_appt, rating_appearance AS
            rating_appearance,
            rating_courtesy AS rating_courtesy, rating_waiting_time AS rating_waiting_time, rating_billing AS
            rating_billing,
            DATE_FORMAT(provider_rating.date_added, '%%Y-%%m-%%d') AS `date`
            FROM provider_rating
            %s
            WHERE %s
            %s
            AND provider_rating.date_added >= '%s'
            GROUP BY `date`
            ORDER BY `date` ASC;",
            $ratingSourceJoin,
            $ratingStatus,
            $ratingSourceCondition,
            self::getFirstReview($ratingStatus)
        );
        $array = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();

        foreach ($array as $rating) {
            $arrRatings[$rating['date']] = array();
            if ($rating['rating'] > 0) {
                $arrRatings[$rating['date']]['rating'] = $rating['rating'];
            }
            if ($rating['rating_getting_appt'] > 0) {
                $arrRatings[$rating['date']]['rating_getting_appt'] = $rating['rating_getting_appt'];
            }
            if ($rating['rating_appearance'] > 0) {
                $arrRatings[$rating['date']]['rating_appearance'] = $rating['rating_appearance'];
            }
            if ($rating['rating_courtesy'] > 0) {
                $arrRatings[$rating['date']]['rating_courtesy'] = $rating['rating_courtesy'];
            }
            if ($rating['rating_waiting_time'] > 0) {
                $arrRatings[$rating['date']]['rating_waiting_time'] = $rating['rating_waiting_time'];
            }
            if ($rating['rating_billing'] > 0) {
                $arrRatings[$rating['date']]['rating_billing'] = $rating['rating_billing'];
            }
        }

        return $arrRatings;
    }

    /**
     * Graph 4: Get average ratings to be displayed in a graph in the review hub dashboard in GADM
     * @param string $statusFilter
     * @param string $deviceFilter
     * @return array
     */
    public static function getLastMonthBreakdown($statusFilter = '', $deviceFilter = '')
    {

        // build parameters
        $ratingStatus = self::buildStatusCondition($statusFilter);

        $sql = sprintf(
            "SELECT SUM(total_0) AS total_0, SUM(total_1) AS total_1, SUM(total_2) AS total_2,
            SUM(total_3) AS total_3, SUM(total_4) AS total_4, SUM(total_5) AS total_5
            FROM (
                SELECT IF(COUNT(provider_rating.id) = 0, 1, 0) AS total_0,
                IF(COUNT(provider_rating.id) = 1, 1, 0) AS total_1,
                IF(COUNT(provider_rating.id) BETWEEN 2 AND 4, 1, 0) AS total_2,
                IF(COUNT(provider_rating.id) BETWEEN 5 AND 9, 1, 0) AS total_3,
                IF(COUNT(provider_rating.id) BETWEEN 10 AND 19, 1, 0) AS total_4,
                IF(COUNT(provider_rating.id) >= 20, 1, 0) AS total_5
                FROM device
                LEFT JOIN provider_rating
                ON provider_rating.device_id = device.id
                AND (DATEDIFF(CURDATE(), provider_rating.date_added) <= 30 OR provider_rating.date_added IS NULL)
                AND (%s OR provider_rating.id IS NULL)
                WHERE device.status %s
                GROUP BY device.id
            ) AS subQuery;",
            $ratingStatus,
            ($deviceFilter == '0' ? 'IN ("U", "S")' : '= "U"')
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Graph 5: Get number of devices in the field
     * @param int $stackFilter
     * @param int $timeFilter
     * @param int $statusFilter
     * @param int $accountId
     * @param int $providerId
     * @param string $exportFrom
     * @param string $exportTo
     * @return array
     */
    public static function getDevicesInField(
        $stackFilter,
        $timeFilter,
        $statusFilter,
        $accountId = 0,
        $providerId = 0,
        $exportFrom = '',
        $exportTo = ''
    ) {

        // build parameters
        $queryField = 'device_history.date_added';
        list($timeField, $timeGroup) = self::buildTimeCondition($timeFilter, $queryField);
        $ratingStatus = self::buildStatusCondition($statusFilter);
        list($additionalFields, $joins, $groupBy) = self::buildStackFilter($stackFilter, 'INFIELD', $ratingStatus);
        $condition = self::buildOwnerCondition($accountId, $providerId);
        $condition .= self::buildExportCondition($exportFrom, $exportTo, $queryField);

        if ($additionalFields == 'COUNT(1) AS total') {
            // here we want to group by device instead
            $additionalFields = 'COUNT(DISTINCT(device_history.id)) AS total';
        }

        // retrieve events when a device started or stopped being active, which we'll use to add or substract from
        //  totals
        $sql = sprintf(
            "SELECT DATE(device_history.date_added) AS event_date, %s AS formatted_event_date,
            IF(prev_status = 'U', 'U', 'X') AS prev_status,
            IF(new_status = 'U', 'U', 'X') AS new_status
            %s
            FROM device_history
            INNER JOIN device
            ON device_history.device_id = device.id
            LEFT JOIN provider_rating
            ON device_history.device_id = provider_rating.device_id
            %s
            AND (%s OR provider_rating.id IS NULL)
            WHERE 1 = 1
            %s
            GROUP BY device_history.device_id, %s, IF(new_status = 'U', 'U', 'X')
            %s
            ORDER BY %s, device_history.date_added, new_status;",
            $timeField,
            ($additionalFields != '' ? ', ' .
                $additionalFields : ''),
            $joins,
            $ratingStatus,
            $condition,
            $timeGroup,
            ($groupBy != '' ? ', ' . $groupBy : ''),
            $timeGroup
        );
        $tmpDevices = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
        $arrDevices = null;
        $tmpTotals['total'] = 0;

        foreach ($tmpDevices as $v) {

            // calculate total
            if ($v['new_status'] == 'U') {
                // if it's unavailable then it's active
                $tmpTotals['total'] = $v['total'] = $tmpTotals['total'] + (isset($v['total']) ? $v['total'] : 0);
            } elseif ($v['prev_status'] == 'U') {
                // x means substract -- but don't substract if we're changing from X to X
                $tmpTotals['total'] = $v['total'] = $tmpTotals['total'] - (isset($v['total']) ? $v['total'] : 0);
            }

            // calculate other totals
            $iTo = self::getFieldsRange($stackFilter, $ratingStatus);

            for ($i = 0; $i <= $iTo; $i++) {
                if (isset($v['total_' . $i])) {
                    if (!(isset($tmpTotals['total_' . $i]))) {
                        $tmpTotals['total_' . $i] = 0;
                    }
                    if ($v['new_status'] == 'U') {
                        $tmpTotals['total_' . $i] = $v['total_' . $i] = $tmpTotals['total_' . $i] + $v['total_' . $i];
                    } elseif ($v['prev_status'] == 'U') {
                        $tmpTotals['total_' . $i] = $v['total_' . $i] = $tmpTotals['total_' . $i] - $v['total_' . $i];
                    }
                    if ($v['total_' . $i] < 0) {
                        $v['total_' . $i] = 0;
                    }
                }
            }

            // don't repeat dates
            if ($v['new_status'] == 'U') {
                $arrDevices[] = $v;
            } elseif (is_array($arrDevices)) {
                end($arrDevices);
                $arrDevices[key($arrDevices)] = $v;
            }
        }

        return $arrDevices;
    }

    /**
     * Graph 6: Get utilized devices
     * @param int $stackFilter
     * @param int $timeFilter
     * @param int $statusFilter
     * @param int $accountId
     * @param int $providerId
     * @param string $exportFrom
     * @param string $exportTo
     * @return array
     */
    public static function getUtilizedDevices(
        $stackFilter,
        $timeFilter = '',
        $statusFilter = '',
        $accountId = 0,
        $providerId = 0,
        $exportFrom = '',
        $exportTo = ''
    ) {

        // build parameters
        $queryField = 'provider_rating.date_added';
        list($timeField, $timeGroup) = self::buildTimeCondition($timeFilter, $queryField);
        $ratingStatus = self::buildStatusCondition($statusFilter);
        list($additionalFields, $joins, $groupBy) = self::buildStackFilter($stackFilter, 'UTILIZED', $ratingStatus);
        $condition = self::buildOwnerCondition($accountId, $providerId);
        $condition .= self::buildExportCondition($exportFrom, $exportTo, $queryField);

        if ($additionalFields == 'COUNT(1) AS total') {
            // here we want to group by device instead
            $additionalFields = 'COUNT(DISTINCT provider_rating.device_id) AS total';
        }

        $sql = sprintf(
            "SELECT DATE(provider_rating.date_added) AS event_date, %s AS formatted_event_date
            %s
            FROM provider_rating
            INNER JOIN device
            ON provider_rating.device_id = device.id
            %s
            WHERE device.status = 'U'
            %s
            AND (%s OR provider_rating.id IS NULL)
            GROUP BY provider_rating.device_id, %s
            %s
            ORDER BY event_date;",
            $timeField,
            ($additionalFields != '' ? ', ' . $additionalFields : ''),
            $joins,
            $condition,
            $ratingStatus,
            $timeGroup,
            ($groupBy != '' ? ', ' . $groupBy : '')
        );
        $tmpDevices = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
        $arrDevices = array();
        $lastDate = null;
        $tmpTotals['total'] = 0;
        foreach ($tmpDevices as $v) {

            $reset = false;
            if ($lastDate != $v['formatted_event_date']) {
                // reset counters
                $tmpTotals['total'] = 0;
                $lastDate = $v['formatted_event_date'];
                $reset = true;
            }

            // calculate total
            $tmpTotals['total'] = $v['total'] = $tmpTotals['total'] + (isset($v['total']) ? $v['total'] : 0);

            // calculate other totals
            $iTo = self::getFieldsRange($stackFilter, $ratingStatus);

            for ($i = 0; $i <= $iTo; $i++) {
                if (!isset($v['total_' . $i])) {
                    continue;
                }

                if ($reset) {
                    // reset counters
                    $tmpTotals['total_' . $i] = 0;
                }

                if (!(isset($tmpTotals['total_' . $i]))) {
                    $tmpTotals['total_' . $i] = 0;
                }
                $tmpTotals['total_' . $i] = $v['total_' . $i] = $tmpTotals['total_' . $i] + $v['total_' . $i];

                if ($v['total_' . $i] < 0) {
                    $v['total_' . $i] = 0;
                }
            }

            // don't repeat dates
            if ($reset) {
                $arrDevices[] = $v;
            } else {
                end($arrDevices);
                $arrDevices[key($arrDevices)] = $v;
            }
        }

        return $arrDevices;
    }

    /**
     * Graph 7: Get average reviews per device in the field
     * Graph 8: Get average reviews per device utilized
     * Graph 9: Get average reviews per account
     * @param int $stackFilter
     * @param int $timeFilter
     * @param int $statusFilter
     * @param int $accountId
     * @param int $providerId
     * @param string $type
     * @param string $exportFrom
     * @param string $exportTo
     * @return array
     */
    public static function getAverageReviewsPerDevice(
        $stackFilter,
        $timeFilter = '',
        $statusFilter = '',
        $accountId = 0,
        $providerId = 0,
        $type = false,
        $exportFrom = '',
        $exportTo = ''
    ) {

        // build parameters
        $queryField = 'account_device_history.date_added';
        list($timeField, $timeGroup) = self::buildTimeCondition($timeFilter, $queryField);
        $ratingStatus = self::buildStatusCondition($statusFilter);
        list($additionalFields, $joins, $groupBy) = self::buildStackFilter($stackFilter, false, $ratingStatus);

        $arrDates = null;
        $curDate = date('Y-m-d');
        $date = self::getFirstReview($ratingStatus);

        // build an array with every date from the beginning of time until now and empty statuses
        while ($date != $curDate) {
            $arrDates[$date]['A'] = 0;
            $date = TimeComponent::getNextDate($date);
        }

        // query history data and fill in the array with values for the dates we have
        $sql = '';

        if ($type == 'INFIELD') {

            $condition = self::buildOwnerCondition($accountId, $providerId, true);
            $condition .= self::buildExportCondition($exportFrom, $exportTo, $queryField);

            $sql = sprintf(
                "SELECT prev_status, new_status, DATE(account_device_history.date_added) AS event_date,
                COUNT(DISTINCT(device.id)) AS total, %s AS formatted_event_date
                FROM account_device_history
                INNER JOIN account_device
                ON account_device_history.account_device_id = account_device.id
                AND account_device.status = 'A'
                INNER JOIN device
                ON account_device.device_id = device.id
                %s
                WHERE (new_status = 'A' OR prev_status = 'A')
                %s
                GROUP BY new_status, prev_status,
                %s
                ORDER BY event_date ASC, new_status = 'A' DESC;",
                $timeField,
                ($condition != '' ? 'LEFT JOIN'
                    . ' account_device_provider ON account_device.id = account_device_provider.account_device_id' : ''),
                $condition,
                $timeGroup
            );
        } elseif ($type == 'ACTIVE') {

            $condition = self::buildOwnerCondition($accountId, $providerId);
            $condition .= self::buildExportCondition($exportFrom, $exportTo, $queryField);

            $sql = sprintf(
                "SELECT prev_status, new_status, DATE(account_device_history.date_added) AS event_date,
                COUNT(DISTINCT(device.id)) AS total, %s AS formatted_event_date
                FROM account_device_history
                INNER JOIN account_device
                    ON account_device_history.account_device_id = account_device.id
                    AND account_device.status = 'A'
                INNER JOIN device
                    ON account_device.device_id = device.id
                INNER JOIN provider_rating
                    ON device.id = provider_rating.device_id
                WHERE (new_status = 'A' OR prev_status = 'A')
                %s
                GROUP BY new_status, prev_status,
                %s
                ORDER BY event_date ASC, new_status = 'A' DESC;",
                $timeField,
                $condition,
                $timeGroup
            );
        } elseif ($type == 'PERACCOUNT') {

            $condition = self::buildOwnerCondition($accountId, $providerId, true);
            $condition .= self::buildExportCondition($exportFrom, $exportTo, $queryField);

            $sql = sprintf(
                "SELECT prev_status, new_status, DATE(account_device_history.date_added) AS event_date,
                COUNT(DISTINCT(account_device.account_id)) AS total, %s AS formatted_event_date
                FROM account_device_history
                INNER JOIN account_device
                ON account_device_history.account_device_id = account_device.id
                AND account_device.status = 'A'
                INNER JOIN device
                ON account_device.device_id = device.id
                %s
                WHERE (new_status = 'A' OR prev_status = 'A')
                %s
                GROUP BY new_status, prev_status,
                %s
                ORDER BY event_date ASC, new_status = 'A' DESC;",
                $timeField,
                ($condition != '' ? 'LEFT JOIN'
                    . ' account_device_provider ON account_device.id = account_device_provider.account_device_id' : ''),
                $condition,
                $timeGroup
            );
        }

        if (empty($sql)) {
            return false;
        }

        // accumulate number of active devices or accounts per day
        $array = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
        foreach ($array as $device) {
            $date = $device['event_date'];
            while ($date != $curDate) {
                if ($device['new_status'] == 'A') {
                    $arrDates[$date]['A'] += $device['total'];
                } else {
                    $arrDates[$date]['A'] -= $device['total'];
                }
                if ($arrDates[$date]['A'] < 0) {
                    $arrDates[$date]['A'] = 0;
                }
                $date = TimeComponent::getNextDate($date);
            }
        }

        // re-build condition (without context)
        $condition = self::buildOwnerCondition($accountId, $providerId);
        $condition .= self::buildExportCondition($exportFrom, $exportTo, $queryField);

        // query number of ratings
        $sql = sprintf(
            "SELECT DATE(account_device_history.date_added) AS event_date,
            COUNT(DISTINCT(provider_rating.id)) AS tmp_total, %s AS formatted_event_date
            %s
            FROM account_device_history
            INNER JOIN account_device
            ON account_device_history.account_device_id = account_device.id
            INNER JOIN device
            ON account_device.device_id = device.id
            INNER JOIN provider_rating
            ON account_device.device_id = provider_rating.device_id
            %s
            WHERE (new_status = 'A' OR prev_status = 'A')
            %s
            AND %s
            GROUP BY %s
            %s
            ORDER BY event_date;",
            $timeField,
            ($additionalFields != '' ? ', ' . $additionalFields : ''),
            $joins,
            $condition,
            $ratingStatus,
            $timeGroup,
            ($groupBy != '' ? ', ' . $groupBy : '')
        );

        $tmpRatings = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();

        $arrRatings = null;
        foreach ($tmpRatings as $v) {

            // calculate number of ratings relative to active devices
            $v['total'] = 0;
            if (!empty($arrDates[$v['event_date']]) && $arrDates[$v['event_date']]['A'] != 0) {
                $v['total'] = round($v['tmp_total'] / $arrDates[$v['event_date']]['A'], 1);
            }

            $iTo = self::getFieldsRange($stackFilter, $ratingStatus);

            for ($i = 0; $i <= $iTo; $i++) {
                if (!isset($v['total_' . $i])) {
                    continue;
                }
                if (!empty($arrDates[$v['event_date']]) && $arrDates[$v['event_date']]['A'] != 0) {
                    $v['total_' . $i] = round($v['total_' . $i] / $arrDates[$v['event_date']]['A'], 1);
                }
            }
            unset($v['tmp_total']);
            $arrRatings[] = $v;
        }

        return $arrRatings;
    }

    /**
     * Graph 10: Get total reviews made
     * Graph 11: Get new reviews made
     * @param int $stackFilter
     * @param int $timeFilter
     * @param int $statusFilter
     * @param int $sourceFilter
     * @param int $accountId
     * @param int $providerId
     * @param bool $accumulate
     * @param string $exportFrom
     * @param string $exportTo
     * @return array
     */
    public static function getTotalReviews(
        $stackFilter,
        $timeFilter,
        $statusFilter,
        $sourceFilter,
        $accountId = 0,
        $providerId = 0,
        $accumulate = false,
        $exportFrom = '',
        $exportTo = ''
    ) {

        // build parameters
        $queryField = 'provider_rating.date_added';
        list($timeField, $timeGroup) = self::buildTimeCondition($timeFilter, $queryField);
        $ratingStatus = self::buildStatusCondition($statusFilter);
        list($additionalFields, $joins, $groupBy) = self::buildStackFilter($stackFilter, false, $ratingStatus);
        list($ratingSourceCondition, $ratingSourceJoin) = self::buildSourceCondition($sourceFilter);
        $condition = self::buildOwnerCondition($accountId, $providerId);
        $condition .= self::buildExportCondition($exportFrom, $exportTo, $queryField);

        $sql = sprintf(
            "SELECT DATE(provider_rating.date_added) AS event_date, %s AS formatted_event_date
            %s
            FROM provider_rating
            %s
            %s
            WHERE (%s OR provider_rating.id IS NULL)
            %s
            %s
            GROUP BY %s
            %s
            ORDER BY event_date;",
            $timeField,
            ($additionalFields != '' ? ', ' . $additionalFields : ''),
            $ratingSourceJoin,
            $joins,
            $ratingStatus,
            $condition,
            $ratingSourceCondition,
            $timeGroup,
            ($groupBy != '' ? ', ' . $groupBy : '')
        );
        $tmpRatings = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();

        $total = $totals = $arrRatings = null;
        foreach ($tmpRatings as $v) {
            // calculate number of ratings relative to active devices
            if ($accumulate) {
                if (isset($v['total'])) {
                    $total += $v['total'];
                }
                $v['total'] = $total;
            }

            // if we're showing stars of other totals calculate them too
            $iTo = self::getFieldsRange($stackFilter, $ratingStatus);

            for ($i = 0; $i <= $iTo; $i++) {
                if ($accumulate && isset($v['total_' . $i])) {
                    if (isset($totals[$i])) {
                        $totals[$i] += $v['total_' . $i];
                    } else {
                        $totals[$i] = $v['total_' . $i];
                    }
                    $v['total_' . $i] = $totals[$i];
                }
            }
            unset($v['tmp_total']);
            $arrRatings[] = $v;
        }
        return $arrRatings;
    }

    /**
     * Graph 12: Get top 10 accounts by review count
     * @param int $stackFilter
     * @param int $statusFilter
     * @param int $sourceFilter
     * @param int $accountId
     * @param int $providerId
     * @param string $exportFrom
     * @param string $exportTo
     * @return array
     */
    public static function getTopTenAccounts(
        $stackFilter,
        $statusFilter,
        $sourceFilter,
        $accountId = 0,
        $providerId = 0,
        $exportFrom = '',
        $exportTo = ''
    ) {

        // build parameters
        $ratingStatus = self::buildStatusCondition($statusFilter);
        list($additionalFields, $joins, $groupBy) = self::buildStackFilter($stackFilter, false, $ratingStatus);
        list($ratingSourceCondition, $ratingSourceJoin) = self::buildSourceCondition($sourceFilter);
        $condition = self::buildOwnerCondition($accountId, $providerId);
        $condition .= self::buildExportCondition($exportFrom, $exportTo, 'provider_rating.date_added');

        $sql = sprintf(
            "SELECT a.organization_name AS name, COUNT(1) AS total
            %s
            FROM provider_rating
            %s
            INNER JOIN account_provider AS ap
            ON provider_rating.provider_id = ap.provider_id
            INNER JOIN account AS a
            ON ap.account_id = a.id
            %s
            WHERE %s
            %s
            %s
            GROUP BY a.id
            %s
            ORDER BY total DESC
            LIMIT 10;",
            ($additionalFields != '' ? ', ' . $additionalFields : ''),
            $ratingSourceJoin,
            $joins,
            $ratingStatus,
            $condition,
            $ratingSourceCondition,
            ($groupBy != '' ? ', ' . $groupBy : '')
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Graph 13: Get top 10 providers by review count
     * @param int $stackFilter
     * @param int $timeFilter
     * @param int $statusFilter
     * @param int $sourceFilter
     * @param int $accountId
     * @param int $providerId
     * @param string $exportFrom
     * @param string $exportTo
     * @return array
     */
    public static function getTopTenProviders(
        $stackFilter,
        $statusFilter,
        $sourceFilter,
        $accountId = 0,
        $providerId = 0,
        $exportFrom = '',
        $exportTo = ''
    ) {

        // build parameters
        $ratingStatus = self::buildStatusCondition($statusFilter);
        list($additionalFields, $joins, $groupBy) = self::buildStackFilter($stackFilter, false, $ratingStatus);
        list($ratingSourceCondition, $ratingSourceJoin) = self::buildSourceCondition($sourceFilter);
        $condition = self::buildOwnerCondition($accountId, $providerId);
        $condition .= self::buildExportCondition($exportFrom, $exportTo, 'provider_rating.date_added');

        $sql = sprintf(
            "SELECT CONCAT(provider_helper.first_name, ' ', provider_helper.last_name) AS name,
            COUNT(1) AS total
            %s
            FROM provider_rating
            %s
            INNER JOIN provider AS provider_helper
            ON provider_rating.provider_id = provider_helper.id
            %s
            WHERE %s
            %s
            %s
            GROUP BY provider_rating.provider_id
            %s
            ORDER BY total DESC
            LIMIT 10;",
            ($additionalFields != '' ? ', ' . $additionalFields : ''),
            $ratingSourceJoin,
            $joins,
            $ratingStatus,
            $condition,
            $ratingSourceCondition,
            ($groupBy != '' ? ', ' . $groupBy : '')
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get the first date we have approved or pending reviews through a review hub that's available
     * @param string $ratingStatus
     * @return string
     */
    private static function getFirstReview($ratingStatus = '1=1')
    {

        $sql = sprintf(
            "SELECT DATE_FORMAT(MIN(provider_rating.date_added), '%%Y-%%m-%%d')
            FROM provider_rating
            INNER JOIN account_device
                ON provider_rating.account_device_id = account_device.id
            INNER JOIN device
                ON account_device.device_id = device.id
            WHERE account_device_id IS NOT NULL
                AND %s;",
            $ratingStatus
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Given the stack filter parameter, build field, join and group by parameters for the graph queries
     * @param int $stackFilter
     * @param mixed $specialCase
     * @param string $ratingStatus
     * @return array
     */
    private static function buildStackFilter($stackFilter, $specialCase = false, $ratingStatus = '')
    {

        switch ($stackFilter) {
            case 1:
                // By star rating (1-star, 2-star, etc.)
                if ($specialCase === 'INFIELD' || $specialCase == 'UTILIZED') {
                    // because it groups by device.id, we need to count 1 per each
                    $additionalFields = 'ROUND(SUM(CASE WHEN rating IS NULL THEN 1 ELSE 0 END) / COUNT(1), 2) AS
                        total_0,
                        ROUND(SUM(CASE WHEN rating = 1 THEN 1 ELSE 0 END) / COUNT(1), 2) AS total_1,
                        ROUND(SUM(CASE WHEN rating = 2 THEN 1 ELSE 0 END) / COUNT(1), 2) AS total_2,
                        ROUND(SUM(CASE WHEN rating = 3 THEN 1 ELSE 0 END) / COUNT(1), 2) AS total_3,
                        ROUND(SUM(CASE WHEN rating = 4 THEN 1 ELSE 0 END) / COUNT(1), 2) AS total_4,
                        ROUND(SUM(CASE WHEN rating = 5 THEN 1 ELSE 0 END) / COUNT(1), 2) AS total_5';
                } else {
                    $additionalFields = 'SUM(CASE WHEN rating IS NULL THEN 1 ELSE 0 END) AS total_0,
                        SUM(CASE WHEN rating = 1 THEN 1 ELSE 0 END) AS total_1,
                        SUM(CASE WHEN rating = 2 THEN 1 ELSE 0 END) AS total_2,
                        SUM(CASE WHEN rating = 3 THEN 1 ELSE 0 END) AS total_3,
                        SUM(CASE WHEN rating = 4 THEN 1 ELSE 0 END) AS total_4,
                        SUM(CASE WHEN rating = 5 THEN 1 ELSE 0 END) AS total_5';
                }
                $joins = '';
                $groupBy = '';
                break;
            case 2:
                // By total reviews made in that time [buckets for # of reviews: 0, 1, 2-4, 5-9, 10-19, 20+]
                if ($specialCase == 'INFIELD' || $specialCase == 'UTILIZED') {
                    $additionalFields = 'IF(COUNT(provider_rating.id) = 0, 1, 0) AS total_0,
                    IF(COUNT(provider_rating.id) = 1, 1, 0) AS total_1,
                    IF(COUNT(provider_rating.id) BETWEEN 2 AND 4, 1, 0) AS total_2,
                    IF(COUNT(provider_rating.id) BETWEEN 5 AND 9, 1, 0) AS total_3,
                    IF(COUNT(provider_rating.id) BETWEEN 10 AND 19, 1, 0) AS total_4,
                    IF(COUNT(provider_rating.id) >= 20, 1, 0) AS total_5';
                } else {
                    $additionalFields = 'IF(COUNT(provider_rating.id) = 0, COUNT(device.id), 0) AS total_0,
                        IF(COUNT(provider_rating.id) = 1, COUNT(provider_rating.id), 0) AS total_1,
                        IF(COUNT(provider_rating.id) BETWEEN 2 AND 4, COUNT(provider_rating.id), 0) AS total_2,
                        IF(COUNT(provider_rating.id) BETWEEN 5 AND 9, COUNT(provider_rating.id), 0) AS total_3,
                        IF(COUNT(provider_rating.id) BETWEEN 10 AND 19, COUNT(provider_rating.id), 0) AS total_4,
                        IF(COUNT(provider_rating.id) >= 20, COUNT(provider_rating.id), 0) AS total_5';
                }
                $joins = '';
                $groupBy = '';
                break;
            case 3:
                // By author type (Anonymous, Initials, or First name Last Initial)
                if ($specialCase == 'INFIELD' || $specialCase == 'UTILIZED') {
                    $additionalFields = 'ROUND(SUM(CASE WHEN reviewer_name_anon = 0 THEN 1 ELSE 0 END) / COUNT(1), 2)
                        AS total_0,
                        ROUND(SUM(CASE WHEN reviewer_name_anon = 1 THEN 1 ELSE 0 END) / COUNT(1), 2) AS total_1,
                        ROUND(SUM(CASE WHEN reviewer_name_anon = 2 THEN 1 ELSE 0 END) / COUNT(1), 2) AS total_2,
                        ROUND(SUM(CASE WHEN reviewer_name_anon = 3 THEN 1 ELSE 0 END) / COUNT(1), 2) AS total_3,
                        ROUND(SUM(CASE WHEN reviewer_name_anon IS NULL THEN 1 ELSE 0 END) / COUNT(1), 2) AS total_4';
                } else {
                    $additionalFields = 'SUM(CASE WHEN reviewer_name_anon = 0 THEN 1 ELSE 0 END) AS total_0,
                        SUM(CASE WHEN reviewer_name_anon = 1 THEN 1 ELSE 0 END) AS total_1,
                        SUM(CASE WHEN reviewer_name_anon = 2 THEN 1 ELSE 0 END) AS total_2,
                        SUM(CASE WHEN reviewer_name_anon = 3 THEN 1 ELSE 0 END) AS total_3,
                        SUM(CASE WHEN reviewer_name_anon IS NULL THEN 1 ELSE 0 END) AS total_4';
                }
                $joins = '';
                $groupBy = '';
                break;
            case 4:
                // By people shared with (emails entered in “share with friends” section or not, 0, 1, 2-5, 5+)
                if ($specialCase == 'INFIELD' || $specialCase == 'UTILIZED') {
                    $additionalFields = ' IF(shared_with = 0 OR shared_with IS NULL, 1, 0) AS total_0,
                        IF(shared_with = 1, 1, 0) AS total_1,
                        IF(shared_with BETWEEN 2 AND 4, 1, 0) AS total_2,
                        IF(shared_with >= 5, 1, 0) AS total_3';
                } else {
                    $additionalFields = 'SUM(IF(shared_with = 0 OR shared_with IS NULL, 1, 0)) AS total_0,
                        SUM(IF(shared_with = 1, 1, 0)) AS total_1,
                        SUM(IF(shared_with BETWEEN 2 AND 4, 1, 0)) AS total_2,
                        SUM(IF(shared_with >= 5, 1, 0)) AS total_3';
                }
                $joins = '';
                $groupBy = '';
                break;
            case 5:
                // By account
                $accounts = self::getAccounts($ratingStatus);

                $additionalFields = null;
                foreach ($accounts as $account) {
                    $additionalFields[] = sprintf(
                        'SUM(CASE WHEN account.id = %d THEN 1 ELSE 0 END) AS `%s`',
                        $account['id'],
                        'total_' . $account['id']
                    ) . PHP_EOL;
                }
                $additionalFields = implode(', ', $additionalFields);

                $joins = 'INNER JOIN provider
                    ON provider_rating.provider_id = provider.id
                    INNER JOIN account_provider
                    ON provider.id = account_provider.provider_id
                    INNER JOIN account
                    ON account_provider.account_id= account.id';
                $groupBy = '';
                break;
            case 6:
                // By specialty
                $specialties = self::getSpecialties($ratingStatus);

                $additionalFields = null;
                foreach ($specialties as $specialty) {
                    if ($specialCase === 'INFIELD' || $specialCase == 'UTILIZED') {
                        $additionalFields[] = sprintf(
                            'IF(FIND_IN_SET(%d, GROUP_CONCAT(specialty.id)), 1, 0) AS `%s`',
                            $specialty['id'],
                            'total_' . $specialty['id']
                        ) . PHP_EOL;
                    } else {
                        $additionalFields[] = sprintf(
                            'SUM(CASE WHEN specialty.id = %d THEN 1 ELSE 0 END) AS `%s`',
                            $specialty['id'],
                            'total_' . $specialty['id']
                        ) . PHP_EOL;
                    }
                }
                $additionalFields = implode(', ', $additionalFields);

                $joins = 'INNER JOIN provider
                    ON provider_rating.provider_id = provider.id
                    INNER JOIN provider_specialty
                    ON provider.id = provider_specialty.provider_id
                    INNER JOIN sub_specialty
                    ON provider_specialty.sub_specialty_id = sub_specialty.id
                    INNER JOIN specialty
                    ON sub_specialty.specialty_id = specialty.id';
                $groupBy = '';
                break;
            default:
                // All (default)
                $additionalFields = 'COUNT(1) AS total';
                $joins = '';
                $groupBy = '';
                break;
        }
        return array($additionalFields, $joins, $groupBy);
    }

    /**
     * Given the time filter parameter, build condition for the graph queries
     * @param int $timeFilter
     * @param string $queryField
     * @return array
     */
    private static function buildTimeCondition($timeFilter, $queryField)
    {

        switch ($timeFilter) {
            case 1:
                // weekly
                $timeField = sprintf('CONCAT("W", WEEKOFYEAR(%s), " ", YEAR(%s))', $queryField, $queryField);
                $timeGroup = sprintf('YEAR(%s), WEEKOFYEAR(%s)', $queryField, $queryField);
                break;
            case 2:
                // monthly
                $timeField = sprintf('CONCAT(YEAR(%s), "/", MONTH(%s))', $queryField, $queryField);
                $timeGroup = sprintf('YEAR(%s), MONTH(%s)', $queryField, $queryField);
                break;
            case 3:
                // bimonthly
                $timeField = sprintf('CONCAT("B", CEILING(MONTH(%s) / 2), " ", YEAR(%s))', $queryField, $queryField);
                $timeGroup = sprintf('YEAR(%s), CEILING(MONTH(%s) / 2)', $queryField, $queryField);
                break;
            case 4:
                // quarterly
                $timeField = sprintf('CONCAT("Q", QUARTER(%s), " ", YEAR(%s))', $queryField, $queryField);
                $timeGroup = sprintf('YEAR(%s), QUARTER(%s)', $queryField, $queryField);
                break;
            case 5:
                // annual
                $timeField = $timeGroup = sprintf('YEAR(%s)', $queryField);
                break;
            default:
                // daily (all)
                $timeField = $timeGroup = sprintf('DATE(%s)', $queryField);
                break;
        }
        return array($timeField, $timeGroup);
    }

    /**
     * Build the export condition for the custom date fields
     * @param string $exportFrom
     * @param string $exportTo
     * @param string $queryField
     * @return string
     */
    private static function buildExportCondition($exportFrom, $exportTo, $queryField)
    {

        if ($exportFrom == '' || $exportTo == '') {
            return '';
        }

        return ' AND ' . $queryField . ' BETWEEN "' . $exportFrom . '" AND "' . $exportTo . '"';
    }

    /**
     * Given the accountId or providerId params, build conditions for the queries
     * @param int $accountId
     * @param int $providerId
     * @param bool $useAccountDevice
     * @return string
     */
    private static function buildOwnerCondition($accountId = 0, $providerId = 0, $useAccountDevice = false)
    {

        $condition = '';
        if ($accountId > 0) {

            $table = 'provider_rating';
            if ($useAccountDevice) {
                $table = 'account_device';
            }

            $condition = sprintf('AND %s.account_id = %d', $table, $accountId);
        } elseif ($providerId > 0) {

            $table = 'provider_rating';
            if ($useAccountDevice) {
                $table = 'account_device_provider';
            }

            $condition = sprintf('AND %s.provider_id = %d', $table, $providerId);
        }
        return $condition;
    }

    /**
     * Build the rating source conditions for the queries according to the source_filter param
     * @param string $sourceFilter 0 => All 1 => ReviewHub Only 2 => Non-ReviewHub Only
     * @return array
     */
    private static function buildSourceCondition($sourceFilter)
    {

        $ratingSourceCondition = "";
        $ratingSourceJoin = "LEFT JOIN device
            ON provider_rating.device_id = device.id";
        if ($sourceFilter === '1') {
            $ratingSourceJoin = "INNER JOIN device
            ON provider_rating.device_id = device.id";
        } elseif ($sourceFilter === '2') {
            $ratingSourceCondition = "AND (provider_rating.device_id = 0 OR provider_rating.device_id IS NULL)";
        }
        return array($ratingSourceCondition, $ratingSourceJoin);
    }

    /**
     * Build the rating status conditions for the queries according to the source_filter param
     * @param string $statusFilter
     * @return string
     */
    public static function buildStatusCondition($statusFilter = '')
    {

        $ratingStatus = "1 = 1";
        if ($statusFilter === '1') {
            $ratingStatus = "provider_rating.status = 'A'";
        }
        return $ratingStatus;
    }

    /**
     * Compare two dates
     * @param string $a
     * @param string $b
     * @return string
     */
    private static function compareDate($a, $b)
    {
        $t1 = strtotime($a['date']);
        $t2 = strtotime($b['date']);
        return $t1 - $t2;
    }

    /**
     * Return specialties for providers linked to devices
     * @param string $ratingStatus
     * @return array
     */
    public static function getSpecialties($ratingStatus)
    {

        $sql = sprintf(
            "SELECT specialty.name AS name, specialty.id, COUNT(1) AS total
            FROM provider_rating
            INNER JOIN provider_specialty
                ON provider_rating.provider_id = provider_specialty.provider_id
            INNER JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
            INNER JOIN specialty
                ON sub_specialty.specialty_id = specialty.id
            INNER JOIN device
                ON provider_rating.device_id = device.id
            WHERE %s
                AND device_id > 0
            GROUP BY specialty.id
            ORDER BY total DESC, specialty.name ASC;",
            $ratingStatus
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Return accounts for providers linked to devices
     * @param string $ratingStatus
     * @return array
     */
    public static function getAccounts($ratingStatus)
    {

        $sql = sprintf(
            "SELECT CONCAT(a.first_name, ' ', a.last_name) AS name, a.id, COUNT(1) AS total
            FROM provider_rating
            INNER JOIN account_provider AS ap
                ON provider_rating.provider_id = ap.provider_id
            INNER JOIN account AS a
                ON ap.account_id = a.id
            INNER JOIN device
                ON provider_rating.device_id = device.id
            WHERE %s
                AND device_id > 0
            GROUP BY a.id
            ORDER BY total DESC, name ASC;",
            $ratingStatus
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get range of fields to be analyzed
     * @param int $stackFilter
     * @param string $ratingStatus
     * @return int
     */
    private static function getFieldsRange($stackFilter, $ratingStatus)
    {

        $iTo = 5;
        if ($stackFilter == 5) {

            // accounts behave different
            $sql = sprintf(
                "SELECT MAX(account.id)
                FROM account
                INNER JOIN account_provider
                    ON account.id = account_provider.account_id
                INNER JOIN provider_rating
                    ON account_provider.provider_id = provider_rating.provider_id
                INNER JOIN device
                    ON provider_rating.device_id = device.id
                WHERE %s
                    AND device_id > 0;",
                $ratingStatus
            );
            $iTo = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
        } elseif ($stackFilter == 6) {

            // specialties behave different
            $sql = sprintf(
                "SELECT MAX(specialty.id)
                FROM specialty
                INNER JOIN sub_specialty
                    ON specialty.id = sub_specialty.specialty_id
                INNER JOIN provider_specialty
                    ON provider_specialty.sub_specialty_id = sub_specialty.id
                INNER JOIN provider_rating
                    ON provider_specialty.provider_id = provider_rating.provider_id
                INNER JOIN device
                    ON provider_rating.device_id = device.id
                WHERE %s
                    AND device_id > 0;",
                $ratingStatus
            );
            $iTo = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
        }
        return $iTo;
    }
}
