<?php

Yii::import('application.modules.core_models.models._base.BaseScanRequestLog');

class ScanRequestLog extends BaseScanRequestLog
{
    /**
     * Override the default method to prevent PDI sync
     * @return boolean
     */
    public function afterSave()
    {
        return true;
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
