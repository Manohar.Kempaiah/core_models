<?php

Yii::import('application.modules.core_models.models._base.BaseIdpProvision');

class IdpProvision extends BaseIdpProvision
{

    /**
     * Property to store the array position when IDP is looping a JSON.
     *
     * @var integer
     */
    public $array_position = null;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeValidate()
    {
        if (empty($this->secret)) {
            $this->secret = password_hash(md5($this->username), PASSWORD_BCRYPT, ['cost' => 12]);
        }

        return parent::beforeValidate();
    }

}
