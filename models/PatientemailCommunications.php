<?php

Yii::import('application.modules.core_models.models._base.BasePatientemailCommunications');

class PatientemailCommunications extends BasePatientemailCommunications
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * When an ARR is sent, update SF values for first, last and count of RR and ARR
     * @return boolean
     */
    public function afterSave()
    {
        if ($this->isNewRecord) {
            // we're creating an ARR

            $patientemail = Patientemail::model()->findByPk($this->patientemail_id);
            if (!$patientemail) {
                // couldn't find the patientemail, we won't be able to look up the account -- leave this process
                return parent::afterSave();
            }
            $account = Account::model()->findByPk($patientemail->account_id);
            if (!$account) {
                // couldn't find the account -- leave this process
                return parent::afterSave();
            }

            // update first automatic review request sent
            ReviewRequest::updateFirstSent($account);

            // update last automatic review request sent
            ReviewRequest::updateLastSent($account);

            // update automatic review request count
            ReviewRequest::updateCount($account);

            // update first automatic review request sent
            PatientemailCommunications::updateFirstSent($account);

            // update last automatic review request sent
            PatientemailCommunications::updateLastSent($account);

            // update automatic review request count
            PatientemailCommunications::updateCount($account);
        }
        return parent::afterSave();
    }

    /**
     * Update SalesForce: first automatic review request sent: First_AutoReviewRequest_Sent__c
     * Fired from ReviewRequest::afterSave
     * @param Account $account
     * @return boolean
     */
    public static function updateFirstSent($account)
    {

        // do we have an account?
        if (empty($account) || !$account instanceof Account) {
            // no, then leave
            return false;
        }

        // look up the first date we sent an automatic reviewrequest
        $sql = sprintf(
            "SELECT MIN(communication_log.date_added)
            FROM communication_log
            INNER JOIN patientemail_communications
                ON patientemail_communications.communication_id = communication_log.id
                AND patientemail_communications.communication_table = 'communication_log'
            INNER JOIN patientemail
                ON patientemail.id = patientemail_communications.patientemail_id
            WHERE account_id = %d;",
            $account->id
        );
        $firstDate = Yii::app()->db->createCommand($sql)->queryScalar();

        // do we have a first date, and is it now?
        if (!empty($firstDate) && date('Y-m-d H:i', strtotime($firstDate)) == date('Y-m-d H:i')) {

            // yes, then look up the organization
            $organizationObj = $account->getOwnedOrganization();
            if (!empty($organizationObj)) {

                // update SF
                $sfData = array(
                    'salesforce_id' => $organizationObj->salesforce_id,
                    'field' => 'First_AutoReviewRequest_Sent__c',
                    'value' => date('c', strtotime($firstDate))
                );
                return SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);
            }
        }
        return false;
    }

    /**
     * Update SalesForce: last automatic review request sent: Last_AutoReviewRequest_Sent__c
     * Fired from ReviewRequest::afterSave
     * @param Account $account
     * @return boolean
     */
    public static function updateLastSent($account)
    {

        $result = false;
        // do we have an account?
        if (empty($account) || !$account instanceof Account) {
            // no, then leave
            return $result;
        }

        // look up the last date we sent an automatic review request (should be now)
        $sql = sprintf(
            "SELECT MAX(communication_log.date_added)
            FROM communication_log
            INNER JOIN patientemail_communications
                ON patientemail_communications.communication_id = communication_log.id
                AND patientemail_communications.communication_table = 'communication_log'
            INNER JOIN patientemail
                ON patientemail.id = patientemail_communications.patientemail_id
            WHERE account_id = %d;",
            $account->id
        );
        $lastDate = Yii::app()->db->createCommand($sql)->queryScalar();

        // do we have a date? (we always should)
        if (!empty($lastDate)) {

            // yes, then look up the organization
            $organizationObj = $account->getOwnedOrganization();
            if (!empty($organizationObj->salesforce_id)) {
                // update SF
                $sfData = array(
                    'salesforce_id' => $organizationObj->salesforce_id,
                    'field' => 'Last_AutoReviewRequest_Sent__c',
                    'value' => date('c', strtotime($lastDate))
                );

                $result = SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);
            }
        }
        return $result;
    }

    /**
     * Update Salesforce: count of automatic review requests sent: Total_AutoReviewRequest_Sent__c
     * Fired from ReviewRequest::afterSave
     * @param Account $account
     * @return boolean
     */
    public static function updateCount($account)
    {

        // do we have an account?
        if (empty($account) || !$account instanceof Account) {
            // no, then leave
            return false;
        }

        // how many automatic review requests have we sent?
        $sql = sprintf(
            "SELECT COUNT(1)
            FROM communication_log
            INNER JOIN patientemail_communications
                ON patientemail_communications.communication_id = communication_log.id
                AND patientemail_communications.communication_table = 'communication_log'
            INNER JOIN patientemail
                ON patientemail.id = patientemail_communications.patientemail_id
            WHERE account_id = %d;",
            $account->id
        );
        (int) $count = Yii::app()->db->createCommand($sql)->queryScalar();

        // do we have a count? (we always should)
        if (!empty($count)) {

            // yes, then look up the organization
            $organizationObj = $account->getOwnedOrganization();
            if (!empty($organizationObj)) {
                // update SF
                $sfData = array(
                    'salesforce_id' => $organizationObj->salesforce_id,
                    'field' => 'Total_AutoReviewRequest_Sent__c',
                    'value' => $count
                );
                return SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);
            }
        }
        return false;
    }
}
