<?php

Yii::import('application.modules.core_models.models._base.BaseDocscore');

class Docscore extends BaseDocscore
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get the max score available
     * @param string $per V (provider) or T (practice)
     * @return mixed
     */
    public static function getMaxScore($per = 'V')
    {
        $per = $per == 'V' ? 'V' : 'T';
        $sql = "SELECT SUM(base_pts) AS `sum` FROM docscore WHERE per = '$per'";
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get practices max score available
     * @return mixed
     */
    public static function getMaxPracticeScore()
    {
        $maxScore = self::getMaxScore('T');

        // Adds default values if db is outdated
        if (!$maxScore) {
            $maxScore = 80;
        }

        return $maxScore;
    }

    /**
     * Get providers max score available
     * @return mixed
     */
    public static function getMaxProviderScore()
    {
        return self::getMaxScore('V');
    }

    /**
     * get base docScore by docScoreId
     * @param int $docScoreId
     * @return type
     */
    public static function getBaseDocScore($docScoreId = 0)
    {

        $sql = sprintf("SELECT base_pts FROM docscore WHERE id=%d;", $docScoreId);
        $docScore = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryScalar();
        if (empty($docScore)) {
            $docScore = 0;
        }
        return $docScore;
    }

    /**
     * get detailed docScore for provider or practice
     * @param string $per
     * @return array()
     */
    public static function getDocscoreRecordsBy($per = 'V')
    {
        return Docscore::model()->cache(Yii::app()->params['cache_long'])->findAll(
            'per=:per',
            array(':per' => $per)
        );
    }

    /**
     * get detailed docScore for provider / section
     * @param int $providerId
     * @param bool $useCache
     * @return array()
     */
    public static function getDetailedByProvider($providerId = 0, $useCache = true)
    {

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_long'] : 0;

        $tmpDocScore = self::getDocscoreRecordsBy('V');
        $docScore = array();

        $provider = Provider::model()->cache($cacheTime)->find(
            'id=:id',
            array(':id' => $providerId)
        );

        if (!empty($provider) && $tmpDocScore) {
            foreach ($tmpDocScore as $entity) {
                $docScore[$entity->id] = $entity->attributes;
                $docScore[$entity->id]['actual_points'] = $provider->getDocScore($entity->id);
            }
        }

        return $docScore;
    }

    /**
     * get detailed docScore for practice / section
     * @param int $practiceId
     * @return array()
     */
    public static function getDetailedByPractice($practiceId = 0)
    {
        $docPoints = self::getDocscoreRecordsBy('T');

        // Adds default values if db is outdated
        if (!$docPoints) {
            $docPoints = array(
                array('id' => 1, 'action' => "Basic Information", 'base_pts' => 20),
                array('id' => 2, 'action' => "Related Provider", 'base_pts' => 15),
                array('id' => 3, 'action' => "Office Hours", 'base_pts' => 15),
                array('id' => 4, 'action' => "Photo", 'base_pts' => 10),
                array('id' => 5, 'action' => "Insurance", 'base_pts' => 10),
                array('id' => 6, 'action' => "Specialty", 'base_pts' => 10),
            );
        }

        $practice = Practice::model()->cache(Yii::app()->params['cache_long'])->find(
            'id = :id',
            array(':id' => $practiceId)
        );
        if (!empty($practice)) {
            $score = array();
            foreach ($docPoints as $value) {
                $score[$value['id']] = $value;
                $score[$value['id']]['actual_points'] = $practice->getPracticeScore($value['id'], $practice);
            }
            return $score;
        }
        return '';
    }

}
