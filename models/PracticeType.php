<?php

Yii::import('application.modules.core_models.models._base.BasePracticeType');

class PracticeType extends BasePracticeType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * called on rendering the column for each row
     * @return string
     */
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id .
            '" rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice . '">' .
            $this->practice . '</a>');
    }

    /**
     * Return an array with the practices types
     * @return array
     */
    public static function getPracticeTypes()
    {
        $practiceTypes = PracticeType::model()->cache(Yii::app()->params['cache_short'])->findAll();

        $arrPracticesTypes = array();
        foreach ($practiceTypes as $pt) {
            $practiceTypeId = $pt->id;
            $arrPracticesTypes[$practiceTypeId] = (array) $pt->attributes;
        }
        return $arrPracticesTypes;
    }

}
