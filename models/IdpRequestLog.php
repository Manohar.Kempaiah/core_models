<?php

Yii::import('application.modules.core_models.models._base.BaseIdpRequestLog');

class IdpRequestLog extends BaseIdpRequestLog
{

    /**
     * Request log status.
     */
    const STATUS_APPROVED = 'A';
    const STATUS_PENDING = 'P';
    const STATUS_REJECTED = 'R';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Fill date added and date updated if are empty before make the validation.
     *
     * @return boolean
     */
    public function beforeValidate()
    {
        if (empty($this->date_added)) {
            $this->date_added = date('Y-m-d H:i:s');
        }

        if (empty($this->date_updated)) {
            $this->date_updated = date('Y-m-d H:i:s');
        }

        return parent::beforeValidate();
    }

}
