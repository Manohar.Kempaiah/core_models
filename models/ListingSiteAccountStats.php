<?php

Yii::import('application.modules.core_models.models._base.BaseListingSiteAccountStats');

class ListingSiteAccountStats extends BaseListingSiteAccountStats
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }


   /**
     * Get the traffic stats for all the providers and practices
     *
     * @param string $period
     * @param string $accountId
     *
     * @return array
     */
    public function getStats($period, $accountId)
    {
        $sql = "
            SELECT
                listing_site_id, account_id,
                estimated_page_views, known_page_views, reported_page_views,
                estimated_page_actions, known_page_actions, reported_page_actions
            FROM
                listing_site_account_stats
            WHERE
                date = '" . date("Y-m-d 00:00:00", strtotime($period)) . "'
                AND account_id = " . $accountId . "
            ORDER BY
                account_id, listing_site_id";

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

}
