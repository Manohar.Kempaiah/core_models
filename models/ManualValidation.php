<?php

Yii::import('application.modules.core_models.models._base.BaseManualValidation');

class ManualValidation extends BaseManualValidation
{

    public $localization;
    public $practices;

    /**
     *
     * @var string To filter by the status of owner account
     */
    public $account_status;

    /**
     *
     * @var string To filter by provider NPI
     */
    public $npi_id;

    /**
     *
     * @var string To filter by provider name
     */
    public $provider_name;

    /**
     *
     * @var string To filter by partner name
     */
    public $partner;

    /**
     * @var string Salesforce id
     */
    public $salesforce_id;

    /**
     * Value for IDP/Ingest source.
     */
    const SOURCE_INGESTION = 'I';

    /**
     * Value for Doctor source (GADM/PADM).
     */
    const SOURCE_DOCTOR = 'D';

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return array
     */
    public static function representingColumn()
    {
        return array('first_name', 'last_name');
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['address_2'] = Yii::t('app', 'Bldg./Floor/Room');
        $labels['account'] = Yii::t('app', 'Account');
        $labels['location'] = Yii::t('app', 'Location');
        $labels['provider'] = Yii::t('app', 'Provider');
        $labels['practice'] = Yii::t('app', 'Practice');
        $labels['account_status'] = Yii::t('app', 'Account Status');
        $labels['npi_id'] = Yii::t('app', 'NPI');
        $labels['partner'] = Yii::t('app', 'Partner');
        $labels['provider_name'] = Yii::t('app', 'Provider Name');

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        //this is to enable eager loading
        //this way we perform 4 queries instead of 116 (!) per page
        $criteria->with = array('location');

        $account = '';
        $providerName = false;
        if (!empty($_GET['ManualValidation'])) {
            $manualValidation = $_GET['ManualValidation'];
            $account = !empty($manualValidation['account']) ? trim($manualValidation['account']) : '';
            $providerName = !empty($manualValidation['provider_name']) ?
                trim($manualValidation['provider_name']) : false;
            $this->localization = !empty($manualValidation['localization']) ?
                trim($manualValidation['localization']) : false;
        }

        if (!empty($account)) {
            $criteria->addSearchCondition('account.first_name', "%" . $account . "%", false, 'OR', 'LIKE');
            $criteria->addSearchCondition('account.last_name', "%" . $account . "%", false, 'OR', 'LIKE');
        }

        // additional data
        $criteria->select = [];
        // manual_validation data
        $criteria->select[] = 't.*';
        // account_status
        $criteria->select[] =
            '(CASE '
                . 'WHEN t.account_id IS NULL THEN null '
                . 'WHEN NOT EXISTS(SELECT id FROM organization_account '
                    . 'WHERE organization_account.account_id = t.account_id) THEN "F" '
                . 'ELSE (SELECT organization.status FROM organization_account JOIN organization '
                    . 'ON organization.id = organization_account.organization_id '
                    . 'WHERE organization_account.account_id = t.account_id AND organization_account.connection = "O" '
                    . 'LIMIT 1) '
            . 'END) AS account_status'
        ;
        // provider npi
        $criteria->select[] = 'provider.npi_id AS npi_id';
        // partner site
        $criteria->select[] = 'partner_site.name AS partner';
        // salesforce id
        $criteria->select[] = '(SELECT organization.salesforce_id FROM organization JOIN organization_account ON ' .
            'organization_account.organization_id = organization.id AND organization_account.connection = "O" WHERE ' .
            'organization_account.account_id = t.account_id LIMIT 1) AS salesforce_id';

        // filter by organization status
        if ($this->account_status) {

            if ($this->account_status == 'F') {
                // the criteria to filter Freemium accounts is different, it should check only if the account do not
                // have any organization associated
                $criteria->join .= "LEFT JOIN organization_account ON organization_account.account_id = t.account_id " .
                    "AND organization_account.connection = 'O' ";
                $criteria->addCondition('t.account_id IS NOT NULL AND organization_account.id IS NULL');
            } else {
                // criteria to filter by status of the organization where the account is owner
                $criteria->join .= "JOIN organization_account ON organization_account.account_id = t.account_id AND "
                    . "organization_account.connection = 'O' ";
                $criteria->join .= "JOIN organization ON organization.id = organization_account.organization_id ";
                $criteria->compare('organization.status', $this->account_status);
            }

        }

        // filter by provider name
        if ($providerName) {
            $criteria->addCondition(
                '(t.provider_id IS NOT NULL AND provider.first_last LIKE :provider_name) OR ' .
                '(t.provider_id IS NULL AND CONCAT(t.first_name, " ", t.last_name) LIKE :provider_name_2)'
            );
            $criteria->params[':provider_name'] = '%' . addcslashes($providerName, '%_') . '%';
            $criteria->params[':provider_name_2'] = '%' . addcslashes($providerName, '%_') . '%';
            $this->provider_name = $providerName;
        }

        $criteria->compare('t.id', $this->id);
        $criteria->compare('t.provider_id', $this->provider_id);
        $criteria->compare('t.practice_id', $this->practice_id);
        $criteria->compare('t.first_name', $this->first_name, true);
        $criteria->compare('t.last_name', $this->last_name, true);
        $criteria->compare('t.address', $this->address, true);
        $criteria->compare('t.address_2', $this->address_2, true);
        $criteria->compare('location.city_name', $this->location, true);
        $criteria->compare('specialty.name', $this->specialty, true);
        $criteria->compare('t.email', $this->email, true);
        $criteria->compare('t.phone_number', $this->phone_number, true);
        $criteria->compare('t.practice_name', $this->practice_name, true);
        $criteria->compare('t.status', $this->status, true);
        $criteria->compare('t.status_comments', $this->status_comments, true);
        $criteria->compare('t.date_added', $this->date_added, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('t.validation_type', $this->validation_type, true);
        $criteria->compare('localization.id', $this->localization, true);
        $criteria->compare('provider.npi_id', $this->npi_id);
        $criteria->compare('t.source', $this->source);
        $criteria->compare('partner_site.name', $this->partner, true);
        $criteria->compare('admin_account_id', $this->admin_account_id);

        $criteria->join .= 'LEFT JOIN account AS account ON (t.account_id = account.id ) ';
        $criteria->join .= 'LEFT JOIN localization AS localization ON localization.id = account.localization_id ';
        $criteria->join .= 'LEFT JOIN provider ON provider.id = t.provider_id ';
        $criteria->join .= 'LEFT JOIN partner_site ON partner_site.id = account.partner_site_id ';

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100)
        ));
    }

    /**
     * After save, create the audit log
     * @return boolean
     */
    public function afterSave()
    {

        $action = 'U';
        if ($this->isNewRecord) {
            $action = 'C';
        }
        $providerName = trim($this->first_name) . ' ' . trim($this->last_name);
        $notes = $providerName . ' id(' . $this->provider_id . ')';
        $section = 'ManualValidation id(' . $this->id . ') Status: ' . $this->status;
        AuditLog::create($action, $section, 'manual_validation', $notes);

        return parent::afterSave();
    }

    /**
     * Make validations before save
     * @return boolean
     */
    public function beforeSave()
    {

        // If status Approved or Pending
        if ($this->status != 'R') {
            $this->status_rejected_reason = null;
            $this->status_comments = null;
        }

        // If is Rejected
        if ($this->status == "R") {
            // Reason is Required
            if ($this->status_rejected_reason == "") {
                $this->addError('status_rejected_reason', 'Status Rejected Reason is Required.');
                return false;
            }
            // IF Reason is 'S' or 'O', Comments are Required
            if (($this->status_rejected_reason == "S" || $this->status_rejected_reason == "O")
                && $this->status_comments == "") {
                $this->addError('status_comments', 'Status Comments is Required.');
                return false;
            }
        }

        $this->address = str_replace('Suite #, Floor #, Room #, etc (optional)', '', $this->address);
        $this->address = str_replace('Address Line 1', '', $this->address);
        $this->address = trim($this->address);

        $this->address_2 = str_replace('Suite #, Floor #, Room #, etc (optional)', '', $this->address_2);
        $this->address_2 = str_replace('Address Line 1', '', $this->address_2);
        $this->address_2 = str_replace('Address Line 2', '', $this->address_2);
        $this->address_2 = trim($this->address_2);
        $this->phone_number = preg_replace('/[^0-9]/', '', $this->phone_number);

        if ($this->isNewRecord) {
            $repeated = ManualValidation::model()->exists(
                'account_id = :account_id AND last_name = :last_name AND email = :email AND location_id = :location_id',
                array(
                    ':account_id' => $this->account_id,
                    ':last_name' => $this->last_name,
                    ':email' => $this->email,
                    ':location_id' => $this->location_id
                )
            );
            if ($repeated) {
                return false;
            }
        } else {
            if (empty($this->first_name)) {
                $this->addError('first_name', "First Name can't be empty");
                return false;
            }
            if (empty($this->last_name)) {
                $this->addError('last_name', "Last Name can't be empty");
                return false;
            }
        }

        return parent::beforeSave();
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id FROM manual_validation WHERE manual_validation.id = %d ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * get Pending providers by ManualValidation
     * @param array $accountInfo
     * @param boolean $useCache
     * @return array
     */
    public static function getPending($accountInfo = array(), $useCache = true)
    {

        if (isset($accountInfo['account_id'])) {
            $accountId = $accountInfo['account_id'];
        } else {
            return false;
        }

        // Get Owner && Managers of this account
        $arrManagers = OrganizationAccount::getManagers($accountId);

        $lstManagers = '';
        foreach ($arrManagers as $manager) {
            if ($manager['connection_type'] == 'O' || $manager['connection_type'] == 'M') {
                $lstManagers .= (int)$manager['id'] . ',';
            }
        }
        $lstManagers = rtrim($lstManagers, ',');

        // pending providers in ManualValidation
        if (!empty($lstManagers)) {
            $sql = sprintf(
                "SELECT manual_validation.*, `provider`.photo_path, '' as real_logo_path
                FROM manual_validation
                LEFT JOIN `provider` ON manual_validation.provider_id = `provider`.id
                WHERE account_id IN (%s) AND `status` = 'P';",
                $lstManagers
            );
            $response = Yii::app()->db->createCommand($sql)->queryAll();
        } else {
            $response = null;
        }

        if ($accountInfo['connection_type'] == 'S' && !empty($response)) {

            /**
             * If a pending provider AND a staff account have the same practice associated:
             *       Show the pending provider on the staff account's Provider page
             * If a pending provider does not have a practice associated:
             *       Only owner and manager accounts can see the pending provider on the Provider page
             * If a staff account does not have a practice associated:
             *       Do not show any pending providers on the staff account's Provider page
             */
            $staffPractices = $accountInfo['privileges']['practices'];
            foreach ($response as $key => $value) {
                $practiceId = !empty($value['practice_id']) ? $value['practice_id'] : 0;

                if (!in_array($practiceId, $staffPractices)) {
                    // The staff account is not allowed access this practice
                    unset($response[$key]);
                }
            }

        }

        if (!empty($response)) {
            foreach ($response as $k => $v) {
                if (!empty($v['photo_path'])) {
                    $response[$k]['real_logo_path'] = AssetsComponent::generateImgSrc(
                        $v['photo_path'],
                        'providers',
                        'L',
                        true
                    );
                }
            }
        }

        return $response;

    }

    /**
     * Override relations of parent object.
     *
     * @return array
     */
    public function relations()
    {
        $relations = parent::relations();

        $relations['provider'] = array(self::BELONGS_TO, 'Provider', 'provider_id');
        $relations['practice'] = array(self::BELONGS_TO, 'Practice', 'practice_id');
        $relations['specialty'] = array(self::BELONGS_TO, 'Specialty', 'specialty_id');
        $relations['sub_specialty'] = array(self::BELONGS_TO, 'SubSpecialty', 'sub_specialty_id');

        return $relations;
    }

    /**
     * Update ManualValidation Status
     * @param object $mvModel
     * @return bool
     */
    public static function updateStatus($mvModel)
    {

        $newProvider = Yii::app()->request->getPost('newProvider', 0);

        // get old status
        $oldStatus = $mvModel->oldRecord->status;
        $mvModel->date_updated = TimeComponent::getNow();

        $accountId = $mvModel->account_id;
        $belongsToOrganization = $accountId ? Account::belongsToOrganization($accountId, true, true) : false;

        $account = $accountId ? Account::model()->findByPk($accountId) : null;
        $providerId = $mvModel->provider_id;
        $practiceId = ($mvModel->practice_id > 0) ? $mvModel->practice_id : 0;

        $returnUrl = '';
        $return['success'] = true;
        $return['error'] = '';
        $return['url'] = '';

        if ($oldStatus == $mvModel->status) {
            // The $_POST['status] is equal to OldRecord->status
            $mvModel->addError('provider_id', 'The status was not changed.');
            $return['success'] = false;
            $return['error'] = 'The status was not changed.';
            return $return;
        }

        // set the admin account id
        $mvModel->admin_account_id = (php_sapi_name() == 'cli') ? 1 : Yii::app()->user->id;

        if ($mvModel->status == 'A'  && ($providerId > 0 || $newProvider == 1)) {

            if ($mvModel->validation_type == 'Provider') {
                // approved
                if ($newProvider == 1) {

                    // TODO: find related data in nppes registry

                    // create new provider
                    $provider = new Provider;
                    $provider->credential_id = 1;
                    $provider->first_name = $mvModel->first_name;
                    $provider->last_name = $mvModel->last_name;
                    if ($mvModel->gender != null) {
                        $provider->gender = $mvModel->gender;
                    }
                    $provider->country_id = $account ? $account->country_id : 1; // default to US
                    $provider->localization_id = $account ? $account->localization_id : 1; // default to EN
                    $provider->display_search_order = 0; // This value is recalculated in Provider->beforeSave()
                    $provider->partner_site_id = isset($account->partner_site_id) ? $account->partner_site_id : null;
                    $providerId = 0;

                    if (!$provider->save()) {
                        $mvModel->addError('provider_id', $provider->getErrors());
                        $return['success'] = false;
                        $return['error'] = $provider->getErrors();
                        return $return;
                    }

                    $providerId = $provider->id;

                    if (!empty($mvModel->sub_specialty_id)) {
                        // Add provider_specialty
                        $providerSpecialty = new ProviderSpecialty;
                        $providerSpecialty->provider_id = $providerId;
                        $providerSpecialty->sub_specialty_id = $mvModel->sub_specialty_id;
                        $providerSpecialty->primary_specialty = 1;
                        $providerSpecialty->date_added = TimeComponent::getNow();
                        $providerSpecialty->date_updated = TimeComponent::getNow();
                        $providerSpecialty->save();
                    }

                    // automatically turn on online appointment requests
                    $providerDetails = new ProviderDetails;
                    $providerDetails->provider_id = $providerId;
                    $providerDetails->schedule_mail_enabled = 1;
                    $providerDetails->save();

                    $mvModel->provider_id = $providerId;

                    // create new account<->provider
                    if ($accountId) {
                        AccountProvider::associateAccountProvider($accountId, $providerId);
                    }

                    // Sync EMR data, to allow booking
                    Yii::app()->params['emr_importation_disabled'] = null;
                    AccountEmr::importAccountEmr($accountId);

                    $providerDetails = ProviderDetails::model()->find(
                        'provider_id = :provider_id',
                        array(':provider_id' => $providerId)
                    );
                    if ($providerDetails == null) {
                        $providerDetails = new ProviderDetails;
                        $providerDetails->provider_id = $providerId;
                    }
                    // automatically turn on online appointment requests
                    $providerDetails->schedule_mail_enabled = 1;
                    $providerDetails->email = $mvModel->email;
                    if (!$providerDetails->save()) {
                        $return['success'] = false;
                        $return['error'] = $providerDetails->getErrors();
                        return $return;
                    }
                    if ($practiceId == 0) {
                        $addPractice = new Practice;
                        $addPractice->name = $mvModel->practice_name;
                        $addPractice->address = $mvModel->address;
                        $addPractice->address_2 = $mvModel->address_2;
                        $addPractice->location_id = $mvModel->location_id;
                        $addPractice->phone_number = $mvModel->phone_number;
                        $addPractice->localization_id = $account ? $account->localization_id : 1; // default to EN
                        $addPractice->country_id = $account ? $account->country_id : 1; // default to US
                        $addPractice->friendly_url = Practice::model()->getFriendlyUrl($mvModel->practice_name);
                        if ($addPractice->save()) {
                            $practiceId = $addPractice->id;
                        }
                    }

                    if ($practiceId > 0) {
                        // Add provider<->practice relation
                        $providerPractice = new ProviderPractice;
                        $providerPractice->provider_id = $provider->id;
                        $providerPractice->practice_id = $practiceId;
                        $providerPractice->primary_location = 1;
                        $providerPractice->save();

                        if ($accountId) {
                            AccountPractice::associateAccountPractice($accountId, $practiceId);
                        }
                    }

                } else {
                    if ($accountId) {
                        AccountProvider::associateAccountProvider($accountId, $providerId);
                    }

                    // find the provider
                    $provider = Provider::model()->find(
                        'id = :provider_id',
                        array(':provider_id' => $providerId)
                    );

                    // set provider names
                    $provider->first_name = $mvModel->first_name;
                    $provider->last_name = $mvModel->last_name;
                    $provider->partner_site_id = isset($account->partner_site_id) ? $account->partner_site_id : null;

                    if ($mvModel->gender != null && $provider != null) {
                        // update gender for this provider
                        $provider->gender = $mvModel->gender;
                    }

                    $provider->save();

                    // Add provider_specialty if needed
                    if (!empty($mvModel->sub_specialty_id)) {

                        $providerSpecialty = ProviderSpecialty::model()->find(
                            'provider_id=:provider_id AND sub_specialty_id=:sub_specialty_id',
                            array(':provider_id' => $providerId, ':sub_specialty_id' => $mvModel->sub_specialty_id)
                        );
                        $primarySpecialty = 1;

                        if (empty($providerSpecialty)) {
                            // Have any other specialty setting?
                            $haveOtherSpecialty = ProviderSpecialty::model()->find(
                                'provider_id=:provider_id',
                                array(':provider_id' => $providerId)
                            );
                            if (is_array($haveOtherSpecialty)) {
                                $primarySpecialty = count($haveOtherSpecialty) + 1;
                            }

                            $providerSpecialty = new ProviderSpecialty;
                            $providerSpecialty->provider_id = $providerId;
                            $providerSpecialty->sub_specialty_id = $mvModel->sub_specialty_id;
                            $providerSpecialty->date_added = TimeComponent::getNow();
                        }

                        $providerSpecialty->primary_specialty = $primarySpecialty;
                        $providerSpecialty->date_updated = TimeComponent::getNow();
                        $providerSpecialty->save();

                    }

                    $providerDetails = ProviderDetails::model()->find(
                        'provider_id = :provider_id',
                        array(':provider_id' => $providerId)
                    );
                    if ($providerDetails == null) {
                        $providerDetails = new ProviderDetails;
                        $providerDetails->provider_id = $providerId;
                    }
                    // automatically turn on online appointment requests
                    $providerDetails->schedule_mail_enabled = 1;
                    $providerDetails->email = $mvModel->email;
                    $providerDetails->save();

                    if ($practiceId > 0) {

                        $providerPracticeExists = ProviderPractice::model()->exists(
                            'provider_id=:provider_id AND practice_id=:practice_id',
                            array(
                                ':provider_id' => $providerId,
                                ':practice_id' => $practiceId
                            )
                        );

                        if (!$providerPracticeExists) {
                            // link if provider-practice relationship doesn't exist already
                            $providerPractice = new ProviderPractice;
                            $providerPractice->provider_id = $providerId;
                            $providerPractice->practice_id = $practiceId;
                            $providerPractice->primary_location = 1;
                            $providerPractice->save();
                        }

                        if ($accountId) {
                            AccountPractice::associateAccountPractice($accountId, $practiceId);
                        }

                    } else {
                        $addPractice = new Practice;
                        $addPractice->name = $mvModel->practice_name;
                        $addPractice->address = $mvModel->address;
                        $addPractice->address_2 = $mvModel->address_2;
                        $addPractice->location_id = $mvModel->location_id;
                        $addPractice->phone_number = $mvModel->phone_number;
                        $addPractice->localization_id = $account ? $account->localization_id : 1; // default to EN
                        $addPractice->country_id = $account ? $account->country_id : 1; // default to US
                        $addPractice->friendly_url = Practice::model()->getFriendlyUrl($mvModel->practice_name);
                        if ($addPractice->save()) {
                            $practiceId = $addPractice->id;

                            $providerPractice = new ProviderPractice;
                            $providerPractice->provider_id = $providerId;
                            $providerPractice->practice_id = $practiceId;
                            $providerPractice->primary_location = 1;
                            $providerPractice->save();

                            if ($accountId) {
                                AccountPractice::associateAccountPractice($accountId, $practiceId);
                            }
                        }
                    }
                }
            }

            $mvModel->save();
            if ($mvModel->email != '' && !$belongsToOrganization) {
                // this email is sent to the account, so if there isn't any account, just skip it
                if ($accountId) {
                    MailComponent::manualValidationApproved($mvModel->id, $providerId, $accountId);
                }

                $returnUrl = Yii::app()->createUrl(
                    'account/admin',
                    array('Account[email]' => $mvModel->email)
                );
            }

            if ($newProvider == 1) {
                $returnUrl = Yii::app()->createUrl('provider/update', array('id' => $providerId));
            }


        } elseif (($oldStatus == 'P' || $oldStatus == 'R') && $mvModel->status == 'A') {
            $mvModel->addError('provider_id', 'You must select a Provider or indicate to create a new one.');
            $return['success'] = false;
            $return['error'] = 'You must select a Provider or indicate to create a new one.';
            return $return;


        } elseif ($oldStatus == 'P' && $mvModel->status == 'R') {
            if (empty($mvModel->status_rejected_reason) || empty($mvModel->status_comments)) {
                $mvModel->addError('provider_id', 'You must select a Rejected Reason.');
                $return['success'] = false;
                $return['error'] = 'You must select a Rejected Reason.';
                return $return;
            }
            $mvModel->save();
            if ($mvModel->email != '' && !$belongsToOrganization && $accountId) {
                MailComponent::manualValidationRejected($mvModel->id, $providerId, $accountId);
            }
            $returnUrl = Yii::app()->createUrl(
                'manualValidation/admin',
                array('ManualValidation[status]' => 'P')
            );

        } else {
            $mvModel->save();
        }
        // Sync EMR data
        if ($accountId) {
            SqsComponent::sendMessage('importAccountEmr', $accountId);
        }

        // get the npi and notify the ingestion
        $npi = isset($provider) ? $provider->npi_id : null;
        static::notifyIngestion($mvModel, $npi);

        $return['success'] = true;
        $return['url'] = $returnUrl;
        return $return;
    }

    /**
     * Notify the changed status to the ingestion process if applies.
     *
     * @param ManualValidation $mvModel
     * @param string $npi
     * @return void
     */
    protected static function notifyIngestion($mvModel, $npi)
    {
        $ingestions = static::getIngestionRecords($mvModel->oldRecord->id);

        // no ingestion record found
        if (empty($ingestions)) {
            return;
        }

        // loop by each ingestion to update the status and resend
        foreach ($ingestions as $i) {
            // update the ingestion log according to the new manual validation status
            static::updateIngestion($mvModel, $i);

            // re-send the ingestion log to finish the paused ingestion, if mv was approved
            static::resendIngestion($mvModel, $i, $npi);
        }
    }

    /**
     * Get the ingestion log record of the given manual validation id.
     * Returns null if nothing found.
     *
     * @param int $manualValidationId
     * @return array
     */
    protected static function getIngestionRecords($manualValidationId)
    {
        // define a default criteria for both tables (they have the same structure)
        $criteria = new CDbCriteria();
        $criteria->condition = 'manual_validation_id = :manual_validation_id AND status = :status';
        $criteria->params[':manual_validation_id'] = $manualValidationId;

        // check if the manual validation is associated to an ingestion (IDP 2020)
        $criteria->params[':status'] = 'R';
        $ingestion = IdpRequestLog::model()->findAll($criteria);

        if ($ingestion) {
            return $ingestion;
        }

        // not found? check if is an old ingestion
        $criteria->params[':status'] = 'P';
        $ingestion = IngestionLog::model()->findAll($criteria);

        if ($ingestion) {
            return $ingestion;
        }

        // nothing found
        return [];
    }

    /**
     * Update the ingestion log record.
     *
     * @param ManualValidation $mvModel
     * @param mixed $ingestion
     * @return void
     */
    protected static function updateIngestion($mvModel, $ingestion)
    {
        // approved action
        if ($mvModel->status == 'A') {
            // only applied to the old ingestion
            if ($ingestion instanceof IngestionLog) {
                // set the ingestion log as approved with a message indicating the manual validation was approved
                $ingestion->addErrorMessage('Provider', [['npi_id' => 'Manual validation approved']], 'A');
            } elseif ($ingestion instanceof IdpRequestLog) {
                $ingestion->status = 'A';
                $ingestion->message = null;
                $ingestion->internal_message = null;
                $ingestion->save();
            }

            // nothing more to do
            return;
        }

        // rejected action
        if ($mvModel->status == 'R') {
            // rejected reasons according to DB enum
            $rejectedReasons = [
                'D' => 'Duplicated',
                'I' => 'Inappropriate language or gibberish',
                'N' => 'Invalid NPI',
                'M' => 'NPI/Name mismatch',
                'P' => 'PHI',
                'S' => 'Suspected fraud',
                'Z' => 'Suspended NPI',
                'T' => 'Testing',
                'O' => 'Other'
            ];

            // create the reason string
            $reason = isset($rejectedReasons[$mvModel->status_rejected_reason]) ?
                $rejectedReasons[$mvModel->status_rejected_reason] : 'Other';

            $reasonText = 'Manual Validation rejected. Reason: ' . $reason;

            if ($mvModel->status_comments) {
                $reasonText .= ' (' . $mvModel->status_comments . ')';
            }

            // update the corresponding ingestion log record
            if ($ingestion instanceof IdpRequestLog) {
                // duplicate the request log record with additional manual validation information
                $ingestion->message = json_encode([$reasonText]);
                $ingestion->save();
            } elseif ($ingestion instanceof IngestionLog) {
                // old IDP
                $ingestion->status = 'R';
                $ingestion->addErrorMessage('Provider', [['npi_id' => $reasonText]]);
                $ingestion->save();
            }
        }
    }

    /**
     *
     * @param ManualValidation $mvModel
     * @param mixed $ingestion
     * @param string $npi
     * @return void
     */
    protected static function resendIngestion($mvModel, $ingestion, $npi)
    {
        // re-send the ingestion only if was approved
        if ($mvModel->status != 'A') {
            return;
        }

        // decode the request
        $request = json_decode($ingestion->request, true);

        // can't be processed
        if (empty($request)) {
            return;
        }

        if ($ingestion instanceof IngestionLog) {
            return static::resendIngestionLog($request);
        } elseif ($ingestion instanceof IdpRequestLog) {
            static::resendIdpRequestLog($ingestion, $request, $npi);
        }
    }

    /**
     * Re-send the ingestion log (old IDP).
     *
     * @param array $request
     * @return void
     */
    protected static function resendIngestionLog($request)
    {
        // the partner site is a required field for this action
        if (empty($request['partner_site_id'])) {
            return;
        }

        // get credentials
        $sql = 'SELECT client_id, client_secret FROM oauth2_clients WHERE partner_site_id = :id';
        $auth = Yii::app()->db->createCommand($sql)->queryRow(
            true,
            [':id' => $request['partner_site_id']]
        );

        // can't continue without credentials
        if (empty($auth)) {
            return;
        }

        // send the record to the ingestion process
        ExternalDataImporterComponent::submitRecord(
            $request,
            $auth['client_id'],
            $auth['client_secret']
        );
    }

    /**
     * Re-send the ingestion log (new IDP).
     *
     * @param IdpRequestLog $ingestion
     * @param array $request
     * @param string $npi
     * @return void
     */
    protected static function resendIdpRequestLog($ingestion, $request, $npi)
    {
        // add additional data to the request
        $request['data']['manual_validation_npi'] = $npi;

        // create a new guid
        $request['guid'] = StringComponent::generateGUID();

        // create the feed and save it
        $idpFeed = new IdpFeed();
        $idpFeed->idp_provision_id = $ingestion->idp_provision_id;
        $idpFeed->verb = $ingestion->verb;
        $idpFeed->request = json_encode($request);
        $idpFeed->save();
    }

}
