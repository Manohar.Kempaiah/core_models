<?php

Yii::import('application.modules.core_models.models._base.BaseOrganizationPartnerGraphics');

// autoload vendors class
Common::autoloadVendors();

class OrganizationPartnerGraphics extends BaseOrganizationPartnerGraphics
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * get partner graphics request by organization_id
     * @param int $organizationId
     * @return array
     */
    public static function getByOrganizationId($organizationId = 0)
    {
        if ($organizationId == 0) {
            return false;
        }

        $sql = sprintf(
            "SELECT opg.id,organization_id, partner_id, phab_task_id AS task_id,
            '' AS task_url, assigned_to,
            partner.name as friendly_name,
            DATE_FORMAT(date_added, '%%m/%%d/%%Y') AS date_added, partner.display_name AS partner_name
            FROM organization_partner_graphics AS opg
            LEFT JOIN partner ON partner.id = opg.partner_id
            WHERE organization_id = %d ",
            $organizationId
        );
        $tmp = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
        $response = [];

        $requiredPartner = array('facebook', 'google', 'healthgrades');

        foreach ($requiredPartner as $partner) {
            $exists = false;
            if (!empty($tmp)) {
                foreach ($tmp as $value) {
                    $partnerKey = StringComponent::slugify($value['friendly_name']);
                    if ($partnerKey == $partner) {
                        if (!empty($value['task_id'])) {
                            if (substr($value['task_id'], 0, 1) == 'T') {
                                $value['task_url'] = Yii::app()->params->servers['dr_dev_hn'] . '/' . $value['task_id'];
                            } else {
                                $value['task_url'] = Yii::app()->params->servers['azure_devops_url']
                                    . '/Core/_workitems/edit/' . $value['task_id'];
                            }
                        }
                        $response[$partnerKey] = $value;
                        $exists = true;
                        break;
                    }
                }
            }
            if (!$exists) {
                $value = array();
                $value['task_id'] = '';
                $value['task_url'] = '';
                $value['date_added'] = date("m/d/Y");
                $response[$partner] = $value;
            }
        }

        return $response;
    }

    /**
     * save new gaphics request
     * @param int $organizationId
     * @param string $partner google | facebook | healthgrades
     */
    public static function saveNewGraphics($organizationId = 0, $partner = '')
    {

        switch ($partner) {
            case 'google':
                $partnerId = 53;
                break;
            case 'facebook':
                $partnerId = 52;
                break;
            case 'healthgrades':
                $partnerId = 44;
                break;
        }

        $organization = Organization::model()->findByPK($organizationId);
        $masterTaskId = null;
        if (!empty($organization->onboarding_task_id)) {
            $masterTaskId = $organization->onboarding_task_id;
        }

        // get Owner Account
        $orgAccount = OrganizationAccount::model()->find(
            'organization_id = :organization_id AND connection = :connection',
            array(':organization_id' => $organizationId, ':connection' => "O"));
        $accountId = $accountRep = '';
        if (!empty($orgAccount->account_id)) {
            $accountId = $orgAccount->account_id;
        }

        if (!empty($organization->salesforce_id)) {
            $sfDetails = Account::getSalesforceDetails($organization->salesforce_id);
            $accountRep = !empty($sfDetails['account_rep']) ? '**'.trim($sfDetails['account_rep']).'**' : '';
        }

        $arrPractices = AccountPractice::getAccountPractices($accountId);
        $tblPractices = '';
        if (!empty($arrPractices)) {
            $tblPractices = '|**Practice ID**|**Practice Name**|'. "\n";
            foreach ($arrPractices as $eachPractice) {
                $tblPractices .= '|' . $eachPractice['id'] . '|' . $eachPractice['name'] . '|'. "\n";
            }
        }

        $description = "Organization: (" . $organizationId . ") **" . $organization->organization_name . "** \n";
        $description .= "Owner Account ID: **" . $accountId . "** \n";
        $description .= "Account Rep: " . $accountRep . " \n \n";
        $description .= $tblPractices . " \n \n";
        $description .= "Request graphics for **" . strtoupper($partner) . "**\n \n";
        $description .= "**remember to mark as 'Resolved'**";

        // Get devops manager vendor
        $vendorSystem = 'phabricator';
        $vendor = DevOps::get()->vendor($vendorSystem);

        // Create Task
        $ticket = DevOpsTicket::build([
            'title' => 'CREATE PARTNER GRAPHICS -> ' . strtoupper($partner),
            'body' => $description,
            'body_format' => 'markdown',
            'salesforce_id' => $organization->salesforce_id,
            'assignedTo' => 'sebastianb',
            'priority' => 25,
            'tags' => ['Client Services'],
            'extra_data' => ["auxiliary" => ["type" => 'PARTNER_GRAPHICS']]
        ]);
        $result = $vendor->createTicket($ticket);

        if ($result->success()) {
            $taskUrl = DevOps::get()->vendor($vendorSystem)->getTicketUrl($ticket);
            // Saved ok
            $phabTaskId = $result->data()->getId();

            if (!empty($masterTaskId)) {
                // Add to masterTask
                $newText = ' - ' . $phabTaskId . ' - **PARTNER GRAPHICS** -> ' . $partner . "\n";
                $vendor->addTaskToOnboardingTicket($masterTaskId, $newText, 'markdown');
            }

            // Save Organization Partner Graphics
            $opn = new OrganizationPartnerGraphics();
            $opn->organization_id = $organizationId;
            $opn->partner_id = $partnerId;
            $opn->phab_task_id = $phabTaskId;
            $opn->assigned_to = 'sebastianb';
            $opn->date_updated = date("Y-m-d H:i:s");
            if ($opn->save()) {
                return DataTransport::build([
                    'success' => true,
                    'data' => [
                        'task_id' => trim($phabTaskId),
                        'task_url' => $taskUrl
                    ]
                ]);
            }

        }
        return $result;

    }

}
