<?php

Yii::import('application.modules.core_models.models._base.BaseTypeGroup');

class TypeGroup extends BaseTypeGroup
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
