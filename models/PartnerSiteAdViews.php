<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteAdViews');

class PartnerSiteAdViews extends BasePartnerSiteAdViews
{

    /*
     * Model
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    /**
     * set as viewed
     * @param integer $partnerSiteId
     * @param integer $partnerSiteAdId
     * @param integer $providerId
     * @param integer $patientId
     * @return json
     */
    public static function setAsViewed($partnerSiteId, $partnerSiteAdId, $providerId, $patientId)
    {

        $psav = new PartnerSiteAdViews();

        $psav->partner_site_id = $partnerSiteId;
        $psav->partner_site_ad_id = $partnerSiteAdId;
        $psav->provider_id = $providerId;
        $psav->patient_id = $patientId;
        $psav->date_added = TimeComponent::getNow();
        $psav->save();
    }
}
