<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteEntityMatch');

class PartnerSiteEntityMatch extends BasePartnerSiteEntityMatch
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function beforeValidate()
    {
        if (empty($this->date_added)) {
            $this->date_added = date('Y-m-d H:i:s');
        }

        return parent::beforeValidate();
    }

    /**
     * Set valid create/edit dates
     * @return boolean
     */
    public function beforeSave()
    {
        // Dates must be set
        $this->date_updated = date("Y-m-d H:i:s");
        if ($this->isNewRecord) {
            $this->date_added = $this->date_updated;
        }

        return parent::beforeSave();
    }

    /**
     * Get the list of reviews which failed to be sent to the partner
     *
     * @param PartnerSite $partnerSite
     * @return mixed
     */
    public function getFailedReviews(PartnerSite $partnerSite)
    {
        $sql = "SELECT
                partner_site_entity_match.id,
                partner_site_entity_match.partner_site_id,
                partner_site_entity_match.match_column_name,
                partner_site_entity_match.match_column_value,
                partner_site_entity_match.internal_entity_type,
                partner_site_entity_match.internal_column_name,
                partner_site_entity_match.internal_column_value,
                partner_site_entity_match.retrieved_entity_type,
                partner_site_entity_match.retrieved_column_name,
                partner_site_entity_match.retrieved_column_value,
                partner_site_entity_match.date_added,
                partner_site_entity_match.date_updated
            FROM
                partner_site_entity_match
            WHERE
              partner_site_entity_match.partner_site_id = :partner_site_id
              AND partner_site_entity_match.retrieved_entity_type = 'ReviewStatus'
              AND partner_site_entity_match.retrieved_column_name = 'ResponseID'
              AND partner_site_entity_match.retrieved_column_value IS NULL
              AND partner_site_entity_match.internal_entity_type = 'ProviderRating'
              AND partner_site_entity_match.internal_column_name = 'id'";

        $result = Yii::app()->db->createCommand($sql)
            ->bindValue(':partner_site_id', $partnerSite->id)
            ->query();

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * Get a rating's sent to partner status.
     *
     * @param PartnerSite $partnerSite
     * @param ProviderRating $rating
     * @return PartnerSiteEntityMatch
     */
    public function getReviewStatus(PartnerSite $partnerSite, ProviderRating $rating)
    {
        return PartnerSiteEntityMatch::model()->find(
            "partner_site_id = :partner_site_id
                AND retrieved_entity_type = 'ReviewStatus'
                AND internal_entity_type = 'ProviderRating'
                AND internal_column_name = 'id'
                AND internal_column_value = :provider_rating_id",
            array(
                ':partner_site_id' => $partnerSite->id,
                ':provider_rating_id' => $rating->id
            )
        );
    }

    /**
     * Get a survey's sent to partner status.
     *
     * @param PartnerSite $partnerSite
     * @param ProviderRatingSurvey $survey
     * @return PartnerSiteEntityMatch
     */
    public function getSurveyStatus(PartnerSite $partnerSite, ProviderRatingSurvey $survey)
    {
        return PartnerSiteEntityMatch::model()->find(
            "partner_site_id = :partner_site_id
                AND retrieved_entity_type = 'ReviewStatus'
                AND internal_entity_type = 'ProviderRatingSurvey'
                AND internal_column_name = 'id'
                AND internal_column_value = :provider_rating_id",
            array(
                ':partner_site_id' => $partnerSite->id,
                ':provider_rating_id' => $survey->id
            )
        );
    }

    /**
     * Convenience method to check if a Review was sent to the partner.
     *
     * @return bool
     */
    public function reviewSent()
    {
        $status = false;

        if (
            $this->internal_entity_type === 'ProviderRating'
            && $this->retrieved_entity_type === 'ReviewStatus'
            && $this->retrieved_column_name === 'ResponseID'
        ) {
            $status = true;
        }

        return $status;
    }

    /**
     * Creates or edits two records inside this table.
     * One stores a log of the response (alwas create),
     * and the other stores the current status (it's created
     * or updated here)
     *
     * @param PartnerSite $partnerSite
     * @param ProviderRating $rating
     * @param string $responseId
     * @return boolean
     * @throws Exception
     */
    public function createReviewSentLogs(PartnerSite $partnerSite, ProviderRating $rating, $responseId = null)
    {
        // Update the unique status record
        $statusRecord = PartnerSiteEntityMatch::getReviewStatus($partnerSite, $rating);
        if (!$statusRecord) {
            $statusRecord = new PartnerSiteEntityMatch();
            $statusRecord->setAttribute('partner_site_id', $partnerSite->id);
            $statusRecord->setAttribute('match_column_name', '');
            $statusRecord->setAttribute('match_column_value', '');
            $statusRecord->setAttribute('internal_entity_type', 'ProviderRating');
            $statusRecord->setAttribute('internal_column_name', 'id');
            $statusRecord->setAttribute('internal_column_value', $rating->id);
            $statusRecord->setAttribute('retrieved_entity_type', 'ReviewStatus');
            $statusRecord->setAttribute('retrieved_column_name', 'ResponseID');
        }
        $statusRecord->setAttribute('retrieved_column_value', $responseId);

        $status1 = $statusRecord->save(false);

        // Create the log
        $entity = new PartnerSiteEntityMatch();
        $entity->setAttribute('partner_site_id', $partnerSite->id);
        $entity->setAttribute('match_column_name', '');
        $entity->setAttribute('match_column_value', '');
        $entity->setAttribute('internal_entity_type', 'ProviderRating');
        $entity->setAttribute('internal_column_name', 'id');
        $entity->setAttribute('internal_column_value', $rating->id);
        $entity->setAttribute('retrieved_entity_type', 'ReviewLog');
        $entity->setAttribute('retrieved_column_name', 'ResponseID');
        $entity->setAttribute('retrieved_column_value', $responseId);

        $status2 = $entity->save(false);

        return ($status1 && $status2);
    }

    /**
     * Creates or edits two records inside this table.
     * One stores a log of the response (alwas create),
     * and the other stores the current status (it's created
     * or updated here)
     *
     * @param PartnerSite $partnerSite
     * @param ProviderRatingSurvey $survey
     * @param string $responseId
     * @return boolean
     * @throws Exception
     */
    public function createSurveySentLogs(PartnerSite $partnerSite, ProviderRatingSurvey $survey, $responseId = null)
    {
        // Update the unique status record
        $statusRecord = PartnerSiteEntityMatch::getSurveyStatus($partnerSite, $survey);
        if (!$statusRecord) {
            $statusRecord = new PartnerSiteEntityMatch();
            $statusRecord->setAttribute('partner_site_id', $partnerSite->id);
            $statusRecord->setAttribute('match_column_name', '');
            $statusRecord->setAttribute('match_column_value', '');
            $statusRecord->setAttribute('internal_entity_type', 'ProviderRatingSurvey');
            $statusRecord->setAttribute('internal_column_name', 'id');
            $statusRecord->setAttribute('internal_column_value', $survey->id);
            $statusRecord->setAttribute('retrieved_entity_type', 'ReviewStatus');
            $statusRecord->setAttribute('retrieved_column_name', 'ResponseID');
        }
        $statusRecord->setAttribute('retrieved_column_value', $responseId);

        $status1 = $statusRecord->save(false);

        // Create the log
        $entity = new PartnerSiteEntityMatch();
        $entity->setAttribute('partner_site_id', $partnerSite->id);
        $entity->setAttribute('match_column_name', '');
        $entity->setAttribute('match_column_value', '');
        $entity->setAttribute('internal_entity_type', 'ProviderRatingSurvey');
        $entity->setAttribute('internal_column_name', 'id');
        $entity->setAttribute('internal_column_value', $survey->id);
        $entity->setAttribute('retrieved_entity_type', 'ReviewLog');
        $entity->setAttribute('retrieved_column_name', 'ResponseID');
        $entity->setAttribute('retrieved_column_value', $responseId);

        $status2 = $entity->save(false);

        return ($status1 && $status2);
    }

    /**
     * Get partnerValue according to the specified arguments.
     *
     * If $internalColumnValue is omitted this will returns an array with all found rows, otherwise will returns a
     * string with the specific partner value found.
     *
     * @param int $partnerSiteId
     * @param string $matchColumnName
     * @param string $retrievedColumnName
     * @param string $internalColumnValue
     * @return mixed
     */
    public static function getPartnerValue(
        $partnerSiteId,
        $matchColumnName,
        $retrievedColumnName,
        $internalColumnValue = null
    ) {
        // create standard query with arguments from the function
        $sql = sprintf(
            "SELECT DISTINCT retrieved_column_value AS partnerValue
            FROM partner_site_entity_match
            WHERE partner_site_id = :partner_site_id
            AND match_column_name = :match_column_name
            AND retrieved_column_name = :retrieved_column_name
            %s
            ORDER BY retrieved_column_value ASC;",
            ($internalColumnValue === null ? '' : 'AND internal_column_value = :internal_column_value')
        );
        $query = Yii::app()->db->createCommand($sql);

        $params = [
            ':partner_site_id' => $partnerSiteId,
            ':match_column_name' => $matchColumnName,
            ':retrieved_column_name' => $retrievedColumnName
        ];

        // return all found rows if no internal column value was specified
        if ($internalColumnValue === null) {
            return $query->queryColumn($params);
        }

        // return only one specific value
        $params[':internal_column_value'] = $internalColumnValue;
        return $query->queryScalar($params);
    }

    /**
     * Find a PartnerSiteEntityMatch record by specialty and listing
     *
     * @param int $specialtyId
     * @param int $listingSiteId
     * @return PartnerSiteEntityMatch
     */
    public function findPartnerSpecialty($specialtyId, $listingSiteId, $type = 'sub_specialty')
    {
        $criteria = new CDbCriteria;
        $criteria->with = [
            'partnerSite'
        ];

        // Eager loading
        $criteria->together = true;

        $criteria->compare('t.internal_entity_type', $type);
        $criteria->compare('t.internal_column_value', $specialtyId);
        $criteria->compare('partnerSite.listing_site_id', $listingSiteId);

        return $this->find($criteria);
    }

    public function findColumnValueByPartnerSiteIdAndEntityType($partnerSiteId, $entityType)
    {
        $criteria = new CDbCriteria;
        $criteria->distinct= true;
        $criteria->select = 'retrieved_column_value';
        $criteria->compare('t.partner_site_id', $partnerSiteId);
        $criteria->compare('t.retrieved_entity_type', $entityType);

        return $this->findAll($criteria);
    }
}
