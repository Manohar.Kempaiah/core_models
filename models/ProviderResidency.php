<?php

Yii::import('application.modules.core_models.models._base.BaseProviderResidency');

class ProviderResidency extends BaseProviderResidency
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    //called on rendering the column for each row
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id . '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">' . $this->provider . '</a>');
    }

    /**
     * Make year null if empty
     * @return boolean
     */
    public function beforeSave()
    {
        if ($this->year_completed == 0 || $this->year_completed == '') {
            $this->year_completed = null;
        }
        return parent::beforeSave();
    }

    /**
     * Get details about a provider's residency
     * @param int $providerId
     * @return array
     */
    public static function getDetails($providerId, $cache = true)
    {

        $sql = sprintf(
            "SELECT pr.id, pr.provider_id, pr.residency_type_id, pr.hospital_name,
            pr.area_of_focus, pr.year_completed,
            residency_type.name AS residency_type
            FROM provider_residency AS pr
            INNER JOIN residency_type
            ON pr.residency_type_id = residency_type.id
            WHERE pr.provider_id = %d;",
            $providerId
        );

        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Save Data
     * @param string $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        foreach ($postData as $data) {
            if (isset($data['backend_action']) && $data['backend_action'] == 'delete' && $data['id'] > 0) {
                $model = ProviderResidency::model()->findByPk($data['id']);
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_RESIDENCY";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }
                    $auditSection = 'Provider Residency id #' . $providerId .' (' . $model->hospital_name . ')';
                    AuditLog::create('D', $auditSection, 'provider_residency', null, false);
                }
            } else {

                // We define $model as null to be able to check conditions correctly
                $model = null;

                if ($data['id'] > 0) {
                    $model = ProviderResidency::model()->findByPk($data['id']);
                    $auditAction = 'U';
                }
                if (empty($model)) {
                    $auditAction = 'C';
                    $model = new ProviderResidency;
                }
                $model->attributes = $data;
                $model->provider_id = $providerId;

                if ($model->year_completed == 'yyyy') {
                    $model->year_completed = null;
                }
                if (!$model->save()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_RESIDENCY";
                    $results['data'] = $model->getErrors();
                    return $results;
                } else {
                    if ($auditAction == 'C') {
                        // return the new item
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                }

                $auditSection = 'ProviderResidency id #' . $providerId .' (' . $model->hospital_name . ')';
                AuditLog::create($auditAction, $auditSection, 'provider_residency', null, false);
            }

        }
        return $results;
    }

}
