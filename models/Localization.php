<?php

Yii::import('application.modules.core_models.models._base.BaseLocalization');

class Localization extends BaseLocalization
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Return available languages
     * @return arr
     */
    public static function getAvailableLanguages()
    {
        $sql = "SELECT * FROM localization ORDER BY id ASC";
        $xTemp = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
        $arrLanguages = array();
        if (!empty($xTemp)) {
            foreach ($xTemp as $value) {
                $arrLanguages[$value['code']] = $value['name'];
            }
        } else {
            $arrLanguages['en'] = 'English';
        }
        return $arrLanguages;
    }

    /**
     * Return available languages
     * @param str $languageCode
     * @return arr
     */
    public static function getLocalizationId($languageCode = 'en')
    {
        $arrLocalization = Localization::model()->cache(Yii::app()->params['cache_long'])->
                find("code = :code", array(':code' => $languageCode));
        return $arrLocalization->id;
    }

    /**
     * Return localization Code
     * @param str $localizationId
     * @return arr
     */
    public static function getLocalizationCode($localizationId = 1)
    {
        $arrLocalization = Localization::model()->cache(Yii::app()->params['cache_long'])->
                find("id = :id", array(':id' => $localizationId));
        return !empty($arrLocalization->code) ? $arrLocalization->code : 'en';
    }

    /**
     * Return appointment array translation
     * @return arr
     */
    public static function getAppointmentTranslation()
    {
        $dataReturn = '';
        $availableLanguages = Localization::model()->getAvailableLanguages();

        $dataReturn.= '// define available languages ' . PHP_EOL;
        $dataReturn.= 'availableLanguages = new Array();' . PHP_EOL;
        foreach ($availableLanguages as $key => $value) {
            $dataReturn.= 'availableLanguages["' . $key . '"] = "' . $value . '";' . PHP_EOL;
        }

        $dataReturn.= 'arrTranslation = new Array();' . PHP_EOL;
        foreach ($availableLanguages as $key => $value) {
            $dataReturn.= 'arrTranslation["' . $key . '"] = new Array();' . PHP_EOL;
        }

        foreach ($availableLanguages as $lang => $value) {
            // testing load all message file
            $dataMessage = Yii::app()->getMessages();
            $basePath = $dataMessage->basePath;
            $fileToInclude = $basePath . '/' . $lang . '/appointment.php';
            $arrTranslations[$lang] = include($fileToInclude);
        }

        foreach ($arrTranslations as $lang => $value) {
            foreach ($value as $key => $data) {
                // clear translation data and retrieve htmlentities
                $dataReturn.= 'arrTranslation["' . $lang . '"]["' . $key . '"] = "' .
                    htmlentities(html_entity_decode($data)) . '";' . PHP_EOL;
            }
        }

        return $dataReturn;
    }

    /**
     * Return documents array translation
     * @param str $defaultLang
     * @return arr
     */
    public static function documentsTranslation($defaultLang = 'en')
    {
        $dataReturn = '';
        $availableLanguages = Localization::model()->getAvailableLanguages();

        $dataReturn.= '// define available languages ' . PHP_EOL;
        $dataReturn.= 'availableLanguages = new Array();' . PHP_EOL;
        foreach ($availableLanguages as $key => $value) {
            $dataReturn.= 'availableLanguages["' . $key . '"] = "' . $value . '";' . PHP_EOL;
        }

        $dataReturn.= 'arrTranslation = new Array();' . PHP_EOL;
        foreach ($availableLanguages as $key => $value) {
            $dataReturn.= 'arrTranslation["' . $key . '"] = new Array();' . PHP_EOL;
        }

        foreach ($availableLanguages as $lang => $value) {
            // testing load all message file
            $dataMessage = Yii::app()->getMessages();
            $basePath = $dataMessage->basePath;
            $fileToInclude = $basePath . '/' . $lang . '/documents.php';
            $arrTranslations[$lang] = include($fileToInclude);
        }

        foreach ($arrTranslations as $lang => $value) {
            foreach ($value as $key => $data) {
                // clear translation data and retrieve htmlentities
                $dataReturn.= 'arrTranslation["' . $lang . '"]["' . $key . '"] = "' .
                    htmlentities(html_entity_decode($data)) . '";' . PHP_EOL;
            }
        }

        return $dataReturn;
    }

    /**
     * Return reviews array translation
     * @return arr
     */
    public static function getReviewsTranslation()
    {

        $dataReturn = Yii::app()->cache->get("ReviewsTranslation" . date("Ymd"));
        if (empty($dataReturn)) {
            $availableLanguages = Localization::model()->getAvailableLanguages();

            $dataReturn.= '// define available languages ' . PHP_EOL;
            $dataReturn.= 'availableLanguages = new Array();' . PHP_EOL;
            foreach ($availableLanguages as $key => $value) {
                $dataReturn.= 'availableLanguages["' . $key . '"] = "' . $value . '";' . PHP_EOL;
            }

            $dataReturn.= 'reviewTrans = new Array();' . PHP_EOL;
            foreach ($availableLanguages as $key => $value) {
                $dataReturn.= 'reviewTrans["' . $key . '"] = new Array();' . PHP_EOL;
            }

            foreach ($availableLanguages as $lang => $value) {
                // testing load all message file
                $dataMessage = Yii::app()->getMessages();
                $basePath = $dataMessage->basePath;
                $fileToInclude = $basePath . '/' . $lang . '/review.php';
                if (!file_exists($fileToInclude)) {
                    $fileToInclude = $basePath . '/en/review.php';
                }
                $arrTranslations[$lang] = include($fileToInclude);
            }

            foreach ($arrTranslations as $lang => $value) {
                foreach ($value as $key => $data) {
                    // clear translation data and retrieve htmlentities
                    $dataReturn.= 'reviewTrans["' . $lang . '"]["' . $key . '"] = "' .
                    htmlentities(html_entity_decode($data)) . '";' . PHP_EOL;
                }
            }

            Yii::app()->cache->set(
                "ReviewsTranslation" . date("Ymd"),
                json_encode($dataReturn),
                Yii::app()->params['cache_long']
            );
        } else {
            $dataReturn = json_decode($dataReturn, true);
        }

        return $dataReturn;
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'name';
    }

}
