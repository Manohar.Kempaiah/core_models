<?php

Yii::import('application.modules.core_models.models._base.BaseEmrScheduleExceptionHour');

class EmrScheduleExceptionHour extends BaseEmrScheduleExceptionHour
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array default scope (applies only to SELECT statements)
     */
    public function defaultScope()
    {
        return array(
            'alias' => $this->tableName(),
        );
    }

    public function scopes()
    {
        return array(
            'active' => array(
                'condition'=> $this->tableName() . '.enabled_flag = 1 AND ' . $this->tableName() . '.del_flag = 0',
            )
        );
    }

    /**
     * Validate that time_start and time_end
     * @return bool
     */
    public function beforeSave()
    {

        if ($this->id) {
            $this->date_updated = date('Y-m-d H:i:s');
        } else {
            $this->date_added = date('Y-m-d H:i:s');
            $this->date_updated = date('Y-m-d H:i:s');
        }

        $arrStart = explode(":", $this->time_start);
        $arrEnd = explode(":", $this->time_end);

        $timeStart = date_parse_from_format("H:i", $arrStart[0] . ":" . $arrStart[1]);
        $timeEnd = date_parse_from_format("H:i", $arrEnd[0] . ":" . $arrEnd[1]);

        if ($timeStart >= $timeEnd) {
            $this->addError('time_start', 'Start time must be earlier than end time');
            return false;
        }

        return parent::beforeSave();
    }
}
