<?php

Yii::import('application.modules.core_models.models._base.BaseCpnInstallationUser');

class CpnInstallationUser extends BaseCpnInstallationUser
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Get Providers User
     *
     * @param str $accountId
     * @param int $cpnInstallationId
     * @return array
     */
    public function getProvidersInstallationUser($cpnInstallationUserId, $cpnInstallationId)
    {
        if (!empty($cpnInstallationUserId)) {
            // Query
            $sql = sprintf(
                "SELECT id,
                    ehr_user_id,
                    ehr_user_data
                FROM cpn_installation_user
                WHERE id = %s; ",
                $cpnInstallationUserId
            );

        } else {
            // Query
            $sql = sprintf(
                "SELECT id,
                    ehr_user_id,
                    ehr_user_data
                FROM cpn_installation_user
                WHERE id NOT IN (
                    SELECT cpn_installation_user_id
                    FROM cpn_installation_provider
                    INNER JOIN cpn_installation
                        ON cpn_installation.id = cpn_installation_provider.cpn_installation_id
                    WHERE cpn_installation.id = %s
                    AND cpn_installation_provider.cpn_installation_user_id IS NOT NULL
                )
                AND cpn_installation_id = %s
                ORDER BY ehr_user_data; ",
                $cpnInstallationId,
                $cpnInstallationId
            );
        }

        // Return all the result
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get Companion Users by Installation Id
     *
     * @param int $cpnInstallationId
     * @return array
     */
    public static function getUsersByInstallationId($cpnInstallationId)
    {
        $sql = sprintf(
            "SELECT id,
                ehr_user_data AS name
            FROM cpn_installation_user
            WHERE cpn_installation_id = %s; ",
            $cpnInstallationId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get UserID using the EHR User Identifier
     * @param $cpnInstallationId
     * @param $ehrUserIdentifier
     * @return mixed
     */
    public static function getUserIdByEhrIdentifier($cpnInstallationId, $ehrUserIdentifier)
    {
        $sql = sprintf(
            "SELECT cpn_installation_user.id AS cpn_installation_user_id
                FROM cpn_installation_user
                WHERE cpn_installation_id= '%s'
                AND cpn_installation_user.ehr_user_id = '%s';",
            $cpnInstallationId,
            $ehrUserIdentifier
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

}
