<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteReviewResponseLog');

class PartnerSiteReviewResponseLog extends BasePartnerSiteReviewResponseLog
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Before save partner_site_review_response_log
     * @return boolean
     */
    public function beforeSave()
    {

        if (empty($this->date_added)) {
            $this->date_added = date("Y-m-d H:i:s");
        }
        return parent::beforeSave();
    }

    /**
     * Create new entry in the log
     * @param array $data
     * @return void
     */
    public static function add($data)
    {
        if (empty($data)) {
            return false;
        }

        $accountId = 1; // Default DDC account
        if (php_sapi_name() != 'cli') {
            if (isset(Yii::app()->user->id)) {
                $accountId = Yii::app()->user->id;
            }
        }

        $psrrl = new PartnerSiteReviewResponseLog();
        $psrrl->partner_site_review_response_id = $data->id;
        $psrrl->account_id = $accountId;
        $psrrl->status = $data->status;
        $psrrl->comment = isset($data->json_details) ? $data->json_details : '';
        $psrrl->attributes = $data;
        $psrrl->save();
    }
}
