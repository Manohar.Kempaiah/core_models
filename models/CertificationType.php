<?php

Yii::import('application.modules.core_models.models._base.BaseCertificationType');

class CertificationType extends BaseCertificationType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'unique'),
            array('name', 'length', 'max' => 50),
            array('id, name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from certifying_type table if associated records exist in provider_certification table
        $has_associated_records = ProviderCertification::model()->exists(
            'certification_type_id=:certification_type_id',
            array(
                ':certification_type_id' => $this->id
            )
        );
        if ($has_associated_records) {
            $this->addError('id', 'This certification type cannot be removed because certifications are linked to it.');
            return false;
        }

        return parent::beforeDelete();
    }

}
