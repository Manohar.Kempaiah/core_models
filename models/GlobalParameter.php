<?php

Yii::import('application.modules.core_models.models._base.BaseGlobalParameter');

class GlobalParameter extends BaseGlobalParameter
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'key' => 'Key',
            'value' => 'Value',
        );
    }

}
