<?php

Yii::import('application.modules.core_models.models._base.BaseSchedulingMailHistory');

class SchedulingMailHistory extends BaseSchedulingMailHistory
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }


    /**
     * Get appointment history to be displayed in PADM
     * @param int $id
     * @return array
     */
    public static function getOnlineAppointmentHistory($id)
    {
        // query
        $sql = sprintf(
            "SELECT DISTINCT
            sm.source,
            sm.status,
            sm.appointment_type,
            sm.time_start,
            sm.time_end,
            sm.cell_phone,
            sm.email,
            sm.appointment_date,
            sm.date_time,
            sm.reason,
            sm.previous_date,
            DATE_FORMAT(
                IF (sm.emr_date_sent IS NULL,
                    IF ((
                            SELECT MAX(sm2.emr_date_sent)
                            FROM scheduling_mail_history sm2
                            WHERE sm.date_added BETWEEN DATE_SUB(sm2.date_added, INTERVAL 5 SECOND)
                                AND sm2.date_added
                                AND sm.status = sm2.status
                                AND sm.scheduling_mail_id = sm2.scheduling_mail_id
                        ) IS NOT NULL,
                        (
                            SELECT MAX(sm2.emr_date_sent)
                            FROM scheduling_mail_history sm2
                            WHERE sm.date_added BETWEEN DATE_SUB(sm2.date_added, INTERVAL 5 SECOND)
                                AND sm2.date_added
                                AND sm.status = sm2.status
                                AND sm.scheduling_mail_id = sm2.scheduling_mail_id
                        ),
                        sm.date_added),
                    sm.date_added), '%%Y-%%m-%%d %%H:%%i') AS date_added_group,
            DATE_FORMAT(
                IF (sm.emr_date_sent IS NULL,
                    (
                        SELECT MAX(sm2.emr_date_sent)
                        FROM scheduling_mail_history sm2
                        WHERE sm.date_added BETWEEN DATE_SUB(sm2.date_added, INTERVAL 5 SECOND)
                            AND sm2.date_added
                            AND sm.status = sm2.status
                            AND sm.scheduling_mail_id = sm2.scheduling_mail_id
                    ),
                    sm.emr_date_sent), '%%Y-%%m-%%d %%H:%%i') AS emr_date_sent
            FROM scheduling_mail_history sm
            WHERE sm.scheduling_mail_id = %d
            ORDER BY sm.id ASC;",
            $id
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }
}
