<?php

Yii::import('application.modules.core_models.models._base.BaseNppesDataValidationForm');

class NppesDataValidationForm extends BaseNppesDataValidationForm
{

    /**
     * @var string Provider SSN
     */
    public $ssn;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Override the search method to avoid filter by date_added and date_sent automatically
     * @return \CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('npi_id', $this->npi_id, true);
        $criteria->compare('country_of_birth', $this->country_of_birth, true);
        $criteria->compare('state_of_birth', $this->state_of_birth, true);
        $criteria->compare('dob', $this->dob, true);
        $criteria->order = 'date_added';

        return new CActiveDataProvider(
            $this,
            array(
                'criteria' => $criteria,
                'pagination' => array('pageSize' => 100),
            )
        );
    }

    /**
     * Save the encrypted ssn in the file system after save a row.
     * @return boolean
     */
    public function afterSave()
    {
        // get filename
        $filename = $this->getSsnFilename();

        // encrypt the ssn
        $sm = new CSecurityManager();
        $sm->setEncryptionKey(Yii::app()->params['nppes_encryption_key']);
        $encryptedSsn = base64_encode($sm->encrypt($this->ssn));

        // save it in filesystem
        file_put_contents($filename, $encryptedSsn);

        return parent::afterSave();
    }

    /**
     * Remove the encrypted ssn from the file system after delete a row.
     * @return boolean
     */
    public function afterDelete()
    {
        // get filename
        $filename = $this->getSsnFilename();

        // remove the file if exists
        if (file_exists($filename)) {
            unlink($filename);
        }

        return parent::afterDelete();
    }

    /**
     * Return the SSN filename of loaded row.
     * @return string
     */
    private function getSsnFilename()
    {
        return Yii::getPathOfAlias('application.data.ssn') . '/' . $this->id . '.dat';
    }

    /**
     * Get the ssn decrypted, in case of error return false.
     * @return string
     */
    public function getSsn()
    {
        // get filename
        $filename = $this->getSsnFilename();

        // return false if the file doesn't exists
        if (file_exists($filename)) {
            $encrypted = file_get_contents($filename);
        } else {
            return false;
        }

        // decrypt the previous encrypted ssn
        $sm = new CSecurityManager();
        $sm->setEncryptionKey(Yii::app()->params['nppes_encryption_key']);
        return $sm->decrypt(base64_decode($encrypted));
    }

    /**
     * Create method, this could be used statically.
     *
     * @param array $data
     * @return boolean|string
     */
    public static function create($data)
    {
        // find the npi in the db
        $exists = Provider::model()->exists('npi_id = :npi_id', [':npi_id' => $data['npi']]);

        if (!$exists) {
            return 'Invalid NPI number.';
        }

        // try to save the sent data
        try {
            // remove old record if exists
            $existentRecords = NppesDataValidationForm::model()->findAll(
                'npi_id = :npi AND date_sent IS NULL',
                [':npi' => trim($data['npi'])]
            );

            // loop by each found record
            foreach ($existentRecords as $row) {
                $row->delete();
            }

            // check if the dob is valid
            if (!StringComponent::validDate($data['dob'], 'Y-m-d')) {
                return 'Please enter a valid date in the format YYYY-MM-DD';
            }

            // now stores the sent data into db
            $nppesData = new NppesDataValidationForm();
            $nppesData->first_name = $data['first_name'];
            $nppesData->last_name = $data['last_name'];
            $nppesData->npi_id = trim($data['npi']);
            // keep only numbers from ssn and encrypt the number before store it into the db
            $nppesData->ssn = str_replace('-', '', $data['ssn']);
            $nppesData->dob = $data['dob'];
            $nppesData->country_of_birth = $data['cob'];
            $nppesData->state_of_birth = $data['sob'];
            $saved = $nppesData->save();
        } catch (Exception $ex) {
            return $ex->getMessage();
        }

        // success
        if ($saved) {
            return true;
        }

        // a validation error occurred
        return print_r('Error while saving data: ' . $nppesData->errors, true);
    }

}
