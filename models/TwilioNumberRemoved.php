<?php

Yii::import('application.modules.core_models.models._base.BaseTwilioNumberRemoved');

class TwilioNumberRemoved extends BaseTwilioNumberRemoved
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
