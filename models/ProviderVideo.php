<?php

Yii::import('application.modules.core_models.models._base.BaseProviderVideo');

class ProviderVideo extends BaseProviderVideo
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    //called on rendering the column for each row
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id .
            '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">' .
            $this->provider . '</a>');
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_provider
            INNER JOIN provider_video
            ON account_provider.provider_id = provider_video.provider_id
            WHERE provider_video.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * Return the provider´s videos
     * @param int $providerId
     * @return array
     */
    public static function getVideos($providerId = 0, $cache = true)
    {
        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

        $sql = sprintf(
            "SELECT id, provider_id, embed_code
            FROM provider_video
            WHERE provider_id = %d",
            $providerId
        );
        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $providerId
     * @param int $accountId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0, $accountId = 0)
    {
        $results['success'] = true;
        $results['error'] = "";

        if (empty($postData)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        if ($providerId == 0 || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "PROVIDER_ID_ZERO";
            return $results;
        }

        // validate access
        ApiComponent::checkPermissions('Provider', $accountId, $providerId);

        foreach ($postData as $data) {
            $model = null;
            $backendAction = !empty($data['backend_action']) ? trim($data['backend_action']) : '';
            $videoUrl = !empty(trim($data['embed_code'])) ? trim($data['embed_code']) : '';
            $data['id'] = isset($data['id']) ? $data['id'] : 0;

            if (!StringComponent::validateUrl($videoUrl)) {
                $results['success'] = false;
                $results['error'] = "ERROR_INVALID_URL";
                $results['data'] = $data;
                return $results;
            }

            if ($data['id'] > 0) {
                $model = ProviderVideo::model()->findByPk($data['id']);
            }

            if ($backendAction == 'delete' || empty($videoUrl)) {
                // delete the row
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_VIDEO";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }
                    $auditSection = 'Provider Video provider_id #' . $providerId;
                    AuditLog::create('D', $auditSection, 'provider_video', null, false);
                }

            } else {

                $auditAction = 'U';
                if (!$model) {
                    $model = new ProviderVideo;
                    $auditAction = 'C';
                }

                $model->embed_code = trim($data['embed_code']);
                $model->provider_id = $providerId;

                if ($model->save()) {
                    $auditSection = 'Provider Video provider_id #' . $providerId;
                    AuditLog::create($auditAction, $auditSection, 'provider_video', null, false);
                    $results['success'] = true;
                    $results['error'] = "";
                    if ($auditAction == 'C') {
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                } else {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_VIDEO";
                    $results['data'] = $model->getErrors();
                    return $results;
                }
            }
        }
        return $results;
    }

}
