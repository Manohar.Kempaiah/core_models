<?php

Yii::import('application.modules.core_models.models._base.BaseProviderSpecialtyLicense');

class ProviderSpecialtyLicense extends BaseProviderSpecialtyLicense
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Save all specialty licenses and states
     * @param int $accountId
     * @param int $providerId
     * @param int $subSpecialtyId
     * @param array $arrLicenses
     */
    public static function saveSpecialtyLicenses($accountId, $providerId, $subSpecialtyId, $arrLicenses)
    {
        // this will contain all the states associated with the specialty
        $arrStates = [];

        if ($arrLicenses) {
            foreach ($arrLicenses['number'] as $licenseOrder => $licenseNumber) {
                $stateId = $arrLicenses['state'][$licenseOrder];

                $arrStates[] = $stateId;

                // search for the license and update the license number if is different
                $specialtyLicense = ProviderSpecialtyLicense::model()->find(
                    'provider_id = :provider_id AND sub_specialty_id = :sub_specialty_id AND state_id = :state_id',
                    [':provider_id' => $providerId, ':sub_specialty_id' => $subSpecialtyId, ':state_id' => $stateId]
                );

                if (empty($specialtyLicense)) {
                    $specialtyLicense = new ProviderSpecialtyLicense();
                    $specialtyLicense->provider_id = $providerId;
                    $specialtyLicense->sub_specialty_id = $subSpecialtyId;
                    $specialtyLicense->state_id = $stateId;
                }

                if ($specialtyLicense->license_number != $licenseNumber) {
                    $specialtyLicense->license_number = $licenseNumber;
                    $specialtyLicense->save();
                }
            }
        }

        // remove not related licenses
        if ($arrStates) {
            $arrSpecialtyLicense = ProviderSpecialtyLicense::model()->findAll(
                'provider_id = :provider_id AND sub_specialty_id = :sub_specialty_id AND state_id NOT IN (' .
                join(',', $arrStates) . ')',
                [':provider_id' => $providerId, ':sub_specialty_id' => $subSpecialtyId]
            );
        } else {
            $arrSpecialtyLicense = ProviderSpecialtyLicense::model()->findAll(
                'provider_id = :provider_id AND sub_specialty_id = :sub_specialty_id',
                [':provider_id' =>$providerId, ':sub_specialty_id' => $subSpecialtyId]
            );
        }

        foreach ($arrSpecialtyLicense as $specialtyLicense) {
            $specialtyLicense->delete();
        }
    }

}
