<?php

Yii::import('application.modules.core_models.models._base.BaseResidencyType');

class ResidencyType extends BaseResidencyType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from residency_type if associated records exist in provider_residency table
        $has_associated_records = ProviderResidency::model()->exists(
            'residency_type_id=:residency_type_id',
            array(':residency_type_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Couldn't delete residency type since there are residencies linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

}
