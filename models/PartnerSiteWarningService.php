<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteWarningService');

class PartnerSiteWarningService extends BasePartnerSiteWarningService
{
    /*
     * Model
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Validate data beforeSave()
     * @return boolean
     */
    public function beforeSave()
    {

        $validPartnerSite = true;
        if (!$this->isNewRecord) {
            if ($this->partner_site_id == $this->oldRecord->partner_site_id) {
                $validPartnerSite = false;
            }
        }

        // partner Site already exists
        if ($validPartnerSite) {
            $has_previous_records = PartnerSiteWarningService::model()->exists(
                '( date_end IS NULL OR date_end > now() ) AND partner_site_id=:partner_site_id',
                array(':partner_site_id' => $this->partner_site_id)
            );
            if ($has_previous_records) {
                $this->addError('partner_site_id', 'Partner Site already selected for this date.');
                return false;
            }
        }
        
        return parent::beforeSave();
    }

}
