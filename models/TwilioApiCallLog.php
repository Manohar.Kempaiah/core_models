<?php
Yii::import('application.modules.core_models.models._base.BaseTwilioApiCallLog');

class TwilioApiCallLog extends BaseTwilioApiCallLog
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
