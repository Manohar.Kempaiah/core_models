<?php

Yii::import('application.modules.core_models.models._base.BasePopulation');

class Population extends BasePopulation
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100)
        ));
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from population if associated records exist in provider_population table
        $has_associated_records = ProviderPopulation::model()->exists(
            'population_id=:population_id',
            array(
                ':population_id' => $this->id
            )
        );
        if ($has_associated_records) {
            $this->addError('id', "Population can't be deleted because there are providers linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    public function findByPartnerSite($partnerSiteId)
    {
        $connection = Yii::app()->db;

        $sql = sprintf(
            "SELECT DISTINCT(p.name), p.id
            FROM prd01.population p
            INNER JOIN prd01.provider_population pp ON pp.population_id = p.id
            INNER JOIN prd01.account_provider ap on ap.provider_id = pp.provider_id
            INNER JOIN prd01.organization_account oa on oa.account_id = ap.account_id
            INNER JOIN prd01.organization o on o.id = oa.organization_id
            WHERE o.partner_site_id = %u
            ORDER BY p.name",
            $partnerSiteId
        );

        return $connection->createCommand($sql)->queryAll();
    }
}
