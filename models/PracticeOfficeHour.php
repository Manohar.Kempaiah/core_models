<?php

Yii::import('application.modules.core_models.models._base.BasePracticeOfficeHour');

class PracticeOfficeHour extends BasePracticeOfficeHour
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Send new office hours to Yelp
     */
    public function afterSave()
    {
        SqsComponent::sendMessage('yelpSync', array('practiceId' => $this->practice_id, 'oldAttributes' => null));
        return parent::afterSave();
    }

    /**
     * Validate that not exists duplicated record for each practice/day
     * @return bool
     */
    public function beforeSave()
    {

        $practiceId = $this->practice_id;
        $dayNumber = $this->day_number;

        // is this a new record?
        if ($this->isNewRecord) {
            // yes, check for existing hours for this day
            $exists = PracticeOfficeHour::model()->exists(
                "practice_id =:practiceId AND day_number=:dayNumber",
                array(':practiceId' => $practiceId, ':dayNumber' => $dayNumber)
            );
        } else {
            // no, check for existing hours for this day
            $exists = PracticeOfficeHour::model()->exists(
                "practice_id =:practiceId AND day_number=:dayNumber AND id!=:id",
                array(':practiceId' => $practiceId, ':dayNumber' => $dayNumber, ':id' => $this->id)
            );
        }

        // was this a duplicate?
        if ($exists) {
            // yes, exit
            $this->addError('day_number', 'There is already a record for that day');
            return false;
        }
        return parent::beforeSave();
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getPracticeTooltip()
    {
        return '<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id .
            '" rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice . '">' .
            $this->practice . '</a>';
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_practice
            INNER JOIN practice_office_hour
            ON account_practice.practice_id = practice_office_hour.practice_id
            WHERE practice_office_hour.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * Find and delete duplicated records
     * because we have duplicated record in db, check if exists for this practice / day
     * used in PADM practiceOfficeHours
     * @param arr $arrOfficeHours
     */
    public static function findAndDeleteDuplicated($arrOfficeHours)
    {
        if (isset($arrOfficeHours->id) && $arrOfficeHours->id > 0) {
            $hasDuplicated = PracticeOfficeHour::model()->findAll(
                "practice_id =:practice_id AND day_number=:day_number AND id!=:id",
                array(
                    ':practice_id' => $arrOfficeHours->practice_id,
                    ':day_number' => $arrOfficeHours->day_number,
                    ':id' => $arrOfficeHours->id
                )
            );
            if ($hasDuplicated) {
                foreach ($hasDuplicated as $eachRecord) {
                    $eachRecord->delete();
                }
            }
        }
    }

    /**
     * get Practice Office Hours
     * @param int $practiceId
     * @param bool $findCombinedHours : if no results and is true, find combined hours from provider at this practice
     * @param int $providerId
     * @return arr $arrHours
     */
    public static function getOfficeHours($practiceId = 0, $findCombinedHours = false, $providerId = false, $useDefault = true)
    {

        $arrDays = array();

        if ($findCombinedHours) {
            $arrDaysPP = ProviderPractice::model()->getCombinedHours($practiceId, $providerId);
            if (!empty($arrDaysPP)) {
                $arrDays = $arrDaysPP;
            }
        }

        if (empty($arrDays)) {
            $arrOfficeHours = PracticeOfficeHour::model()->findAll(array(
                'order' => 'day_number ASC',
                'condition' => 'practice_id = :id',
                'params' => array(':id' => $practiceId)));

            $arrDays = array();
            $days = array(1, 2, 3, 4, 5, 6, 0);
            $dayOfWeek = array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
            foreach ($days as $i) {
                $arrDays[$i]['id'] = null;
                $arrDays[$i]['day'] = $dayOfWeek[$i];
                $arrDays[$i]['selected'] = 0;
                $arrDays[$i]['hoursFrom'] = '';
                $arrDays[$i]['minsFrom'] = '';
                $arrDays[$i]['amFrom'] = 'selected';
                $arrDays[$i]['pmFrom'] = '';
                $arrDays[$i]['hoursTo'] = '';
                $arrDays[$i]['minsTo'] = '';
                $arrDays[$i]['amTo'] = '';
                $arrDays[$i]['pmTo'] = 'selected';
            }

            // format array to send to view
            if ($arrOfficeHours) {
                foreach ($arrOfficeHours as $arrHours) {

                    $hours_from = $mins_from = $am_from = $pm_from = $hours_to = $mins_to = $am_to = $timeRange = '';
                    $pm_to = 'selected';
                    if (stristr($arrHours['hours'], '-')) {
                        list($strFrom, $strTo) = explode(' - ', $arrHours['hours']);

                        $timeFrom = strtotime(date('Y-m-d ' . $strFrom));
                        $timeTo = strtotime(date('Y-m-d ' . $strTo));

                        $hours_from = date('h', $timeFrom);
                        $mins_from = date('i', $timeFrom);
                        $am_from = (date('A', $timeFrom) == 'AM' ? 'selected' : '');
                        $pm_from = (date('A', $timeFrom) == 'PM' ? 'selected' : '');

                        $hours_to = date('h', $timeTo);
                        $mins_to = date('i', $timeTo);
                        $am_to = (date('A', $timeTo) == 'AM' ? 'selected' : '');
                        $pm_to = (date('A', $timeTo) == 'PM' ? 'selected' : '');

                        $timeRange = date('h:iA', $timeFrom) . ' - ' . date('h:iA', $timeTo);
                    }

                    $dayNumber = $arrHours['day_number'];
                    $arrDays[$dayNumber]['id'] = $arrHours['id'];
                    $arrDays[$dayNumber]['selected'] = 1;
                    $arrDays[$dayNumber]['hoursFrom'] = $hours_from;
                    $arrDays[$dayNumber]['minsFrom'] = $mins_from;
                    $arrDays[$dayNumber]['amFrom'] = $am_from;
                    $arrDays[$dayNumber]['pmFrom'] = $pm_from;
                    $arrDays[$dayNumber]['hoursTo'] = $hours_to;
                    $arrDays[$dayNumber]['minsTo'] = $mins_to;
                    $arrDays[$dayNumber]['amTo'] = $am_to;
                    $arrDays[$dayNumber]['pmTo'] = $pm_to;
                    $arrDays[$dayNumber]['timeRange'] = $timeRange;
                }
            }
        }

        if ($useDefault) {
            if (empty($arrDays)) {
                // assign default hours 09AM to 05PM
                $arrDays = array();
                foreach ($days as $i) {
                    $arrDays[$i]['id'] = null;
                    $arrDays[$i]['day'] = $dayOfWeek[$i];
                    $arrDays[$i]['selected'] = 0;
                    $arrDays[$i]['hoursFrom'] = '09';
                    $arrDays[$i]['minsFrom'] = '00';
                    $arrDays[$i]['amFrom'] = 'selected';
                    $arrDays[$i]['pmFrom'] = '';
                    $arrDays[$i]['hoursTo'] = '05';
                    $arrDays[$i]['minsTo'] = '00';
                    $arrDays[$i]['amTo'] = '';
                    $arrDays[$i]['pmTo'] = 'selected';
                }
            }
        }

        return $arrDays;
    }

    /** Process the save data for Time range
     *  arrDays[day number][
     *      'id' => '26706'
     *      'day' => 'Mon'
     *      'selected' => 1
     *      'hoursFrom' => '08'
     *      'minsFrom' => '00'
     *      'amFrom' => 'selected'
     *      'pmFrom' => ''
     *      'hoursTo' => '08'
     *      'minsTo' => '00'
     *      'amTo' => ''
     *      'pmTo' => 'selected'
     *      'timeRange' => '08:00AM - 08:00PM'
     *      ]
     * @param int $practiceId
     * @param array $arrDays
     * @return bool
     */
    public static function postTimeRange($practiceId = 0, $arrDays = array())
    {
        if ($practiceId == 0 || empty($arrDays)) {
            return false;
        }
        $results['success'] = true;
        $results['error'] = "";
        $results['data'] = [];

        foreach ($arrDays as $dayNumber => $value) {

            $practiceOfficeHoursId = !empty($value['id']) ? $value['id'] : 0;

            if (!empty($value['selected'])) {
                // this day is selected
                if ($practiceOfficeHoursId > 0) {
                    $arrOfficeHours = PracticeOfficeHour::model()->findByPk($practiceOfficeHoursId);
                } else {
                    $arrOfficeHours = new PracticeOfficeHour();
                    $arrOfficeHours->practice_id = $practiceId;
                    $arrOfficeHours->day_number = $dayNumber;
                }
                $formattedHours = TimeComponent::formatHoursRange($value);
                if ($arrOfficeHours) {

                    $arrOfficeHours->hours = $formattedHours;
                    PracticeOfficeHour::findAndDeleteDuplicated($arrOfficeHours);

                    if (!$arrOfficeHours->save()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_SAVING_PRACTICE_OFFICE_HOURS";
                        $results['data'] = $arrOfficeHours->getErrors();
                        return $results;
                    } else {
                        $results['success'] = true;
                        $results['error'] = "";
                        $value['id'] = $arrOfficeHours->id;
                        $value['formattedHours'] = $formattedHours;
                        $results['data'][$dayNumber] =  $value;
                    }

                }

            } else {
                // is not selected
                if ($practiceOfficeHoursId > 0) {
                    $practiceOfficeHour = PracticeOfficeHour::model()->findByPk($practiceOfficeHoursId);
                    if ($practiceOfficeHour) {
                        if (!$practiceOfficeHour->delete()) {
                            $results['success'] = false;
                            $results['error'] = "ERROR_DELETING_PRACTICE_OFFICE_HOURS";
                            $results['data'] = $practiceOfficeHour->getErrors();
                            return $results;
                        }
                    }
                }
            }
        }
        return $results;
    }
}
