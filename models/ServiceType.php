<?php

Yii::import('application.modules.core_models.models._base.BaseServiceType');

class ServiceType extends BaseServiceType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
