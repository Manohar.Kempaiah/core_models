<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPhoto');

class ProviderPhoto extends BaseProviderPhoto
{
    public $real_path = null;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id='
            . $this->provider_id . '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="'
            . $this->provider . '">' . $this->provider . '</a>');
    }

    /**
     * returns a html link, used in the admin view
     * @return string
     */
    public function getPhotoPath()
    {

        if ($this->photo_path) {
            return '<a href="' . AssetsComponent::generateImgSrc($this->photo_path, 'providers', 'XL', true)
                . '"  target="_blank" title="Click to open">' . $this->photo_path . '</a>';
        } else {
            return '<span>No photo</span>';
        }
    }

    /**
     * Regenerate thumbnails, update cover
     * @return boolean
     */
    public function afterSave()
    {
        if ($this->cover && $this->photo_path != '') {

            //update the provider table with the new photo_path
            Provider::afterProviderPhotoUpdate($this);

            SqsComponent::sendMessage('thumbnailProviderRegeneration', $this->photo_path);
        } elseif (!$this->cover) {
            // if the 'cover' field changed, check that we have at least one 'cover' in the table
            $countCovers = ProviderPhoto::model()->exists(
                'provider_id = :provider_id AND cover = 1',
                array(':provider_id' => $this->provider_id)
            );
            if (!$countCovers) {
                Provider::setPhotoPathNull($this);
            }
        }

        if (empty($this->width) || empty($this->height)) {
            // Get and Update width and height
            $filename = str_replace(' ', '_', $this->photo_name);
            $filename = $this->id . '-' . StringComponent::sanitize($filename);

            $image = $this->uploadPath . $filename;

            if (file_exists($image)) {
                $imageInfo = getimagesize($image);
                if (!empty($imageInfo)) {
                    // Update record
                    $this->width = $imageInfo[0];
                    $this->height = $imageInfo[1];
                    $this->save();
                }
            }
        }

        return parent::afterSave();
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_provider
            INNER JOIN provider_photo
                ON account_provider.provider_id = provider_photo.provider_id
            WHERE provider_photo.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * Refresh cache every time a Photo is updated
     * @param array $queue
     */
    public static function regenerateThumbnail($queue)
    {
        $arrSizes = array(
            '1' => 'XS',
            '2' => 'S',
            '3' => 'L',
            '4' => 'XL',
            '5' => 'XXL',
            '6' => 'M',
            '7' => 'Other',
        );

        foreach ($arrSizes as $size) {
            $imgPhotoPath = AssetsComponent::generateImgSrc(
                $queue['body'],
                'providers',
                $size,
                false,
                false,
                false,
                true,
                true
            );
            if (!empty($imgPhotoPath)) {
                @file_get_contents($imgPhotoPath);
            }
        }
    }

    /**
     * Get Provider's Photo(s)
     * @param int $providerId
     * @param bool $returnArray
     * @param bool $useCache
     * @return array
     */
    public static function getProviderPhoto($providerId = 0, $returnArray = false, $useCache = true)
    {

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;
        $result = ProviderPhoto::model()->cache($cacheTime)->findAll(
            array(
                "condition" => sprintf("provider_id=%d", $providerId),
                "order" => "cover DESC, sort_order ASC, date_added ASC",
            )
        );

        if (!empty($result) && Common::isTrue($returnArray)) {
            $photos = array();
            foreach ($result as $photo) {
                $photos[] = (array)$photo->attributes;
            }
            $result = $photos;
        }

        $areCoverPhoto = false;
        if (!empty($result)) {
            foreach ($result as $key => $value) {
                if ($value['cover'] == 1) {
                    $areCoverPhoto = true;
                }
                $result[$key]['real_path'] = AssetsComponent::generateImgSrc(
                    $value['photo_path'],
                    'providers',
                    'XL',
                    true
                );
            }

            // Are there any cover photo?
            if (!$areCoverPhoto && !empty($result[0]['id'])) {
                // Nop, ok find and set the first record (older photo) as cover
                ProviderPhoto::makeCoverPhoto($result[0]['id']);
                $result[0]['cover'] = 1;
            }
        }

        return $result;
    }

    /**
     * Get Provider's Cover Photo(s)
     * @param int $providerId
     * @param boole $allPhotos
     * @return array
     */
    public static function getCoverPhoto($providerId = 0, $allPhotos = null)
    {

        if (empty($allPhotos)) {
            // get all photos for this provider
            $allPhotos = ProviderPhoto::getProviderPhoto($providerId, true, false);
        }

        $coverPhoto = '';
        if (!empty($allPhotos)) {
            foreach ($allPhotos as $eachPhoto) {
                if ($eachPhoto['sort_order'] == 0) {
                    $coverPhoto = $eachPhoto;
                }
                if ($eachPhoto['cover'] == 1) {
                    $coverPhoto = $eachPhoto;
                    break;
                }
            }
        }
        return $coverPhoto;
    }

    /**
     * Set as cover Photo
     * @param int $ppId provider_photo.id
     * @return string $photoRealPath
     */
    public static function makeCoverPhoto($ppId = 0)
    {
        $pp = ProviderPhoto::model()->findByPk($ppId);

        if (empty($pp)) {
            return false;
        }

        ProviderPhoto::model()->updateAll(
            array('cover' => '0'),
            'provider_id=:provider_id',
            array(':provider_id' => $pp->provider_id)
        );

        $pp->cover = 1;
        $pp->save();

        $objProvider = Provider::model()->findByPk($pp->provider_id);
        $objProvider->photo_path = $pp->photo_path;
        $objProvider->save();

        // save to audit log and notify changes to phabricator
        $auditSection = 'Cover photo for provider #' . $pp->provider_id . '(' . $objProvider->first_name . ' ' .
            $objProvider->last_name . ')';
        AuditLog::create('U', $auditSection, 'provider_photo', null, true);

        $realPhotoPath = '';
        if ($objProvider->photo_path) {
            $realPhotoPath = AssetsComponent::generateImgSrc($objProvider->photo_path, 'providers', 'S', true, true);
        }

        return $realPhotoPath;
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $providerId
     * @param int $accountId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0, $accountId = 0)
    {
        $results['success'] = true;
        $results['error'] = "";

        if (empty($postData)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        if ($providerId == 0 || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "PROVIDER_ID_ZERO";
            return $results;
        }

        // validate access
        ApiComponent::checkPermissions('Provider', $accountId, $providerId);

        foreach ($postData as $data) {
            $model = null;
            $backendAction = !empty($data['backend_action']) ? trim($data['backend_action']) : '';
            $data['id'] = isset($data['id']) ? $data['id'] : 0;

            if ($data['id'] > 0) {
                $model = ProviderPhoto::model()->findByPk($data['id']);
            }

            if ($backendAction == 'delete') {
                // delete the row
                if (!empty($model)) {
                    $cover = $model->cover;

                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_PHOTO";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }

                    if ($cover == 1) {
                        $provider = Provider::model()->findByPk($providerId);
                        $provider->photo_path = '';
                        $provider->save();
                    }
                    $auditSection = 'Provider Photo provider_id #' . $providerId;
                    AuditLog::create('D', $auditSection, 'provider_photo', null, false);
                }
                continue;
            }

            $auditAction = 'U';
            if (empty($model)) {
                $model = new ProviderPhoto;
                $auditAction = 'C';
                $model->provider_id = $providerId;
                // upload the new photo
                $resultUpload = File::upload($model, "photo_path", true);
                if (!$resultUpload['success']) {
                    $results['success'] = false;
                    $results['error'] = $resultUpload['error'];
                    $results['data'] = $resultUpload['data'];
                    return $results;
                }
                $model->photo_name = $resultUpload['uploaded_file_name'];
            }
            $model->sort_order = $data['sort_order'];
            $model->cover = $data['cover'];
            $model->caption = StringComponent::truncate($data['caption'], 100);

            if ($model->save()) {
                $auditSection = 'Provider Photo provider_id #' . $providerId;
                AuditLog::create($auditAction, $auditSection, 'provider_photo', null, false);
                $results['success'] = true;
                $results['error'] = "";
                if ($auditAction == 'C') {
                    $data['id'] = $model->id;
                    unset($data['backend_action']);
                    $data['real_path'] = AssetsComponent::generateImgSrc(
                        $model->photo_path,
                        'providers',
                        'XL',
                        true,
                        true
                    );
                    $results['data'][] = $data;
                }

                if ($model->cover == 1) {
                    // set as Cover Photo
                    ProviderPhoto::makeCoverPhoto($model->id);
                }
            } else {
                $results['success'] = false;
                $results['error'] = "ERROR_SAVING_PROVIDER_PHOTO";
                $results['data'] = $model->getErrors();
                return $results;
            }
        }

        return $results;
    }

    /**
     * Get photo URL
     * @param array $params
     * @return string
     */
    public function getUrl($params = [])
    {
        if (empty($this->photo_path)) {
            return '';
        }
        $url = 'https://' . Yii::app()->params->servers['dr_ast_hn'] . '/img?p=providers/' . $this->photo_path;

        if ($params) {
            $querystring = http_build_query($params);
            $url .= '&' . $querystring;
        }

        return $url;
    }
}
