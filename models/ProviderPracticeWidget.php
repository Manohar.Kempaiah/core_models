<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeWidget');

class ProviderPracticeWidget extends BaseProviderPracticeWidget
{

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Get all the provider_practice_widget records for a given widget
     *
     * @param int $widgetId
     * @return array
     */
    public static function getProviderPractices($widgetId)
    {
        $sql = sprintf(
            "SELECT ppw.id, ppw.widget_id, ppw.provider_id, ppw.practice_id, ppw.prefix, ppw.suffix,
            ppw.middle_name
            FROM provider_practice_widget ppw
            INNER JOIN provider_details pd
                ON ppw.provider_id = pd.provider_id
            WHERE ppw.widget_id = %d",
            $widgetId
        );
        return ProviderPracticeWidget::model()->findAllBySql($sql);
    }

    /**
     * Get all the provider_practice_widget records for this provider - practice - account
     *
     * @param int $providerId
     * @param int $practiceId
     * @param int $accountId
     * @return array
     */
    public static function getWidgetFromProviderPracticeAccount($providerId = 0, $practiceId = 0, $accountId = 0)
    {
        $sql = sprintf(
            "SELECT ppw.id
            FROM provider_practice_widget AS ppw
            LEFT JOIN widget
                ON widget.id = ppw.widget_id
            WHERE ppw.provider_id = %d
                AND ppw.practice_id = %d
                AND widget.account_id = %d
                AND widget.hidden = 0;",
            $providerId,
            $practiceId,
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get all the provider_practice_widget records for this account - provider
     *
     * @param int $accountId
     * @param int $providerPracticeId
     * @param bool $isProvider
     * @param string $widgetType
     * @param string $useGroupBy
     * @param int $practiceId
     * @return array
     */
    public static function getWidgetFromProviderAccount(
        $accountId = 0,
        $providerPracticeId = 0,
        $isProvider = 1,
        $widgetType = '',
        $useGroupBy = true,
        $practiceId = 0
    ) {

        if ($isProvider) {
            $providerPracticeCondition = sprintf(' AND ppw.provider_id = %d ', $providerPracticeId);
            if ($practiceId > 0) {
                $providerPracticeCondition .= sprintf(' AND ppw.practice_id = %d ', $practiceId);
            }

        } else {
            $providerPracticeCondition = sprintf('AND ppw.practice_id = %d', $providerPracticeId);
        }

        $selectValue = 'widget.name';
        $providerPracticeGroupBy = 'GROUP BY widget.name';
        if (!$useGroupBy) {
            $selectValue = 'ppw.id';
            $providerPracticeGroupBy = '';
        }

        if ($widgetType == 'ALL') {
            $widgetType = '';
        } else {
            $widgetType = "AND widget.widget_type = '".$widgetType."'";
        }

        $sql = sprintf(
            "SELECT %s
            FROM provider_practice_widget AS ppw
            LEFT JOIN widget
                ON widget.id = ppw.widget_id
            WHERE draft = '0'
                AND widget.account_id = %d
                %s
                %s
                %s;",
            $selectValue,
            $accountId,
            $widgetType,
            $providerPracticeCondition,
            $providerPracticeGroupBy
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get all the provider_practice_widget records for providers with enabled scheduling
     *
     * @param int $widgetId
     * @return array
     */
    public static function getWithEnabledScheduling($widgetId)
    {
        $sql = sprintf(
            "SELECT ppw.id, ppw.widget_id, ppw.provider_id, ppw.practice_id, ppw.prefix, ppw.suffix,
            ppw.middle_name
            FROM provider_practice_widget ppw
            INNER JOIN provider_details pd
                ON ppw.provider_id = pd.provider_id
            WHERE pd.schedule_mail_enabled = 1
                AND ppw.widget_id = %d",
            $widgetId
        );
        return ProviderPracticeWidget::model()->findAllBySql($sql);
    }

    /**
     * Delete widget before unlink practice
     *
     * @param int $accountId
     * @param int $providerId
     * @param bool $practiceId
     * @return bool
     */
    public static function deleteWidget($accountId = 0, $providerId = 0, $practiceId = 0)
    {

        if ($providerId > 0 && $practiceId == 0) {
            $sql = sprintf(
                "SELECT provider_practice_widget.id
                FROM provider_practice_widget
                INNER JOIN widget
                    ON provider_practice_widget.widget_id = widget.id
                WHERE widget.account_id = %d
                    AND provider_practice_widget.provider_id = %d ",
                $accountId,
                $providerId
            );

        } elseif ($providerId == 0 && $practiceId > 0) {

            $sql = sprintf(
                "SELECT provider_practice_widget.id
                FROM provider_practice_widget
                INNER JOIN widget
                    ON provider_practice_widget.widget_id = widget.id
                WHERE widget.account_id = %d
                    AND provider_practice_widget.practice_id = %d ",
                $accountId,
                $practiceId
            );

        } elseif ($providerId > 0 && $practiceId > 0) {

            $sql = sprintf(
                "SELECT provider_practice_widget.id
                FROM provider_practice_widget
                INNER JOIN widget
                    ON provider_practice_widget.widget_id = widget.id
                WHERE widget.account_id = %d
                    AND provider_practice_widget.provider_id = %d
                    AND provider_practice_widget.practice_id = %d ",
                $accountId,
                $providerId,
                $practiceId
            );
        }

        $arrPPW = Yii::app()->db->createCommand($sql)->queryAll();
        if (!empty($arrPPW)) {
            foreach ($arrPPW as $widget) {
                $ppw = ProviderPracticeWidget::model()->findByPk($widget['id']);
                if (isset($ppw) && $ppw->delete()) {
                    AuditLog::create('D', 'Unlink Widget #' . $widget['id']);
                } else {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Clone the entries from a given $sourceWidgetId into another $targetWidgetId
     *
     * @param int $sourceWidgetId
     * @param int $targetWidgetId
     * @return bool
     */
    public static function cloneProviderPracticeEntries($sourceWidgetId, $targetWidgetId)
    {
        $sqlInsert = sprintf(
            "INSERT INTO provider_practice_widget
            (widget_id, provider_id, practice_id, prefix, middle_name, suffix)
            SELECT
            %d AS widget_id, provider_id, practice_id,
            prefix, middle_name, suffix
            FROM provider_practice_widget
            WHERE widget_id = %d;",
            $targetWidgetId,
            $sourceWidgetId
        );
        return Yii::app()->db->createCommand($sqlInsert)->execute();
    }

}
