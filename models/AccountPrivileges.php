<?php

Yii::import('application.modules.core_models.models._base.BaseAccountPrivileges');

class AccountPrivileges extends BaseAccountPrivileges
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Returns the privileges to show sections in PADM for the accountId
     * @param int $accountId
     * @param string $connectionType
     * @return array
     */
    public static function getAccountPrivileges($accountId = 0, $connectionType = '')
    {
        $response = array();

        if (empty($connectionType)) {
            $organizationAccount = OrganizationAccount::model()
                ->cache(Yii::app()->params['cache_short'])
                ->find(
                    'account_id=:account_id',
                    array(':account_id' => $accountId)
                );
            if ($organizationAccount) {
                $connectionType = $organizationAccount->connection;
            } else {
                $connectionType = 'O'; // is Freemium account
            }
        }

        $targetAccount = Account::model()->cache(Yii::app()->params['cache_short'])->findByPK($accountId);
        if (empty($targetAccount)) {
            $response['success'] = false;
            $response['error'] = 'USER_NOT_EXISTS';
            return $response;
        }
        // Account data
        $response['id'] = $targetAccount->id;
        $response['first_name'] = $targetAccount->first_name;
        $response['last_name'] = $targetAccount->last_name;
        $response['email'] = $targetAccount->email;

        // setting default values
        $response['access_granted'] = '';
        $response['reviews'] = 0;
        $response['appointment_request'] = 0;
        $response['tracked_calls'] = 0;
        $response['products_services'] = 0;
        $response['directory'] = 0;
        $response['edit_providers'] = 0;
        $response['edit_practices'] = 0;

        // look for privilege restrictions
        $arrPrivileges = AccountPrivileges::model()->cache(Yii::app()->params['cache_short'])->find(
            'account_id=:account_id',
            array(':account_id' => $accountId)
        );

        if ($connectionType == 'S') {

            if (!empty($arrPrivileges)) {
                $response['reviews'] = $arrPrivileges->reviews;
                $response['appointment_request'] = $arrPrivileges->appointment_request;
                $response['tracked_calls'] = $arrPrivileges->tracked_calls;
                $response['directory'] = $arrPrivileges->directory;
                $response['edit_providers'] = $arrPrivileges->edit_providers;
                $response['edit_practices'] = $arrPrivileges->edit_practices;
            }
            // Access granted
            $response['access_granted'] = 'custom';

            // Asociate practices to account
            $practices = AccountPractice::model()->cache(Yii::app()->params['cache_short'])->findAll(
                'account_id=:account_id',
                array(':account_id' => $accountId)
            );

            $practicesData = array();
            if (!empty($practices)) {
                foreach ($practices as $practice) {
                    $practicesData[] = $practice->practice_id;
                    if ($practice->all_providers) {
                        $response['access_granted'] = 'practices';
                    }
                }
            }
            $response['practices'] = $practicesData;

            // Asociate providers to account
            $providers = AccountProvider::model()->cache(Yii::app()->params['cache_short'])->findAll(
                'account_id=:account_id',
                array(':account_id' => $accountId)
            );

            $providersData = array();
            if (!empty($providers)) {
                foreach ($providers as $provider) {
                    $providersData[] = $provider->provider_id;
                    if ($provider->all_practices) {
                        $response['access_granted'] = 'providers';
                    }
                }
            }
            $response['providers'] = $providersData;
        } else {

            // no restrictions set for the account
            if (!$arrPrivileges) {
                // Set all privileges if it is not a staff account
                $response['reviews'] = 1;
                $response['appointment_request'] = 1;
                $response['tracked_calls'] = 1;
                $response['products_services'] = 1;
                $response['directory'] = 1;
                $response['edit_providers'] = 1;
                $response['edit_practices'] = 1;
            } else {
                // apply restrictions
                $response['reviews'] = $arrPrivileges->reviews;
                $response['appointment_request'] = $arrPrivileges->appointment_request;
                $response['tracked_calls'] = $arrPrivileges->tracked_calls;
                $response['products_services'] = $arrPrivileges->products_services;
                $response['directory'] = $arrPrivileges->directory;
                $response['edit_providers'] = $arrPrivileges->edit_providers;
                $response['edit_practices'] = $arrPrivileges->edit_practices;
            }
        }

        return $response;
    }

    /**
     * Save the account privileges
     * @param arr $postData
     * @param arr $accessData - Account data for the logged user
     * @return array
     */
    public static function saveData($postData, $accessData)
    {
        $accountId = $accessData['account_id'];
        $response['success'] = false;

        $newConnectionType = !empty($postData['connection_type']) ? $postData['connection_type'] : 'S';

        $loggedAccountOrg = OrganizationAccount::model()->find(
            'account_id=:account_id',
            array(':account_id' => $accountId)
        );

        if (
            !$loggedAccountOrg // not exists
            || $loggedAccountOrg->connection == 'S' // is staffer
            || ($loggedAccountOrg->connection == 'M' && $newConnectionType == 'O')
        ) {
            // Manager trying to update the owner or is an staffer
            // Access denied
            $response['http_response_code'] = 403;
            $response['error'] = 'ACCESS_DENIED';
            $response['data'] = 'You don´t have access to this section';
            return $response;
        }

        $organizationId = $loggedAccountOrg->organization_id;
        if ($organizationId != $postData['organization_id']) {
            $response['http_response_code'] = 403;
            $response['error'] = 'MISMATCHED_ORGANIZATION';
            $response['data'] = 'Mismatched Organization';
            return $response;
        }

        $requestedAccountId = !empty($postData['requested_account_id']) ? $postData['requested_account_id'] : 0;
        $email = !empty($postData['email']) ? StringComponent::validateEmail($postData['email']) : '';
        $privileges = !empty($postData['privileges']) ? $postData['privileges'] : null;

        $providers = !empty($privileges['providers']) ? $privileges['providers'] : null;
        if (empty($providers)) {
            $providers = !empty($postData['providers']) ? $postData['providers'] : null;
        }

        $practices = !empty($privileges['practices']) ? $privileges['practices'] : null;
        if (empty($practices)) {
            $practices = !empty($postData['practices']) ? $postData['practices'] : null;
        }

        $all = 0;
        if (!isset($privileges['access_granted'])) {
            $all = !empty($postData['all']) ? $postData['all'] : 0;
        } elseif ($privileges['access_granted'] == 'practices' || $privileges['access_granted'] == 'providers') {
            $all = 1;
        }

        $details = array();
        $details['first_name'] = trim($postData['first_name']);
        $details['last_name'] = trim($postData['last_name']);
        $details['connection_type'] = $newConnectionType;
        $details['email'] = $email;
        $details['providers'] = $providers;
        $details['practices'] = $practices;
        $details['privileges'] = $privileges;
        $details['all'] = $all;

        if ($requestedAccountId == 0) {
            //
            // Create new account
            if (empty($email)) {
                $response['error'] = true;
                $response['data'] = "The email could not be empty.";
                return $response;
            }

            $mailAlreadyUsed = Account::model()->exists('email=:email', array(':email' => $email));
            if ($mailAlreadyUsed) {
                $response['error'] = "The account's email is already used.";
                return $response;
            }

            $currentOrg = Organization::model()->findByPK($organizationId);
            $response['success'] = "done!";

            if ($newConnectionType == 'S') { // Create new staff account
                if (!$currentOrg->createStafferAccount($details)) {
                    $response['error'] = "There was an error creating the staffer account.";
                }
            } elseif ($newConnectionType == 'M') { // Create new manager account
                if (!$currentOrg->createManagerAccount($details)) {
                    $response['error'] = "There was an error creating the manager account.";
                }
            }
        } else {
            //
            // Edit account
            $targetAccount = Account::model()->findByPK($requestedAccountId);
            if (empty($targetAccount)) {
                $response['error'] = 'INVALID_ACCOUNT_ID';
            } else {

                // Do we have to Update the connection_type
                $targetOrganizationAccount = OrganizationAccount::model()->find(
                    'account_id=:account_id',
                    array(':account_id' => $requestedAccountId)
                );
                if ($newConnectionType != $targetOrganizationAccount->connection && $newConnectionType != 'O') {
                    // Check if it's owner
                    if ($loggedAccountOrg->connection != 'O') {
                        // Access denied
                        $response['http_response_code'] = 403;
                        $response['error'] = 'ACCESS_DENIED';
                        $response['data'] = 'You don´t have access to this section';
                        return $response;
                    }
                    // Yes
                    $targetOrganizationAccount->connection = $newConnectionType;
                    $targetOrganizationAccount->save();
                }

                if ($newConnectionType == 'S') {
                    // Edit staff account
                    $targetAccount->first_name = $details['first_name'];
                    $targetAccount->last_name = $details['last_name'];
                    $targetAccount->save();

                    // Associate practices to account
                    $accountPractices = AccountPractice::model()->model()->findAll(
                        'account_id=:account_id',
                        array(':account_id' => $requestedAccountId)
                    );

                    if (!empty($accountPractices)) {
                        foreach ($accountPractices as $accountPractice) {
                            if (!$accountPractice->delete()) {
                                $response['error'] = 'Error when try to save practices';
                            }
                        }
                    }
                    if (!empty($details['practices'])) {
                        foreach ($details['practices'] as $practice) {
                            $accountPractice = new AccountPractice();
                            $accountPractice->practice_id = $practice;
                            $accountPractice->account_id = $requestedAccountId;
                            $accountPractice->all_providers = $details['all'];
                            if (!$accountPractice->save()) {
                                $response['error'] = 'Error when try to save practices';
                            }
                        }
                    }

                    // Associate providers to account
                    $accountProviders = AccountProvider::model()->model()->findAll(
                        'account_id=:account_id',
                        array(':account_id' => $requestedAccountId)
                    );

                    if (!empty($accountProviders)) {
                        foreach ($accountProviders as $accountProvider) {
                            if (!$accountProvider->delete()) {
                                $response['error'] = 'Error when try to save providers';
                            }
                        }
                    }
                    if (!empty($details['providers'])) {
                        foreach ($details['providers'] as $provider) {
                            $accountProvider = new AccountProvider();
                            $accountProvider->provider_id = $provider;
                            $accountProvider->account_id = $requestedAccountId;
                            $accountProvider->all_practices = $details['all'];
                            if (!$accountProvider->save()) {
                                $response['error'] = 'Error when try to save providers';
                            }
                        }
                    }

                    // Create privileges to access staffer account
                    $targetAccountPrivileges = AccountPrivileges::model()->model()->find(
                        'account_id=:account_id',
                        array(':account_id' => $requestedAccountId)
                    );

                    if (empty($targetAccountPrivileges)) {
                        $targetAccountPrivileges = new AccountPrivileges();
                        $targetAccountPrivileges->account_id = $requestedAccountId;
                    }
                    $targetAccountPrivileges->reviews = $privileges['reviews'];
                    $targetAccountPrivileges->appointment_request = $privileges['appointment_request'];
                    $targetAccountPrivileges->tracked_calls = $privileges['tracked_calls'];
                    $targetAccountPrivileges->products_services = $privileges['products_services'];
                    $targetAccountPrivileges->directory = isset($privileges['directory']) ? $privileges['directory'] : 0;
                    if (!$targetAccountPrivileges->save()) {
                        $response['error'] = 'Error when try to save privileges';
                    }
                } elseif ($newConnectionType == 'M') {
                    // Edit manager account
                    $targetAccount->first_name = $details['first_name'];
                    $targetAccount->last_name = $details['last_name'];
                    if (empty($targetAccount->date_signed_in)) {
                        $targetAccount->date_signed_in = (!empty($targetAccount->date_added)
                            ? $targetAccount->date_added : date('Y-m-d H:i:s'));
                    }

                    if (!$targetAccount->save()) {
                        $response['error'] = 'Error when try to save manager account';
                    }

                    // Search and delete if exists
                    $targetAccountPrivileges = AccountPrivileges::model()->model()->find(
                        'account_id=:account_id',
                        array(':account_id' => $requestedAccountId)
                    );
                    if (!empty($targetAccountPrivileges)) {
                        $targetAccountPrivileges->delete();
                    }
                }
            }
        }

        if (empty($response['error'])) {

            if ($requestedAccountId == 0) {
                $newAccount = Account::model()->find('email=:email', array(':email' => $email));
                $requestedAccountId = $newAccount->id;
            }

            $response['success'] = true;
            $details['account_id'] = $requestedAccountId;
            $response['data'] = $details;
        }

        return $response;
    }
}
