<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteParams');

class PartnerSiteParams extends BasePartnerSiteParams
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * return a maximum of 3 PartnerSiteParams objects
     */
    public static function getPracticeYelpCategories($practiceID)
    {
        return PartnerSiteParams::model()->findAll(
            "practice_id = :practice_id AND partner_site_id = 54 AND param_name = 'practice_yelp_category' LIMIT 3",
            array(
                ":practice_id" => $practiceID
            )
        );
    }

    /**
     * create a DB entry per our Yelp schema
     */
    public static function createPracticeYelpCategory($practiceID, $catCode)
    {
        $category = new PartnerSiteParams;
        $category->practice_id = $practiceID;
        $category->partner_site_id = 54;
        $category->param_name = "practice_yelp_category";
        $category->param_value = $catCode;
        return $category->save();
    }

    /**
     * check and then create/delete Yelp categories
     */
    public static function adjustYelpCategories($practiceID, $catSet)
    {
        $currentCategories = [];
        $foundCategories = self::getPracticeYelpCategories($practiceID);
        foreach ($foundCategories as $cat) {
            $currentCategories[] = $cat->param_value;

            if (!in_array($cat->param_value, $catSet)) {
                $cat->delete();
            }
        }

        $diff = array_diff($catSet, $currentCategories);

        foreach ($diff as $newCat) {
            self::createPracticeYelpCategory($practiceID, $newCat);
        }
    }

    /**
     * Return param value for Specialties
     * @param int $providerId
     * @param int $partnerSiteId
     * @param string $paramName
     * @return object
     */
    public static function getPartnerSiteParam($providerId, $partnerSiteId, $paramName)
    {
        return PartnerSiteParams::model()->find(
            'provider_id = :provider_id AND partner_site_id = :partner_site_id AND param_name = :param_name',
            [
                ':provider_id' => $providerId,
                ':partner_site_id' => $partnerSiteId,
                ':param_name' => $paramName
            ]
        );
    }

    /**
     * This gets the partner site param value based on the practice
     * @param int $practiceId
     * @param int $partnerSiteId
     * @param string $paramName
     * @return string
     */
    public static function getPartnerSiteValue($practiceId, $partnerSiteId, $paramName)
    {
        //grab the partner site param based on the practice
        $partnerSiteParams = PartnerSiteParams::model()->find(
            'provider_id IS NULL
            AND practice_id = :practice_id
            AND partner_site_id = :partner_site_id
            AND param_name = :param_name',
            [
                ':practice_id' => $practiceId,
                ':partner_site_id' => $partnerSiteId,
                ':param_name' => $paramName
            ]
        );

        //do we have a param value?
        //yes
        if ($partnerSiteParams &&
                $partnerSiteParams["param_value"]) {
            //return the value
            return $partnerSiteParams["param_value"];
        }
        //return null if we got here without a value
        return null;
    }

    /**
     * Get all the records related to practices from Bing Business
     *
     * @return array
     */
    public static function getBingBusinessPlacesPractices()
    {
        //Get all the records from Bing Business Places related to practices
        return PartnerSiteParams::model()->findAll(
            "partner_site_id = 17 AND param_name = 'StoreId'"
        );
    }
}
