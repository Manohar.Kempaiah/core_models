<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteAdServer');

class PartnerSiteAdServer extends BasePartnerSiteAdServer
{

    public $uploadPath;

    /*
     * Model
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Initializes the model with the correct basePath according to the active module.
     * @internal Avoid using __construct. Use init() instead.
     *  if you still need to use it, call parent::__construct($scenario). Read yii documentation.
     */
    public function init()
    {
        parent::init();
        $basePath = Yii::app()->basePath . DIRECTORY_SEPARATOR . 'modules' . DIRECTORY_SEPARATOR . 'providers';
        $this->uploadPath = $basePath . Yii::app()->params['upload_ads'];

        // The directory exists?
        if (!file_exists($this->uploadPath)) {
            mkdir($this->uploadPath, 0777);
        }
    }

    /**
     * Process beforeSave
     */
    public function beforeSave()
    {

        if (empty($this->name)) {
            return false;
        }
        if (empty($this->file_path) && empty($this->content)) {
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * get Partners
     * @return array
     */
    public static function getPartners()
    {
        $sql = "SELECT partner_site.id, partner_site.display_name
            FROM partner_site
            WHERE partner_site.id in (
                select distinct partner_site_id from scheduling_mail where partner_site_id != 1
            ) AND partner_site.partner_id > 1
            ORDER BY partner_site.display_name;";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Search for aqn Ads and mark as viewes by a patient
     *
     * @params int $inviteId
     * @return array
     */
    public static function searchAndMarkAsViewed($inviteId)
    {
        $ads = null;
        // Search for ads via waiting_Room_invite->id
        $partnerSiteAdServer = PartnerSiteAdServer::searchByWaitingRoomInviteId($inviteId);
        if (!empty($partnerSiteAdServer['partner_site_id'])) {
            // Get partner_site related ads
            $ads = PartnerSiteAdServer::getAds($partnerSiteAdServer['partner_site_id']);

            foreach ($ads as $key => $ad) {
                // This ad was viewed by the patient?
                $psav = PartnerSiteAdViews::model()->exists(
                    "partner_site_id=:partner_site_id AND partner_site_ad_id=:partner_site_ad_id
                AND provider_id=:provider_id AND patient_id=:patient_id",
                    array(
                        ":partner_site_id" => $partnerSiteAdServer['partner_site_id'],
                        ":partner_site_ad_id" => $ad['id'],
                        ":provider_id" => $partnerSiteAdServer['provider_id'],
                        ":patient_id" => $partnerSiteAdServer['patient_id'],
                    )
                );

                if ($psav) {
                    // This ad was viewed, we dont have to display it again
                    unset($ads[$key]);
                } else {
                    $partnerSiteId = $partnerSiteAdServer['partner_site_id'];
                    $partnerSiteAdId = $ad['id'];
                    $providerId = $partnerSiteAdServer['provider_id'];
                    $patientId = $partnerSiteAdServer['patient_id'];
                    PartnerSiteAdViews::setAsViewed($partnerSiteId, $partnerSiteAdId, $providerId, $patientId);

                    if (!empty($partnerSiteAdServer['sm_id'])) {
                        // Update waiting_room_invite->scheduling_mail_id
                        $invite = WaitingRoomInvite::model()->findByPk($inviteId);
                        if (empty($invite->scheduling_mail_id)) {
                            $invite->scheduling_mail_id = $partnerSiteAdServer['sm_id'];
                            $invite->save();
                        }
                    }
                }
            }
        }

        return $ads;
    }

    /**
     * get Ads
     * @param integer $partnerSiteId
     * @return array
     */
    public static function getAds($partnerSiteId)
    {

        $ads = PartnerSiteAdServer::model()->findAll(
            'partner_site_id=:partner_site_id AND active = 1',
            [':partner_site_id' => $partnerSiteId]
        );
        $results = [];

        foreach ($ads as $ad) {
            $path = '';
            // If Ad is media type (image/video) fill base URL
            if (in_array($ad->type, ['image', 'video'])) {
                $path = 'https://' . Yii::app()->params['servers']['dr_pro_hn'] . '/images/uploads/ad_server/';
            }
            if (!empty($ad)) {
                $results[] = array(
                    'name' => $ad->name,
                    'id' => $ad->id,
                    'type' => $ad->type,
                    'content' => json_decode($ad->content),
                    'options' => json_decode($ad->options),
                    'url' => $path . str_replace('[timestamp]', time(), $ad->file_path),
                );
            }
        }

        return $results;
    }

    /**
     * Search by waiting_room_invite->id
     *
     * @return array
     */
    public static function searchByWaitingRoomInviteId($wriId)
    {

        $sql = sprintf(
            "SELECT wrs.provider_id AS provider_id,wri.patient_id,
            p.first_name, p.last_name, p.email, p.mobile_phone,
            wri.id AS wri_id, MAX(sm.id) AS sm_id,
            MAX(sm.appointment_date) AS sm_appt_date,
            sm.partner_site_id
            FROM waiting_room_invite AS wri
            INNER JOIN waiting_room_setting as wrs on wri.waiting_room_setting_id = wrs.id
            INNER JOIN patient AS p ON wri.patient_id = p.id
            INNER JOIN scheduling_mail as sm
                ON (wri.patient_id = sm.patient_id AND wrs.provider_id = sm.provider_id)
            WHERE wri.id = %d
                AND DATE_ADD(sm.appointment_date, INTERVAL 90 DAY) > NOW();",
            $wriId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryRow();

    }
}
