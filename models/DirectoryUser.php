<?php

Yii::import('application.modules.core_models.models._base.BaseDirectoryUser');

class DirectoryUser extends BaseDirectoryUser
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Generate the public JSON of the specified directory.
     *
     * @param DirectorySetting $setting
     * @return array
     */
    public static function getUser($email, $password)
    {
        try {

            $result = [];

            // Get the user with given email and password
            $user =  DirectoryUser::model()->find(
                "email = :email AND password = :password",
                [
                    ':email' => $email,
                    ':password' => $password
                ]
            );

            if (!empty($user["email"])) {
                $result = [
                    'email' => $user["email"],
                    'role' => $user["role"],
                    'first_name' => $user["first_name"],
                    'last_name' => $user["last_name"]
                ];
            }

            return $result;

        } catch (\Exception $e) {
            throw new CHttpException(500, '500 - General Error');
        }

    }
}
