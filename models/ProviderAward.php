<?php

Yii::import('application.modules.core_models.models._base.BaseProviderAward');

class ProviderAward extends BaseProviderAward
{

    public $time_period;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id . '" '
            . 'rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">' .
            $this->provider . '</a>');
    }

    /**
     * Gets details about a provider's awards
     * @param int $providerId
     * @return array
     */
    public static function getDetails($providerId)
    {

        $sql = sprintf(
            "SELECT award_name, awarding_institution, received_years
            FROM provider_award
            WHERE provider_id = %d;",
            $providerId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Gets provider's awards
     * prepares the array of time_period
     * @param int $providerId
     * @param bool $useCache
     * @param bool $explodeTimePeriod
     * @return array
     */
    public static function getAwards($providerId = 0, $cache = true, $explodeTimePeriod = false)
    {
        if ($providerId == 0) {
            return false;
        }

        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

        $arrAward = ProviderAward::model()->cache($cacheTime)->findAll("provider_id=:id", array(':id' => $providerId));

        if (!empty($arrAward)) {
            // prepares the array of time_period
            foreach ($arrAward as $key => $value) {
                $arrAward[$key]->time_period = array();
                $receivedYears = $value['received_years'];

                if (!empty($receivedYears)) {
                    $arrReceivedYears = explode(',', $receivedYears);
                    $actualYear = '';
                    $timePeriod = [];
                    $range = 0;

                    foreach ($arrReceivedYears as $eachYear) {

                        $eachYear = trim($eachYear);
                        if (strpos($eachYear, '-') === false) {

                            if (empty($actualYear)) {
                                // first year
                                $timePeriod[$range] = $eachYear . '-' . $eachYear;
                            } else {
                                if ($eachYear == $actualYear + 1) {
                                    $new = substr($timePeriod[$range], 0, -5);
                                    $timePeriod[$range] = $new . '-' . $eachYear;
                                } else {
                                    $range = $range + 1;
                                    $timePeriod[$range] = $eachYear . '-' . $eachYear;
                                }
                            }
                            $actualYear = $eachYear;
                        } else {
                            // is a time range
                            $range = $range + 1;
                            $years = explode('-', $eachYear);
                            $decade = substr($years[0], 0, 2);
                            $decade = ($decade == '19') ? '19' : '20';

                            if (strlen($years[1]) == 4) {
                                $decade = '';
                            }
                            $timePeriod[$range] = $years[0] . '-' . $decade . $years[1];
                            $range = $range + 1;
                            $actualYear = '';
                        }
                    }

                    $receivedYears = '';
                    $arrTimePeriod = array();
                    if (is_array($timePeriod) || is_object($timePeriod)) {
                        foreach ($timePeriod as $tpKey => $tp) {
                            $years = explode('-', $tp);

                            $arrTimePeriod[$tpKey]['from'] = $years[0];
                            $arrTimePeriod[$tpKey]['to'] = $years[1];

                            if ($years[0] == $years[1]) {
                                // only one year
                                $receivedYears.= $years[0] . ', ';
                            } else {
                                $from = $years[0];
                                $to = $years[1];
                                $receivedYears .= $from . '-' . $to . ', ';
                            }

                        }
                    }

                    $receivedYears = trim($receivedYears, ',');
                    $arrAward[$key]->received_years = $receivedYears;

                    if ($explodeTimePeriod) {
                        $timePeriod = $arrTimePeriod;
                    }
                    $arrAward[$key]->time_period = $timePeriod;

                }
            }
        }
        return $arrAward;
    }

    /**
     * Save Data
     * @param string $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";
        $results['data'] = array();

        foreach ($postData as $data) {

            if (empty($data['award_name']) || empty($data['awarding_institution'])) {
                continue;
            }

            if (isset($data['backend_action']) && $data['backend_action'] == 'delete' && $data['id'] > 0) {
                $model = ProviderAward::model()->findByPk($data['id']);
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_AWARD";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }
                    $auditSection = 'ProviderAward #' . $providerId .' (#' . $model->award_name . ')';
                    AuditLog::create('D', $auditSection, 'provider_award', null, false);
                }
            } else {

                if ($data['id'] > 0) {
                    $model = ProviderAward::model()->findByPk($data['id']);
                    $auditAction = 'U';
                }
                if (empty($model)) {
                    $auditAction = 'C';
                    $model = new ProviderAward;
                }

                $receivedYears = $years = '';
                if (!empty($data['years'])) {
                    $years = $data['years']; // old PADM
                } elseif (!empty($data['time_period'])) {
                    $years = $data['time_period']; // from API
                }

                if (!empty($years)) {
                    foreach ($years as $values) {
                        if (!empty($values['from'])) {
                            if ($values['to'] == $values['from'] || empty($values['to'])) {
                                // only this year
                                $receivedYears.= $values['from'] . ', ';
                            } elseif ($values['to'] > $values['from']) {
                                // is a range
                                $thisRange = $values['from'] . '-' . $values['to'];
                                $receivedYears.= $thisRange . ', ';
                            }
                        }
                    }
                }
                if (!empty($receivedYears)) {
                    $receivedYears = trim($receivedYears);
                    $receivedYears = substr($receivedYears, 0, -1);
                }

                $model->provider_id = $providerId;
                $model->award_name = $data['award_name'];
                $model->awarding_institution = $data['awarding_institution'];
                $model->received_years = $receivedYears;

                if (!$model->save()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_AWARD";
                    $results['data'] = $model->getErrors();
                    return $results;
                } else {
                    // return the updated entries
                    unset($data['backend_action']);
                    $data['id'] = $model->id;
                    $results['data'][] = $data;
                }
                $auditSection = 'ProviderAward #' . $providerId .' (#' . $model->award_name . ')';
                AuditLog::create($auditAction, $auditSection, 'provider_award', null, false);

            }

            unset($model);
        }
        return $results;
    }

}
