<?php

Yii::import('application.modules.core_models.models._base.BaseTrackingActivityParameter');

class TrackingActivityParameter extends BaseTrackingActivityParameter
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
