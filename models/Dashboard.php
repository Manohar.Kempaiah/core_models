<?php

Yii::import('application.modules.core_models.models._base.BaseDashboard');

class Dashboard extends BaseDashboard
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Read IB reviews
     * @param array $params
     * @return array
     */
    public static function readReviews($params = array())
    {
        // date filter params
        $from = Common::get($params, 'date_from', 0);
        $to = Common::get($params, 'date_to', 0);

        // review's parameters
        $reviewId = Common::get($params, 'review_id', 0);
        $listingSitesIds = Common::get($params, 'listing_sites_ids', 0);
        $listingSitesIds = !empty($listingSitesIds) ? $listingSitesIds : 0;
        $readComment = Common::get($params, 'read_comment', '0,1');
        $sentimentIds = Common::get($params, 'sentiment_ids', '1,2,3,4,5');
        $themes = Common::get($params, 'themes', '8,11,15,9,13,7,6,12');
        $addResponse = Common::get($params, 'add_response', 1);

        // Pagination parameters
        $offset = Common::get($params, 'offset', null);
        $limit = Common::get($params, 'limit', null);

        // extra parameters
        $advanced = Common::get($params, 'advanced', 0);
        $keyName = Common::get($params, 'key_name', 'IB_ALL_REVIEWS');
        $minimumRating = Common::get($params, 'minimum_rating', 0);
        $providers = Common::get($params, 'providers', 0);

        // account & organization
        $accountId = Common::get($params, 'account_id', 0);
        $organizationId = Common::get($params, 'organization_id', 0);
        $useCache = Common::get($params, 'use_cache', true);

        // prepare the cache key
        $flatParams = print_r($params, true);
        $cacheKey = "reviews_widget_" . md5($flatParams);

        // Is this query cached?
        $cached = Yii::app()->cache->get($cacheKey);
        if ($useCache && $cached) {
            // output cached response
            $results = json_decode($cached, true);
            if ($offset !== null && $limit !== null) {
                $response = array(
                    'total' => (!empty($results)) ? $results[0]['cnt'] : 0,
                    'data' => $results,
                    'response_type' => 'custom',
                );
            } else {
                $response = array(
                    'data' => $results,
                    'response_type' => 'success'
                );
            }
            return $response;
        }

        // @todo change the key_name based on the partner site
        $dashboard = Yii::app()->db->createCommand("SELECT query, cache FROM dashboard WHERE key_name =:key_name")->
            bindParam(":key_name", $keyName, PDO::PARAM_STR)->queryRow();

        $query = rtrim(trim($dashboard['query']), ';');
        $cache = $dashboard['cache'];

        $to = ($to == 0) ? date("Y-m-d") : $to;
        $from = ($from == 0) ? "1990-01-01" : $from;

        $query = str_replace(':from', $from, $query);
        $query = str_replace(':to', $to, $query);

        // extra parameters
        $query = str_replace(':advanced', $advanced, $query);

        // review's parameters
        $query = str_replace(':review_id', $reviewId, $query);
        $query = str_replace(':listing_sites_ids', $listingSitesIds, $query);
        $query = str_replace(':read_comment', $readComment, $query);
        $query = str_replace(':sentiment_ids', $sentimentIds, $query);
        $query = str_replace(':themes', $themes, $query);
        $query = str_replace(':provider_filter_condition', $providers, $query);
        $query = str_replace(':rating_filter_condition', $minimumRating, $query);

        try {

            // Paginated request
            if ($offset !== null && $limit !== null) {
                $query = "WITH all_reviews AS (" . $query . ")";
                $query .= "SELECT  *,
                            max(rank)  OVER() as cnt
                            FROM (
                                SELECT
                                    *,
                                    dense_rank() OVER(ORDER BY NPI,\"Name\",\"Credentials\",\"ProviderReviewID\",
                                        \"SourceID\",\"PostedDate\",\"SourceURL\",\"Comment\",\"Author\",\"Rating\",
                                        reply_enabled,feature_review_enabled,request_removal_enabled,featured,read) AS rank
                                FROM all_reviews
                                ) t
                            ORDER BY
                                \"PostedDate\" DESC";
                $query .= " LIMIT " . $limit . " OFFSET " . $offset;
            }

            // Create the command binding
            // organization and account parameters
            $results = Yii::app()->analyticsRO->cache($cache)->createCommand($query)
                ->bindValue(':account_id', $accountId, PDO::PARAM_STR)
                ->bindValue(':organization_id', $organizationId, PDO::PARAM_STR)
                ->queryAll();

            if ($addResponse) {
                // @todo change the key_name based on the partner site
                $responses = Yii::app()->db->createCommand(
                    "SELECT query, cache FROM dashboard WHERE key_name ='IB_REVIEW_RESPONSES'"
                )->queryRow();

                $queryResponses = rtrim(trim($responses['query']), ';');
                $cacheResponses = $responses['cache'];

                foreach ($results as $key => $value) {

                    $results[$key]['responses'] =
                        Yii::app()->analyticsRO->cache($cacheResponses)->createCommand($queryResponses)
                        ->bindValue(':review_id', $value['ProviderReviewID'], PDO::PARAM_INT)
                        ->queryAll();
                }
            }

            if ($offset !== null && $limit !== null) {
                $response = array(
                    'total' => (!empty($results)) ? $results[0]['cnt'] : 0,
                    'data' => $results,
                    'response_type' => 'custom',
                );
            } else {
                $response = array(
                    'data' => $results,
                    'response_type' => 'success'
                );
            }
            // Save the the results to cache
            Yii::app()->cache->set($cacheKey, json_encode($results), 2 * Yii::app()->params['cache_long']);
        } catch (Exception $e) {
            // return the exception message
            $exceptionMessage = $e->getMessage();
            $response = array(
                'error' => 'DB_ERROR',
                'data' => $exceptionMessage,
                'response_type' => 'error'
            );
        }
        return $response;
    }
}
