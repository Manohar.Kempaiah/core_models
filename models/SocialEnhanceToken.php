<?php

Yii::import('application.modules.core_models.models._base.BaseSocialEnhanceToken');

class SocialEnhanceToken extends BaseSocialEnhanceToken
{

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Actions to take before deleting this record
     */
    public function beforeDelete()
    {

        // Delete all ppse linked to this socialEnhanceToken
        $arrPpse = ProviderPracticeSocialEnhance::model()->findAll(
            'social_enhance_token_id=:social_token_id',
            array(':social_token_id' => $this->id)
        );
        if (!empty($arrPpse)) {
            foreach ($arrPpse as $ppse) {
                if (!$ppse->delete()) {
                    $this->addError('id', 'Couldn\'t delete providerPracticeSocialEnhance ID:' . $ppse->id);
                    return false;
                }
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Try to delete unused token
     *
     * @param int $profileId
     * @return void
     */
    public static function getProfileName($profileId = 0)
    {
        if ($profileId == 0) {
            return '';
        }

        $socialEnhanceToken = SocialEnhanceToken::model()->find(
            'profile_id=:profile_id',
            array(':profile_id' => $profileId)
        );
        if (!empty($socialEnhanceToken)) {
            // Yes exists
            return $socialEnhanceToken->profile_name;
        }

        return 'Facebook page_id: ' . $profileId;
    }

    /**
     * Try to delete unused token
     *
     * @param int $socialEnhanceTokenId
     * @return void
     */
    public static function tryToDelete($socialEnhanceTokenId = 0)
    {
        if ($socialEnhanceTokenId == 0) {
            return true;
        }

        // Exists social_link attached to this profile?
        $providerPracticeSocialEnhance = ProviderPracticeSocialEnhance::model()->find(
            'social_enhance_token_id=:social_enhance_token_id',
            array(
                ':social_enhance_token_id' => $socialEnhanceTokenId
            )
        );
        if (!empty($providerPracticeSocialEnhance)) {
            // Yes exists
            return true;
        }

        // If not exists, we try to delete the old social_enhance record
        $socialEnhanceToken = SocialEnhanceToken::model()->find('id=:id', array(':id' => $socialEnhanceTokenId));
        if (!empty($socialEnhanceToken)) {
            // Yes exists
            if ($socialEnhanceToken->delete()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Delete expired tokens
     *
     * @param int $listingTypeId - 16:Facebook | 39:Twitter
     * @param bool $debug - if true, write the debugging
     * @return void
     */
    public static function deleteExpiredToken($listingTypeId = 16, $debug = false)
    {

        if ($listingTypeId == 16) {
            // Facebook
            $condition = "fb_token_expiration_date IS NOT NULL AND fb_token_expiration_date < NOW()";
        } elseif ($listingTypeId == 39) {
            // Twitter
            $condition = "auth_expire > 0 AND auth_expire < NOW()";
        } else {
            // Other, no condition defined yet
            Yii::log(
                "Error while deleting listingTypeID: " . $listingTypeId,
                'error',
                'system.commands.reviewscommand'
            );
            return false;
        }

        $arrExpiredToken = SocialEnhanceToken::model()->findAll(
            "listing_type_id=:listing_type_id AND ( :condition )",
            array(
                ":listing_type_id" => $listingTypeId,
                ":condition" => $condition
            )
        );
        if (!empty($arrExpiredToken)) {

            if ($debug) {
                echo PHP_EOL . 'Delete expired Facebook tokens: ' . PHP_EOL;
            }

            foreach ($arrExpiredToken as $et) {
                if (!$et->delete()) {
                    Yii::log(
                        "Error while deleting provider_social_enhance: " . serialize($et->getErrors()),
                        'error',
                        'system.commands.reviewscommand'
                    );
                } else {
                    // save to our audit log
                    AuditLog::create(
                        'D',
                        Yii::app()->controller->id . '/' . Yii::app()->controller->action->id,
                        'social_enhance_token',
                        'Delete expired Facebook token - Profile Name: ' . $et->profile_name . ' (' . $et->profile_id . ')',
                        false
                    );

                    if ($debug) {
                        echo "Profile Name: " . $et->profile_name . ' (' . $et->profile_id . ')';
                        echo '----------------------------' . PHP_EOL;
                    }
                }
            }
        }
    }

}
