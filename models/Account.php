<?php

use application\components\API\Auth\AccessToken;

Yii::import('application.modules.core_models.models._base.BaseAccount');

class Account extends BaseAccount
{
    // defined here so we don't depend on the internal ids all over the place
    const PATIENT_ACCOUNT_ID = 2;

    // passwords are stored in Account->secret - we use the password variable when a user submits a change
    public $password;
    public $password_confirm;
    public $accepted_terms_of_use;

    /**
     * @var int value of provider_id to be published
     */
    public $publishProvider = 0;

    /**
     * @var int value of practice_id to be published
     */
    public $publishPractice = 0;

    /**
     * @var bool value to do research() anyway
     */
    public $forceResearchMode = false;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules[] = array('password', 'required', 'on' => 'insert');
        $rules[] = array('password', 'required', 'on' => 'recoverPassword, register', 'message'
        => 'Please enter your password');
        $rules[] = array('password_confirm', 'required', 'on' => 'insert, recoverPassword, register', 'message'
        => 'Please confirm your password');
        $rules[] = array('accepted_terms_of_use', 'required', 'on' => 'register', 'message'
        => 'You have to accept the Terms of Use to continue');
        $rules[] = array(
            'account_type_id, position_id, is_beta_account, enabled, premium_explored', 'numerical',
            'integerOnly' => true, 'message' => 'Please choose one'
        );
        $rules[] = array('location_id', 'validateZipcode', 'on' => 'register, create_account, myaccount');
        $rules[] = array('email', 'unique', 'message' => 'Email "{value}" has already been taken. '
            . '<span style="color: black;">Is this you?</span> <a href="' . Yii::app()->createUrl('site/login')
            . '?ref_email={value}">Click here to log in</a>');
        $rules[] = array('password', 'length', 'min' => self::$passwordMinimumLength);
        $rules[] = array('organization_name', 'required');
        // The following rule is used by search().
        $rules[] = array('id, partner_site_id, account_type_id, location_id, is_beta_account, position_id, email, '
            . 'password, first_name, last_name, enabled, premium_explored, has_review_kiosk_program, reward_order_id, '
            . 'date_reward_sent, date_added, date_updated, organization_name, address, address_2, phone_number, '
            . 'fax_number, contacted, follow_up, email_cc, campaign, note, country_id', 'safe', 'on' => 'search');

        return $rules;
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        $relations = parent::relations();

        // don't use HAS_MANY
        $relations['provider'] = array(self::MANY_MANY, 'Provider', 'account_provider(account_id, provider_id)');
        // don't use HAS_MANY
        $relations['practices'] = array(self::MANY_MANY, 'Practice', 'account_practice(account_id, practice_id)');
        $relations['organizationAccount'] = array(self::HAS_ONE, 'OrganizationAccount', 'account_id');
        $relations['organization'] = array(
            self::HAS_ONE,
            'Organization',
            array('organization_id' => 'id'),
            'through' => 'organizationAccount'
        );

        $relations['country'] = array(self::BELONGS_TO, 'Country', 'country_id');

        return $relations;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will
     * filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // valid date reward sent
        if (!is_null($this->date_reward_sent)) {
            $isValidDate = StringComponent::validDate($this->date_reward_sent, 'Y-m-d');
            if (!$isValidDate) {
                $this->date_reward_sent = null;
            }
        }

        $criteria = new CDbCriteria;
        //this is to enable eager loading
        //this way we perform 5 queries instead of 105 (!) per page
        $criteria->with = array('accountType');
        $criteria->compare('id', $this->id);
        $criteria->compare('partner_site_id', $this->partner_site_id);
        $criteria->compare('account_type_id', $this->account_type_id);
        $criteria->compare('location_id', $this->location_id);
        $criteria->compare('position_id', $this->position_id);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('is_beta_account', $this->is_beta_account);
        $criteria->compare('enabled', $this->enabled);
        $criteria->compare('premium_explored', $this->premium_explored);
        $criteria->compare('salesforce_lead_id', $this->salesforce_lead_id, true);
        $criteria->compare('has_review_kiosk_program', $this->has_review_kiosk_program);
        $criteria->compare('reward_order_id', $this->reward_order_id, true);
        $criteria->compare('date_reward_sent', $this->date_reward_sent, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('organization_name', $this->organization_name, true);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('address_2', $this->address_2, true);
        $criteria->compare('phone_number', $this->phone_number, true);
        $criteria->compare('fax_number', $this->fax_number, true);
        $criteria->compare('contacted', $this->contacted);
        $criteria->compare('follow_up', $this->follow_up);
        $criteria->compare('email_cc', $this->email_cc, true);
        $criteria->compare('campaign', $this->campaign, true);
        $criteria->compare('utm_variables', $this->utm_variables, true);
        $criteria->compare('note', $this->note, true);
        return new CActiveDataProvider(get_class($this), array('criteria' =>
        $criteria, 'pagination' => array('pageSize' => 100)));
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'partner_site_id' => 'Partner Site',
            'account_type_id' => 'Account Type',
            'location_id' => 'Location',
            'position_id' => 'Position in Practice',
            'email' => 'Email',
            'password' => 'Password',
            'secret' => 'Password',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'is_beta_account' => 'Is Beta Account',
            'enabled' => 'Enabled',
            'premium_explored' => 'Premium Svcs Explored',
            'research_mode' => 'Research mode',
            'research_date_populated' => 'Research Date Populated',
            'research_date_initialization' => 'Research Date Initialization',
            'salesforce_lead_id' => Yii::t('app', 'Salesforce Lead'),
            'has_review_kiosk_program' => 'Has Review Kiosk Program',
            'reward_order_id' => 'Starbuck Gift Card Order ID',
            'date_reward_sent' => 'Date Reward Sent',
            'date_added' => 'Date Added',
            'date_updated' => 'Date Updated',
            'organization_name' => Yii::t('app', 'Organization Name'),
            'address' => 'Address',
            'address_2' => 'Address Line #2',
            'phone_number' => 'Phone Number',
            'fax_number' => 'Fax Number',
            'contacted' => 'Contacted',
            'follow_up' => 'Follow Up Needed',
            'email_cc' => Yii::t('app', 'CC Emails To'),
            'campaign' => 'Campaign',
            'utm_variables' => 'UTM-Variables',
            'note' => Yii::t('app', 'Note'),
            'country_id' => Yii::t('app', 'Country'),
        );
    }

    /**
     * Model
     * @param object $className
     * @return object
     */
    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return array
     */
    public static function representingColumn()
    {
        return array('first_name', 'last_name');
    }

    /**
     * Label
     * @param int $n
     * @return str
     */
    public static function label($n = 1)
    {
        return Yii::t('app', 'Account|Accounts', $n);
    }

    /**
     * Validate data beforeSave()
     * @return boolean
     */
    public function beforeSave()
    {
        // not is new record?
        if (!$this->isNewRecord) {
            // did the value for the field is_beta_account_change, and was it a change made by a human?
            if ($this->is_beta_account != $this->oldRecord->is_beta_account) {
                // Update beta account field in Salesforce
                $currentOrg = $this->getOwnedOrganization();
                if (!empty($currentOrg)) {

                    // update salesforce
                    $sfData = array(
                        'salesforce_id' => $currentOrg->salesforce_id,
                        'field' => 'Is_Beta_Account__c',
                        'value' => (bool) $this->is_beta_account
                    );
                    SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);
                }
            }
            if (empty($this->date_signed_in)) {
                $this->date_signed_in = (!empty($this->date_added) ? $this->date_added : date('Y-m-d H:i:s'));
            }
        } else {

            // Is a new record
            $this->date_signed_in = null;
        }

        // strip tags and trim all fields that may need it
        $this->first_name = strip_tags(trim($this->first_name));
        $this->last_name = strip_tags(trim($this->last_name));
        $this->email = strip_tags(trim($this->email));

        $this->phone_number = preg_replace('/[^0-9]/', '', $this->phone_number);
        $this->fax_number = preg_replace('/[^0-9]/', '', $this->fax_number);

        if ($this->phone_number == '' || $this->phone_number == 'Unknown') {
            $this->phone_number = null;
        }
        if ($this->fax_number == '' || $this->fax_number == 'Unknown') {
            $this->fax_number = null;
        }
        if (empty($this->partner_site_id)) {
            $this->partner_site_id = 1; // Doctor by default
        }

        // if has email_cc, validate them: email_cc could be an array with multiple addresses
        if (isset($this->email_cc) && !empty($this->email_cc)) {

            $emailList = explode(';', $this->email_cc);
            if (!empty($emailList)) {
                $this->email_cc = '';
                foreach ($emailList as $key => $emailCc) {
                    if (!empty($emailCc)) {
                        $emailCc = StringComponent::validateEmail($emailCc);
                        if (empty($emailCc)) {
                            unset($emailList[$key]);
                            $this->addError('email_cc', 'Incorrect CC EMail Format.');
                        } else {
                            // update with correct email
                            $emailList[$key] = $emailCc;
                        }
                    } else {
                        unset($emailList[$key]);
                    }
                }
                if (!empty($emailList)) {
                    $this->email_cc = implode('; ', $emailList);
                }
            }
        }
        // new position?
        if ($this->position_id == 0) {

            $position_id_save = Yii::app()->request->getPost('position_id_save');

            if (!empty($position_id_save)) {

                //check if already existed
                $arrPosition = Position::model()->find('name=:name', array(':name' => $position_id_save));
                if (!empty($arrPosition)) {
                    $position_id = $arrPosition->id;
                    $this->position_id = $position_id;

                    return parent::beforeSave();
                }

                $position = new Position;
                $position->name = $position_id_save;
                $position->save();
                $this->position_id = $position->id;
                return parent::beforeSave();
            }

            return false;
        }
        // validate account type and email
        if ($this->account_type_id == 1) {
            if (
                !strpos($this->email, '@pressganey.com') && !strpos($this->email, '@corp.doctor.com')
                && !strpos($this->email, '@connectcorp.com')
            ) {
                $this->addError('email', 'Incorrect Email. Only doctor.com/pressganey.com accounts.');
                return false;
            }
        }

        // If is a new account and it is free
        if ($this->isNewRecord && $this->account_type_id == 7 && $this->getScenario() != "noLeadRequired") {

            // Create the Salesforce Lead object
            $leadData = array(
                "FirstName" => $this->first_name,
                "LastName" => $this->last_name,
                "Company" => (trim($this->organization_name) != '' ? $this->organization_name : '[Not Provided]'),
                "Phone" => $this->phone_number,
                "Email" => $this->email,
                "LeadSource" => "Doctor.com Account",
                "Lead_Source__c" => (isset($this->partnerSite) ? $this->partnerSite->display_name : ""),
                "Doctor_com_Account_Id__c" => $this->id
            );

            $salesforceLead = Yii::app()->salesforce->createLead($leadData, true, ['DUPLICATES_DETECTED']);

            if (!empty($salesforceLead["id"])) {
                $this->salesforce_lead_id = $salesforceLead["id"];
            }
        }

        return parent::beforeSave();
    }

    /**
     * Clean up data before validating
     * @return boolean
     */
    public function beforeValidate()
    {

        // make sure we clean up null values
        if ($this->date_signed_in == '0000-00-00 00:00:00') {
            $this->date_signed_in = null;
        }
        return parent::beforeValidate();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     * @important The order in which we delete related models is important in order to avoid foreign key errors.
     * Please take it into account if you add to, delete from, or modify below:
     * CampaignEmail
     * EmrScheduleException by account_id
     * AccountPracticeEmr
     * AccountProviderEmr
     * AccountEmr
     * AccountPractice
     * AccountProvider
     * ManualValidation
     * AccountDetails
     * AuditLog by account_id
     * AuditLog by admin_account_id
     * VisitReason
     * ReviewRequest
     * Widget
     * ProviderPreviousPractice
     */
    public function beforeDelete()
    {

        // avoid deleting from account table if associated records exist in account_device table
        $has_associated_records = AccountDevice::model()->exists(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError('id', 'This account cannot be deleted because a device is attached to it.');
            return false;
        }

        // avoid deleting from account table if associated records exist in waiting_room_setting table
        $has_associated_records = WaitingRoomSetting::model()->exists(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError('id', 'This account cannot be deleted because a WaitingRoom is attached to it.');
            return false;
        }

        // avoid deleting from account table if associated records exist in organization_account table
        $has_associated_records = OrganizationAccount::model()->exists(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError('id', 'This account cannot be deleted because an organization is attached to it.');
            return false;
        }

        // avoid deleting from account table if associated records exist in organization_history table
        $has_associated_records = OrganizationHistory::model()->exists(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError(
                'id',
                'This account cannot be deleted because an organization\'s history is attached to it.'
            );
            return false;
        }

        // avoid deleting from account table if associated records exist in cpn_installation table
        $has_associated_records = CpnInstallation::model()->exists(
            'account_id=:account_id OR added_by_account_id=:added_by_account_id',
            array(':account_id' => $this->id, ':added_by_account_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError(
                'id',
                'This account has a Companion installation.'
            );
            return false;
        }

        // delete records from campaign_email before deleting this account
        $arrCampaignEmail = CampaignEmail::model()->findAll(
            'account_id=:accountId',
            array(':accountId' => $this->id)
        );
        foreach ($arrCampaignEmail as $campaignEmail) {
            if (!$campaignEmail->delete()) {
                $this->addError(
                    'id',
                    'This account cannot be deleted because it has Campaign Email data associated to it.'
                );
                return false;
            }
        }

        // delete records from account_contact before deleting this account
        $arrAccountContact = AccountContact::model()->findAll(
            'account_id=:accountId',
            array(':accountId' => $this->id)
        );
        foreach ($arrAccountContact as $accountContact) {
            if (!$accountContact->delete()) {
                $this->addError(
                    'id',
                    'This account cannot be deleted because it has AccountContact data associated to it.'
                );
                return false;
            }
        }

        // delete records from account_practice_emr before deleting this account
        $arrAccountPracticeEmr = AccountPracticeEmr::model()->findAll(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );
        foreach ($arrAccountPracticeEmr as $accountPracticeEmr) {
            if (!$accountPracticeEmr->delete()) {
                $this->addError(
                    'id',
                    'This account cannot be deleted because a practice\'s EMR data is attached to it.'
                );
                return false;
            }
        }

        // delete records from account_provider_emr before deleting this account
        $arrAccountProviderEmr = AccountProviderEmr::model()->findAll(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );
        foreach ($arrAccountProviderEmr as $accountProviderEmr) {
            if (!$accountProviderEmr->delete()) {
                $this->addError(
                    'id',
                    'This account cannot be deleted because a provider\'s EMR data is attached to it.'
                );
                return false;
            }
        }

        // delete records from account_emr before deleting this account
        $arrAccountEmr = AccountEmr::model()->findAll('account_id=:accountId', array(':accountId' => $this->id));
        foreach ($arrAccountEmr as $accountEmr) {
            if (!$accountEmr->delete()) {
                $this->addError('id', 'This account cannot be deleted because it has EMR data associated to it.');
                return false;
            }
        }

        // delete records from emr_schedule_exception before deleting this account
        $eSE = EmrScheduleException::deleteByAccountId($this->id);
        if (!$eSE) {
            $this->addError('id', 'This account cannot be deleted because it has EMR data associated to it.');
            return false;
        }

        // delete records from account_practice before deleting this account
        $arrAccountPractice = AccountPractice::model()->findAll(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );
        foreach ($arrAccountPractice as $accountPractice) {
            if (!$accountPractice->delete()) {
                $this->addError('id', 'This account cannot be deleted because a practice is attached to it.');
                return false;
            }
        }

        // delete records from account_provider before deleting this account
        $arrAccountProvider = AccountProvider::model()->findAll(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );
        foreach ($arrAccountProvider as $accountProvider) {
            if (!$accountProvider->delete()) {
                $this->addError('id', 'This account cannot be deleted because a provider is attached to it.');
                return false;
            }
        }

        // delete records from account_privileges before deleting this account
        $arrAccountPrivileges = AccountPrivileges::model()->findAll(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );
        foreach ($arrAccountPrivileges as $accountPrivileges) {
            if (!$accountPrivileges->delete()) {
                $this->addError('id', 'This account cannot be deleted because a privilege is attached to it.');
                return false;
            }
        }

        // delete records from manual_validation before deleting this account
        $arrManualValidation = ManualValidation::model()->findAll(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );
        foreach ($arrManualValidation as $manualValidation) {
            if (!$manualValidation->delete()) {
                $this->addError('id', 'This account cannot be deleted because it has requested a manual validation.');
                return false;
            }
        }

        // delete account details before deleting account
        $accountDetailsCondition = sprintf(" account_id = %s ", $this->id);
        $hasDetails = AccountDetails::model()->exists($accountDetailsCondition);
        if ($hasDetails) {
            $accountDetails = AccountDetails::model()->findAll($accountDetailsCondition);
            foreach ($accountDetails as $accountDetail) {
                if (!$accountDetail->delete()) {
                    $this->addError(
                        'id',
                        'This account cannot be deleted because it has account details attached to it'
                    );
                    return false;
                }
            }
        }

        // delete records from audit_log before deleting this account
        $arrAuditLog = AuditLog::model()->findAll('account_id=:accountId', array(':accountId' => $this->id));
        foreach ($arrAuditLog as $auditLog) {
            if (!$auditLog->delete()) {
                $this->addError('id', 'This account cannot be deleted because an audit log is attached to it.');
                return false;
            }
        }

        // delete records from audit_log before deleting this account
        $arrAuditLog = AuditLog::model()->findAll('admin_account_id=:accountId', array(':accountId' => $this->id));
        foreach ($arrAuditLog as $auditLog) {
            if (!$auditLog->delete()) {
                $this->addError('id', 'This account cannot be deleted because there are audit logs attached to it.');
                return false;
            }
        }

        // delete records from visit_reason before deleting this account
        $arrVR = VisitReason::model()->findAll('account_id=:accountId', array(':accountId' => $this->id));
        foreach ($arrVR as $vr) {
            if (!$vr->delete()) {
                $this->addError('id', 'This account cannot be deleted because it has visit reasons attached to it.');
                return false;
            }
        }

        // delete records from review_request before deleting this account
        $arrRR = ReviewRequest::model()->findAll('account_id=:accountId', array(':accountId' => $this->id));
        foreach ($arrRR as $rr) {
            if (!$rr->delete()) {
                $this->addError(
                    'id',
                    'This account cannot be deleted because it has Patient Messaging attached to it.'
                );
                return false;
            }
        }

        // delete records from widget before deleting this account
        $arrW = Widget::model()->findAll('account_id=:accountId', array(':accountId' => $this->id));
        foreach ($arrW as $w) {
            if (!$w->delete()) {
                $this->addError('id', 'This account cannot be deleted because a widget is attached to it.');
                return false;
            }
        }

        // delete records from provider_previous_practice before deleting this account
        $arrPPP = ProviderPreviousPractice::model()->findAll('account_id=:accountId', array(':accountId' => $this->id));
        foreach ($arrPPP as $ppp) {
            if (!$ppp->delete()) {
                $this->addError(
                    "id",
                    "This account cannot be deleted because a provider's previous practice is attached to it."
                );
                return false;
            }
        }

        // delete records from patientemail before deleting this account
        $arrP = Patientemail::model()->findAll('account_id=:accountId', array(':accountId' => $this->id));
        foreach ($arrP as $p) {
            if (!$p->delete()) {
                $this->addError("id", "This account cannot be deleted because it is linked to an Automatic RR.");
                return false;
            }
        }

        // delete records from scheduling_mail before deleting this account
        $arrSchedulingMail = SchedulingMail::model()->findAll(
            'updated_by_account_id=:accountId',
            array(':accountId' => $this->id)
        );
        foreach ($arrSchedulingMail as $schedulingMail) {
            if (!$schedulingMail->delete()) {
                $this->addError(
                    "id",
                    "This account cannot be deleted because a scheduling mail is attached to it."
                );
                return false;
            }
        }

        // delete records from scheduling_mail_comment before deleting this account
        $arrSchedulingMailComment = SchedulingMailComment::model()->findAll(
            'account_id=:accountId',
            array(':accountId' => $this->id)
        );
        foreach ($arrSchedulingMailComment as $schedulingMailComment) {
            if (!$schedulingMailComment->delete()) {
                $this->addError(
                    "id",
                    "This account cannot be deleted because a scheduling mail comment is attached to it."
                );
                return false;
            }
        }

        // Check if exists records in account_practice_emr with practice_id > 0
        $arrEmr = AccountPracticeEmr::model()->findAll(
            'account_id = :accountId AND practice_id > 0',
            array(':accountId' => $this->id)
        );
        if (!empty($arrEmr)) {
            $this->addError(
                'id',
                'This account cannot be deleted because account_practice_emr data is attached to it.'
            );
            return false;
        }

        // delete records from provider_practice_gmb before deleting this account
        $arrGMB = ProviderPracticeGmb::model()->findAll(
            'account_id = :accountId OR added_by_account_id = :addedByAccountId'
                . ' OR updated_by_account_id = :updatedByAccountId',
            array(':accountId' => $this->id, ':addedByAccountId' => $this->id, ':updatedByAccountId' => $this->id)
        );
        foreach ($arrGMB as $gmb) {
            if (!$gmb->delete()) {
                $this->addError('id', 'This account cannot be deleted because GMB data is attached to it.');
                return false;
            }
        }

        // delete records from account_login before deleting this account
        $arrLogins = AccountLogin::model()->findAll('account_id = :accountId', array(':accountId' => $this->id));
        if (!empty($arrLogins)) {
            foreach ($arrLogins as $login) {
                if (!$login->delete()) {
                    $this->addError('id', 'This account cannot be deleted because login data is attached to it.');
                    return false;
                }
            }
        }

        // delete records from account_practice_emr before deleting this account
        $arrEmr = AccountPracticeEmr::model()->findAll(
            'account_id = :accountId AND practice_id IS NULL',
            array(':accountId' => $this->id)
        );
        if (!empty($arrEmr)) {
            foreach ($arrEmr as $emr) {
                if (!$emr->delete()) {
                    $this->addError(
                        'id',
                        'This account cannot be deleted because account_practice_emr data is attached to it.'
                    );
                    return false;
                }
            }
        }

        // delete records from listing_site_account_stats before deleting this account
        $recordsToBeDeleted = ListingSiteAccountStats::model()->findAll(
            'account_id = :accountId',
            array(':accountId' => $this->id)
        );
        if (!empty($recordsToBeDeleted)) {
            foreach ($recordsToBeDeleted as $rTb) {
                if (!$rTb->delete()) {
                    $this->addError(
                        'id',
                        'This account cannot be deleted because listing_site_account_stats data is attached to it.'
                    );
                    return false;
                }
            }
        }

        // delete records from listing_site_practice_stats before deleting this account
        $recordsToBeDeleted = ListingSitePracticeStats::model()->findAll(
            'account_id = :accountId',
            array(':accountId' => $this->id)
        );
        if (!empty($recordsToBeDeleted)) {
            foreach ($recordsToBeDeleted as $rTb) {
                if (!$rTb->delete()) {
                    $this->addError(
                        'id',
                        'This account cannot be deleted because listing_site_practice_stats data is attached to it.'
                    );
                    return false;
                }
            }
        }

        // delete records from listing_site_provider_stats before deleting this account
        $recordsToBeDeleted = ListingSiteProviderStats::model()->findAll(
            'account_id = :accountId',
            array(':accountId' => $this->id)
        );
        if (!empty($recordsToBeDeleted)) {
            foreach ($recordsToBeDeleted as $rTb) {
                if (!$rTb->delete()) {
                    $this->addError(
                        'id',
                        'This account cannot be deleted because listing_site_provider_stats data is attached to it.'
                    );
                    return false;
                }
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Return account record by ID in Read Only DB
     * @param int $accountId
     * @param bool $useCache
     * @return array
     */
    public static function getByIdRO($accountId = 0, $useCache = true)
    {
        if ($accountId == 0) {
            return null;
        }

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;

        $sql = sprintf(
            "SELECT
            `id`, `localization_id`, `country_id`, `partner_site_id`, `account_type_id`, `location_id`,
            `position_id`, `email`, `secret`, `first_name`, `last_name`, `enabled`, `verified_fremium_account`,
            `is_beta_account`, `premium_explored`, `research_mode`, `research_date_initialization`,
            `research_date_populated`, `salesforce_lead_id`, `has_review_kiosk_program`,
            `reward_order_id`, `date_reward_sent`, `date_signed_in`, `date_added`, `date_updated`,
            `organization_name`, `address`, `address_2`, `phone_number`, `fax_number`,
            `contacted`, `follow_up`, `email_cc`, `campaign`, `note`, `utm_variables`
            FROM `account`
            WHERE `id` = %d
            LIMIT 1;",
            $accountId
        );
        return Yii::app()->dbRO->cache($cacheTime)->createCommand($sql)->queryRow();
    }

    /**
     * Get the account owner's first and last name
     * @return string
     */
    public function getAccountName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf("SELECT id FROM account WHERE account.id = %d ORDER BY id = %d DESC;", $recordId, $accountId);
    }

    /**
     * This function validate if the location_id exist.
     * This function is generally used for validation of both the zipcode of Create My Account as the Account.
     * In the form, this field have zipCode, but before save we need to convert to location_id
     */
    public function validateZipcode()
    {
        if (empty($this->location_id)) {
            $this->addError('location_id', 'Zipcode cannot be blank.');
        } else {
            $arrLocation = Location::model()->find('zipcode =:zipcode', array(':zipcode' => $this->location_id));
            if (empty($arrLocation)) {
                $this->location_id = '';
                $this->addError('location_id', 'Please enter a valid zipcode');
            } else {
                $this->location_id = $arrLocation->id;
            }
        }
    }

    /**
     * Get an account full name (first, last, email)
     * @return string
     */
    public function getFullAccount()
    {

        return addslashes($this->first_name . ' ' . $this->last_name . ' - ' . $this->email);
    }

    /**
     * Get the time zone an account is in, given its phone number
     * @return string
     */
    public function getTimezone()
    {
        $number = $this->phone_number;
        $number = str_replace('(', '', $number);
        $number = str_replace(')', '', $number);
        $number = str_replace('-', '', $number);
        $number = str_replace(' ', '', $number);
        $number = trim($number);
        $number = substr($number, 0, 3);
        $tz = Timezone::model()->cache(Yii::app()->params['cache_long'])->find(
            'area_code = :area_code',
            array(':area_code' => $number)
        );
        return $tz ? $tz->time_zone : 'n/a';
    }

    /**
     * Get account's related to a practice and provider
     * @param int $providerId
     * @param int $practiceId
     * @param bool $getOwner
     * @return array
     */
    public static function getAccountBasedOnProviderAndPractice($providerId = 0, $practiceId = 0, $getOwner = false)
    {

        if ($providerId == 0 || $practiceId == 0) {
            return [];
        }

        $sql = sprintf(
            "SELECT account.id AS account_id, account.email, account.email_cc,
            account.`first_name`, account.`last_name`,
            organization_account.organization_id, organization_account.connection
            FROM account
            INNER JOIN account_provider ON account_provider.account_id = account.id
            INNER JOIN account_practice ON account_practice.account_id = account.id
            LEFT JOIN organization_account ON organization_account.account_id = account.id
            WHERE account_provider.provider_id = %d
                AND account_practice.practice_id = %d",
            $providerId,
            $practiceId
        );
        $result = Yii::app()->db->createCommand($sql)->queryAll();

        if (!empty($result) && $getOwner) {
            foreach ($result as $r) {
                if ($r['connection'] == 'O') {
                    return $r['account_id'];
                }
            }
            // If no owners, return null
            // magic account_id = 1:
            // default to tech@corp.doctor.com for command-line processes that don't have a user id
            return (php_sapi_name() != "cli" && !empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
        }

        return $result;
    }

    /**
     * Add and process the account_contacts emails
     * @param array $arrAuthorizedRecipients
     * @param string $ownerAccountId comma separated list
     *
     * @return array
     */
    public static function getProcessedEmailRecipients(
        $arrAuthorizedRecipients = array(),
        $ownerAccountId = '',
        $field = 'alert_app_request'
    ) {
        if (empty($arrAuthorizedRecipients) || empty($ownerAccountId)) {
            return [];
        }

        // Do we have to add an account_contact emails?
        $sql = sprintf(
            "SELECT name,email, `%s` as `field`, active
            FROM account_contact
            WHERE account_id in (%s)
                AND email != '';",
            $field,
            $ownerAccountId
        );
        $accountNotifications = Yii::app()->db->createCommand($sql)->queryAll();

        $lstEmailNotifications = '';
        if (!empty($accountNotifications)) {
            // Process duplicated and unsolicited email
            foreach ($arrAuthorizedRecipients as $k => $v) {
                $email = $v['email'];

                // This email exists in account_notifications?
                foreach ($accountNotifications as $k1 => $v1) {
                    if ($v1['email'] == $email) {
                        // Yes, exists
                        if ($v1['field'] == 'EMAIL') {
                            // Is duplicated, so I will remove it from contacts
                            unset($accountNotifications[$k1]);
                        } else {
                            // Exists but they dont want to receive email
                            // I'll have to remove from both arrays
                            unset($accountNotifications[$k1]);
                            $arrAuthorizedRecipients[$k]['email'] = 'no-reply';
                        }
                    }
                }
            }

            // Remove unselect contacts
            foreach ($accountNotifications as $k1 => $v1) {
                // Yes, exists
                if ($v1['field'] != 'EMAIL') {
                    unset($accountNotifications[$k1]);
                }
            }

            // Make a list of the finals account_notifications emails
            foreach ($accountNotifications as $v) {
                $lstEmailNotifications .= $v['email'] . ',';
            }
            // we dont needed anymore
            unset($accountNotifications);
            $lstEmailNotifications = rtrim($lstEmailNotifications, ',');
        }

        foreach ($arrAuthorizedRecipients as $k => $v) {
            $arrAuthorizedRecipients[$k]['email_bcc'] = '';
            if ($v['connection'] == 'O') {
                // Add account_contacts as email_bcc
                $arrAuthorizedRecipients[$k]['email_bcc'] = $lstEmailNotifications;
                $lstEmailNotifications = '';
            }
        }

        return $arrAuthorizedRecipients;
    }

    /**
     * Get the number of providers and practices linked to an account, to display in the admin grid
     * @return string
     */
    public function getProvidersPractices()
    {
        $provider = AccountProvider::model()->cache(Yii::app()->params['cache_short'])->count(
            'account_id=:account_id',
            array(':account_id' => $this->id)
        );

        $practice = Yii::app()->db->createCommand()
            ->select('COUNT(account_practice.account_id)')
            ->from('account_practice')
            ->join('practice', 'practice.id = account_practice.practice_id')
            ->join('practice_details', 'practice_details.practice_id = practice.id')
            ->where('account_id = :account_id AND practice_type_id != 5', array(':account_id' => $this->id))
            ->queryScalar();

        return ('<a class="providerTooltip" href="/administration/accountProviderTooltip?id=' . $this->id .
            '" rel="/administration/accountProviderTooltip?id=' . $this->id . '" title="' . $this->first_name . ' ' .
            $this->last_name . ' Providers">' . $provider .
            '</a> / <a class="providerTooltip" href="/administration/accountPracticeTooltip?id=' . $this->id .
            '" rel="/administration/accountPracticeTooltip?id=' . $this->id . '" title="' . $this->first_name . ' ' .
            $this->last_name . ' Practices">' . $practice . '</a>');
    }

    /**
     * Return localization code for a give account
     * @param int $accountId
     * @return string $localizationCode
     */
    public static function getLocalizationCode($accountId = 0)
    {
        if ($accountId == 0) {
            return 'en';
        }

        $sql = sprintf(
            "SELECT l.code FROM account AS a LEFT JOIN localization AS l "
                . "ON a.localization_id = l.id WHERE a.id = %d LIMIT 1;",
            $accountId
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get whether an account has pending maunal validations, to show in the admin grid
     * @return string
     */
    public function getHasManualValidation()
    {
        $mv = ManualValidation::model()->cache(Yii::app()->params['cache_long'])->find(
            'account_id = :account_id AND status = "P"',
            array(':account_id' => $this->id)
        );
        return $mv ? $mv->id : 0;
    }

    /**
     * Get whether an account is featured (obsolete!), to show in the admin grid
     * @return string
     */
    public function getHasFeatured()
    {
        $mv = Account::model()->cache(Yii::app()->params['cache_long'])->with('provider')->find(
            'account_id = :account_id AND featured = 1',
            array(':account_id' => $this->id)
        );
        return $mv ? 'Yes' : 'No';
    }

    /**
     * Get whether an account has been contacted, to show in the admin grid
     * @return string
     */
    public function getIsContacted()
    {
        if ($this->contacted) {
            return '<div id="contacted-' . $this->id .
                '" class="checkButtonContainer"><a href="javascript:void(0)" onclick="contactOff( this, ' . $this->id .
                ');" class="chkOnS"></a><div>';
        }
        return '<div id="contacted-' . $this->id .
            '" class="checkButtonContainer"><a href="javascript:void(0)" onclick="contactOn( this, ' . $this->id .
            ');" class="chkOffS"></a><div>';
    }

    /**
     * Get whether an account needs follow up, to show in the admin grid
     * @return string
     */
    public function getIsFollowUp()
    {
        if ($this->follow_up) {
            return '<div id="follow_up-' . $this->id .
                '" class="checkButtonContainer"><a href="javascript:void(0)" onclick="followUpOff( this, ' . $this->id .
                ');" class="chkOnS"></a><div>';
        }
        return '<div id="follow_up-' . $this->id .
            '" class="checkButtonContainer"><a href="javascript:void(0)" onclick="followUpOn( this, ' . $this->id .
            ');" class="chkOffS"></a><div>';
    }

    /**
     * Auto-complete an email address with addresses from the account table
     * @param string $email_address
     * @param int $limit
     * @return array
     */
    public function autocomplete($email_address, $limit = 10)
    {

        $email_address = trim($email_address);

        $sql = "SELECT email
            FROM account
            WHERE email LIKE '" . $email_address . "%'
            ORDER BY email
            LIMIT " . $limit . ";";

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the number of profile views for all providers in an account between two dates
     * @param int $accountId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function getProfileViews($accountId, $dateFrom, $dateTo)
    {
        $sql = sprintf(
            "SELECT SUM(profile_views) AS total
            FROM provider_stats_report
            INNER JOIN account_provider
            ON account_provider.provider_id = provider_stats_report.provider_id
            WHERE account_id = '%d'
            AND (date >= '%s' AND date <= '%s');",
            $accountId,
            $dateFrom,
            $dateTo
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the number of search views for all providers in an account between two dates
     * @param int $accountId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function getSearchViews($accountId, $dateFrom, $dateTo)
    {
        $sql = sprintf(
            "SELECT SUM(search_views) AS total
            FROM provider_stats_report
            INNER JOIN account_provider
            ON account_provider.provider_id = provider_stats_report.provider_id
            WHERE account_id = '%d'
            AND (date >= '%s' AND date <= '%s');",
            $accountId,
            $dateFrom,
            $dateTo
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the number of tracked calls for all providers in an account between two dates
     * @param int $accountId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function getTrackedCalls($accountId, $dateFrom, $dateTo)
    {
        $sql = sprintf(
            "SELECT SUM(1) AS total, twilio_blocked_numbers.id
            FROM twilio_log
            INNER JOIN account_practice
            ON (account_practice.practice_id = twilio_log.practice_id)
            LEFT JOIN twilio_number
            ON twilio_log.twilio_number_id = twilio_number.id
            LEFT JOIN twilio_blocked_numbers
            ON (twilio_log.tw_from = twilio_blocked_numbers.number
            OR twilio_log.tw_from = CONCAT('+1', twilio_blocked_numbers.number))
            AND (twilio_number.twilio_partner_id = twilio_blocked_numbers.twilio_partner_id
            OR twilio_blocked_numbers.twilio_partner_id IS NULL)
            WHERE tw_call_duration >= 40
            AND account_id = '%s'
            AND (time_started >= '%s 00:00:00' AND time_started <= '%s 23:59:59')
            AND SUBSTRING(tw_from, 0, 5) != '+1800'
            AND SUBSTRING(tw_from, 0, 5) != '+1888'
            AND SUBSTRING(tw_from, 0, 5) != '+1877'
            AND SUBSTRING(tw_from, 0, 5) != '+1866'
            AND SUBSTRING(tw_from, 0, 5) != '+1855'
            HAVING twilio_blocked_numbers.id IS NULL;",
            $accountId,
            $dateFrom,
            $dateTo
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the call list for all providers in an account between two dates
     * @param int $accountId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function getTrackedCallsList($accountId, $dateFrom, $dateTo)
    {
        $sql = sprintf(
            "SELECT twilio_log.id AS tracked_call_id, time_started, tw_from,
            tw_call_duration, twilio_blocked_numbers.id AS twilio_blocked_number_id
            FROM twilio_log
            INNER JOIN account_practice
            ON (account_practice.practice_id = twilio_log.practice_id)
            LEFT JOIN twilio_number
            ON twilio_log.twilio_number_id = twilio_number.id
            LEFT JOIN twilio_blocked_numbers
            ON (twilio_log.tw_from = twilio_blocked_numbers.number
            OR twilio_log.tw_from = CONCAT('+1', twilio_blocked_numbers.number))
            AND (twilio_number.twilio_partner_id = twilio_blocked_numbers.twilio_partner_id
            OR twilio_blocked_numbers.twilio_partner_id IS NULL)
            WHERE account_id = '%s'
            AND (time_started >= '%s 00:00:00' AND time_started <= '%s 23:59:59')
            AND tw_call_duration >= 15
            HAVING twilio_blocked_numbers.id IS NULL
            ORDER BY time_started DESC;",
            $accountId,
            $dateFrom,
            $dateTo
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the call list for all providers in an account between two dates
     * Includes extra information for provider reports
     * @param int $accountId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function getTrackedCallsListReport($accountId, $dateFrom, $dateTo)
    {
        $sql = sprintf(
            "SELECT twilio_log.id AS tracked_call_id, time_started, tw_from,
            tw_call_duration, twilio_log.twilio_partner_id, is_qualified,
            caller_option
            FROM twilio_log
            INNER JOIN account_practice
            ON (account_practice.practice_id = twilio_log.practice_id)
            LEFT JOIN twilio_number
            ON twilio_log.twilio_number_id = twilio_number.id
            LEFT JOIN twilio_blocked_numbers
            ON (twilio_log.tw_from = twilio_blocked_numbers.number
            OR twilio_log.tw_from = CONCAT('+1',
            twilio_blocked_numbers.number))
            AND (twilio_number.twilio_partner_id = twilio_blocked_numbers.twilio_partner_id
            OR twilio_blocked_numbers.twilio_partner_id IS NULL)
            WHERE account_id = '%s'
            AND (time_started >= '%s 00:00:00' AND time_started <= '%s 23:59:59')
            AND twilio_blocked_numbers.id IS NULL
            ORDER BY time_started DESC;",
            $accountId,
            $dateFrom,
            $dateTo
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the review list for all providers in an account between two dates
     * Includes extra information for provider reports
     * @param int $accountId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function getReviewListReport($accountId, $dateFrom, $dateTo)
    {
        $sql = sprintf(
            "SELECT provider.prefix, provider.first_last, title,
            description, provider_rating.date_added, rating,
            reviewer_name_anon, patient.first_name, patient.last_name
            FROM provider_rating
            LEFT JOIN provider
            ON provider_rating.provider_id = provider.id
            LEFT JOIN account_provider
            ON provider_rating.provider_id = account_provider.provider_id
            LEFT JOIN patient
            ON provider_rating.patient_id = patient.id
            WHERE account_provider.account_id = '%d'
            AND (provider_rating.date_added >= '%s 00:00:00'
            AND provider_rating.date_added <= '%s 23:59:59')
            AND status = 'A'
            ORDER BY provider_rating.date_added DESC;",
            $accountId,
            $dateFrom,
            $dateTo
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the number of appointment requests for all providers in an account between two dates
     * @param int $accountId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function getAppointmentRequests($accountId, $dateFrom, $dateTo)
    {
        $sql = sprintf(
            "SELECT SUM(1) AS total
            FROM scheduling_mail
            INNER JOIN account_provider
            ON account_provider.provider_id = scheduling_mail.provider_id
            WHERE account_id = '%d'
            AND (scheduling_mail.date_added >= '%s 00:00:00'
            AND scheduling_mail.date_added <= '%s 23:59:59');",
            $accountId,
            $dateFrom,
            $dateTo
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get the appointment requests for all providers in an account between two dates
     * @param int $accountId
     * @param string $dateFrom
     * @param string $dateTo
     * @return array
     */
    public function getAppointmentRequestsList($accountId, $dateFrom, $dateTo)
    {
        $sql = sprintf(
            "SELECT CONCAT(first_name, ' ', last_name) AS name, cell_phone, email,
            appointment_date, date_time, scheduling_mail.date_added,
            CONCAT(insurance_company.name, ' - ', insurance.name) AS insurance, scheduling_mail.id
            FROM scheduling_mail
            INNER JOIN account_provider
            ON account_provider.provider_id = scheduling_mail.provider_id
            LEFT JOIN insurance
            ON scheduling_mail.insurance_id = insurance.id
            LEFT JOIN insurance_company
            ON insurance.company_id = insurance_company.id
            WHERE account_id = '%d'
            AND (scheduling_mail.date_added >= '%s 00:00:00'
            AND scheduling_mail.date_added <= '%s 23:59:59')
            ORDER BY scheduling_mail.date_added DESC;",
            $accountId,
            $dateFrom,
            $dateTo
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Gets the set of accounts that have the given practice and provider associated with it.
     *
     * @param int $practiceId
     * @param int $providerId
     * @return array
     */
    public static function getOwningAccounts($practiceId = null, $providerId = null)
    {

        if ($practiceId && $providerId) {

            // get all accounts with BOTH this practice AND this provider
            $sql = sprintf(
                "SELECT DISTINCT account.*
                FROM account
                INNER JOIN account_practice
                ON account.id = account_practice.account_id
                INNER JOIN account_provider
                ON account.id = account_provider.account_id
                WHERE account_practice.practice_id=%d
                AND account_provider.provider_id=%d
                UNION
                SELECT DISTINCT account.*
                FROM account
                INNER JOIN account_provider
                ON account.id = account_provider.account_id
                INNER JOIN provider_practice AS all_practice
                ON all_practice.provider_id= account_provider.provider_id AND account_provider.all_practices='1'
                INNER JOIN account_practice AS owner_account_practice
                ON all_practice.practice_id= owner_account_practice.practice_id
                AND account_provider.account_id = owner_account_practice.account_id
                WHERE all_practice.practice_id=%d
                UNION
                SELECT DISTINCT account.*
                FROM account
                INNER JOIN account_practice
                ON account.id = account_practice.account_id
                INNER JOIN provider_practice AS all_provider
                ON all_provider.practice_id= account_practice.practice_id AND account_practice.all_providers='1'
                INNER JOIN account_provider AS owner_account_provider
                ON all_provider.provider_id= owner_account_provider.provider_id
                AND account_practice.account_id = owner_account_provider.account_id
                WHERE all_provider.provider_id=%d;",
                $practiceId,
                $providerId,
                $practiceId,
                $providerId
            );
        } elseif ($practiceId) {

            $sql = sprintf(
                "SELECT DISTINCT account.*
                FROM account
                INNER JOIN account_practice
                ON (account_practice.practice_id = %d
                AND account.id = account_practice.account_id)
                UNION
                SELECT DISTINCT account.*
                FROM account
                INNER JOIN account_provider
                ON account.id = account_provider.account_id
                INNER JOIN provider_practice AS all_practice
                ON all_practice.provider_id= account_provider.provider_id AND account_provider.all_practices='1'
                INNER JOIN account_practice AS owner_account_practice
                ON all_practice.practice_id= owner_account_practice.practice_id
                AND account_provider.account_id = owner_account_practice.account_id
                WHERE all_practice.practice_id = %d;",
                $practiceId,
                $practiceId
            );
        } elseif ($providerId) {

            $sql = sprintf(
                "SELECT DISTINCT account.*
                FROM account
                INNER JOIN account_provider
                ON (account_provider.provider_id = %d
                AND account.id = account_provider.account_id)
                UNION
                SELECT DISTINCT account.*
                FROM account
                INNER JOIN account_practice
                ON account.id = account_practice.account_id
                INNER JOIN provider_practice AS all_provider
                ON all_provider.practice_id= account_practice.practice_id AND account_practice.all_providers='1'
                INNER JOIN account_provider AS owner_account_provider
                ON all_provider.provider_id= owner_account_provider.provider_id
                WHERE all_provider.provider_id = %d;",
                $providerId,
                $providerId
            );
        } else {

            // no practice or provider provided
            return array();
        }
        $output = Yii::app()->db->createCommand($sql)->queryAll();

        // convert arrays to account objects
        $outputLength = count($output);
        for ($i = 0; $i < $outputLength; $i++) {
            $newAccount = new Account;
            $newAccount->attributes = $output[$i];
            $newAccount->id = $output[$i]['id'];
            $output[$i] = $newAccount;
        }

        return $output;
    }

    /**
     * Run providers with account report
     * @return array
     */
    public function getProvidersWithAccountsReport()
    {
        $sql = "SELECT IF(account.first_name = account.email, '-', account.first_name) AS account_first_name,
            IF(account.last_name = account.email,'-',account.last_name) AS account_last_name,
            account.email AS account_email,
            account.date_added AS account_added, account.date_updated AS account_updated, account.secret AS
            account_password,
            IF(account.phone_number IS NULL OR account.phone_number = '', IF(provider_practice.phone_number IS NULL OR
            provider_practice.phone_number = '',  practice.phone_number, provider_practice.phone_number),
            account.phone_number) AS phone_number,
            IF(account.phone_number IS NULL OR account.phone_number = '', IF(provider_practice.phone_number IS NULL OR
            provider_practice.phone_number = '', timezone_p.time_zone, timezone_pp.time_zone), timezone_a.time_zone) AS
            timezone,
            state.name AS state_name, city.name AS city_name, provider_stats.phone_views AS provider_phone_views,
            provider.prefix AS provider_prefix, provider.first_name AS provider_first_name, provider.last_name AS
            provider_last_name,
            provider.docscore AS provider_docpoints, account.date_signed_in AS provider_date_signed_in,
            IF(provider.featured, 'Yes', 'No') AS provider_is_featured,
            specialty.name AS specialty_name, sub_specialty.name AS sub_specialty_name
            FROM account
            LEFT JOIN account_provider
            ON account.id = account_provider.account_id
            LEFT JOIN provider
            ON account_provider.provider_id = provider.id
            LEFT JOIN provider_practice
            ON provider.id = provider_practice.provider_id
            AND primary_location
            LEFT JOIN practice
            ON provider_practice.practice_id = practice.id
            LEFT JOIN location
            ON practice.location_id = location.id
            LEFT JOIN city
            ON location.city_id = city.id
            LEFT JOIN state
            ON city.state_id = state.id
            LEFT JOIN provider_specialty
            ON provider_specialty.provider_id = provider.id
            LEFT JOIN sub_specialty
            ON sub_specialty.id = provider_specialty.sub_specialty_id
            LEFT JOIN specialty
            ON sub_specialty.specialty_id = specialty.id
            LEFT JOIN provider_stats
            ON provider_stats.provider_id = provider.id
            LEFT JOIN timezone AS timezone_a
            ON timezone_a.area_code = LEFT(TRIM(REPLACE(REPLACE(REPLACE(REPLACE(account.phone_number, '(', ''), ')', '')
            , '-', ''), ' ', '')), 3)
            LEFT JOIN timezone AS timezone_pp
            ON timezone_pp.area_code = LEFT(TRIM(REPLACE(REPLACE(REPLACE(REPLACE(provider_practice.phone_number, '(',
            ''), ')', ''), '-', ''), ' ', '')), 3)
            LEFT JOIN timezone AS timezone_p
            ON timezone_p.area_code = LEFT(TRIM(REPLACE(REPLACE(REPLACE(REPLACE(practice.phone_number, '(', ''), ')',
            ''), '-', ''), ' ', '')), 3)
            GROUP BY account.id;";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Run providers without account report
     * @return array
     */
    public function getProvidersWithoutAccountsReport()
    {
        $sql = "SELECT account.id AS account_id, account.email AS account_email, IF(account.first_name = account.email,
            '-', account.first_name) AS account_first_name,
            IF(account.last_name = account.email, '-', account.last_name) AS account_last_name, account.date_added AS
            account_added, account.date_updated AS account_updated,
            account.secret AS account_password, account.phone_number AS account_phone_number
            FROM account
            LEFT JOIN account_provider
            ON account_provider.account_id = account.id
            WHERE account_provider.id IS NULL;";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Sends the account a Starbucks gift card using the Tango Card API.
     * This arrives as an email (FROM TANGOCARD, NOT US) at their email address.
     * With staging credentials, it sends an email that's not redeemable.
     *
     * On success, an email is sent to Client Services (from us) to let them
     * know that this happened.
     *
     * Tango Card documentation is here: https://github.com/tangocarddev/RaaS
     *
     * @param boolean $forcePurchaseFunds force the purchase of funds
     * @param int $practiceId
     * @return boolean True if reward sent, else false
     */
    public function sendReward($forcePurchaseFunds = false, $practiceId = 0)
    {

        // Verify TangoCard account has enough money to buy a gift card
        $result = self::sendTangoCardRequest('accounts/' . Yii::app()->params['tangocard_customer_id'] . '/' .
            Yii::app()->params['tangocard_account_id']);

        // Example response:
        //{
        //    "success": true,
        //    "account": {
        //        "identifier"       : "123456",
        //        "email"            : "demo@example.com",
        //        "customer"         : "CompanyA",
        //        "available_balance": 0
        //    }
        //}

        if (!$result->success) {
            throw new Exception('Cannot get account details from Tango Card: ' . json_encode($result));
        }

        // if we have $20 or more, that's enough funds
        $hasEnoughFunds = ($result->account->available_balance >= Yii::app()->params['tangocard_gift_card_value']);

        // if not enough funds, fund an additional tangocard_refill_value more
        if (!$hasEnoughFunds || $forcePurchaseFunds) {

            // prepare request
            $postData = array(
                'customer' => Yii::app()->params['tangocard_customer_id'],
                'account_identifier' => Yii::app()->params['tangocard_account_id'],
                'amount' => Yii::app()->params['tangocard_refill_value'],
                'client_ip' => Yii::app()->request->userHostAddress,
                'security_code' => Yii::app()->params['tangocard_cc_security_code'],
                'cc_token' => Yii::app()->params['tangocard_cc_token']
            );

            $result = self::sendTangoCardRequest('cc_fund', $postData);

            // send request and store order details on account
            // Example response:
            //{
            //  "success": true,
            //  "fund_id": "RF11-22222222-33",
            //  "amount" : 50000
            //}
            // assert we got funds, else throw exception
            if (!$result->success) {
                throw new Exception('Tango Card request to fund $500 failed: ' . json_encode($result));
            }
        }

        // send starbucks gift card via TangoCard API and update all of
        // the account_devices associated with the account
        $accountName = $this->getAccountName();

        $message = 'To the office of ' . $accountName . <<<EOS
: Thank you for using our Review Hub(TM), we appreciate you collecting patient feedback. Have a round of coffees on us
as a token of our gratitude, and keep those great reviews coming!
- Best,
The Doctor.com Team
EOS;

        $recipientEmail = $this->email;

        if ($practiceId == 0) {
            $accountP = AccountPractice::model()->findAll(
                'account_id=:account_id',
                array(':account_id' => $this->id)
            );
            if (count($accountP) == 1) {
                $practiceId = $accountP[0]->practice_id;
            }
        }

        if ($practiceId) {
            // Send Email to Practice reward
            $practiceD = PracticeDetails::model()->find(
                'practice_id=:practice_id',
                array(':practice_id' => $practiceId)
            );
            if (isset($practiceD->reward_email)) {
                $recipientEmail = $practiceD->reward_email;
            }
        }

        // prepare request
        $postData = array(
            'customer' => Yii::app()->params['tangocard_customer_id'],
            'account_identifier' => Yii::app()->params['tangocard_account_id'],
            'recipient' => array(
                'name' => $accountName,
                'email' => $recipientEmail
            ),
            'sku' => Yii::app()->params['tangocard_gift_card_sku'],
            'amount' => Yii::app()->params['tangocard_gift_card_value'],
            'reward_message' => $message,
            'reward_subject' => 'A gift card as thanks! From Doctor.com',
            'reward_from' => 'Doctor.com'
        );

        // Example response:
        //{
        //    "success": true,
        //    "order"  : {
        //        "order_id"          : "123-12345678-12",
        //        "account_identifier": "12345678",
        //        "sku"               : "TNGO-E-V-STD",
        //        "amount"            : 1000,
        //        "reward_message"    : "Thank you for participating in the XYZ survey.",
        //        "reward_subject"    : "XYZ Survey, thank you...",
        //        "reward_from"       : "Jon Survey Doe",
        //        "delivered_at"      : "2013-03-12T15:17:16+00:00",
        //        "recipient"         : {
        //            "name" : "John Doe",
        //            "email": "john.doe@example.com"
        //        }
        //    }
        //}

        $result = self::sendTangoCardRequest('orders', $postData);

        if (empty($result)) {
            Yii::log(
                "Error while sending TangoCard: " . var_export($postData, true),
                'error',
                'application.account.sendReward'
            );
            return false;
        }

        // assert we sent the gift card successfully, else throw exception
        if (!$result->success) {
            throw new Exception('Failed to send TangoCard gift card to ' . $accountName . ': ' . json_encode($result));
        }

        // store order details in DB (order_id for reward, date_reward_sent as
        // the reward sent date)
        $this->reward_order_id = $result->order->order_id;
        $this->date_reward_sent = date('Y-m-d H:i:s');


        // Update Gift Card Sent Date field in Salesforce
        $this->updateSalesforceAccountField('Gift_Card_Sent_Date__c', date('Y-m-d'));

        if (!$this->save()) {
            throw new Exception('Could not save TangoCard order details to ' . $accountName . ': ' .
                json_encode($result) . ' - cause: ' . json_encode($this->getErrors()));
        }

        // gift card sent, so return true
        return true;
    }

    /**
     * Sends a post request to TangoCard with the give url and POST parameters
     *
     * @param string $relativeUrl relative url in TangoCard's API
     * @param string $postParams POST request parameters to send to TangoCard;
     * if none provided, we assume this is a GET request
     */
    protected static function sendTangoCardRequest($relativeUrl, $postParams = null)
    {

        $url = Yii::app()->params['tangocard_api_url'] . $relativeUrl;
        $dataString = $postParams ? json_encode($postParams) : null;

        // Initialize cURL
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Accept: application/json'
        ));
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt(
            $ch,
            CURLOPT_USERPWD,
            Yii::app()->params['tangocard_api_id'] . ":" .  Yii::app()->params['tangocard_api_key']
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);

        // add any post data
        if ($dataString) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
        } else {
            curl_setopt($ch, CURLOPT_HTTPGET, true);
        }

        // wait for and catch the response against the request made
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Execute the request
        $response = curl_exec($ch);
        curl_close($ch);

        // response is JSON
        return json_decode($response);
    }

    /**
     * Get account by provider_npi and partner_site_id
     * @param int $providerId
     * @param int $partnerSiteId
     * @return int account_id
     */
    public static function getByNpiAndPartnerSiteId($providerNpi, $partnerSiteId)
    {
        $sql = sprintf(
            "SELECT account.id
            FROM account
            INNER JOIN account_provider
                ON account_provider.account_id = account.id
            INNER JOIN `provider`
                ON `provider`.id = account_provider.provider_id
            WHERE npi_id = %d
                AND account.partner_site_id = %d",
            $providerNpi,
            $partnerSiteId
        );
        $accountId = Yii::app()->db->createCommand($sql)->queryScalar();
        if (!empty($accountId)) {
            return $accountId;
        }
        return null;
    }

    /**
     * if there is an O-level connection with an Organization, return the Organization
     * @return mixed
     */
    public function getOwnedOrganization()
    {
        $orgAccount = OrganizationAccount::model()->cache(Yii::app()->params['cache_short'])->find(
            'account_id = :account_id AND connection = :connection',
            array(':account_id' => $this->id, ':connection' => "O")
        );
        if (!empty($orgAccount)) {
            return Organization::model()->cache(Yii::app()->params['cache_short'])->
                findByPK($orgAccount->organization_id);
        }
        return false;
    }

    /**
     * Return the Organization which the account belongs to
     *
     * @return mixed
     */
    public function getOrganization()
    {
        $orgAccount = OrganizationAccount::model()->cache(Yii::app()->params['cache_short'])->find(
            'account_id = :account_id',
            array(':account_id' => $this->id)
        );
        if (!empty($orgAccount)) {
            return Organization::model()->cache(Yii::app()->params['cache_short'])->
                findByPK($orgAccount->organization_id);
        }
        return false;
    }

    /**
     * is there an active Organization that has this feature enabled and active?
     * @param featureID int ID of the feature (2 for ReviewHub, etc.)
     */
    public function isFeatureEnabled($featureID)
    {
        $ownedOrg = $this->getOwnedOrganization();

        $found = false;

        if (!empty($ownedOrg) && $ownedOrg->status == "A") {
            foreach ($ownedOrg->organizationFeatures as $orgFeat) {
                if ($orgFeat->feature_id == $featureID && $orgFeat->active == 1) {
                    $found = true;
                }
            }
        }

        return $found;
    }

    /**
     * If there's an associated Salesforce account, records the given
     * note on the account, returning the response object from Salesforce.
     * If not, it returns false.
     *
     * @param string $subject subject of note (brief "preview" summary)
     * @param string $description detailed description of note
     * @return boolean
     */
    public function saveSalesforceAccountNote($subject, $description)
    {

        $currentOrg = $this->getOwnedOrganization();
        if (!empty($currentOrg)) {

            // update salesforce
            return Yii::app()->salesforce->saveAccountNote($currentOrg->salesforce_id, $subject, $description);
        }

        // false indicates no SF account exists
        return false;
    }

    /**
     * Save own account settings
     * @param int $accountId
     * @param array $data
     * @return boolean
     */
    public static function saveSettings($accountId = 0, $data = null)
    {
        $response['success'] = false;
        $response['error'] = '';
        $response['data'] = '';
        if ($accountId != $data['id']) {
            $results['success'] = false;
            $results['http_response_code'] = 403;
            $results['error'] = 'ACCESS_DENIED';
            $results['data'] = 'You don´t have access to this section';
            return $results;
        }

        if (!empty($data['zipcode'])) {
            $location = Location::model()->getLocationIdByZipCode($data['zipcode']);
            if (empty($location['location_id'])) {
                $results['success'] = false;
                $results['http_response_code'] = 200;
                $results['error'] = 'ACCOUNT_SETTING_NOT_SAVED';
                $results['data']['location_id'][0] = 'The zip code you entered is invalid.';
                return $results;
            }

            $data['location_id'] = $location['location_id'];
        }

        $objAccount = Account::model()->findByPk($accountId);
        if (empty($objAccount)) {
            $results['success'] = false;
            $results['http_response_code'] = 403;
            $results['error'] = 'ACCESS_DENIED';
            $results['data'] = 'You don´t have access to this section';
            return $results;
        }

        $objAccount->attributes = $data;
        if (!empty($data['password_confirm'])) {
            $objAccount->password_confirm = $data['password_confirm'];
        }

        if ($objAccount->save()) {
            // no need to notify phabricator here
            AuditLog::create('U', 'Account settings #' . $accountId, 'account', null, false);
            $response['success'] = true;

            $account = (array) $objAccount->attributes;
            unset($account['secret']);
            unset($account['enabled']);
            unset($account['password']);
            $response['data'] = $account;
        } else {
            $response['error'] = 'ACCOUNT_SETTING_NOT_SAVED';
            $response['data'] = $objAccount->getErrors();
        }
        return $response;
    }

    /**
     * If there's an associated Salesforce account, updates the given
     * field, returning the response object from Salesforce. If not, it returns
     * false.
     *
     * @param string $salesforceFieldId name of salesforce field to update
     * @param string $value value to update in Salesforce
     * @return boolean
     */
    public function updateSalesforceAccountField($salesforceFieldId, $value)
    {

        $currentOrg = $this->getOwnedOrganization();
        if (!empty($currentOrg)) {

            // update salesforce
            return Yii::app()->salesforce->updateAccountField($currentOrg->salesforce_id, $salesforceFieldId, $value);
        }

        // false indicates no SF account exists
        return false;
    }

    /**
     * If there's an associated Salesforce account, updates the given field, returning the response object from
     * Salesforce.
     * If not, it returns false.
     *
     * @param array $data dictionary of fields to update
     * @return boolean
     */
    public function updateSalesforceAccount($data)
    {

        $currentOrg = $this->getOwnedOrganization();
        if (!empty($currentOrg)) {

            // update salesforce
            return Yii::app()->salesforce->updateAccount($currentOrg->salesforce_id, $data);
        }

        // false indicates no SF account exists
        return false;
    }

    /**
     * Command: Gets the main performance by account report
     * @return array
     */
    public static function getPerformanceByAccountReport()
    {
        $sql = sprintf("SELECT SQL_NO_CACHE
            account.id AS account_id, account.organization_name AS practice_name,
            CONCAT(account.first_name,' ',last_name) AS account_name,
            `month`, `year`, SUM(scheduling_mail) AS scheduling_mail,
            SUM(total_calls) AS total_calls, SUM(qualified_call) AS qualified_call
            FROM
            (
                SELECT account_id, `month`, `year`,
                '0' AS scheduling_mail, COUNT(tracked_call) AS total_calls, SUM(qualified_call) AS qualified_call
                FROM (
                    SELECT DISTINCT twilio_log.id AS tracked_call, IF(is_qualified='1','1','0') AS qualified_call,
                    account_practice.account_id AS account_id,
                    MONTH(twilio_log.time_started) AS month, YEAR(twilio_log.time_started) AS year
                    FROM twilio_log
                    INNER JOIN account_practice
                    ON (account_practice.practice_id = twilio_log.practice_id)
                    LEFT JOIN twilio_number
                    ON twilio_log.twilio_number_id = twilio_number.id
                    LEFT JOIN twilio_blocked_numbers
                    ON (twilio_log.tw_from = twilio_blocked_numbers.number OR twilio_log.tw_from = CONCAT('+1',
                    twilio_blocked_numbers.number))
                    AND (twilio_number.twilio_partner_id = twilio_blocked_numbers.twilio_partner_id
                    OR twilio_blocked_numbers.twilio_partner_id IS NULL)
                    WHERE twilio_blocked_numbers.id IS NULL
                    AND (twilio_log.time_started > date_sub(NOW(), INTERVAL 12 MONTH)
                    AND twilio_log.time_started <= NOW())
                ) AS twilio_calls
                GROUP BY account_id, month, year

                UNION ALL

                SELECT account_id, MONTH(scheduling_mail.date_added) AS `month`, YEAR(scheduling_mail.date_added) AS
                `year`,
                COUNT(scheduling_mail.id) AS scheduling_mail, '0' AS total_calls, '0' AS qualified_call
                FROM scheduling_mail
                INNER JOIN account_provider
                ON account_provider.provider_id = scheduling_mail.provider_id
                AND (scheduling_mail.date_added > date_sub(NOW(), INTERVAL 12 MONTH)
                AND scheduling_mail.date_added <= NOW())
                GROUP BY account_id, MONTH(scheduling_mail.date_added), YEAR(scheduling_mail.date_added)
            ) AS report
            INNER JOIN account
            ON account.id = report.account_id
            INNER JOIN organization_account
            ON organization_account.account_id = account.id
            AND `connection` = 'O'
            INNER JOIN organization
            ON organization.id = organization_account.organization_id
            AND organization.status = 'A'
            GROUP BY account.id, month, year
            ORDER BY account.id, year, month;");
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Update scheduling after delete
     */
    public function afterDelete()
    {

        // update scheduling if necessary
        if ($this->enabled == 1 && !empty($this->id)) {
            // async update
            SqsComponent::sendMessage('updateBooking', json_encode(['account_id' => $this->id]));
        }

        return parent::afterDelete();

    }

    /**
     * Create Salesforce Lead from new free accounts
     * @return bool
     */
    public function afterSave()
    {

        $aPrivileges = Yii::app()->request->getParam("AccountPrivileges", false);

        if (isset($aPrivileges['edit']) && $aPrivileges['edit'] == 1) {

            $accountId = $this->id;

            // update privileges
            $accountPrivileges = AccountPrivileges::model()->model()->find(
                'account_id=:account_id',
                array(':account_id' => $accountId)
            );

            if (empty($accountPrivileges)) {
                $accountPrivileges = new AccountPrivileges();
                $accountPrivileges->account_id = $accountId;
            }

            $accountPrivileges->edit_providers = isset($aPrivileges['edit_providers'])
                ? $aPrivileges['edit_providers'] : 0;
            $accountPrivileges->edit_practices = isset($aPrivileges['edit_practices'])
                ? $aPrivileges['edit_practices'] : 0;

            // Owners and Managers should have all privileges for an Organization.
            $organizationAccount = OrganizationAccount::model()->find(
                'account_id = :account_id',
                [':account_id' => $accountId]
            );

            if ($organizationAccount && in_array($organizationAccount->connection, ['M','O'])) {
                $accountPrivileges->reviews = 1;
                $accountPrivileges->appointment_request = 1;
                $accountPrivileges->tracked_calls = 1;
                $accountPrivileges->products_services = 1;
            }

            $accountPrivileges->save();
        }

        // clear widget cache
        foreach ($this->organizationAccounts as $orgAccount) {
            $widgets = Widget::model()->findAll(
                "account_id = :account_id",
                array(":account_id" => $orgAccount->account_id)
            );

            foreach ($widgets as $widget) {
                Widget::clear($widget->key_code);
            }
        }

        if (!empty($this->oldRecord->localization_id) && !empty($this->oldRecord->country_id)) {
            // if the localization of the account has changed
            if (
                $this->oldRecord->localization_id != $this->localization_id
                ||
                $this->oldRecord->country_id != $this->country_id
            ) {
                // change the localization of its practices if they are not associated with another account
                foreach ($this->accountPractices as $ap) {
                    $practice = Practice::model()->findByPk($ap->practice_id);
                    if (count($practice->accountPractices) == 1) {
                        $practice->localization_id = $this->localization_id;
                        $practice->country_id = $this->country_id;
                        $practice->save(false);
                    }
                }

                // change the localization of its providers if they are not associated with another account
                foreach ($this->accountProviders as $ap) {
                    $provider = Provider::model()->findByPk($ap->provider_id);
                    if (count($provider->accountProviders) == 1) {
                        $provider->localization_id = $this->localization_id;
                        $provider->country_id = $this->country_id;
                        $provider->save(false);
                    }
                }
            }
        }

        // Is this the Owner account?
        if (OrganizationAccount::isOwnerAccount($this->id)) {
            // Yes is the owner
            // Copy the ownerAccount `is_beta_account` && `only_dashboard` value to the manager and staff account

            // Get the other accounts for this org
            $orgAccount = OrganizationAccount::getAllAccountId($this->id);
            if (!empty($orgAccount)) {
                foreach ($orgAccount as $value) {
                    if ($value == $this->id) {
                        // we don´t want to process this account again
                        continue;
                    }
                    $account = Account::model()->findByPk($value);

                    $needUpdate = false;
                    if ($account->is_beta_account != $this->is_beta_account) {
                        $account->is_beta_account = $this->is_beta_account;
                        $needUpdate = true;
                    }
                    if ($account->only_dashboard != $this->only_dashboard) {
                        $account->only_dashboard = $this->only_dashboard;
                        $needUpdate = true;
                    }
                    if ($needUpdate) {
                        // Set the same value as the owner
                        if ($account) {
                            $account->save();
                        }
                    }
                }
            }
        } else {

            // Inherit the value of `is_beta_account` && `only_dashboard` from the account owner
            $ownerAccountId = OrganizationAccount::switchOwnerAccountID($this->id, true);
            if ($ownerAccountId != $this->id) {
                $ownerAccount = Account::model()->findByPk($ownerAccountId);
                if (!empty($ownerAccount)) {
                    $needUpdate = false;
                    if ($this->is_beta_account != $ownerAccount->is_beta_account) {
                        $this->is_beta_account = $ownerAccount->is_beta_account;
                        $needUpdate = true;
                    }
                    if ($this->only_dashboard != $ownerAccount->only_dashboard) {
                        $this->only_dashboard = $ownerAccount->only_dashboard;
                        $needUpdate = true;
                    }
                    if ($needUpdate) {
                        // Set the same value as the owner
                        $this->save();
                    }
                }
            }
        }

        // update scheduling if necessary
        if ($this->isNewRecord
            ||
            (!empty($this->oldRecord) && $this->oldRecord->enabled != $this->enabled)
        ) {
            if (!empty($this->id)) {
                // async update
                SqsComponent::sendMessage('updateBooking', json_encode(['account_id' => $this->id]));
            }
        }

        return parent::afterSave();
    }

    /**
     * Get all accounts linked to active organizations that were created
     * 90/180/etc days ago
     * Used by ProviderCommand.php | actionClientsInformationUpdateReminder
     * @return array
     */
    public function getAccountsForUpdateReminder()
    {
        $sql = "SELECT account.id
            FROM account
            INNER JOIN organization_account
            ON account.id = organization_account.account_id
            AND `connection` = 'O'
            INNER JOIN organization
            ON organization_account.organization_id = organization.id
            AND `status` = 'A'
            WHERE DATEDIFF(NOW(), organization.date_added) % 90 = 0;";
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get accounts linked to providerId ANd practiceId
     * Used by SchedulingMail::sendToSalesForce
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function getFreeAccountByProviderPractice($providerId = 0, $practiceId = 0)
    {
        $sql = sprintf(
            "SELECT a.id, a.email, a.salesforce_lead_id, a.organization_name,
            a.phone_number, a.date_added, a.first_name, a.last_name
            FROM account AS a
            INNER JOIN account_provider AS apro
            ON apro.account_id = a.id
            INNER JOIN account_practice AS apra
            ON apra.account_id = a.id
            LEFT JOIN organization_account AS oa
            ON a.id = oa.account_id
            WHERE apro.provider_id = %d
            AND apra.practice_id = %d
            AND oa.id IS NULL
            LIMIT 1;",
            $providerId,
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Returns wether an account belongs to an organization that has churned
     * @param int $accountId
     * @return boolean
     */
    public static function belongsToChurnedOrganization($accountId)
    {

        $sql = sprintf(
            "SELECT organization_id, organization.status
            FROM organization_account
                INNER JOIN organization
                    ON organization.id = organization_account.organization_id
            WHERE account_id = %d AND connection = 'O' ",
            $accountId
        );
        $organization = Yii::app()->dbRO->createCommand($sql)->queryRow();

        if (!empty($organization['status']) && $organization['status'] == 'C') {
            return true;
        }
        return false;
    }

    /**
     * Returns organization status for an account
     * @param int $accountId
     * @return string Status
     */
    public static function getOrganizationStatus($accountId)
    {
        $account = Account::model()->findByPK($accountId);

        if (empty($account)) {
            return "";
        }

        $organization = $account->getOwnedOrganization();
        if ($organization) {
            $status = $organization->status;
            switch ($status) {
                case 'A':
                    return "Active";
                case 'P':
                    return "Paused";
                case 'C':
                    return "Canceled";
            }
        }
        return "";
    }

    /**
     * Determinate whether an account belongs to an organization
     * @param int $accountId
     * @param bool $activeOnly
     * @param bool $allowManagers
     * @return bool
     */
    public static function belongsToOrganization($accountId, $activeOnly = false, $allowManagers = false)
    {

        if (!$allowManagers) {
            // only accounts that are organization owners
            $connectionType = sprintf(" AND connection = 'O' ");
        } else {
            // accounts that are organization owners or managers
            $connectionType = sprintf(" AND connection IN ('M', 'O') ");
        }

        $sql = sprintf(
            "SELECT organization_id, organization.status
            FROM organization_account
                INNER JOIN organization
                    ON organization.id = organization_account.organization_id
            WHERE account_id = %d
            %s",
            $accountId,
            $connectionType
        );
        $organization = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();

        if (!empty($organization)) {
            if (!$activeOnly || ($activeOnly && $organization['status'] == 'A')) {
                // allow organizations in any status or only active ones
                return $organization['organization_id'];
            }
        }
        return false;
    }

    /**
     * Determinate connection from an account belongs to an active organization
     * @param int $accountId
     * @return mix
     */
    public static function belongsToOrganizationConnection($accountId)
    {
        $organizationAccount = OrganizationAccount::model()->cache(Yii::app()->params['cache_medium'])->find(
            'account_id=:account_id',
            array(':account_id' => $accountId)
        );
        if ($organizationAccount) {
            $organization = Organization::model()->cache(Yii::app()->params['cache_medium'])->findByPk(
                $organizationAccount->organization_id
            );
            if ($organization->status == 'A') {
                return $organizationAccount->connection;
            }
        }
        return false;
    }

    /**
     * Retrieve details from the account stored in salesforce for the account api
     * @param string $salesforceId
     * @param bool $skipCache
     * @return array
     */
    public static function getSalesforceDetailsApi($salesforceId, $skipCache = false)
    {
        $accountData['salesforce_id'] = $salesforceId;
        $sfDetails = Account::getSalesforceDetails($salesforceId, true);

        $accountData['csRepName'] =
            !empty($sfDetails['CSupport_Rep__c'])
            ? trim($sfDetails['CSupport_Rep__c'])
            : '';

        // look up organization in salesforce
        $sfAccount = Yii::app()->salesforce->getObject("Account", $accountData['salesforce_id']);

        // get account manager info
        $accountManager['name'] = '';
        $accountManager['email'] = '';
        $accountManager['phone'] = '';
        $accountManager['full_photo_url'] = '';
        $accountManager['small_photo_url'] = '';

        if (!empty($sfAccount['Account_Rep__c'])) {
            $sfUser = Yii::app()->salesforce->getObject("User", $sfAccount['Account_Rep__c']);
            if (!empty($sfUser)) {
                $accountManager['name'] = Common::getAttribute('Name', $sfUser, '');
                $accountManager['email'] = Common::getAttribute('Email', $sfUser, '');
                $accountManager['phone'] = Common::getAttribute('Phone', $sfUser, '');
                $accountManager['mobile_phone'] = Common::getAttribute('MobilePhone', $sfUser, '');
                $accountManager['full_photo_url'] = Common::getAttribute('FullPhotoUrl', $sfUser, '');
                $accountManager['small_photo_url'] = Common::getAttribute('SmallPhotoUrl', $sfUser, '');
            }
        }

        // The Product Specialist field in Salesforce is Onboard_Rep__c
        $productSpecialist['name'] = '';
        $productSpecialist['email'] = '';
        $productSpecialist['phone'] = '';
        $productSpecialist['full_photo_url'] = '';
        $productSpecialist['small_photo_url'] = '';
        if (!empty($sfAccount['Onboard_Rep__c'])) {
            $sfUser = Yii::app()->salesforce->getObject("User", $sfAccount['Onboard_Rep__c']);
            if (!empty($sfUser)) {
                $productSpecialist['name'] = Common::getAttribute('Name', $sfUser, '');
                $productSpecialist['email'] = Common::getAttribute('Email', $sfUser, '');
                $productSpecialist['phone'] = Common::getAttribute('Phone', $sfUser, '');
                $productSpecialist['mobile_phone'] = Common::getAttribute('MobilePhone', $sfUser, '');
                $productSpecialist['full_photo_url'] = Common::getAttribute('FullPhotoUrl', $sfUser, '');
                $productSpecialist['small_photo_url'] = Common::getAttribute('SmallPhotoUrl', $sfUser, '');
            }
        }

        return [
            'accountData' => $accountData,
            'accountManager' => $accountManager,
            'productSpecialist' => $productSpecialist
        ];
    }

    /**
     * Retrieve details from the account stored in salesforce
     * @param string $salesforceId
     * @param bool $skipCache
     * @return mixed
     */
    public static function getSalesforceDetails($salesforceId, $skipCache = false)
    {

        $sfAccount = false;
        if (!$skipCache) {
            // attempt to retrieve the acccount rep info from cache
            $sfJsonAccount = Yii::app()->cache->get($salesforceId);
            if ($sfJsonAccount) {
                $sfAccount = json_decode($sfJsonAccount);
            }
        }

        $sfUser = false;
        $sfUserOnboardRep = false;
        $sfUserOwner = false;
        $sfCSupportRepName = false;

        if ($sfAccount) {
            // we could get the account info from cache, try to get the rep info from cache
            $sfUser = json_decode(Yii::app()->cache->get($sfAccount->Account_Rep__c), true);
            $sfUserOnboardRep = json_decode(Yii::app()->cache->get($sfAccount->Onboard_Rep__c), true);
            $sfUserOwner = json_decode(Yii::app()->cache->get($sfAccount->OwnerId), true);
            $sfCSupportRepName = json_decode(Yii::app()->cache->get($sfAccount->CSupport_Rep__c), true);
        } else {
            // if the account rep info is not cached, attempt to retrieve it from salesforce
            // first get the account information
            $sfAccount = Yii::app()->salesforce->getObject("Account", $salesforceId);
        }

        if (empty($sfAccount)) {
            return false;
        }
        $sfAccount = (array) $sfAccount;

        if (!$sfUser && !empty($sfAccount['Account_Rep__c'])) {
            // set account cache
            Yii::app()->cache->set($salesforceId, json_encode($sfAccount), Yii::app()->params['cache_long']);
            // then get the account rep information
            $sfUser = Yii::app()->salesforce->getObject("User", $sfAccount['Account_Rep__c']);
            // set account rep cache
            Yii::app()->cache->set(
                $sfAccount['Account_Rep__c'],
                json_encode($sfUser),
                Yii::app()->params['cache_long']
            );
        }

        if (!$sfUserOnboardRep && !empty($sfAccount['Onboard_Rep__c'])) {
            // set account cache
            Yii::app()->cache->set($salesforceId, json_encode($sfAccount), Yii::app()->params['cache_long']);
            // then get the onboard rep information
            $sfUserOnboardRep = Yii::app()->salesforce->getObject("User", $sfAccount['Onboard_Rep__c']);
            // set onboard rep cache
            Yii::app()->cache->set(
                $sfAccount['Onboard_Rep__c'],
                json_encode($sfUserOnboardRep),
                Yii::app()->params['cache_long']
            );
        }

        if (!$sfCSupportRepName && !empty($sfAccount['CSupport_Rep__c'])) {
            // set account cache
            Yii::app()->cache->set($salesforceId, json_encode($sfAccount), Yii::app()->params['cache_long']);
            // then get the onboard rep information
            $sfCSupportRepName = $sfAccount['CSupport_Rep__c'];
            // set onboard rep cache
            Yii::app()->cache->set(
                $sfAccount['CSupport_Rep__c'],
                json_encode($sfCSupportRepName),
                Yii::app()->params['cache_long']
            );
        }

        if (!$sfUserOwner && !empty($sfAccount['OwnerId'])) {
            // set account cache
            Yii::app()->cache->set($salesforceId, json_encode($sfAccount), Yii::app()->params['cache_long']);
            // then get the onboard rep information
            $sfUserOwner = Yii::app()->salesforce->getObject("User", $sfAccount['OwnerId']);
            // set onboard rep cache
            Yii::app()->cache->set(
                $sfAccount['OwnerId'],
                json_encode($sfUserOwner),
                Yii::app()->params['cache_long']
            );
        }

        $result = array();
        if (!empty($sfUser['Name'])) {
            $result['account_rep'] = $sfUser['Name'];
            $result['salesforce_link'] = Yii::app()->salesforce->applicationUri . $salesforceId;
            $result['account_rep_email'] = $sfUser['Username'];
            $result['account_rep_phone'] = $sfUser['Phone'];
        } else {
            $result['account_rep'] = '';
            $result['salesforce_link'] = '';
            $result['account_rep_email'] = '';
            $result['account_rep_phone'] = '';
        }

        if (!empty($sfUserOnboardRep['Name'])) {
            $result['onboard_rep_name'] = $sfUserOnboardRep['Name'];
            $result['onboard_rep_email'] = $sfUserOnboardRep['Username'];
        } else {
            $result['onboard_rep_name'] = '';
            $result['onboard_rep_email'] = '';
        }

        if (!empty($sfCSupportRepName)) {
            $result['CSupport_Rep__c'] = $sfCSupportRepName;
        } else {
            $result['CSupport_Rep__c'] = '';
        }

        if (!empty($sfUserOwner['Name'])) {
            $result['sales_person_name'] = $sfUserOwner['Name'];
        } else {
            $result['sales_person_name'] = '';
        }
        if (empty($result)) {
            return false;
        }

        return $result;
    }

    /**
     * Can Claim
     *
     * @return bool
     */
    public function canClaim()
    {
        $blocklist = Blocklist::model()->findAll();
        $email = $this->email;
        $matches = array();
        foreach ($blocklist as $blockedItem) {
            try {
                $regex = '/' . $blockedItem->regex . '/';
                if (preg_match($regex, $email, $matches) == 1) {
                    return false;
                }
            } catch (Exception $e) {
            }
        }
        return true;
    }

    /**
     * Return if all providers and a practices from an account could be published
     * @param int $accountId
     * @return boolean
     */
    public static function canAccountBePublished($accountId)
    {

        $sql = sprintf(
            "SELECT research_status FROM account_practice WHERE account_id = %d
            UNION
            SELECT research_status FROM account_provider WHERE account_id = %d;",
            $accountId,
            $accountId
        );
        $result = Yii::app()->db->createCommand($sql)->query();

        if (empty($result)) {
            return false;
        }

        $canBePublished = true;

        foreach ($result as $value) {
            if ($value['research_status'] == 'Pending' || $value['research_status'] == 'Rejected') {
                $canBePublished = false;
            }
        }

        return $canBePublished;
    }

    /**
     * Get providers and Practices
     * @param int $accountId
     */
    public static function getProvidersAndPractices($accountId = 0)
    {
        $accountInfo = Account::getInfo($accountId, false, false);
        unset($accountInfo['providers']);
        unset($accountInfo['practices']);

        //
        // ADDING PROVIDERS DETAILS
        //
        $accountData['pending_providers'] = ManualValidation::getPending($accountInfo, true);


        $ownerAccountId = $accountInfo['owner_account_id'];
        $connectionType = $accountInfo['connection_type'];

        // Use the loggedAccountId (for the Staff) or the ownerAccountId (for the owner)
        $accountId = ($connectionType == 'S') ? $accountId : $ownerAccountId;

        if (empty($accountInfo['providers'])) {
            // Yes, was loaded, but without providers, so we need to added it
            $data = [
                'account_id' => $accountId,
            ];
            $resultProvider = AccountProvider::getProviders($data);
        } else {

            // restore saved data
            $resultProvider = [
                'has_online_scheduling' => !empty($accountInfo['has_online_scheduling']) ? 1 : 0,
                'accountProviders' => $accountInfo['providers'],
                'provider_current_page' => 0,
                'provider_page_size' => 0,
                'provider_total_records' => count($accountInfo['providers'])
            ];
        }


        if (empty($accountInfo['practices'])) {
            // Yes, was loaded, but without practices, so we need to added it
            $data = [
                'account_id' => $accountId,
            ];
            $resultPractice = AccountPractice::getPractices($data);
        } else {

            // restore saved data
            $resultPractice = [
                'access_granted' => $accountInfo['access_granted'],
                'accountPractices' => $accountInfo['practices'],
                'practice_current_page' => 0,
                'practice_page_size' => 0,
                'practice_total_records' => count($accountInfo['practices'])

            ];
        }

        $resultProvider['provider_avg_profile_completeness'] = 0;
        if (!empty($resultProvider['accountProviders'])) {
            $maxScore = Docscore::getMaxProviderScore();
            if (!empty($maxScore)) {
                $resultProvider['provider_avg_profile_completeness'] =
                    (array_sum(array_column($resultProvider['accountProviders'], "docscore"))
                    /
                    count($resultProvider['accountProviders'])) / $maxScore * 100;
            }
        }
        $resultPractice['practice_avg_profile_completeness'] = 0;
        if (!empty($resultPractice['accountPractices'])) {
            $maxScore = Docscore::getMaxPracticeScore();
            if (!empty($maxScore)) {
                $resultPractice['practice_avg_profile_completeness'] =
                    (array_sum(array_column($resultPractice['accountPractices'], "score"))
                    /
                    count($resultPractice['accountPractices'])) / $maxScore * 100;
            }
        }

        return array_merge($resultProvider, $resultPractice);
    }


    /**
     * Return Staff Account Info & Setting
     * @param int $loggedAccountId
     * @param int $ownerAccountId
     * @param string $connectionType
     * @param bool $useCache
     * @return array
     */
    public static function getAccessGranted(
        $loggedAccountId = 0,
        $ownerAccountId = 0,
        $connectionType = 'S',
        $useCache = true
    ) {

        $response = array();
        $cacheTime = $useCache ? Yii::app()->params['cache_short'] : 0;

        if ($ownerAccountId == 0) {
            $ownerAccountId = OrganizationAccount::switchOwnerAccountID($loggedAccountId, true);
        }

        // Use the loggedAccountId (for the Staff) or the ownerAccountId (for the owner)
        $accountId = ($connectionType == 'S') ? $loggedAccountId : $ownerAccountId;

        // Default Access granted
        $response['access_granted'] = 'custom';

        // Associated practices to account
        $sql = sprintf(
            "SELECT ap.practice_id, practice.name, ap.all_providers
            FROM practice
            INNER JOIN account_practice AS ap ON ap.practice_id = practice.id
            WHERE ap.account_id = %d
            GROUP BY practice.id
            ORDER BY practice.name ASC;",
            $accountId
        );
        $practices = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();

        $practicesData = array();
        if (!empty($practices)) {
            foreach ($practices as $practice) {
                $practicesData[$practice['practice_id']] = $practice['practice_id'];
                if ($practice['all_providers']) {
                    $response['access_granted'] = 'practices';
                }
            }
        }
        $response['practices'] = $practicesData;

        // Associated providers to account
        $sql = sprintf(
            "SELECT provider.id AS provider_id, first_m_last, ap.all_practices
            FROM provider
            INNER JOIN account_provider AS ap ON ap.provider_id = provider.id
            WHERE ap.account_id = %d
            GROUP BY provider.id ORDER BY first_m_last ASC;",
            $accountId
        );
        $providers = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();

        $providersData = array();
        if (!empty($providers)) {
            foreach ($providers as $provider) {
                $providersData[$provider['provider_id']] = $provider['provider_id'];
                if ($provider['all_practices']) {
                    $response['access_granted'] = 'providers';
                }
            }
        }
        $response['providers'] = $providersData;

        if ($response['access_granted'] != 'custom') {

            $accountProviderPractices = AccountProvider::getAccountProvidersPractices($ownerAccountId);

            if (!empty($accountProviderPractices)) {
                if ($response['access_granted'] == 'practices') {
                    // find related providers
                    $response['providers'] = array();
                    foreach ($response['practices'] as $practiceId) {
                        foreach ($accountProviderPractices as $value) {
                            if ($value['id'] == $practiceId) {
                                $response['providers'][$value['provider_id']] = $value['provider_id'];
                            }
                        }
                    }
                } elseif ($response['access_granted'] == 'providers') {
                    // find related practices
                    $response['practices'] = array();
                    foreach ($response['providers'] as $providerId) {
                        foreach ($accountProviderPractices as $value) {
                            if ($value['provider_id'] == $providerId) {
                                $response['practices'][$value['id']] = $value['id'];
                            }
                        }
                    }
                }
            }
        }
        return $response;
    }

    /**
     * Return account Info & Setting
     * Ex:
     * array (
     *       'account_id' => '16490'
     *       'account_name' => 'Facebook Testing Account'
     *       'account_email' => 'account@email.com'
     *       'owner_account_id' => '16490'
     *       'organization_id' => '1573'
     *       'connection_type' => 'O'
     *       'privileges' => array (
     *               'reviews' => 1
     *               'appointment_request' => 1
     *               'tracked_calls' => 1
     *               'products_services' => 1
     *       )
     *       'access_granted' => 'custom'
     *       'practices' => array (
     *               3659772 => '3659772'
     *               3723506 => '3723506'
     *               3732849 => '3732849'
     *       )
     *       'providers' => array (
     *               2933003 => '2933003'
     *               3346049 => '3346049'
     *               3699310 => '3699310'
     *       )
     * )
     * @param int $accountId
     * @param bool $useCache -> default:true
     * @param bool $addProvidersAndPractices -> default: true
     * @return array
     */
    public static function getInfo($accountId = 0, $useCache = true, $addProvidersAndPractices = true)
    {
        if (php_sapi_name() != 'cli' && !empty(Yii::app()->session['account_id'])) {
            $accountId = Yii::app()->session['account_id'];
        }
        $result = null;
        // retrieve info from session only if indicated through param + we're not in cli
        if ($useCache && php_sapi_name() != 'cli') {
            $result = Yii::app()->session['accountInfo'];
            if (!empty($result) && !empty($result['time'])) {
                $diff = intval((time() - $result['time']) / 60);
                if ($diff > 1) {
                    // More than 1 minute, reset the session and read it again
                    unset($result);
                } elseif (Common::isTrue($addProvidersAndPractices)) {
                    // Now, we request a Provider And practice data
                    unset($result);
                }
            }
        }

        if (empty($result)) {

            $result = array();
            $cache = ($useCache) ? Yii::app()->params['cache_short'] : 0;

            $account = Account::model()->cache($cache)->findByPK($accountId);
            if (empty($account)) {
                return false;
            }

            $result['canClaim'] = !empty($account) ? $account->canClaim() : false;

            $ownerAccountId = $accountId;
            $organizationId = 0;
            $connectionType = '';

            $sql = sprintf(
                "SELECT o.id AS organization_id, oa.connection, o.organization_name,
                o.status, o.salesforce_id, account_id
                FROM organization_account AS oa
                LEFT JOIN organization AS o ON oa.organization_id = o.id
                WHERE oa.account_id = %d LIMIT 1;",
                $accountId
            );

            $result['is_freemium'] = 0;
            $currentAccountOrg = Yii::app()->db->cache($cache)->createCommand($sql)->queryRow();
            if (!empty($currentAccountOrg) && !empty($currentAccountOrg['organization_id'])) {
                // The account is linked to an organization
                $connectionType = $currentAccountOrg['connection'];
                $organizationId = $currentAccountOrg['organization_id'];
                if ($connectionType != 'O') {
                    // Looking for the Owner
                    $ownerAccountOrg = OrganizationAccount::model()->cache($cache)->find(
                        "organization_id=:organizationId  AND `connection`='O'",
                        array(":organizationId" => $organizationId)
                    );
                    if ($ownerAccountOrg) {
                        // Found the owner
                        $ownerAccountId = $ownerAccountOrg->account_id;
                    }
                }
            } else {
                // is Freemium account, setting as owner
                $result['is_freemium'] = 1;
                $connectionType = 'O';
            }

            $researchMode = $account->research_mode;

            $onlyDashboard = !empty($account->only_dashboard) ? $account->only_dashboard : 0;
            if ($ownerAccountId != $accountId) {
                // find the owner account
                $oAccount = Account::model()->cache($cache)->findByPK($ownerAccountId);
                $researchMode = $oAccount->research_mode;
                // If the owner has only_dashboard access, this account too...
                $onlyDashboard = !empty($oAccount->only_dashboard) ? $oAccount->only_dashboard : 0;
            }

            $result['account_id'] = $accountId;
            $result['is_beta_account'] = !empty($account->is_beta_account) ? $account->is_beta_account : 0;
            $result['only_dashboard'] = $onlyDashboard;
            $result['first_time_login'] = Account::firstTimeLogin($accountId, $result['is_beta_account']);
            $result['account_type_id'] = $account->account_type_id;
            $result['account_name'] = $account->first_name . ' ' . $account->last_name;
            $result['account_email'] = $account->email;
            $result['account_research_mode'] = $researchMode;
            $result['location_id'] = $account->location_id;
            $result['owner_account_id'] = $ownerAccountId;
            $result['organization_id'] = $organizationId;
            $result['organization_name'] = !empty($currentAccountOrg['organization_name']) ?
                $currentAccountOrg['organization_name'] : '';
            $result['salesforce_id'] = !empty($currentAccountOrg['salesforce_id']) ?
                $currentAccountOrg['salesforce_id'] : '';
            $result['connection_type'] = $connectionType;
            $result['partner_site_id'] = $account->partner_site_id;
            $result['date_signed_in'] = $account->date_signed_in;
            $result['last_password_change'] = $account->last_password_change;
            $result['privileges'] = AccountPrivileges::getAccountPrivileges($accountId, $connectionType);

            if (Common::isTrue($addProvidersAndPractices)) {
                // We don't want the providerAndPractice data right now

                $provPrac = Account::getProvidersAndPractices($accountId);

                $result['access_granted'] = $provPrac['access_granted'];
                $result['practices'] = $provPrac['accountPractices'];
                $result['providers'] = $provPrac['accountProviders'];
            }

            // read token
            $token = ApiComponent::request('token', '');

            // If is and admin impersonate
            $result['admin_account_id'] = null;
            $result['admin_account_type_id'] = null;
            $result['admin_account_name'] = null;

            // if we're not in cli and we're an admin, find who it is
            $adminAccountId = '';
            if (php_sapi_name() != 'cli' && !empty(Yii::app()->session['admin_account_id'])) {
                $adminAccountId = Yii::app()->session['admin_account_id'];
            } elseif (!empty($token)) {
                if (method_exists('JWT', 'validate')) {
                    // GMB has his own JWT with no validate method
                    $resultToken = JWT::validate($token, 'array');
                    if (!empty($resultToken['data']['admin_account_id'])) {
                        $adminAccountId = $resultToken['data']['admin_account_id'];
                    }
                }
            }

            if (!empty($adminAccountId)) {
                $adminAccount = Account::model()->cache($cache)->find(
                    'MD5(id)=:adminAccountId',
                    array(':adminAccountId' => $adminAccountId)
                );

                if (!empty($adminAccount)) {
                    $result['admin_account_id'] = $adminAccount['id'];
                    $result['admin_account_type_id'] = $adminAccount['account_type_id'];
                    $result['admin_account_name'] = $adminAccount->first_name . ' ' . $adminAccount->last_name;
                }
            }

            $result['time'] = time();
            // save info in session only if we're not in cli
            if (php_sapi_name() != 'cli') {
                Yii::app()->session['accountInfo'] = $result;
            }
        }

        return $result;
    }

    /**
     * Get emails from authorized recipients
     * @param string $accessTo -> reviews || appointment_request || tracked_calls || products_services
     * @param int $practiceId
     * @param int $providerId
     * @return array
     */
    public static function getEmailsFromAuthorizedRecipients($accessTo = '', $practiceId = 0, $providerId = 0)
    {

        $arrAccount = Account::getAccountBasedOnProviderAndPractice($providerId, $practiceId);
        if (!empty($accessTo) && !empty($arrAccount)) {
            // Check if those accounts have access to $accessTo (Eg: 'appointment_request')
            foreach ($arrAccount as $k => $v) {
                if ($v['connection'] == 'S') {
                    $accountInfo = Account::getInfo($v['account_id'], false, false);
                    $haveAccess = !empty($accountInfo['privileges'][$accessTo]) ? 1 : 0;
                    if ($haveAccess != 1) {
                        // if have no access, remove it
                        unset($arrAccount[$k]);
                    }
                } elseif (empty($v['connection'])) {
                    // Is a freemium user, so set as the owner
                    $arrAccount[$k]['connection'] = 'O';
                }
            }
        }

        // only return the account that have access to this privileges
        return $arrAccount;
    }

    /**
     * Look at account_practice / account_provider and detect if an account is ready to publish
     * @param str $provPrac -> provider || practice
     * @param int $profileId
     * @param int $accountId
     * @return boolean
     */
    public static function isReadyToPublish($provPrac, $profileId, $accountId)
    {
        $accountResearch = true;

        if ($provPrac == 'provider') {
            // pending providers to be published
            $sql = sprintf(
                "SELECT account_id
                FROM account_provider
                WHERE account_id = %d
                AND research_status <> 'Published'
                AND provider_id <> %d",
                $accountId,
                $profileId
            );
            $arrProviders = Yii::app()->db->createCommand($sql)->queryAll();

            if ($arrProviders) {
                $accountResearch = false;
            } else {
                $arrPracticesToPublish = ProviderPractice::canProviderBePublished($profileId, $accountId, true);
                // pending practices to be published
                if (!empty($arrPracticesToPublish[2])) {
                    $sql = sprintf(
                        "SELECT account_id
                        FROM account_practice
                        WHERE account_id = %d
                        AND research_status <> 'Published'
                        AND practice_id NOT IN (%s)",
                        $accountId,
                        implode(",", $arrPracticesToPublish[2])
                    );
                    $arrPractices = Yii::app()->db->createCommand($sql)->queryAll();
                    if ($arrPractices) {
                        $accountResearch = false;
                    }
                }
            }
        } else {
            // pending practices to be published
            $sql = sprintf(
                "SELECT account_id
                FROM account_practice
                WHERE account_id = %d
                AND research_status <> 'Published'
                AND practice_id <> %d",
                $accountId,
                $profileId
            );
            $arrPractices = Yii::app()->db->createCommand($sql)->queryAll();

            if ($arrPractices) {
                $accountResearch = false;
            } else {

                $arrProvidersToPublish = ProviderPractice::canPracticeBePublished($profileId, $accountId, true);
                //Eg: $arrProvidersToPublish = array($canBePublished, $practiceStatus, $arrayProviders);

                if (!empty($arrProvidersToPublish[2])) {
                    // pending providers to be published
                    $sql = sprintf(
                        "SELECT account_id
                        FROM account_provider
                        WHERE account_id = %d
                        AND research_status <> 'Published'
                        AND provider_id NOT IN (%s)",
                        $accountId,
                        implode(",", $arrProvidersToPublish[2])
                    );
                    $arrProviders = Yii::app()->db->createCommand($sql)->queryAll();
                    if ($arrProviders) {
                        $accountResearch = false;
                    }
                }
            }
        }
        return $accountResearch;
    }

    /**
     * Find out whether to show insurance options for the logged in account
     * (We don't for country_id or localization_id != 1)
     * @return bool
     */
    public static function showInsuranceOptions()
    {
        $account = Account::model()->cache(Yii::app()->params['cache_medium'])->findByPk(Yii::app()->user->id);
        $showInsuranceOptions = false;
        if (!empty($account) && $account->country_id == 1 && $account->localization_id == 1) {
            $showInsuranceOptions = true;
        }
        return $showInsuranceOptions;
    }

    /**
     * Update claimed value of all providers and practices related to the specified account
     * @param int $accountId
     */
    public static function updateClaimedValue($accountId)
    {
        // find all providers related to the account
        $arrProviders = AccountProvider::model()->findAll(
            'account_id = :account_id',
            [':account_id' => $accountId]
        );

        // update claimed of each provider
        foreach ($arrProviders as $ap) {
            AccountProvider::updateClaimed($ap->provider->id);
        }

        // find all practices related to the account
        $arrPractices = AccountPractice::model()->findAll(
            'account_id = :account_id',
            [':account_id' => $accountId]
        );

        // update claimed for each practice
        foreach ($arrPractices as $ap) {
            AccountPractice::updateClaimed($ap->practice->id);
        }
    }

    /**
     * AutoLogin to the RPADM
     * @param int $accountId
     * @param bool $redirect
     *
     */
    public static function autoLogin($accountId = 0, $redirect = false)
    {
        if ($accountId == 0) {
            return false;
        }

        // Search for the account
        $account = Account::model()->findByPk($accountId);
        if (empty($account)) {

            $results['success'] = false;
            $results['http_response_code'] = 422;
            $results['error'] = 'USER_NOT_EXISTS';
            $results['data'] = "The Account not found";
            ApiComponent::displayResults($results);
        }

        // similar to impersonate process
        $identity = new UserIdentity($account->email, $account->password);

        //did it authenticate ok?
        //yes
        if ($identity->authenticate(true)) {

            $accountInfo = Account::getInfo($accountId, false, false);

            Yii::app()->user->login($identity);

            // get last date_signed_in and then update
            Account::model()->updateByPk($accountId, array('date_signed_in' => date('Y-m-d H:i:s')));

            Yii::app()->user->setState('date_signed_in', $accountInfo['date_signed_in']);

            // Create new login Token
            $expiredTime = time() + (3600 * 24); // one day

            $accessToken = new AccessToken();
            $accessToken = $accessToken->generate($accountInfo);

            $options = array(
                'expire' => $expiredTime // expire in one day
            );

            // Delete cookies if exists
            // and Set new cookies for the RPADM
            Cookies::delete('accountId');
            Cookies::set('accountId', $accountId, $options);

            Cookies::delete('token');
            Cookies::set('token', $accessToken, $options);

            Cookies::delete('impersonateFlag');
            Cookies::set('impersonateFlag', 'false', $options);

            Cookies::delete('accountType');
            Cookies::set('accountType', $accountInfo['account_type_id'], $options);

            // Create token cookie to use in Telemedicine Office
            setcookie(Yii::app()->params['environment'] . '_token', false, -1, '/', 'doctor.com');
            Yii::app()->request->cookies[Yii::app()->params['environment'] . '_token'] = new CHttpCookie(
                Yii::app()->params['environment'] . '_token',
                $accessToken,
                array(
                    'domain' => 'doctor.com',
                    'expire' => time() + $expiredTime // expire in one day
                )
            );

            $domain = Yii::app()->params['servers']['dr_pro_hn'];
            // Create token cookie to use for external login (AAD)
            setcookie('token', false, -1, '/', $domain);
            Yii::app()->request->cookies['token'] = new CHttpCookie(
                'token',
                $accessToken,
                array(
                    'domain' => $domain,
                    'expire' => $expiredTime // expire in one day
                )
            );

            // The user was authenticated
            $results['success'] = true;
            $results['http_response_code'] = 200;
            $results['error'] = '';
            $results['data']['token'] = $accessToken;
            if ($redirect) {
                return $results;
            }

            ApiComponent::displayResults($results);
        }

        // The user couldnt be authenticated
        $results['success'] = false;
        $results['http_response_code'] = 422;
        $results['error'] = 'IDENTITY_ERROR';
        $results['data'] = $identity->errorMessage;
        ApiComponent::displayResults($results);
    }

    /**
     * Is the 'First Time Login' ?
     * @param int $accountId
     * @return bool
     */
    public static function firstTimeLogin($accountId, $isBetaAccount = 0)
    {
        if ($isBetaAccount == 1) {
            return 0;
        } else {
            $details = AccountDetails::getSettings($accountId);
            return !empty($details['data']['first_time_login']) ? $details['data']['first_time_login'] : 0;
        }
    }

    /**
     * Is a beta account?
     *
     * @param int $accountId
     * @return bool
     */
    public function isBetaAccount($accountId)
    {
        $account =  Account::model()->cache(Yii::app()->params['cache_short'])->findByPk($accountId);

        if (!empty($account) && $account->is_beta_account == 1) {
            return true;
        }

        return false;
    }

    /**
     * Is a freemium account?
     *
     * @param int $accountId
     * @return bool
     */
    public static function isFreemiumAccount($accountId)
    {
        // get queue from cache
        $queueId = "isFree_" . $accountId;
        $jsonQueue = Yii::app()->cache->get($queueId);
        $arrQueue = json_decode($jsonQueue, true);
        if (!isset($arrQueue['is_free'])) {
            // The cache is empty
            $organizationAccount =  OrganizationAccount::model()
                ->cache(Yii::app()->params['cache_short'])
                ->find('account_id=:account_id', array(':account_id' => $accountId));

            $arrQueue['is_free'] = true;
            if (!empty($organizationAccount)) {
                $arrQueue['is_free'] = false;
            }

            // update the entry in the cache
            $jsonQueue = json_encode($arrQueue);
            Yii::app()->cache->set($queueId, $jsonQueue, Yii::app()->params['cache_long']);
        }
        return $arrQueue['is_free'];
    }

    /**
     * Get partner data
     *
     * @param int $accountId
     * @return array
     */
    public static function getPartnerData($accountId)
    {
        // Initialize vars
        $accountTypeId = 0;
        $partnerSiteId = 0;
        $logoPath = "";
        $scanTemplateType = ScanTemplateType::TYPE_SALES;
        $scanTemplateTypeId = ScanTemplateType::TYPE_SALES_ID;
        $name = "";
        $displayName = "";

        // Get the account for the given id
        $account = Account::model()->findByPk($accountId);

        // Does the account exist?
        if ($account) {
            $accountTypeId = $account->account_type_id;
            // Does the account belong to a partner site?
            if ($account->partnerSite) {
                $partnerSiteId = $account->partnerSite->id;
                $name = $account->partnerSite->name;
                $displayName = $account->partnerSite->display_name;
                $logoPath = $account->partnerSite->logo_path;
                if (!empty($account->partnerSite->scanTemplateType)) {
                    $scanTemplateTypeId = $account->partnerSite->scanTemplateType->id;
                    $scanTemplateType = $account->partnerSite->scanTemplateType->name;
                }
            }
        }
        $arrSpecialties = ScanTemplate::getSpecialties($scanTemplateTypeId);

        // Build the result array
        return array(
            'account_id' => $accountId,
            'account_type_id' => $accountTypeId,
            'partner_site_id' => $partnerSiteId,
            'partner_site_name' => $name,
            'partner_site_display_name' => $displayName,
            'partner_site_logo_path' => $logoPath,
            'scan_template_type' => $scanTemplateType,
            'specialties' => $arrSpecialties,
        );
    }

    /**
     * Wether the account has virtual visit waiting rooms or not
     * @return bool
     */
    public function hasWaitingRooms()
    {
        $sql = sprintf(
            "SELECT IF(COUNT(w.id) > 0, 1, 0) AS has_rooms
            FROM account a
            INNER JOIN waiting_room_setting w ON a.id = w.account_id
            WHERE w.enabled = 1 AND a.id = %d",
            $this->id
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    public function getFullName($nameFirst = false)
    {
        $name = $this->last_name . ' ' . $this->first_name;
        if ($nameFirst) {
            $name = $this->first_name . ' ' . $this->last_name;
        }

        return $name;
    }
}
