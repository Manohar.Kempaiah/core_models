<?php

Yii::import('application.modules.core_models.models._base.BaseProviderRatingResponse');

class ProviderRatingResponse extends BaseProviderRatingResponse
{

    public $localization_id;

    /**
     * For filter by provider in search method
     * @var string
     */
    public $provider_id;

    /**
     * For filter by reviewer name in search method
     * @var string
     */
    public $reviewer_name;

    /**
     * For filter by reviewer email in search method
     * @var string
     */
    public $reviewer_email;

    /**
     * For filter by reviewer location in search method
     * @var string
     */
    public $reviewer_location;

    /**
     * For filter by rating in search method
     * @var int
     */
    public $rating_value;

    /**
     * For filter by origin in search method
     * @var int
     */
    public $origin;

    /**
     * For filter by partner in search method
     * @var int
     */
    public $partner_id;

    /**
     * For filter by partner site in search method
     * @var int
     */
    public $partner_site_id;

    /**
     * For filter by account device id or "verified" flag in search method
     * @var int
     */
    public $account_device_id;

    /**
     * For filter by organization ID in search method
     * @var int
     */
    public $organization_id;

    /**
     * For filter by organization name in search method
     * @var string
     */
    public $organization_name;

    /**
     * For filter by salesforce ID in search method
     * @var string
     */
    public $salesforce_id;

    /**
     * For filter by organization status in search method
     * @var string
     */
    public $organization_status;

    /**
     * Partner display name
     * @var string
     */
    public $partner_display_name;

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        $rating = ProviderRating::model()->findByPk($this->rating_id);
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $rating->provider_id .
            '" rel="/administration/providerTooltip?id=' . $rating->provider_id . '" title="'
            . $rating->provider . '">' . $rating->provider . '</a>');
    }

    /**
     * Declares the validation rules.
     * @return array
     */
    public function rules()
    {
        return array(
            array('rating_id, response', 'required'),
            array('rating_id', 'numerical', 'integerOnly' => true),
            array('status', 'length', 'max' => 1),
            array('status_comments', 'length', 'max' => 255),
            array('date_added, date_updated', 'safe'),
            array(
                'status, status_comments, date_added, date_updated, status_rejected_reason',
                'default',
                'setOnEmpty' => true,
                'value' => null
            ),
            array(
                'id, rating_id, provider_id, response, status, status_comments, date_added, date_updated'
                    . ', reviewer_name, reviewer_email, reviewer_location, rating_value, origin, partner_id'
                    . ', partner_site_id, account_device_id, organization_id, organization_name, salesforce_id'
                    . ', organization_status, localization_id, status_rejected_reason',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        $relations = parent::relations();
        $relations['provider'] = array(
            self::BELONGS_TO,
            'Provider',
            array('provider_id' => 'id'),
            'through' => 'rating'
        );
        return $relations;
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        $labels['localization_id'] = Yii::t('app', 'Localization');
        $labels['rating_value'] = Yii::t('app', 'Overall Rating');
        $labels['partner_site_id'] = Yii::t('app', 'Partner Site');
        $labels['organization_id'] = Yii::t('app', 'Organization ID');
        $labels['salesforce_id'] = Yii::t('app', 'Salesforce ID');

        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        if (strlen($this->rating_id)) {
            if (is_numeric($this->rating_id)) {
                $criteria->compare('rating_id', $this->rating_id);
            } else {
                $criteria->compare('rating.title', $this->rating_id, true);
            }
        }
        $criteria->compare('t.status', $this->status);
        $criteria->compare('t.date_added', $this->date_added, true);
        $criteria->compare('t.date_updated', $this->date_updated, true);
        $criteria->compare('rating.localization_id', $this->localization_id);

        if (strlen($this->provider_id)) {
            if (is_numeric($this->provider_id)) {
                $criteria->compare('provider.id', $this->provider_id);

            } else {
                $criteria->compare('provider.first_last', $this->provider_id, true);
            }
        }

        $criteria->compare('rating.reviewer_name', $this->reviewer_name, true);
        $criteria->compare('rating.reviewer_email', $this->reviewer_email, true);
        $criteria->compare('rating.reviewer_location', $this->reviewer_location, true);
        $criteria->compare('rating.rating', $this->rating_value, true);
        $criteria->compare('rating.rating', $this->rating_value, true);

        if ($this->origin == 'K') {
            $criteria->addCondition('rating.account_device_id IS NOT NULL');

        } else {
            $criteria->compare('rating.origin', $this->origin);
        }

        $criteria->compare('rating.partner_id', $this->partner_id);
        $criteria->compare('rating.partner_site_id', $this->partner_site_id);

        if ($this->account_device_id == 'Y') {
            $criteria->addCondition('rating.account_device_id IS NOT NULL');

        } elseif ($this->account_device_id == 'N') {
            $criteria->addCondition('rating.account_device_id IS NULL');

        } else {
            $criteria->compare('rating.account_device_id', $this->account_device_id);
        }

        $criteria->compare('organization.id', $this->organization_id);
        $criteria->compare('organization.organization_name', $this->organization_name, true);
        $criteria->compare('organization.salesforce_id', $this->salesforce_id, true);
        $criteria->compare('organization.status', $this->organization_status);

        $criteria->select = 't.*, rating.provider_id, rating.rating AS rating_value, organization.id AS organization_id'
            . ', organization.organization_name, organization.salesforce_id'
            . ', organization.status organization_status, partner.display_name AS partner_display_name';
        $criteria->join .= ' LEFT JOIN provider_rating AS rating ON rating.id = t.rating_id';
        $criteria->join .= ' LEFT JOIN partner ON partner.id = rating.partner_id';
        $criteria->join .= ' LEFT JOIN provider ON provider.id = rating.provider_id';
        $criteria->join .= ' LEFT JOIN account_provider ON account_provider.provider_id = rating.provider_id';
        $criteria->join .= ' LEFT JOIN organization_account'
            . ' ON organization_account.account_id = account_provider.account_id';
        $criteria->join .= ' LEFT JOIN organization ON organization.id = organization_account.organization_id';
        $criteria->group = 't.id';

        return new CActiveDataProvider(
            get_class($this),
            array(
                'criteria' => $criteria,
                'pagination' => array('pageSize' => 25)
            )
        );
    }

    /**
     * Needed for the extension ERememberFiltersBehavior, which saves the filters inputted after refreshing the page.
     * @return array
     */
    public function behaviors()
    {
        return array('ERememberFiltersBehavior' => array('class' => 'ERememberFiltersBehavior'));
    }

    /**
     * Renders the value shown in the grid for the "Provier's Response' column.
     * @return raw HTML to be used later on in the tooltip.
     */
    public function getResponseText()
    {
        $response = $this->response;
        if (strlen($response) > 35) {
            $response = substr($response, 0, 30) . "[...]";
        }
        return "<a class='commentTooltip' rel='/administration/responseTooltip?id=" . $this->id
        . "' title='Provider\'s Response'>" . $response . "</a>";
    }

    /**
     * Get the review response
     * @param int $reviewId
     * @return array
     */
    public static function getReviewResponse($reviewId = 0)
    {
        $sql = sprintf(
            "SELECT id, rating_id, response, `status`, status_comments, date_added, date_updated
            FROM provider_rating_response
            WHERE rating_id = %d
            ORDER BY date_added DESC;",
            $reviewId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * BeforeSave
     * @return boolean
     */
    public function beforeSave()
    {
        // If status Approved or Pending
        if ($this->status != 'R') {
            $this->status_rejected_reason = null;
            $this->status_comments = null;
        }

        // If Review is Rejected
        if ($this->status == "R") {
            // Reason is Required
            if ($this->status_rejected_reason == "") {
                $this->addError('status_rejected_reason', 'Status Rejected Reason is Required.');
                return false;
            }
            // IF Reason is 'S' or 'O', Comments are Required
            if (($this->status_rejected_reason == "S" || $this->status_rejected_reason == "O")
                && $this->status_comments == "") {
                $this->addError('status_comments', 'Status Comments is Required.');
                return false;
            }
        }
        return parent::beforeSave();
    }

}
