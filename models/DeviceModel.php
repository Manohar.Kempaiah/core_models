<?php

Yii::import('application.modules.core_models.models._base.BaseDeviceModel');

class DeviceModel extends BaseDeviceModel
{
    const REVIEW_REQUEST_ID = 7;

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('serial_length', 'numerical', 'integerOnly' => true),
            array('name', 'length', 'max' => 255),
            array('status', 'length', 'max' => 1),
            array('serial_starts', 'length', 'max' => 10),
            array('serial_starts', 'length', 'max' => 10),
            array('status, serial_length, serial_starts', 'default',
                'setOnEmpty' => true, 'value' => null),
            array('id, name, status, serial_length, serial_starts', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'status' => Yii::t('app', 'Status'),
            'serial_length' => Yii::t('app', 'Serial length'),
            'serial_starts' => Yii::t('app', 'Serial starts with'),
            'devices' => null,
            'deviceModelCount' => Yii::t('app', 'Quantity of Devices'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('status', $this->status, true);
        $criteria->compare('serial_length', $this->serial_length);
        $criteria->compare('serial_starts', $this->serial_starts, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'pagination' => array('pageSize' => 100),
        ));
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from device if associated records exist in device table
        $has_associated_records = Device::model()->exists(
            'device_model_id=:device_id',
            array(':device_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Device model can't be deleted because there are devices linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Count devices of this model
     * @return int
     */
    public function getDeviceModelCount()
    {
        return Device::model()->cache(Yii::app()->params['cache_medium'])->count(
            'device_model_id=:device_model_id',
            array(':device_model_id' => $this->id)
        );
    }

}
