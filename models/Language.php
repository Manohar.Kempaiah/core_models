<?php

Yii::import('application.modules.core_models.models._base.BaseLanguage');

class Language extends BaseLanguage
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('primary', $this->primary);

        return new CActiveDataProvider(get_class($this), array('criteria' => $criteria, 'pagination' => array('pageSize' => 100)));
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from language if associated records exist in provider_language table
        $has_associated_records = ProviderLanguage::model()->exists(
            'language_id=:language_id',
            array(':language_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Language can't be deleted because there are providers linked to it.");
            return false;
        }

        // avoid deleting from language if associated records exist in practice_language table
        $has_associated_records = PracticeLanguage::model()->exists(
            'language_id=:language_id',
            array(':language_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Language can't be deleted because there are providers linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Load keyword from all tranlations files
     * @params array $arrKeywords
     * @params array $arrLanguages
     * @return array
     */
    public static function loadKeyword($arrLanguages = '', $arrKeywords = '')
    {
        if (empty($arrLanguages)) {
            $arrLanguages = Common::getAvailableLanguages();
        }
        if (empty($arrLanguages)) {
            return null;
        }

        // Extract default text for each languages
        $dataMessage = Yii::app()->getMessages();
        $basePath = $dataMessage->basePath;
        $dataReturn = array();
        foreach ($arrLanguages as $lang => $name) {
            $fileToInclude = $basePath . '/' . $lang . '/kiosk.php';
            if (!file_exists($fileToInclude)) {
                $fileToInclude = $basePath . '/en/kiosk.php';
            }
            $fileTranslation = include($fileToInclude);
            if (!empty($arrKeywords)) {
                foreach ($arrKeywords as $keyword) {
                    $dataReturn[$lang][$keyword] = !empty($fileTranslation[$keyword]) ? $fileTranslation[$keyword] : '';
                }
            } else {
                // Load all keywords
                $dataReturn[$lang] = !empty($fileTranslation) ? $fileTranslation : '';
            }
        }
        return $dataReturn;
    }

    public function findByPartnerSite($partnerSiteId)
    {
        $connection = Yii::app()->db;

        $sql = sprintf(
            "SELECT DISTINCT(l.name), l.id
            FROM prd01.language l
            INNER JOIN prd01.provider_language pl ON pl.language_id = l.id
            INNER JOIN prd01.account_provider ap on ap.provider_id = pl.provider_id
            INNER JOIN prd01.organization_account oa on oa.account_id = ap.account_id
            INNER JOIN prd01.organization o on o.id = oa.organization_id
            WHERE o.partner_site_id = %u
            ORDER BY l.name",
            $partnerSiteId
        );

        return $connection->createCommand($sql)->queryAll();
    }

}
