<?php

Yii::import('application.modules.core_models.models._base.BasePaymentType');

class PaymentType extends BasePaymentType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from payment_type if associated records exist in provider_payment_type table
        $has_associated_records = ProviderPaymentType::model()->exists(
            'payment_type_id=:payment_type_id',
            array(
                ':payment_type_id' => $this->id
            )
        );
        if ($has_associated_records) {
            $this->addError("id", "Payment type can't be deleted because there are payments linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

}
