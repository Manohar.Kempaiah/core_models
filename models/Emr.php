<?php

Yii::import('application.modules.core_models.models._base.BaseEmr');

class Emr extends BaseEmr
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from EMR if associated records exist in practice_details table
        $has_associated_records = PracticeDetails::model()->exists('emr_id=:emr_id', array(':emr_id' => $this->id));
        if ($has_associated_records) {
            $this->addError("id", "EMR data can't be deleted because there are practices linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Find all EHRs that practices belonging to the organization's owner account marked as using
     * @param int $organizationId
     * @return string
     */
    public static function getOrganizationEhrs($organizationId)
    {
        $sql = sprintf(
            "SELECT GROUP_CONCAT(DISTINCT(emr.name) SEPARATOR ', ') AS ehr_names
            FROM emr
            INNER JOIN practice_details
            ON practice_details.emr_id = emr.id
            INNER JOIN account_practice
            ON practice_details.practice_id = account_practice.practice_id
            INNER JOIN organization_account
            ON account_practice.account_id = organization_account.account_id
            WHERE organization_id = %d;",
            $organizationId
        );
        $ehrNames = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
        if ($ehrNames == 'No') {
            // same as nothing
            return '';
        }
        return $ehrNames;
    }

}
