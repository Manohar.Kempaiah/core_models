<?php

/**
 * @see protected/controllers/MailComponent.php
 * This model servers several information used at mailComponent.php
 */
class Mail
{

    /**
     * Get an account's details for sending an email referencing it
     * @param int $accountId
     * @return array
     */
    public static function getAccount($accountId = 0)
    {

        $sql = sprintf(
            "SELECT account_type.name AS a_account_type, email AS a_email, first_name AS a_first_name,
            last_name AS a_last_name, IF(organization_name IS NOT NULL, organization_name,
            CONCAT(first_name, ' ', last_name)) AS a_organization_name, position.name AS a_position,
            address AS a_address, address_2 AS a_address_2, location.zipcode AS a_zipcode,
            phone_number AS a_phone_number, fax_number AS a_fax_number, DATE_FORMAT(date_added, '%%m/%%d/%%Y') AS
            a_date_added, partner_site_id,
            DATE_FORMAT(date_updated, '%%m/%%d/%%Y') AS a_date_updated, email_cc AS a_email_cc, account.id AS a_id,
            IF(organization_name IS NOT NULL, organization_name,
            CONCAT(first_name, ' ', last_name)) AS a_practice
            FROM account
            INNER JOIN account_type
                ON account.account_type_id = account_type.id
            LEFT JOIN position
                ON account.position_id = position.id
            LEFT JOIN location
                ON account.location_id = location.id
            WHERE account.id = %d
            LIMIT 1;",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get a provider's details for sending an email referencing it
     * @param int $providerId
     * @return array
     */
    public static function getProvider($providerId = 0)
    {

        $server = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'www.doctor.com';

        // TODO: generate size url properly with createUrl()
        $sql = sprintf(
            "SELECT npi_id AS p_npi, credential.title AS p_credential, prefix AS p_prefix,
            provider.first_name AS p_first_name, provider.last_name AS p_last_name, docscore AS p_docpoints,
            avg_rating AS p_avg_rating, IF(photo_path != '',
            CONCAT('https://%s/site/resize?p=/images/uploads/providers/', photo_path), '') AS p_photo_path,
            CONCAT('https://%s/', friendly_url) AS p_friendly_url,
            DATE_FORMAT( provider.date_added, '%%m/%%d/%%Y') AS p_date_added,
            DATE_FORMAT( provider.date_updated, '%%m/%%d/%%Y') AS p_date_updated,
            DATE_FORMAT(account.date_signed_in, '%%m/%%d/%%Y') AS p_date_signed_in,
            IF(featured, 'Featured', 'Basic') AS p_featured,
            photo_path AS plain_photo_path
            FROM provider
                LEFT JOIN credential
                    ON provider.credential_id = credential.id
                LEFT JOIN account_provider
                    ON provider.id = account_provider.provider_id
                LEFT JOIN account
                    ON account.id = account_provider.account_id
            WHERE provider.id = %d
            LIMIT 1;",
            $server,
            Yii::app()->params->servers['dr_web_hn'],
            $providerId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get a manual validation's details for sending an email referencing it
     * @param int $manualValidationId
     * @return array
     */
    public static function getManualValidation($manualValidationId = 0)
    {

        $sql = sprintf(
            "SELECT account.email AS m_account_name,
            CONCAT(provider.prefix, ' ', provider.first_name, ' ', provider.last_name) AS m_provider_name, practice.name
            AS m_practice_name,
            manual_validation.first_name AS m_first_name, manual_validation.last_name AS m_last_name,
            manual_validation.address AS m_address,
            location.zipcode AS m_zipcode, manual_validation.email AS m_email, manual_validation.phone_number AS
            m_phone_number,
            IF(manual_validation.status = 'A', 'Approved', IF(manual_validation.status = 'P', 'Pending', 'Rejected'))
            AS m_status, manual_validation.validation_type AS m_validation_type, manual_validation.status_comments AS
            m_status_comments,
            DATE_FORMAT(manual_validation.date_added, '%%m/%%d/%%Y') AS m_date_added,
            DATE_FORMAT(manual_validation.date_updated, '%%m/%%d/%%Y') AS m_date_updated
            FROM manual_validation
                LEFT JOIN account
                    ON manual_validation.account_id = account.id
                LEFT JOIN provider
                    ON manual_validation.provider_id = provider.id
                LEFT JOIN practice
                    ON manual_validation.practice_id = practice.id
                LEFT JOIN location
                    ON manual_validation.location_id = location.id
            WHERE manual_validation.id = %d
            LIMIT 1;",
            $manualValidationId
        );
        return Yii::app()->dbRO->createCommand($sql)->queryRow();
    }

    /**
     * Get details for an online appointment request via email to send the actual email
     * Approve and reschedule links will be generated at runtime
     * @param int $schedulingMailId
     * @return array
     */
    public static function getScheduleMail($schedulingMailId = 0)
    {

        $sql = sprintf(
            "SELECT
                scheduling_mail.id AS s_id,
                first_name AS s_first_name,
                last_name AS s_last_name,
                CONCAT(LEFT(last_name, 1), '.') AS s_last_initial,
                cell_phone AS s_cell_phone,
                scheduling_mail.email AS s_email,
                scheduling_mail.self_paying,
                provider_details.no_insurance_reason,
                DATE_FORMAT(appointment_date, '%%a, %%M %%D, %%Y') AS s_appointment_date,
                CONCAT(DATE_FORMAT(appointment_date, '%%c/%%e/%%Y'), ' ', IF(appointment_type = 'S', time_start,
                IF(date_time = 'M', 'Morning', IF(date_time = 'A', 'Afternoon', 'Evening')))) AS s_date_time,
                IF(prescription = 1, 'Yes', 'No') AS s_prescription,
                reason AS s_reason,
                CONCAT(insurance_company.name, ' - ', IF(scheduling_mail.insurance_other_plan IS NULL,
                insurance.name,
                scheduling_mail.insurance_other_plan)) AS s_insurance,
                source AS s_source,
                scheduling_mail.status AS s_status,
                scheduling_mail.time_start AS s_time_start,
                scheduling_mail.time_end AS s_time_end,
                '' AS s_formatted_time_start,
                '' AS s_formatted_time_end,
                appointment_type AS s_appointment_type,
                practice_id AS s_practice_id,
                patient_id AS s_patient_id,
                appointment_date AS s_unformatted_appointment_date,
                time_start AS s_unformatted_appointment_time_start,
                time_end AS s_unformatted_appointment_time_end,
                vr.name AS s_visit_reason,
                IF(reminder_mail = '1', 'Mail reminder','') AS s_reminder_mail,
                IF(reminder_sms = '1', 'SMS reminder','') AS s_reminder_sms,
                IF(reminder_autocall = '1', 'AutoCall reminder','') AS s_reminder_autocall,
                IF(reminder_staffcall = '1', 'StaffCall reminder','') AS s_reminder_staffcall,
                scheduling_mail.provider_id, partner_site_id AS s_partner_site_id,
                insurance_company.name AS insurance_name,
                insurance_company.id AS insurance_company_id,
                insurance.name AS plan_name,
                insurance_plan_types.name AS plan_type_name,
                scheduling_mail.provider_id AS s_provider_id,
                '' AS s_approve_link,
                '' AS s_reschedule_link,
                '' AS s_partner_name
            FROM scheduling_mail
            LEFT JOIN insurance ON scheduling_mail.insurance_id = insurance.id
            LEFT JOIN insurance_company ON scheduling_mail.insurance_company_id = insurance_company.id
            LEFT JOIN insurance_plan_types ON scheduling_mail.insurance_plan_types_id = insurance_plan_types.id
            LEFT JOIN visit_reason AS vr ON scheduling_mail.visit_reason_id = vr.id
            LEFT JOIN provider_details ON scheduling_mail.provider_id = provider_details.provider_id
            WHERE scheduling_mail.id = %d;",
            $schedulingMailId
        );
        $dataResult = Yii::app()->db->createCommand($sql)->queryRow();

        if (empty($dataResult)) {
            return null;
        }

        // Get the correct source based on partner site
        $partnerSite = PartnerSite::model()->cache(Yii::app()->params['cache_short'])->findByPk($dataResult['s_partner_site_id']);
        if (!empty($partnerSite)) {
            if (!empty($partnerSite->display_name)) {
                $dataResult['s_source'] = $partnerSite->display_name;
            }
            if (!empty($partnerSite->name)) {
                // To match on the template->internal_name
                $dataResult['s_partner_name'] = strtoupper(trim($partnerSite->name));
            }
        }

        if (!empty($dataResult['s_time_start'])) {
            $dataResult['s_formatted_time_start'] = date("g:i a", strtotime($dataResult['s_time_start']));
        }
        if (!empty($dataResult['s_time_end'])) {
            $dataResult['s_formatted_time_end'] = date("g:i a", strtotime($dataResult['s_time_end']));
        }

        if (empty($dataResult['s_formatted_time_start'])) {
            $dataResult['s_formatted_time_start'] = (!empty($dataResult['s_date_time']) ? $dataResult['s_date_time'] :
                null);
        }
        if (empty($dataResult['s_formatted_time_end'])) {
            $dataResult['s_formatted_time_end'] = (!empty($dataResult['s_date_time']) ? $dataResult['s_date_time'] :
                null);
        }
        if (empty($dataResult['s_reason'])) {
            $dataResult['s_reason'] = 'None provided';
        }

        if (empty($dataResult['s_appointment_type'])) {
            $dataResult['s_appointment_type'] = 'S';
        }
        if (empty($dataResult['s_insurance'])) {
            if ($dataResult['s_appointment_type'] == 'R') {
                $dataResult['s_insurance'] = 'Unspecified';
            } else {
                if (!empty($dataResult['insurance_name'])) {
                    $dataResult['s_insurance'] = $dataResult['insurance_name'];
                } else {
                    $dataResult['s_insurance'] = 'Self-paying';
                }
            }
        }

        if ($dataResult['self_paying'] == 1) {
            $dataResult['s_insurance'] = 'Self-paying';
        }

        $dataResult['insurance'] = $dataResult['insurance_name'];
        unset($dataResult['insurance_name']);

        return $dataResult;
    }

    /**
     * Get details for an online appointment request via email to send the actual email
     * @param int $schedulingMailId
     * @return array
     */
    public static function getScheduleEHRMail($schedulingMailId = 0)
    {

        $sql = sprintf(
            "SELECT appointment.id AS s_id,
            first_name AS s_first_name,
            last_name AS s_last_name,
            CONCAT(LEFT(last_name, 1), '.') AS s_last_initial,
            cell_phone AS s_cell_phone,
            email AS s_email,
            DATE_FORMAT(appointment_date, '%%c/%%e/%%Y') AS s_appointment_date,
            CONCAT(DATE_FORMAT(appointment_date, '%%c/%%e/%%Y'), ' ', time_start) AS s_date_time,
            'No' AS s_prescription,
            reason AS s_reason,
            '' AS s_insurance,
            '' AS s_source,
            appointment.status AS s_status,
            appointment.time_start AS s_time_start,
            appointment.time_end AS s_time_end,
            '' AS s_formatted_time_start,
            '' AS s_formatted_time_end,
            '' AS s_appointment_type,
            appointment.practice_id AS s_practice_id,
            patient_id AS s_patient_id,
            appointment_date AS s_unformatted_appointment_date,
            time_start AS s_unformatted_appointment_time_start,
            time_end AS s_unformatted_appointment_time_end,
            '' AS s_visit_reason,
            IF(pre_appt_reminder_mail = '1', 'Mail reminder','') AS s_reminder_mail,
            IF(pre_appt_reminder_sms = '1', 'SMS reminder','') AS s_reminder_sms,
            IF(pre_appt_reminder_autocall = '1', 'AutoCall reminder','') AS s_reminder_autocall,
            IF(pre_appt_reminder_staffcall = '1', 'StaffCall reminder','') AS s_reminder_staffcall,
            provider_id, '1' AS s_partner_site_id,
            provider_id AS s_provider_id,
            '' AS s_approve_link,
            '' AS s_reschedule_link
            FROM appointment
            INNER JOIN account_practice_emr
            ON appointment.practice_id = account_practice_emr.practice_id
            WHERE appointment.id = %d;",
            $schedulingMailId
        );
        $dataResult = Yii::app()->db->createCommand($sql)->queryRow();

        if (!empty($dataResult['s_time_start'])) {
            $dataResult['s_formatted_time_start'] = date("g:i a", strtotime($dataResult['s_time_start']));
        }
        if (!empty($dataResult['s_time_end'])) {
            $dataResult['s_formatted_time_end'] = date("g:i a", strtotime($dataResult['s_time_end']));
        }

        if (empty($dataResult['s_formatted_time_start'])) {
            $dataResult['s_formatted_time_start'] = (!empty($dataResult['s_date_time']) ? $dataResult['s_date_time'] :
                null);
        }
        if (empty($dataResult['s_formatted_time_end'])) {
            $dataResult['s_formatted_time_end'] = (!empty($dataResult['s_date_time']) ? $dataResult['s_date_time'] :
                null);
        }
        if (empty($dataResult['s_reason'])) {
            $dataResult['s_reason'] = 'None provided';
        }

        return $dataResult;
    }

    /**
     * Return information about a response to a rating
     * @param int $reviewId
     * @return array
     */
    public static function getResponse($reviewId = 0)
    {
        $sql = sprintf(
            "SELECT a.id, a.rating_id, b.reviewer_name,
            b.reviewer_email, GROUP_CONCAT(d.email SEPARATOR ', ') AS account_email
            FROM provider_rating_response a
            INNER JOIN provider_rating b
            ON a.rating_id = b.id
            INNER JOIN account_provider c
            ON b.provider_id = c.provider_id
            INNER JOIN account d
            ON c.account_id = d.id
            WHERE a.id =  %d
            LIMIT 1;",
            $reviewId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Obtain informatio to  "review response approved to patient" when a provider response is approved by the admin.
     * @param int $reviewId
     * @return array
     */
    public static function getResponseToPatient($reviewId = 0)
    {
        $sql = sprintf(
            "SELECT
            a.id, a.rating_id, a.response,
            b.reviewer_name, b.reviewer_email, b.title,
            e.friendly_url, e.first_middle_last as provider_name,
            d.email account_email, f.url, f.display_name as site_name
            FROM provider_rating_response a
            INNER JOIN provider_rating b
            ON a.rating_id = b.id
            INNER JOIN account_provider c
            ON b.provider_id = c.provider_id
            INNER JOIN account d
            ON c.account_id = d.id
            INNER JOIN provider e
            ON b.provider_id = e.id
            INNER JOIN partner_site f
            ON b.partner_id = f.partner_id
            WHERE a.id =  %d
            LIMIT 1;",
            $reviewId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Return information about a practice
     * @param int $practiceId
     * @param bool $isNewTemplate
     * @return array
     */
    public static function getPractice($practiceId = 0, $isNewTemplate = false)
    {
        $sql = sprintf(
            "SELECT IF(practice_details.practice_type_id = 5, 'Online Visit', practice.name) AS p_practice,
            email AS p_email, address_full AS p_address_full,
            phone_number AS p_phone_number, city.name AS p_city, state.abbreviation AS p_state_abbreviation,
            zipcode AS p_zipcode, country.name AS p_country, logo_path AS p_logo_path,
            address AS p_address, practice.website AS p_website_url, '' AS p_google_maps_link,
            '' AS p_website_link, ROUND(AVG(rating), 1) AS p_rating, practice_details.practice_type_id AS
            p_practice_type_id
            FROM practice
            LEFT JOIN location
            ON practice.location_id = location.id
            LEFT JOIN city
            ON location.city_id = city.id
            LEFT JOIN state
            ON city.state_id = state.id
            LEFT JOIN country
            ON country.id = state.country_id
            LEFT JOIN provider_rating
            ON practice.id = provider_rating.practice_id
            AND origin = 'D'
            AND status = 'A'
            LEFT JOIN practice_details
            ON practice.id = practice_details.practice_id
            WHERE practice.id = %d
            LIMIT 1;",
            $practiceId
        );
        $arrPractice = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();
        // do we have ratings?
        if (empty($arrPractice['p_rating'])) {
            // no, use 0
            $arrPractice['p_rating'] = 0;
        }

        // If rating has more than 3 stars show it, otherwise don't
        if ($arrPractice['p_rating'] > 3) {
            $stars = AssetsComponent::showStars($arrPractice['p_rating']);
            $arrPractice['p_rating'] = $stars . $arrPractice['p_rating'];
        } else {
            $arrPractice['p_rating'] = '';
        }


        if (!empty($arrPractice['p_phone_number'])) {
            $arrPractice['p_phone_number'] = StringComponent::formatPhone($arrPractice['p_phone_number']);
        }

        // do we have a logo?
        if (!empty($arrPractice['p_logo_path'])) {
            // yes, use it with size E unless we're using the new ReviewHub templates, which is size R
            $arrPractice['p_logo_path'] = AssetsComponent::generateImgSrc(
                $arrPractice['p_logo_path'],
                'practices',
                ($isNewTemplate ? 'R' : 'E'),
                true,
                false,
                ($isNewTemplate ? false : true)
            );
        } elseif (!$isNewTemplate) {
            // no, use placeholder - but not for the new templates, which act different when there's no logo
            // @see templates PATIENT_MESSAGING and PATIENT_MESSAGING_nologo
            $arrPractice['p_logo_path'] = 'https://assets.doctor.com/email/practice_default.png';
        }

        if (!empty($arrPractice['p_website_url'])) {
            $arrPractice['p_website_url'] = StringComponent::sanitizeUrl($arrPractice['p_website_url']);
        }

        $addressFull = (!empty($arrPractice['p_address_full']) ? trim($arrPractice['p_address_full']) : '');
        $cityName = (!empty($arrPractice['p_city']) ? trim($arrPractice['p_city']) : '');
        $abbreviation = (!empty($arrPractice['p_state_abbreviation']) ? trim($arrPractice['p_state_abbreviation']) :
            '');
        $zipCode = (!empty($arrPractice['p_zipcode']) ? trim($arrPractice['p_zipcode']) : '');
        $countryName = (!empty($arrPractice['p_country']) ? trim($arrPractice['p_country']) : '');

        $arrPractice['p_address_link'] = $addressFull . ', ' . $cityName . ', ' . $abbreviation . ' ' . $zipCode;
        if (empty($cityName) && empty($abbreviation) && empty($zipCode) && empty($countryName)) {
            $arrPractice['p_google_maps_link'] = '';
            $arrPractice['p_google_maps_image'] = '';
            $arrPractice['p_google_maps_url'] = '';
        } else {
            $strLocation = trim($arrPractice['p_address']) . ' ' . $cityName . ", " . $abbreviation . " " . $zipCode .
                ', ' . $countryName;

            $googleMapsUrl = 'https://www.google.com/maps/place/' . urlencode($strLocation);

            $arrPractice['p_address_link'] = '<a href="' . $googleMapsUrl . '">' . $arrPractice['p_address_link'] . '</a>';
            $arrPractice['p_google_maps_link'] = '<a href="' . $googleMapsUrl . '">View on Map</a>';

            $mapOptions = array(
                'key' => Yii::app()->params['embed_map_google_api_key'],
                'zoom' => 15,
                'size' => '410x205',
                'maptype' => 'roadmap',
                'center' => $strLocation,
                'markers' => 'color:red|' . $strLocation
            );
            $arrPractice['p_google_maps_image'] = 'https://maps.googleapis.com/maps/api/staticmap?' . http_build_query($mapOptions);

            $arrPractice['p_google_maps_url'] = $googleMapsUrl;
        }

        // try to build website link
        if (empty($arrPractice['p_website_url'])) {
            $arrPractice['p_website_link'] = '';
        } else {
            $arrPractice['p_website_link'] = '<a href="' . $arrPractice['p_website_url'] . '">' .
                $arrPractice['p_website_url'] . '</a>';
        }

        return $arrPractice;
    }

    /**
     * Return information about a review, including links for sharing
     * @param int $ratingId
     * @return array
     */
    public static function getRating($ratingId = 0)
    {

        // @TODO: get rating text from ProviderRating::getWaitTimeMessage()
        $sql = sprintf(
            "SELECT provider_rating.id AS t_provider_rating_id, provider.prefix AS t_provider_prefix,
            provider.first_name AS t_provider_first_name,
            provider.last_name AS t_provider_last_name, credential.title AS t_credential, partner.name AS
            t_partner_name,
            partner_site.name AS t_partner_site_name, reviewer_name AS t_reviewer_name, reviewer_name_anon AS
            t_display_reviewer_name,
            IF(reviewer_name_anon = 3, '%s', IF(reviewer_name_anon = 2, '%s', '%s')) AS t_display_reviewer_name_as,
            reviewer_email AS t_reviewer_email, reviewer_location AS t_reviewer_location,
            rating AS t_rating, provider_rating.title AS t_rating_title, provider_rating.description AS
            t_rating_description,
            rating_getting_appt AS t_rating_getting_appt, rating_appearance AS t_rating_appearance, rating_courtesy AS
            t_rating_courtesy,
            IF(rating_waiting_time = 5, '%s', IF(rating_waiting_time = 4, '%s', IF(rating_waiting_time = 3, '%s',
            IF(rating_waiting_time = 2, '%s', IF(rating_waiting_time = 1, '%s', '%s'))))) AS t_rating_waiting_time,
            rating_billing AS t_rating_billing,
            provider_rating.date_added AS t_date_added, provider_rating.date_updated AS t_date_updated,
            patient.email AS u_email,
            patient.first_name AS u_first_name, patient.last_name AS u_last_name, patient.id AS patient_id,
            IF(provider_facebook.url IS NOT NULL, provider_facebook.url, practice_facebook.url) AS u_facebook,
            IF(provider_google.url IS NOT NULL, provider_google.url, practice_google.url) AS u_google_review,
            IF(provider_linkedin.url IS NOT NULL, provider_linkedin.url, practice_linkedin.url) AS u_linkedin,
            IF(provider_twitter.url IS NOT NULL, provider_twitter.url, practice_twitter.url) AS u_twitter,
            IF(provider_yelp.url IS NOT NULL, provider_yelp.url, practice_yelp.url) AS u_yelp_review,
            practice.name AS u_practice_name, provider_rating.provider_id AS u_provider_id, practice.id AS u_practice_id
            FROM provider_rating
                INNER JOIN provider
                    ON provider_rating.provider_id = provider.id
                LEFT JOIN provider_practice_listing AS provider_facebook
                    ON (provider.id = provider_facebook.provider_id
                    AND provider_facebook.listing_type_id = 16) /* Facebook */
                LEFT JOIN provider_practice_listing AS provider_google
                    ON (provider.id = provider_google.provider_id
                    AND provider_google.listing_type_id = 21) /* Google+ */
                LEFT JOIN provider_practice_listing AS provider_linkedin
                    ON (provider.id = provider_linkedin.provider_id
                    AND provider_linkedin.listing_type_id = 22) /* LinkedIn */
                LEFT JOIN provider_practice_listing AS provider_twitter
                    ON (provider.id = provider_twitter.provider_id
                    AND provider_twitter.listing_type_id = 39) /* Twitter */
                LEFT JOIN provider_practice_listing AS provider_yelp
                    ON (provider.id = provider_yelp.provider_id
                    AND provider_yelp.listing_type_id = %d) /* Yelp */
                INNER JOIN credential
                    ON provider.credential_id = credential.id
                LEFT JOIN partner
                    ON provider_rating.partner_id = partner.id
                LEFT JOIN partner_site
                    ON provider_rating.partner_site_id = partner_site.id
                LEFT JOIN patient
                    ON provider_rating.patient_id = patient.id
                LEFT JOIN account_device_provider
                    ON provider_rating.provider_id = account_device_provider.id
                LEFT JOIN account_device
                    ON provider_rating.account_device_id= account_device.id
                LEFT JOIN practice
                    ON practice.id = account_device.practice_id
                LEFT JOIN provider_practice_listing AS practice_facebook
                    ON (practice.id = practice_facebook.practice_id
                    AND practice_facebook.listing_type_id = 16) /* Facebook */
                LEFT JOIN provider_practice_listing AS practice_google
                    ON (practice.id = practice_google.practice_id
                    AND practice_google.listing_type_id = 21) /* Google+ */
                LEFT JOIN provider_practice_listing AS practice_linkedin
                    ON (practice.id = practice_linkedin.practice_id
                    AND practice_linkedin.listing_type_id = 22) /* LinkedIn */
                LEFT JOIN provider_practice_listing AS practice_twitter
                    ON (practice.id = practice_twitter.practice_id
                    AND practice_twitter.listing_type_id = 39) /* Twitter */
                LEFT JOIN provider_practice_listing AS practice_yelp
                    ON (practice.id = practice_yelp.practice_id
                    AND practice_yelp.listing_type_id = %d) /* Yelp */
            WHERE provider_rating.id = %d
            LIMIT 1;",
            Yii::t('kiosk', "lbl_show_name_a"),
            Yii::t('kiosk', "lbl_show_name_b"),
            Yii::t('kiosk', "lbl_show_name_c"),
            Yii::t('kiosk', "waiting_time_5"),
            Yii::t('kiosk', "waiting_time_4"),
            Yii::t('kiosk', "waiting_time_3"),
            Yii::t('kiosk', "waiting_time_2"),
            Yii::t('kiosk', "waiting_time_1"),
            Yii::t('kiosk', "waiting_time_0"),
            ListingType::YELP_PROFILE_ID,
            ListingType::YELP_PROFILE_ID,
            $ratingId
        );
        return Yii::app()->dbRO->createCommand($sql)->queryRow();
    }

    /**
     * Get a patient for sending an email referencing it
     * Some empty values will be replaced at runtime
     *
     * @param int $patientId
     * @return array
     */
    public static function getPatient($patientId = 0)
    {
        $sql = sprintf(
            "SELECT email AS u_email, first_name AS u_first_name, last_name AS u_last_name,
            mobile_phone AS u_mobile_phone, location_id AS u_location_id, lang_id AS u_lang_id,
            id AS u_id, '' AS u_password, '' AS u_review_description,
            '' AS u_social_links
            FROM patient
            WHERE id = %d
            LIMIT 1;",
            $patientId
        );
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Try to retrieve an email address for a given schedulingMail entry as follows:
     * 1. provider_practice email
     * 2. practice email
     * 3. provider email
     * 4. account email for any affiliated account
     * @param int $schedulingMailId
     * @return string
     */
    public static function getReplyTo($schedulingMailId)
    {

        $schedulingMail = SchedulingMail::model()->findByPk($schedulingMailId);
        if (!$schedulingMail) {
            return false;
        }

        if ($schedulingMail->provider_id > 0 && $schedulingMail->practice_id > 0) {
            $providerPractice = ProviderPractice::model()->find(
                'provider_id=:provider_id AND practice_id=:practice_id',
                array(':provider_id' => $schedulingMail->provider_id, ':practice_id' => $schedulingMail->practice_id)
            );
            if ($providerPractice && !empty($providerPractice->email)) {
                return $providerPractice->email;
            }
        }

        if ($schedulingMail->practice_id > 0) {
            $practice = Practice::model()->findByPk($schedulingMail->practice_id);
            if ($practice && !empty($practice->email)) {
                return $practice->email;
            }
        }

        if ($schedulingMail->provider_id > 0) {
            $provider = Provider::model()->findByPk($schedulingMail->provider_id);
            if (!empty($provider)) {
                $providerDetails = ProviderDetails::model()->find(
                    'provider_id=:provider_id',
                    array(':provider_id' => $provider->id)
                );
                if ($providerDetails && !empty($providerDetails->email)) {
                    return $providerDetails->email;
                }
            }

            $arrAccountProviders = AccountProvider::model()->findAll(
                array(
                    'condition' => 'provider_id=:provider_id',
                    'params' => array(':provider_id' => $schedulingMail->provider_id),
                    'order' => 'id DESC'
                )
            );

            if (empty($arrAccountProviders)) {
                return false;
            }
            foreach ($arrAccountProviders as $accountProvider) {
                $account = Account::model()->findByPk($accountProvider->account_id);
                if ($account && !empty($account->email)) {
                    return $account->email;
                }
            }
        }

        return false;
    }
}
