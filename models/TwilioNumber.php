<?php

Yii::import('application.modules.core_models.models._base.BaseTwilioNumber');

class TwilioNumber extends BaseTwilioNumber
{

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Set default values
     */
    public function init()
    {
        // by default, we want the interface and email to be enabled
        // but call recording to be disabled
        $this->enabled_interface = 1;
        $this->enabled_email = 1;
        $this->enabled_recording = 0;
        return parent::init();
    }

    /**
     * Before adding or modifying a twilio_number entry, check that the provider+practice+partner key isn't repeated
     * (unless it's a dynamic number generated via Dynamic Number Insertion (DNI), in which case this is expected)
     * @return boolean
     */
    public function beforeValidate()
    {

        // is this a non-dynamic number?
        if (!$this->is_dynamic) {
            // yes, validate it's not a duplicate
            // is this a new record?
            if ($this->isNewRecord) {
                // yes
                $twilioNumberExists = TwilioNumber::model()->findAll(
                    'provider_id=:provider_id AND practice_id=:practice_id AND twilio_partner_id=:twilio_partner_id',
                    array(
                        ':provider_id' => $this->provider_id,
                        ':practice_id' => $this->practice_id,
                        ':twilio_partner_id' => $this->twilio_partner_id
                    )
                );
            } else {
                // no
                $twilioNumberExists = TwilioNumber::model()->findAll(
                    'provider_id=:provider_id AND practice_id=:practice_id
                    AND twilio_partner_id=:twilio_partner_id AND id!=:id',
                    array(
                        ':provider_id' => $this->provider_id,
                        ':practice_id' => $this->practice_id,
                        ':twilio_partner_id' => $this->twilio_partner_id,
                        ':id' => $this->id
                    )
                );
            }

            // was the number a dupe?
            if ($twilioNumberExists) {
                // yes, exit
                self::addError(
                    'twilio_partner_id',
                    'This combination of provider, practice and partner already has a Twilio number.'
                );
                return false;
            }
        }

        // Remove mask from twilio number
        $twNumber = trim($this->twilio_number);
        $toReplace = array("(", ")", " ", "-", ".", "_");
        $this->twilio_number = str_replace($toReplace, "", $twNumber);

        // Check if there'll be a target number
        $targetNumber = "";

        if (((int) $this->provider_id) > 0) {

            $pp = ProviderPractice::model()->find(
                "provider_id = :providerId AND practice_id = :practiceId",
                array(":providerId" => $this->provider_id, ":practiceId" => $this->practice_id)
            );

            if (!$pp) {
                self::addError('twilio_partner_id', 'This combination of provider and practice doesn\'t exist.');
                return false;
            }

            $targetNumber = $pp->phone_number;
        }

        // if provider@practice doesn't have a number, fall back to number from practice
        if (!$targetNumber) {

            $p = Practice::model()->findByPk($this->practice_id);

            if (!$p) {
                self::addError('twilio_partner_id', 'Practice doesn\'t exist.');
                return false;
            }

            $targetNumber = $p->phone_number;
        }

        if (!$targetNumber) {
            self::addError(
                'twilio_partner_id',
                'No target number could be found.
                Please check the practice\'s or the provider-at-practice\'s phone number.'
            );
            return false;
        }

        return parent::beforeValidate(null, false);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // if twilio is enabled, remove number from the twilio side
        if ($this->enabled == 1) {

            $queueBody = array(
                'twilio_number' => $this->twilio_number
            );
            SqsComponent::sendMessage('deleteNumberFromTwilio', $queueBody);
        }

        // delete records from analytics_events before deleting this twilio_number
        $arrAnalyticsEvent = AnalyticsEvent::model()->findAll(
            'twilio_number_id=:twilio_number_id',
            array(':twilio_number_id' => $this->id)
        );
        foreach ($arrAnalyticsEvent as $analyticsEvent) {
            if (!$analyticsEvent->delete()) {
                $this->addError("id", "Couldn't delete from Google Analytics.");
                return false;
            }
        }

        // create/update TwilioNumberRemoved before deleting the TwilioNumber
        $twilioNR = TwilioNumberRemoved::model()->findByPk($this->id);
        if (empty($twilioNR)) {
            $twilioNR = new TwilioNumberRemoved();
            $twilioNR->id = $this->id;
        }
        $twilioNR->provider_id = $this->provider_id;
        $twilioNR->practice_id = $this->practice_id;
        $twilioNR->twilio_partner_id = $this->twilio_partner_id;
        $twilioNR->twilio_number = $this->twilio_number;
        $twilioNR->enabled = $this->enabled;
        $twilioNR->is_dynamic = $this->is_dynamic;
        $twilioNR->redirect_to = $this->redirect_to;
        $twilioNR->redirect_created = $this->redirect_created;
        $twilioNR->intro_greeting_message = $this->intro_greeting_message;
        $twilioNR->enabled_interface = $this->enabled_interface;
        $twilioNR->enabled_recording = $this->enabled_recording;
        $twilioNR->enabled_email = $this->enabled_email;
        $twilioNR->date_added = $this->date_added;
        $twilioNR->date_delete_on = $this->date_delete_on;
        $twilioNR->date_force_delete_on = $this->date_force_delete_on;
        $twilioNR->date_deleted = date("Y-m-d H:i:s");
        $twilioNR->forced_valid = $this->forced_valid;
        // magic account_id = 1: for command-line processes that don't have a user id
        $twilioNR->admin_account_id = (php_sapi_name() == "cli") ? 1 : Yii::app()->user->id;
        $twilioNR->save();

        return parent::beforeDelete();
    }

    /**
     * Get the name of the owning account(s)
     * @param int $practiceId
     * @return string
     */
    public function getAccountName($practiceId)
    {
        if ($practiceId) {
            $sql = sprintf(
                "SELECT SQL_NO_CACHE GROUP_CONCAT(CONCAT(first_name, ' ', last_name) SEPARATOR ', ')
                FROM account_practice
                INNER JOIN account
                    ON account.id = account_id
                WHERE account_practice.practice_id = %d;",
                $practiceId
            );

            return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
        }
        return '';
    }

    /**
     * called on rendering the column for each row in the administration
     * @return string
     */
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id . '" '
            . 'rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice . '">'
            . '' . $this->practice . '</a>');
    }

    /**
     * called on rendering the column for each row in the administration
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id . '" '
            . 'rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">'
            . '' . $this->provider . '</a>');
    }

    /**
     * If there's an active redirect number, it is returned, else
     * null is returned
     */
    public function getActiveRedirectNumber()
    {

        // redirects only last for 30 minutes
        if ($this->redirect_to && strtotime($this->redirect_created) > strtotime('-30 minutes')) {
            return $this->redirect_to;
        }

        return null;
    }

    /**
     * Get twilio_number stats for a givven account
     * @param integer $accountId
     * @return array
     */
    public static function getAccountStats($accountId = 0)
    {

        $sql = sprintf(
            "SELECT
            COUNT(DISTINCT account_practice.practice_id) as total_practices,
            COUNT(DISTINCT twilio_number.practice_id) as total_practices_with_active_twilio_number,
            COUNT(DISTINCT twilio_number.provider_id) as total_provider_with_active_twilio_number,
            COUNT(DISTINCT twilio_log.id) as total_of_calls_all_time
            FROM account_practice
            LEFT JOIN twilio_number
                ON account_practice.practice_id= twilio_number.practice_id
                AND twilio_number.enabled = 1
                AND twilio_number.is_dynamic=0
            LEFT JOIN twilio_log
                ON twilio_log.twilio_number_id = twilio_number.id
            WHERE account_practice.account_id=%d;",
            $accountId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();
    }

    /**
     * GADM: Get a list of twilio_numbers that aren't linked to an account that's linked to an active organization
     * @return string
     */
    public static function getChurnedNumbers()
    {

        return sprintf(
            "SELECT twilio_number.*, CONCAT(provider.first_name, provider.last_name) AS provider,
                practice.name AS practice, twilio_partner.name AS twilio_partner,
                DATE_FORMAT(twilio_number.date_added, '%%m/%%d/%%Y') AS date_added_print,
                IF(twilio_number.enabled = 1, 'Yes', 'No') AS enabled_print
            FROM twilio_number
            LEFT JOIN account_practice
                ON twilio_number.practice_id = account_practice.practice_id
            LEFT JOIN account
                ON account.id = account_practice.account_id
            LEFT JOIN organization_account
                ON organization_account.account_id = account.id
                AND `connection` = 'O'
            LEFT JOIN organization
                ON organization.id = organization_account.organization_id
                AND organization.status = 'A'
            LEFT JOIN provider
                ON twilio_number.provider_id = provider.id
            LEFT JOIN practice
                ON twilio_number.practice_id = practice.id
            LEFT JOIN twilio_partner
                ON twilio_partner_id = twilio_partner.id
            WHERE organization_account.id IS NULL
                OR organization.id IS NULL
                OR account.id IS NULL
                OR account_practice.id IS NULL
            GROUP BY twilio_number.id"
        ); // don't add a semicolon here since a limit and order may be added later
    }

    /**
     * Get purchased Twilio numbers per provider+practice
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function getPurchasedNumbers($providerId, $practiceId)
    {

        $where = 'WHERE twilio_number.practice_id = ' . $practiceId;
        if ($providerId != null && $providerId != '') {
            $where .= ' AND twilio_number.provider_id = ' . $providerId;
        } else {
            $where .= ' AND twilio_number.provider_id = NULL';
        }

        $sql = sprintf(
            'SELECT twilio_partner.name AS twilio_partner,
                twilio_number.id AS twilio_number_id,
                twilio_number.twilio_number AS twilio_number,
                twilio_number.enabled AS enabled,
                twilio_number.enabled_interface AS enabled_interface,
                twilio_number.enabled_recording AS enabled_recording,
                twilio_number.enabled_email AS enabled_email
            FROM twilio_number
            INNER JOIN twilio_partner
                ON twilio_partner.id = twilio_number.twilio_partner_id
            %s;',
            $where
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get all enabled partners
     * @return array
     */
    public static function getPartners()
    {
        $sql = 'SELECT twilio_partner.name, twilio_partner.id
            FROM twilio_partner
            WHERE enabled = 1';

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get purchased numbers and their partner IDs per provider+practice
     * @param int $providerId
     * @param int $practiceId
     * @return array
     */
    public static function getNumbersPerPartner($providerId, $practiceId)
    {

        $where = 'WHERE twilio_number.practice_id = ' . $practiceId;
        if ($providerId != null && $providerId != '') {
            $where .= ' AND twilio_number.provider_id = ' . $providerId;
        }

        $sql = sprintf(
            'SELECT twilio_number.twilio_partner_id,
                twilio_number.twilio_number
            FROM twilio_number
            %s;',
            $where
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Find partner_site for $topLevelDomain domain and the twilio partner associated with that partner_site that is
     * meant to track forwarded referrals to showcase pages (indicated by $isShowcase)
     * @param string $topLevelDomain
     * @param bool $isShowcase
     * @return int
     */
    public static function getPartnerSite($topLevelDomain, $isShowcase)
    {
        $sql = sprintf(
            "SELECT twilio_partner.id
            FROM twilio_partner
            INNER JOIN partner_site
                ON (twilio_partner.partner_site_id = partner_site.id)
            WHERE for_showcase = %s
                AND enabled = 1
                AND partner_site.url = '%s'",
            $isShowcase ? 1 : 0,
            $topLevelDomain
        );

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Return default showcase page phone number to use
     * @return int
     */
    public static function getDefaultShowcasePagePartner()
    {
        $sql = sprintf(
            "SELECT twilio_partner.id
            FROM twilio_partner
            WHERE partner_site_id IS NULL
                AND enabled = 1
                AND for_showcase = 1"
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * CMD: Return a list of:
     * - Twilio numbers linked to providers or
     * - practices that cancelled more than 30 days ago
     * - Numbers cancelled more than 30 days ago not linked to any provider or practice
     * - DNI numbers that should be deleted
     * Numbers will be deleted if:
     * - date_delete_on is not set or if it's set and it's in the past or
     * - date_force_delete is set
     * - number is dynamic and was created more than 28 days ago
     * @see TwilioCommand.php::actionDeleteTwilioNumbers
     * @return array
     */
    public static function getTwilioNumbersForDeletion()
    {

        $sql = sprintf(
            "SELECT twilio_number.id AS twilio_number_id, twilio_number.provider_id,
                twilio_number.practice_id, twilio_number.twilio_number,
                organization_practice.organization_name AS organization_practice_name,
                DATE_FORMAT(organization_practice.date_updated, '%%m/%%d/%%y') AS organization_practice_updated,
                DATEDIFF(CURDATE(), organization_practice.date_updated) AS organization_practice_updated_ago,
                organization_practice.status AS organization_practice_status,
                organization_practice.salesforce_id AS organization_practice_salesforce_id,
                organization_provider.organization_name AS organization_provider_name,
                DATE_FORMAT(organization_provider.date_updated, '%%m/%%d/%%y') AS organization_provider_updated,
                DATEDIFF(CURDATE(), organization_provider.date_updated) AS organization_provider_updated_ago,
                organization_provider.status AS organization_provider_status,
                organization_provider.salesforce_id AS organization_provider_salesforce_id
            FROM twilio_number
            LEFT JOIN account_provider
               ON twilio_number.provider_id = account_provider.provider_id
            LEFT JOIN organization_account AS organization_account_provider
               ON organization_account_provider.account_id = account_provider.account_id
               AND organization_account_provider.`connection` = 'O'
            LEFT JOIN organization AS organization_provider
               ON organization_provider.id = organization_account_provider.organization_id
               AND organization_provider.status = 'C'
            LEFT JOIN account_practice
               ON twilio_number.practice_id = account_practice.practice_id
            LEFT JOIN organization_account AS organization_account_practice
               ON organization_account_practice.account_id = account_practice.account_id
               AND organization_account_practice.`connection` = 'O'
            LEFT JOIN organization AS organization_practice
               ON organization_practice.id = organization_account_practice.organization_id
               AND organization_practice.status = 'C'
            LEFT JOIN twilio_partner
                ON twilio_number.twilio_partner_id = twilio_partner.id
            LEFT JOIN partner_site
                ON twilio_partner.partner_site_id = partner_site.id
            WHERE partner_site.force_twilio_enabled = '0'
                AND twilio_number.forced_valid = 0
                AND (
                    (
                        (
                            organization_provider.id IS NOT NULL AND twilio_number.provider_id NOT IN
                            (
                                SELECT provider_id
                                FROM account_provider
                                JOIN organization_account
                                    ON account_provider.account_id = organization_account.account_id
                                    AND connection = 'O'
                                JOIN organization
                                    ON organization.id = organization_account.organization_id
                                    AND status != 'C'
                            )
                        )

                        AND (
                            organization_practice.id IS NOT NULL AND twilio_number.practice_id NOT IN
                            (
                                SELECT practice_id
                                FROM account_practice
                                JOIN organization_account
                                    ON account_practice.account_id = organization_account.account_id
                                    AND connection = 'O'
                                JOIN organization
                                    ON organization.id = organization_account.organization_id
                                    AND status != 'C'
                                )
                        )

                        AND (
                            (DATEDIFF(CURDATE(), organization_provider.date_updated) > 30)
                            OR (DATEDIFF(CURDATE(), organization_practice.date_updated) > 30)
                        )
                        AND twilio_partner_id != 54 -- Exclude chanel partner cosentyx/novartis
                        AND twilio_partner_id != 55
                        AND twilio_number IS NOT NULL
                        AND (date_delete_on IS NULL OR date_delete_on < NOW())
                    )
                    OR (date_force_delete_on IS NOT NULL AND date_force_delete_on <= NOW())
                    OR (
                        date_delete_on < NOW()
                        AND organization_provider.id IS NULL
                        AND organization_practice.id IS NULL
                        )
                    OR (DATEDIFF(CURDATE(), twilio_number.date_added) > 28 AND is_dynamic = 1)
                )
            GROUP BY twilio_number.twilio_number;"
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Checkes whether a Twilio Number has churned in the last 30 days, according to its provider or practice orgs
     * @param int $twilioNumberId
     * @return bool
     */
    public static function isRecentlyChurned($twilioNumberId)
    {

        $sql = sprintf(
            "SELECT twilio_number.id
            FROM twilio_number
            LEFT JOIN account_provider
                ON twilio_number.provider_id = account_provider.provider_id
            LEFT JOIN organization_account AS organization_account_provider
                ON organization_account_provider.account_id = account_provider.account_id
                AND organization_account_provider.`connection` = 'O'
            LEFT JOIN organization AS organization_provider
                ON organization_provider.id = organization_account_provider.organization_id
                AND organization_provider.status = 'C'
            LEFT JOIN account_practice
                ON twilio_number.practice_id = account_practice.practice_id
            LEFT JOIN organization_account AS organization_account_practice
                ON organization_account_practice.account_id = account_practice.account_id
                AND organization_account_practice.`connection` = 'O'
            LEFT JOIN organization AS organization_practice
                ON organization_practice.id = organization_account_practice.organization_id
                AND organization_practice.status = 'C'
            WHERE (organization_provider.id IS NOT NULL OR organization_practice.id IS NOT NULL)
                AND (
                    (DATEDIFF(CURDATE(), organization_provider.date_updated) <= 30)
                    OR (DATEDIFF(CURDATE(), organization_practice.date_updated) <= 30)
                    OR (date_delete_on IS NOT NULL AND date_delete_on > NOW())
                    )
            AND twilio_number.id = %d;",
            $twilioNumberId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Ensure all twilio numbers are set up correctly and are valid
     *
     * Iterate through all our numbers allocated by twilio and verify that
     * (a) the IDs linked to are valid, then iterate through all our DB
     * entries and verify numbers are allocated on the Twilio end. Report
     * any inconsistencies
     *
     * @return array
     */
    public static function getConsistencyReport()
    {
        set_time_limit(0);
        ini_set("memory_limit", -1);

        $filePath = Yii::getPathOfAlias('application.data.twilioCR');

        // create data directory if not exists
        if (!file_exists($filePath)) {
            mkdir($filePath, 0777, true);
        }

        $filePath .= DIRECTORY_SEPARATOR . "ConsistencyReport.csv";
        $fp = fopen($filePath, 'w');

        // hardcoded urls to check
        $voiceFallbackUrl = 'https://phone.doctor.com/twilioLog/error';
        $voiceUrlPrefix = 'https://phone.doctor.com/twilioLog/start?twilio_number_id=';

        $return = array();
        $return['practiceErrors'] = array();
        $return['dniErrors'] = array();
        $return['nonErrors'] = array();

        // fetch all numbers from the database and make a hashmap out of them
        $allNumbersFromDb = TwilioNumber::model()->findAll(array("condition" => "enabled = 1"));
        $numDbEntries = count($allNumbersFromDb);

        $numbersHashMapFromDb = array();
        // go through the numbers in our db
        foreach ($allNumbersFromDb as $objTwilioNumber) {

            // by default, all errors are errors
            $target = 'practiceErrors';
            if (!empty($objTwilioNumber->friendly_name) && substr($objTwilioNumber->friendly_name, 0, 9) ==
                'FWD (DNI)') {
                // this number is of the DNI type (dynamic number integrations) - those numbers are volatile so this
                // doesn't represent an error, rather a warning
                $target = 'dniErrors';
            } elseif (!empty($objTwilioNumber->friendly_name)
                    && substr($objTwilioNumber->friendly_name, 0, 3) != 'FWD') {
                // internal ddc number
                $target = 'nonErrors';
            }

            // do we have an actual phone number?
            if ($objTwilioNumber->twilio_number) {
                // yes

                // normalize it
                $normNum = StringComponent::normalizePhoneNumber($objTwilioNumber->twilio_number);
                if ($normNum) {
                    $numbersHashMapFromDb[$normNum] = $objTwilioNumber->attributes;
                }

                // if the number is forced valid, skip validations
                if ($objTwilioNumber->forced_valid == 1) {
                    // if number is forced valid don't consider this an error
                    continue;
                }

                // do we have a provider?
                if ($objTwilioNumber->provider_id > 0) {
                    // yes, look him up
                    $objProvider = Provider::model()->findByPk($objTwilioNumber->provider_id);
                    if (!empty($objProvider) && (int) $objProvider->suspended == 1) {
                        // is he suspended?
                        // yes, report it
                        self::consistencyReportAddError(
                            $return[$target],
                            $objTwilioNumber,
                            'Linked provider #' . $objTwilioNumber->provider_id . ' (' . $objProvider->first_last .
                                ') is suspended.'
                        );
                    }
                }

                // look up the practice
                $objPractice = Practice::model()->findByPk($objTwilioNumber->practice_id);
                if (!empty($objPractice)) {
                    // is it suspended?
                    if ((int) $objPractice->suspended == 1) {
                        // yes, report it
                        self::consistencyReportAddError(
                            $return[$target],
                            $objTwilioNumber,
                            'Linked practice #' . $objTwilioNumber->practice_id . ' (' . $objPractice->name .
                            ') is suspended.'
                        );
                    } else {
                        // no, practice isn't suspended, but maybe all of its providers are
                        // retrieve providers
                        $arrProviderPractices = ProviderPractice::model()->findAll(
                            'practice_id=:practiceId',
                            array(':practiceId' => $objTwilioNumber->practice_id)
                        );
                        // we'll assume it's bad
                        $hasWorkingProviders = false;
                        // do we have linked providers?
                        if (!empty($arrProviderPractices)) {
                            // yes, go through them
                            foreach ($arrProviderPractices as $objProviderPractice) {
                                // look up the provider
                                $objProvider = Provider::model()->findByPk($objProviderPractice->provider_id);
                                if (!empty($objProvider) && (int) $objProvider->suspended == 0) {
                                    // is he suspended?
                                    // no, so this practice is ok
                                    $hasWorkingProviders = true;
                                }
                            }
                        }
                        // did we have at least one working provider?
                        if (!$hasWorkingProviders) {
                            // no, report error
                            self::consistencyReportAddError(
                                $return[$target],
                                $objTwilioNumber,
                                'Practice #' . $objTwilioNumber->practice_id . ' (' . $objPractice->name .
                                ') has all of its providers suspended or no providers are attached.'
                            );
                        }
                    }
                }

            } else {
                // no, report an error
                self::consistencyReportAddError(
                    $return[$target],
                    $objTwilioNumber,
                    'Twilio number is not set on this DB object'
                );
            }
        }

        // Instantiate a new Twilio Rest Client
        $client = new Yii_Services_Twilio();

        // go through all assigned phone numbers, verifying the urls are correct
        $numTwilioEntries = 0;
        $w = 0;
        $numberTwilio = $client->account->incoming_phone_numbers->getPage($w, 100);

        while (!empty($numberTwilio->page->incoming_phone_numbers)) {

            foreach ($numberTwilio->page->incoming_phone_numbers as $number) {

                $numTwilioEntries++;

                $target = 'practiceErrors';
                if (!empty($number->friendly_name) && substr($number->friendly_name, 0, 9) == 'FWD (DNI)') {
                    $target = 'dniErrors';
                } elseif (!empty($number->friendly_name) && substr($number->friendly_name, 0, 3) != 'FWD') {
                    $target = 'nonErrors';
                }

                if (!$number->capabilities->voice) {
                    self::consistencyReportAddError(
                        $return[$target],
                        $number,
                        'Voice capabilities are not enabled'
                    );
                }

                if ($number->voice_fallback_url != $voiceFallbackUrl) {
                    self::consistencyReportAddError(
                        $return[$target],
                        $number,
                        'Voice fallback URL is '
                            . (empty($number->voice_fallback_url) ? 'empty'  : $number->voice_fallback_url)
                            . ' but should be ' . $voiceFallbackUrl
                    );
                }

                // look up phone number in database; if found, get db, then
                // unset from hash so we can keep track of numbers that
                // weren't found
                if (!empty($numbersHashMapFromDb[$number->phone_number])) {

                    $expectedVoiceUrl = $voiceUrlPrefix . (!empty($numbersHashMapFromDb[$number->phone_number]->id)
                        ? $numbersHashMapFromDb[$number->phone_number]->id
                        : '');
                    unset($numbersHashMapFromDb[$number->phone_number]);

                    // @TODO: remove &PhoneNumber= from urls then add this back
                    //if ($number->voice_url != $expectedVoiceUrl) {
                    if (strpos($number->voice_url, $expectedVoiceUrl) !== 0) {

                        self::consistencyReportAddError(
                            $return[$target],
                            $number,
                            'Voice URL is '
                                . (empty($number->voice_url) ? 'empty' : $number->voice_url)
                                . ' but should be ' . $expectedVoiceUrl
                        );
                    }

                } else {
                    self::consistencyReportAddError(
                        $return[$target],
                        $number,
                        'Number not found in the Doctor.com DB'
                    );
                }
            }

            $w++;
            $numberTwilio = $client->account->incoming_phone_numbers->getPage($w, 100);
        }

        // go through remaining items in hashmap and generate errors for them
        // as they indicate a number in the database not allocated on the
        // Twilio side
        foreach ($numbersHashMapFromDb as $number) {
            self::consistencyReportAddError(
                $return['practiceErrors'],
                $number,
                'Number in our DB but not owned on Twilio'
            );
        }

        // create csv
        $row = array();
        $row[] = 'Twilio Number Consistency Report';
        fputcsv($fp, $row);

        $row = array();
        $row[] = '';
        fputcsv($fp, $row);

        $row = array();
        $row[] = 'Doctor.com database entries checked:';
        $row[] = $numDbEntries;
        fputcsv($fp, $row);

        $row = array();
        $row[] = 'Twilio entries checked:';
        $row[] = $numTwilioEntries;
        fputcsv($fp, $row);


        if ($return['practiceErrors']) {

            $row = array();
            $row[] = '';
            fputcsv($fp, $row);

            $row = array();
            $row[] = 'Errors';
            fputcsv($fp, $row);

            $row = array();
            $row[] = 'Source';
            $row[] = 'Number';
            $row[] = 'Name or ID';
            $row[] = 'Other';
            $row[] = 'Errors';
            fputcsv($fp, $row);

            foreach ($return['practiceErrors'] as $error) {

                $row = array();
                $row[] = $error['data'][0];
                $row[] = $error['data'][1];
                $row[] = Common::cleanVar($error['data'][2]);
                $row[] = Common::cleanVar($error['data'][3]);
                $row[] = join(',', $error['list']);
                fputcsv($fp, $row);
            }
        }


        if ($return['dniErrors']) {

            $row = array();
            $row[] = '';
            fputcsv($fp, $row);

            $row = array();
            $row[] = 'DNI Errors';
            fputcsv($fp, $row);

            $row = array();
            $row[] = 'Source';
            $row[] = 'Number';
            $row[] = 'Name or ID';
            $row[] = 'Other';
            $row[] = 'Errors';
            fputcsv($fp, $row);

            foreach ($return['dniErrors'] as $error) {

                $row = array();
                $row[] = $error['data'][0];
                $row[] = $error['data'][1];
                $row[] = Common::cleanVar($error['data'][2]);
                $row[] = Common::cleanVar($error['data'][3]);
                $row[] = join(',', $error['list']);
                fputcsv($fp, $row);
            }
        }

        if ($return['nonErrors']) {

            $row = array();
            $row[] = '';
            fputcsv($fp, $row);

            $row = array();
            $row[] = 'Non-errors';
            fputcsv($fp, $row);

            $row = array();
            $row[] = 'Source';
            $row[] = 'Number';
            $row[] = 'Name or ID';
            $row[] = 'Other';
            $row[] = 'Errors';
            fputcsv($fp, $row);

            foreach ($return['nonErrors'] as $error) {

                $row = array();
                $row[] = $error['data'][0];
                $row[] = $error['data'][1];
                $row[] = Common::cleanVar($error['data'][2]);
                $row[] = Common::cleanVar($error['data'][3]);
                $row[] = join(',', $error['list']);
                fputcsv($fp, $row);
            }
        }

        fclose($fp);

        return array(
            $numDbEntries, $numTwilioEntries, $return['practiceErrors'], $return['dniErrors'],
            $return['nonErrors']
        );
    }

    /**
     * Adds an additional error to the report by reference to the $errors parameter
     *
     * @param array $errors
     * @param object $numberObject
     * @param string $message
     */
    private static function consistencyReportAddError(&$errors, $numberObject, $message)
    {

        if (isset($numberObject->phone_number)) {

            $name = 'Twilio'
                . '|' . $numberObject->phone_number
                . '|' . $numberObject->sid
                . '|' . $numberObject->friendly_name
                . '|' . $numberObject->voice_url
                . '|' . $numberObject->voice_fallback_url;

            // this is a twilio object
            $data = array(
                'Twilio',
                $numberObject->phone_number,
                '<a href="https://www.twilio.com/user/account/phone-numbers/' . $numberObject->sid
                    . '" target="_blank"><small>' . $numberObject->friendly_name . '</small></a>', 'Voice URL: ' .
                    $numberObject->voice_url . '<br />Fallback URL: ' . $numberObject->voice_fallback_url
            );
        } else {
            $name = 'Doctor.com'
                . '|' . (isset($numberObject['twilio_number']) ? $numberObject['twilio_number'] : 'Unknown')
                . '|' . $numberObject['id']
                . '|' . TwilioPartner::model()->findByPk($numberObject['twilio_partner_id'])->name
                . '|' . ($numberObject['is_dynamic'] ? '-Dynamic' : '-Non-dynamic');
            $data = array(
                'Doctor.com',
                (isset($numberObject['twilio_number']) ? $numberObject['twilio_number'] : 'Unknown'),
                '<a href="https://' . Yii::app()->params->servers['dr_pro_hn']
                    . '/twilioNumber/update?id=' . $numberObject['id']
                    . '" target="_blank"><small>#' . $numberObject['id']
                    . '</small></a>',
                'Partner:&nbsp;'
                    . TwilioPartner::model()->findByPk($numberObject['twilio_partner_id'])->name
                    . '<br />' . ($numberObject['is_dynamic'] ? '-Dynamic' : '-Non-dynamic')
                    . '<br />' . ($numberObject['forced_valid'] ? '-FORCED VALID' : '-Not forced valid')
            );
        }

        if (empty($errors[$name])) {
            $errors[$name] = array(
                'data' => $data,
                'list' => array()
            );
        }

        $errors[$name]['list'][] = $message;
    }

    /**
     * Check if a given organization has Twilio numbers
     * @param int $organizationId
     * @return bool
     */
    public static function hasTwilio($organizationId)
    {

        $sql = sprintf(
            "SELECT COUNT(DISTINCT twilio_number.id) AS total
            FROM twilio_number
            INNER JOIN account_practice
                ON twilio_number.practice_id = account_practice.practice_id
            LEFT JOIN account_provider
                ON  account_provider.account_id= account_practice.account_id
            AND (account_provider.provider_id = twilio_number.provider_id
                OR twilio_number.provider_id IS NULL)
            INNER JOIN organization_account
                ON account_practice.account_id = organization_account.account_id
                AND `connection` = 'O'
            WHERE organization_account.organization_id = %d;",
            $organizationId
        );
        return (bool) Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get a practice's twilio numbers by it's id.
     *
     * @param integer $id Practice id
     *
     * @return array
     */
    public function getPracticeTwilioNumbersForScan($id)
    {
        $sql = "SELECT twilio_number, listing_type.code FROM twilio_number
            LEFT JOIN twilio_partner ON twilio_partner.id = twilio_number.twilio_partner_id
            LEFT JOIN partner_site ON twilio_partner.partner_site_id = partner_site.id
            LEFT JOIN listing_type ON partner_site.listing_type_id = listing_type.id
            WHERE twilio_number.enabled = 1 AND twilio_number.practice_id = :id";

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValue(':id', $id, PDO::PARAM_INT)
            ->queryAll();
    }

    /**
     * Delete a number from Twilio using its API
     * @param $twilioNumber
     */
    public static function deleteNumberFromTwilio($twilioNumber)
    {
        // Instantiate a new Twilio Rest Client
        $client = new Yii_Services_Twilio();

        $matches = $client->account->incoming_phone_numbers->getPage(
            0,
            1,
            array('PhoneNumber' => '+1' . $twilioNumber)
        );
        $number = !empty($matches->page->incoming_phone_numbers) ? $matches->page->incoming_phone_numbers[0] : 0;

        // it's ok to query http_host directly here, since we only want to run this from production
        // this also may not have a server set if run from the command line by
        // TwilioCommand::actionDeleteChurnedNumbers
        if ($number && (php_sapi_name() == "cli" || $_SERVER['HTTP_HOST'] == 'providers.doctor.com')) {
            try {
                $client->account->incoming_phone_numbers->delete($number->sid);
            } catch (Services_Twilio_RestException $e) {
                Yii::log(
                    "Error while deleting Twilio number " . $number->sid . ": " . $e->getMessage(),
                    'error',
                    'application.twilionumber.delete'
                );
            }
        }
    }

    /**
     * Check if we need to create/remove Twilio Tracking Number
     * @param ProviderPracticeGmb $ppg
     * @return false
     */
    public static function checkGmbTrackingNumber(ProviderPracticeGmb $ppg)
    {
        // We have a practice_id?
        if (empty($ppg->practice_id)) {
            // Yes
            return false;
        }

        $accountId = $ppg->account_id;
        $providerId = $ppg->provider_id;
        $practiceId = $ppg->practice_id;
        $partnerSiteId = 60; // Google
        $twilioPartnerId = 8; // Google Places

        $organization = Organization::getByAccountId($accountId);
        // Is related to an organization?
        if (empty($organization)) {
            // No
            return false;
        }

        // Do we have a twilio_number for this provider@practice ?
        $recTwilioNumber = TwilioNumber::searchTwilioNumber($twilioPartnerId, $practiceId, $providerId);

        // Is the sync_tracking_number checkbox selected?
        if ($ppg->sync_tracking_number == 1) {

            // Yes, we need to sync
            // Do we have a provider?
            if (!empty($providerId)) {

                // Create or Update PartnerSiteProviderFeatured
                $pspf = PartnerSiteProviderFeatured::featureProvider($partnerSiteId, $providerId);

                // Ok, the provider is featured and TwilioNumber doesn´t exists
                if (empty($recTwilioNumber)) {
                    // so create it
                    $pspf->generateTwilioNumbers($practiceId, true);
                }

            } else {

                // No, is only for the practice

                // Create the object for generateTwilioNumbers
                $pspf = new PartnerSiteProviderFeatured;
                $pspf->partner_site_id = $partnerSiteId;

                if (empty($recTwilioNumber)) {
                    // Create Twilio number only for the selected practice
                    $pspf->generateTwilioNumbers($practiceId, false);
                }

            }
        } else {
            // we didn't sync tracking number, the checkbox is unselected

            // Search for the PartnerSiteProviderFeatured record
            $pspf = PartnerSiteProviderFeatured::model()->find(
                'partner_site_id=:partner_site_id AND provider_id=:provider_id',
                array(
                    ':partner_site_id' => $partnerSiteId,
                    ':provider_id' => $providerId
                )
            );
            if (!empty($pspf)) {
                // Delete the twilio number
                $pspf->deleteTwilioNumbers($partnerSiteId, $providerId, $practiceId);

                // Remove the provider feature
                $pspf->removeProvider($partnerSiteId, $providerId);
            }

        }
    }

    /**
     * Search for a twilio_number to check if exists
     * @param integer $partner_id
     * @param integer $practiceId
     * @param integer $providerId
     *
     * @return void
     */
    public static function searchTwilioNumber($partnerId, $practiceId = null, $providerId = null)
    {

        $condition = "twilio_partner_id=:twilio_partner_id";
        $params = array(
            ':twilio_partner_id' => $partnerId,
        );

        if (!empty($practiceId)) {
            $params[':practice_id'] = $practiceId;
            $condition .= " AND practice_id=:practice_id";
        }

        if (!empty($providerId)) {
            $params[':provider_id'] = $providerId;
            $condition .= " AND provider_id=:provider_id";
        }

        return TwilioNumber::model()->find(
            array(
                'condition' => $condition,
                'params' => $params
            )
        );

    }
}
