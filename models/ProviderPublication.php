<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPublication');

class ProviderPublication extends BaseProviderPublication
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id .
            '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">' .
            $this->provider . '</a>');
    }

    /**
     * Returns list of publication types
     * @return array
     */
    public static function getPublicationTypes()
    {
        return array(
            '1' => 'Book',
            '2' => 'Dissertation',
            '3' => 'Magazine Article',
            '4' => 'Medical/Scholarly Journal Article',
            '5' => 'Online Article',
            '6' => 'Text Book',
            '7' => 'Other Publication'
        );
    }

    /**
     * Returns the display name of the publication type
     * @return string
     */
    public function getPublicationType()
    {

        $publicationTypes = self::getPublicationTypes();

        if (!empty($publicationTypes[$this->publication_type_id])) {
            return $publicationTypes[$this->publication_type_id];
        }
        return 'Undefined';
    }

    /**
     * Return the provider´s publications
     * @param int $providerId
     * @return array
     */
    public static function getPublications($providerId = 0, $cache = true)
    {
        $sql = sprintf(
            "SELECT id, provider_id, publication_type_id, publisher_name, title,
            published_year, published_month, '' AS publication_type
            FROM provider_publication
            WHERE provider_id = %d",
            $providerId
        );

        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

        $publications = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();

        if (!empty($publications)) {
            $publicationTypes = self::getPublicationTypes();
            foreach ($publications as $key => $value) {
                $publications[$key]['publication_type'] = $publicationTypes[$value['publication_type_id']];
            }
        }
        return $publications;
    }

    /**
     * Save Data
     * @param string $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        foreach ($postData as $data) {

            if (empty($data['publisher_name']) || empty($data['title'])) {
                continue;
            }

            if (isset($data['backend_action']) && $data['backend_action'] == 'delete' && $data['id'] > 0) {
                $model = ProviderPublication::model()->findByPk($data['id']);
                if (!empty($model)) {
                    if (!$model->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_PROVIDER_PUBLICATION";
                        $results['data'] = $model->getErrors();
                        return $results;
                    }
                    $auditSection = 'ProviderPublication #' . $providerId .' (#' . $model->title . ')';
                    AuditLog::create('D', $auditSection, 'provider_publication', null, false);

                }
            } else {

                if ($data['id'] > 0) {
                    $model = ProviderPublication::model()->findByPk($data['id']);
                    $auditAction = 'U';
                } else {
                    $auditAction = 'C';
                    $model = new ProviderPublication;
                }
                $model->attributes = $data;
                $model->provider_id = $providerId;

                if (!$model->save()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_PUBLICATION";
                    $results['data'] = $model->getErrors();
                    return $results;
                } else {
                    if ($data['id'] == 0) {
                        // return the new entries
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                }
                $auditSection = 'ProviderPublication #' . $providerId .' (#' . $model->title . ')';
                AuditLog::create($auditAction, $auditSection, 'provider_publication', null, false);

            }

        }
        return $results;
    }

}
