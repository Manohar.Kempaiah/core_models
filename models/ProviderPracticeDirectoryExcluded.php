<?php

Yii::import('application.modules.core_models.models._base.BaseProviderPracticeDirectoryExcluded');

class ProviderPracticeDirectoryExcluded extends BaseProviderPracticeDirectoryExcluded
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Return an array containing basic information of all excluded providers of the specified directory setting id.
     *
     * @param int $directorySettingId
     * @return array
     */
    public static function getExcludedProviders($directorySettingId)
    {
        $sql = "SELECT provider_practice_directory_excluded.id, provider.id AS provider_id, provider.npi_id AS npi, provider.first_m_last AS name
            FROM provider_practice_directory_excluded
            JOIN provider ON provider.id = provider_practice_directory_excluded.provider_id
            WHERE provider_practice_directory_excluded.directory_setting_id = :id
            ORDER BY provider.first_m_last";

        return Yii::app()->db->createCommand($sql)->queryAll(true, [':id' => $directorySettingId]);
    }

    /**
     * {@inheritdoc}
     */
    public function beforeValidate()
    {
        if (empty($this->date_added)) {
            $this->date_added = date('Y-m-d H:i:s');
        }

        return parent::beforeValidate();
    }
}
