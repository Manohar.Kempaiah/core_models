<?php

Yii::import('application.modules.core_models.models._base.BaseDataElement');

class DataElement extends BaseDataElement
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
