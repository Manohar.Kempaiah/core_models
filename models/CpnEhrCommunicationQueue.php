<?php

Yii::import('application.modules.core_models.models._base.BaseCpnEhrCommunicationQueue');

class CpnEhrCommunicationQueue extends BaseCpnEhrCommunicationQueue
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Inbound
     *
     * @param int $installationId
     * @param int $messageId
     * @param str $message
     * @return CpnEhrCommunicationQueue|bool
     */
    public function inbound($installationId, $messageId, $message, $strmessage)
    {
        if (isset($message->reprocess) && $message->reprocess) {
            $cecq = CpnEhrCommunicationQueue::model()->findByPk($message->id);
            if ($cecq) {
                return $cecq;
            }
        }

        //--grab the ehr_installation id
        $selectq = sprintf(
            "SELECT ei.id, i.account_id
            FROM cpn_ehr_installation ei
                INNER JOIN cpn_installation i on ei.cpn_installation_id = i.id
            WHERE i.id = '%s'
                AND i.del_flag = 0
                AND ei.del_flag = 0;",
            $installationId
        );

        $row = Yii::app()->dbRO->createCommand($selectq)->queryRow();

        if ($row) {

            // Create a new entry
            $cecq = new CpnEhrCommunicationQueue();
            $cecq->cpn_ehr_installation_id = $row['id'];
            $cecq->guid = Yii::app()->db->createCommand("SELECT uuid();")->queryScalar();
            $cecq->endpoint = 'FROM';
            $cecq->message_identifier = $messageId;
            $cecq->message = $strmessage;
            $cecq->cpn_ehr_message_type_id = $message->messageType;
            $cecq->cpn_installation_id = $installationId;
            $cecq->added_by_account_id = $row['account_id'];
            $cecq->save();

            return $cecq;

        }
        return false;
    }

    /**
     * Outbound
     *
     * @param int $installationId
     * @return array
     */
    public function outbound($installationId)
    {
        //--grab the ehr_installation id
        $selectq = sprintf(
            "SELECT ei.id
            FROM cpn_ehr_installation ei
                INNER JOIN cpn_installation i on ei.cpn_installation_id = i.id
            WHERE i.id = '%s'
                AND i.del_flag = 0
                AND ei.del_flag = 0;",
            $installationId
        );
        $installationId = Yii::app()->dbRO->createCommand($selectq)->queryScalar();


        $cecq = array();

        if ($installationId) {

            //--grab the message
            $sql = sprintf(
                "SELECT *
                FROM cpn_ehr_communication_queue
                WHERE cpn_ehr_installation_id = '%s'
                    AND endpoint = 'TO'
                    AND processed = 0
                    AND del_flag = 0
                ORDER BY id;",
                $installationId
            );

            $cecq = Yii::app()->dbRO->createCommand($sql)->queryAll();

        }

        return $cecq;

    }

    /**
     * OutboundACK
     *
     * @param int $installationId
     * @param int $messageId
     * @param str $successFlag
     * @return array
     */
    public function outboundACK($installationId, $messageId, $successFlag)
    {
        //--grab the ehr_installation id
        $selectq = sprintf(
            "SELECT ei.cpn_ehr_id
            FROM cpn_ehr_installation ei
                INNER JOIN cpn_installation i on ei.cpn_installation_id = i.id
            WHERE i.id = '%s'
                AND i.del_flag = 0
                AND ei.del_flag = 0;",
            $installationId
        );

        $ehrInstallationId = Yii::app()->db->createCommand($selectq)->queryScalar();

        if ($ehrInstallationId) {
            $cECQ = CpnEhrCommunicationQueue::model()->find(
                'guid=:guid AND del_flag = 0',
                array(
                    ':guid' => $messageId
                )
            );

            if ($cECQ) {

                $cECQ->success = ($successFlag=="true"?1:0);

                if ($successFlag=="true") {
                    $cECQ->processed = "1";
                }
                $cECQ->date_updated = date("Y-m-d H:i:s");
                $res = $cECQ->save();

                if ($res) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Get EhrByInstallation
     *
     * @param int $installationId
     * @return array
     */
    public function getEhrByInstallation($installationId)
    {
        // Query to the cpn_ehr_installation_attribute table
        $sql = sprintf(
            "SELECT      att.cpn_ehr_attribute_type_id,
                        t.`name`,
                        att.attribute_value,
                        t.sockets_flag,
                        t.folder_flag,
                        t.sftp_flag,
                        t.custom_flag
            FROM        cpn_ehr_installation_attribute att
            INNER JOIN  cpn_ehr_installation ei on att.cpn_ehr_installation_id = ei.id
            INNER JOIN  cpn_installation i on ei.cpn_installation_id = i.id
            INNER JOIN  cpn_ehr_attribute_type t on att.cpn_ehr_attribute_type_id = t.id
            INNER JOIN  cpn_ehr e on ei.cpn_ehr_id = e.id
                    AND t.sockets_flag = e.sockets_flag
                    AND t.folder_flag = e.folder_flag
                    AND t.sftp_flag = e.sftp_flag
                    AND t.custom_flag = e.custom_flag
            WHERE       i.id = '%s'
                    AND e.del_flag  = 0
                    AND ei.del_flag = 0
                    AND i.del_flag = 0
                    AND att.del_flag = 0
                    AND t.del_flag = 0;",
            $installationId
        );

        // Return all the results
        return Yii::app()->dbRO->createCommand($sql)->queryAll();
    }

    /**
     * Process cpn communication queue: take every message not success processed in our cpn_ehr_communication_queue
     * table and re-processs without creating a new cpn_ehr_communication_queue row
     *
     * @param int $cpnInstallationId
     * @param int $interval: indicates how long ago (in hours) messages will be re-processed from now on
     */
    public static function reprocess($cpnInstallationId, $interval = 24)
    {
        $from = date("Y/m/d H:i:s", strtotime("-" . $interval . " hours"));

        echo 'Processing only cpnInstallationId = '.$cpnInstallationId;
        echo PHP_EOL;

        $sql = sprintf(
            "SELECT id, message
            FROM cpn_ehr_communication_queue cq
            WHERE processed = 1
                AND success = 0
                AND del_flag = 0
                AND cpn_installation_id = '%s'
                AND date_added > '%s'
            ORDER BY id",
            $cpnInstallationId,
            $from
        );
        $result = Yii::app()->db->createCommand($sql)->queryAll();

        echo "There are " . count($result) . " messages to be reprocessed" . PHP_EOL;

        foreach ($result as $msg) {
            // Remove db safety backslashes and decode json
            $request = str_replace("\\\\", '', $msg['message']);
            $request = json_decode($request);

            echo "Reprocessing message #" . $msg['id'] . " (messageType = " .
                $request->messageType . ", messageDate = " . $request->messageDate . ")" .
                PHP_EOL;

            // Add this attribute to let Inbound function know that this called has been done by the reprocessCommand
            // and don't need to save the message in the Cpn_ehr_communication_queue again, as it's already there
            $request->id = $msg['id'];
            $request->reprocess = 'true';

            $opts = array(
                'http' => array(
                    'method' => 'POST',
                    'header' => 'Content-type: application/json',
                    'content' => json_encode($request, JSON_UNESCAPED_SLASHES)
                ),
                "ssl" => array(
                    "verify_peer" => false,
                    "verify_peer_name" => false,
                ),
            );

            $response = file_get_contents(
                "https://" . Yii::app()->params->servers['dr_pro_hn'] . "/api/CpnEhr/Inbound",
                false,
                stream_context_create($opts)
            );
            echo "Response: " . $response . PHP_EOL;
        }
    }

    /**
     * Search for entries that contains linked EHR User IDs in the message and failed to be processed, returns an array
     * of IDs
     *
     * @param int $cpnInstallationId
     * @return array|null
     */
    public static function getIdsForFailedLinkedProviders($cpnInstallationId)
    {
        $sql = sprintf(
            "SELECT DISTINCT u.ehr_user_id FROM cpn_installation_provider p
                JOIN cpn_installation_user u ON p.cpn_installation_user_id = u.id
                WHERE p.cpn_installation_id = '%s'",
            $cpnInstallationId
        );
        $userIds = Yii::app()->db->createCommand($sql)->queryColumn();

        $sqlUsers = null;
        foreach ($userIds as $userId) {
            if (!$sqlUsers) {
                $sqlUsers = "message LIKE '%" . $userId . "%'";
            } else {
                $sqlUsers .= " OR message LIKE '%" . $userId . "%'";
            }
        }

        if ($sqlUsers) {
            $sql = sprintf(
                "SELECT id FROM cpn_ehr_communication_queue WHERE success = 0 AND cpn_installation_id = %d",
                $cpnInstallationId
            );
            $sql.= ' AND (' . $sqlUsers . ')';

            return Yii::app()->db->createCommand($sql)->queryColumn();
        }

        return null;
    }
}
