<?php

Yii::import('application.modules.core_models.models._base.BaseWaitingRoomInviteNotes');

class WaitingRoomInviteNotes extends BaseWaitingRoomInviteNotes
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
