<?php

Yii::import('application.modules.core_models.models._base.BasePatient');

class Patient extends BasePatient
{
    // passwords are stored in Patient->secret - we use the password variable when a user submits a change
    public $password;
    public $password_confirm;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function getAccountName()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * Columns representing the schema
     * @return array
     */
    public static function representingColumn()
    {
        return array('first_name', 'last_name', 'email');
    }

    public function rules()
    {
        return array(
            array('email', 'required'),
            array('first_name, last_name', 'required', 'on' => 'kiosk'),
            array('password', 'required', 'on' => 'recoverPassword, insert', 'message' => 'Please enter your password'),
            array('password_confirm', 'required', 'on' => 'recoverPassword, insert', 'message' => 'Please confirm your password'),
            array('pregnant', 'required', 'on' => 'isFemale'),
            array('localization_id, receive_information, enabled, super_admin, partner_site_id, account_device_id, location_id, pregnant, insurance_id, employer_id', 'numerical', 'integerOnly' => true),
            array('id, zipcode', 'length', 'max' => 10),
            array('email', 'length', 'max' => 254),
            array('password', 'length', 'max' => 32),
            array('password', 'length', 'min' => self::$passwordMinimumLength),
            array('gender, receive_information', 'length', 'max' => 1),
            array('first_name', 'length', 'max' => 50),
            array('emergency_relationship', 'length', 'max' => 100),
            array('last_name, address, address_2, emergency_first_name, emergency_last_name', 'length', 'max' => 80),
            array('day_phone, night_phone, mobile_phone, fax_phone, emergency_phone', 'length', 'max' => 45),
            array('ethnicity, employment_status', 'length', 'max' => 2),
            array('race', 'length', 'max' => 4),
            array('ocupation', 'length', 'max' => 255),
            array('email', 'email'),
            array('email', 'unique'),
            array('date_added, date_updated, date_of_birth, date_signed_in', 'safe'),
            array('localization_id, receive_information, enabled, super_admin, lang_id, partner_site_id, account_device_id, location_id, zipcode, age, gender, first_name, last_name, date_added, pregnant, insurance_id, address, address_2, day_phone, night_phone, mobile_phone, fax_phone, emergency_first_name, emergency_last_name, emergency_phone, emergency_relationship, date_of_birth, ethnicity, race, employment_status, ocupation, employer_id', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, localization_id, enabled, super_admin, lang_id, email, password, partner_site_id, account_device_id, location_id, zipcode, age, gender, first_name, last_name, date_added, date_updated, date_signed_in, pregnant, insurance_id, address, address_2, day_phone, night_phone, mobile_phone, fax_phone, emergency_first_name, emergency_last_name, emergency_phone, emergency_relationship, date_of_birth, ethnicity, race, employment_status, ocupation, employer_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        $relations = parent::relations();
        $relations['partnerSite'] = array(self::BELONGS_TO, 'PartnerSite', 'partner_site_id');

        return $relations;
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();
        $labels['location_id'] = Yii::t('app', 'Location');
        $labels['insurance_id'] = Yii::t('app', 'Insurance');
        $labels['emergency_relationship'] = Yii::t('app', 'Relationship');
        $labels['account_device_id'] = Yii::t('app', 'Account Device Id');

        return $labels;
    }

    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id, true);
        $criteria->compare('email', $this->email, true);
        $criteria->compare('location_id', $this->location_id);
        $criteria->compare('zipcode', $this->zipcode, true);
        $criteria->compare('gender', $this->gender, true);
        $criteria->compare('account_device_id', $this->account_device_id, true);
        $criteria->compare('partner_site_id', $this->partner_site_id, true);
        $criteria->compare('first_name', $this->first_name, true);
        $criteria->compare('last_name', $this->last_name, true);
        $criteria->compare('date_added', $this->date_added, true);
        $criteria->compare('date_updated', $this->date_updated, true);
        $criteria->compare('pregnant', $this->pregnant);
        $criteria->compare('insurance_id', $this->insurance_id);
        $criteria->compare('address', $this->address, true);
        $criteria->compare('address_2', $this->address_2, true);
        $criteria->compare('day_phone', $this->day_phone, true);
        $criteria->compare('night_phone', $this->night_phone, true);
        $criteria->compare('mobile_phone', $this->mobile_phone, true);
        $criteria->compare('fax_phone', $this->fax_phone, true);
        $criteria->compare('emergency_first_name', $this->emergency_first_name, true);
        $criteria->compare('emergency_last_name', $this->emergency_last_name, true);
        $criteria->compare('emergency_phone', $this->emergency_phone, true);
        $criteria->compare('emergency_relationship', $this->emergency_relationship, true);
        $criteria->compare('date_of_birth', $this->date_of_birth, true);
        $criteria->compare('ethnicity', $this->ethnicity, true);
        $criteria->compare('race', $this->race, true);
        $criteria->compare('employment_status', $this->employment_status, true);
        $criteria->compare('receive_information', $this->receive_information, true);
        $criteria->compare('ocupation', $this->ocupation, true);
        $criteria->compare('enabled', $this->enabled, true);
        $criteria->compare('super_admin', $this->super_admin, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

    /**
     * Retrieves the name of a patient from the alliance administrative db.
     * @param int $id Patient id
     * @return string The patient's name
     */
    public static function getAllianceUserName($id)
    {
        $sql = sprintf(
            "SELECT first_name
            FROM the_alliance_admin.patient
            WHERE id = %d;",
            $id
        );
        $result = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryColumn();

        return $result[0];
    }

    /**
     * Retrieves the data of a patient from the alliance administrative db.
     * @param int $id Patient id
     * @return array The patient's record
     */
    public static function getAllianceUser($id)
    {
        $sql = sprintf(
            "SELECT *
            FROM the_alliance_admin.patient
            WHERE id = %d;",
            $id
        );
        $result = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();

        return $result[0];
    }

    /**
     * This function validate if the location_id exist.
     * This function is generally used for validation of both the zipcode of Create My Account as the Account.
     */
    public function validateZipcode()
    {
        if ($this->location_id == '') {
            $this->addError('location_id', 'Zipcode cannot be blank.');
        } else {
            $location = Location::model()->find('zipcode=:zipcode', array(':zipcode' => $this->location_id));
            if (!$location) {
                $this->addError('location_id', 'Please enter a valid zipcode');
            } else {
                $this->location_id = $location->id;
                if ($this->location_id == '') {
                    $this->addError('location_id', 'Please enter a valid zipcode');
                }
            }
        }
    }

    /**
     * Initialize values
     *
     * @return boolean
     */
    public function beforeSave()
    {
        if ($this->isNewRecord) {
            $this->date_signed_in = date("Y-m-d H:i:s");
            $this->date_added = date("Y-m-d H:i:s");
        }

        if (empty($this->location_id)) {
            // careful: Patient->zipcode needs to be set, otherwise location_id
            // changes won't save because of this:
            $locationData = Location::model()->getLocationIdByZipCode($this->zipcode);
            if (!empty($locationData)) {
                $this->location_id = $locationData['location_id'];
            } else {
                $this->addError('zipcode', Yii::t('patients', "svrErrZipCodeInvalid"));
                return false;
            }
        }

        return parent::beforeSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from patient table if associated records exist in review_request table
        $has_associated_records = ReviewRequest::model()->exists(
            'patient_id=:patient_id',
            array(':patient_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError('id', 'This patient cannot be deleted because a review request is associated to it.');
            return false;
        }

        // set patient_id = NULL in provider_rating before delete patient
        $arrProviderRating = ProviderRating::model()->findAll("patient_id = :patient_id", array(":patient_id" =>
        $this->id));
        foreach ($arrProviderRating as $providerRating) {
            $providerRating->patient_id = null;
            if (!$providerRating->save()) {
                Yii::log('Error while updating provider_rating: ' . json_encode($providerRating->getErrors()));
                $this->addError("id", "Couldn't update linked reviews");
                return false;
            }
        }

        // delete records from practice_patient before deleting this patient
        $arrPracticePatient = PracticePatient::model()->findAll(
            "patient_id = :patientId",
            array(":patientId" => $this->id)
        );
        foreach ($arrPracticePatient as $practicePatient) {
            if (!$practicePatient->delete()) {
                $this->addError("id", "Couldn't delete linked practices");
                return false;
            }
        }

        // delete records from provider_patient before deleting this patient
        $arrProviderPatient = ProviderPatient::model()->findAll(
            'patient_id=:patientId',
            array(':patientId' => $this->id)
        );
        foreach ($arrProviderPatient as $providerPatient) {
            if (!$providerPatient->delete()) {
                $this->addError("id", "Couldn't delete linked providers.");
                return false;
            }
        }

        // delete records from review_request before deleting this patient
        $arrRR = ReviewRequest::model()->findAll('patient_id=:patientId', array(':patientId' => $this->id));
        foreach ($arrRR as $rr) {
            if (!$rr->delete()) {
                $this->addError("id", "Couldn't delete linked Patient Messaging.");
                return false;
            }
        }

        // delete records from scheduling_mail_history before deleting this patient
        $arrSchedulingMailHistory = SchedulingMailHistory::model()->findAll(
            'patient_id=:patientId',
            array(':patientId' => $this->id)
        );
        foreach ($arrSchedulingMailHistory as $schedulingMailHistory) {
            if (!$schedulingMailHistory->delete()) {
                $this->addError("id", "Couldn't delete linked appointments.");
                return false;
            }
        }

        // delete records from scheduling_mail before deleting this patient
        $arrSchedulingMail = SchedulingMail::model()->findAll(
            'patient_id=:patientId',
            array(':patientId' => $this->id)
        );
        foreach ($arrSchedulingMail as $schedulingMail) {
            if (!$schedulingMail->delete()) {
                $this->addError("id", "Couldn't delete linked appointments.");
                return false;
            }
        }

        // delete records from account_patient_emr before deleting this patient
        $arrPatientEmr = AccountPatientEmr::model()->findAll(
            'patient_id=:patientId',
            array(':patientId' => $this->id)
        );
        foreach ($arrPatientEmr as $patientEmr) {
            if (!$patientEmr->delete()) {
                $this->addError("id", "Couldn't delete linked EMR data.");
                return false;
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Returns the name of the practice whose ReviewHub originated this
     * patient, or null if that wasn't the case
     *
     * @return string
     */
    public function getOriginReviewHubPractice()
    {

        if (intval($this->account_device_id) > 0) {

            // look up practice corresponding to this device id
            $sql = sprintf(
                "SELECT name
                FROM practice
                INNER JOIN account_device
                ON account_device.practice_id = practice.id
                WHERE account_device.id = %d;",
                $this->account_device_id
            );

            $output = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryScalar();

            if ($output != '') {
                return $output;
            }

            return 'Doctor.com ReviewHub #' . $this->account_device_id;
        }

        // default
        return null;
    }

    /**
     * Checks the eligibility of a given patient
     * @return boolean
     */
    public function checkEligibility()
    {
        if (php_sapi_name() != "cli" && Yii::app()->params["eligibilityAddress"]) {
            if (!Yii::app()->params["eligibilityAddress"]) {
                return true;
            }
        }

        $eligible = false;

        $special = array("'", ",", ".", "-");
        $first = str_replace($special, "", $this->first_name);
        $last = str_replace($special, "", $this->last_name);

        $ch = curl_init();
        $url = Yii::app()->params["eligibilityAddress"] . "/eligibility/CheckEmployee?firstName=" . urlencode($first) .
            "&lastName=" . urlencode($last) . "&birthDate=" . urlencode($this->date_of_birth);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $response = curl_exec($ch);
        curl_close($ch);
        $out = CJSON::decode($response);
        if (isset($out[0]["result"])) {
            Yii::app()->session["employer_id"] = $out[0]["employer_id"];
            $eligible = true;
            $this->employer_id = Yii::app()->session["employer_id"];
        } else {
            if (!$this->super_admin) {
                $this->employer_id = null;
                $this->enabled = 0;
            }
        }
        if ($this->super_admin == 1) {
            Yii::app()->session["super_admin"] = true;
            if ($this->employer_id) {
                Yii::app()->session["employer_id"] = $this->employer_id;
            }
            $eligible = true;
        }
        $this->password_confirm = $this->password;
        $this->save();
        return $eligible;
    }

    /**
     * Kiosk: logs in a patient
     * @param string $patient_email
     * @param string $patient_password
     * @return int
     */
    public static function loginPatient($patient_email, $patient_password)
    {
        $patient = Patient::model()->findByAttributes(array('email' => $patient_email));
        return $patient->authenticate($patient_password);
    }

    /**
     * Kiosk: gets a patient's details
     * @param int $patientId
     * @return array
     */
    public static function getPatientDetails($patientId)
    {
        $sql = sprintf(
            "SELECT id, email, location_id,
            zipcode, first_name, last_name
            FROM patient
            WHERE id = %d
            LIMIT 1;",
            $patientId
        );
        $arrPatient = Yii::app()->db->createCommand($sql)->queryAll();
        return $arrPatient[0];
    }

    /**
     * Kiosk: Gets the anonymous option last used by a patient
     * @param int $patientId
     * @return array
     */
    public static function getPatientLastReview($patientId)
    {

        $sql = sprintf(
            "SELECT reviewer_name_anon
            FROM provider_rating
            WHERE patient_id = %d
            ORDER BY id DESC
            LIMIT 1;",
            $patientId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Validate Date of Birth
     * @param date $dateOfBirth
     * @return boolean
     */
    public static function validateDOB($dateOfBirth)
    {
        $dobValidation = true;
        if ($dateOfBirth != '') {

            $dob = explode('-', $dateOfBirth);
            if (count($dob) == 3) {
                if ($dob[2] != '' && $dob[0] != '' && $dob[1] != '') {
                    $dateOB = $dob[2] . '-' . $dob[0] . '-' . $dob[1];
                    if (checkdate($dob[0], $dob[1], $dob[2])) {
                        if (Yii::app()->theme->getName() != "alliance") {
                            $then = strtotime($dateOB);
                            $min = strtotime('-18 years');
                            if ($then > $min) {
                                $dobValidation = false;
                            }
                        }
                    } else {
                        $dobValidation = false;
                    }
                } else {
                    $dobValidation = false;
                }
            } else {
                $dobValidation = false;
            }
        } else {
            $dobValidation = false;
        }

        return $dobValidation;
    }

    /**
     * MI7 - New Patient
     *
     * @param array $patient
     * @param int $accountId
     * @param int $aEmrId
     * @param int $practiceId
     * @return array
     */
    public static function mi7NewPatient($patient, $accountId, $aEmrId, $practiceId)
    {
        $result = array();

        // Patient already create?
        if (empty($patient['HomeEmailAddress'])) {

            $result['result'] = true;
            $result['patientId'] = null;
            $result['results'] = "success";

            return $result;
        } else {
            $pat = Patient::model()->find("email = :email", array(":email" => $patient['HomeEmailAddress']));
        }

        if (empty($pat)) {

            if (empty($patient['HomePhone'])) {
                $patient['HomePhone'] = $patient['WorkPhone'];
            }

            $pat = new Patient;
            $pat->first_name = $patient['FirstName'];
            $pat->last_name = $patient['LastName'];
            $pat->email = $patient['HomeEmailAddress'];
            $pat->zipcode = $patient['Postal'];
            $pat->date_added = $patient['RecordedDate'];
            $pat->date_updated = $patient['RecordedDate'];

            if ($patient['GenderMale']) {
                $pat->gender = "M";
            } else {
                $pat->gender = "F";
            }

            $pat->localization_id = 1;
            $pat->enabled = 1;
            $pat->super_admin = 0;
            $pat->lang_id = "en";
            $pat->address = $patient['Address1'];
            $pat->day_phone = preg_replace('/[^0-9]/', '', $patient['HomePhone']);
            $pat->night_phone = preg_replace('/[^0-9]/', '', $patient['HomePhone']);
            $pat->mobile_phone = preg_replace('/[^0-9]/', '', $patient['CellPhone']);
            $pat->password = $pat->assignRandomPassword();
            $pat->password_confirm = $pat->password;

            if (!empty($patient['DOB'])) {
                $dob = explode("T", $patient['DOB']);
                $pat->date_of_birth = $dob[0];
            }

            if ($patient['Postal']) {

                $location = new Location;
                $loc = $location->getLocationIdByZipCode($patient['Postal']);
                $pat->location_id = $loc['location_id'];
            }

            if ($pat->save()) {

                $result['result'] = true;
                $result['patientId'] = $pat->id;
                $result['results'] = "success";
            } else {
                $result['result'] = false;
                $result['results'] = json_encode($pat->errors);
                return $result;
            }
        }

        $patientId = $pat->id;

        $resultProvider = AccountProviderEmr::mi7FindProviderEmr($patient, $accountId, $aEmrId);
        $providerId = null;

        if ($resultProvider['result']) {
            $providerId = $resultProvider['providerId'];
        }

        $exists = AccountPatientEmr::model()->exists(
            "account_id = :account_id AND mi7_patient_id = :mi7_patient_id",
            array(":account_id" => $accountId, ":mi7_patient_id" => $patient['PatientID_MI7'])
        );

        if (!$exists) {
            $aPatientEmr = new AccountPatientEmr;
            $aPatientEmr->account_id = $accountId;
            $aPatientEmr->patient_id = $patientId;
            $aPatientEmr->emr_patient_id = $aEmrId . $patient['PatientID_EMR'];
            $aPatientEmr->mi7_patient_id = $patient['PatientID_MI7'];
            $aPatientEmr->save();
        }

        if (!empty($providerId)) {

            $exists = ProviderPatient::model()->exists(
                "provider_id = :provider_id AND patient_id = :patient_id",
                array(":provider_id" => $providerId, ":patient_id" => $patientId)
            );

            if (!$exists) {
                $pPatient = new ProviderPatient;
                $pPatient->provider_id = $providerId;
                $pPatient->patient_id = $patientId;
                $pPatient->primary_provider = 1;
                $pPatient->save();
            }
        }

        $exists = PracticePatient::model()->exists(
            "practice_id = :practice_id AND patient_id = :patient_id",
            array(":practice_id" => $practiceId, ":patient_id" => $patientId)
        );

        if (!$exists) {
            $pPatient = new PracticePatient;
            $pPatient->practice_id = $practiceId;
            $pPatient->patient_id = $patientId;
            $pPatient->primary_practice = 1;
            $pPatient->save();
        }

        $result['result'] = true;
        $result['patientId'] = $patientId;
        $result['results'] = "success";

        return $result;
    }

    /**
     * MI7 - Update Patient
     *
     * @param array $patient
     * @param int $accountId
     * @param int $aEmrId
     * @param int $practiceId
     * @return array
     */
    public static function mi7UpdatePatient($patient, $accountId, $aEmrId, $practiceId)
    {
        $result = array();
        $aPatientEmr = AccountPatientEmr::model()->find(
            "account_id = :account_id AND "
                . "(mi7_patient_id = :mi7_patient_id OR emr_patient_id = :emr_patient_id)",
            array(
                ":account_id" => $accountId,
                ":mi7_patient_id" => empty($patient['PatientID_MI7']) ? '-1' : $patient['PatientID_MI7'],
                ":emr_patient_id" => empty($patient['PatientID_EMR']) ? '-1' : $patient['PatientID_EMR']
            )
        );

        if ($aPatientEmr) {

            $pat = Patient::model()->findByPk($aPatientEmr->patient_id);

            if ($pat) {

                if (empty($patient['HomePhone'])) {
                    $patient['HomePhone'] = $patient['WorkPhone'];
                }

                if ($patient['GenderMale']) {
                    $pat->gender = "M";
                } else {
                    $pat->gender = "F";
                }

                if (!empty($patient['Address2'])) {
                    $pat->address_2 = $patient['Address2'];
                }

                $pat->password_confirm = $pat->password;
                $pat->first_name = $patient['FirstName'];
                $pat->last_name = $patient['LastName'];
                $pat->email = $patient['HomeEmailAddress'];
                $pat->zipcode = $patient['Postal'];
                $pat->date_updated = $patient['RecordedDate'];
                $pat->address = $patient['Address1'];
                $pat->day_phone = preg_replace('/[^0-9]/', '', $patient['HomePhone']);
                $pat->night_phone = preg_replace('/[^0-9]/', '', $patient['HomePhone']);
                $pat->mobile_phone = preg_replace('/[^0-9]/', '', $patient['CellPhone']);

                if ($patient['DOB']) {
                    $dob = explode("T", $patient['DOB']);
                    $pat->date_of_birth = $dob[0];
                }

                if ($patient['Postal']) {

                    $location = new Location;
                    $loc = $location->getLocationIdByZipCode($patient['Postal']);
                    $pat->location_id = $loc['location_id'];
                }

                if ($pat->save()) {

                    $result['result'] = true;
                    $result['patientId'] = $pat->id;
                    $result['results'] = "success";
                } else {
                    $result['result'] = false;
                    $result['results'] = json_encode($pat->errors);
                }
            } else {
                $result['result'] = false;
                $result['results'] = "The patient doesn't exist";
            }
        } else {

            if (empty($patient['HomeEmailAddress'])) {

                $result['result'] = false;
                $result['results'] = "The patient doesn't exist";
            } else {

                $pat = Patient::model()->find("email = :email", array(":email" => $patient['HomeEmailAddress']));

                if (empty($pat)) {
                    $result = Patient::mi7NewPatient($patient, $accountId, $aEmrId, $practiceId);
                } else {
                    $result['result'] = true;
                    $result['patientId'] = $pat->id;
                    $result['results'] = "success";
                }
            }
        }

        return $result;
    }

    /*
     * Seek by Email and then: Create or Update patient info
     * @param obj $patientData
     * return obj $patientRecord
     */
    public static function createOrUpdatePatient($patientData = null)
    {

        $patientEmail = StringComponent::validateEmail($patientData->email);
        $patientMobilePhone = $formatedPhone = $patientData->phone;
        $plaintextPassword = '';
        $fakeEmail = '';
        $haveToSave = false;

        if (!empty($patientEmail)) {
            // Look for a patient by email
            $patient = Patient::model()->find('email=:email', array(':email' => $patientEmail));
            if (!empty($patient)) {
                $formatedPhone = $patient->mobile_phone;
            }
        } elseif (!empty($patientMobilePhone)) {
            // No, Look for a patient by phone number
            $cleanedPhone = StringComponent::normalizePhoneNumber($patientMobilePhone, true);
            $formatedPhone = StringComponent::formatPhone($cleanedPhone);
            $patient = Patient::model()->find(
                'mobile_phone=:cleaned_phone OR mobile_phone=:formated_phone',
                [
                    ':cleaned_phone' => $cleanedPhone,
                    ':formated_phone' => $formatedPhone,
                ]
            );
        }

        // find patient by fake email
        if (empty($patient->id)) {

            if (empty($patientEmail) && !empty($patientMobilePhone)) {
                // create FakeEmail
                $fakePatient = new Patient;
                $fakePatient->first_name = $patientData->firstName;
                $fakePatient->last_name = $patientData->lastName;
                $fakePatient->mobile_phone = $formatedPhone;

                // Fake1
                $fakeEmail = $fakePatient->createFakeEmail();

                if (empty($patientData->firstName)) {
                    $fakePatient->first_name = 'Anonymous';
                }
                if (empty($patientData->lastName)) {
                    $fakePatient->last_name = 'A';
                }

                // Fake2
                $fakeEmail2 = $fakePatient->createFakeEmail();

                // Look for a patient by fakeEmail
                $patient = Patient::model()->find(
                    'email=:fake OR email=:fake2',
                    array(':fake' => $fakeEmail, ':fake2' => $fakeEmail2)
                );
            }
        }

        if (empty($patient->id)) {
            // We need to create a new patient record
            $patient = new Patient;
            $patient->setScenario('kiosk');
            $plaintextPassword = $patient->assignRandomPassword();
            $patient->first_name = $patientData->firstName;
            $patient->last_name = $patientData->lastName;
            $patient->mobile_phone = $formatedPhone;
            $patient->location_id = 0;
            $patient->email = !empty($patientEmail) ? $patientEmail : $fakeEmail;
            $haveToSave = true;
        }

        if (!empty($formatedPhone)) {
            if (empty($patient->mobile_phone) || ($patient->mobile_phone != $formatedPhone)) {
                // add phone if not empty or if is different
                $patient->mobile_phone = $formatedPhone;
                $haveToSave = true;
            }
        }

        if (!empty($patientEmail) && ($patient->email != $patientEmail) && $patientEmail != $fakeEmail) {
            // Update fakeEmail if needed
            $patient->email = $patientEmail;
            $haveToSave = true;
        }

        if ($haveToSave) {
            // we update some patients field, so we need to save the changes
            $patient->save();
        }

        if ($patient->lang_id != $patientData->lang_id) {
            // Language was changed
            $patient->lang_id = $patientData->lang_id;
            $patient->localization_id = Localization::model()->getLocalizationId($patient->lang_id);
        }

        if ($patient->location_id != $patientData->location_id) {
            // Location was changed
            $patient->location_id = $patientData->location_id;
            $location = Location::model()->cache(Yii::app()->params['cache_long'])->findByPk($patientData->location_id);
            $patient->zipcode = null;
            if (!empty($location)) {
                $patient->zipcode = $location->zipcode;
            }
        }

        // always save this patient data
        $patient->partner_site_id = !empty($patientData->partner_site_id) ? $patientData->partner_site_id : "1";
        $patient->account_device_id = $patientData->account_device_id;
        if (!empty(trim($patientData->firstName))) {
            $patient->first_name = trim($patientData->firstName);
        }
        if (!empty(trim($patientData->lastName))) {
            $patient->last_name = trim($patientData->lastName);
        }
        $patient->mobile_phone = !empty($formatedPhone) ? $formatedPhone : '';

        if (empty($patient->first_name)) {
            $patient->first_name = 'Anonymous';
        }
        if (empty($patient->last_name)) {
            $patient->last_name = 'Anonymous';
        }

        if ($patient->validate()) {
            // if data is valid, create or update the patient
            $patient->save();
            return array('arrPatient' => $patient, 'plainPasswordText' => $plaintextPassword);
        }
        return false;
    }

    /**
     * Check if the email isd fake or not
     *
     * @return bool
     */
    public function isFakeEmail()
    {
        $patientFirstName = strtolower($this->first_name);
        $patientLastName = strtolower($this->last_name);
        $patientUsername = preg_replace('/[^A-Za-z0-9\-]/', '_', $patientFirstName . ' ' . $patientLastName);

        $normalizedPatientMobilePhone = preg_replace('/[^A-Za-z0-9\-() ]/', '_', $this->mobile_phone);

        $patientEmail = $patientUsername . '+' .
            StringComponent::normalizePhoneNumber($normalizedPatientMobilePhone, true) . '@corp.doctor.com';

        if (strpos($this->email, 'nomail') !== false && strpos($this->email, 'corp.doctor.com') !== false) {
            return true;
        }

        if ($this->email == $patientEmail) {
            return true;
        }

        return false;
    }

    /**
     * Create fake Email
     *
     * @return string $fakeEmail
     */
    public function createFakeEmail()
    {
        $patientFirstName = strtolower($this->first_name);
        $patientLastName = strtolower($this->last_name);
        $patientUsername = preg_replace('/[^A-Za-z0-9\-]/', '_', $patientFirstName . ' ' . $patientLastName);

        $normalizedPatientMobilePhone = preg_replace('/[^A-Za-z0-9\-() ]/', '_', $this->mobile_phone);

        return $patientUsername . '+' .
            StringComponent::normalizePhoneNumber($normalizedPatientMobilePhone, true) . '@corp.doctor.com';
    }

    /**
     * Autocomplete to search patients for a given provider
     * @param array $params
     *  $params = array(
     *       'query_string' => ApiComponent::request('search'),
     *       'provider_id' => ApiComponent::request('provider_id'),
     *       'limit' => 10,
     *       'use_cache' => true,
     *       'account_id' => $accountId,
     *   );
     *
     * @return array
     */
    public static function autocomplete($params = array())
    {

        $providerId = Common::get($params, 'provider_id', null);
        $queryString = Common::get($params, 'query_string', null);
        $accountId = Common::get($params, 'account_id', null);
        $useCache = Common::get($params, 'use_cache', true);
        $limit = Common::get($params, 'limit', 10);

        if (empty($providerId) || empty($queryString) || empty($accountId)) {
            return array(
                'success' => false,
                'error' => true,
                'data' => 'EMPTY_DATA'
            );
        }

        $cacheTime = $useCache ? Yii::app()->params['cache_short'] : 0;

        // Have access to this provider?
        $sql = "SELECT id FROM account_provider WHERE account_id = :account_id AND provider_id = :provider_id";
        $result = Yii::app()->db
            ->cache($cacheTime)
            ->createCommand($sql)
            ->bindValue(':provider_id', $providerId, PDO::PARAM_INT)
            ->bindValue(':account_id', $accountId, PDO::PARAM_INT)
            ->queryRow();
        if (empty($result)) {
            return array(
                'success' => false,
                'error' => true,
                'data' => 'ACCESS_DENIED_PROVIDER_ACCOUNT'
            );
        }

        // Make a search
        $sql = sprintf(
            "SELECT patient.id as patient_id, patient.first_name, patient.last_name, patient.email, patient.mobile_phone
            FROM scheduling_mail
                LEFT JOIN patient ON patient.id = scheduling_mail.patient_id
            WHERE provider_id = %d AND patient_id > 0
                AND (patient.first_name LIKE '%s%%' OR patient.last_name LIKE '%s%%')
            GROUP BY patient.id
            LIMIT %d;",
            $providerId,
            $queryString,
            $queryString,
            $limit
        );

        $data = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();

        return array(
            'success' => true,
            'error' => false,
            'data' => $data
        );
    }
}
