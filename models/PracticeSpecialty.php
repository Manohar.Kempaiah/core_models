<?php

Yii::import('application.modules.core_models.models._base.BasePracticeSpecialty');

class PracticeSpecialty extends BasePracticeSpecialty
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    //called on rendering the column for each row
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id='
                . $this->practice_id . '" rel="/administration/practiceTooltip?id='
                . $this->practice_id . '" title="' . $this->practice . '">' . $this->practice . '</a>');
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_practice
            INNER JOIN practice_specialty
            ON account_practice.practice_id = practice_specialty.practice_id
            WHERE practice_specialty.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

    /**
     * get Practice Specialties
     * @param int $practiceId
     * @param bool $useCache (false for admin use)
     * @return arr
     */
    public static function getPracticeSpecialties($practiceId, $useCache = true)
    {

        $sql = sprintf(
            "SELECT ps.id, ps.sub_specialty_id, ss.name AS ss_name, s.name AS s_name
            FROM practice_specialty AS ps
            LEFT JOIN sub_specialty AS ss
            ON ps.sub_specialty_id = ss.id
            LEFT JOIN specialty AS s
            ON ss.specialty_id = s.id
            WHERE ps.practice_id = '%d'",
            $practiceId
        );

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Check if all specialties linked to a practice are of type Dentist (23)
     * @param int $practiceId
     * @return boolean
     */
    public static function isDentist($practiceId)
    {

        // check if all sub-specialties assigned to this practice are of the Dentist specialty (23)
        $sql = sprintf(
            "SELECT DISTINCT specialty_id
            FROM practice_specialty
            INNER JOIN sub_specialty
            ON practice_specialty.sub_specialty_id = sub_specialty.id
            WHERE practice_id = %d;",
            $practiceId
        );
        $specialties = Yii::app()->db->createCommand($sql)->queryAll();
        // 23 = Dentist
        if (!empty($specialties)) {
            if (sizeof($specialties) == 1 && $specialties[0]['specialty_id'] == 23) {
                // all linked sub-specialties belong to the Dentist specialty
                return true;
            }
        } else {
            // no sub-specialties linked directly to the practice, check through the provider/s
            $sql = sprintf(
                "SELECT DISTINCT specialty_id
                FROM provider_specialty
                INNER JOIN provider_practice
                ON provider_specialty.provider_id = provider_practice.provider_id
                INNER JOIN sub_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
                WHERE practice_id = %d;",
                $practiceId
            );
            $specialties = Yii::app()->db->createCommand($sql)->queryAll();
            // 23 = Dentist
            if (!empty($specialties)) {
                if (sizeof($specialties) == 1 && $specialties[0]['specialty_id'] == 23) {
                    // all linked sub-specialties belong to the Dentist specialty
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Return Bing-mapped specialties for a given practice
     * @param int $practiceId
     * @return array
     */
    public static function getBingCategories($practiceId)
    {
        // 17 is Bing
        $sql = sprintf(
            "SELECT DISTINCT
            SUBSTRING(retrieved_column_value, LOCATE('|', retrieved_column_value) + 1, 100) AS CategoryName,
            LEFT(retrieved_column_value, LOCATE('|', retrieved_column_value) - 1) AS BPCategoryId
            FROM practice_specialty
            INNER JOIN partner_site_entity_match
            ON practice_specialty.sub_specialty_id = partner_site_entity_match.internal_column_value
            WHERE partner_site_entity_match.partner_site_id = 17
            AND partner_site_entity_match.match_column_name = 'taxonomy_code'
            AND partner_site_entity_match.internal_entity_type = 'sub_specialty'
            AND partner_site_entity_match.internal_column_name = 'id'
            AND partner_site_entity_match.retrieved_entity_type = 'bing_practice_category'
            AND partner_site_entity_match.retrieved_column_name = 'category_id_name'
            AND practice_specialty.practice_id = %d
            ORDER BY practice_specialty.id;",
            $practiceId
        );
        $arrPracticeSpecialties = Yii::app()->db->createCommand($sql)->queryAll();

        if (empty($arrPracticeSpecialties)) {
            // use default category
            $arrPracticeSpecialties = array(
                array(
                    'BPCategoryId' => '711601',
                    'CategoryName' => 'Medical Group'
                )
            );
        }

        return $arrPracticeSpecialties;
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $practiceId
     * @param int $accountId
     * @return array
     */
    public static function saveData($postData = '', $practiceId = 0, $accountId = 0)
    {

        $results['success'] = true;
        $results['error'] = "";

        if (empty($postData)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        if ($practiceId == 0 || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "PRACTICE_ID_ZERO";
            return $results;
        }

        // validate access
        ApiComponent::checkPermissions('Practice', $accountId, $practiceId);

        foreach ($postData as $data) {
            $action = empty($data['backend_action']) ? 'create' : $data['backend_action'] ;
            $model = null;
            if ($action == 'D' || $action == 'delete' || empty($data['sub_specialty_id'])) {
                if ($data['id'] > 0) {
                    $model = PracticeSpecialty::model()->findByPk($data['id']);
                    if ($model) {
                        if (!$model->delete()) {
                            $results['success'] = false;
                            $results['error'] = "ERROR_DELETING_PRACTICE_SPECIALTY";
                            $results['data'] = $model->getErrors();
                            return $results;
                        }
                        $auditSection = 'PracticeSpecialty practice_id #' . $practiceId;
                        AuditLog::create('D', $auditSection, 'practice_specialty', null, false);
                    }
                }
            } else {
                $model = PracticeSpecialty::model()->find(
                    'sub_specialty_id=:sub_specialty_id AND practice_id=:practice_id',
                    array(
                        ':sub_specialty_id' => $data['sub_specialty_id'],
                        ':practice_id' => $practiceId
                    )
                );

                $auditAction = 'U';
                if (!$model) {
                    $model = new PracticeSpecialty;
                    $auditAction = 'C';
                }

                $model->sub_specialty_id = $data['sub_specialty_id'];
                $model->practice_id = $practiceId;
                if (!$model->save()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PRACTICE_SPECIALTY";
                    $results['data'] = $model->getErrors();
                    return $results;
                } else {
                    $auditSection = 'PracticeSpecialty practice_id #' . $practiceId;
                    AuditLog::create($auditAction, $auditSection, 'practice_specialty', null, false);
                    $results['success'] = true;
                    $results['error'] = "";
                    if ($auditAction == 'C') {
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                }
           }
        }
        return $results;
    }
}
