<?php

Yii::import('application.modules.core_models.models._base.BaseManufacturer');

class Manufacturer extends BaseManufacturer
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100),
        ));
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from manufacturer if associated records exist in medication_manufacturer table
        $has_associated_records = MedicationManufacturer::model()->exists(
            'manufacturer_id=:manufacturer_id',
            array(
                ':manufacturer_id' => $this->id
            )
        );
        if ($has_associated_records) {
            $this->addError("id", "Manufacturer can't be deleted because there are medications linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

}
