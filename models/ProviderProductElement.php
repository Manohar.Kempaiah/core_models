<?php

Yii::import('application.modules.core_models.models._base.BaseProviderProductElement');

class ProviderProductElement extends BaseProviderProductElement
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
