<?php

Yii::import('application.modules.core_models.models._base.BaseAuditLog');

/**
 * @property Account $account
 * @property Account $adminAccount
 */
class AuditLog extends BaseAuditLog
{

    public $organization;
    public $organization_id;
    public $account_full_name;
    public $admin_account_full_name;

    /**
     * @var string Global definition of origin value to be used as a local session parameter.
     */
    static $defaultOrigin = null;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Add fixed model relations.
     *
     * @return array
     */
    public function relations()
    {
        $data = parent::relations();

        $data['account'] = [
            self::BELONGS_TO,
            'Account',
            'account_id'
        ];

        $data['adminAccount'] = [
            self::BELONGS_TO,
            'Account',
            'admin_account_id'
        ];

        return $data;
    }

    /**
     * Make the representing column the Audit Log ID
     * @return string
     */
    public static function representingColumn()
    {
        return 'id';
    }

    /**
     * Add some fixed model rules.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules[] = array('account_full_name, admin_account_full_name', 'safe', 'on'=>'search');

        return $rules;
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'account_id' => Yii::t('app', 'Account'),
            'action' => Yii::t('app', 'Action'),
            'ip_address' => Yii::t('app', 'IP Address'),
            'hidden' => Yii::t('app', 'Hidden'),
            'table' => Yii::t('app', 'Table'),
            'section' => Yii::t('app', 'Section'),
            'date_added' => Yii::t('app', 'Date Added'),
            'admin_account_id' => Yii::t('app', 'Admin Account'),
            'organization_id' => Yii::t('app', 'Organization'),
            'notes' => Yii::t('app', 'Notes'),
            'account' => null,
            'adminAccount' => null,
            'account_full_name' => 'Logged-in as',
            'admin_account_full_name' => 'Admin',
        );
    }

    /**
     * Set date automatically if needed
     */
    public function beforeValidate()
    {
        if (empty($this->date_added) || $this->date_added == '0000-00-00 00:00:00') {
            $this->date_added = date('Y-m-d H:i:s');
        }
        return parent::beforeSave();
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->with = array('account', 'adminAccount');

        // enable searching for the account name from gridview filter
        if ($this->account_full_name) {
            $criteria->addSearchCondition(
                'CONCAT(account.first_name, " ", account.last_name)',
                $this->account_full_name
            );
        }

        $organization = '';
        if (!empty($_GET['AuditLog'])) {
            $auditLog = $_GET['AuditLog'];
            $organization = !empty($auditLog['organization_id']) ? trim($auditLog['organization_id']) : '';
        }

        // enable searching for the organization name from gridview filter
        if (!empty($organization)) {
            $criteria->addCondition('oa.organization_id = ' . $organization);

            $criteria->join = "INNER JOIN organization_account oa ON t.account_id = oa.account_id AND "
                    . "oa.connection = 'O' ";

        }

        // enable searching for the admin account name from gridview filter
        if ($this->admin_account_full_name) {
            $criteria->addSearchCondition(
                'CONCAT(adminAccount.first_name, " ", adminAccount.last_name)',
                $this->admin_account_full_name
            );
        }

        $criteria->compare('t.id', $this->id, true);
        if (intval($this->account_id)) {
            $criteria->addCondition('t.account_id = ' . $this->account_id);
        }
        $criteria->compare('action', $this->action, true);
        $criteria->compare('ip_address', $this->ip_address, true);
        $criteria->compare('t.table', $this->table, true);
        $criteria->compare('section', $this->section, true);
        $criteria->compare('t.date_added', $this->date_added, true);
        if (intval($this->admin_account_id)) {
            $criteria->addCondition('admin_account_id = ' . $this->admin_account_id);
        }
        if (intval($this->sub_section)) {
            $criteria->addCondition('t.sub_section =' . $this->sub_section);
        }
        $criteria->compare('notes', $this->notes, true);
        $criteria->compare('hidden', $this->hidden);

        if (
            $this->account_full_name == '' && $organization == '' && $this->admin_account_full_name == ''
            && $this->id == '' && $this->account_id == '' && $this->action == ''
            && $this->ip_address == '' && $this->table == '' && $this->section == ''
            && $this->date_added == '' && $this->admin_account_id == '' && $this->notes == ''
            && $this->hidden == ''
        ) {
            return false;
        }

        $criteria->order = "t.id DESC";

        return new CActiveDataProvider(
            $this,
            array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => 10
                )
            )
        );
    }


    /**
     * Create an entry in the audit_log table.
     *
     * $action can have letters as values:
     *      - C: Create
     *      - D: Delete
     *      - U: Update
     *      - F: Failure
     *
     * @param string $action values C|D|U|F
     * @param string $section
     * @param string $table
     * @param string $notes
     * @param bool $notifyPhabricator
     * @param int $userId
     * @param bool $hidden
     * @return mixed
     */
    public static function create(
        $action = '',
        $section = null,
        $table = null,
        $notes = null,
        $notifyPhabricator = false,
        $userId = 0,
        $hidden = false,
        $gmbId = null,
        $gmbAccountId = null
    ) {
        // are we in a process?
        $cli = (php_sapi_name() == 'cli') ? true : false;

        $userId = (int) $userId;
        if (empty($userId)) {
            if ($cli) {
                $userId = 1;
            } else {
                $userId = Yii::app()->user->id;
            }
        }

        if (empty($userId)) {
            $userId = 1; // default user
        }

        $allowedActions = ['C', 'U', 'D', 'S', 'F'];
        if (!in_array($action, $allowedActions)) {
            return false;
        }

        $auditLog = new AuditLog();
        // magic account_id = 1:
        // default to tech@corp.doctor.com for command-line processes that don't have a user id
        $auditLog->account_id = $userId;
        $auditLog->action = $action;
        $auditLog->sub_section= $gmbId;

        // Do we have the $section
        if (empty($section)) {
            // No, so try to generate from backtrace
            $section = 'Unknown';
            $backtrace = debug_backtrace();
            if (isset($backtrace[1]['function'])) {
                $section = $backtrace[1]['function'];
            }
        }
        $auditLog->section = $section;

        if ($table != null) {
            $auditLog->table = $table;
        }
        if ($notes != null) {
            $auditLog->notes = $notes;
        }
        $auditLog->date_added = date('Y-m-d H:i:s');
        $adminAccountId = null;
        if (!$cli) {
            if (!empty(Yii::app()->session['admin_account_id'])) {
                // it's an admin impersonating
                $adminAccount = Account::model()->find('MD5(id)=:adminAccountId', array(':adminAccountId' =>
                    Yii::app()->session['admin_account_id']));
                if ($adminAccount) {
                    $adminAccountId = $adminAccount->id;
                    $auditLog->account_id = $gmbAccountId;
                  }
            } else {
                // it's the actual account owner
                $adminAccountId = Yii::app()->user->id;
                $auditLog->account_id = $gmbAccountId;
            }
        }

        if (empty($adminAccountId)) {
            $adminAccountId = 1; // default admin
        }

        $auditLog->admin_account_id = $adminAccountId;

        // get the real ip address somehow
        $ipAddress = (!empty($_SERVER["HTTP_X_FORWARDED_FOR"]) ? $_SERVER["HTTP_X_FORWARDED_FOR"] : '');
        if (empty($ipAddress)) {
            $ipAddress = (!empty($_SERVER["HTTP_CLIENT_IP"]) ? $_SERVER["HTTP_CLIENT_IP"] : '');
        }
        if (empty($ipAddress)) {
            $ipAddress = (!empty($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '');
        }
        if (empty($ipAddress)) {
            $ipAddress = (!empty($_SERVER["HTTP_X_FORWARDED"]) ? $_SERVER["HTTP_X_FORWARDED"] : '');
        }
        if (empty($ipAddress)) {
            $ipAddress = (!empty($_SERVER["HTTP_X_CLUSTER_CLIENT_IP"]) ? $_SERVER["HTTP_X_CLUSTER_CLIENT_IP"] : '');
        }
        if (empty($ipAddress)) {
            $ipAddress = (!empty($_SERVER["HTTP_FORWARDED_FOR"]) ? $_SERVER["HTTP_FORWARDED_FOR"] : '');
        }
        if (empty($ipAddress)) {
            $ipAddress = (!empty($_SERVER["HTTP_FORWARDED"]) ? $_SERVER["HTTP_FORWARDED"] : '');
        }
        if (empty($ipAddress)) {
            // it was a local command
            $ipAddress = '127.0.0.1';
        }

        $auditLog->ip_address = substr($ipAddress, 0, 39);
        $auditLog->hidden = Common::isTrue($hidden) ? 1 : 0;

        // add the origin value
        $auditLog->origin = static::$defaultOrigin;

        if (!$auditLog->save()) {
            return $auditLog->getErrors();
        }

        // no need to notify phabricator
        if (!$notifyPhabricator) {
            return true;
        }

        // no need to notify phabricator
        if ($cli) {
            return true;
        }

        // we won't notify phabricator because this was an admin
        if ($userId != $adminAccountId) {
            return true;
        }

        // we have to notify phabricator
        $organizationId = Account::belongsToOrganization($userId, true);
        if (!$organizationId) {
            // account doesn't belong to an organization so just leave
            return true;
        }

        return true;
    }

    /**
     * PADM: Get the audit log for a given account
     * @param int $accountId
     * @return array
     */
    public static function getAccountAuditLog($accountId)
    {

        $sql = sprintf(
            "SELECT audit_log.id AS id, IF(admin_account_id IS NULL, '-', IF(account.first_name !=
            account.last_name, CONCAT(account.first_name, ' ', account.last_name), account.first_name)) AS admin,
            action, hidden,
            section, DATE_FORMAT(audit_log.date_added, '%%m/%%d/%%y %%h:%%i%%p') AS date_added, notes,
            ip_address
            FROM audit_log
            LEFT JOIN account
            ON audit_log.admin_account_id = account.id
            WHERE account_id = %d
            ORDER BY audit_log.id DESC
            LIMIT 1000;",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Save audit from research mode
     * @param int $accountId
     * @param string $provPrac
     * @param int $profileId
     * @param string $targetMode
     * @param string $prevStatus
     * @param string $menuOption
     * @param int $adminAccountId
     */
    public static function saveResearchModeLog(
        $accountId,
        $provPrac,
        $profileId,
        $targetMode,
        $prevStatus,
        $menuOption,
        $adminAccountId
        )
    {
        $section = ($menuOption == 'Research Mode') ? $menuOption : ResearchModeLog::researchSectionMenu($menuOption);

        $researchModeLog = new ResearchModeLog();
        $researchModeLog->owner_account_id = $accountId;

        if ($provPrac == 'provider') {
            $researchModeLog->provider_id = $profileId;
            $researchModeLog->practice_id = null;
        } else {
            $researchModeLog->provider_id = null;
            $researchModeLog->practice_id = $profileId;
        }
        $researchModeLog->new_status = $targetMode;
        $researchModeLog->prev_status = $prevStatus;
        $researchModeLog->section = $section;
        $researchModeLog->user_account_id = $adminAccountId;
        $researchModeLog->date_added = date("Y-m-d H:i:s");
        $researchModeLog->save();
    }

}
