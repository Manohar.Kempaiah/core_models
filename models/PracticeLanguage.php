<?php

Yii::import('application.modules.core_models.models._base.BasePracticeLanguage');

class PracticeLanguage extends BasePracticeLanguage
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    //called on rendering the column for each row
    public function getPracticeTooltip()
    {
        return ('<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id .
                '" rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice .
                '">' . $this->practice . '</a>');
    }

    /**
     * Checks a record's ownership
     * @param int $recordId
     * @param int $accountId
     * @return sql
     */
    public static function checkPermissions($recordId, $accountId)
    {
        return sprintf(
            "SELECT account_id
            FROM account_practice
            INNER JOIN practice_language
            ON account_practice.practice_id = practice_language.practice_id
            WHERE practice_language.id = %d
            ORDER BY account_id = %d DESC;",
            $recordId,
            $accountId
        );
    }

}
