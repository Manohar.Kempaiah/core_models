<?php

Yii::import('application.modules.core_models.models._base.BaseScanRequestPractice');

class ScanRequestPractice extends BaseScanRequestPractice
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        $relations = parent::relations();
        $relations['practice'] =  [self::BELONGS_TO, 'Practice', 'practice_id'];
        $relations['primarySpecialty'] =  [self::BELONGS_TO, 'SubSpecialty', 'primary_specialty_id'];

        return $relations;
    }

    /**
     * Return the last practice scan
     *
     * @param int $practiceId
     * @return array()
     */
    public static function getLastPracticeScan($practiceId = 0)
    {
        if ($practiceId == 0) {
            return [];
        }

        $scanWeb = Yii::app()->params['servers']['scan_web'];

        $sql = sprintf(
            "SELECT
                scan_request.date_added,
                scan_request.scan_log_id,
                scan_stats.op_score,
                CONCAT('%s', '/historical?id=', scan_request.scan_log_id ) AS historical_url
            FROM
                scan_request_practice
                INNER JOIN scan_request
                    ON scan_request_practice.scan_request_id = scan_request.id
                INNER JOIN scan_stats
                    ON scan_stats.scan_request_id = scan_request.id
            WHERE
                scan_request_practice.practice_id = %d
                    AND scan_request.organization_id IS NOT NULL
            ORDER BY scan_request.date_added DESC
            LIMIT 1",
            $scanWeb,
            $practiceId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryRow();
    }

    public function getByScanLogId($scanLogId)
    {
        $criteria = new CDbCriteria();

        $criteria->with[] = 'scanRequest';

        $criteria->compare('scanRequest.scan_log_id', $scanLogId);

        return $this->find($criteria);
    }
}
