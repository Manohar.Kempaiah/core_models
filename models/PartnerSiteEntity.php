<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteEntity');

class PartnerSiteEntity extends BasePartnerSiteEntity
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Additional validation rules
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules[] = array('structure', 'jsonValidator');
        $rules[] = array('dump_query', 'checkValidSQL');
        $rules[] = array('event_query', 'eventQueryValidator');

        return $rules;
    }

    /**
     * Gets a full list of partner site entity names with their associated
     * partner site names
     */
    public function dropDownDescriptions()
    {
        $array = array();
        $entities = PartnerSiteEntity::model()->with('partnerSite')->findAll();
        foreach ($entities as $e) {
            $array[$e->id] = $e->partnerSite->name . " - " . $e->name;
        }
        return $array;
    }

    /**
     * Checks that the entered structure is a valid JSON string
     * @param string $attribute Name of the atribute being validated
     * @param array $params Associative array of parameters
     */
    public function jsonValidator($attribute, $params)
    {
        if ($this->attributes[$attribute] != "" &&
                !json_decode($this->attributes[$attribute], true)) {
            $this->addError($attribute, $attribute . " must be a valid JSON string");
        }
    }

    /**
     * Checks the syntax and structure of the event query
     * @param string $attribute Name of the atribute being validated
     * @param array $params Associative array of parameters
     */
    public function eventQueryValidator($attribute, $params)
    {
        if ($this->attributes[$attribute] == "") {
            $other = PartnerSiteEntity::model()->find(
                "name = :name AND id != :id",
                array(
                    ":name" => $this->name,
                    ":id" => $this->id
                )
            );

            $event = PartnerSiteEntityEvent::model()->find(
                "partner_site_entity_id = :id AND query = ''",
                array(
                    ":id" => $this->id
                )
            );

            if ($other && $event) {
                $this->addError($attribute, "The field " . $attribute . " is required.");
            }
        } else {
            $this->checkValidSQL($attribute, $params);
        }
    }

    /**
     * Checks the syntax and structure of a query
     * @param string $attribute Name of the atribute being validated
     * @param array $params Associative array of parameters
     */
    public function checkValidSQL($attribute, $params)
    {

        //Get the first row
        $query = (substr($this->attributes[$attribute], -1) == ";" ? substr($this->attributes[$attribute], 0, -1) :
            $this->attributes[$attribute]);

        //Skip params for default event queries (used in DataStorageController::generatePDFEntries)
        $query = str_replace("$$" . "where" . "$$", "", $query);
        $query = str_replace("$$" . "and_where" . "$$", "", $query);
        $query = str_replace("$$" . "where_and" . "$$", "", $query);

        if ($query != "") {
            $sql = $query . " LIMIT 0, 1;";

            try {
                $row = Yii::app()->db->createCommand($sql)->queryRow(true);
            } catch (Exception $e) {
                $this->addError($attribute, "The query specified at " . $attribute . " couldn't be executed.");
                return;
            }

            if (!$row) {
                $this->addError($attribute, "The query specified at " . $attribute . " does not retrieve results.");
            }


            $structure = json_decode($this->structure, true);
            if (!$structure) {
                $ps = PartnerSiteEntity::model()->find(
                    "id != :id AND name = :name AND LENGTH(structure) > 0",
                    array(":id" => $this->id, ":name" => $this->name)
                );
                if ($ps) {
                    $structure = json_decode($ps->structure, true);
                }
            }

            foreach ($structure as $s) {
                if (!array_key_exists($s["name"], $row)) {
                    $this->addError(
                        $attribute,
                        "The query specified at " . $attribute . " does not match the provided structure."
                    );
                }
            }
        }
    }

}
