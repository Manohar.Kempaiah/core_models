<?php

Yii::import('application.modules.core_models.models._base.BaseProviderRating');

class ProviderRating extends BaseProviderRating
{
    public $client;
    public $partner;
    public $provider_full_name;
    public $provider_id;
    public $organizationId;
    public $organizationName;
    public $salesforceId;
    public $organizationStatus;
    public $provider_suffix;
    public $provider_prefix;
    public $provider_middle_name;

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'title';
    }

    /**
     * Reviewer Name cannot be blank.
     * Reviewer Email cannot be blank.
     * Reviewer Location cannot be blank.
     * Review Headline cannot be blank.
     * Comments cannot be blank.
     * @return array
     */
    public function rules()
    {
        return array(
            array('reviewer_name, reviewer_name_anon, title, description', 'required'),
            array(
                'localization_id, provider_id, partner_id, partner_site_id, practice_id, patient_id, '
                    . 'account_device_id, account_id, device_id, shared_with, reviewer_name_anon, rating, '
                    . 'rating_getting_appt, rating_appearance, rating_courtesy, rating_waiting_time, '
                    . 'rating_friendliness, rating_spending_time, rating_diagnosis, rating_follow_up, rating_billing,'
                    . 'rating_listening_skills, rating_clear_explanations, rating_trust, rating_sentiment,'
                    . 'yext_review_id, partner_site_review_id',
                'numerical',
                'integerOnly' => true
            ),
            array(
                'rating, rating_getting_appt, rating_appearance, rating_courtesy, rating_waiting_time, '
                    . 'rating_spending_time, rating_diagnosis, rating_follow_up, rating_billing, '
                    . 'rating_listening_skills, rating_clear_explanations, rating_trust, rating_sentiment',
                'numerical',
                'integerOnly' => true,
                'min' => 0,
                'max' => 5
            ),
            array('reviewer_name', 'length', 'max' => 50),
            array('reviewer_email', 'length', 'max' => 254),
            array('reviewer_location', 'length', 'max' => 100),
            array('title', 'length', 'max' => 255),
            array('status, origin', 'length', 'max' => 1),
            array('yext_site_id', 'length', 'max' => 20),
            array('source_ip_address', 'length', 'max' => 45),
            array('reviewer_email', 'email'),
            array('reference_url', 'length', 'max' => 1024),
            array('date_added, date_updated, date_facebook_sent', 'safe'),
            array('reviewer_email', 'default', 'setOnEmpty' => true, 'value' => ''),
            array(
                'provider_id, reviewer_location, rating, rating_getting_appt, rating_appearance, '
                    . 'reviewer_location, rating_courtesy, rating_waiting_time, origin, account_device_id, account_id,'
                    . 'device_id, shared_with, rating_spending_time, rating_diagnosis, rating_follow_up,rating_billing,'
                    . 'rating_listening_skills, rating_clear_explanations, rating_trust, rating_sentiment, status,'
                    . 'source_ip_address, date_added, date_updated, client, status_reason, status_comments,'
                    . 'partner_site_review_id',
                'default',
                'setOnEmpty' => true,
                'value' => null
            ),
            array(
                'id, provider_id,practice_id, reviewer_name, reviewer_location, reviewer_name_anon, reviewer_email, '
                    . 'origin, account_device_id, reviewer_location, rating, title, description, rating_getting_appt, '
                    . 'rating_appearance, rating_courtesy, rating_waiting_time, rating_spending_time, rating_diagnosis,'
                    . 'rating_follow_up, rating_billing, rating_listening_skills, rating_clear_explanations, '
                    . 'rating_trust, rating_sentiment, status, source_ip_address, date_added, date_updated, '
                    . 'partner_site_id, client, partner, status_reason, status_comments,'
                    . 'partner_site_review_id',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        $relations = parent::relations();
        $relations['partnerSite'] = array(self::BELONGS_TO, 'PartnerSite', 'partner_site_id');
        $relations['accountDevice'] = array(self::BELONGS_TO, 'AccountDevice', 'account_device_id');
        $relations['accountPractice'] = array(self::BELONGS_TO, 'AccountPractice', 'practice_id');

        return $relations;
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        $labels = parent::attributeLabels();

        $labels['provider_id'] = Yii::t('app', 'Provider');
        $labels['practice_id'] = Yii::t('app', 'Practice');
        $labels['reviewer_name_anon'] = Yii::t('app', 'Display Name Publicly As');
        $labels['reviewer_email'] = Yii::t('app', 'Reviewer Email');
        $labels['reviewer_location'] = Yii::t('app', 'Reviewer Location');
        $labels['rating'] = Yii::t('app', 'Overall Rating');
        $labels['title'] = Yii::t('app', 'Review Headline');
        $labels['description'] = Yii::t('app', 'Comments');
        $labels['rating_getting_appt'] = Yii::t('app', 'Ease of Getting an Appointment');
        $labels['rating_appearance'] = Yii::t('app', 'Appearance and Atmosphere of Office');
        $labels['rating_courtesy'] = Yii::t('app', 'Courtesy of Practice Staff');
        $labels['rating_waiting_time'] = Yii::t('app', 'Average Wait Time');
        $labels['rating_spending_time'] = Yii::t('app', 'Willingness to Spend Time with You');
        $labels['rating_diagnosis'] = Yii::t('app', 'Accuracy of Diagnosis');
        $labels['rating_follow_up'] = Yii::t('app', 'Post-Visit Follow-Up');
        $labels['rating_billing'] = Yii::t('app', 'Handling of Billing and Insurance');
        $labels['rating_listening_skills'] = Yii::t('app', 'Listening Skills');
        $labels['rating_clear_explanations'] = Yii::t('app', 'Clear Explanations');
        $labels['rating_trust'] = Yii::t('app', 'Trust in Decision Making');
        $labels['rating_sentiment'] = Yii::t('app', 'Sentiment');
        $labels['status'] = Yii::t('app', 'Status');
        $labels['origin'] = Yii::t('app', 'Origin');
        $labels['account_device_id'] = Yii::t('app', 'Account Device ID');
        $labels['account_id'] = Yii::t('app', 'Account ID');
        $labels['device_id'] = Yii::t('app', 'Device ID');
        $labels['shared_with'] = Yii::t('app', 'Shared With #');
        $labels['organizationId'] = 'Organization ID';
        $labels['organizationName'] = 'Organization Name';
        $labels['salesforceId'] = 'Salesforce ID';
        $labels['organizationStatus'] = 'Organization Status';
        return $labels;
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider
     */
    public function search()
    {

        $getData = Yii::app()->request->getParam('ProviderRating');
        $this->organizationId = !empty($getData['organizationId']) ? $getData['organizationId'] : null;
        $this->organizationName = !empty($getData['organizationName']) ? $getData['organizationName'] : null;
        $this->salesforceId = !empty($getData['salesforceId']) ? $getData['salesforceId'] : null;
        $this->organizationStatus = !empty($getData['organizationStatus']) ? $getData['organizationStatus'] : null;

        $criteria = new CDbCriteria;

        // Filter for partners
        $partnerId = Yii::app()->request->getParam('Partners', null);
        if (!empty($partnerId)) {
            $criteria->addInCondition('t.partner_id', $partnerId);
        }

        // Filter for sites
        $partnerSiteId = Yii::app()->request->getParam('PartnerSites', null);
        if (!empty($partnerSiteId)) {
            $criteria->addInCondition('t.partner_site_id', $partnerSiteId);
        }

        //
        // filter for date
        //
        $where = '';

        $date = Yii::app()->request->getParam('Date', 8);
        $date = PartnerFilters::getDate($date);
        if (!empty($date)) {
            $aux = explode('-', $date);
            if (count($aux) > 1) {
                $where .= 't.date_added >= "' . $date . '"';
            } else {
                $where .= ' year(t.date_added) >= "' . $date . '"';
            }
            $criteria->addCondition($where);
        }

        if (!intval($this->provider_id) && is_string($this->provider_id) && strlen($this->provider_id) > 0) {
            $criteria->addSearchCondition(
                "CONCAT(IF(prov.prefix != '', CONCAT(prov.prefix, ' '), ''), prov.first_name, ' ', prov.last_name,
                IF(credential.title IS NOT NULL, CONCAT(', ', credential.title), ''))",
                $this->provider_id
            );
        } else {
            $criteria->compare('t.provider_id', $this->provider_id);
        }

        $criteria->compare('t.reviewer_name', $this->reviewer_name, true);
        $criteria->compare('t.rating', $this->rating);
        $criteria->compare('t.title', $this->title, true);
        $criteria->compare('t.reviewer_email', $this->reviewer_email, true);
        $criteria->compare('t.reviewer_location', $this->reviewer_location, true);
        $criteria->compare('t.status', $this->status);

        $criteria->compare('t.unverified', $this->unverified);

        if ($this->origin == 'K' || $this->origin == 'V') {
            // K: Kiosk - V:ReviewRequest
            $criteria->addCondition('account_device_id IS NOT NULL');
            if ($this->origin == 'V') {
                $criteria->addCondition('device_model_id = ' . DeviceModel::REVIEW_REQUEST_ID);
            } else {
                $criteria->addCondition('device_model_id != ' . DeviceModel::REVIEW_REQUEST_ID);
            }
        } elseif ($this->origin == 'D') {
            $criteria->addCondition('account_device_id IS NULL AND t.origin = "D" ');
        } else {
            $criteria->compare('t.origin', $this->origin);
        }
        $criteria->compare('t.date_added', $this->date_added, true);
        $criteria->compare('t.date_facebook_sent', $this->date_facebook_sent, true);
        $criteria->compare('t.partner_site_id', $this->partner_site_id);
        $criteria->compare('t.partner_site_id', $this->partner);
        if (strlen($this->localization_id) > 0) {
            $criteria->compare('localization.id', $this->localization_id, true);
        }

        $this->organizationStatus = is_null($this->organizationStatus) ? '' : $this->organizationStatus;
        // comparison for organization status
        $this->organizationStatus = trim($this->organizationStatus);
        // C: Canceled, P: Paused, A: Active
        $criteria->compare('organization.status', $this->organizationStatus, true);

        $criteria->compare('organization.id', $this->organizationId, true);
        $criteria->compare('organization.organization_name', $this->organizationName, true);
        $criteria->compare('organization.salesforce_id', $this->salesforceId, true);

        if ($this->account_device_id == 'Y') {
            $criteria->addCondition('t.account_device_id IS NOT NULL');
        } elseif ($this->account_device_id == 'N') {
            $criteria->addCondition('t.account_device_id IS NULL');
        } else {
            $criteria->compare('t.account_device_id', $this->account_device_id, true);
        }

        // fields need to be specified manually since we also need the org values
        $criteria->select = "t.id, t.partner_id, t.partner_site_id,
            t.date_added, t.provider_id, t.reviewer_name,
            t.rating, t.title, t.reviewer_email,
            t.reviewer_location, t.status, t.origin, t.device_id, device_model_id, t.unverified,
            t.localization_id, organization.id AS organizationId, organization.organization_name AS organizationName,
            organization.salesforce_id AS salesforceId, organization.status AS organizationStatus, t.account_device_id,
            CONCAT(IF(prov.prefix != '', CONCAT(prov.prefix, ' '), ''), prov.first_name, ' ', prov.last_name,
            IF(credential.title IS NOT NULL, CONCAT(', ', credential.title), '')) AS provider_full_name";

        // join to get to the org
        $criteria->join = 'INNER JOIN provider AS prov
            ON prov.id = t.provider_id
            INNER JOIN localization
            ON localization.id = t.localization_id
            LEFT JOIN device
            ON device.id = t.device_id
            LEFT JOIN account_provider
            ON account_provider.provider_id = t.provider_id
            LEFT JOIN organization_account
            ON organization_account.account_id = account_provider.account_id
            LEFT JOIN organization
            ON organization.id = organization_account.organization_id
            LEFT JOIN credential
            ON prov.credential_id = credential.id';

        $criteria->group = 't.id';

        $pageSize = 50;
        if (Yii::app()->request->getParam('export')) {
            $pageSize = 500;
        }

        return new CActiveDataProvider(
            get_class($this),
            array(
                'criteria' => $criteria,
                'pagination' => array(
                    'pageSize' => $pageSize
                )
            )
        );
    }

    /**
     * Update the provider's average rating when a rating is updated (because they're added with status = P, so it's
     * only important for us to do it when updating and not when inserting a new record. Otherwise e.g. rating a
     * provider from a ReviewHub causes the provider's DocScore to be recalculated, which we don't want to since
     * a) it's slow and b) it wouldn't change anyway)
     * @return bool
     */
    public function afterSave()
    {
        // If is a new entry send location update to a queue
        if ($this->isNewRecord) {
            $queueBody = array(
                'provider_rating_id' => $this->id,
            );
            SqsComponent::sendMessage('updateProviderRatingLocation', $queueBody);
        }

        // load provider to update avg_rating and display_search_order were necessary
        $provider = Provider::model()->findByPk($this->provider_id);

        if ($provider) {
            // update avg_rating only in updates
            if ($this->isNewRecord === false) {
                $provider->avg_rating = self::calculateAverage($this->provider_id);
            }

            // display_search_order will be updated automatically in Provider model
            $provider->save();
        }

        // New Review from ReviewHub, prepare SQScommand to send alert message
        if (!empty($this->account_device_id) && $this->isNewRecord) {
            $queueBody = array();
            $queueBody['providerRatingId'] = $this->id;
            $queueBody['alertType'] = 'alert_review';
            SqsComponent::sendMessage('alertReview', $queueBody);
        }

        // If this is an approved review from a ReviewHub,
        // check eligibility for sending reward to owner of ReviewHub.
        if ($this->status == 'A' && $this->account_device_id) {

            // Are we in production environment?
            // Yes
            if (Yii::app()->params['environment'] == 'prod') {
                // only send a review to Healthgrades if the provider has the right feature/s
                $hasProfileSync = OrganizationFeature::model()->find(
                    'feature_id IN (10) AND active = 1 AND provider_id = :providerId',
                    array(':providerId' => $this->provider_id)
                );

                // Check if provider and practice are NASP verified
                $naspVerified = false;
                $naspVerifiedPrimaryPractice = ProviderPractice::model()
                    ->with('practice')
                    ->find(
                        'provider_id = :provider_id AND primary_location = 1 AND practice.nasp_date IS NOT NULL',
                        [':provider_id' => $this->provider_id]
                    );

                if ($naspVerifiedPrimaryPractice && $provider->nasp_date !== null) {
                    $naspVerified = true;
                }

                if ($hasProfileSync && $naspVerified) {
                    SqsComponent::sendMessage('propagateReviewToHealthgrades', $this->id);
                }
            }
        }

        $canBeShared = true;
        if (empty($this->partner_site_id)) {
            // We cannot determine if can be shared
            $canBeShared = false;
        }
        // 1: Doctor
        // 63: Site Enhance widget
        $validPartnerSiteId = array(1, 63);
        if (!in_array($this->partner_site_id, $validPartnerSiteId)) {
            // It is not our review, so we cannot shared to other sites
            $canBeShared = false;
        }

        // if was approved and has 4 or 5 stars and posting from RH check if it has to "auto-post" for socialEnhance
        if (
            $canBeShared
            &&
            $this->status == 'A'
            &&
            $this->origin == 'D'
            &&
            ($this->rating == 4 || $this->rating == 5)
        ) {
            // if it is new or was approved recently, send autoPosting request
            if ($this->isNewRecord || $this->oldRecord->status != 'A') {
                ProviderPracticeSocialEnhance::model()->makePost($this->id);
            }
        }

        // clear widget cache
        $providerWidgets = ProviderPracticeWidget::model()->findAll(
            "provider_id = :provider_id",
            array(":provider_id" => $this->provider_id)
        );

        foreach ($providerWidgets as $providerWidget) {
            Widget::clear($providerWidget->widget->key_code);
        }

        if (
            $this->oldRecord
            && $this->oldRecord->status != 'A'
            && empty($this->account_device_id)
            && $this->status == 'A'
        ) {
            // a review that didn't come from a ReviewHub was approved, send email to share on Yelp
            SqsComponent::sendMessage('shareReviewOnYelp', $this->id);
        }
        return parent::afterSave();
    }

    /**
     * Update avg_rating and display_search_order of the related provider
     * @return bool
     */
    public function afterDelete()
    {
        // load provider to update avg_rating and display_search_order were necessary
        $provider = Provider::model()->findByPk($this->provider_id);

        if ($provider) {
            // update avg_rating only in updates
            if ($this->isNewRecord === false) {
                $provider->avg_rating = self::calculateAverage($this->provider_id);
            }

            // display_search_order will be updated automatically in Provider model
            $provider->save();
        }

        return parent::afterDelete();
    }

    /**
     * Add a notification message in the CpnCommunicationQueue
     * @return void
     */
    public function addNotification()
    {
        // If Origin is not external (E) then go ahead with the companion notification
        if ($this->origin != 'E') {
            //create our SQL to grab the installation information
            $sql = sprintf(
                "SELECT
                    DISTINCT cpn_installation.id
                FROM cpn_installation
                LEFT JOIN cpn_installation_practice
                    ON cpn_installation_practice.cpn_installation_id = cpn_installation.id
                INNER JOIN cpn_installation_provider
                    ON cpn_installation_provider.cpn_installation_id = cpn_installation.id
                WHERE
                    cpn_installation.enabled_flag = 1
                    AND cpn_installation.del_flag = 0
                    AND cpn_installation.allow_installation_flag = 0
                    AND cpn_installation.installation_identifier IS NOT NULL
                    AND (
                        cpn_installation.practice_id = '%s'
                        OR cpn_installation_practice.practice_id = '%s'
                        )
                    AND cpn_installation_provider.provider_id = '%s'
                    AND cpn_installation_provider.provider_rating_notification = 1;",
                $this->practice_id,
                $this->practice_id,
                $this->provider_id
            );

            //query the database for the installation
            $arrCpnInstallation = Yii::app()->db->createCommand($sql)->queryColumn();

            //grab the message type 2 (review)
            $cpnMessageType = CpnMessageType::model()->find(
                "id = :id",
                array(":id" => '2')
            );

            //do we have a message type?
            //no
            if (!$cpnMessageType) {
                //return failure
                return false;
            }

            //iterate our installation
            foreach ($arrCpnInstallation as $cpnInstallationId) {
                $cpnCommunicationQueue = new CpnCommunicationQueue();
                $cpnCommunicationQueue->cpn_installation_id = $cpnInstallationId;
                $cpnCommunicationQueue->guid = Yii::app()->db->createCommand("SELECT uuid()")->queryScalar();
                $cpnCommunicationQueue->cpn_message_type_id = 2; // REVIEW REQUEST
                $cpnCommunicationQueue->message = $cpnMessageType->name;
                $cpnCommunicationQueue->url = 'https://' . Yii::app()->params['servers']['dr_pro_hn']
                    . '/app/reviews/review-manager?id=' . $this->id;
                $cpnCommunicationQueue->title = 'Doctor.com ' . $cpnMessageType->name;
                $cpnCommunicationQueue->seconds = '5';
                $cpnCommunicationQueue->date_to_publish = date('Y-m-d H:i:s');
                $cpnCommunicationQueue->date_expires = date(
                    'Y-m-d H:i:s',
                    strtotime($cpnCommunicationQueue->date_to_publish . ' +1 day')
                );
                $cpnCommunicationQueue->added_by_account_id = '1'; // Tech

                //set up our message json here for a appointment
                //{
                //  "messageIdentifier":"blah",
                //  "messageType":blah,
                //  "url":"blah",
                //  "urlTitle":"blah",
                //  "seconds":blah,
                //  "notificationDate":"blah",
                //  "expiresDate":"blah",
                //  "review":
                //  {
                //      "date":"blah",
                //      "name":blah,
                //      "anonymousFlag":blah,
                //      "email":"blah",
                //      "location":"blah",
                //      "rating":blah,
                //      "title":"blah",
                //      "description":"blah",
                //      "source":""
                //  }
                //}
                //create our message_json array
                $messageJson = array();
                $messageJson['messageIdentifier'] = $cpnCommunicationQueue->guid;
                $messageJson['messageType'] = $cpnCommunicationQueue->cpn_message_type_id;
                $messageJson['url'] = $cpnCommunicationQueue->url;
                $messageJson['urlTitle'] = $cpnCommunicationQueue->title;
                $messageJson['seconds'] = $cpnCommunicationQueue->seconds;
                $messageJson['notificationDate'] = $cpnCommunicationQueue->date_to_publish;
                $messageJson['expiresDate'] = $cpnCommunicationQueue->date_expires;

                //create our review array
                $review = array();
                $review['date'] = $cpnCommunicationQueue->date_to_publish;
                $review['name'] = $this->reviewer_name;
                $review['anonymousFlag'] = $this->reviewer_name_anon;
                $review['email'] = $this->reviewer_email;
                $review['location'] = $this->reviewer_location;
                $review['rating'] = $this->rating;
                $review['title'] = $this->title;
                $review['description'] = $this->description;

                //get the partner from the partner site id
                $partnerSite = PartnerSite::model()->findByPk($this->partner_site_id);

                //do we have a partner site?
                //yes
                if ($partnerSite) {
                    //set the source as the display name of the site
                    $review['source'] = $partnerSite->display_name;
                }

                //add the review array
                $messageJson['review'] = $review;

                //encode the JSON into our message_json field
                $cpnCommunicationQueue->message_json = json_encode($messageJson);

                //save it in the database
                $cpnCommunicationQueue->save();
            }
        }
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // delete records from provider_rating_abuse before deleting this provider_rating id
        $arrProviderRatingAbuse = ProviderRatingAbuse::model()->findAll(
            "rating_id = :rating_id",
            array(":rating_id" => $this->id)
        );
        foreach ($arrProviderRatingAbuse as $providerRatingAbuse) {
            if (!$providerRatingAbuse->delete()) {
                $this->addError("id", "Couldn't delete review abuse records.");
                return false;
            }
        }

        // delete records from provider_rating_response before deleting this provider_rating id
        $arrProviderRatingResponse = ProviderRatingResponse::model()->findAll(
            "rating_id = :rating_id",
            array(":rating_id" => $this->id)
        );
        foreach ($arrProviderRatingResponse as $providerRatingResponse) {
            if (!$providerRatingResponse->delete()) {
                $this->addError("id", "Couldn't delete review responses");
                return false;
            }
        }

        // delete records from provider_rating_abuse before deleting this provider_rating id
        $arrProviderRatingWidget = ProviderRatingWidget::model()->findAll(
            "provider_rating_id = :rating_id",
            array(":rating_id" => $this->id)
        );
        foreach ($arrProviderRatingWidget as $providerRatingWidget) {
            // Delete cached widget
            Widget::clear($providerRatingWidget->widget->key_code);
            if (!$providerRatingWidget->delete()) {
                $this->addError("id", "Couldn't delete review's widgets.");
                return false;
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Load old status for PDI and set remote IP address as source IP address if it's a new record
     * @return boolean
     */
    public function beforeSave()
    {

        // Sanitize vars
        $this->reviewer_email = StringComponent::validateEmail($this->reviewer_email);
        $this->title = trim(Common::cleanVar($this->title));
        $this->reviewer_name = trim(Common::cleanVar($this->reviewer_name));

        // is this a new reviewhub review or a GMB review?
        // 53 means partner_id = Google
        if ($this->isNewRecord && ($this->device_id > 0 || $this->partner_id == 53)) {
            // yes: Prevent duplicates

            $md5Data = md5($this->reviewer_name . $this->title . $this->description);

            // look for reviews for that date since sometimes we have time differences (ie dupes with inexact time)
            if ($this->device_id > 0) {
                // yes, check based on device_id
                $exists = ProviderRating::model()->exists(
                    'provider_id=:provider_id AND practice_id=:practice_id AND device_id=:device_id AND rating=:rating
                    AND DATE(date_added)=:date_added AND reviewer_email=:reviewer_email
                    AND MD5(CONCAT(reviewer_name, title, description))=:md5Data',
                    array(
                        ':provider_id' => $this->provider_id,
                        ':practice_id' => $this->practice_id,
                        ':device_id' => $this->device_id,
                        ':rating' => $this->rating,
                        ':date_added' => date('Y-m-d', strtotime($this->date_added)),
                        ':reviewer_email' => $this->reviewer_email,
                        ':md5Data' => $md5Data
                    )
                );
            } else {
                // no, check only the rest of the fields
                // do we have a provider?
                if ($this->provider_id > 0) {
                    // yes, check using the provider_id
                    $exists = ProviderRating::model()->exists(
                        'provider_id=:provider_id AND practice_id=:practice_id AND rating=:rating
                        AND DATE(date_added)=:date_added AND reviewer_email=:reviewer_email
                        AND MD5(CONCAT(reviewer_name, title, description))=:md5Data',
                        array(
                            ':provider_id' => $this->provider_id,
                            ':practice_id' => $this->practice_id,
                            ':rating' => $this->rating,
                            ':date_added' => date('Y-m-d', strtotime($this->date_added)),
                            ':reviewer_email' => $this->reviewer_email,
                            ':md5Data' => $md5Data
                        )
                    );
                } else {
                    // no, check with the practice_id only
                    $exists = ProviderRating::model()->exists(
                        'practice_id=:practice_id AND rating=:rating AND DATE(date_added)=:date_added
                        AND reviewer_email=:reviewer_email AND MD5(CONCAT(reviewer_name, title, description))=:md5Data',
                        array(
                            ':practice_id' => $this->practice_id,
                            ':rating' => $this->rating,
                            ':date_added' => date('Y-m-d', strtotime($this->date_added)),
                            ':reviewer_email' => $this->reviewer_email,
                            ':md5Data' => $md5Data
                        )
                    );
                }
            }
            if ($exists) {
                $this->addError('id', 'Duplicated Review');
                if (php_sapi_name() != 'cli') {
                    echo "{error: true,message: 'Duplicated Review'}";
                }
                return false;
            }
        }

        if (empty($this->friendly_url)) {
            // always generate a friendly url
            $this->friendly_url = self::getFriendlyUrl($this->title, $this->id);

            if (!empty($this->provider_id)) {
                // get provider friendly url
                $recProvider = Provider::model()->cache(Yii::app()->params['cache_short'])->find(
                    'id = :id',
                    array(':id' => $this->provider_id)
                );
                $providerFriendlyUrl = !empty($recProvider->friendly_url) ? $recProvider->friendly_url : '';

                // Create urls to share review
                $urls['twitter'] = StringComponent::prepareUrlToShareReview($this, 39, false, $providerFriendlyUrl);
                $this->twitter_url_short = !empty($urls['twitter']['short']) ? $urls['twitter']['short'] : null;
            }
        }

        // If Review is Rejected and wasn't rejected before (so that we don't reject simple updates)
        if (!empty($this->oldRecord->status) && $this->status == "R" && $this->oldRecord->status != 'R') {
            // check if we're in the command line
            if (php_sapi_name() != 'cli') {
                // we aren't: the user must set the rejected reason and notes
                // Reason is Required
                if ($this->status_reason == "") {
                    $this->addError('status_reason', 'Status Rejected Reason is Required.');
                    return false;
                }
                // IF Reason is 'S' or 'O', Comments are Required
                if (
                    ($this->status_reason == "S" || $this->status_reason == "O")
                    && $this->status_comments == ""
                ) {
                    $this->addError('status_comments', 'Status Comments is Required.');
                    return false;
                }
            } else {
                // we are so update the reason automatically (to avoid failing in a non-interactive process)
                $this->status_reason = 'O';
                $this->status_comments = 'Rejected reason set automatically by process.';
            }
        }

        if ($this->isNewRecord) {

            // set source ip address when creating a review
            $sourceIpAddress = trim(
                (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'] != ''
                    ? $_SERVER['HTTP_X_FORWARDED_FOR']
                    : (isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''))
            );

            // if testing locally, simulate 'Rosario' ip address
            $sourceIpAddress = ($sourceIpAddress == '127.0.0.1' ? '190.17.51.225' : $sourceIpAddress);

            $this->source_ip_address = substr($sourceIpAddress, 0, 45);
            $this->reviewer_location = 'Unknown';
        } elseif (
            !empty($this->oldRecord->status)
            && php_sapi_name() != "cli"
            && !empty(Yii::app()->user->id)
            && !empty(Yii::app()->controller->module->id)
            && Yii::app()->controller->module->id == 'providers'
        ) {

            if ($this->oldRecord->status != $this->status && $this->status == 'A') {
                // approved
                AuditLog::create('U', 'GADM: Review approval', 'provider_rating', $this->id);

                // check if this is an active client and we have enough information
                if (
                    $this->account_device_id > 0
                    && $this->account_id > 0
                    && $this->provider_id > 0
                    && Provider::isClient($this->provider_id)
                ) {

                    // check if we have to send real-time alerts for this account
                    $accountDetails = AccountDetails::model()->find(
                        'account_id=:accountId',
                        array(':accountId' => $this->account_id)
                    );
                    if (!$accountDetails) {
                        $accountDetails = new AccountDetails;
                        $accountDetails->account_id = $this->account_id;
                        $accountDetails->send_rh_realtime = 1;
                        $accountDetails->send_rh_summary = 1;
                        $accountDetails->save();
                    }

                    // check if we have to send notifications for this account device
                    $accountDevice = AccountDevice::model()->findByPk($this->account_device_id);

                    if (
                        $accountDetails
                        && $accountDetails->send_rh_realtime
                        && $accountDevice->send_email_review_to_account
                    ) {
                        // real-time alerts are enabled
                        if ((int) $this->rating <= 2) {
                            // send_rh_realtime = 1 means send only for 1-2 stars reviews
                            MailComponent::sendReviewHubRealtimeAlertBad($this);
                        } elseif ($accountDetails->send_rh_realtime == 2) {
                            // send_rh_realtime means send for everything
                            MailComponent::sendReviewHubRealtimeAlertGood($this);
                        }
                    }

                    // if this is the first approved review for this account
                    $approvedRHReviews = ProviderRating::model()->count(
                        'account_id=:accountId AND status="A" AND origin != "E"',
                        array(':accountId' => $this->account_id)
                    );
                    if ($approvedRHReviews == 0) {
                        // look up the reviewhub
                        $accountDevice = AccountDevice::model()->findByPk($this->account_device_id);
                        // if the reviewhub is active
                        if ($accountDevice->status == 'A') {
                            // look up the account
                            $account = Account::model()->findByPk($this->account_id);
                            // if we can find the account
                            if ($account) {
                                // populate the First_Review_Left__c field in SalesForce (ISO 8601 date)
                                $account->updateSalesforceAccountField('First_Review_Left__c', date('c'));
                            }
                        }
                    }
                }
                // Insert Notification in cpn_communication_queue
                $this->addNotification();
            } elseif (
                !empty($this->oldRecord->status)
                && $this->oldRecord->status != $this->status
                && $this->status == 'R'
            ) {
                // rejected
                AuditLog::create('U', 'GADM: Review rejection', 'provider_rating', $this->id);
            } elseif (!empty($this->oldRecord->status) && $this->oldRecord->status != $this->status) {
                $status = array('A' => 'Approved', 'P' => 'Pending', 'R' => 'Rejected');

                // Any change of status is filed.
                AuditLog::create(
                    'U',
                    'GADM: Review change of status from '
                        . !empty($this->oldRecord->status) ? $this->oldRecord->status : '?'
                        . ' to '
                        . ((array_key_exists($this->status, $status)) ? $status[$this->status] : $this->status),
                    'provider_rating',
                    $this->id
                );
            }
        }

        if (
            $this->account_device_id > 0
            || (
                (!empty($this->oldRecord->status) && $this->oldRecord->status != $this->status)
                && (!empty($this->oldRecord->status) && $this->oldRecord->status == 'A' || $this->status == 'A'))
        ) {
            // a review had its status changed, it was accepted before or is accepted now, so update SalesForce
            // we always update it if it's a rh since we count the submitted ones as well (regardless of status)
            // locate the organization through the provider
            $arrOrganizations = Provider::belongsToOrganization($this->provider_id);
            if (!empty($arrOrganizations)) {
                foreach ($arrOrganizations as $organizationId) {
                    SqsComponent::sendMessage('updateSFOrganizationReviews', $organizationId);
                }
            }
        }

        // Changes needed in the provider_rating->date_approved
        if ($this->isNewRecord) {
            if (
                $this->status == 'A'
                && (empty($this->date_approved) || $this->date_approved == '0000-00-00 00:00:00')
            ) {
                // For all new reviews where the status is "Approved" and date_approved is NULL, fill the value of
                // date_approved with the timestamp of provider_rating.date_updated
                if (empty($this->date_updated)) {
                    $this->date_updated = date("Y-m-d H:i:s");
                }
                $this->date_approved = $this->date_updated;
            }
        } else {

            if (
                !empty($this->oldRecord->status)
                && $this->oldRecord->status == 'P'
                && $this->oldRecord->status != $this->status
            ) {
                // Update provider_rating.date_approved with the current timestamp when the status of a review is
                // changed from P to any other status (including Rejected)
                $this->date_approved = date("Y-m-d H:i:s");
            } elseif (
                $this->status == 'A'
                && (empty($this->date_approved) || $this->date_approved == '0000-00-00 00:00:00')
            ) {
                // Backfill the date_approved field for all reviews where the status is "Approved"
                // and date_approved is NULL using the value in provider_rating.date_updated
                $this->date_approved = $this->date_updated;
            }
        }

        return parent::beforeSave();
    }

    /**
     * Needed for the extension ERememberFiltersBehavior, which saves the filters inputted after refreshing the page.
     * @return array
     */
    public function behaviors()
    {
        return array(
            'ERememberFiltersBehavior' => array(
                'class' => 'ERememberFiltersBehavior',
            ),
        );
    }

    /**
     * get Review by url
     * @example https://local.doctor.com/Dr-Lawrence_A_Kurzweil-DDS/review/Great-Dentist
     * @param string $condition
     * @param string $status
     * return array
     */
    public static function getReviewByUrl($condition = '', $status = 'A')
    {

        if (empty($condition)) {
            return null;
        }
        $sql = sprintf(
            "SELECT provider_rating.id, provider_rating.reviewer_name, provider_rating.reviewer_name_anon,
            provider_rating.rating, provider_rating.provider_id, provider_rating.description,
            provider_rating.friendly_url AS pr_friendly_url,
            provider.prefix, provider.first_last, provider.friendly_url AS p_friendly_url
            FROM provider_rating
            LEFT JOIN provider ON provider.id = provider_rating.provider_id
            WHERE `status` = 'A'
            %s",
            $condition
        );
        return (object) Yii::app()->dbRO->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();
    }

    /**
     * Return links to share a review
     * @param type $reviewId
     * @param type $shareOn
     * @param type $accountInfo
     */
    public static function shareReview($reviewId = 0, $shareOn = '', $accountInfo = array())
    {
        if (empty($reviewId)) {
            $reviewId = ApiComponent::request('review_id', 0);
        }
        if (empty($shareOn)) {
            $shareOn = ApiComponent::request('share_on', ''); // facebook | twitter
        }

        $results = ProviderRating::checkPermissions($accountInfo, $reviewId);
        if (!$results['success']) {
            return $results;
        }

        $allowedArg = array('facebook', 'twitter');
        if (!in_array($shareOn, $allowedArg)) {
            $results['success'] = false;
            $results['error'] = "ERROR_SAVING_PROVIDER_RATING";
            $results['data'] = "Invalid Arguments";
            return $results;
        }

        $recProviderRating = ProviderRating::model()->find(
            'id=:review_id AND status = "A"',
            array(':review_id' => $reviewId)
        );
        if (empty($recProviderRating)) {
            $results['success'] = false;
            $results['error'] = "ERROR_SAVING_PROVIDER_RATING";
            $results['data'] = "Review does not exist or it is not approved";
            return $results;
        }

        // get provider friendly url
        $recProvider = Provider::model()->cache(Yii::app()->params['cache_short'])->find(
            'id = :id',
            array(':id' => $recProviderRating->provider_id)
        );
        $providerFriendlyUrl = !empty($recProvider->friendly_url) ? $recProvider->friendly_url : '';

        if (empty($recProviderRating->friendly_url) || empty($recProviderRating->twitter_url_short)) {

            // always generate a friendly url
            $recProviderRating->friendly_url = ProviderRating::getFriendlyUrl(
                $recProviderRating->title,
                $recProviderRating->id
            );

            // Create urls to share review
            $twitterListingTypeId = 39;
            $urls['twitter'] = StringComponent::prepareUrlToShareReview(
                $recProviderRating,
                $twitterListingTypeId,
                false,
                $providerFriendlyUrl
            );
            $recProviderRating->twitter_url_short = !empty($urls['twitter']['short'])
                ? $urls['twitter']['short']
                : null;

            if (!$recProviderRating->save()) {
                $results['success'] = false;
                $results['error'] = "ERROR_SAVING_PROVIDER_RATING";
                $results['data'] = $recProviderRating->getErrors();
                return $results;
            }

            $auditSection = 'Share on ' . $shareOn . ' for provider #' . $recProviderRating->provider_id .
                ' (ReviewId:' . $reviewId . ')';
            AuditLog::create('U', $auditSection, 'provider_rating', null, false);
        }

        $shareOnAttribute = 'share_on_' . $shareOn;
        if (empty($recProviderRating->$shareOnAttribute)) {
            $recProviderRating->$shareOnAttribute = date("Y-m-d H:i:s");
            $recProviderRating->save();
        }

        $results['success'] = true;
        $results['error'] = "";
        $results['data'] = ProviderRating::getProviderRatings(
            $accountInfo,
            $recProviderRating->practice_id,
            $recProviderRating->provider_id,
            " AND provider_rating.id = " . $recProviderRating->id
        );
        $results['data'][0]['external'] = 0;
        $results['data'][0]['share_on_twitter_href'] = $recProviderRating->twitter_url_short;
        $results['data'][0]['share_on_facebook_href'] = 'https://' . Yii::app()->params->servers['dr_web_hn'] . '/'
            . $providerFriendlyUrl . '/review/' . $recProviderRating->friendly_url . '?source=facebook';

        return $results;
    }

    /**
     * Return full friendly url, based on the provider_rating->id
     *
     * @param int $reviewId
     * @return string $reviewFriendlyUrl
     */
    public static function getFullReviewFriendlyUrl($reviewId)
    {
        $sql = sprintf(
            "SELECT provider.friendly_url as provider_url, provider_rating.friendly_url as review_url
            FROM provider_rating
            INNER JOIN provider
                ON provider_rating.provider_id = provider.id
            WHERE provider_rating.id = %d
            LIMIT 1;",
            $reviewId
        );
        $result = Yii::app()->dbRO->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();

        if (!empty($result)) {
            return 'https://' . Yii::app()->params->servers['dr_web_hn'] . '/' .
                $result['provider_url'] . '/review/' . $result['review_url'];
        }

        return null;
    }

    /**
     * Generate the friendly URL for a review given its title
     *
     * @param string $providerRatingTitle
     * @param int $providerRatingId
     * @return string
     */
    public static function getFriendlyUrl($providerRatingTitle, $providerRatingId = 0)
    {
        /**
         * YP doesn't allow for URLs longer than 200 chars so we need to shorten our own URLs
         * We're shortening to 100 chars here because:
         * 1) We may append an autoincremental number if the URL already exists, or a randon number between 111111 and
         * 999999 so we need room for a dash and that number
         * 2) We need room for 'https://www.doctor.com/' (23)
         * 3) We need room for provider.friendly_url (current max length is 56)
         * 4) We need room for '/review/' (8) -> 87
         */

        // sanitize string
        $url = StringComponent::prepareNewFriendlyUrl(substr($providerRatingTitle, 0, 100));

        // create unique friendly_url
        return StringComponent::getNewFriendlyUrl($url, $providerRatingId, 'provider_rating');
    }

    /**
     * @todo document
     * @return array
     */
    public function getRegisterCount()
    {

        $where = '';
        if ($_GET['partners'] != '') {
            $where = ' WHERE partner_id IN (' . $_GET['partners'] . ') ';
        }

        if ($_GET['partner_sites'] != '') {
            if ($where != '') {
                $where .= ' AND ';
            } else {
                $where = ' WHERE ';
            }
            $where .= ' partner_site_id IN (' . $_GET['partner_sites'] . ') ';
        }

        $date = PartnerFilters::getDate($_GET['data_range']);
        if ($date != '') {
            if ($where != '') {
                $where .= ' AND ';
            } else {
                $where = ' WHERE ';
            }
            $aux = explode('-', $date);
            if (count($aux) > 1) {
                $where .= 'date_added >= "' . $date . '"';
            } else {
                $where .= ' year(date_added) >= "' . $date . '"';
            }
        }

        $sql = 'SELECT COUNT(id) as count FROM provider_rating ' . $where;

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id . '" '
            . 'rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="'
            . $this->provider_full_name . '">' . $this->provider_full_name . '</a>');
    }

    /**
     * Calculate a provider's average rating among approved ratings
     * @param int $providerId
     * @return int
     */
    public function calculateAverage($providerId)
    {

        $sql = sprintf(
            "SELECT ROUND(AVG(rating), 0) AS avg_rating
            FROM provider_rating
            WHERE provider_id = %d
            AND status = 'A'
            AND partner_site_id IN (1, 63, 152);",
            $providerId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Calculate provider's averages rating among approved ratings
     * @param array $arrReviews
     * @return array
     */
    public function getStats($arrReviews)
    {
        $reviewsCount = 0;

        $avgRating = $avgRatingCount = 0;
        $avgGettingAppt = $avgGettingApptCount = 0;
        $avgAppearance = $avgAppearanceCount = 0;
        $avgCourtesy = $avgCourtesyCount = 0;
        $avgWaitingTime = $avgWaitingTimeCount = 0;
        $avgSpendingTime = $avgSpendingTimeCount = 0;
        $avgDiagnosis = $avgDiagnosisCount = 0;
        $avgFollowUp = $avgFollowUpCount = 0;
        $avgBilling = $avgBillingCount = 0;
        $avgListeningSkills = $avgClearExplanations = $avgTrust = $avgSentiment = 0;
        $avgListeningSkillsCount = $avgClearExplanationsCount = $avgTrustCount = $avgSentimentCount = 0;

        foreach ($arrReviews as $eachReview) {

            $reviewsCount += 1;

            if (!empty($eachReview['rating'])) {
                $avgRatingCount += 1;
                $avgRating += $eachReview['rating'];
            }
            if (!empty($eachReview['rating_getting_appt']) && $eachReview['rating_getting_appt'] > 0) {
                $avgGettingApptCount += 1;
                $avgGettingAppt += $eachReview['rating_getting_appt'];
            }
            if (!empty($eachReview['rating_appearance']) && $eachReview['rating_appearance'] > 0) {
                $avgAppearanceCount += 1;
                $avgAppearance += $eachReview['rating_appearance'];
            }
            if (!empty($eachReview['rating_courtesy']) && $eachReview['rating_courtesy'] > 0) {
                $avgCourtesyCount += 1;
                $avgCourtesy += $eachReview['rating_courtesy'];
            }
            if (!empty($eachReview['rating_waiting_time'])) {
                $avgWaitingTimeCount += 1;
                $avgWaitingTime += $eachReview['rating_waiting_time'];
            }
            if (!empty($eachReview['rating_spending_time']) && $eachReview['rating_spending_time'] > 0) {
                $avgSpendingTimeCount += 1;
                $avgSpendingTime += $eachReview['rating_spending_time'];
            }
            if (!empty($eachReview['rating_diagnosis']) && $eachReview['rating_diagnosis'] > 0) {
                $avgDiagnosisCount += 1;
                $avgDiagnosis += $eachReview['rating_diagnosis'];
            }
            if (!empty($eachReview['rating_follow_up']) && $eachReview['rating_follow_up'] > 0) {
                $avgFollowUpCount += 1;
                $avgFollowUp += $eachReview['rating_follow_up'];
            }
            if (!empty($eachReview['rating_billing']) && $eachReview['rating_billing'] > 0) {
                $avgBillingCount += 1;
                $avgBilling += $eachReview['rating_billing'];
            }

            if (!empty($eachReview['rating_listening_skills']) && $eachReview['rating_listening_skills'] > 0) {
                $avgListeningSkillsCount += 1;
                $avgListeningSkills += $eachReview['rating_listening_skills'];
            }

            if (!empty($eachReview['rating_clear_explanations']) && $eachReview['rating_clear_explanations'] > 0) {
                $avgClearExplanationsCount += 1;
                $avgClearExplanations += $eachReview['rating_clear_explanations'];
            }

            if (!empty($eachReview['rating_trust']) && $eachReview['rating_trust'] > 0) {
                $avgTrustCount += 1;
                $avgTrust += $eachReview['rating_trust'];
            }

            if (!empty($eachReview['rating_sentiment']) && $eachReview['rating_sentiment'] > 0) {
                $avgSentimentCount += 1;
                $avgSentiment += $eachReview['rating_sentiment'];
            }
        }

        if ($avgRatingCount > 0) {
            $avgRating = ceil($avgRating / $avgRatingCount);
        }
        if ($avgGettingApptCount > 0) {
            $avgGettingAppt = ceil($avgGettingAppt / $avgGettingApptCount);
        }
        if ($avgAppearanceCount > 0) {
            $avgAppearance = ceil($avgAppearance / $avgAppearanceCount);
        }
        if ($avgCourtesyCount > 0) {
            $avgCourtesy = ceil($avgCourtesy / $avgCourtesyCount);
        }
        if ($avgWaitingTimeCount > 0) {
            $avgWaitingTime = ceil($avgWaitingTime / $avgWaitingTimeCount);
        }
        if ($avgSpendingTimeCount > 0) {
            $avgSpendingTime = ceil($avgSpendingTime / $avgSpendingTimeCount);
        }
        if ($avgDiagnosisCount > 0) {
            $avgDiagnosis = ceil($avgDiagnosis / $avgDiagnosisCount);
        }
        if ($avgFollowUpCount > 0) {
            $avgFollowUp = ceil($avgFollowUp / $avgFollowUpCount);
        }
        if ($avgBillingCount > 0) {
            $avgBilling = ceil($avgBilling / $avgBillingCount);
        }
        if ($avgListeningSkillsCount > 0) {
            $avgListeningSkills = ceil($avgListeningSkills / $avgListeningSkillsCount);
        }
        if ($avgClearExplanationsCount > 0) {
            $avgClearExplanations = ceil($avgClearExplanations / $avgClearExplanationsCount);
        }
        if ($avgTrustCount > 0) {
            $avgTrust = ceil($avgTrust / $avgTrustCount);
        }
        if ($avgSentimentCount > 0) {
            $avgSentiment = ceil($avgSentiment / $avgSentimentCount);
        }

        $arrStats = array();
        $arrStats['reviews'] = $reviewsCount;
        $arrStats['avg_rating'] = $avgRating;
        $arrStats['avg_getting_appt'] = $avgGettingAppt;
        $arrStats['avg_appearance'] = $avgAppearance;
        $arrStats['avg_courtesy'] = $avgCourtesy;
        $arrStats['avg_waiting_time'] = $avgWaitingTime;
        $arrStats['avg_spending_time'] = $avgSpendingTime;
        $arrStats['avg_diagnosis'] = $avgDiagnosis;
        $arrStats['avg_follow_up'] = $avgFollowUp;
        $arrStats['avg_billing'] = $avgBilling;
        $arrStats['avg_listening_skills'] = $avgListeningSkills;
        $arrStats['avg_clear_explanations'] = $avgClearExplanations;
        $arrStats['avg_trust'] = $avgTrust;
        $arrStats['avg_sentiment'] = $avgSentiment;

        return $arrStats;
    }

    /**
     * Return practices reviews
     * @param int $practiceId
     * @param int $limit
     * @return array
     */
    public static function getPracticeReviews($practiceId = 0, $limit = 100)
    {
        if ($practiceId == 0) {
            return null;
        }

        return ProviderRating::model()->findAll(
            array(
                'order' => 'date_added DESC',
                'condition' => "practice_id = :practice_id  AND status = 'A'",
                'limit' => $limit,
                'params' => array(":practice_id" => $practiceId)
            )
        );
    }

    /**
     * Renders the value shown in the grid for the 'Review Headline' column.
     * @return raw HTML to be used later on in the tooltip.
     */
    public function getTitleAndComment()
    {
        $title = $this->title;
        if (strlen($title) > 35) {
            $title = substr($title, 0, 30) . "[...]";
        }
        return ('<a class="commentTooltip" rel="/administration/commentTooltip?id=' . $this->id . '" title="'
            . $this->title . '">' . $title . '</a>');
    }

    /**
     * Get all reviews from Organization by Provider
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationByProvider($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT
                provider.first_middle_last AS ProviderName,
                practice.name AS PracticeName,
                provider_rating.status AS Status,
                provider_rating.rating AS OverallRating,
                IF(
                    (provider_rating.partner_site_id = 54 OR provider_rating.partner_id = 49 ),
                    '<redacted>',
                    provider_rating.title
                    ) AS Title,
                IF(
                    (provider_rating.partner_site_id = 54 OR provider_rating.partner_id = 49 ),
                    '<redacted>',
                    provider_rating.description
                    ) AS Description,
                provider_rating.date_added AS ReviewDate,
                IF(provider_rating.account_device_id is NULL, 'Web', 'Reviewhub') AS Source
            FROM provider_rating
            INNER JOIN practice
                ON practice.id = provider_rating.practice_id
            LEFT JOIN provider
                ON provider.id = provider_rating.provider_id
            WHERE provider_rating.practice_id  IN (SELECT practice_id FROM account_practice WHERE account_id = %d)
                OR provider_rating.provider_id IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                AND provider_rating.status = 'A'
            ORDER BY provider_rating.date_added;",
            $account->id,
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization by Practice
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationByPractice($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT
                practice.name AS PracticeName,
                TRIM(CONCAT(practice.address, ' ', IF(practice.address_2 IS NOT NULL, practice.address_2, ''), ', ',
                location.zipcode, ' ', location.city_name)) AS Location,
                provider_rating.status AS Status,
                provider_rating.rating AS OverallRating,
                IF(
                    (provider_rating.partner_site_id = 54 OR provider_rating.partner_id = 49 ),
                    '<redacted>',
                    provider_rating.title
                    ) AS Title,
                IF(
                    (provider_rating.partner_site_id = 54 OR provider_rating.partner_id = 49 ),
                    '<redacted>',
                    provider_rating.description
                    ) AS Description,
                provider_rating.date_added AS ReviewDate,
                IF(provider_rating.account_device_id is NULL, 'Web', 'Reviewhub') AS Source
            FROM provider_rating
            INNER JOIN practice
                ON practice.id = provider_rating.practice_id
            LEFT JOIN location
                ON location.id = practice.location_id
            WHERE provider_rating.practice_id IN (SELECT practice_id FROM account_practice WHERE account_id = %d)
                AND provider_rating.status = 'A'
            GROUP BY practice.id
            ORDER BY practice.name , `location`;",
            $account->id,
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization by Provider and Practice
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationByProviderPractice($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT
                provider.first_middle_last AS ProviderName,
                practice.name AS PracticeName,
                TRIM(CONCAT(practice.address, ' ', IF(practice.address_2 IS NOT NULL, practice.address_2, ''), ', ',
                location.zipcode, ' ', location.city_name)) AS Location,
                provider_rating.status AS Status,
                provider_rating.rating AS OverallRating,
                IF(
                    (provider_rating.partner_site_id = 54 OR provider_rating.partner_id = 49 ),
                    '<redacted>',
                    provider_rating.title
                    ) AS Title,
                IF(
                    (provider_rating.partner_site_id = 54 OR provider_rating.partner_id = 49 ),
                    '<redacted>',
                    provider_rating.description
                    ) AS Description,
                provider_rating.date_added AS ReviewDate,
                IF(provider_rating.account_device_id is NULL, 'Web', 'Reviewhub') AS Source
            FROM provider_rating
            INNER JOIN practice
                ON practice.id = provider_rating.practice_id
            INNER JOIN provider
                ON provider.id = provider_rating.provider_id
            LEFT JOIN location
                ON location.id = practice.location_id
            WHERE provider_rating.practice_id
                    IN (SELECT practice_id FROM account_practice WHERE account_id = %d)
                OR provider_rating.provider_id
                    IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                AND provider_rating.status = 'A'
            GROUP BY provider.id, practice.id
            ORDER BY provider.first_middle_last, practice.name, location;",
            $account->id,
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization by year by provider
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationByYearByProvider($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT provider.first_middle_last AS ProviderName,
            COUNT(provider_rating.id) AS COUNT,
            ROUND(AVG(provider_rating.rating), 1) AS AVG
            FROM provider_rating
            INNER JOIN provider
                ON provider.id = provider_rating.provider_id
            WHERE provider_rating.provider_id
                    IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                AND YEAR(provider_rating.date_added)= YEAR(NOW())
                AND provider_rating.status = 'A'
            GROUP BY provider.id
            ORDER BY provider.first_middle_last;",
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization by year by practice
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationByYearByPractice($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT practice.name as PracticeName,
            TRIM(CONCAT(practice.address, ' ', IF(practice.address_2 IS NOT NULL, practice.address_2, ''), ', ',
            location.zipcode, ' ', location.city_name)) as Location,
            COUNT(provider_rating.id) AS COUNT,
            ROUND(AVG(provider_rating.rating), 1) AS AVG
            FROM provider_rating
            INNER JOIN practice
                ON practice.id = provider_rating.practice_id
            LEFT JOIN location
                ON location.id = practice.location_id
            WHERE provider_rating.practice_id
                    IN (SELECT practice_id FROM account_practice WHERE account_id = %d)
                AND YEAR(provider_rating.date_added)= YEAR(NOW())
                AND provider_rating.status = 'A'
            GROUP BY practice.id
            ORDER BY practice.name , location;",
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization by year by provider and practice
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationByYearByProviderPractice($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT provider.first_middle_last AS ProviderName, practice.name as PracticeName,
            TRIM(CONCAT(practice.address, ' ', IF(practice.address_2 IS NOT NULL, practice.address_2, ''), ', ',
            location.zipcode, ' ', location.city_name)) as Location,
            COUNT(provider_rating.id) AS COUNT,
            ROUND(AVG(provider_rating.rating), 1) AS AVG
            FROM provider_rating
            INNER JOIN provider
                ON provider.id = provider_rating.provider_id
            INNER JOIN practice
                ON practice.id = provider_rating.practice_id
            LEFT JOIN location
                ON location.id = practice.location_id
            WHERE provider_rating.provider_id
                    IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                AND YEAR(provider_rating.date_added)= YEAR(NOW())
                AND provider_rating.status = 'A'
            GROUP BY provider.id, practice.id
            ORDER BY provider.first_middle_last, practice.name , location;",
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization monthly by provider
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationMonthlyByProvider($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT ProviderName,
            SUM(COUNT_1) AS COUNT_1, SUM(AVG_1) AS AVG_1,
            SUM(COUNT_2) AS COUNT_2, SUM(AVG_2) AS AVG_2,
            SUM(COUNT_3) AS COUNT_3, SUM(AVG_3) AS AVG_3
            FROM (
                SELECT
                    provider.first_middle_last AS ProviderName,
                    COUNT(provider_rating.id) AS COUNT_1,
                    ROUND(AVG(provider_rating.rating), 1) AS AVG_1,
                    0 AS COUNT_2,
                    0 AS AVG_2,
                    0 AS COUNT_3,
                    0 AS AVG_3
                FROM provider
                    INNER JOIN account_provider ON account_provider.provider_id = provider.id
                    LEFT JOIN provider_rating ON (provider.id = provider_rating.provider_id
                    AND MONTH(provider_rating.date_added)=MONTH(DATE_SUB(NOW(),INTERVAL 2 MONTH))
                    AND YEAR(provider_rating.date_added)=YEAR(DATE_SUB(NOW(),INTERVAL 2 MONTH)))
                WHERE account_provider.account_id = %d
                    AND provider_rating.status = 'A'
                GROUP BY provider.id
                UNION ALL
                SELECT
                    provider.first_middle_last AS ProviderName,
                    0 AS COUNT_1,
                    0 AS AVG_1,
                    COUNT(provider_rating.id) AS COUNT_2,
                    ROUND(AVG(provider_rating.rating), 1) AS AVG_2,
                    0 AS COUNT_3,
                    0 AS  AVG_3
                FROM provider
                    INNER JOIN account_provider ON account_provider.provider_id = provider.id
                    LEFT JOIN provider_rating ON (provider.id = provider_rating.provider_id
                    AND MONTH(provider_rating.date_added)=MONTH(DATE_SUB(NOW(),INTERVAL 1 MONTH))
                    AND YEAR(provider_rating.date_added)=YEAR(DATE_SUB(NOW(),INTERVAL 1 MONTH)))
                WHERE account_provider.account_id = %d
                    AND provider_rating.status = 'A'
                GROUP BY provider.id
                UNION ALL
                SELECT
                    provider.first_middle_last AS ProviderName,
                    0 AS COUNT_1,
                    0 AS AVG_1,
                    0 AS COUNT_2,
                    0 AS AVG_2,
                    COUNT(provider_rating.id) AS COUNT_3,
                    ROUND(AVG(provider_rating.rating), 1) AS AVG_3
                FROM provider
                    INNER JOIN account_provider ON account_provider.provider_id = provider.id
                    LEFT JOIN provider_rating ON (provider.id = provider_rating.provider_id
                    AND MONTH(provider_rating.date_added) = MONTH(NOW())
                    AND YEAR(provider_rating.date_added) = YEAR(NOW()))
                WHERE account_provider.account_id = %d
                    AND provider_rating.status = 'A'
                GROUP BY provider.id
            )
            AS stats_in_last_3_month
            GROUP BY ProviderName
            HAVING (COUNT_1 <> 0 OR COUNT_2 <> 0 OR COUNT_3 <> 0)
            ORDER BY ProviderName;",
            $account->id,
            $account->id,
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization monthly by practice
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationMonthlyByPractice($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT PracticeName,  Location,
            SUM(COUNT_1) AS COUNT_1, SUM(AVG_1) AS AVG_1,
            SUM(COUNT_2) AS COUNT_2, SUM(AVG_2) AS AVG_2,
            SUM(COUNT_3) AS COUNT_3, SUM(AVG_3) AS AVG_3
            FROM (
                SELECT
                    practice.name as PracticeName,
                    TRIM(CONCAT(practice.address, ' ', IF(address_2 IS NOT NULL, address_2, ''), ', ', zipcode, ' ',
                    city_name)) AS Location,
                    COUNT(provider_rating.id) AS COUNT_1,
                    ROUND(AVG(provider_rating.rating), 1) AS AVG_1,
                    0 AS COUNT_2,
                    0 AS AVG_2,
                    0 AS COUNT_3,
                    0 AS AVG_3
                FROM practice
                    INNER JOIN account_practice ON account_practice.practice_id = practice.id
                    LEFT JOIN provider_rating ON (practice.id = provider_rating.practice_id
                    AND MONTH(provider_rating.date_added)=MONTH(DATE_SUB(NOW(),INTERVAL 2 MONTH))
                    AND YEAR(provider_rating.date_added)=YEAR(DATE_SUB(NOW(),INTERVAL 2 MONTH)))
                    INNER JOIN location ON location.id = practice.location_id
                WHERE account_practice.account_id = %d
                GROUP BY practice.id
                UNION ALL
                SELECT
                    practice.name as PracticeName,
                    TRIM(CONCAT(practice.address, ' ', IF(address_2 IS NOT NULL,address_2, ''), ', ', zipcode, ' ',
                    city_name)) AS Location,
                    0 AS COUNT_1,
                    0 AS AVG_1,
                    COUNT(provider_rating.id) AS COUNT_2,
                    ROUND(AVG(provider_rating.rating), 1) AS AVG_2,
                    0 AS COUNT_3,
                    0 AS  AVG_3
                FROM practice
                    INNER JOIN account_practice ON account_practice.practice_id = practice.id
                    LEFT JOIN provider_rating ON (practice.id = provider_rating.practice_id
                    AND MONTH(provider_rating.date_added)=MONTH(DATE_SUB(NOW(),INTERVAL 1 MONTH))
                    AND YEAR(provider_rating.date_added)=YEAR(DATE_SUB(NOW(),INTERVAL 1 MONTH)))
                    INNER JOIN location ON location.id = practice.location_id
                WHERE account_practice.account_id = %d
                GROUP BY practice.id
                UNION ALL
                SELECT
                    practice.name as PracticeName,
                    TRIM(CONCAT(practice.address, ' ', IF(address_2 IS NOT NULL, address_2, ''), ', ', zipcode, ' ',
                    city_name)) AS Location,
                    0 AS COUNT_1,
                    0 AS AVG_1,
                    0 AS COUNT_2,
                    0 AS AVG_2,
                    COUNT(provider_rating.id) AS COUNT_3,
                    ROUND(AVG(provider_rating.rating), 1) AS AVG_3
                FROM practice
                    INNER JOIN account_practice ON account_practice.practice_id = practice.id
                    LEFT JOIN provider_rating ON (practice.id = provider_rating.practice_id
                    AND MONTH(provider_rating.date_added) = MONTH(NOW())
                    AND YEAR(provider_rating.date_added) = YEAR(NOW()))
                    INNER JOIN location ON location.id = practice.location_id
                WHERE account_practice.account_id = %d
                GROUP BY practice.id
            )
            AS stats_in_last_3_month
            GROUP BY PracticeName, Location
            HAVING (COUNT_1 <> 0 OR COUNT_2 <> 0 OR COUNT_3 <> 0)
            ORDER BY PracticeName, Location;",
            $account->id,
            $account->id,
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization monthly by provider and practice
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationMonthlyByProviderPractice($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT ProviderName, PracticeName,  Location,
            SUM(COUNT_1) AS COUNT_1, SUM(AVG_1) AS AVG_1,
            SUM(COUNT_2) AS COUNT_2, SUM(AVG_2) AS AVG_2,
            SUM(COUNT_3) AS COUNT_3, SUM(AVG_3) AS AVG_3
            FROM (
                SELECT
                    provider.first_middle_last AS ProviderName,
                    practice.name as PracticeName,
                    TRIM(CONCAT(practice.address, ' ', IF(address_2 IS NOT NULL, address_2, ''), ', ', zipcode, ' ',
                    city_name)) AS Location,
                    COUNT(provider_rating.id) AS COUNT_1,
                    ROUND(AVG(provider_rating.rating), 1) AS AVG_1,
                    0 AS COUNT_2,
                    0 AS AVG_2,
                    0 AS COUNT_3,
                    0 AS AVG_3
                FROM provider
                    INNER JOIN account_provider ON account_provider.provider_id = provider.id
                    LEFT JOIN provider_rating ON (provider.id = provider_rating.provider_id
                    AND MONTH(provider_rating.date_added)=MONTH(DATE_SUB(NOW(),INTERVAL 2 MONTH))
                    AND YEAR(provider_rating.date_added)=YEAR(DATE_SUB(NOW(),INTERVAL 2 MONTH)))
                    LEFT JOIN practice ON practice.id = provider_rating.practice_id
                    LEFT JOIN location ON location.id = practice.location_id
                WHERE account_provider.account_id = %d
                GROUP BY provider.id
                UNION ALL
                SELECT
                    provider.first_middle_last AS ProviderName,
                    practice.name as PracticeName,
                    TRIM(CONCAT(practice.address, ' ', IF(address_2 IS NOT NULL,address_2, ''), ', ', zipcode, ' ',
                    city_name)) AS Location,
                    0 AS COUNT_1,
                    0 AS AVG_1,
                    COUNT(provider_rating.id) AS COUNT_2,
                    ROUND(AVG(provider_rating.rating), 1) AS AVG_2,
                    0 AS COUNT_3,
                    0 AS  AVG_3
                FROM provider
                    INNER JOIN account_provider ON account_provider.provider_id = provider.id
                    LEFT JOIN provider_rating ON (provider.id = provider_rating.provider_id
                    AND MONTH(provider_rating.date_added)=MONTH(DATE_SUB(NOW(),INTERVAL 1 MONTH))
                    AND YEAR(provider_rating.date_added)=YEAR(DATE_SUB(NOW(),INTERVAL 1 MONTH)))
                    LEFT JOIN practice ON practice.id = provider_rating.practice_id
                    LEFT JOIN location ON location.id = practice.location_id
                WHERE account_provider.account_id = %d
                GROUP BY provider.id
                UNION ALL
                SELECT
                    provider.first_middle_last AS ProviderName,
                    practice.name as PracticeName,
                    TRIM(CONCAT(practice.address, ' ', IF(address_2 IS NOT NULL, address_2, ''), ', ', zipcode, ' ',
                    city_name)) AS Location,
                    0 AS COUNT_1,
                    0 AS AVG_1,
                    0 AS COUNT_2,
                    0 AS AVG_2,
                    COUNT(provider_rating.id) AS COUNT_3,
                    ROUND(AVG(provider_rating.rating), 1) AS AVG_3
                FROM provider
                    INNER JOIN account_provider ON account_provider.provider_id = provider.id
                    LEFT JOIN provider_rating ON (provider.id = provider_rating.provider_id
                    AND MONTH(provider_rating.date_added) = MONTH(NOW())
                    AND YEAR(provider_rating.date_added) = YEAR(NOW()))
                    LEFT JOIN practice ON practice.id = provider_rating.practice_id
                    LEFT JOIN location ON location.id = practice.location_id
                WHERE account_provider.account_id = %d
                GROUP BY provider.id
            )
            AS stats_in_last_3_month
            GROUP BY ProviderName
            HAVING (COUNT_1 <> 0 OR COUNT_2 <> 0 OR COUNT_3 <> 0)
            ORDER BY ProviderName;",
            $account->id,
            $account->id,
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization from the last week by provider
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationLastWeekByProvider($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT
            provider.first_middle_last AS ProviderName,
            COUNT(provider_rating.id) AS COUNT,
            ROUND(AVG(provider_rating.rating), 1) AS AVG
            FROM provider_rating
            INNER JOIN provider
                ON provider.id = provider_rating.provider_id
            WHERE provider_rating.provider_id
                    IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                AND DATE(provider_rating.date_added) BETWEEN DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND CURDATE()
                AND provider_rating.status='A'
            GROUP BY provider.id
            ORDER BY provider.first_middle_last;",
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization from the last week by practice
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationLastWeekByPractice($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT
            practice.name as PracticeName,
            TRIM(CONCAT(practice.address, ' ', IF(address_2 IS NOT NULL,address_2, ''), ', ', zipcode, ' ',
            city_name)) as Location,
            COUNT(provider_rating.id) AS COUNT,
            ROUND(AVG(provider_rating.rating), 1) AS AVG
            FROM provider_rating
            INNER JOIN practice
                ON practice.id = provider_rating.practice_id
            LEFT JOIN location
                ON location.id = practice.location_id
            WHERE provider_rating.practice_id
                    IN (SELECT practice_id FROM account_practice WHERE account_id = %d)
                AND DATE(provider_rating.date_added) BETWEEN DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND CURDATE()
                AND provider_rating.status='A'
            GROUP BY practice.id
            ORDER BY practice.name, Location;",
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all reviews from Organization from the last week by provider and practice
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationLastWeekByProviderPractice($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT provider.first_middle_last AS ProviderName, practice.name as PracticeName,
            TRIM(CONCAT(practice.address, ' ', IF(address_2 IS NOT NULL,address_2, ''), ', ', zipcode, ' ',
            city_name)) as Location,
            COUNT(provider_rating.id) AS COUNT,
            ROUND(AVG(provider_rating.rating), 1) AS AVG
            FROM provider_rating
            INNER JOIN provider
                ON provider.id = provider_rating.provider_id
            INNER JOIN practice
                ON practice.id = provider_rating.practice_id
            LEFT JOIN location
                ON location.id = practice.location_id
            WHERE provider_rating.provider_id
                    IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                AND DATE(provider_rating.date_added) BETWEEN DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND CURDATE()
                AND provider_rating.status='A'
            GROUP BY provider.id, practice.id
            ORDER BY provider.first_middle_last, practice.name, Location;",
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get details from reviews from providers' Organization by provider
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationDetailsByProvider($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT provider_rating.date_added AS ReviewDate,
            provider.first_middle_last AS ProviderName, provider_rating.rating AS Overall, title AS Title,
            provider_rating.description AS Description
            FROM provider_rating
            INNER JOIN practice
                ON practice.id = provider_rating.practice_id
            LEFT JOIN provider
                ON provider.id = provider_rating.provider_id
            WHERE provider_rating.provider_id
                    IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                AND provider_rating.status='A'
                AND  DATE(provider_rating.date_added) BETWEEN DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND CURDATE()
            ORDER BY provider_rating.date_added;",
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get details from reviews from providers' Organization by practice
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationDetailsByPractice($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT provider_rating.date_added AS ReviewDate,
            practice.name as PracticeName,
            TRIM(CONCAT(practice.address,' ',IF(address_2 IS NOT NULL,address_2, ''),', ',zipcode, ' ',city_name)) AS
            Location,
            provider_rating.rating AS Overall, title AS Title,
            provider_rating.description AS Description
            FROM provider_rating
            INNER JOIN practice
                ON practice.id = provider_rating.practice_id
            LEFT JOIN location
                ON location.id = practice.location_id
            WHERE provider_rating.practice_id
                    IN (SELECT practice_id FROM account_practice WHERE account_id = %d)
                AND provider_rating.status='A'
                AND DATE(provider_rating.date_added) BETWEEN DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND CURDATE()
            ORDER BY provider_rating.date_added;",
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get details from reviews from providers' Organization by provider and practice
     * @param int $organizationId
     * @return array
     */
    public static function getReviewsFromOrganizationDetailsByProviderPractice($organizationId)
    {
        $organization = Organization::model()->findByPk($organizationId);

        if (empty($organization)) {
            return false;
        }

        $account = $organization->getOwnerAccount();

        $sql = sprintf(
            "SELECT provider_rating.date_added AS ReviewDate, provider.first_middle_last AS ProviderName,
            practice.name as PracticeName,
            TRIM(CONCAT(practice.address,' ',IF(address_2 IS NOT NULL,address_2, ''),', ',zipcode, ' ',city_name)) AS
            Location,
            provider_rating.rating AS Overall, title AS Title,
            provider_rating.description AS Description
            FROM provider_rating
            INNER JOIN practice
                ON practice.id = provider_rating.practice_id
            LEFT JOIN location
                ON location.id = practice.location_id
            LEFT JOIN provider
                ON provider.id = provider_rating.provider_id
            WHERE provider_rating.provider_id
                    IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                AND provider_rating.status='A'
                AND  DATE(provider_rating.date_added) BETWEEN DATE_SUB(CURDATE(), INTERVAL 6 DAY) AND CURDATE()
            ORDER BY provider_rating.date_added;",
            $account->id
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get the total of ratings depends the status of these
     * @param int $id
     * @param string $status
     * @param boolean $hasEmail
     * @return int
     */
    public function getCountRatingsOfStatus($id, $status = '', $hasEmail = '')
    {
        $patientCondition = sprintf("patient_id = %d AND status = '%s'", $id, $status);

        if ($hasEmail != '') {
            $patientCondition = sprintf("reviewer_email = '%s' ", $hasEmail);
        }

        $sql = sprintf(
            "SELECT COUNT(1) AS ratings
            FROM provider_rating
            WHERE %s;",
            $patientCondition
        );

        return intval(Yii::app()->db->createCommand($sql)->queryScalar());
    }

    /**
     * Get reviews for logged in patient
     * @param array $request
     * @return array
     */
    public static function getRatingsData($request = array())
    {

        $order = !empty($request['order']) ? $request['order'] : '';

        if ($order == 'p') {
            $order = 'provider_name';
        } elseif ($order == 'r') {
            $order = 'rating';
        } else {
            $order = 'provider_rating.date_added';
        }

        $sort = !empty($request['sort']) ? $request['sort'] : '';
        if ($sort == 'd') {
            $sort = 'DESC';
        } else {
            $sort = 'ASC';
        }

        // filter
        $filterProvider = addslashes(trim(!empty($request['filter_provider']) ? $request['filter_provider'] : ''));
        $filterReviewer = addslashes(trim(!empty($request['filter_reviewer']) ? $request['filter_reviewer'] : ''));
        $filterTitle = addslashes(trim(!empty($request['filter_title']) ? $request['filter_title'] : ''));

        $condition = '';

        if (!empty($filterProvider) && $filterProvider != 'Search by provider') {
            $condition .= 'AND (provider LIKE "%' . $filterProvider . '%")';
        }
        if (!empty($filterReviewer) && $filterReviewer != 'Search by reviewer') {
            $condition .= 'AND reviewer_name LIKE "%' . $filterReviewer . '%"';
        }
        if (!empty($filterTitle) && $filterTitle != 'Search by title') {
            $condition .= 'AND title LIKE "%' . $filterTitle . '%"';
        }

        $patientCondition = '1 = 1';
        if (php_sapi_name() != 'cli') {
            $patientCondition = sprintf("patient_id = %d", Yii::app()->user->id);
        }

        $sql = sprintf(
            "SELECT CONCAT(provider.prefix, ' ', provider.first_name, ' ', provider.last_name, ' ',
            credential.title) AS provider, provider_rating.reviewer_name, provider_rating.title,
            provider_rating.rating, DATE_FORMAT(provider_rating.date_added, '%%m/%%d/%%Y') AS date_added_f,
            provider_rating.id, provider_rating.status,
            provider_rating.reviewer_name_anon, CONCAT(provider.first_name, ' ', provider.last_name) AS provider_name
            FROM provider_rating
            INNER JOIN provider
                ON provider_rating.provider_id = provider.id
            INNER JOIN credential
                ON provider.credential_id = credential.id
            WHERE %s
            HAVING 1 = 1
            %s
            ORDER BY %s %s;",
            $patientCondition,
            $condition,
            $order,
            $sort
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Outputs the printable origin name
     */
    public function getOrigin()
    {
        if (!is_null($this->account_device_id)) {
            // Checking if this review was made from a Digital Review Request
            $device = Device::model()->find('id=:id', array(':id' => $this->device_id));
            if (!empty($device) && $device->device_model_id == DeviceModel::REVIEW_REQUEST_ID) {
                return 'Doctor.com ReviewRequest';
            }
            return 'Doctor.com ReviewHub';
        }

        switch ($this->origin) {
            case 'D':
                return 'Doctor.com';
            case 'X':
                return 'DoctorsDig.com';
            case 'E':
                return 'Enterprise';
            default:

                // look up based on partner site id
                if ($this->partner_site_id) {

                    $ps = PartnerSite::model()->cache(Yii::app()->params['cache_medium'])->findByPk(
                        $this->partner_site_id
                    );
                    if (isset($ps) && isset($ps->display_name)) {
                        return $ps->display_name;
                    }
                }

                if ($this->origin == 'P') {
                    // we don't know which partner
                    return 'Doctor.com Partner';
                } else {
                    // not a recognized origin
                    return 'Unknown';
                }
        }
    }

    /**
     * Outputs the appropriate message for the given wait time amount
     * @param int $ratingValue
     * @return string
     * @throws CException
     */
    public static function getWaitTimeMessage($ratingValue, $localizationCode = 'en')
    {
        $localizationCode = !empty($localizationCode) ? $localizationCode : 'en';
        Yii::app()->language = $localizationCode;
        // use: echo Yii::t('review', "lblSelectOption05");

        switch ($ratingValue) {
            case 5:
                return Yii::t('review', "lblSelectOption05");
            case 4:
                return Yii::t('review', "lblSelectOption04");
            case 3:
                return Yii::t('review', "lblSelectOption03");
            case 2:
                return Yii::t('review', "lblSelectOption02");
            case 1:
                return Yii::t('review', "lblSelectOption01");
            case 0:
                return Yii::t('review', "lblSelectOption00");
            default:
                throw new CException('Unknown wait time value: ' . $ratingValue);
        }
    }

    /* Copied */

    /**
     * Returns the avg rating of the providers in a given city
     * @param int $cityId
     * @return int
     */
    public static function getRatingByCity($cityId)
    {

        $sql = sprintf(
            "SELECT SQL_NO_CACHE AVG(provider.avg_rating) AS avg_rating
            FROM provider
            INNER JOIN provider_practice
                ON provider.id = provider_practice.provider_id
            INNER JOIN practice
                ON provider_practice.practice_id = practice.id
            INNER JOIN location
                ON practice.location_id = location.id
            WHERE provider.avg_rating > 0
                AND location.city_id = %d;",
            $cityId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryScalar();
    }

    /**
     * Returns rating avg for providers in a city (optionally specialty, optionally insurance)
     * @param int $idCity
     * @param int $idSpecialty
     * @param int $idInsurance
     * @return array
     */
    public function getRatingByCitySpecialtyInsurance($idCity = null, $idSpecialty = null, $idInsurance = null)
    {
        $criteria = [];
        $criteria['practice.city_id'] = $idCity;
        if ($idSpecialty) {
            $criteria['specialty.specialty_id'] = $idSpecialty;
        }
        if ($idInsurance) {
            $criteria['insurance.company_id'] = $idInsurance;
        }
        return $this->getAverageRating($criteria);
    }

    /**
     * Returns user rating averange for a list of subspecialties and an insurance
     * @param int $specialtyId
     * @param int $insuranceId
     * @return float
     */
    public function getRatingBySpecialtyInsurance($specialtyId = null, $insuranceId = null)
    {
        $criteria = [];
        if ($specialtyId) {
            $criteria['specialty.specialty_id'] = $specialtyId;
        }
        if ($insuranceId) {
            $criteria['insurance.company_id'] = $insuranceId;
        }
        return $this->getAverageRating($criteria);
    }

    /**
     * Returns the avg rating of the providers in a given state
     * @param int $stateId
     * @return float
     */
    public function getRatingByState($stateId)
    {
        $criteria = [];
        $criteria['practice.state_id'] = $stateId;
        return $this->getAverageRating($criteria);
    }

    /**
     * Returns rating avg for providers in a state (optionally specialty, optionally insurance)
     * @param int $stateId
     * @param int $specialtyId
     * @param int $insuranceId
     * @return float
     */
    public function getRatingByStateSpecialtyInsurance($stateId, $specialtyId = null, $insuranceId = null)
    {
        $criteria = [];
        $criteria['practice.state_id'] = $stateId;
        if ($specialtyId) {
            $criteria['specialty.specialty_id'] = $specialtyId;
        }
        if ($insuranceId) {
            $criteria['insurance.company_id'] = $insuranceId;
        }
        return $this->getAverageRating($criteria);
    }

    /**
     * Export all ratings at once
     * @param int $limitFrom
     * @param int $limitTo
     * @return array
     */
    public static function exportAllRatings($limitFrom, $limitTo)
    {
        $tableJoin = self::getJoinsAllRatings();

        $sql = sprintf(
            "SELECT partner.display_name AS partner, partner_site.url AS partner_site_url,
            CONCAT(provider.first_name, ' ', provider.last_name) AS provider,
            practice.name AS practice, CONCAT(patient.first_name, ' ', patient.last_name) AS patient,
            account.organization_name AS account,
            device.nickname AS device, reviewer_name, reviewer_name_anon,
            reviewer_email, reviewer_location, rating,
            title, provider_rating.description, rating_getting_appt,
            rating_appearance, rating_courtesy, rating_waiting_time,
            rating_friendliness, rating_spending_time, rating_diagnosis,
            rating_follow_up, rating_billing, rating_listening_skills,
            rating_clear_explanations, provider_rating.status,
            origin, reference_url, provider_rating.date_added,
            provider_rating.date_updated, provider_rating.date_approved,
            organization.id AS organization_id,
            organization.organization_name AS organization_name,
            CONCAT('%s', organization.salesforce_id) AS salesforce_id,
            IF(organization.id IS NULL, '',
                CASE
                WHEN organization.status = 'C' THEN 'Canceled'
                WHEN organization.status = 'A' THEN 'Active'
            ELSE 'Paused'
                END) AS Status
            FROM %s
            ORDER BY provider_rating.id
            LIMIT %d, %d ;",
            Yii::app()->salesforce->applicationUri,
            $tableJoin,
            $limitFrom,
            $limitTo
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Export ratings by date filter
     * @param int $limitFrom
     * @param int $limitTo
     * @param date $from_date
     * @param date $to_date
     * @param int $limitTo
     * @return array
     */
    public static function exportRatingsByDate($limitFrom, $limitTo, $from_date, $to_date)
    {

        $tableJoin = self::getJoinsAllRatings();

        $sql = sprintf(
            "SELECT partner.display_name AS partner, partner_site.url AS partner_site_url,
            CONCAT(provider.first_name, ' ', provider.last_name) AS provider,
            practice.name AS practice, CONCAT(patient.first_name, ' ', patient.last_name) AS patient,
            account.organization_name AS account,
            device.nickname AS device, reviewer_name, reviewer_name_anon,
            reviewer_email, reviewer_location, rating,
            title, provider_rating.description, rating_getting_appt,
            rating_appearance, rating_courtesy, rating_waiting_time,
            rating_friendliness, rating_spending_time, rating_diagnosis,
            rating_follow_up, rating_billing, rating_listening_skills,
            rating_clear_explanations, provider_rating.status,
            origin, reference_url, provider_rating.date_added,
            provider_rating.date_updated, provider_rating.date_approved,
            organization.id AS organization_id,
            organization.organization_name AS organization_name,
            CONCAT('%s', organization.salesforce_id) AS salesforce_id,
            IF(organization.id IS NULL, '',
                CASE
                WHEN organization.status = 'C' THEN 'Canceled'
                WHEN organization.status = 'A' THEN 'Active'
            ELSE 'Paused'
                END) AS Status
            FROM %s
            WHERE provider_rating.date_added BETWEEN '%s' AND '%s'
            ORDER BY provider_rating.date_added DESC
            LIMIT %d, %d ;",
            Yii::app()->salesforce->applicationUri,
            $tableJoin,
            $from_date,
            $to_date,
            $limitFrom,
            $limitTo
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Count all ratings
     * @return array
     */
    public static function countAllRatings()
    {
        $tableJoin = self::getJoinsAllRatings();

        $sql = sprintf("SELECT COUNT(1) FROM %s", $tableJoin);
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Centralize Tables and JOINs for AllRatings query
     * @return string
     */
    private static function getJoinsAllRatings()
    {
        return " provider_rating
            LEFT JOIN account_provider
                ON account_provider.provider_id = provider_rating.provider_id
            LEFT JOIN organization_account
                ON organization_account.account_id = account_provider.account_id
            LEFT JOIN organization
                ON organization.id = organization_account.organization_id
            LEFT JOIN partner
                ON provider_rating.partner_id = partner.id
            LEFT JOIN partner_site
                ON provider_rating.partner_site_id = partner_site.id
            LEFT JOIN provider
                ON provider_rating.provider_id = provider.id
            LEFT JOIN practice
                ON provider_rating.practice_id = practice.id
            LEFT JOIN patient
                ON provider_rating.patient_id = patient.id
            LEFT JOIN account
                ON provider_rating.account_id = account.id
            LEFT JOIN device
                ON provider_rating.device_id = device.id ";
    }

    /**
     * Generate a list of the reviews that have been approved or rejected from the audit log
     * @return array
     */
    public static function exportApprovalLog()
    {
        $sql = sprintf(
            "SELECT account.email AS admin_email, provider_rating.id AS rating_id,
            IF(
                provider_rating.origin = 'D',
                IF(device_id > 0, IF (device.device_model_id = 7, 'ReviewRequest', 'ReviewHub'),
                'Doctor.com'
            ),
            IF(provider_rating.origin = 'X', 'External', partner_site.name)) AS rating_origin,
            rating AS overall_rating,
            IF(
                `provider_rating`.`status` = 'A',
                'Approved',
                IF(`provider_rating`.`status` = 'D', 'Duplicated', 'Rejected')
            ) AS approved_or_rejected,
            DATE_FORMAT(audit_log.date_added, '%%m/%%d/%%Y %%H:%%i %%p') AS date_time
            FROM audit_log
            INNER JOIN provider_rating
                ON notes = provider_rating.id
            LEFT JOIN device
                ON provider_rating.device_id = device.id
            INNER JOIN account
                ON audit_log.admin_account_id = account.id
            LEFT JOIN partner_site
                ON provider_rating.partner_site_id = partner_site.id
            WHERE `table` = 'provider_rating'
            ORDER BY date_time DESC;"
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Return Internal Provider Rating
     * @param array $accountInfo
     * @param int $practiceId
     * @param int $providerId
     * @param string $having
     * @param string $order
     * @param string $sort
     * @return array
     */
    public static function getProviderRatings(
        $accountInfo = array(),
        $practiceId = 0,
        $providerId = 0,
        $having = '',
        $order = 'provider_rating.date_added',
        $sort = 'DESC'
    ) {

        //$accountInfo['practices'] and $accountInfo['providers'] are expected to be an array of ids
        $accountInfo['providers'] = array_column($accountInfo['providers'], 'id');
        $accountInfo['practices'] = array_column($accountInfo['practices'], 'id');

        $condition = '';

        // Check if the account has providers/practices associated
        if (empty($accountInfo['providers']) || empty($accountInfo['practices'])) {
            // Workaround: If not return an empty array (this method must be fixed)
            return array();
        }

        // validate if the user can access to this practice
        if (!empty($accountInfo['practices']) && is_array($accountInfo['practices'])) {
            if (!array_key_exists($practiceId, $accountInfo['practices'])) {
                $practiceId = 0;
            }
        }
        // validate if the user can access to this provider
        if (!empty($accountInfo['providers']) && is_array($accountInfo['providers'])) {
            if (!array_key_exists($providerId, $accountInfo['providers'])) {
                $providerId = 0;
            }
        }

        $generalCondition = '';
        $and = '';

        // Apply filters based on the account_type
        if ($accountInfo['connection_type'] == 'S') {
            // If the logged account is a Staff
            if ($accountInfo['privileges']['access_granted'] == 'providers') {
                // Allow access to the following providers
                if (!empty($accountInfo['providers'])) {
                    $generalCondition .= sprintf(
                        "provider_rating.provider_id IN (%s) ",
                        implode(',', $accountInfo['providers'])
                    );
                }
            } else {
                // Allow access to the following practices/providers
                if (!empty($accountInfo['practices'])) {
                    $generalCondition .= sprintf(
                        "provider_rating.practice_id IN (%s) ",
                        implode(',', $accountInfo['practices'])
                    );
                    $and = 'AND';
                }
                if (!empty($accountInfo['providers'])) {
                    $generalCondition .= sprintf(
                        "%s provider_rating.provider_id IN (%s) ",
                        $and,
                        implode(',', $accountInfo['providers'])
                    );
                }
            }
        } else {

            // Is the owner or manager
            if (!empty($accountInfo['practices'])) {
                $generalCondition .= sprintf(
                    "provider_rating.practice_id IN (%s) ",
                    implode(',', $accountInfo['practices'])
                );
                $and = 'AND';
            }
            if (!empty($accountInfo['providers'])) {
                $generalCondition .= sprintf(
                    "%s provider_rating.provider_id IN (%s) ",
                    $and,
                    implode(',', $accountInfo['providers'])
                );
            }
        }

        if (!empty($generalCondition)) {
            $generalCondition = sprintf("AND (%s) ", $generalCondition);
        }

        if ($practiceId > 0) {
            $condition .= sprintf("AND provider_rating.practice_id = %d ", $practiceId);
        }
        if ($providerId > 0) {
            $condition .= sprintf("AND provider_rating.provider_id = %d ", $providerId);
        }

        // Full path for 'full_friendly_url'
        $siteUrl = 'https://' . Yii::app()->params->servers['dr_web_hn'] . '/';

        $sql = sprintf(
            "SELECT provider_rating.provider_id,
            CONCAT(provider.prefix, ' ', provider.first_name, ' ', provider.last_name, ' ', credential.title) AS
            provider,
            CONCAT('%s',provider.friendly_url,'/review/',provider_rating.friendly_url ) AS full_friendly_url,
            provider.last_name, provider.first_name, provider_rating.practice_id, practice.name AS practice_name,
            practice.phone_number AS practice_phone_number,
            provider_rating.reviewer_name, provider_rating.title, provider_rating.status,
            provider_rating.patient_id, provider_rating.account_device_id, provider_rating.rating,
            provider_rating.yext_site_id, provider_rating.friendly_url as pr_friendly_url,
            origin, device.device_model_id,
            DATE_FORMAT(provider_rating.date_added, '%%m/%%d/%%y') AS date_added_f, provider_rating.date_added,
            provider_rating.id, provider_rating.reviewer_name_anon, provider_rating.share_on_facebook,
            provider_rating.date_facebook_sent, provider_rating.share_on_twitter,
            IF (provider_rating_abuse.id IS NOT NULL, true, false) AS flagged,
            IF(provider_rating_abuse.id IS NOT NULL, abuse_report, NULL) AS flagged_report,
            IF (widget.hidden > 0 , 0, IF (widget.draft > 0 , 0, prw.provider_rating_id)) AS promoted,
            twitter_url_short, facebook_clicks, twitter_clicks,
            provider_rating.description, provider.friendly_url, partner_site.display_name AS partner_site
            FROM provider_rating
            LEFT JOIN provider
                ON provider_rating.provider_id = provider.id
            LEFT JOIN credential
                ON provider.credential_id = credential.id
            LEFT JOIN provider_rating_abuse
                ON provider_rating.id = provider_rating_abuse.rating_id
            LEFT JOIN partner
                ON provider_rating.partner_id = partner.id
            LEFT JOIN patient
                ON provider_rating.patient_id = patient.id
            LEFT JOIN account_device
                ON account_device.id = provider_rating.account_device_id
            LEFT JOIN practice
                ON provider_rating.practice_id = practice.id
            LEFT JOIN device
                ON device.id = account_device.device_id
            LEFT JOIN device_model
                ON device_model.id = device.device_model_id
            LEFT JOIN partner_site
                ON provider_rating.partner_site_id = partner_site.id
            LEFT JOIN provider_rating_widget as prw
                ON provider_rating.id = prw.provider_rating_id
            LEFT JOIN widget
                ON widget.id = prw.widget_id
            WHERE
                provider_rating.status IN ('A', 'P')
                AND provider_rating.origin != 'E'
                %s /* generalCondition */
                AND yext_review_id IS NULL
                AND provider_rating.partner_site_id != 60 /* Exclude GMB */
                %s /* condition */
            GROUP BY provider_rating.id
            HAVING 1 = 1
                %s /* having */
            ORDER BY %s %s;",
            $siteUrl,
            $generalCondition,
            $condition,
            $having,
            $order,
            $sort
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * Get provider_rating by accountId to send to API (api/account)
     * @param int $accountId
     * @param string $listAccountProvider
     * @param int $limit
     * @return array
     */
    public static function getProviderRatingsForApi($accountId = 0, $listAccountProvider = '', $limit = 0)
    {
        $whereCondition = '';
        if ($accountId > 0 && !empty($listAccountProvider)) {
            $whereCondition = sprintf("account_id = %d AND provider_id IN (%s)", $accountId, $listAccountProvider);
        } elseif ($accountId > 0) {
            $whereCondition = sprintf("account_id = %d", $accountId);
        } elseif (!empty($listAccountProvider)) {
            $whereCondition = sprintf("provider_id IN (%s)", $listAccountProvider);
        } else {
            return '';
        }

        $limitCondition = '';
        if ($limit > 0) {
            $limitCondition = sprintf("LIMIT %d", $limit);
        }
        $sql = sprintf(
            "SELECT pr.id, pr.localization_id, provider_id, first_last as provider_name,
            pr.partner_id, pr.partner_site_id, partner_site.display_name as partner_name, practice_id, patient_id,
            account_device_id, account_id, device_id, shared_with, reviewer_name, reviewer_name_anon,
            reviewer_email, reviewer_location, reviewer_city_id, rating, title, pr.friendly_url,
            `description`, rating_getting_appt, rating_appearance, rating_courtesy, rating_waiting_time,
            rating_friendliness, rating_spending_time, rating_diagnosis, rating_follow_up,
            rating_billing, rating_listening_skills, rating_clear_explanations, rating_trust, rating_sentiment,
            `status`, origin, reference_url, yext_site_id, yext_review_id, source_ip_address,
            pr.date_added, pr.date_updated, date_approved, facebook_clicks, twitter_clicks,
            twitter_url_short, date_facebook_sent, share_on_facebook, share_on_twitter
            FROM provider_rating AS pr
            LEFT JOIN `provider`
                ON provider_id = `provider`.id
            LEFT JOIN partner_site
                ON pr.partner_site_id = partner_site.id
            WHERE %s
            ORDER by date_added DESC
            %s;",
            $whereCondition,
            $limitCondition
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
    }

    /**
     * PADM: Get external ratings (coming from Yext, not linked to providers but rather to practices) for an account
     * @param int $accountId
     * @param array $accountInfo
     * @return mixed
     */
    public static function getPracticeRatings($accountId = 0, $accountInfo = array())
    {
        if (empty($accountInfo)) {
            $accountInfo = Account::getInfo($accountId, true, true);
        }

        //$accountInfo['practices'] and $accountInfo['providers'] are expected to be an array of ids
        $accountInfo['providers'] = array_column($accountInfo['providers'], 'id');
        $accountInfo['practices'] = array_column($accountInfo['practices'], 'id');

        $lstProviders = implode(',', $accountInfo['providers']);
        $providerCondition = '';
        if (!empty($lstProviders)) {
            $providerCondition = sprintf(' AND (provider_id IS NULL OR provider_id IN (%s))', $lstProviders);
        }

        $lstPractices = implode(',', $accountInfo['practices']);
        $practiceCondition = '';
        if (!empty($lstPractices)) {
            $practiceCondition = sprintf(' AND practice_id IN (%s)', $lstPractices);
        } else {
            // external ratings have to be at least a practice associated to the account
            return null;
        }

        $sql = sprintf(
            "SELECT provider_rating.id AS id, reviewer_name,
            provider.first_last as provider_name, provider.first_name as first_name,
            provider.last_name as last_name,
            IF(yext_site_id IS NOT NULL, yext_site_id, partner_site.display_name) AS partner_site,
            title, rating, practice.name AS practice_name, provider_rating.yext_site_id, provider_rating.origin,
            DATE_FORMAT(provider_rating.date_added, '%%m/%%d/%%y') AS date_added_f, provider_rating.date_added,
            reference_url, status, provider.friendly_url AS provider_friendly_url,
            provider_rating.friendly_url AS friendly_url, practice.id AS practice_id,
            provider_rating.partner_site_id, provider_id, practice.friendly_url AS practice_friendly_url
            FROM provider_rating
            INNER JOIN practice
                ON provider_rating.practice_id = practice.id
            LEFT JOIN provider
                ON provider_rating.provider_id = provider.id
            LEFT JOIN partner_site
                ON provider_rating.partner_site_id = partner_site.id
            WHERE provider_rating.status IN ('A', 'P')
                %s
                %s
                /* Coming from Yext or GMB  oR Enterprise */
                AND (
                    yext_review_id IS NOT NULL
                    OR provider_rating.partner_site_id = 60
                    OR provider_rating.origin = 'E'
                    )
            ORDER BY provider_rating.date_added DESC;",
            $providerCondition,
            $practiceCondition
        );

        $arrProviderRating = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();

        foreach ($arrProviderRating as $key => $rating) {
            // competition reviews shouldn't be linked
            if (!preg_match('/(demandforce)/', $rating['reference_url'])) {
                if ($rating['partner_site_id'] == 60) {
                    // if it's GMB then link to ddc profile because the reference_url is something else
                    $rating['reference_url'] = '';
                    if ($rating['provider_id'] > 0) {
                        // we have a provider, link to the specific review in the profile
                        $arrProviderRating[$key]['reference_url'] = '//' . Yii::app()->params->servers['dr_web_hn']
                            . '/' . $rating['provider_friendly_url'] . '/review/' . $rating['friendly_url'];
                    } else {
                        // link to the generic practice profile
                        $arrProviderRating[$key]['reference_url'] = '//' . Yii::app()->params->servers['dr_web_hn']
                            . $rating['practice_friendly_url'];
                    }
                }
            } else {
                $arrProviderRating[$key]['reference_url'] = '';
            }
        }

        return $arrProviderRating;
    }

    /**
     * PADM: Count an account's total reviews
     * @param int $accountId
     * @param bool $externalSources
     * @return string
     */
    public static function countAccountReviews($accountId, $externalSources = false)
    {
        if (empty($accountId)) {
            // we won't be able to get the account info
            return false;
        }

        // by default exclude yext and gmb
        $condition = sprintf("(yext_review_id IS NULL AND partner_site_id != 60)");
        if ($externalSources) {
            // include yext and GMB
            $condition = sprintf("(yext_review_id IS NOT NULL OR partner_site_id = 60)");
        }

        $sql = sprintf(
            "SELECT COUNT(DISTINCT provider_rating.id)
            FROM provider_rating
            WHERE
                (
                    provider_rating.provider_id
                        IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                    OR provider_rating.practice_id
                        IN (SELECT practice_id FROM account_practice WHERE account_id = %d)
                )
                AND provider_rating.status IN ('A', 'P')
                AND %s;",
            $accountId,
            $accountId,
            $condition
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
    }

    /**
     * PADM: Get an account's last review date
     * @param int $accountId
     * @return string
     */
    public static function getAccountLastReview($accountId = 0)
    {
        $cacheTime = Yii::app()->params['cache_short'];

        // Asociated practices to account
        $sql = sprintf("SELECT practice_id FROM account_practice WHERE account_id = %d", $accountId);
        $practices = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryColumn();

        // Asociated providers to account
        $sql = sprintf("SELECT provider_id FROM account_provider WHERE account_id = %d", $accountId);
        $providers = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryColumn();

        $condition = '';
        $or = '';
        if (!empty($practices)) {
            $condition .= sprintf("practice_id IN (%s) ", implode(', ', $practices));
            $or = 'OR';
        }
        if (!empty($providers)) {
            $condition .= sprintf("%s provider_id IN (%s) ", $or, implode(', ', $providers));
        }
        if (!empty($condition)) {
            $condition = sprintf("AND (%s) ", $condition);
        }

        $sql = sprintf(
            "SELECT DATE_FORMAT(MAX(provider_rating.date_added), '%%m/%%d/%%Y') AS last_review
            FROM provider_rating
            WHERE provider_rating.status IN ('A' ,'P')
            %s",
            $condition
        );

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryScalar();
    }

    /**
     * PADM: Get an account's average rating
     * @param int $accountId
     * @param bool $externalSources
     * @param bool $ceiling
     * @return string
     */
    public static function getAccountAverageRating($accountId = 0, $externalSources = false, $ceiling = true)
    {

        if (empty($accountId)) {
            // we won't be able to get the account info
            return false;
        }

        // by default exclude yext and gmb
        $condition = sprintf("(yext_review_id IS NULL AND partner_site_id != 60)");
        if ($externalSources) {
            // include yext and GMB
            $condition = sprintf("(yext_review_id IS NOT NULL OR partner_site_id = 60)");
        }

        $sql = sprintf(
            "SELECT %s(AVG(provider_rating.rating)) AS average_rating
            FROM provider_rating
            WHERE
                (
                provider_rating.provider_id
                    IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                OR provider_rating.practice_id
                    IN (SELECT practice_id FROM account_practice WHERE account_id = %d)
                )
                AND provider_rating.status IN ('A', 'P')
                AND %s;",
            ($ceiling ? 'CEILING' : ''),
            $accountId,
            $accountId,
            $condition
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get an account's first rating date
     * @param int $accountId
     * @return string
     */
    public static function getAccountFirstRating($accountId = null)
    {
        if (empty($accountId)) {
            // we won't be able to get the account info
            return false;
        }

        $sql = sprintf(
            "SELECT MIN(provider_rating.date_added)
            FROM provider_rating
            LEFT JOIN account_provider
                ON provider_rating.provider_id = account_provider.provider_id
            LEFT JOIN account_practice
                ON provider_rating.practice_id = account_practice.practice_id
            WHERE (account_provider.account_id = %d OR account_practice.account_id = %d)
                AND (provider_rating.status = 'A' OR provider_rating.status = 'P');",
            $accountId,
            $accountId
        );
        $firstRating = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();

        if (!empty($firstRating)) {
            $firstRating = date("Y-m", strtotime($firstRating)) . '-01';
        }

        return $firstRating;
    }

    /**
     * Get the first time a review was sent through a ReviewHub for a given account
     * @param int $accountId
     * @return string
     */
    public static function getAccountFirstRHRating($accountId)
    {

        $sql = sprintf(
            "SELECT SQL_NO_CACHE MIN(date_added)
            FROM provider_rating
            INNER JOIN account_device
                ON provider_rating.account_device_id = account_device.id
            WHERE provider_rating.account_id = %d
                AND provider_rating.status = 'A'
                AND account_device.status = 'A';",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * PADM: Gets an account's provider rating history to be displayed as a graph
     * @param int $accountId
     * @param string $dateField
     * @param string $additionalFields
     * @param string $groupBy
     * @param array $accountProviders
     * @param array $accountPractices
     * @return array
     */
    public static function getAccountProviderRatingHistory(
        $accountId = 0,
        $dateField = '',
        $additionalFields = '',
        $groupBy = '',
        $accountProviders = array(),
        $accountPractices = array()
    ) {

        if (empty($additionalFields) || $accountId == 0) {
            return false;
        }

        // get possible providers
        $listProviders = [];
        if (!empty($accountProviders)) {
            foreach ($accountProviders as $accountProvider) {
                $listProviders[] = $accountProvider['provider_id'];
            }
        }
        if (is_array($listProviders) && !empty($listProviders)) {
            $listProviders = implode(',', $listProviders);
        }

        // get possible practices
        $listPractices = [];
        if (!empty($accountPractices)) {
            foreach ($accountPractices as $accountPractice) {
                $listPractices[] = $accountPractice['practice_id'];
            }
        }
        if (is_array($listPractices) && !empty($listPractices)) {
            $listPractices = implode(',', $listPractices);
        }

        // build condition
        $ppCondition = '';
        if (!empty($listProviders) && !empty($listPractices)) {
            $ppCondition = sprintf(
                'provider_rating.provider_id IN (%s) OR provider_rating.practice_id IN (%s)',
                $listProviders,
                $listPractices
            );
        } elseif (!empty($listProviders)) {
            $ppCondition = sprintf('provider_rating.provider_id IN (%s)', $listProviders);
        } elseif (!empty($listPractices)) {
            $ppCondition = sprintf('provider_rating.practice_id IN (%s)', $listPractices);
        }

        if (empty($ppCondition)) {
            return false;
        }

        // run query
        $sql = sprintf(
            "SELECT SQL_NO_CACHE %s AS date_added,
            %s
            FROM provider_rating
            LEFT JOIN provider
                ON provider_rating.provider_id = provider.id
            LEFT JOIN credential
                ON provider.credential_id = credential.id
            WHERE (%s)
                AND (provider_rating.status = 'A' OR provider_rating.status = 'P')
            GROUP BY %s
            ORDER BY provider_rating.date_added;",
            $dateField,
            $additionalFields,
            $ppCondition,
            $groupBy
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * PADM: Gets an account's provider rating history for a given month
     * @param int $accountId
     * @param string $yearMonth
     * @param string $filterSource
     * @return array
     */
    public static function getAccountProviderRatingMonthly($accountId, $yearMonth, $filterSource = '')
    {

        if (empty($accountId)) {
            return false;
        }

        list($year, $month) = explode('-', $yearMonth);
        $sql = sprintf(
            "SELECT SQL_NO_CACHE COUNT(DISTINCT provider_rating.id) AS q
            FROM provider_rating
            WHERE (
                    provider_id IN (SELECT provider_id FROM account_provider WHERE account_id = %d)
                    OR
                    practice_id IN (SELECT practice_id FROM account_practice WHERE account_id = %d)
                )
                %s
                AND YEAR(date_added) = %d
                AND MONTH(date_added) = %d
                AND (provider_rating.status = 'A' OR provider_rating.status = 'P')
            LIMIT 1;",
            $accountId,
            $accountId,
            $filterSource,
            $year,
            $month
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Checks a record's ownership
     * @param array $accountInfo
     * @param array $reviewId
     * @return string
     */
    public static function checkPermissions($accountInfo, $reviewId)
    {
        if ((is_array($accountInfo) && $accountInfo['privileges']['reviews'] == 0) || $reviewId == 0) {
            $results['success'] = false;
            $results['http_response_code'] = 403;
            $results['error'] = "ACCESS_DENIED";
            $results['data'] = "You are not allowed to access.";
            return $results;
        }

        $recReview = ProviderRating::model()->cache(Yii::app()->params['cache_short'])->findByPk($reviewId);

        // the user can access to this review?
        $canAccess = false;
        if (!empty($recReview->provider_id) && !empty($accountInfo['providers'][$recReview->provider_id])) {
            $canAccess = true;
        } elseif (!empty($recReview->practice_id) && !empty($accountInfo['practices'][$recReview->practice_id])) {
            $canAccess = true;
        }

        if (!$canAccess) {
            $results['success'] = false;
            $results['http_response_code'] = 403;
            $results['error'] = "ACCESS_DENIED";
            $results['data'] = "You are not allowed to access.";
            return $results;
        }

        $results['success'] = true;
        $results['data'] = (array) $recReview->attributes;
        return $results;
    }

    /**
     * Gets reviews updated in the last 7 days for NON-CLIENTS ONLY
     * @return array
     */
    public static function getNewReviewsNonclients()
    {

        $sql = sprintf(
            "SELECT provider_rating.provider_id AS id
            FROM provider_rating
            LEFT JOIN account_provider
                ON provider_rating.provider_id = account_provider.provider_id
            LEFT JOIN organization_account
                ON account_provider.account_id = organization_account.account_id
                AND `connection` = 'O'
            LEFT JOIN organization
                ON organization_account.organization_id = organization.id
                AND organization.status = 'A'
            WHERE provider_rating.status = 'A'
                AND (provider_rating.date_approved BETWEEN DATE_SUB(CURDATE(), INTERVAL 7 DAY) AND CURDATE())
                AND organization.id IS NULL
            GROUP BY provider_rating.provider_id;"
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Gets ReviewHub reviews updated in the last 7/30 days for active organizations
     * @param string $interval
     * @param int $organizationId to get only provider rating ids as a string
     * @return mixed
     */
    public static function getNewReviewHubReviews($interval, $organizationId = null)
    {

        if ($interval == 'week') {
            // 0=Do not send, 1=Weekly, 2=Monthly
            $interval = 7;
            $sendRhSummary = '(account_details.send_rh_summary = 1 OR account_details.id IS NULL)';
        } else {
            $interval = 30;
            $sendRhSummary = '(account_details.send_rh_summary = 2)';
        }

        $sqlFrom = sprintf(
            "FROM organization
            INNER JOIN organization_account
                ON organization_account.organization_id = organization.id
                AND `connection` = 'O'
            INNER JOIN provider_rating
                ON provider_rating.account_id = organization_account.account_id
                AND provider_rating.status = 'A'
                AND (DATE(provider_rating.date_approved) BETWEEN DATE_SUB(CURDATE(), INTERVAL %d DAY) AND CURDATE())
                AND provider_rating.device_id > 0
            INNER JOIN account_device
                ON provider_rating.account_device_id = account_device.id
                AND account_device.send_email_review_to_account = 1
            LEFT JOIN account_details
                ON organization_account.account_id = account_details.account_id
            LEFT JOIN provider_rating AS pending_rating
                ON pending_rating.account_id = organization_account.account_id
                AND pending_rating.status = 'P'
                AND (DATE(pending_rating.date_added) BETWEEN DATE_SUB(CURDATE(), INTERVAL %d DAY) AND CURDATE())
                AND pending_rating.device_id > 0
            LEFT JOIN account_device AS pending_device
                ON pending_rating.account_device_id = pending_device.id
                AND pending_device.send_email_review_to_account = 1",
            $interval,
            $interval
        );

        if ($organizationId) {
            $sql = sprintf(
                "SELECT DISTINCT(provider_rating.id) AS provider_rating_id
                %s
                WHERE organization.status = 'A'
                    AND %s
                    AND organization.id = %d
                LIMIT 100;",
                $sqlFrom,
                $sendRhSummary,
                $organizationId
            );

            return join(',', Yii::app()->db->createCommand($sql)->queryColumn());
        } else {
            $sql = sprintf(
                "SELECT organization.id,
                COUNT(DISTINCT(pending_rating.id)) AS pending_reviews
                %s
                WHERE organization.status = 'A'
                    AND %s
                GROUP BY organization.id;",
                $sqlFrom,
                $sendRhSummary
            );

            return Yii::app()->db->createCommand($sql)->queryAll();
        }
    }

    /**
     * Gets active organizations with no ReviewHub reviews updated in the last 7/30 days
     * @param string $interval
     * @return array
     */
    public static function getNoReviewHubReviews($interval)
    {

        if ($interval == 'biweek') {
            /**
             * If the process runs on 28th, the variable $$days is 14
             * If the process runs on 14th, the variable $$days depends on the number of days on the previous month.
             * @important This shouldn't run at any other date
             * If the number of days in the previous month is 28-> $$days= 14
             * If the number of days in the previous month is 29-> $$days= 15
             * If the number of days in the previous month is 30-> $$days= 16
             * If the number of days in the previous month is 31-> $$days= 17
             */
            if (date('d') == 28) {
                $interval = 14;
            } else {
                $lastMonth = strtotime('last month');

                $lastMonthYear = date('Y', $lastMonth);
                $lastMonth = date('m', $lastMonth);

                $daysLastMonth = cal_days_in_month(CAL_GREGORIAN, $lastMonth, $lastMonthYear); // 31

                if ($daysLastMonth == 28) {
                    $interval = 14;
                } elseif ($daysLastMonth == 29) {
                    $interval = 15;
                } elseif ($daysLastMonth == 30) {
                    $interval = 16;
                } else {
                    $interval = 17;
                }
            }

            // 0=Do not send, 1=Weekly, 2=Monthly
            $sendRhSummary = '(account_details.send_rh_summary = 1 OR account_details.id IS NULL)';
        } else {
            $interval = 30;
            $sendRhSummary = '(account_details.send_rh_summary = 2)';
        }

        $sql = sprintf(
            "SELECT DISTINCT organization.id, organization.organization_name, organization.salesforce_id
            FROM organization
            INNER JOIN organization_account
                ON organization_account.organization_id = organization.id
            INNER JOIN account_device
                ON account_device.account_id = organization_account.account_id
            LEFT JOIN provider_rating
                ON (provider_rating.account_id = organization_account.account_id
                    AND account_device.id= provider_rating.account_device_id
                    AND provider_rating.status IN ('A', 'P')
                    AND provider_rating.device_id > 0
                    AND (provider_rating.date_added BETWEEN DATE_SUB(CURDATE(), INTERVAL %d DAY) AND CURDATE())
                )
            LEFT JOIN account_details
                ON organization_account.account_id = account_details.account_id
            WHERE organization.status = 'A'
                AND `connection` = 'O'
                AND account_device.send_email_review_to_account = 1
                AND %s
                AND provider_rating.id IS NULL;",
            $interval,
            $sendRhSummary
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Check if a Yext review already exists on our database. If it doesn't, create it
     * @param int $practiceId
     * @param array $yextReview
     * @return bool whether we already had this review in our database
     * @example ProviderRating::yextCreate(1, array(
     * 'reviewDate' => '2015-07-14T10:23:21.000-04:00',
     * 'locationId' => '3730781',
     * 'authorName' => 'Alan B.',
     * 'rating' => 5,
     * 'siteId' => 'YELP',
     * 'id' => 251930700,
     * 'content' => 'Greatest ENT/surgeon ever. Period. Went to Dr. Kohan with a cholesteatoma which caused me to be
     * practically deaf on one side . Thanks to him, I can finally...',
     * 'url' => 'http://www.yelp.com/biz/darius-kohan-md-new-york',
     * ));
     */
    public static function yextCreate($practiceId, $yextReview)
    {

        // check if we have basic data
        if (
            empty($yextReview['locationId'])
            || empty($yextReview['authorName'])
            || empty($yextReview['rating'])
        ) {
            // we don't have this location in our database or there's no author or overall rating, leave
            return true;
        }

        // check if we have basic content
        if (empty($yextReview['title']) && empty($yextReview['content'])) {
            // no content, leave
            return true;
        }

        // prepare some of the data
        // get title (if it exists) from yext and description, default to content
        $reviewTitle = substr(
            (!empty($yextReview['title'])  ? $yextReview['title'] : substr(
                $yextReview['content'],
                0,
                (strpos($yextReview['content'], "\n") !== false ? strpos($yextReview['content'], "\n") : 255)
            )),
            0,
            255
        );
        // get full author name from yext
        $reviewerName = substr($yextReview['authorName'], 0, 50);
        // get rating
        $reviewRating = (!empty($yextReview['rating']) ? round($yextReview['rating']) : null);

        // try to find the rating in our own db (using yext's review_id)
        $providerRating = ProviderRating::model()->find(
            'practice_id=:practiceId AND yext_review_id=:yextReviewId',
            array(
                ':practiceId' => $practiceId,
                ':yextReviewId' => $yextReview['id']
            )
        );
        // do we already have this review?
        if ($providerRating) {
            // yes, leave
            return false;
        }

        // try to find the rating again, this time by:
        // provider_id, practice_id, reviewer_name, reviewer_email, title, description, rating
        $providerRating = ProviderRating::model()->find(
            'provider_id IS NULL
            AND practice_id = :practiceId
            AND reviewer_name = :reviewerName
            AND title = :reviewTitle
            AND rating = :rating',
            array(
                ':practiceId' => $practiceId,
                ':reviewerName' => $reviewerName,
                ':reviewTitle' => $reviewTitle,
                ':rating' => $yextReview['id']
            )
        );
        // do we already have this review?
        if ($providerRating) {
            // yes, leave
            return false;
        }

        // is this a gmb review?
        if ($yextReview['siteId'] == 'GOOGLEPLACES' || $yextReview['siteId'] == 'GOOGLEMYBUSINESS') {
            // yes: check if there's an active GMB association for this practice
            $gmb = ProviderPracticeGmb::model()->cache(Yii::app()->params['cache_short'])->find(
                'practice_id = :practiceId',
                array(':practiceId' => $practiceId)
            );
            if (!empty($gmb)) {
                // don't import the review (since we'd rather trust Google than Yext)
                echo 'Skipping Google review since GMB integration is active for practice #' . $practiceId . PHP_EOL;
                return true;
            }
        }

        // everything went well: create the actual provider_rating entry with the data that came from yext
        $providerRating = new ProviderRating;
        $providerRating->partner_id = 1; // use doctor.com
        $providerRating->partner_site_id = 1; // use doctor.com
        $providerRating->practice_id = $practiceId; // the practice we looked up
        $providerRating->reviewer_name = $reviewerName;
        $providerRating->rating = $reviewRating;
        $providerRating->title = $reviewTitle;
        $providerRating->description = (!empty($yextReview['content']) ? $yextReview['content'] : '-');
        $providerRating->status = 'A'; // mark as approved
        $providerRating->origin = 'P'; // mark as coming from a partner site
        $providerRating->reference_url = $yextReview['url']; // get url where the full review lives
        $providerRating->yext_site_id = $yextReview['siteId']; // get yext's site id (FACEBOOK, GOOGLEPLACES, YELP, etc)
        $providerRating->yext_review_id = $yextReview['id']; // get yext's own review id to later match when importing
        $providerRating->date_added = date('Y-m-d H:i:s', strtotime($yextReview['reviewDate']));
        $providerRating->date_updated = date('Y-m-d H:i:s', strtotime($yextReview['reviewDate']));
        $providerRating->date_approved = date('Y-m-d H:i:s', strtotime($yextReview['reviewDate']));
        $providerRating->reviewer_email = '';

        if (!$providerRating->save()) {
            // couldn't save the rating, log the error
            echo 'Error while saving Yext\'s rating: ' . json_encode($providerRating->getErrors()) . PHP_EOL;
            Yii::log(
                'Error while saving Yext\'s rating: ' . json_encode($providerRating->getErrors()),
                'error',
                'application.provider_rating.yext'
            );
        }
        return true;
    }

    /**
     * Gets a practice's average rating (unlinke getPracticeRating there's no rounding here)
     * @param int $practiceId
     * @return float
     */
    public static function getPracticeRatingAverage($practiceId)
    {
        $sql = sprintf(
            "SELECT AVG(rating) AS average
            FROM provider_rating
            WHERE practice_id = %d
                AND origin = 'D'
                AND status = 'A';",
            $practiceId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get top 50 accounts per number of reviews coming from RHs in last 7 days
     * @return string
     */
    public static function getWeeklyRockstarCustomersReportRH()
    {
        return sprintf(
            "SELECT account.id AS account_id, CONCAT(account.first_name, ' ', account.last_name) AS account_name,
            account.organization_name AS account_organization_name,
            practice.name AS practice_name,
            CONCAT('https://kiosk.doctor.com?sid=', SHA2(CONCAT('%s', device.id), 512)) AS
            reviewhub_link, COUNT(1) AS reviews
            FROM provider_rating
            INNER JOIN account_device
                ON provider_rating.account_device_id = account_device.id
            INNER JOIN account
                ON account_device.account_id = account.id
            INNER JOIN practice
                ON account_device.practice_id = practice.id
            INNER JOIN device
                ON account_device.device_id = device.id
            WHERE account_device_id > 0
                AND account_device.status IN ('P', 'T', 'A')
                AND device.status = 'U'
                AND DATEDIFF(CURDATE(), provider_rating.date_added) BETWEEN 1 AND 7
                AND origin = 'D'
                AND provider_rating.status = 'A'
            GROUP BY account_device_id
            ORDER BY reviews DESC
            LIMIT 50;",
            'Doctor.com ReviewHub-' . Yii::app()->params['rh_secret'] . '-'
        );
    }

    /**
     * Get top 50 accounts per number of reviews (coming from RHs or not) in last 7 days
     * @return string
     */
    public static function getWeeklyRockstarCustomersReport()
    {
        return sprintf(
            "SELECT account.id AS account_id,
            CONCAT(account.first_name, ' ', account.last_name) AS account_name,
            account.organization_name AS account_organization_name, practice.name AS practice_name,
            GROUP_CONCAT(DISTINCT CONCAT('https://kiosk.doctor.com?sid=', SHA2(CONCAT('%s', device.id), 512))
            SEPARATOR ', ') AS reviewhub_links,
            COUNT(DISTINCT provider_rating.id) AS reviews
            FROM provider_rating
            LEFT JOIN account_provider
                ON (account_provider.provider_id = provider_rating.provider_id)
            LEFT JOIN account_practice
                ON (account_practice.practice_id = provider_rating.practice_id)
            LEFT JOIN account
                ON (provider_rating.account_id = account.id
                    OR account.id = account_provider.account_id
                    OR account.id = account_practice.account_id
                    )
            LEFT JOIN account_device
                ON (account.id = account_device.account_id
                    AND account_device.status IN ('P', 'T', 'A')
                    )
            LEFT JOIN practice
                ON account_device.practice_id = practice.id
            LEFT JOIN device
                ON (account_device.device_id = device.id
                    AND device.status = 'U'
                    )
            WHERE DATEDIFF(CURDATE(), provider_rating.date_added) BETWEEN 1 AND 7
                AND origin = 'D'
                AND provider_rating.status = 'A'
            GROUP BY account.id
            ORDER BY  reviews DESC
            LIMIT 50;",
            'Doctor.com ReviewHub-' . Yii::app()->params['rh_secret'] . '-'
        );
    }

    /**
     * Get average rating of a given set of reviews
     * @param string $providerRatingIds
     * @return int
     */
    public static function getNewReviewHubReviewsAverage($providerRatingIds)
    {
        $sql = sprintf(
            "SELECT ROUND(AVG(rating), 1) AS average_rating
            FROM provider_rating
            WHERE id IN (%s);",
            $providerRatingIds
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Get average rating and best performing practice of a given set of reviews
     * @param int $accountId
     * @param string $providerRatingIds
     * @return int
     */
    public static function getNewReviewHubReviewsTopRatedPractice($accountId, $providerRatingIds)
    {
        $practiceCount = AccountPractice::model()->count('account_id=:accountId', array(':accountId' => $accountId));

        if ($practiceCount == 1) {
            $sql = sprintf(
                "SELECT practice.name AS practice, ROUND('0', 2) AS average_rating
                FROM practice
                INNER JOIN account_practice
                    ON practice.id = account_practice.practice_id
                WHERE account_practice.account_id = %d
                LIMIT 1;",
                $accountId
            );
        } else {
            $sql = sprintf(
                "SELECT practice.name AS practice, ROUND(AVG(rating), 2) AS average_rating
                FROM provider_rating
                INNER JOIN practice
                    ON provider_rating.practice_id = practice.id
                WHERE provider_rating.id IN (%s)
                ORDER BY average_rating DESC
                LIMIT 1;",
                $providerRatingIds
            );
        }
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get average rating and best performing provider of a given set of reviews
     * @param int $accountId
     * @param string $providerRatingIds
     * @return int
     */
    public static function getNewReviewHubReviewsTopRatedProvider($accountId, $providerRatingIds)
    {
        $providerCount = AccountProvider::model()->count('account_id=:accountId', array(':accountId' => $accountId));

        if ($providerCount == 1) {
            $sql = sprintf(
                "SELECT CONCAT(provider.prefix, ' ', provider.first_last) AS provider, ROUND('0', 2) AS average_rating
                FROM provider
                INNER JOIN account_provider
                    ON provider.id = account_provider.provider_id
                WHERE account_provider.account_id = %d
                LIMIT 1;",
                $accountId
            );
        } else {
            $sql = sprintf(
                "SELECT CONCAT(provider.prefix, ' ', provider.first_last) AS provider,
                ROUND(AVG(rating), 2) AS average_rating
                FROM provider_rating
                INNER JOIN provider
                    ON provider_rating.provider_id = provider.id
                WHERE provider_rating.id IN (%s)
                ORDER BY average_rating DESC
                LIMIT 1;",
                $providerRatingIds
            );
        }
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get number of reviews an organization has, to update SalesForce:
     * DDC_Total_Reviews__c
     * DDC_Review_Average__c
     * DDC_ReviewHub_Reviews__c
     * DDC_ReviewHub_Review_Average__c
     * @param int $organizationId
     * @param bool $reviewHubOnly
     * @return array
     */
    public static function updateSFOrganizationReviews($organizationId, $reviewHubOnly = false)
    {

        $organization = Organization::model()->findByPk($organizationId);
        if (!$organization || empty($organization->salesforce_id)) {
            return false;
        }

        // get number of reviews and average
        $reviews = self::getOrganizationReviews($organizationId, $reviewHubOnly);
        $submittedReviews = self::getOrganizationReviews($organizationId, $reviewHubOnly, true);

        if ($reviewHubOnly) {
            // update rh fields and leave
            return Yii::app()->salesforce->updateAccountField(
                $organization->salesforce_id,
                'DDC_ReviewHub_Reviews__c',
                $reviews['review_count']
            ) && Yii::app()->salesforce->updateAccountField(
                $organization->salesforce_id,
                'DDC_ReviewHub_Review_Average__c',
                floatval($reviews['review_avg'])
            ) && Yii::app()->salesforce->updateAccountField(
                $organization->salesforce_id,
                'ReviewHub_Reviews_Submitted__c',
                floatval($submittedReviews['review_count'])
            );
        }

        // get last review date
        $lastReview = self::getOrganizationLastReviewDate($organizationId);
        if (!empty($lastReview)) {
            $sfData = array(
                'salesforce_id' => $organization->salesforce_id,
                'field' => 'Last_Review_Submitted__c',
                'value' => date('c', strtotime($lastReview))
            );
            SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);
        }

        // update other network reviews
        $otherReviews = self::getOrganizationOtherReviews($organizationId);
        $sfData = array(
            'salesforce_id' => $organization->salesforce_id,
            'field' => 'Other_Network_Reviews__c',
            'value' => $otherReviews
        );
        SqsComponent::sendMessage('salesforceUpdateAccountField', $sfData);

        // update generic fields
        return Yii::app()->salesforce->updateAccountField(
            $organization->salesforce_id,
            'DDC_Total_Reviews__c',
            $reviews['review_count']
        ) && Yii::app()->salesforce->updateAccountField(
            $organization->salesforce_id,
            'DDC_Review_Average__c',
            floatval($reviews['review_avg'])
        );
    }

    /**
     * Get the number of reviews and the average an organization has
     * @param int $organizationId
     * @param bool $reviewHubOnly
     * @param bool $allSubmitted
     * @return array
     */
    public static function getOrganizationReviews($organizationId, $reviewHubOnly = false, $allSubmitted = false)
    {

        $statusCondition = "AND provider_rating.status = 'A'";
        if ($allSubmitted) {
            $statusCondition = '';
        }

        $condition = '';
        if ($reviewHubOnly) {
            // in this case we'll only update the reviewhub review count and avg and leave
            $condition = ' AND provider_rating.account_device_id > 0 ';
        }

        $sql = sprintf(
            "SELECT COUNT(1) AS review_count, AVG(provider_rating.rating) AS review_avg
            FROM organization
            INNER JOIN organization_account
                ON organization.id = organization_account.organization_id
                AND `connection` = 'O'
            INNER JOIN account_provider
                ON organization_account.account_id = account_provider.account_id
            INNER JOIN provider_rating
                ON account_provider.provider_id = provider_rating.provider_id
                AND provider_rating.origin != 'E'
            %s
            %s
            WHERE organization.id = %d;",
            $statusCondition,
            $condition,
            $organizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();
    }

    /**
     * Get an organization's last review date
     * @param int $organizationId
     * @return string
     */
    public static function getOrganizationLastReviewDate($organizationId)
    {
        // update last review date: get organization's last review date
        $sql = sprintf(
            "SELECT MAX(provider_rating.date_added)
            FROM organization
            INNER JOIN organization_account
                ON organization.id = organization_account.organization_id
                AND `connection` = 'O'
            INNER JOIN account_provider
                ON organization_account.account_id = account_provider.account_id
            INNER JOIN provider_rating
                ON account_provider.provider_id = provider_rating.provider_id
                AND provider_rating.status = 'A' AND provider_rating.origin != 'E'
            WHERE organization.id = %d;",
            $organizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
    }

    /**
     * Count an organization's other reviews
     * @param int $organizationId
     * @return int
     */
    public static function getOrganizationOtherReviews($organizationId)
    {
        $sql = sprintf(
            "SELECT COUNT(1) AS review_count
            FROM organization
            INNER JOIN organization_account
                ON organization.id = organization_account.organization_id
                AND `connection` = 'O'
            INNER JOIN account_practice
                ON organization_account.account_id = account_practice.account_id
            INNER JOIN provider_rating
                ON account_practice.practice_id = provider_rating.practice_id
                AND provider_rating.status = 'A'
                AND provider_rating.origin NOT IN ('D', 'E')
            WHERE organization.id = %d;",
            $organizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get the IDs of organizations whose number of reviews in the last 30 days has changed
     * This is in order to update SalesForce field DDC_ReviewHub_Reviews_Last_30_Days__c
     * @see ReviewsCommand::actionUpdateSalesForce
     * @return array
     */
    public static function getOrganizationsReviewsForSalesForce()
    {

        $sql = sprintf(
            "SELECT
            SUM(IF(DATE(provider_rating.date_added) BETWEEN DATE_SUB('%s', INTERVAL 30 day) AND '%s','1','0')) AS
            review_count,
            organization_id
            FROM organization
            INNER JOIN organization_account
                ON organization.id = organization_account.organization_id
                AND `connection` = 'O'
            INNER JOIN account_provider
                ON organization_account.account_id = account_provider.account_id
            INNER JOIN provider_rating
                ON account_provider.provider_id = provider_rating.provider_id
            WHERE provider_rating.status = 'A'
                AND provider_rating.account_device_id > 0
                AND (
                    DATE(provider_rating.date_added) BETWEEN DATE_SUB('%s', INTERVAL 30 day) AND '%s'
                    OR (
                        DATE(provider_rating.date_added) = '%s'
                        OR
                        DATE(provider_rating.date_added) = DATE_SUB('%s', INTERVAL 31 DAY)
                    )
                )
            GROUP BY organization_id;",
            date('Y-m-d'),
            date('Y-m-d'),
            date('Y-m-d'),
            date('Y-m-d'),
            date('Y-m-d'),
            date('Y-m-d')
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Return average rating of all providers based on criteria
     * @param array $criteria
     * @return float
     */
    private function getAverageRating($criteria)
    {
        return DoctorIndex::calculateAverage($criteria, 'avg_rating');
    }

    /**
     * Process and send SMS/Mail alert when saving new provider rating
     * alertType = alert_review
     * @param int $providerRatingId
     * @return bool
     */
    public static function sendAlertReview($providerRatingId = 0)
    {
        if ($providerRatingId == 0) {
            return false;
        }

        // find review
        $sql = sprintf(
            "SELECT pr.id, pr.provider_id, pr.practice_id,
            rating, pro.first_last AS provider_name, pra.name AS practice_name,
                pra.address_full AS practice_address, pra.phone_number AS practice_phone
            FROM provider_rating AS pr
            LEFT JOIN practice AS pra
                ON pr.practice_id = pra.id
            LEFT JOIN provider AS pro
                ON pr.provider_id = pro.id
            WHERE pr.id = %d
            LIMIT 1;",
            $providerRatingId
        );
        $pr = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryRow();

        if (empty($pr['practice_id']) || empty($pr['provider_id'])) {
            return false;
        }

        $alertsTargetSMS = AccountContactSetting::model()->getAlertsTarget(
            'SMS',
            'alert_review',
            $pr['practice_id'],
            $pr['provider_id'],
            0
        );

        if (!empty($alertsTargetSMS)) {
            // send alerts...
            foreach ($alertsTargetSMS as $alert) {
                $to = $alert['mobile_phone'];
                $body = $pr['provider_name'] . ' just received a ' . $pr['rating'] .
                    ' star review on the Doctor.com network. Login to read the full text https://goo.gl/td7udu';
                $locationId = $alert['location_id'];
                SmsComponent::sendSms($to, $body, false, $locationId);
            }
        }

        return true;
    }

    /**
     * Share a review (not coming from the ReviewHub) on Yelp, called via SQS
     * If link is not found in provider_practice_listing look it up using the Scan Tool and save it to the DB
     * If provider or practice is a client, use the ReviewHub template, otherwise use a standard template
     *
     * @param int $providerRatingId
     * @return boolean
     */
    public static function shareOnYelp($providerRatingId)
    {
        $providerRating = ProviderRating::model()->findByPk($providerRatingId);
        if (!$providerRating) {
            // not found, just leave
            return false;
        }

        $practiceId = $providerRating->practice_id;
        $providerId = $providerRating->provider_id;

        if (empty($providerId)) {
            // we can't do anything without a provider, just leave
            return false;
        }

        $providerPracticeListing = ProviderPracticeListing::findLinkYelp($providerId, $practiceId);

        // check if the provider or practice belong to an organization
        $arrOrganizations = Provider::belongsToOrganization($providerId);
        if (empty($arrOrganizations)) {
            $arrOrganizations = Practice::belongsToOrganization($practiceId);
        }

        if (empty($providerPracticeListing)) {
            // neither found, use the scan tool to look it up
            // look up the provider's primary practice
            $providerPractice = ProviderPractice::model()->find(
                'provider_id=:providerId AND primary_location = 1',
                array(':providerId' => $providerId)
            );

            if (empty($practiceId) && empty($providerPractice)) {
                // no primary practice found and we had no practice, just, leave
                return false;
            } elseif (!empty($providerPractice)) {
                // found, use it
                $practiceId = $providerPractice->practice_id;
            }

            // first check if we have it this way
            $providerPracticeListing = ProviderPracticeListing::model()->find(
                'listing_type_id=:listingTypeId AND practice_id=:practiceId AND provider_id=:providerId',
                array(
                    ':listingTypeId' => ListingType::YELP_PROFILE_ID,
                    ':practiceId' => $practiceId,
                    ':providerId' => $providerId
                )
            );

            if ($providerPracticeListing) {
                // use the link we found in the DB
                $yelpLink = $providerPracticeListing->url;
            } else {
                // look up the practice
                $practice = Practice::model()->findByPk($practiceId);

                if ($practice->country_id != 1) {
                    // we can't process practices outside the US
                    return false;
                }

                $providerPracticeListing = ProviderPracticeListing::findLinkYelp($providerId, $practiceId);

                if ($providerPracticeListing) {
                    // use the link we found in the DB
                    $yelpLink = $providerPracticeListing->url;
                }
            }
        } else {
            // use the link we found in the DB
            $yelpLink = $providerPracticeListing->url;
        }

        if (empty($yelpLink)) {
            // we couldn't find a link to link to
            return false;
        }

        if (empty($arrOrganizations)) {
            // send standard review email
            return MailComponent::reviewApproved($providerRatingId, $yelpLink);
        } else {
            // send reviewhub-like email
            $patient = Patient::model()->findByPk($providerRating->patient_id);
            if (!$patient) {
                // nothing to do, leave
                return false;
            }

            $patientArray = $patient->getAttributes();
            $htmlLink = '<a href="' . $yelpLink .
                '"><img src="http://assets.doctor.com/email/u_social_links_yelp.png" /></a>';
            $patientArray['social_links'] = array(
                $yelpLink,
                $htmlLink,
                $yelpLink,
                'Yelp'
            );
            $patientArray['review_description'] = $providerRating->description;
            $patientArray['password'] = false;
            return MailComponent::reviewSubmitted($patientArray, $providerId, $practiceId, null);
        }
    }

    /**
     * Prepare email to send to Primary Account Email, when there's an error trying to auto publish on facebook
     * @param array $queueBody
     *   Eg:
     *       [accountId] => 23106
     *       [providerRatingId] => 161964
     *       [providerId] => 3700750
     *       [facebookPageName] => Actualidad y Tecnologia
     *       [facebookPageId] => 104667306275844
     *       [errorDescription] => Error validating access token: The session has been invalidated because the user has
     *       changed the password.
     */
    public static function alertAutoPublishSocialEnhanceFail($queueBody = array())
    {

        $accountId = $queueBody['accountId'];

        // find related organization
        $organizationId = Account::belongsToOrganization($accountId);
        $recOrganization = Organization::model()->find(
            "id=:organizationId",
            array(":organizationId" => $organizationId)
        );

        // find SF details
        $salesForceDetails = Account::getSalesforceDetails($recOrganization['salesforce_id']);

        $providerName = '';
        if (!empty($queueBody['providerId'])) {
            $recProvider = Provider::getByIdRO($queueBody['providerId']);
            $providerName = $recProvider['first_middle_last'];
        }

        $sfEmail = isset($salesForceDetails['account_rep_email']) ? $salesForceDetails['account_rep_email'] : '';
        $account = Account::getByIdRO($accountId);
        $accountEmail = $account['email'];

        $queueBody['accountEmail'] = $accountEmail;
        $queueBody['sfEmail'] = $sfEmail;
        $queueBody['providerName'] = $providerName;

        if (isset($queueBody['isTest']) && $queueBody['isTest']) {
            // only when coming from phpUnit Test
            return $queueBody;
        }
        MailComponent::alertAutoPublishSocialEnhanceFail($queueBody);
    }

    /**
     * Send a patient messaging to Patient
     * @param array $data
     * @return array
     */
    public function sendPatientMessaging()
    {
        // default return values
        $results['success'] = true;
        $results['error'] = '';
        $results['data'] = '';

        $objPatient = Patient::model()->findByPk($this->patient_id);
        $providerId = $this->provider_id;
        $practiceId = $this->practice_id;
        $reviewDescription = $this->description;

        // If missing patient
        if (empty($objPatient)) {
            return array(
                'success' => false,
                'error' => 'MISSING_PATIENT',
                'data' => 'The patient was not found in our DB'
            );
        }

        $practiceDetails = PracticeDetails::model()->find(
            'practice_id=:practiceId',
            array(':practiceId' => $practiceId)
        );

        // Check if the practice has reviewhub sharing disabled (PADM v1 -> Manage ReviewHub)
        if ($practiceDetails->reviewhub_sharing == 'NONE') {
            return array(
                'success' => false,
                'error' => 'REVIEWHUB_SHARING_DISABLED',
                'data' => 'The practice has disabled ReviewHub Sharing'
            );
        }

        // Re send sms to share review message
        $smsSent = (bool) SmsComponent::sendReviewSubmittedSMS($providerId, $practiceId, $objPatient, $this);

        // Re send mail to share review message
        $mailSent = (bool) MailComponent::sendReviewSubmittedEmail(
            $providerId,
            $practiceId,
            $objPatient,
            $reviewDescription,
            '',
            0,
            $this->id
        );

        if ($mailSent || $smsSent) {
            $practice = Practice::model()->findByPk($practiceId);
            // Create the entry in the audit_log
            AuditLog::create(
                'C',
                'Ratings & Reviews request resent from ' . $practice->name . ' to patient #'
                    . $objPatient->id . ' (' . $objPatient->first_name . ' ' . $objPatient->last_name . ')',
                null,
                null,
                false
            );
        } else {
            $results['success'] = false;
            $results['error'] = 'COULD_NOT_RESEND_REVIEW';
            $results['data'] = 'No email and/or sms was sent';
        }

        return $results;
    }

    /**
     * get full review report for export on organization tool
     * @param int $organizationId
     * @return array
     */
    public static function getOrganizationExport($organizationId)
    {

        $sql = sprintf(
            "SELECT account_id
            FROM organization_account
            WHERE `connection`= 'O'
                AND organization_id = %s",
            $organizationId
        );
        $ownerAccountId = Yii::app()->db->createCommand($sql)->queryScalar();

        if ($ownerAccountId) {
            $sql = sprintf(
                "SELECT DISTINCT
                provider.first_middle_last AS ProviderName,
                practice.name AS PracticeName,
                practice.address_full AS Location,
                IF(account_device_id>0,'ReviewHub','Website') Review_Type,
                IF(origin='D','Doctor.com','Partners') AS Origin,
                DATE_FORMAT(provider_rating.date_added,'%%m/%%d/%%Y') AS DateAdded,
                DATE_FORMAT(provider_rating.date_approved,'%%m/%%d/%%Y') AS DateApproved,
                provider_rating.status AS Status,
                IF(
                    (provider_rating.partner_site_id = 54 OR provider_rating.partner_id = 49 ),
                    '<redacted>',
                    provider_rating.title
                    ) AS Title,
                IF(
                    (provider_rating.partner_site_id = 54 OR provider_rating.partner_id = 49 ),
                    '<redacted>',
                    REPLACE( ( REPLACE(provider_rating.description, '\r\n', '')), '\n', '')
                    ) AS Description,
                provider_rating.reviewer_email AS ReviewerEmail,
                provider_rating.rating AS OverallRating,
                rating_getting_appt AS RatingGettingAppt,
                rating_appearance as RatingAppearance,
                rating_courtesy as RatingCourtesy,
                rating_waiting_time as RatingWaitingTime,
                rating_friendliness as RatingFriendliness,
                rating_spending_time as RatingSpendingTime,
                rating_diagnosis as RatingDiagnosis,
                rating_follow_up as RatingFollowUp,
                rating_billing as RatingBilling,
                rating_listening_skills as RatingListeningSkills,
                rating_clear_explanations as RatingClearExplanations,
                rating_trust as RatingTrust,
                rating_sentiment as RatingSentiment
                FROM provider_rating
                INNER JOIN account_provider
                    ON account_provider.provider_id= provider_rating.provider_id
                INNER JOIN provider
                    ON provider.id = provider_rating.provider_id
                INNER JOIN account_practice
                    ON account_practice.practice_id= provider_rating.practice_id
                INNER JOIN practice
                    ON practice.id = provider_rating.practice_id
                WHERE account_provider.account_id= account_practice.account_id
                    AND account_practice.account_id = %s
                ORDER BY provider_rating.date_added;",
                $ownerAccountId
            );
            return Yii::app()->db->createCommand($sql)->queryAll();
        } else {
            return array();
        }
    }

    /**
     * Get 3 random recent reviews for a given organization for use in the monthly report
     * @param int $organizationId
     * @return array
     */
    public static function getMRRecentReviews($organizationId)
    {

        $sql = sprintf(
            "SELECT GROUP_CONCAT(id)
            FROM (
                SELECT provider_rating.id
                FROM provider_rating
                INNER JOIN account_practice
                    ON provider_rating.practice_id = account_practice.practice_id
                INNER JOIN organization_account
                    ON account_practice.account_id = organization_account.account_id
                    AND `connection` = 'O'
                WHERE organization_id = %d
                    AND provider_rating.status = 'A'
                    AND DATEDIFF(CURDATE(), provider_rating.date_added) <= 90
                ORDER BY provider_rating.date_added DESC
                LIMIT 20
                ) AS recent_ratings;",
            $organizationId
        );
        $recentReviewsIds = Yii::app()->db->createCommand($sql)->queryScalar();

        if (!empty($recentReviewsIds)) {

            // we want random 3 from ratings with 4, 5 stars in the most recent 20
            $sql = sprintf(
                "SELECT GROUP_CONCAT(id)
                FROM (
                    SELECT CONCAT(id) AS id
                    FROM provider_rating
                    WHERE id IN (%s)
                        AND rating IN (4, 5)
                    ORDER BY RAND()
                    LIMIT 3
                    ) AS high;",
                $recentReviewsIds
            );
            $recentHighIds = Yii::app()->db->createCommand($sql)->queryScalar();

            // we want random 2 from ratings with 1, 2 stars in the most recent 20
            $sql = sprintf(
                "SELECT GROUP_CONCAT(id)
                FROM (
                    SELECT CONCAT(id) AS id
                    FROM provider_rating
                    WHERE id IN (%s)
                        AND rating IN (1, 2)
                    ORDER BY RAND()
                    LIMIT 2
                    ) AS low;",
                $recentReviewsIds
            );
            $recentLowIds = Yii::app()->db->createCommand($sql)->queryScalar();

            if (!empty($recentHighIds) || !empty($recentLowIds)) {

                $arrHigh = explode(',', $recentHighIds);
                $arrLow = explode(',', $recentLowIds);
                $arrReviewIds = array_merge($arrHigh, $arrLow);

                if (empty($arrReviewIds)) {
                    return false;
                }

                $arrReviewIds = array_filter($arrReviewIds);
                $reviewIds = implode(',', $arrReviewIds);

                $sql = sprintf(
                    "SELECT provider_rating.id, localization_id, provider_id,
                    partner_id, partner_site_id, provider_rating.practice_id,
                    patient_id, account_device_id, provider_rating.account_id,
                    device_id, shared_with, reviewer_name,
                    reviewer_name_anon, reviewer_email, reviewer_location,
                    reviewer_city_id, rating, title,
                    friendly_url, description, rating_getting_appt,
                    rating_appearance, rating_courtesy, rating_waiting_time,
                    rating_friendliness, rating_spending_time, rating_diagnosis,
                    rating_follow_up, rating_billing, rating_listening_skills,
                    rating_clear_explanations, rating_trust, rating_sentiment, status,
                    origin, reference_url, yext_site_id,
                    yext_review_id, source_ip_address, provider_rating.date_added,
                    provider_rating.date_updated, date_approved, facebook_clicks,
                    twitter_clicks, friendly_url AS facebook_url, friendly_url AS twitter_url,
                    twitter_url_short, date_facebook_sent
                    FROM provider_rating
                    WHERE id IN (%s)
                    ORDER BY rating DESC
                    LIMIT 5;",
                    $reviewIds
                );

                return Yii::app()->db->createCommand($sql)->queryAll();
            }
        }

        return false;
    }

    /**
     * Count RH reviews from last month given an organization ID
     * For use in the new Monthly Report
     * @param int $organizationId
     * @param int $minusMonths
     * @return array
     */
    public static function getMRByOrganizationId($organizationId, $minusMonths = 1)
    {

        $base = "SELECT COUNT(DISTINCT(provider_rating.id)) AS total,
            ROUND(AVG(rating), 1) AS rating,
            ROUND(AVG(rating_getting_appt), 1) AS rating_getting_appt,
            ROUND(AVG(rating_billing), 1) AS rating_billing,
            ROUND(AVG(rating_courtesy), 1) AS rating_courtesy,
            ROUND(AVG(rating_waiting_time), 0) AS rating_waiting_time,
            ROUND(AVG(rating_appearance), 1) AS rating_appearance
            FROM provider_rating
            INNER JOIN organization_account
            ON provider_rating.account_id = organization_account.account_id
            AND `connection` = 'O'
            WHERE organization_id = %d
            AND DATE_FORMAT(provider_rating.date_added, '%%Y-%%m') = '%s'
            AND `status` IN ('A', 'P')
            AND account_device_id > 0
            AND origin != 'E';";

        $sql = sprintf($base, $organizationId, date('Y-m', strtotime(sprintf('-%d month', $minusMonths))));
        return Yii::app()->db->createCommand($sql)->queryRow();
    }

    /**
     * Get number of social media clicks in an organization's last month's RH review follow up emails
     * @param int $organizationId
     * @return int
     */
    public static function getMRSocialClicksByOrganizationId($organizationId)
    {
        $sql = sprintf(
            "SELECT COUNT(DISTINCT(tracking_activity.id)) AS total
            FROM provider_rating
            INNER JOIN account_provider
                ON provider_rating.provider_id = account_provider.provider_id
            INNER JOIN organization_account
                ON account_provider.account_id = organization_account.account_id
                AND `connection` = 'O'
            INNER JOIN tracking_activity_parameter AS pr_id
                ON provider_rating.id = pr_id.value
                AND pr_id.name = 'id'
            INNER JOIN tracking_activity_parameter AS pr_table
                ON pr_id.tracking_activity_id = pr_table.tracking_activity_id
                AND pr_table.value = 'provider_rating'
            INNER JOIN tracking_activity
                ON tracking_activity.id = pr_id.tracking_activity_id
                AND tracking_activity.tracking_activity_type_id = 7
            WHERE organization_id = %d
                AND DATE_FORMAT(provider_rating.date_added, '%%Y-%%m') = '%s'
                AND account_device_id > 0;",
            $organizationId,
            date('Y-m', strtotime('-1 month'))
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Count how many sites an organization's reviews are syndicated to
     * @param int $organizationId
     * @return int
     */
    public static function getMRSyndicatedToSites($organizationId)
    {
        $cacheTime = Yii::app()->params['cache_medium'];
        $sql = sprintf(
            "SELECT COUNT(1)
            FROM partner_site
            WHERE shares_reviews='1'
            AND active='1'"
        );
        $countPartnerSites = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryScalar();

        $sql = sprintf(
            "SELECT COUNT(1)
            FROM organization_feature
            WHERE feature_id IN (14, 15)
            AND organization_id = %d;",
            $organizationId
        );
        $hasHealthGrades = Yii::app()->db->createCommand($sql)->queryScalar();

        if ($hasHealthGrades > 0) {
            $countPartnerSites++;
        }

        return $countPartnerSites;
    }

    /**
     * Get the latest approved reviews of a provider
     *
     * @param integer $providerId
     * @param integer $howMany
     * @return array
     */
    public function getLatestByProvider($providerId, $howMany = 6)
    {
        $sql = "SELECT
                  provider_rating.id,
                  provider_rating.reviewer_name_anon,
                  provider_rating.account_device_id,
                  provider_rating.reviewer_name,
                  provider_rating.rating,
                  provider_rating.description,
                  provider_rating.date_added
                FROM provider_rating
                WHERE provider_id = :provider_id AND status = 'A' AND origin <> 'E'
                ORDER BY id DESC LIMIT :limit";

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])
            ->createCommand($sql)
            ->bindValues([
                ":provider_id" => $providerId,
                ":limit" => $howMany
            ])
            ->queryAll();
    }

    /**
     * Update location based on IP address
     */
    public function updateLocation()
    {
        if ($this->source_ip_address) {
            // get ip translation to location data
            $detectedLocation = GeoLocationComponent::detectLocation($this->source_ip_address);

            // find location, if not exists, create
            $locationData = City::findOrCreateCity($detectedLocation);

            if (!empty($locationData)) {
                if (!empty($locationData['location_zipcode']) && $locationData['country_code'] == 'US') {
                    $this->reviewer_location = $locationData['location_zipcode'];
                } else {
                    $this->reviewer_location = $locationData['city_name'] . ', ' . $locationData['state_abb'];
                }
                $this->reviewer_city_id = $locationData['city_id'];

                if ($this->patient_id > 0) {
                    // update patient->location and zipcode
                    $patient = Patient::model()->find("id = :id", array(":id" => $this->patient_id));
                    if (empty($patient->location_id) || empty($patient->zipcode)) {
                        $patient->location_id = $locationData['location_id'];
                        $patient->zipcode = $locationData['location_zipcode'];
                        $patient->save();
                    }
                }
            }
        }

        $this->save();
    }

    /**
     * Get reviews for the admin reviews feature of PADM.
     * Returns either an array with the results or false in case of security excelption.
     *
     * @param integer $accountId
     * @param integer $filters
     * @param integer $sort
     * @param integer $order
     * @param integer $limit
     * @param integer $offset
     * @return mixed
     */
    public static function getProviderRatingIndex($accountId, $filters, $sort, $order, $limit, $offset)
    {

        $practiceIds = "";
        if (!empty($filters['practice_ids'])) {
            $pIds = explode(',', $filters['practice_ids']);
            $practiceIds = sprintf("AND pr.practice_id IN (%s)", implode(',', $pIds));

            if (!empty($pIds)) {

                // Check if the user can access the given practice
                $sql = sprintf(
                    "SELECT practice_id
                    FROM account_practice
                    WHERE account_id = %d",
                    $accountId
                );
                $allowedPacticeIds = Yii::app()->db->createCommand($sql)->queryColumn();

                $forbiddenPractices = false;
                foreach ($pIds as $id) {
                    if (!in_array($id, $allowedPacticeIds)) {
                        $forbiddenPractices = true;
                        break;
                    }
                }

                if ($forbiddenPractices) {
                    return false;
                }
            }
        }

        $providerFilter = "";
        if (!empty($filters['provider'])) {
           $providerFilter = sprintf(" AND p.id IN (%s)", $filters['provider']);
        }

        $practiceFilter = "";
        if (!empty($filters['practice'])) {
            $practiceFilter = sprintf(" AND q.name LIKE '%%%s%%'", addslashes($filters['practice']));
        }

        $reviewerCondition = "";
        if (!empty($filters['patient'])) {
            $reviewerCondition = sprintf(" AND pr.reviewer_name LIKE '%%%s%%'", addslashes($filters['patient']));
        }

        $dateFilter = "";
        if (!empty($filters['date'])) {
            $dateFilter = sprintf(" AND DATE(pr.date_added) = '%s'", $filters['date']);
        } elseif (!empty($filters['date_from']) && !empty($filters['date_to'])) {
            $dateFilter = sprintf(
                " AND DATE(pr.date_added) BETWEEN '%s' AND '%s'",
                $filters['date_from'],
                $filters['date_to']
            );
        }

        $ratingFilters = "";
        if (!empty($filters['rating'])) {
            $ratingFilters = sprintf(" AND pr.rating IN (%s)", $filters['rating']);
        }

        $statusFilter = "";
        if (!empty($filters['status'])) {
            $statusFilter = sprintf(" AND pr.status = '%s'", $filters['status']);
        }

        $sharedFilter = "";
        if (!empty($filters['shared'])) {
            switch (strtolower($filters['shared'])) {
                case 'facebook':
                    $statusFilter = sprintf(" AND pr.share_on_facebook IS NOT NULL");
                    break;
                case 'twitter':
                    $statusFilter = sprintf(" AND pr.share_on_twitter IS NOT NULL");
                    break;
                case 'none':
                    $statusFilter = sprintf(" AND pr.share_on_facebook IS NULL AND pr.share_on_twitter IS NULL");
                    break;
                default:
            }
        }

        $partnerSiteFilter = "";
        if (!empty($filters['partner_site'])) {
            $aPartnerSite = explode(",", $filters['partner_site']);
            if (is_array($aPartnerSite) && !empty($aPartnerSite)) {
                $aPartnerSite = array_map('strtolower', $aPartnerSite);
                $partnerSiteFilter .= " AND ( 0 ";
                if (in_array('reviewrequest', $aPartnerSite)) {
                    $partnerSiteFilter .= sprintf(
                        " OR d.device_model_id IS NOT NULL AND d.device_model_id = %d",
                        DeviceModel::REVIEW_REQUEST_ID
                    );
                }
                if (in_array('reviewhub', $aPartnerSite)) {
                    $partnerSiteFilter .= sprintf(
                        " OR d.device_model_id IS NOT NULL AND d.device_model_id <> %d",
                        DeviceModel::REVIEW_REQUEST_ID
                    );
                }
                if (in_array('doctor.com', $aPartnerSite)) {
                    $partnerSiteFilter .= sprintf(
                        " OR ps.display_name = '%s' AND pr.account_device_id is NULL",
                        "Doctor.com"
                    );
                }
                if (in_array('google', $aPartnerSite)) {
                    $partnerSiteFilter .= sprintf(
                        " OR ps.display_name = '%s' AND pr.account_device_id is NULL",
                        "Google"
                    );
                }
                if (in_array('siteenhancewidgets', $aPartnerSite)) {
                    $partnerSiteFilter .= sprintf(
                        " OR ps.display_name = '%s' AND pr.account_device_id is NULL",
                        "SiteEnhance Widgets"
                    );
                }
                $partnerSiteFilter .= " )";
            }
        }

        // Sorting
        $orderQuery = "";
        $orderBy = array(
            "desc" => "DESC",
            "asc" => "ASC"
        );
        if (strtolower($order) === 'asc' || strtolower($order) === 'desc') {
            switch ($sort) {
                case 'provider':
                    $orderQuery = "ORDER BY p.first_m_last " . $orderBy[strtolower($order)];
                    break;
                case 'practice':
                    $orderQuery = "ORDER BY q.name " . $orderBy[strtolower($order)];
                    break;
                case 'rating':
                    $orderQuery = "ORDER BY pr.rating " . $orderBy[strtolower($order)];
                    break;
                case 'partner_site':
                    $orderQuery = "ORDER BY ps.display_name " . $orderBy[strtolower($order)];
                    break;
                case 'patient':
                    $orderQuery = "ORDER BY pr.reviewer_name " . $orderBy[strtolower($order)];
                    break;
                case 'status':
                    $orderQuery = "ORDER BY pr.status " . $orderBy[strtolower($order)];
                    break;
                case 'shared':
                    $orderQuery = "ORDER BY pr.share_on_facebook " . $orderBy[strtolower($order)] .
                        ", pr.share_on_twitter " . $orderBy[strtolower($order)];
                    break;
                case 'date':
                default:
                    $orderQuery = "ORDER BY pr.date_added " . $orderBy[strtolower($order)];
                    break;
            }
        }

        $selectQuery = "SELECT pr.id, pr.origin, pr.provider_id, pr.practice_id, pr.reviewer_name,
        pr.reviewer_name_anon, p.first_m_last AS provider, pr.date_added, pr.title, pr.status, pr.rating,
        pr.share_on_facebook, pr.share_on_twitter, pr.date_facebook_sent, pr.twitter_url_short, pr.yext_site_id,
        pr.account_device_id, pr.reference_url, pr.partner_site_id, ps.display_name AS partner_site_name,
        pr.friendly_url, pr.patient_id, q.name AS practice_name, d.device_model_id, p.friendly_url AS
        provider_friendly_url, q.friendly_url AS practice_friendly_url,
        IF(ra.id IS null, 0, 1) AS flagged, ra.abuse_report AS flagged_report";

        $countQuery = "SELECT COUNT(pr.id) ";

        $limitQuery = sprintf(" LIMIT %d, %d", $offset, $limit);

        $queryBody = sprintf(
            " FROM provider_rating pr
                INNER JOIN practice q ON pr.practice_id = q.id
                INNER JOIN account_practice aq ON aq.practice_id = pr.practice_id
                LEFT JOIN provider p ON pr.provider_id = p.id
                LEFT JOIN provider_rating_abuse ra ON pr.id = ra.rating_id
                LEFT JOIN partner_site ps ON pr.partner_site_id = ps.id
                LEFT JOIN provider_rating_widget prw ON pr.id = prw.provider_rating_id
                LEFT JOIN widget w ON prw.widget_id = w.id
                LEFT JOIN account_device ad ON pr.account_device_id = ad.id
                LEFT JOIN device d ON ad.device_id = d.id
                WHERE aq.account_id = '%d' AND pr.status IN ('A','P')
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s
                %s",
            $accountId,
            $practiceIds,
            $providerFilter,
            $practiceFilter,
            $reviewerCondition,
            $dateFilter,
            $ratingFilters,
            $statusFilter,
            $sharedFilter,
            $partnerSiteFilter,
            $orderQuery
        );

        $sql = $selectQuery . $queryBody . $limitQuery;
        $result = Yii::app()->db->createCommand($sql)->queryAll();

        $sql = $countQuery . $queryBody;
        $totalItems = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();

        return array(
            'result' => $result,
            'limit' => $limit,
            'offset' => $offset,
            'totalItems' => $totalItems
        );
    }
}
