<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSite');

class PartnerSite extends BasePartnerSite
{
    // defined here so we don't depend on the internal ids all over the place
    const GOOGLE_PARTNER_SITE_ID = 60;
    const YELP_PARTNER_SITE_ID = 54;
    const HEALTHGRADES_SITE_ID = 46;
    const VITALS_SITE_ID = 111;
    const WELLNESS_SITE_ID = 23;
    const CONTRAVE_SITE_ID = 186;
    const EVERYDAY_HEALTH_SITE_ID = 176;
    const COSENTYX_AS_SITE_ID = 179;
    const PARTNER_SITE_AAD = 77;

    // passwords are stored in PartnerSite->secret - we use the password variable when a user submits a change
    public $password;
    public $password_confirm;

    // PartnerSite credentials are PDI "API keys" and therefore do not require expiration
    public static $passwordCanExpire = false;

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function scopes()
    {
        return array(
            'active' => array(
                'condition' => 'active = 1',
            ),
            'channelPartner' => array(
                'condition' => 'is_channel_partner = 1',
            ),
            'orderByDisplayName' => array(
                'order' => 'display_name ASC'
            ),
        );
    }

    /**
     * Get the partner sites from the database as html list data
     *
     * @return array("id" => "name", [...]);
     */
    public static function getHtmlList()
    {
        return CHtml::listData(
            self::model()->cache(Yii::app()->params['cache_medium'])->findAll(array('order' => 'display_name')),
            'id',
            'display_name'
        );
    }

    /**
     * Takes the given string name of a partner and set an array
     * of details for use on the frontend to the 'partner' session
     * variable. This is for setting things like the floating
     * header with the partner's logo.
     *
     * @TODO: store these details in the DB
     *
     * @param string $partnerSiteId
     * @param string $returnUrl
     * @return array("id" => "name", [...]);
     */
    public static function setInteractionDetails($partnerSiteId, $returnUrl = null)
    {

        // if $partnerSiteId is an integer, it's a key to the
        // partner_site table, so look it up. Otherwise, it's
        // a string describing the partner
        if (is_numeric($partnerSiteId)) {
            $partnerSiteName = PartnerSite::model()->findByPk($partnerSiteId)->name;
        } else {
            $partnerSiteName = $partnerSiteId;
        }

        if ($partnerSiteName === "yourdoctor") {
            $partnerSiteDetailsArray = array(
                "siteUrl" => "https://www.yourdoctor.com",
                "siteLogo" => "yourdoctor.png",
                "siteName" => "YourDoctor.com",
                "partner_site_id" => "2"
            );
        } elseif ($partnerSiteName === "ask") {
            $partnerSiteDetailsArray = array(
                "siteUrl" => "https://www.ask.com",
                "siteLogo" => "ask.png",
                "siteName" => "Ask",
                "partner_site_id" => "3"
            );
        } elseif ($partnerSiteName === "yahoohealth") {
            $partnerSiteDetailsArray = array(
                "siteUrl" => "https://www.yahoo.com",
                "siteLogo" => "yahoo_health.png",
                "siteName" => "Yahoo Health",
                "partner_site_id" => "10"
            );
        } elseif ($partnerSiteName === "dentalplans") {
            $partnerSiteDetailsArray = array(
                "siteUrl" => "https://www.dentalplans.com",
                "siteLogo" => "dentalplans.png",
                "siteName" => "Dentalplans.com",
                "partner_site_id" => "11",
                "showOverlay" => false, // don't show overlay when landing
            );
        } elseif ($partnerSiteName === "chiropractor") {
            $partnerSiteDetailsArray = array(
                "siteUrl" => "https://www.chiropractorhub.com",
                "siteLogo" => "CH-Logo.png",
                "siteName" => "Chiropractorhub.com",
                "partner_site_id" => "12"
            );
        } elseif ($partnerSiteName === "healthline") {
            $partnerSiteDetailsArray = array(
                "siteUrl" => "https://healthline.doctor.com",
                "siteLogo" => "hl-logo.png",
                "siteName" => "Healthline.com",
                "partner_site_id" => "2"
            );
        } elseif ($partnerSiteName === "alliance") {
            $partnerSiteDetailsArray = array(
                "siteUrl" => "https://www.the-alliance.org",
                "siteLogo" => "alliance.png",
                "siteName" => "The Alliance",
                "partner_site_id" => "20"
            );
        } else {
            // no partner set
            return false;
        }

        // default for showOverlay is true
        if (!isset($partnerSiteDetailsArray['showOverlay'])) {
            $partnerSiteDetailsArray['showOverlay'] = true;
        }

        $partnerSiteDetailsArray['site'] = $partnerSiteName;
        $partnerSiteDetailsArray['returnUrl'] = $returnUrl ?
            $returnUrl : $partnerSiteDetailsArray['siteUrl'];

        // we return array variable to asign session and display partner-related
        // elements on the page
        return $partnerSiteDetailsArray;
    }

    /**
     * DA: get the current list of Twilio and Partner Site IDs
     */
    public static function getTwilioAndPartnerSites()
    {
        $sql = 'SELECT partner_site.id AS partnerSiteID, partner_id AS partnerID, alias_name, display_name,
                MIN(twilio_partner.id) AS twilioID
                FROM partner_site
                LEFT JOIN twilio_partner
                ON twilio_partner.partner_site_id = partner_site.id
                WHERE alias_name IS NOT NULL
                GROUP BY partnerID;';
        return Yii::app()->db->createCommand($sql)->query();
    }

    /**
     * Get partner site id from soruce
     * @param string $source
     * @return int $partnerSiteId
     */
    public static function getPartnerBySource($source = '')
    {
        if (empty($source)) {
            return 0;
        }
        $source = strtolower(trim($source));

        $sql = sprintf(
            "SELECT id, `name`, display_name FROM partner_site
            WHERE LOWER(`name`) = '%s' OR LOWER(`display_name`) = '%s'
            LIMIT 1;",
            $source,
            $source
        );
        $result = Yii::app()->db->createCommand($sql)->queryRow();
        if (empty($result)) {
            return 0;
        }

        return $result['id'];
    }

    public function getChannelPartners()
    {
        return $this->with('organizations')
            ->findAll("t.is_channel_partner = 1 AND t.active = 1");
    }

    public function getByIds($ids)
    {
        $c = new CDbCriteria();
        $c->addInCondition('id', $ids);

        return $this->findAll($c);
    }

    /**
     * Warning Service
     * @param string $partnerSite
     * @return string $str
     */
    public static function getWarningService($partnerSite = null)
    {
        $sql = 'SELECT DISTINCT partner_site.id, listing_site.name
        FROM prd01.partner_site
        INNER JOIN prd01.listing_site
        ON partner_site.listing_site_id = listing_site.id
        WHERE  partner_site.active=1
        AND listing_site.active=1
        AND partner_site.id NOT IN (
            SELECT partner_site_id FROM partner_site_warning_service WHERE date_end IS NULL Or date_end > now()
        )
        AND (custom_provider is not null or custom_practice is not null)
        ORDER BY listing_site.name;';
        $res = Yii::app()->db->createCommand($sql)->query();

        // build the partner_site ids string
        $str = "";
        foreach ($res as $value) {
            if ($str == "") {
                $str .= "(";
            } else {
                $str .= " OR ";
            }
            $str .= " id=" . $value['id'] . " ";
        }

        // if partnerSite are setting
        if (!is_null($partnerSite)) {
            if ($str == "") {
                $str .= "(";
            }
            $str .= " OR id=" . $partnerSite . " ";
        }

        if ($str != "") {
            $str .= ")";
        }

        return $str;
    }
}
