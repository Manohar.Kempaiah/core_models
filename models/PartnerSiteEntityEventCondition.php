<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteEntityEventCondition');

class PartnerSiteEntityEventCondition extends BasePartnerSiteEntityEventCondition
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
