<?php

Yii::import('application.modules.core_models.models._base.BaseScanRequestProvider');

class ScanRequestProvider extends BaseScanRequestProvider
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function relations()
    {
        $relations = parent::relations();
        $relations['primarySpecialty'] = [self::BELONGS_TO, 'SubSpecialty', 'primary_specialty_id'];

        return $relations;
    }
}
