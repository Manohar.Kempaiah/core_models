<?php

Yii::import('application.modules.core_models.models._base.BaseEducationType');

class EducationType extends BaseEducationType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('name, display_name', 'required'),
            array('name, display_name', 'unique'),
            array('name, display_name', 'length', 'max' => 50),
            array('id, name, display_name', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('name', $this->name, true);
        $criteria->compare('display_name', $this->display_name, true);

        return new CActiveDataProvider(get_class($this), array('criteria' => $criteria, 'pagination' => array('pageSize' => 100)));
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from education_type if associated records exist in provider_education table
        $has_associated_records = ProviderEducation::model()->exists(
            'education_type_id=:education_type_id',
            array(':education_type_id' => $this->id)
        );
        if ($has_associated_records) {
            $this->addError("id", "Education type can't be deleted because there are providers linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

}
