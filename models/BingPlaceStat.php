<?php

Yii::import('application.modules.core_models.models._base.BaseBingPlaceStat');

/**
 * A single Bing Place analytics unit.
 */
class BingPlaceStat extends BaseBingPlaceStat
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Find a Bing Place Stat by a stat result from the API
     *
     * @param object $data
     * @param string $storeId
     * @return void
     */
    public function findFromResult($data, $storeId)
    {
        $date = str_replace('T', ' ', $data->BusinessStatStartTime);
        return self::model()->findByAttributes([
            'start_date' => $date,
            'store_id' => $storeId
        ]);
    }

    /**
     * Determine if the stat instance belongs to the current week
     *
     * @return boolean
     */
    public function isCurrentWeek()
    {
        $date = date("Y-m-d H:i:s");

        if ($date >= $this->start_date && $date <= $this->end_date) {
            return true;
        }

        return false;
    }

    /**
     * Update attributes from an API stat result
     * This method just updates the attributes and doesn't save to DB
     *
     * @param object $data
     * @return void
     */
    public function updateFromResult($data)
    {
        $this->impressions = $data->ImpressionCount;
        $this->created_at = date("Y-m-d H:i:s");
    }

    /**
     * Create a BingPlaceStat object from an API result
     *
     * @param object $data
     * @param string $storeId
     * @return BingPlaceStat
     */
    public static function fromResult($data, $storeId)
    {
        $bps = new BingPlaceStat();
        $date = str_replace('T', ' ', $data->BusinessStatStartTime);
        $dateEnd = date("Y-m-d H:i:s", strtotime($date) + (60 * 60 * 24 * 7));
        $bps->start_date = $date;
        $bps->end_date = $dateEnd;
        $bps->impressions = $data->ImpressionCount;
        $bps->store_id = $storeId;
        $bps->created_at = date("Y-m-d H:i:s");
        $bps->addEntities();

        return $bps;
    }

    /**
     * Add the related entities and their foreign key fields
     *
     * Currently we only support provider and practice ids
     * the store id format is the following:
     *     BP-{practice_id}-{provider_id}
     *
     * Examples:
     *     BP-1234 (practice id: 1234)
     *     BP-1234-5678 (practice id: 1234, provider id: 5678)
     *     BP--5678 (provider id: 5678)
     *     BP-1234- (practice id: 1234 (same as first example))
     *
     * @return void
     */
    public function addEntities()
    {
        $results = [];
        $practiceId = '';
        $providerId = '';

        preg_match('/BP-(.*)-(.*)/', $this->store_id, $results);
        if (!$results) {
            preg_match('/BP-(.*)/', $this->store_id, $results);
        }

        if ($results) {
            if (isset($results[1]) && $results[1]) {
                $practiceId = $results[1];
                $practice = Practice::model()->findByPk($practiceId);
                if ($practice) {
                    $this->practice_id = $practice->id;
                    $this->practice = $practice;
                }
            }

            if (isset($results[2]) && $results[2]) {
                $providerId = $results[2];
                $provider = Provider::model()->findByPk($providerId);
                if ($provider) {
                    $this->provider_id = $provider->id;
                    $this->provider = $provider;
                }
            }
        }
    }
}
