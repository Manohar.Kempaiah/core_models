<?php

Yii::import('application.modules.core_models.models._base.BaseCampaign');

class Campaign extends BaseCampaign
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Create an email campaign
     * @param string $name
     * @return boolean
     */
    public static function generate($name)
    {
        $result = false;
        switch ($name) {
            case "officeHoursConfirmation":
                $result = self::createOfficeHoursConfirmation();
                break;
            case "officeHoursConfirmationFreemium":
                $result = self::createOfficeHoursConfirmationFreemium();
                break;
            default :
                break;
        }
        return $result;
    }

    /**
     * Create an OFFICE_HOURS_CONFIRMATION campaign
     * @return boolean
     */
    public static function createOfficeHoursConfirmation()
    {
        $campaign = Campaign::model()->find("name = :name", array(":name" => "officeHoursConfirmation"));
        $template = Template::model()->find(
            "internal_type = :internal_type",
            array(
                ":internal_type" => "APPOINTMENT_OFFICE_HOURS_CONFIRMATION"
            )
        );

        if (!$campaign) {
            $campaign = new Campaign();
            $campaign->name = "officeHoursConfirmation";
            $campaign->template_id = $template->id;
            if (!$campaign->save()) {
                return false;
            }
        }

        $sql = sprintf("SELECT DISTINCT
            account_practice.account_id as account_id, account.organization_name AS account_organization_name,
            account.email as account_email, provider.id as provider_id, provider.first_m_last as provider_name,
            practice.id as practice_id, practice.name as practice_name, provider_practice.id as provider_practice_id
            FROM provider_practice
            INNER JOIN provider_details ON provider_details.provider_id=provider_practice.provider_id
            INNER JOIN account_practice ON account_practice.practice_id = provider_practice.practice_id
            INNER JOIN practice ON account_practice.practice_id = practice.id
            INNER JOIN account_provider ON account_provider.provider_id = provider_practice.provider_id
            INNER JOIN provider ON account_provider.provider_id = provider.id
            INNER JOIN account ON account_practice.account_id = account.id
            INNER JOIN organization_account ON organization_account.account_id = account.id
            INNER JOIN organization ON organization.id = organization_account.organization_id
            WHERE account_practice.account_id = account_provider.account_id
            AND provider_details.schedule_mail_enabled = 1
            AND organization.status = 'A'
            ORDER BY account_practice.account_id, provider.id, practice.id ASC;");
        $accounts = Yii::app()->db->createCommand($sql)->query();

        $ap = null;
        $accountId = 0;
        foreach ($accounts as $a) {
            if ($a['account_id'] != $accountId) {
                $accountId = $a['account_id'];
                if (isset($ap)) {
                    CampaignEmail::createOfficeHoursConfirmEmail($campaign->id, $template, $ap);
                }
                $ap = array();
                $ap['account_id'] = $a['account_id'];
                $ap['account_organization_name'] = $a['account_organization_name'];
                $ap['to'] = $a['account_email'];
                $ap['providers'] = array(
                    $a['provider_id'] => array(
                        'id' => $a['provider_id'],
                        'name' => $a['provider_name'],
                        'practices' => array(
                            $a['practice_id'] = array(
                                'id' => $a['practice_id'],
                                'name' => $a['practice_name'],
                                'schedule' => ProviderPracticeSchedule::getSchedule($a['provider_practice_id'])
                            )
                        )
                    )
                );
            } else {
                if (isset($ap['providers'][$a['provider_id']])) {
                    $ap['providers'][$a['provider_id']]['practices'][$a['practice_id']] = array(
                        'id' => $a['practice_id'],
                        'name' => $a['practice_name'],
                        'schedule' => ProviderPracticeSchedule::getSchedule($a['provider_practice_id'])
                    );
                } else {
                    $ap['providers'][$a['provider_id']] = array(
                        'id' => $a['provider_id'],
                        'name' => $a['provider_name'],
                        'practices' => array(
                            $a['practice_id'] = array(
                                'id' => $a['practice_id'],
                                'name' => $a['practice_name'],
                                'schedule' => ProviderPracticeSchedule::getSchedule($a['provider_practice_id'])
                            )
                        )
                    );
                }
            }

        }
        if (isset($ap)) {
            $campaignEmail = CampaignEmail::createOfficeHoursConfirmEmail($campaign->id, $template, $ap);
        }
    }

    /**
     * Create an OFFICE_HOURS_CONFIRMATION_FREEMIUM campaign
     * @return boolean
     */
    public static function createOfficeHoursConfirmationFreemium()
    {
        $campaign = Campaign::model()->find("name = :name", array(":name" => "officeHoursConfirmationFreemium"));
        $template = Template::model()->find(
            "internal_type = :internal_type",
            array(
                ":internal_type" => "APPOINTMENT_OFFICE_HOURS_CONFIRMATION_FREEMIUM"
            )
        );

        if (!$campaign) {
            $campaign = new Campaign();
            $campaign->name = "officeHoursConfirmationFreemium";
            $campaign->template_id = $template->id;
            if (!$campaign->save()) {
                return false;
            }
        }

        $sql = sprintf("SELECT DISTINCT
            account_practice.account_id as account_id, account.organization_name AS account_organization_name,
            account.email as account_email, provider.id as provider_id, provider.first_m_last as provider_name,
            practice.id as practice_id, practice.name as practice_name, provider_practice.id as provider_practice_id
            FROM provider_practice
            INNER JOIN provider_details ON provider_details.provider_id=provider_practice.provider_id
            INNER JOIN account_practice ON account_practice.practice_id = provider_practice.practice_id
            INNER JOIN practice ON account_practice.practice_id = practice.id
            INNER JOIN account_provider ON account_provider.provider_id = provider_practice.provider_id
            INNER JOIN provider ON account_provider.provider_id = provider.id
            INNER JOIN account ON account_practice.account_id = account.id
            LEFT JOIN organization_account ON organization_account.account_id = account.id
            LEFT JOIN organization ON organization.id = organization_account.organization_id
            WHERE account_practice.account_id = account_provider.account_id
            AND provider_details.schedule_mail_enabled = 1
            AND (organization.status IS NULL OR organization.status != 'A')
            ORDER BY account_practice.account_id, provider.id, practice.id ASC;");
        $accounts = Yii::app()->db->createCommand($sql)->query();

        $ap = null;
        $accountId = 0;
        foreach ($accounts as $a) {
            if ($a['account_id'] != $accountId) {
                $accountId = $a['account_id'];
                if (isset($ap)) {
                    CampaignEmail::createOfficeHoursConfirmEmail($campaign->id, $template, $ap);
                }
                $ap = array();
                $ap['account_id'] = $a['account_id'];
                $ap['account_organization_name'] = $a['account_organization_name'];
                $ap['to'] = $a['account_email'];
                $ap['providers'] = array(
                    $a['provider_id'] => array(
                        'id' => $a['provider_id'],
                        'name' => $a['provider_name'],
                        'practices' => array(
                            $a['practice_id'] = array(
                                'id' => $a['practice_id'],
                                'name' => $a['practice_name'],
                                'schedule' => ProviderPracticeSchedule::getSchedule($a['provider_practice_id'])
                            )
                        )
                    )
                );
            } else {
                if (isset($ap['providers'][$a['provider_id']])) {
                    $ap['providers'][$a['provider_id']]['practices'][$a['practice_id']] = array(
                        'id' => $a['practice_id'],
                        'name' => $a['practice_name'],
                        'schedule' => ProviderPracticeSchedule::getSchedule($a['provider_practice_id'])
                    );
                } else {
                    $ap['providers'][$a['provider_id']] = array(
                        'id' => $a['provider_id'],
                        'name' => $a['provider_name'],
                        'practices' => array(
                            $a['practice_id'] = array(
                                'id' => $a['practice_id'],
                                'name' => $a['practice_name'],
                                'schedule' => ProviderPracticeSchedule::getSchedule($a['provider_practice_id'])
                            )
                        )
                    );
                }
            }

        }
        if (isset($ap)) {
            $campaignEmail = CampaignEmail::createOfficeHoursConfirmEmail($campaign->id, $template, $ap);
        }
    }

}
