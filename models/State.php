<?php

Yii::import('application.modules.core_models.models._base.BaseState');

class State extends BaseState
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Add friendly_url if necessary
     * @important I've removed the ability to update/delete states from the interface, since it'd affect many records.
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->country_id > 1 && empty($this->id)) {
            $code = Country::model()->findByPk($this->country_id)->code;
            $this->abbreviation = $code . '_' . $this->abbreviation;
            $this->friendly_url = $code . '_'. preg_replace("/[^a-zA-Z0-9]/", "_", $this->name);
        } elseif (empty($this->friendly_url)) {
            $this->friendly_url = $this->getFriendlyUrl($this->name);
        }

        // is this a new record?
        if ($this->id > 0) {
            // yes, check for duplicate name
            $duplicateName = State::model()->exists(
                'name=:name AND country_id=:country_id AND id != :id',
                array(':name' => $this->name, ':country_id' => $this->country_id, ':id' => $this->id)
            );
        } else {
            // no, check for duplicate name
            $duplicateName = State::model()->exists(
                'name=:name AND country_id=:country_id',
                array(':name' => $this->name, ':country_id' => $this->country_id)
            );
        }

        // was the name a dupe?
        if ($duplicateName) {
            // yes, exit
            $this->addError('name', 'Duplicate Name.');
            return false;
        }

        // is this a new record?
        if ($this->id > 0) {
            // yes, check for duplicate name
            $duplicateAbbr = State::model()->exists(
                'abbreviation=:abbreviation AND id != :id',
                array(':abbreviation' => $this->abbreviation, ':id' => $this->id)
            );
        } else {
            // no, check for duplicate abbreviation
            $duplicateAbbr = State::model()->exists(
                'abbreviation=:abbreviation',
                array(':abbreviation' => $this->abbreviation)
            );
        }

        // was the abbreviation a dupe?
        if ($duplicateAbbr) {
            // yes. exit
            $this->addError('abbreviation', 'Duplicate Abbreviation.');
            return false;
        }

        // is this a new record?
        if ($this->id > 0) {
            // yes, check for duplicate name
            $duplicateFri = State::model()->exists(
                'friendly_url=:friendly_url AND id != :id',
                array(':friendly_url' => $this->friendly_url, ':id' => $this->id)
            );
        } else {
            // no, check for duplicate abbreviation
            $duplicateFri = State::model()->exists(
                'friendly_url=:friendly_url',
                array(':friendly_url' => $this->friendly_url)
            );
        }

        // was the friendly url a dupe?
        if ($duplicateFri) {
            // yes, exit
            $this->addError('friendly_url', 'Duplicate Friendly Url.');
            return false;
        }

        return parent::beforeSave();

    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // avoid deleting from state if associated records exist in city table
        $hasAssociatedRecords = City::model()->exists('state_id=:state_id', array(':state_id' => $this->id));
        if ($hasAssociatedRecords) {
            $this->addError("id", "State can't be deleted because there are cities linked to it.");
            return false;
        }

        return parent::beforeDelete();
    }

    /**
     * Returns the friendly url of a given state name
     *
     * @param string $stateName
     * @param int $stateId
     * @return string
     */
    public static function getFriendlyUrl($stateName, $stateId = 0)
    {
        $url = StringComponent::prepareNewFriendlyUrl($stateName);
        return StringComponent::getNewFriendlyUrl($url, $stateId, 'state');
    }

    /**
     * Returns top 5 states with the most providers accepting a given insurance
     * @param int $insuranceId
     * @return array
     */
    public function getTopStatesByInsurance($insuranceId)
    {
        $criteria['insurance.company_id'] = $insuranceId;
        return $this->getTopStates($criteria, 5);
    }

    /**
     * Get states with the most providers practicing a specialty
     * @param int $specialtyId
     * @param int $maxResultSize
     * @param int $countryId
     * @return array
     */
    public function getTopStatesBySpecialty($specialtyId, $maxResultSize = 9999, $countryId = false)
    {

        $criteria['specialty.specialty_id'] = $specialtyId;
        return $this->getTopStates($criteria, $maxResultSize, $countryId);
    }

    /**
     * Get top states by number of providers practicing a specialty and accepting an insurance
     * @param int $specialtyId
     * @param int $insuranceId
     * @return array
     */
    public function getTopStatesBySpecialtyInsurance($specialtyId, $insuranceId)
    {
        $criteria['specialty.specialty_id'] = $specialtyId;
        $criteria['insurance.company_id'] = $insuranceId;
        return $this->getTopStates($criteria);
    }

    /**
     * Find state by Abbreviation
     * @param str $abbreviation
     * @return array
     */
    public static function findStateByAbbreviation($abbreviation = '')
    {
        if (empty($abbreviation)) {
            return false;
        }
        return State::model()->cache(Yii::app()->params['cache_short'])->find(
            'abbreviation = :state',
            array(':state' => $abbreviation)
        );

    }

    /**
     * Return top states by number of providers based on criteria
     * @param array $criteria
     * @param int $maxResultSize
     * @param int $countryId
     * @return array
     */
    private function getTopStates($criteria, $maxResultSize = 9999, $countryId = false)
    {
        $topHits = DoctorIndex::topHitsAggregate($criteria, 'practice.state_id', $maxResultSize);
        $stateIds = array_column($topHits, 'key');
        $stateIds = array_filter($stateIds);

        $providerCounts = array_column($topHits, 'doc_count');

        if (empty($stateIds)) {
            return array();
        }

        $countryWhere = "";
        if (isset($countryId) && is_numeric($countryId)) {
            $countryWhere = "AND country_id = " . $countryId;
        }

        $sql = sprintf(
            "SELECT SQL_NO_CACHE id, name, abbreviation, friendly_url
            FROM state
            WHERE id IN (%s)
            %s
            ORDER BY FIELD(id, %s);",
            implode(',', $stateIds),
            $countryWhere,
            implode(',', $stateIds)
        );
        $results = Yii::app()->db->cache(86400)->createCommand($sql)->queryAll();

        foreach ($results as $i => $result) {
            $results[$i]['providers'] = $providerCounts[$i];
        }

        return $results;
    }

    /**
     * for extra validations
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();

        $rules[] = array('name, friendly_url', 'compare', 'operator' => '!=', 'compareValue' => '-');
        $rules[] = array('name, friendly_url', 'compare', 'operator' => '!=', 'compareValue' => '_');

        return $rules;
    }

}
