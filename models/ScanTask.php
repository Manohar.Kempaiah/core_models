<?php

Yii::import('application.modules.core_models.models._base.BaseScanTask');

class ScanTask extends BaseScanTask
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Find a ScanTask by the ticket id
     * By Convention this method assumes the ticket url has the following format:
     *     http(s)://some/site/{ticket_id}
     * The important part is that it ends with "/{ticket_id}"
     *
     * @param integer $id
     * @param string typeCode
     * @return ScanTask
     */
    public function getByTicketId($id, $typeCode = '')
    {
        $criteria = new CDbCriteria();
        $criteria->with = ['type'];
        $criteria->together = true;
        $criteria->addSearchCondition('url', '%/' . $id, false);
        $criteria->compare('type.code', $typeCode);
        return ScanTask::model()->find($criteria);
    }
}
