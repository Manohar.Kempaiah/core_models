<?php

Yii::import('application.modules.core_models.models._base.BaseProviderDetails');

class ProviderDetails extends BaseProviderDetails
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id . '" '
            . 'rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">' .
            $this->provider . '</a>');
    }

    /**
     * @todo document
     * @return string
     */
    public function getPhilosophy()
    {

        if ($this->philosophy_id > 0) {
            return Philosophy::model()->findByPk($this->philosophy_id)->name;
        }
        return '-None set-';
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $accountId
     * @param int $accountId
     * @return array $results
     */
    public static function saveData($postData, $accountId, $providerId)
    {
        if (empty($postData) || $accountId == 0 || $providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }

        $providerDetails = ProviderDetails::model()->find(
            'provider_id=:provider_id',
            array(':provider_id' => $providerId)
        );
        $auditAction = 'U';
        if (!$providerDetails) {
            $providerDetails = new ProviderDetails();
            $providerDetails->provider_id = $providerId;
            $auditAction = 'C';
        }

        $schema = ProviderDetails::model()->getSchema();
        foreach ($schema as $fieldName => $fieldType) {
            if (isset($postData[$fieldName])) {
                if ($fieldType == 'string') {
                    $value = trim($postData[$fieldName]);
                } elseif ($fieldType == 'email') {
                    $value = StringComponent::validateEmail($postData[$fieldName]);
                } elseif ($fieldType == 'integer') {
                    $value = (int) $postData[$fieldName];
                } else {
                    $value = $postData[$fieldName];
                }
                $providerDetails->$fieldName = $value;
            }
        }

        if ($providerDetails->save()) {

            $auditSection = 'Provider Details provider_id #' . $providerId;
            AuditLog::create($auditAction, $auditSection, 'provider_details', null, false);

            $results['success'] = true;
            $results['error'] = "";
            $results['data'] = (array) $providerDetails->attributes;
        } else {
            $results['success'] = false;
            $results['error'] = "ERROR_SAVING_PROVIDER_DETAILS";
            $results['data'] = $providerDetails->getErrors();
        }
        return $results;
    }

    /**
     * Prepare DOB for saving in date format, check for duplicates and avoid turning off scheduling if widgets active
     * @return boolean
     */
    public function beforeSave()
    {
        if (isset($_SERVER['REQUEST_URI'])) {
            $uriPath = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
            $uriSegments = explode('/', $uriPath);
            $uriController = $uriSegments[1];
            } 
          // is this a new record?
        if ($this->isNewRecord) {
            // yes: find if this provider has a details record
            $hasProviderDetails = ProviderDetails::model()->exists(
                'provider_id=:providerId',
                array(':providerId' => $this->provider_id)
            );
        } else {
            // no: find if this provider has a details record
            $hasProviderDetails = ProviderDetails::model()->exists(
                'provider_id=:providerId AND id != :id',
                array(':providerId' => $this->provider_id, ':id' => $this->id)
            );
        }
        if ($hasProviderDetails) {
            $this->addError(
                'id',
                'Provider already has details'
            );
            return false;
        }

        if (
            $this->schedule_mail_enabled == 0
            &&
            (!empty($uriController) && !empty($uriSegments) && $uriController == 'providerDetails')
        ) {
            $sqlaccid = sprintf(
                "SELECT account_id, emr_user_data
                FROM account_provider_emr
                WHERE provider_id = %d
                AND emr_user_data IS NOT NULL ",
                $this->provider_id
            );
            $sqltid = sprintf(
                "SELECT telemedicine_enabled,telemedicine_enabled_by_account_id
                FROM provider_details
                WHERE provider_id = %d",
                $this->provider_id
            );
            $telemedicineBooking = sprintf(
                " SELECT provider_practice.id AS id, provider_practice.practice_id AS practice_id,
                    provider_practice.provider_id AS provider_id, provider_details.telemedicine_accept_new_patients
                    FROM provider_practice
                    INNER JOIN practice_details
                    ON provider_practice.practice_id = practice_details.practice_id
                    AND practice_details.practice_type_id = 5
                    LEFT JOIN provider_details
                    ON provider_practice.provider_id = provider_details.provider_id
                    WHERE provider_practice.provider_id = %d
                    LIMIT 1",
                $this->provider_id
            );
            $accountId = Yii::app()->db->createCommand($sqlaccid)->queryColumn();
            $accountTid = Yii::app()->db->createCommand($sqltid)->queryRow();
            $telemedicineEnable = Yii::app()->db->createCommand($telemedicineBooking)->queryColumn();
            if (count($accountId) > 0) {
                $accIdArr = [];
                foreach ($accountId as $aid) {
                    $accIdArr[] = $aid;
                }
                $this->addError(
                    'schedule_mail_enabled',
                    'Appointment booking could not be turned off due to another account that has it enabled. ' .
                    'The Account ID were booking is enabled is ' . implode(",", $accIdArr) .
                    ' please disable booking from that account before disabling it in this page.'
                );
                return false;
            }
            if (count($telemedicineEnable) > 0) {

                $this->addError(
                    'schedule_mail_enabled',
                    'Appointment booking could not be turned off due to another account that has telemedicine enabled. '
                    . 'The Account ID were telemedicine booking is enabled by ' .
                    $accountTid['telemedicine_enabled_by_account_id'] .
                    ' please disable booking from that account before disabling it in this page.'
                );
                return false;
            }
        }

        // is this an existing record, and are we turning off scheduling?
        if (
            !$this->isNewRecord
            && !empty($this->oldRecord->schedule_mail_enabled)
            && $this->oldRecord->schedule_mail_enabled && !$this->schedule_mail_enabled
        ) {
            // yes
            // Is the provider linked to a widget?
            $sql = sprintf(
                "SELECT COUNT(*)
                FROM provider_practice_widget
                INNER JOIN widget
                ON widget.id = provider_practice_widget.widget_id
                WHERE widget.widget_type IN ('GET_APPOINTMENT', 'MOBILE_ENHANCE')
                AND widget.draft = 0 and widget.hidden = 0
                AND widget.options NOT LIKE '%%\"hide_appointment_button\":\"1\"%%'
                AND provider_practice_widget.provider_id = %d",
                $this->provider_id
            );
            $cnt = Yii::app()->db->createCommand($sql)->queryScalar();

            if ($cnt > 0) {
                $this->addError(
                    'schedule_mail_enabled',
                    'Cannot disable online scheduling if a SiteEnhance component is active'
                );
                return false;
            }
        }

        if (strstr($this->date_birth, '/') !== false) {
            $arrDOB = explode('/', $this->date_birth);
            $this->date_birth = $arrDOB[2] . '-' . $arrDOB[0] . '-' . $arrDOB[1];
        }

        $this->personal_phone_number = preg_replace('/[^0-9]/', '', $this->personal_phone_number);

        $this->email = StringComponent::validateEmail($this->email);
        $this->system_email = StringComponent::validateEmail($this->system_email);
        $this->licence_number = StringComponent::sanitize($this->licence_number);
        $this->score_missing_data = StringComponent::sanitize($this->score_missing_data);

        return parent::beforeSave();
    }

    /**
     * Update provider details data
     * @return boolean
     */
    public function afterSave()
    {
        if (
            empty($this->oldRecord) || $this->is_insurance_currently_saving ==
            $this->oldRecord->is_insurance_currently_saving
        ) {

            if (!self::researchMode()) {

                // if not in the research mode, import account_emr settings
                $accountProvider = AccountProvider::model()->find(
                    "provider_id = :provider_id",
                    array(":provider_id" => $this->provider_id)
                );
                if ($accountProvider) {
                    SqsComponent::sendMessage('importAccountEmr', $accountProvider->account_id);
                }
            }
        }

        // is this an existing record, and changed the scheduling?
        if (
            !empty($this->oldRecord)
            && $this->oldRecord->schedule_mail_enabled != $this->schedule_mail_enabled
        ) {
            AuditLog::create(
                'U',
                'Provider #' . $this->provider_id . ' scheduling mail changed ',
                'provider_details',
                'Schedule Mail Enabled: ' . $this->schedule_mail_enabled,
                false
            );
        }

        // update scheduling if necessary
        if (
            $this->isNewRecord
            ||
            (!empty($this->oldRecord) && $this->oldRecord->schedule_mail_enabled != $this->schedule_mail_enabled)
            ||
            (!empty($this->oldRecord) && $this->oldRecord->provider_id != $this->provider_id)
        ) {
            if (!empty($this->provider_id)) {
                // async update
                SqsComponent::sendMessage('updateProviderBooking', $this->provider_id);
            }
            if (
                !empty($this->oldRecord) && !empty($this->provider_id)
                &&
                $this->oldRecord->provider_id != $this->provider_id
            ) {
                // async update the previous provider_id
                SqsComponent::sendMessage('updateProviderBooking', $this->oldRecord->provider_id);
            }
        }

        return parent::afterSave();
    }

    /**
     * return provider details
     * @param int $providerId
     * @return mixed
     */
    public static function getDetail($providerId = 0)
    {
        return ProviderDetails::model()->find('provider_id = :provider_id', array(':provider_id' => $providerId));
    }

    /**
     * Update provider_details.in_person_booking and provider_details.telemedicine_booking when related attributes
     * change
     *
     * @see SELECT * FROM database_analytics_queries WHERE id IN (20,21);
     * @see ProviderPractice::afterSave
     * @see ProviderPractice::afterDelete
     * @see AccountPracticeEmr::afterSave
     * @see AccountPracticeEmr::afterDelete
     * @see AccountEmr::afterSave
     * @see AccountEmr::afterDelete
     * @see PracticeDetails::afterSave
     * @see PracticeDetails::afterDelete
     * @see AccountProviderEmr::afterSave
     * @see AccountProviderEmr::afterDelete
     * @see OrganizationAccount::afterSave
     * @see OrganizationAccount::afterDelete
     * @see Account::afterSave
     * @see Account::afterDelete
     *
     * @param array $arrConditions
     * @return void
     */
    public static function updateBooking($arrConditions)
    {

        $arrConditions = (array)$arrConditions;

        $sql = null;

        if (!empty($arrConditions['account_id']) && !empty($arrConditions['practice_id'])) {
            // used by account_practice_emr
            $sql = sprintf(
                "SELECT account_provider.provider_id AS provider_id
                    FROM account_provider
                    INNER JOIN provider_practice
                    ON account_provider.provider_id = provider_practice.provider_id
                    WHERE account_provider.account_id = %d
                    AND provider_practice.practice_id = %d;",
                $arrConditions['account_id'],
                $arrConditions['practice_id']
            );
        } elseif (!empty($arrConditions['account_id'])) {
            // used by account_emr, organization_account, account
            $sql = sprintf(
                "SELECT account_provider.provider_id AS provider_id
                    FROM account_provider
                    WHERE account_provider.account_id = %d;",
                $arrConditions['account_id']
            );
        } elseif (!empty($arrConditions['practice_id'])) {
            // used by practice_details
            $sql = sprintf(
                "SELECT provider_id
                    FROM provider_practice
                    WHERE practice_id = %d;",
                $arrConditions['practice_id']
            );
        }

        // no valid query found
        if (empty($sql)) {
            return false;
        }

        // run chosen query
        $arrProviders = Yii::app()->db->createCommand($sql)->queryAll();

        // loop through providers and update them
        if (!empty($arrProviders)) {
            foreach ($arrProviders as $providerId) {
                self::updateProviderBooking($providerId['provider_id']);
            }
        }
    }

    /**
     * Update provider_details.in_person_booking and provider_details.telemedicine_booking (if necessary) for a given
     * provider
     * Async calls to SqsComponent::sendMessage('updateProviderBooking', ...) are based on these queries
     *
     * @param int $providerId
     * @return bool
     */
    public static function updateProviderBooking($providerId)
    {

        if (empty($providerId)) {
            // leave early
            return false;
        }

        // look up provider details
        $pd = ProviderDetails::model()->find('provider_id=:providerId', [':providerId' => $providerId]);

        if (!$pd) {
            // leave early
            return false;
        }

        // update in_person_booking
        $sql = sprintf(
            "SELECT provider.id AS provider_id
            FROM provider
            INNER JOIN provider_details
            ON provider_details.provider_id = provider.id
            INNER JOIN account_provider_emr
            ON provider.id = account_provider_emr.provider_id
            AND emr_user_id IS NOT NULL
            AND account_provider_emr.emr_user_data IS NOT NULL
            INNER JOIN provider_practice
            ON provider_practice.provider_id = provider.id
            INNER JOIN account_practice_emr
            ON account_practice_emr.account_id = account_provider_emr.account_id
            AND provider_practice.practice_id = account_practice_emr.practice_id
            AND account_practice_emr.emr_location_id IS NOT NULL
            INNER JOIN account_emr
            ON account_emr.account_id = account_provider_emr.account_id
            AND account_emr.account_id = account_practice_emr.account_id
            AND account_emr.emr_type_id= 3
            INNER JOIN account
            ON account.id = account_emr.account_id
            LEFT JOIN practice_details
            ON practice_details.practice_id = provider_practice.practice_id
            LEFT JOIN prd01.organization_account
            ON organization_account.account_id = account_emr.account_id
            WHERE (organization_account.id IS NULL OR  organization_account.`connection`='O')
            AND account.enabled = 1
            AND (practice_details.practice_type_id <> 5 OR practice_details.id IS NULL)
            AND provider.id = %d
            LIMIT 1;",
            $providerId
        );
        $boolBooking = (bool)Yii::app()->db->createCommand($sql)->queryScalar();

        if ($pd->in_person_booking != $boolBooking) {
            $pd->in_person_booking = (int)$boolBooking;
            $pd->save();
        }

        // update telemedicine_booking
        $sql = sprintf(
            "SELECT provider.id AS provider_id
            FROM provider
            INNER JOIN provider_details
            ON provider_details.provider_id = provider.id
            INNER JOIN account_provider_emr
            ON provider.id = account_provider_emr.provider_id
            AND account_provider_emr.emr_user_id IS NOT NULL
            AND account_provider_emr.emr_user_data IS NOT NULL
            INNER JOIN provider_practice
            ON provider_practice.provider_id = provider.id
            INNER JOIN account_practice_emr
            ON account_practice_emr.account_id = account_provider_emr.account_id
            AND provider_practice.practice_id = account_practice_emr.practice_id
            AND account_practice_emr.emr_location_id IS NOT NULL
            INNER JOIN account_emr
            ON account_emr.account_id = account_provider_emr.account_id
            AND account_emr.account_id = account_practice_emr.account_id
            AND account_emr.emr_type_id= 3
            INNER JOIN account
            ON account.id = account_emr.account_id
            INNER JOIN practice_details
            ON practice_details.practice_id = provider_practice.practice_id
            AND practice_details.practice_type_id = 5
            LEFT JOIN prd01.organization_account
            ON organization_account.account_id = account_emr.account_id
            WHERE (organization_account.id IS NULL OR  organization_account.`connection`='O')
            AND account.enabled = 1
            AND provider.id = %d
            LIMIT 1;",
            $providerId
        );
        $boolBooking = (bool)Yii::app()->db->createCommand($sql)->queryScalar();

        if ($pd->telemedicine_booking != $boolBooking) {
            $pd->telemedicine_booking = (int)$boolBooking;
            $pd->save();
        }

        return true;
    }
}
