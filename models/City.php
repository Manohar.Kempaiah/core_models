<?php

Yii::import('application.modules.core_models.models._base.BaseCity');

class City extends BaseCity
{

    public $country_id;
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        return array(
            array('state_id, name, friendly_url', 'required'),
            array('state_id, is_pending_approval, visits_number', 'numerical', 'integerOnly' => true),
            array('name, friendly_url', 'length', 'max' => 50),
            array('is_pending_approval, visits_number', 'default', 'setOnEmpty' => true, 'value' => null),
            array('name, friendly_url', 'compare', 'operator' => '!=', 'compareValue' => '-'),
            array('name, friendly_url', 'compare', 'operator' => '!=', 'compareValue' => '_'),
            array(
                'id, state_id, is_pending_approval, name, friendly_url, visits_number, country_id',
                'safe',
                'on' => 'search'
            ),
        );
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        return array(
            'state' => array(self::BELONGS_TO, 'State', 'state_id'),
            'locations' => array(self::HAS_MANY, 'Location', 'city_id'),
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'state_id' => Yii::t('app', 'State'),
            'is_pending_approval' => Yii::t('app', 'Is Pending Approval'),
            'name' => Yii::t('app', 'Name'),
            'friendly_url' => Yii::t('app', 'Friendly Url'),
            'visits_number' => Yii::t('app', 'Visits Number'),
            'country_id' => Yii::t('app', 'Country'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     * @return CActiveDataProvider
     */
    public function search()
    {
        $criteria = new CDbCriteria;
        $criteria->compare('id', $this->id);
        $criteria->compare('state_id', $this->state_id);
        $criteria->compare('t.is_pending_approval', $this->is_pending_approval, true);
        $criteria->compare('t.name', $this->name, true);
        $criteria->compare('t.friendly_url', $this->friendly_url, true);
        $criteria->compare('visits_number', $this->visits_number);
        $criteria->compare('country.id', $this->country_id, true);

        $criteria->select = 't.id, t.name, t.state_id, t.friendly_url, t.visits_number, t.is_pending_approval ';
        $criteria->join = '
            LEFT JOIN state
            ON t.state_id = state.id
            LEFT JOIN country
            ON state.country_id = country.id';

        return new CActiveDataProvider(
            get_class($this),
            array('criteria' => $criteria, 'pagination' => array('pageSize' => 10))
        );
    }

    /**
     * Add friendly_url if necessary
     * @important I removed the ability to add/update/delete cities from the interface, since it'd affect many records
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->country_id > 1 && empty($this->id)) {
            $stateAbbreviation = State::model()->findByPk($this->state_id)->abbreviation;
            $this->friendly_url = $stateAbbreviation . '_' . preg_replace("/[^a-zA-Z0-9]/", "_", $this->name);
        } elseif (empty($this->friendly_url)) {
            $this->friendly_url = $this->getFriendlyUrl($this->name);
        }

        // is this an existing record?
        if ($this->id > 0) {
            // yes, check if the name already exists in that state, or if the friendly URL already exists
            $duplicateNameOrFriendlyURL = City::model()->exists(
                '((name = :name AND state_id = :stateId) OR friendly_url = :friendlyUrl) AND id != :id',
                array(
                    ':name' => $this->name,
                    ':stateId' => $this->state_id,
                    ':friendlyUrl' => $this->friendly_url,
                    ':id' => $this->id
                )
            );
        } else {
            // no, check if the name already exists in that state, or if the friendly URL already exists
            $duplicateNameOrFriendlyURL = City::model()->exists(
                '((name = :name AND state_id = :stateId) OR friendly_url = :friendlyUrl)',
                array(
                    ':name' => $this->name,
                    ':stateId' => $this->state_id,
                    ':friendlyUrl' => $this->friendly_url
                )
            );
        }

        // was it a duplicate?
        if ($duplicateNameOrFriendlyURL) {
            // yes, exit
            $this->addError('name', 'Duplicate name or friendly URL');
            $this->addError('friendly_url', 'Duplicate name or friendly URL');
            return false;
        }

        return parent::beforeSave();
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {

        // avoid deleting from city table if associated records exist in location table
        $has_associated_records = Location::model()->exists('city_id=:city_id', array(':city_id' => $this->id));
        if ($has_associated_records) {
            $this->addError('id', 'This city cannot be deleted because it has locations in it.');
            return false;
        }
        return parent::beforeDelete();
    }

    /**
     * Returns the friendly url of a given city
     *
     * @param string $cityName
     * @param int $cityId
     * @return string
     */
    public static function getFriendlyUrl($cityName, $cityId = 0)
    {
        $url = StringComponent::prepareNewFriendlyUrl($cityName);
        return StringComponent::getNewFriendlyUrl($url, $cityId, 'city');
    }

    /**
     * Update de-normalized city data in location table
     * Update PDI with local changes
     * @return boolean
     */
    public function afterSave()
    {
        if (!$this->isNewRecord) {

            // update location
            $arrLocation = Location::model()->findAll('city_id=:city_id', array(':city_id' => $this->id));
            foreach ($arrLocation as $location) {
                $location->city_name = trim($this->name);
                $location->save();
            }
        }

        return parent::afterSave();
    }

    /* copied */

    /**
     * Returns name and friendly url of all the cities in a given state
     * @param int $stateId
     * @return array
     */
    public function getCitiesByState($stateId)
    {

        $sql = sprintf(
            "SELECT SQL_NO_CACHE city.id, city.name, city.friendly_url
            FROM city
            WHERE state_id = %d
            ORDER BY city.name;",
            $stateId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }

    /**
     * Return location and state by cityId
     * @param int $cityId
     * @return array
     */
    public static function getLocationStatebyCityId($cityId = 0)
    {
        $sql = sprintf(
            "SELECT c.name AS city_name, s.abbreviation AS state_ab,
            s.country_id, l.id AS location_id
            FROM city AS c
            LEFT JOIN `state` AS s ON c.state_id = s.id
            LEFT JOIN `location` AS l ON l.city_id = c.id
            WHERE c.id = %d LIMIT 1;",
            $cityId
        );
        $result = Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
        if (!empty($result)) {
            return $result[0];
        }
    }

    /**
     * Returns top 5 cities with the most providers accepting a given insurance
     * @param int $insuranceId
     * @return array
     */
    public function getTopCitiesByInsurance($insuranceId)
    {
        $criteria['insurance.company_id'] = $insuranceId;
        return $this->getTopCitiesByState(null, $criteria, 5);
    }

    /**
     * Returns the top 5 cities, per amount of providers given a specialty
     * @param int $specialtyId
     * @return array
     */
    public function getTopCitiesBySpecialty($specialtyId)
    {

        $criteria['specialty.specialty_id'] = $specialtyId;
        return $this->getTopCitiesByState(null, $criteria, 5);
    }

    /**
     * Get cities with the most providers in a given state for a given insurance
     * @param int $stateId
     * @param int $insuranceId
     * @return array
     */
    public function getTopCitiesByStateInsurance($stateId, $insuranceId)
    {
        $criteria['insurance.company_id'] = $insuranceId;
        return $this->getTopCitiesByState($stateId, $criteria);
    }

    /**
     * Get cities with the most providers in a given state for a given specialty
     * @param int $stateId
     * @param int $specialtyId
     * @return array
     */
    public function getTopCitiesByStateSpecialty($stateId, $specialtyId)
    {
        $criteria['specialty.specialty_id'] = $specialtyId;
        return $this->getTopCitiesByState($stateId, $criteria);
    }

    /**
     * Return top cities by number of providers in a state+specialty+insurance
     * @param int $stateId
     * @param int $specialtyId
     * @param int $insuranceId
     * @return array
     */
    public function getTopCitiesByStateSpecialtyInsurance($stateId, $specialtyId, $insuranceId)
    {
        $criteria['specialty.specialty_id'] = $specialtyId;
        $criteria['insurance.company_id'] = $insuranceId;
        return $this->getTopCitiesByState($stateId, $criteria);
    }

    /**
     * Return top cities by number of providers based on criteria (optionally within a particular state)
     * @param int $stateId
     * @param array $criteria
     * @param int $maxResultSize
     * @return array
     */
    private function getTopCitiesByState($stateId, $criteria, $maxResultSize = 999)
    {
        if ($stateId) {
            $criteria['practice.state_id'] = $stateId;
        }

        // DEPRECATE when killing ELS 1.5 instance
        if (Yii::app()->params->elasticsearch_version == '1.5') {
            // elasticSearch 1.5 Code
            $topHits = DoctorIndex15::topHits($criteria, 'practice.city_id', $maxResultSize);
        } else {
            // elasticSearch 7.8 Code
            $topHits = DoctorIndex78::topHitsAggregate($criteria, 'practice.city_id', $maxResultSize);
        }

        $cityIds = array_column($topHits, 'key');

        if ($stateId) {
            $citiesInState = $this->getCitiesByState($stateId);
            $cityIds = array_intersect($cityIds, array_column($citiesInState, 'id'));
        }

        if (empty($cityIds)) {
            return array();
        }

        $cityIds = array_filter($cityIds);

        $sql = sprintf(
            "SELECT SQL_NO_CACHE city.id AS city_id, state_id, `state`.abbreviation,
            city.friendly_url, city.name, city.visits_number
            FROM city
            INNER JOIN `state`
            ON city.state_id = `state`.id
            WHERE city.id IN (%s)
            ORDER BY FIELD(city_id, %s);",
            implode(',', $cityIds),
            implode(',', $cityIds)
        );
        return Yii::app()->db->cache(86400)->createCommand($sql)->queryAll();
    }

    /**
     * Get cities to be shown in the footer of most pages
     * @return array
     */
    public static function getCitiesForFooter()
    {
        $sql = "SELECT SQL_NO_CACHE CONCAT(city.name, ', ', abbreviation) AS city,
            CONCAT(`state`.friendly_url, '/Doctors-', city.friendly_url, '-', `state`.abbreviation) AS friendly
            FROM city
            INNER JOIN `state`
            ON city.state_id = `state`.id
            WHERE city.friendly_url != ''
            AND `state`.friendly_url != ''
            AND city.id IN
            (54, 78, 280, 73, 23, 2, 164, 94, 274, 311, 457, 200, 30, 86, 824, 354, 148, 49, 18, 19)
            ORDER BY
            city.id = 54 DESC, city.id = 78 DESC, city.id = 280 DESC, city.id = 73 DESC, city.id = 23 DESC,
            city.id = 2 DESC, city.id = 164 DESC, city.id = 94 DESC, city.id = 274 DESC, city.id = 311 DESC,
            city.id = 457 DESC, city.id = 200 DESC, city.id = 30 DESC, city.id = 86 DESC, city.id = 824 DESC,
            city.id = 354 DESC, city.id = 148 DESC, city.id = 49 DESC, city.id = 18 DESC, city.id = 19 DESC;";
        return Yii::app()->db->cache(Yii::app()->params['cache_long'])->createCommand($sql)->queryAll();
    }

    /**
     * Checks if notifications should be sent to partners for the current record.
     * @return boolean
     */
    public function pdiDisabled()
    {
        if (isset($this->state->country_id) && $this->state->country_id != 1) {
            return true;
        }
        return false;
    }

    /**
     * Find city, if not exists.. create
     * param arr $detectedLocation
     * Ex: Array (
     *      [detected_ip] => 186.59.9.5
     *      [country_code] => AR
     *      [country_name] => ARGENTINA
     *      [state_name] => MENDOZA
     *      [city_name] => MENDOZA
     *      [zipcode] => 5500
     *      [latitude] => -32.8908
     *      [longitude] => -68.8272
     *      [timezone] => -03:00
     *  )
     * @return arr
     */
    public static function findOrCreateCity($detectedLocation = '')
    {
        if (empty($detectedLocation)) {
            return '';
        }
        $countryCode = $detectedLocation['country_code'];
        $countryName = $detectedLocation['country_name'];
        $stateName = $detectedLocation['state_name'];
        $cityName = $detectedLocation['city_name'];
        $zipcode = $detectedLocation['zipcode'];
        $latitude = $detectedLocation['latitude'];
        $longitude = $detectedLocation['longitude'];

        // prepare stateAbbreviation
        if ($countryCode != 'US') {
            // Ex: Santa Fe -> SF
            $abb = $detectedLocation['state_name'];
            $arrAbb = explode(' ', $abb);
            if (count($arrAbb) > 1) {
                $abb = substr($arrAbb[0], 0, 1) . end($arrAbb);
            }
            $stateAbbreviation = substr($abb, 0, 2);
        } else {
            $stateAbbreviation = substr($detectedLocation['state_name'], 0, 2);
        }

        // find country
        $arrCountry = Country::model()->find('code=:code', array(':code' => $countryCode));
        if (empty($arrCountry)) {
            // if not exists, create
            $arrCountry = new Country();
            $arrCountry->name = $countryName;
            $arrCountry->code = $countryCode;
            $arrCountry->default_localization_id = 1;
            $arrCountry->save();
        }
        $countryId = intval($arrCountry->id);
        if ($countryId < 1) {
            return '';
        }

        // find state
        $arrState = State::model()->find(
            'country_id=:countryId AND name=:name',
            array(':countryId' => $countryId, ':name' => $stateName)
        );
        if (empty($arrState)) {
            // if not exists, create
            $arrState = new State();
            $arrState->country_id = $countryId;
            $arrState->name = $stateName;
            $arrState->abbreviation = $stateAbbreviation;
            $arrState->friendly_url = $countryCode . '_' . preg_replace("/[^a-zA-Z0-9]/", "_", $stateName);
            $arrState->save();
        }
        $stateId = intval($arrState->id);
        if ($stateId < 1) {
            return '';
        }

        // find city
        $arrCity = City::model()->find(
            'state_id=:stateId AND name =:cityName',
            array(':stateId' => $stateId, ':cityName' => $cityName)
        );
        if (empty($arrCity)) {
            // if not exists, create
            $arrCity = new City();
            $arrCity->state_id = $stateId;
            $arrCity->name = $cityName;
            $arrCity->friendly_url = $arrState->abbreviation . '_' . preg_replace("/[^a-zA-Z0-9]/", "_", $cityName);
            $arrCity->save();
        }
        $cityId = intval($arrCity->id);
        if ($cityId < 1) {
            return '';
        }

        // find location
        if (empty($zipcode)) {
            $arrLocation = Location::model()->find('city_id=:cityId', array(':cityId' => $cityId));
        } else {
            $arrLocation = Location::model()->find(
                'city_id=:cityId AND zipcode=:zipcode',
                array(':cityId' => $cityId, ':zipcode' => $zipcode)
            );
        }
        $locationId = 0;
        $locationZipCode = '';
        if (empty($arrLocation)) {
            if (!empty($zipcode)) {
                $arrLocation = new Location();
                $arrLocation->city_id = $cityId;
                $arrLocation->city_name = $cityName;
                $arrLocation->zipcode = $zipcode;
                $arrLocation->latitude = $latitude;
                $arrLocation->longitude = $longitude;
                $arrLocation->save();

                $locationId = $arrLocation->id;
                $locationZipCode = $zipcode;
            }
        } else {
            $locationId = $arrLocation->id;
            $locationZipCode = $arrLocation->zipcode;
        }

        $return['country_code'] = $arrCountry->code;
        $return['country_name'] = $arrCountry->name;
        $return['country_id'] = $arrCountry->id;
        $return['city_id'] = $arrCity->id;
        $return['city_name'] = $arrCity->name;
        $return['state_id'] = $arrState->id;
        $return['state_name'] = $arrState->name;
        $return['state_abb'] = $arrState->abbreviation;
        $return['location_id'] = $locationId;
        $return['location_zipcode'] = $locationZipCode;

        return $return;
    }
}
