<?php

Yii::import('application.modules.core_models.models._base.BaseUnsubscribeEmail');

class UnsubscribeEmail extends BaseUnsubscribeEmail
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'email';
    }

    public static function label($n = 1)
    {
        return Yii::t('app', 'Unsubscribe Email|Unsubscribe Email', $n);
    }

    /**
     * Validations before Save
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->isNewRecord) {
            $this->date_added = date("Y-m-d H:i:s");
        }
        return parent::beforeSave();
    }

    /**
     * get config from unsubscribe table
     * @param string $encodeEmail = md5('Doctor . 'email@patients.com')
     * @return array
     */
    public static function getPatientConfig($encodeEmail = '')
    {
        if (empty($encodeEmail)) {
            return false;
        }
        $salt = Yii::app()->params['saltEncodingEmail'];

        // Looking for in unsubscribe table
        $sql = "SELECT id, email, config
            FROM unsubscribe_email
            WHERE MD5(CONCAT(:salt, email)) = :encodeEmail
            LIMIT 1;";

        $result = Yii::app()->db->cache('CACHE_SHORT')->createCommand($sql)->
            bindParam(":salt", $salt, PDO::PARAM_STR)->
            bindParam(":encodeEmail", $encodeEmail, PDO::PARAM_STR)->queryRow();

        if (!$result) {
            // Looking for in patient table
            $sql = "SELECT 0 AS id, email, '' AS config
                FROM patient
                WHERE MD5(CONCAT(:salt, email)) = :encodeEmail
                LIMIT 1;";

            $result = Yii::app()->db->cache('CACHE_SHORT')->createCommand($sql)->
                bindParam(":salt", $salt, PDO::PARAM_STR)->
                bindParam(":encodeEmail", $encodeEmail, PDO::PARAM_STR)->queryRow();
        }

        if (!$result) {
            // Looking for in patientemail table
            $sql = "SELECT 0 AS id, patient_email AS email, '' AS config
                FROM patientemail
                WHERE MD5(CONCAT(:salt, patient_email)) = :encodeEmail
                LIMIT 1;";

            $result = Yii::app()->db->cache('CACHE_SHORT')->createCommand($sql)->
                bindParam(":salt", $salt, PDO::PARAM_STR)->
                bindParam(":encodeEmail", $encodeEmail, PDO::PARAM_STR)->queryRow();
        }

        return $result;
    }

    /*
     * Return true or false depending is this email is unsubscribed for this template
     * @use: $result = UnsubscribeEmail::isUnsubscribed('fernandodolci@gmail.com', 43);
     * @param string $email
     * @param int $templateId
     * @return bool
     */
    public static function isUnsubscribed($email = '', $templateId = 0)
    {
        if (empty($email) || empty($templateId)) {
            return false;
        }

        $salt = Yii::app()->params['saltEncodingEmail'];
        $email = StringComponent::validateEmail($email);
        $encodeEmail = md5($salt . $email);

        $arrConfig = UnsubscribeEmail::model()->getPatientConfig($encodeEmail);
        if (empty($arrConfig) || empty($arrConfig['config'])) {
            return false;
        }
        $listIds = explode(',', $arrConfig['config']);
        if (in_array($templateId, $listIds)) {
            // Is unsubscribed from this template
            return true;
        }
        return false;

    }

}
