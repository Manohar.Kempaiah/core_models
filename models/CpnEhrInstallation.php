<?php

Yii::import('application.modules.core_models.models._base.BaseCpnEhrInstallation');

class CpnEhrInstallation extends BaseCpnEhrInstallation
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * get information on EmrType for this account
     * @param type $accountId
     * @return array
     */
    public static function getInfo($accountId = 0)
    {
        if ($accountId == 0) {
            return false;
        }

        $sql = sprintf(
            "SELECT ce.id AS emr_type_id, ce.name, 'Standalone' AS `type`, NULL AS driver, ce.logo_path,
            1 AS mi7_integration, ce.read_only_integration,
            0 AS overbooking, 0 AS sync_exceptions
            FROM account a
                LEFT JOIN cpn_installation i ON a.id = i.account_id
                LEFT JOIN cpn_ehr_installation cei ON cei.cpn_installation_id = i.id
                LEFT JOIN cpn_ehr ce ON ce.id = cei.cpn_ehr_id and i.enabled_flag =1 and i.del_flag=0
                LEFT JOIN organization_account oa ON oa.account_id = a.id
                LEFT JOIN organization_account oa2 ON oa.organization_id = oa2.organization_id
            WHERE i.account_id = '%d' OR oa2.account_id = '%d' ORDER BY ce.id DESC LIMIT 1;",
            $accountId,
            $accountId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryRow();

    }

    /**
     * Find by Cpn Installation ID
     * @param $cpnInstallationId
     * @return mixed
     */
    public static function findByCpnInstallationId($cpnInstallationId)
    {
        return static::model()->cache(Yii::app()->params['cache_long'])->find(
            'cpn_installation_id = :cpn_installation_id AND enabled_flag = 1 AND del_flag = 0 ORDER BY id ASC',
            array(':cpn_installation_id' => $cpnInstallationId)
        );
    }
}
