<?php

Yii::import('application.modules.core_models.models._base.BaseWaitingRoomGuest');

class WaitingRoomGuest extends BaseWaitingRoomGuest
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
}
