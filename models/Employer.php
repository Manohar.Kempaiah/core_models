<?php

Yii::import('application.modules.core_models.models._base.BaseEmployer');

class Employer extends BaseEmployer
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Show employer suggestions in autocomplete
     * Used by AutocompleteController.php
     * @param string $q
     * @param int $limit
     * @return array
     */
    public function autocomplete($q, $limit)
    {

        $connection = Yii::app()->db;

        $q = trim($q);

        $sql = sprintf(
            "SELECT DISTINCT employer_id, network_id, name, alt_name
            FROM employer
            WHERE name LIKE '%%%s%%' OR alt_name LIKE '%%%s%%'
            ORDER BY name ASC
            LIMIT %u;",
            $q,
            $q,
            $limit
        );

        return $connection->createCommand($sql)->queryAll();
    }

}
