<?php

Yii::import('application.modules.core_models.models._base.BaseWaitingRoomGuestQueue');

class WaitingRoomGuestQueue extends BaseWaitingRoomGuestQueue
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * handle logic for setting the queue
     */
    public function afterSave()
    {

        if (!$this->waiting_room_queue_id) {
            return parent::afterSave();
        }

        $waitingRoomQueue = $this->waitingRoomQueue;

        // Every change in the queue clear the waiting Room telemedicine cache for this setting->id
        // params: name, value, time (in seconds)
        $waitingRoomInvite = $waitingRoomQueue->waitingRoomInvite;
        Yii::app()->cache->set("telemedicine_" . $waitingRoomInvite->waiting_room_setting_id, json_encode(''), 300);

    }
}
