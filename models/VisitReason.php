<?php

Yii::import('application.modules.core_models.models._base.BaseVisitReason');

class VisitReason extends BaseVisitReason
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Return visit reason name
     * @param int $visitReasonId
     * @return str $name
     */
    public static function getVisitReasonName($visitReasonId)
    {
        $visitReasonObj = VisitReason::model()->findByPk($visitReasonId);
        return addslashes($visitReasonObj->name);
    }

    /**
     * Return provider practice visitReason
     * @param int $accountId
     * @param int $providerId
     * @param int $practiceId
     * @param bool $cache
     * @return arr
     */
    public static function getProviderPracticeVisitReason(
        $accountId = 0,
        $providerId = 0,
        $practiceId = 0,
        $cache = true
        )
    {

        $cacheTime = $cache ? Yii::app()->params['cache_short'] : 0;

        // get visitReason related to providers specialties
        $providerSpecialties = SpecialtyVisitReason::getProviderSpecialties($providerId);

        // get visitReason related to all_specialties
        $sql = sprintf(
            "SELECT id AS visit_id, name AS visit_name, standard_visit_duration AS visit_duration
            FROM visit_reason
            WHERE all_specialties = 1
                AND ISNULL(account_id)
                AND status = 0
            ORDER BY visit_name ASC"
        );
        $stdVisitReason = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();

        // get Private visitReason related to all_specialties
        $sql = sprintf(
            "SELECT id AS visit_id, name AS visit_name, standard_visit_duration AS visit_duration
            FROM visit_reason
            WHERE all_specialties = 1 AND account_id = %d AND status = 0
            ORDER BY visit_name ASC",
            $accountId
        );
        $privateVisitReason = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();

        // formating output array
        $visitReason = array();
        if ($providerSpecialties) {
            foreach ($providerSpecialties as $providerSpecialty) {
                $formattedRecord = array();
                $formattedRecord['visit_name'] = $providerSpecialty['visit_name'];
                $formattedRecord['visit_duration'] = $providerSpecialty['visit_duration'];
                $formattedRecord['account_id'] = 0;
                $formattedRecord['specialty'] = trim($providerSpecialty['specialty_name'])
                    . ' - ' . trim($providerSpecialty['sub_name']);
                $formattedRecord['check'] = 0;
                $visitReason[$providerSpecialty['visit_id']] = $formattedRecord;
            }
        }
        if ($stdVisitReason) {
            foreach ($stdVisitReason as $eachVisitReason) {
                $formattedRecord = array();
                $formattedRecord['visit_name'] = $eachVisitReason['visit_name'];
                $formattedRecord['visit_duration'] = $eachVisitReason['visit_duration'];
                $formattedRecord['account_id'] = 0;
                $formattedRecord['specialty'] = '';
                $formattedRecord['check'] = 0;
                $visitReason[$eachVisitReason['visit_id']] = $formattedRecord;
            }
        }
        if ($privateVisitReason) {
            foreach ($privateVisitReason as $eachVisitReason) {
                $formattedRecord = array();
                $formattedRecord['visit_name'] = $eachVisitReason['visit_name'];
                $formattedRecord['visit_duration'] = $eachVisitReason['visit_duration'];
                $formattedRecord['account_id'] = $accountId;
                $formattedRecord['specialty'] = '';
                $formattedRecord['check'] = 0;
                $visitReason[$eachVisitReason['visit_id']] = $formattedRecord;
            }
        }

        // get own visitReason related to this provider/practice
        $sql = sprintf(
            "SELECT provider_practice_visit_reason.id AS visit_id, visit_reason_id, appointment_visit_duration AS
            visit_duration, visit_reason.name AS visit_name, visit_reason.account_id AS account_id
            FROM provider_practice_visit_reason
            LEFT JOIN visit_reason
            ON visit_reason_id = visit_reason.id
            LEFT JOIN practice_details
            ON provider_practice_visit_reason.practice_id = practice_details.practice_id
            WHERE provider_practice_visit_reason.provider_id = %d
            AND (provider_practice_visit_reason.practice_id = %d)
            GROUP BY visit_reason_id;",
            $providerId,
            $practiceId
        );
        $ownVisitReason = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();

        // merge with all visitReason
        if (!empty($ownVisitReason)) {
            foreach ($ownVisitReason as $ovr) {
                $vrId = $ovr['visit_reason_id'];
                if (isset($visitReason[$vrId])) {
                    $visitReason[$vrId]['visit_duration'] = $ovr['visit_duration'];
                    $visitReason[$vrId]['check'] = 1;
                } else {
                    $visitReason[$vrId] = $ovr;
                    $visitReason[$vrId]['check'] = 1;
                }

            }
        }

        return $visitReason;
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        // Delete associated provider_practice_visit_reason records
        $arrProviderPracticeVisitReason = ProviderPracticeVisitReason::model()->findAll(
            'visit_reason_id=:visitReasonId',
            array(':visitReasonId' => $this->id)
        );
        foreach ($arrProviderPracticeVisitReason as $ppw) {
            $ppw->delete();
        }

        return parent::beforeDelete();
    }

    /**
     * Save visit reasons
     * @param array $postData
     * @param int $accountId
     * @return array $results
     */
    public static function saveData($postData = array(), $accountId = 0)
    {
        $results['success'] = true;
        $results['error'] = '';
        $results['data'] = [];

        if (empty($postData) || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        // Expected data:
        //      "visit_reasons": {
        //            "1": {                         -> visit_reason_id
        //                "account_id": 0,
        //                "check": 1,
        //                "specialty": "",
        //                "visit_duration": 60,
        //                "visit_name": "Initial Consultation"
        //                "backend_action": "update"  -> update | delete | create
        //                "providers": {
        //                      "2933003": "1"       -> provider_id : "check" = 1
        //                      "2933004": "0"       -> provider_id : "check" = 0
        //                },
        //                "practices": {
        //                      "2933003": "1"       -> practice_id : "check" = 1
        //                      "2933004": "0"       -> practice_id : "check" = 0
        //                },
        //            },
        //        }

        $accountProvidersPractices = AccountProvider::getAccountProvidersPracticesAPI($accountId);

        foreach ($postData as $visitReasonId => $visitReason) {

            if ($visitReasonId == 1 || $visitReasonId == 2) {
                // This visitReason must be checked
                $visitReason['check'] = 1;
            }

            $visitReason['check'] = isset($visitReason['check']) ? $visitReason['check'] : 0;
            $backendAction = !empty($visitReason['backend_action']) ? $visitReason['backend_action'] : '';
            if ($visitReason['check'] == 0 && $backendAction != 'delete') {
                $backendAction = 'deleteThis';
            }

            if ($visitReason['account_id'] == $accountId) {
                // exists this visit reason ?
                $vr = VisitReason::model()->find(
                    "id = :visitReasonId",
                    array(":visitReasonId" => $visitReasonId)
                );
                if (!empty($vr)) {

                    if (
                        $backendAction == 'update'
                        &&
                        (
                            $vr->name != $visitReason['visit_name']
                            ||
                            $vr->standard_visit_duration != $visitReason['visit_duration']
                        )
                    ) {
                        $vr->name = $visitReason['visit_name'];
                        $vr->standard_visit_duration = $visitReason['visit_duration'];
                        $vr->save();

                    } elseif ($backendAction == 'delete') {
                        // delete de visit reason itself
                        if (!$vr->delete()) {
                            $results['success'] = false;
                            $results['error'] = "";
                            $visitReason['status'] = "ERROR_DELETING_VISIT_REASONS";
                            $visitReason['error'] = $vr->getErrors();
                            $results['data'][$providerId][$practiceId][$visitReasonId] = $visitReason;
                        }
                        continue;
                    }
                }
            }

            if ($backendAction != 'delete') {

                $concatProviders = AccountProvider::getProvidersConcatByAccount($accountId);
                $concatPractices = AccountPractice::getPracticesConcatByAccount($accountId);

                // Delete associated provider_practice_visit_reason records
                $sql = sprintf(
                    "SELECT id FROM provider_practice_visit_reason
                    WHERE visit_reason_id = %d
                    AND provider_id IN (%s)
                    AND practice_id IN (%s);",
                    $visitReasonId,
                    $concatProviders,
                    $concatPractices
                );
                $arrProviderPracticeVisitReason = Yii::app()->db->createCommand($sql)->queryAll();
                foreach ($arrProviderPracticeVisitReason as $ppw) {
                    $pPVR = ProviderPracticeVisitReason::model()->findByPk($ppw['id']);
                    if ($pPVR) {
                        $pPVR->delete();
                    }
                }

                // insert all
                foreach ($accountProvidersPractices as $providersPractices) {

                    if (isset($visitReason['providers'][$providersPractices['provider_id']]) &&
                            isset($visitReason['practices'][$providersPractices['practice_id']])) {

                        $providerId = $providersPractices['provider_id'];
                        $practiceId = $providersPractices['practice_id'];

                        $backendActionPP = 'deleteThis';
                        if (isset($visitReason['providers'][$providersPractices['provider_id']])
                                && $visitReason['providers'][$providersPractices['provider_id']] == 1
                                && isset($visitReason['practices'][$providersPractices['practice_id']])
                                && $visitReason['practices'][$providersPractices['practice_id']] == 1) {
                            $backendActionPP = 'update';
                        }

                        $ppvr = ProviderPracticeVisitReason::model()->find(
                            "practice_id = :practiceId AND provider_id = :providerId "
                                . "AND visit_reason_id = :visitReasonId",
                            array(
                                ":practiceId" => $practiceId,
                                ":providerId" => $providerId,
                                ":visitReasonId" => $visitReasonId
                            )
                        );

                        if ($backendActionPP == 'update') {

                            if (empty($ppvr)) {
                                $ppvr = new ProviderPracticeVisitReason();
                                $ppvr->provider_id = $providerId;
                                $ppvr->practice_id = $practiceId;
                                $ppvr->visit_reason_id = $visitReasonId;
                            }
                            $ppvr->appointment_visit_duration = (int)$visitReason['visit_duration'];

                            if ($ppvr->save()) {
                                $results['success'] = true;
                                $results['error'] = "";
                                $visitReason['status'] = 'SAVED';
                            } else {
                                $results['success'] = false;
                                $results['error'] = "";
                                $visitReason['status'] = "ERROR_SAVING_VISIT_REASONS";
                                $visitReason['error'] = $ppvr->getErrors();
                            }

                            // enable telemedicine
                            if ($visitReason['enable_telemedicine_request']) {
                                $telVisitReasons = array();
                                $telVisitReasons[$visitReasonId]['account_id'] = $accountId;
                                $telVisitReasons[$visitReasonId]['check'] = 1;
                                $telVisitReasons[$visitReasonId]['providers'][0] = $providerId;

                                VisitReason::saveTelemedicineData($telVisitReasons, $accountId);
                            }

                            $results['data'][$providerId][$practiceId][$visitReasonId] = $visitReason;
                        }
                    }
                }
            }
        }

        AuditLog::create('U', 'Visit reasons', null, '', false);

        return $results;
    }

    /**
     * Save telemedicine visit reasons
     * @param array $postData
     * @param int $accountId
     * @return array $results
     */
    public static function saveTelemedicineData($postData = array(), $accountId = 0)
    {
        $results['success'] = true;
        $results['error'] = '';
        $results['data'] = [];

        if (empty($postData) || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        /*
        array(1) {
            [38]=>
            array(3) {
                ["account_id"]=>
                NULL
                ["check"]=>
                int(1)
                ["providers"]=>
                array(1) {
                [0]=>
                string(7) "2933003"
                }
            }
        }
        */

        // get providers in this account
        $accountProviders = AccountProvider::getAccountProviders($accountId);

        // go through each visit reason
        foreach ($postData as $visitReasonId => $value) {

            $arrProviders = null;

            // go through each provider that's present in the configuration
            foreach ($value['providers'] as $providerId) {

                $arrProviders[] = $providerId;

                // Check if i have access to this record
                $allowAccess = false;
                foreach ($accountProviders as $v) {

                    if ($v['provider_id'] == $providerId) {
                        $allowAccess = true;
                        break;
                    }
                }
                if (!$allowAccess) {
                    $results['success'] = false;
                    $results['error'] = "ACCESS_DENIED";
                    return $results;
                }

                $forceCheck = false;
                if ($visitReasonId == 1 || $visitReasonId == 2) {
                    // This visitReason must be checked
                    $forceCheck = true;
                }

                $visitReason['check'] = $forceCheck || (isset($value['check']) && $value['check'] == 1);

                // get telemedicine practice
                $arrPracticeTelemed = ProviderPractice::getTelemedicinePractice($providerId);

                // yes, make sure we have a practice
                if (empty($arrPracticeTelemed)) {
                    $arrPracticeTelemed = ProviderPractice::getOrCreateTelemedicinePractice(
                        $providerId,
                        $accountId,
                        ['enabled' => 1, 'accept_new_patients' => 1]
                    );
                }
                $practiceId = $arrPracticeTelemed['practice_id'];

                // did we already have this association?
                $ppvr = ProviderPracticeVisitReason::model()->find(
                    'provider_id=:providerId AND practice_id=:practiceId AND visit_reason_id=:visitReasonId',
                    [
                        ":providerId" => $providerId,
                        ":practiceId" => $practiceId,
                        ":visitReasonId" => $visitReasonId
                    ]
                );

                // is the visit reason checked for telemedicine?
                if ($visitReason['check']) {
                    // yes
                    if (!$ppvr) {
                        // association didn't exist, create it
                        $ppvr = new ProviderPracticeVisitReason();
                        $ppvr->provider_id = $providerId;
                        $ppvr->practice_id = $practiceId;
                        $ppvr->visit_reason_id = $visitReasonId;
                        if (!$ppvr->save()) {
                            $results['success'] = false;
                            $results['error'] = "ERROR_SAVING_VISIT_REASONS";
                            return $results;
                        }
                    }
                } elseif ($ppvr) {
                    // no, remove telemedicine setting
                    if (!$ppvr->delete()) {
                        $results['success'] = false;
                        $results['error'] = "ERROR_DELETING_VISIT_REASONS";
                        return $results;
                    }
                }
            } // end for each selected provider

            // the rest of the providers shouldn't have this visit reason, remove them
            $arrVisitReasonProviders = $arrProviders;
            foreach ($accountProviders as $providerId) {
                if (array_search($providerId, $arrVisitReasonProviders) === false) {
                    // remove the provider

                    // get telemedicine practice
                    $arrPracticeTelemed = ProviderPractice::getTelemedicinePractice($providerId);
                    if (empty($arrPracticeTelemed)) {
                        continue;
                    }
                    $practiceId = $arrPracticeTelemed['practice_id'];

                    // did we find one?
                    if (!empty($practiceId)) {
                        // yes -- did we already have this association?
                        $ppvr = ProviderPracticeVisitReason::model()->find(
                            'provider_id=:providerId AND practice_id=:practiceId AND visit_reason_id=:visitReasonId',
                            [
                                ":providerId" => $providerId,
                                ":practiceId" => $practiceId,
                                ":visitReasonId" => $visitReasonId
                            ]
                        );
                        if (!empty($ppvr) && !$ppvr->delete()) {
                            // yes, delete it
                            $results['success'] = false;
                            $results['error'] = "";
                            $visitReason['status'] = "ERROR_DELETING_VISIT_REASONS";
                            $visitReason['error'] = $ppvr->getErrors();
                        }
                    }
                }

            } // end for each account provider

        } // end for each visit reason

        AuditLog::create('U', 'Telemedicine visit reasons', null, '', false);

        $results['success'] = true;
        $results['error'] = "";
        return $results;
    }

    /**
     * New Visit Reason
     * @param array $postData
     * @param int $accountId
     * @return array $results
     */
    public static function newVisitReason($postData = array(), $accountId = 0)
    {
        $results['success'] = true;
        $results['error'] = '';
        $results['data'] = [];

        if (empty($postData) || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        $accountProvidersPractices = AccountProvider::getAccountProvidersPracticesAPI($accountId);

        foreach ($postData as $value) {

            $practiceId = isset($value['practice_id']) ? $value['practice_id'] : 0;
            $providerId = $value['provider_id'];
            $allowAccess = false;
            foreach ($accountProvidersPractices as $v) {
                if ($v['practice_id'] == $practiceId || $v['provider_id'] == $providerId) {
                    $allowAccess = true;
                    break;
                }
            }

            if (!$allowAccess) {
                $results['success'] = false;
                $results['error'] = "ACCESS_DENIED";
                return $results;
            }

            if (!empty($value['vr_name'])) {

                $newVisitReasonName = trim($value['vr_name']);
                $newVisitReasonDuration = intval($value['vr_duration']);
                $newVisitReasonScope = intval($value['vr_scope']);

                $exists = VisitReason::model()->find(
                    "account_id = :accountId AND name = :visitReasonName",
                    array(":accountId" => $accountId, ":visitReasonName" => $newVisitReasonName)
                );
                $resultSave = '';
                if (empty($exists)) {

                    $visitReason = new VisitReason();
                    $visitReason->all_specialties = 1;
                    $visitReason->account_id = $accountId;
                    $visitReason->name = $newVisitReasonName;
                    $visitReason->status = 1;
                    $visitReason->standard_visit_duration = $newVisitReasonDuration;
                    $resultSave = $visitReason->save();

                    $visitReasonId = intval($visitReason->id);
                    if ($visitReasonId > 0) {
                        // Associate this visitReason with:
                        switch ($newVisitReasonScope) {
                            case 1: // provider-practice-level (Only for this provider in this practice)

                                $exists = ProviderPracticeVisitReason::model()->exists(
                                    'provider_id=:providerId AND practice_id=:practiceId AND '
                                        . 'visit_reason_id=:visitReasonId',
                                    array(
                                        ':providerId' => $providerId,
                                        ':practiceId' => $practiceId,
                                        ':visitReasonId' => $visitReasonId)
                                    );

                                if (!$exists) {
                                    $ppvr = new ProviderPracticeVisitReason();
                                    $ppvr->provider_id = $providerId;
                                    $ppvr->practice_id = $practiceId;
                                    $ppvr->visit_reason_id = $visitReasonId;
                                    $ppvr->appointment_visit_duration = $newVisitReasonDuration;
                                    $resultSave = $ppvr->save();
                                    $r['account_id'] = $accountId;
                                    $r['check'] = 1;
                                    $r['specialty'] = "";
                                    $r['visit_duration'] = $newVisitReasonDuration;
                                    $r['visit_name'] = $newVisitReasonName;
                                    $returnData[$providerId][$practiceId][$visitReasonId] = $r;
                                }
                                break;

                            case 2: // provider-level (This provider, all the practices)

                                foreach ($accountProvidersPractices as $pl) {
                                    if ($pl['provider_id'] == $providerId) {
                                        $exists = ProviderPracticeVisitReason::model()->exists(
                                            'provider_id=:providerId AND practice_id=:practiceId AND '
                                                . 'visit_reason_id=:visitReasonId',
                                            array(
                                                ':providerId' => $providerId,
                                                ':practiceId' => $pl['practice_id'],
                                                ':visitReasonId' => $visitReasonId)
                                            );

                                        if (!$exists) {
                                            $ppvr = new ProviderPracticeVisitReason();
                                            $ppvr->provider_id = $providerId;
                                            $ppvr->practice_id = $pl['practice_id'];
                                            $ppvr->visit_reason_id = $visitReasonId;
                                            $ppvr->appointment_visit_duration = $newVisitReasonDuration;
                                            $resultSave = $ppvr->save();

                                            $r['account_id'] = $accountId;
                                            $r['check'] = 1;
                                            $r['specialty'] = "";
                                            $r['visit_duration'] = $newVisitReasonDuration;
                                            $r['visit_name'] = $newVisitReasonName;
                                            $returnData[$ppvr->provider_id][$ppvr->practice_id][$visitReasonId] = $r;
                                        }
                                    }
                                }
                                break;

                            case 3: // practice-level (This practice, all the providers)
                                foreach ($accountProvidersPractices as $pl) {
                                    if ($pl['practice_id'] == $practiceId) {
                                        $exists = ProviderPracticeVisitReason::model()->exists(
                                            'provider_id=:providerId AND practice_id=:practiceId AND '
                                                . 'visit_reason_id=:visitReasonId',
                                            array(
                                                ':providerId' => $pl['provider_id'],
                                                ':practiceId' => $practiceId,
                                                ':visitReasonId' => $visitReasonId)
                                        );
                                        if (!$exists) {
                                            $ppvr = new ProviderPracticeVisitReason();
                                            $ppvr->provider_id = $pl['provider_id'];
                                            $ppvr->practice_id = $practiceId;
                                            $ppvr->visit_reason_id = $visitReasonId;
                                            $ppvr->appointment_visit_duration = $newVisitReasonDuration;
                                            $ppvr->save();

                                            $r['account_id'] = $accountId;
                                            $r['check'] = 1;
                                            $r['specialty'] = "";
                                            $r['visit_duration'] = $newVisitReasonDuration;
                                            $r['visit_name'] = $newVisitReasonName;
                                            $returnData[$ppvr->provider_id][$ppvr->practice_id][$visitReasonId] = $r;
                                        }
                                    }
                                }
                                $resultSave = true;
                                break;

                            case 4: // account-level (All the providers and practices from this account)
                                foreach ($accountProvidersPractices as $value) {

                                    $exists = ProviderPracticeVisitReason::model()->exists(
                                        'provider_id = :providerId AND practice_id = :practiceId AND'
                                            . ' visit_reason_id = :visitReasonId',
                                        array(
                                            ':providerId' => $value['provider_id'],
                                            ':practiceId' => $value['practice_id'],
                                            ':visitReasonId' => $visitReasonId)
                                    );

                                    if (!$exists) {
                                        $ppvr = new ProviderPracticeVisitReason();
                                        $ppvr->provider_id = $value['provider_id'];
                                        $ppvr->practice_id = $value['practice_id'];
                                        $ppvr->visit_reason_id = $visitReasonId;
                                        $ppvr->appointment_visit_duration = $newVisitReasonDuration;
                                        $resultSave = $ppvr->save();

                                        $r['account_id'] = $accountId;
                                        $r['check'] = 1;
                                        $r['specialty'] = "";
                                        $r['visit_duration'] = $newVisitReasonDuration;
                                        $r['visit_name'] = $newVisitReasonName;
                                        $returnData[$ppvr->provider_id][$ppvr->practice_id][$visitReasonId] = $r;
                                    }
                                }
                                break;

                            case 5:
                                // provider-telemedicine-practice-level (Only for this provider in his telemedicine
                                // practice)

                                $telVisitReasons = array();
                                $telVisitReasons[$visitReasonId]['account_id'] = $accountId;
                                $telVisitReasons[$visitReasonId]['check'] = 1;
                                $telVisitReasons[$visitReasonId]['providers'][0] = $providerId;

                                $returnData = VisitReason::saveTelemedicineData($telVisitReasons, $accountId);
                                $resultSave = true;

                                break;
                            default:
                        }

                    }

                    if ($resultSave) {
                        $results['success'] = true;
                        $results['error'] = "";
                        $results['data'] = $returnData;
                    } else {
                        $results['success'] = false;
                        $results['error'] = "ERROR_SAVING_NEW_VISIT_REASONS";
                        $results['data'] =  $ppvr->attributes;
                    }

                } else {
                    $results['success'] = false;
                    $results['error'] = "ERROR_VISIT_REASONS_EXISTS";
                    $results['data'][] =  $exists;
                }

            } else {
                // If vr_name empty
                // Yes
                $results['success'] = false;
                $results['error'] = "ERROR_SAVING_VISIT_REASONS";
                $results['data'][] = "Visit reason name cannot be blank";
            }
        }
        return $results;

    }

    /**
     * Find visit reasons using sub specialty ID
     * @param $subSpecialtyId
     * @return mixed
     */
    public static function findBySpecialties($subSpecialtyId)
    {
        $sql = sprintf(
            "SELECT
              visit_reason.id,
              visit_reason.standard_visit_duration
            FROM visit_reason
            LEFT JOIN specialty_visit_reason ON
              specialty_visit_reason.visit_reason_id = visit_reason.id
            WHERE
              specialty_visit_reason.sub_specialty_id = %d",
            $subSpecialtyId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Delete Telemedicine Visit Reason
     * @param int $providerId
     * @param int $accountId
     * @param int $visitReasonId
     * @return mixed
     */
    public static function deleteTelemedicineVisitReason($providerId, $accountId, $visitReasonId)
    {
        $sql = sprintf(
            "SELECT pp.practice_id
            FROM provider_practice AS pp
            INNER JOIN account_provider AS apro
                ON pp.provider_id = apro.provider_id AND apro.account_id = %d
            INNER JOIN account_practice AS apra
                ON pp.practice_id = apra.practice_id  AND apra.account_id = %d
            LEFT JOIN provider
                ON provider.id = pp.provider_id
            LEFT JOIN practice
                ON practice.id = pp.practice_id
            LEFT JOIN practice_details
                ON practice.id = practice_details.practice_id
            WHERE practice_type_id = 5
            AND pp.provider_id = %d;",
            $accountId,
            $accountId,
            $providerId
        );
        $practice = Yii::app()->db->createCommand($sql)->queryAll();
        foreach ($practice as $pra) {
            $practiceId = $pra['practice_id'];
            $ppvr = ProviderPracticeVisitReason::model()->find(
                "practice_id = :practiceId AND provider_id = :providerId "
                    . "AND visit_reason_id = :visitReasonId",
                array(
                    ":practiceId" => $practiceId,
                    ":providerId" => $providerId,
                    ":visitReasonId" => $visitReasonId
                )
            );
            if (!empty($ppvr)) {
                $ppvr->delete();
            }
        }
    }

}
