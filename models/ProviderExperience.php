<?php

Yii::import('application.modules.core_models.models._base.BaseProviderExperience');

class ProviderExperience extends BaseProviderExperience
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    //called on rendering the column for each row
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id .
            '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">' .
            $this->provider . '</a>');
    }

    /**
     * BeforeSave provider Experience
     * @return boolean
     */
    public function beforeSave()
    {

        if ($this->from_date == 0 || $this->from_date == '') {
            $this->from_date = null;
        }
        if ($this->to_date == 0 || $this->to_date == '' || $this->to_date == 'Present' || $this->to_date == 'yyyy') {
            $this->to_date = null;
        }
        return parent::beforeSave();
    }

    /**
     * Return provider Experience
     * @param int $providerId
     * @return array
     */
    public function getDetails($providerId, $cache = false)
    {
        $sql = sprintf(
            "SELECT pe.id, pe.provider_id, pe.position, pe.organization, pe.from_date,
            pe.to_date, pe.description
            FROM provider_experience AS pe
            WHERE pe.provider_id = %d
            ORDER BY from_date, to_date DESC;",
            $providerId
        );

        $cacheTime = ($cache) ? Yii::app()->params['cache_short'] : 0;

        return Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();
    }

    /**
     * Save Data
     * @param string $postData
     * @param int $providerId
     * @return array $results
     */
    public static function saveData($postData = '', $providerId = 0)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }
        $results['success'] = true;
        $results['error'] = "";

        foreach ($postData as $data) {

            if (empty($data['position']) || empty($data['organization'])) {
                continue;
            }

            if (isset($data['backend_action']) && $data['backend_action'] == 'delete') {
                if ($data['id'] > 0) {
                    $model = ProviderExperience::model()->findByPk($data['id']);
                    if (!empty($model)) {
                        if (!$model->delete()) {
                            $results['success'] = false;
                            $results['error'] = "ERROR_DELETING_PROVIDER_EXPERIENCE";
                            $results['data'] = $model->getErrors();
                            return $results;
                        }
                        $auditSection = 'ProviderExperience #' . $providerId .' (' . $model->position . ')';
                        AuditLog::create('D', $auditSection, 'provider_experience', null, false);
                    }
                }
            } else {
                $model = null;
                if ($data['id'] > 0) {
                    $model = ProviderExperience::model()->findByPk($data['id']);
                    $auditAction = 'U';
                }
                if (empty($model)) {
                    $auditAction = 'C';
                    $model = new ProviderExperience;
                }
                $model->attributes = $data;
                $model->provider_id = $providerId;
                $model->to_date = (empty($model->from_date)) ? null : $model->to_date;

                if (!$model->save()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_EXPERIENCE";
                    $results['data'] = $model->getErrors();
                    return $results;
                } else {
                    if ($auditAction == 'C') {
                        // return the new item
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                }
                $auditSection = 'ProviderExperience #' . $providerId .' (#' . $model->position . ')';
                AuditLog::create($auditAction, $auditSection, 'provider_experience', null, false);

            }

        }
        return $results;
    }

}
