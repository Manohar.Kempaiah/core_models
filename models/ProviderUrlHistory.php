<?php

Yii::import('application.modules.core_models.models._base.BaseProviderUrlHistory');

class ProviderUrlHistory extends BaseProviderUrlHistory
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

}
