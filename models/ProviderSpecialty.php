<?php

Yii::import('application.modules.core_models.models._base.BaseProviderSpecialty');
// autoload vendors class
Common::autoloadVendors();

class ProviderSpecialty extends BaseProviderSpecialty
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Validates uniqueness of the provider_id|sub_specialty_id key
     * @return boolean
     */
    public function beforeValidate()
    {

        if ($this->isNewRecord) {
            // validate unique composite key on insert
            if (ProviderSpecialty::model()->exists(
                'provider_id=:provider_id AND sub_specialty_id=:sub_specialty_id',
                array(
                    ':provider_id' => $this->provider_id,
                    ':sub_specialty_id' => $this->sub_specialty_id)
                    )
                ) {
                $this->addError('sub_specialty_id', 'This provider already has this specialty.');
                return false;
            }
        } else {
            // validate unique composite key on update
            if (ProviderSpecialty::model()->exists(
                'provider_id=:provider_id AND sub_specialty_id=:sub_specialty_id AND id!=:id',
                array(
                    ':provider_id' => $this->provider_id,
                    ':sub_specialty_id' => $this->sub_specialty_id,
                    ':id' => $this->id
                ))
                ) {
                $this->addError('sub_specialty_id', 'This provider already has this specialty.');
                return false;
            }

        }
        return parent::beforeValidate();
    }

    /**
     * Update primary specialty value before saving
     * @return boolean
     */
    public function beforeSave()
    {
        // update the primary_specialty with a correct value if it's empty
        if (empty($this->primary_specialty)) {
            $sql = sprintf(
                'SELECT MAX(primary_specialty) FROM provider_specialty WHERE provider_id = %d',
                $this->provider_id
            );
            $primarySpecialty = Yii::app()->db->createCommand($sql)->queryScalar();
            $this->primary_specialty = $primarySpecialty + 1;
        }

        // Is a new record?
        if (!$this->isNewRecord && $this->oldRecord->sub_specialty_id != $this->sub_specialty_id) {
            // No
            // sub_specialty_id was changed?
            // Yes, was changed
            // delete related rows in provider_specialty_license
            $arrLicenses = ProviderSpecialtyLicense::model()->findAll(
                'provider_id = :provider_id AND sub_specialty_id = :sub_specialty_id',
                [':provider_id' => $this->provider_id, ':sub_specialty_id' => $this->oldRecord->sub_specialty_id]
            );

            foreach ($arrLicenses as $license) {
                if (!$license->delete()) {
                    $this->addError('id', 'Couldn\'t delete specialty license.');
                    return false;
                }
            }
        }

        return parent::beforeSave();
    }

    /**
     * After delete actions.
     * @return boolean
     */
    public function afterDelete()
    {
        // special logic for specialties associated to novartis
        if ($this->sub_specialty_id == 442 || $this->sub_specialty_id == 256) {
            // check if the provider already has one of both specialties assigned
            $exists = ProviderSpecialty::model()->exists(
                'provider_id = :provider_id AND sub_specialty_id IN (442, 56)',
                [':provider_id' => $this->provider_id]
            );

            // if not, all suggestions for novartis widget should be removed
            if (!$exists) {
                // get all suggestions for novartis widget
                $sql = "SELECT partner_site_entity_match.*
                    FROM partner_site_entity_match
                    WHERE partner_site_id = 76
                        AND match_column_name = 'location_id'
                        AND internal_column_value IN (
                            SELECT provider_practice.id
                            FROM provider_practice
                            WHERE provider_practice.provider_id = :provider_id
                        )";
                $matches = PartnerSiteEntityMatch::model()->findAllBySql($sql, [':provider_id' => $this->provider_id]);

                // remove each one (the daily job will add new relations)
                if ($matches) {
                    foreach ($matches as $m) {
                        $m->delete();
                    }
                }
            }
        }

        return parent::afterDelete();
    }

    /**
     * Called on rendering the column for each row in the administration
     * @return string
     */
    public function getProviderTooltip()
    {
        if (isset($this->provider_id)) {
            $dataReturn = sprintf(
                '<a class="providerTooltip" href="/administration/providerTooltip?id=%1$s" ' .
                'rel="/administration/providerTooltip?id=%1$s" title="%1$s">%1$s</a>',
                $this->provider
            );
            return ($dataReturn);
        } else {
            return '';
        }
    }

    /**
     * Called on rendering the column for each row in the administration
     * @return string
     */
    public function getSpecialtyAndSub()
    {
        if (isset($this->sub_specialty_id)) {
            $subspecialty = Subspecialty::model()->findByPk($this->sub_specialty_id);
            $specialty = Specialty::model()->findByPk($subspecialty->specialty_id);
            return $specialty['name'] . ' - ' . $this->subSpecialty;
        } else {
            return '';
        }
    }

    /**
     * Returns specialty details for a given provider
     * @param string $providerId provider id
     * @param bool $useCache
     * @return array
     */
    public static function getDetails($providerId, $useCache = true)
    {
        $sql = sprintf(
            "SELECT specialty.name AS specialty, sub_specialty.name AS sub_specialty, specialty.id AS specialty_id,
                sub_specialty.id AS sub_specialty_id, specialty.friendly_url, primary_specialty
            FROM provider_specialty
            INNER JOIN sub_specialty
            ON provider_specialty.sub_specialty_id = sub_specialty.id
            INNER JOIN specialty
            ON sub_specialty.specialty_id = specialty.id
            WHERE provider_id = %d
            ORDER BY provider_specialty.primary_specialty;",
            $providerId
        );

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;
        $arrProviderSpecialties = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();

        foreach ($arrProviderSpecialties as $key => $value) {
            // find licenses of each specialty
            $arrProviderSpecialtyLicense = ProviderSpecialtyLicense::model()->cache($cacheTime)->findAll(
                'provider_id = :provider_id AND sub_specialty_id = :sub_specialty_id',
                [':provider_id' => $providerId, ':sub_specialty_id' => $value['sub_specialty_id']]
            );

            foreach ($arrProviderSpecialtyLicense as $psl) {
                $license = $psl->attributes;
                $state = State::model()->cache($cacheTime)->findByPk($psl->state_id);
                $license['state'] = $state->attributes;
                $arrProviderSpecialties[$key]['license'][] = $license;
            }
        }

        return $arrProviderSpecialties;
    }

    /**
     * Gets primary specialty from a given provider id
     * @param int $providerId provider's id.
     * @return mixed
     */
    public static function getPrimarySpecialty($providerId)
    {
        if ($providerId > 0) {
            $sql = sprintf(
                'SELECT DISTINCT specialty.id AS id, specialty.name AS name,
                IF(provider_specialty.id IS NULL, false, true ) AS checked,
                provider_specialty.id AS provider_specialty_id
                FROM specialty
                INNER JOIN sub_specialty
                    ON sub_specialty.specialty_id = specialty.id
                INNER JOIN provider_specialty
                    ON provider_specialty.sub_specialty_id = sub_specialty.id
                AND provider_id = %d
                WHERE specialty.level = "V"
                ORDER BY primary_specialty, specialty.name ASC;',
                $providerId
            );
        } else {
            $sql = sprintf(
                "SELECT specialty.id AS id, specialty.name AS name,
                false AS checked
                FROM specialty
                WHERE specialty.level = 'V'
                ORDER BY specialty.name;"
            );
        }
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Gets primary medical Sub-Specialty from a given provider id and specialty id
     *
     * @param int $providerId
     * @param int $primaryId
     * @return mixed
     */
    public static function getPrimaryMedicalSubSpecialty($providerId, $primaryId)
    {
        if ($providerId > 0) {
            $sql = 'SELECT sub_specialty.id AS id, sub_specialty.name AS name,
                IF(provider_specialty.id IS NULL, false, true ) AS checked
                FROM sub_specialty
                LEFT JOIN provider_specialty
                ON provider_specialty.sub_specialty_id = sub_specialty.id
                AND provider_id = ' . $providerId . '
                WHERE sub_specialty.specialty_id = "' . $primaryId . '"
                ORDER BY provider_specialty.id IS NULL ASC, provider_specialty.primary_specialty,
                sub_specialty.name ASC;';
        } else {
            $sql = 'SELECT sub_specialty.id AS id, sub_specialty.name AS name,
                false AS checked
                FROM sub_specialty
                ORDER BY sub_specialty.name;';
        }

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the number of providers with a specialty
     * @param int $specialtyId
     * @return int
     */
    public static function getIndexCount($specialtyId)
    {
        $criteria['specialty.specialty_id'] = $specialtyId;
        return DoctorIndex::count($criteria);
    }

    /**
     * Delete from provider_specialty_license before deleting provider_specialty
     * @return boolean
     */
    public function beforeDelete()
    {
        // delete related rows in provider_specialty_license
        $arrLicenses = ProviderSpecialtyLicense::model()->findAll(
            'provider_id = :provider_id AND sub_specialty_id = :sub_specialty_id',
            [':provider_id' => $this->provider_id, ':sub_specialty_id' => $this->sub_specialty_id]
        );

        foreach ($arrLicenses as $license) {
            if (!$license->delete()) {
                $this->addError('id', 'Couldn\'t delete specialty license.');
                return false;
            }
        }

        return parent::beforeDelete();
    }

    /**
     * Gets all sub_specialties and specialty data from a given provider
     * @param int $providerId
     * @return array
     */
    public static function getSpecialties($providerId)
    {
        $sql = sprintf(
            'SELECT ps.*, ss.specialty_id FROM provider_specialty ps JOIN sub_specialty ss ON ss.id = ' .
                'ps.sub_specialty_id WHERE ps.provider_id = %d ORDER BY ps.primary_specialty',
            $providerId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Save all sub_specialties
     * @param int $accountId
     * @param int $providerId
     * @param array $arrSpecialties
     *
     * @return array $results
     */
    public static function saveSpecialties($accountId, $providerId, $arrSpecialties)
    {
        if ($providerId == 0) {
            $results['success'] = false;
            $results['error'] = "ERROR_EMPTY_DATA";
            return $results;
        }

        $results['success'] = true;
        $results['error'] = "";

        // check whether any of the provided specialties is primary
        $primaryInSet = false;
        foreach ($arrSpecialties as $specialty) {
            if (!empty($specialty['primary_specialty']) && intval($specialty['primary_specialty']) == 1) {
                $primaryInSet = true;
            }
        }

        if (!$primaryInSet) {
            // To mantain compatibility with PADM v1
            foreach ($arrSpecialties as $k => $specialty) {
                $isPrimary = 0;
                if ($k == 0) {
                    $isPrimary = 1;
                }

                if (!isset($arrSpecialties[$k]['primary_specialty'])) {
                    $arrSpecialties[$k]['primary_specialty'] = $isPrimary;
                    $primaryInSet = true;
                }

            }

        }

        if (!$primaryInSet) {
            $results['success'] = false;
            $results['error'] = "NO_PRIMARY_SPECIALTY";
            $results['data'] = array("One of the specialties needs to be the primary for this provider.");
            return $results;
        }

        // Find all practices where the provider is
        $sql = sprintf("SELECT practice_id FROM provider_practice WHERE provider_id = %d", $providerId);
        $providerPractices = Yii::app()->db
            ->cache(Yii::app()->params['cache_medium'])
            ->createCommand($sql)
            ->queryColumn();

        // find specialties in the DB
        $criteria = new CDbCriteria;
        $criteria->condition = 'provider_id = :provider_id';
        $criteria->params = array(':provider_id' => $providerId);
        $criteria->order = 'primary_specialty ASC';

        $arrDbSpecialties = ProviderSpecialty::model()->findAll($criteria);

        // Look for old specialties to be deleted
        $primarySpecialty = 0;
        foreach ($arrDbSpecialties as $dbSpecialty) {
            $primarySpecialty++;
            $found = false;

            foreach ($arrSpecialties as $newSpecialty) {
                if ($newSpecialty['sub_specialty_id'] == $dbSpecialty['sub_specialty_id']) {
                    $found = true;

                    if ($newSpecialty['primary_specialty'] != $dbSpecialty['primary_specialty']) {
                        $dbSpecialty->primary_specialty = $newSpecialty['primary_specialty'];
                        if (!$dbSpecialty->save()) {
                            $results['success'] = false;
                            $results['error'] = "ERROR_SAVING_PROVIDER_SPECIALTY";
                            $results['data'] = $dbSpecialty->getErrors();
                            return $results;
                        }
                    }
                    break;
                }
            }

            // If the specialty no longer applies, proceed to delete it
            if (!$found) {

                // first remove all associated licenses
                $licenses = ProviderSpecialtyLicense::model()->findAll(
                    'provider_id = :provider_id AND sub_specialty_id = :sub_specialty_id',
                    [
                        ':provider_id' => $providerId,
                        ':sub_specialty_id' => $dbSpecialty->sub_specialty_id
                    ]
                );

                foreach ($licenses as $license) {
                    $license->delete();
                }

                // Delete visit reasons for the given specialty
                $visitReasons = VisitReason::findBySpecialties($dbSpecialty->sub_specialty_id);
                foreach ($visitReasons as $vr) {
                    $attributes = array(
                        'provider_id' => $providerId,
                        'visit_reason_id' => $vr['id']
                    );
                    $providerPracticeVisitReasons = ProviderPracticeVisitReason::model()->findAllByAttributes(
                        $attributes
                    );
                    foreach ($providerPracticeVisitReasons as $item) {
                        $item->delete();
                    }
                }

                if (!$dbSpecialty->delete()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_DELETING_PROVIDER_SPECIALTY";
                    $results['data'] = $dbSpecialty->getErrors();
                    return $results;
                }
            }
        }

        // Look for new specialties to be created
        $primarySpecialty = 0;
        foreach ($arrSpecialties as $newSpecialty) {
            $primarySpecialty++;
            $found = false;

            // this is for save state and license number
            $specialtyLicense = empty($newSpecialty['license']) ? [] : $newSpecialty['license'];

            foreach ($arrDbSpecialties as $dbSpecialty) {
                if ($newSpecialty['sub_specialty_id'] == $dbSpecialty['sub_specialty_id']) {
                    $found = true;

                    // update states and license before continue
                    ProviderSpecialtyLicense::saveSpecialtyLicenses(
                        $accountId,
                        $providerId,
                        $newSpecialty['sub_specialty_id'],
                        $specialtyLicense
                    );
                    break;
                }
            }

            // If the specialty was not found in the DB
            if (!$found) {

                // Insert a new record
                $providerSpecialty = new ProviderSpecialty();
                $providerSpecialty->provider_id = $providerId;
                $providerSpecialty->sub_specialty_id = $newSpecialty['sub_specialty_id'];
                $providerSpecialty->primary_specialty = $newSpecialty['primary_specialty'];

                if ($providerSpecialty->save()) {

                    // Initialize default visit reasons
                    BaseImporter::initializeVisitReasons($accountId);

                    // Find visit reasons for the new specialty
                    $visitReasons = VisitReason::findBySpecialties($newSpecialty['sub_specialty_id']);

                    // Add visit reasons for all practices
                    foreach ($providerPractices as $practiceId) {
                        foreach ($visitReasons as $vr) {
                            $attributes = array(
                                'provider_id' => $providerId,
                                'practice_id' => $practiceId,
                                'visit_reason_id' => $vr['id'],
                                'appointment_visit_duration' => $vr['standard_visit_duration']
                            );
                            $ppvr = ProviderPracticeVisitReason::model()->findByAttributes($attributes);
                            if (!$ppvr) {
                                $ppvr = new ProviderPracticeVisitReason();
                                $ppvr->attributes = $attributes;
                                $ppvr->save();
                            }
                        }
                    }

                    $results['success'] = true;
                    $results['error'] = "";

                } else {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PROVIDER_SPECIALTY";
                    $results['data'] = $providerSpecialty->getErrors();
                }

                // save sates and licenses
                ProviderSpecialtyLicense::saveSpecialtyLicenses(
                    $accountId,
                    $providerId,
                    $newSpecialty['sub_specialty_id'],
                    $specialtyLicense
                );
            }
        }

        return $results;
    }

    /**
     * Updates provider's specialties from partner's data
     *
     * @deprecated No longer used by internal code and not recommended.
     *
     * @param mixed $provider
     * @param Provider $currentProvider
     * @param array $received
     * @param Account $account
     * @return void
     */
    public static function updatePartnersSpecialties($provider, $currentProvider, $received, $account)
    {
        if ($provider['specialty_id']) {

            // Set devops manager vendor
            $vendorSystem = 'phabricator';
            $vendor = DevOps::get()->vendor($vendorSystem);

            $partnerValues = self::getPartnersData();

            foreach ($partnerValues as $partnerData) {
                $specialty = self::getPartnerSpecialty(
                    $partnerData['id'],
                    $provider['specialty_id'],
                    $provider['sub_specialty_id']
                );

                if ($specialty) {
                    $partnerSiteParams = PartnerSiteParams::getPartnerSiteParam(
                        $currentProvider->id,
                        $partnerData['id'],
                        $partnerData['param_name']
                    );

                    if (!$partnerSiteParams) {
                        $partnerSiteParams = new PartnerSiteParams();
                        $partnerSiteParams->partner_site_id = $partnerData['id'];
                        $partnerSiteParams->provider_id = $currentProvider->id;
                        $partnerSiteParams->param_name = $partnerData['param_name'];
                        $partnerSiteParams->param_value = $specialty->retrieved_column_value;

                        if (!$partnerSiteParams->save()) {
                            $partnerData = self::getPartnerData();
                            $blob = print_r($received, true);
                            $errorReported = "Error saving provider " . $partnerData['name'] . " specialty: "
                                . print_r($partnerSiteParams->getErrors(), true);

                            $vendor->forgeClosingToolErrorTask($blob, $account, $errorReported);
                        }
                    }
                }
            }
        }
    }

    /**
     * Get hardcoded Partner's data
     *
     * @return array
     */
    public static function getPartnersData()
    {
        return [
            'Healthgrades' => array(
                'id' => 46,
                'param_name' => 'provider_hg_primary_specialty',
                'name' => 'Healthgrades'
            ),
            'Wellness' => array(
                'id' => 23,
                'param_name' => 'provider_wellness_primary_specialty',
                'name' => 'Wellness'
            ),
            'Vitals' => array(
                'id' => 111,
                'param_name' => 'provider_vitals_primary_specialty',
                'name' => 'Vitals'
            )
        ];
    }

    /**
     * Get partner's specialty
     *
     * @param integer $partnerId
     * @param integer $specialtyId
     * @param integer $subSpecialtyId
     * @return PartnerSiteEntityMatch
     */
    public static function getPartnerSpecialty($partnerId, $specialtyId, $subSpecialtyId = null)
    {
        $query = "";
        $params = array();
        $specialty = null;

        if ($partnerId === 46) {
            $query = 'partner_site_id = :partner_site_id AND '
                . 'internal_entity_type = "specialty" AND internal_column_name = "id" AND '
                . 'internal_column_value = :internal_column_value';

            $params = array(
                ':partner_site_id' => $partnerId,
                ':internal_column_value' => $specialtyId
            );
        } elseif ($partnerId === 23 || $partnerId === 111) {
            $query = 'partner_site_id = :partner_site_id AND '
                . 'internal_entity_type = "sub_specialty" AND internal_column_name = "id" AND '
                . 'internal_column_value = :internal_column_value';

            $params = array(
                ':partner_site_id' => $partnerId,
                ':internal_column_value' => $subSpecialtyId
            );
        }

        if ($query) {
            $specialty = PartnerSiteEntityMatch::model()->find($query, $params);
        }

        return $specialty;
    }

    /**
     * Get a provider's sub-specialties, with the related
     * specialties records.
     *
     * @param mixed $providerId
     * @return ProviderSpecialty[]
     */
    public function getProviderSubSpecialties($providerId)
    {
        return ProviderSpecialty::model()
            ->with("subSpecialty")
            ->with("subSpecialty.specialty")
            ->findAll(
                "t.provider_id = :provider_id",
                [":provider_id" => $providerId]
            );
    }

    /**
     * Get a provider's sub-specialties, with the related
     * specialties records.
     *
     * @param mixed $providerId
     * @return array
     */
    public function getProviderSubSpecialtiesNames($providerId)
    {
        $names = [];
        $providerSpecialties = ProviderSpecialty::model()->getProviderSubSpecialties($providerId);

        if ($providerSpecialties) {
            foreach ($providerSpecialties as $providerSpecialty) {
                $names[] = $providerSpecialty->subSpecialty->name;
            }
        }

        return $names;
    }

    /**
     * Check if a patient has specialties that are supported by PatientPoint (partner_site_id 322)
     * @param int $providerId
     * @return mixed
     */
    public static function hasPatientPointSupportedSpecialty($providerId)
    {
        $sql = sprintf(
            "SELECT psem.match_column_value as visit_reasons, retrieved_column_name
            FROM prd01.provider_specialty ps
            INNER JOIN sub_specialty ss
            ON ps.sub_specialty_id = ss.id
            INNER JOIN specialty s
            ON s.id = ss.specialty_id
            INNER JOIN partner_site_entity_match psem on
            s.id = psem.internal_column_value
            WHERE ps.provider_id = %d
            AND psem.partner_site_id = 322
            GROUP BY visit_reasons",
            $providerId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get the specialty data according to the NPPES healthcare taxonomy code 1
     *
     * @param int $npi
     * @return array
     */
    public static function getFromNppes($npi)
    {
        // get the healthcare taxonomy code 1 from PDI db
        $sql = "SELECT * FROM nppes.npidata WHERE npi = :npi LIMIT 1";
        $nppesData = Yii::app()->dbPostgresql->createCommand($sql)->queryRow(true, [':npi' => $npi]);

        // no data? exit
        if (empty($nppesData)) {
            return null;
        }

        // decode the json
        $jdoc = json_decode($nppesData['jdoc'], true);

        // empty result? exit
        if (empty($jdoc)) {
            return null;
        }

        // collect all specialties in a single array to make the query
        $taxonomies = [];

        for ($i = 1 ; $i < 10; ++$i) {
            $taxonomy = $jdoc['Healthcare Provider Taxonomy Code_' . $i] ?? null;

            if ($taxonomy) {
                $taxonomies[] = $taxonomy;
            }
        }

        // get the specialty data since the found taxonomy code
        $query = Yii::app()->db->createCommand()
            ->select("specialty.name AS specialty, sub_specialty.name AS sub_specialty, " .
                "specialty.id AS specialty_id, sub_specialty.id AS sub_specialty_id, specialty.friendly_url")
            ->from("sub_specialty")
            ->join("specialty", "sub_specialty.specialty_id = specialty.id")
            ->where(['IN', 'sub_specialty.taxonomy_code', $taxonomies]);

        // get all matching specialties
        return $query->queryAll();
    }

    /**
     * Return Bing-mapped specialties for a given provider
     * @param int $providerId
     * @return array
     */
    public static function getBingCategories($providerId)
    {
        // 17 is Bing
        $sql = sprintf(
            "SELECT DISTINCT
            SUBSTRING(retrieved_column_value, LOCATE('|', retrieved_column_value) + 1, 100) AS CategoryName,
            LEFT(retrieved_column_value, LOCATE('|', retrieved_column_value) - 1) AS BPCategoryId
            FROM provider_specialty
            INNER JOIN partner_site_entity_match
            ON provider_specialty.sub_specialty_id = partner_site_entity_match.internal_column_value
            WHERE partner_site_entity_match.partner_site_id = 17
            AND partner_site_entity_match.match_column_name = 'taxonomy_code'
            AND partner_site_entity_match.internal_entity_type = 'sub_specialty'
            AND partner_site_entity_match.internal_column_name = 'id'
            AND partner_site_entity_match.retrieved_entity_type = 'bing_provider_category'
            AND partner_site_entity_match.retrieved_column_name = 'category_id_name'
            AND provider_specialty.provider_id = %d
            ORDER BY provider_specialty.id;",
            $providerId
        );
        $arrProviderSpecialties = Yii::app()->db->createCommand($sql)->queryAll();

        if (empty($arrProviderSpecialties)) {
            // use default category
            $arrProviderSpecialties = array(
                array(
                    'BPCategoryId' => '700457',
                    'CategoryName' => 'Doctors'
                )
            );
        }

        return $arrProviderSpecialties;
    }

}
