<?php

Yii::import('application.modules.core_models.models._base.BaseDirectoryPartnerLayout');

class DirectoryPartnerLayout extends BaseDirectoryPartnerLayout
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
