<?php

Yii::import('application.modules.core_models.models._base.BaseCostQuality');

class CostQuality extends BaseCostQuality
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Declares the validation rules.
     */
    public function rules()
    {
        return array(
            array('is_excluded, measure, number_of_episodes, analysis_period_start, analysis_period_end', 'required'),
            array('is_excluded, measure, treatment_id, treatment_friendly_id, is_bilateral_procedure, practice_group_id, practice_id, provider_id, quality_area_id, quality_factor_metric_id, quality_factor_value_id, number_of_episodes, cost_factor_volume_id, cost_factor_volatility_id', 'numerical', 'integerOnly' => true),
            array('aggregatecostaverage, aggregatecoststandarddeviation, aggregatecostpercentile10th, aggregatecostpercentile25th, aggregatecostpercentile50th, aggregatecostpercentile75th, aggregatecostpercentile90th, facilitycostaverage, facilitycoststandarddeviation, facilitycostpercentile10th, facilitycostpercentile25th, facilitycostpercentile50th, facilitycostpercentile75th, facilitycostpercentile90th, anesthesiacostaverage, anesthesiacoststandarddeviation, anesthesiacostpercentile10th, anesthesiacostpercentile25th, anesthesiacostpercentile50th, anesthesiacostpercentile75th, anesthesiacostpercentile90th, radiologycostaverage, radiologycoststandarddeviation, radiologycostpercentile10th, radiologycostpercentile25th, radiologycostpercentile50th, radiologycostpercentile75th, radiologycostpercentile90th, pathologycostaverage, pathologycoststandarddeviation, pathologycostpercentile10th, pathologycostpercentile25th, pathologycostpercentile50th, pathologycostpercentile75th, pathologycostpercentile90th, professionalcostaverage, professionalcoststandarddeviation, professionalcostpercentile10th, professionalcostpercentile25th, professionalcostpercentile50th, professionalcostpercentile75th, professionalcostpercentile90th, ancillarycostaverage, ancillarycoststandarddeviation, ancillarycostpercentile10th, ancillarycostpercentile25th, ancillarycostpercentile50th, ancillarycostpercentile75th, ancillarycostpercentile90th, pharmacycostaverage, pharmacycoststandarddeviation, pharmacycostpercentile10th, pharmacycostpercentile25th, pharmacycostpercentile50th, pharmacycostpercentile75th, pharmacycostpercentile90th', 'numerical'),
            array('inpat_outpat_services', 'length', 'max' => 3),
            array('cost_tier', 'length', 'max' => 10),
            array('treatment_id, inpat_outpat_services, treatment_friendly_id, is_bilateral_procedure, practice_group_id, practice_id, provider_id, quality_area_id, quality_factor_metric_id, quality_factor_value_id, cost_tier, cost_factor_volume_id, cost_factor_volatility_id, aggregatecostaverage, aggregatecoststandarddeviation, aggregatecostpercentile10th, aggregatecostpercentile25th, aggregatecostpercentile50th, aggregatecostpercentile75th, aggregatecostpercentile90th, facilitycostaverage, facilitycoststandarddeviation, facilitycostpercentile10th, facilitycostpercentile25th, facilitycostpercentile50th, facilitycostpercentile75th, facilitycostpercentile90th, anesthesiacostaverage, anesthesiacoststandarddeviation, anesthesiacostpercentile10th, anesthesiacostpercentile25th, anesthesiacostpercentile50th, anesthesiacostpercentile75th, anesthesiacostpercentile90th, radiologycostaverage, radiologycoststandarddeviation, radiologycostpercentile10th, radiologycostpercentile25th, radiologycostpercentile50th, radiologycostpercentile75th, radiologycostpercentile90th, pathologycostaverage, pathologycoststandarddeviation, pathologycostpercentile10th, pathologycostpercentile25th, pathologycostpercentile50th, pathologycostpercentile75th, pathologycostpercentile90th, professionalcostaverage, professionalcoststandarddeviation, professionalcostpercentile10th, professionalcostpercentile25th, professionalcostpercentile50th, professionalcostpercentile75th, professionalcostpercentile90th, ancillarycostaverage, ancillarycoststandarddeviation, ancillarycostpercentile10th, ancillarycostpercentile25th, ancillarycostpercentile50th, ancillarycostpercentile75th, ancillarycostpercentile90th, pharmacycostaverage, pharmacycoststandarddeviation, pharmacycostpercentile10th, pharmacycostpercentile25th, pharmacycostpercentile50th, pharmacycostpercentile75th, pharmacycostpercentile90th', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, is_excluded, measure, treatment_id, inpat_outpat_services, treatment_friendly_id, is_bilateral_procedure, practice_group_id, practice_id, provider_id, quality_area_id, quality_factor_metric_id, quality_factor_value_id, number_of_episodes, analysis_period_start, analysis_period_end, cost_tier, cost_factor_volume_id, cost_factor_volatility_id, aggregatecostaverage, aggregatecoststandarddeviation, aggregatecostpercentile10th, aggregatecostpercentile25th, aggregatecostpercentile50th, aggregatecostpercentile75th, aggregatecostpercentile90th, facilitycostaverage, facilitycoststandarddeviation, facilitycostpercentile10th, facilitycostpercentile25th, facilitycostpercentile50th, facilitycostpercentile75th, facilitycostpercentile90th, anesthesiacostaverage, anesthesiacoststandarddeviation, anesthesiacostpercentile10th, anesthesiacostpercentile25th, anesthesiacostpercentile50th, anesthesiacostpercentile75th, anesthesiacostpercentile90th, radiologycostaverage, radiologycoststandarddeviation, radiologycostpercentile10th, radiologycostpercentile25th, radiologycostpercentile50th, radiologycostpercentile75th, radiologycostpercentile90th, pathologycostaverage, pathologycoststandarddeviation, pathologycostpercentile10th, pathologycostpercentile25th, pathologycostpercentile50th, pathologycostpercentile75th, pathologycostpercentile90th, professionalcostaverage, professionalcoststandarddeviation, professionalcostpercentile10th, professionalcostpercentile25th, professionalcostpercentile50th, professionalcostpercentile75th, professionalcostpercentile90th, ancillarycostaverage, ancillarycoststandarddeviation, ancillarycostpercentile10th, ancillarycostpercentile25th, ancillarycostpercentile50th, ancillarycostpercentile75th, ancillarycostpercentile90th, pharmacycostaverage, pharmacycoststandarddeviation, pharmacycostpercentile10th, pharmacycostpercentile25th, pharmacycostpercentile50th, pharmacycostpercentile75th, pharmacycostpercentile90th', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        return array(
            'practice' => array(self::BELONGS_TO, 'Practice', 'practice_id'),
            'treatment' => array(self::BELONGS_TO, 'Treatment', 'treatment_id'),
        );
    }

}
