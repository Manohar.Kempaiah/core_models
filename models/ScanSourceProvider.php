<?php

Yii::import('application.modules.core_models.models._base.BaseScanSourceProvider');

class ScanSourceProvider extends BaseScanSourceProvider
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Search for a provider's scan source previous result
     *
     * @param ListingType $listingType
     * @param integer $maxAge
     * @param string $hash
     *
     * @return array
     * @throws Exception
     */
    public function findByHashAndListing(ListingType $listingType, $maxAge, $hash)
    {
        $sql = "SELECT
                  scan_source_provider.id,
                  scan_source_provider.scan_result_provider_id,
                  scan_source_provider.input_hash,
                  scan_source_provider.source_data
                FROM scan_source_provider
                  JOIN scan_result_provider
                      ON (
                        scan_source_provider.scan_result_provider_id = scan_result_provider.id
                        AND scan_result_provider.listing_type_id = :listing_type_id
                      )
                  JOIN scan_request_provider
                    ON (scan_result_provider.scan_request_provider_id = scan_request_provider.id)
                  JOIN scan_request_practice
                    ON (scan_request_provider.scan_request_practice_id = scan_request_practice.id)
                  JOIN scan_request
                    ON (scan_request_practice.scan_request_id = scan_request.id)
                WHERE scan_source_provider.input_hash = :hash
                  AND scan_request.date_added > DATE_SUB(NOW(), INTERVAL " . $maxAge . " MINUTE)
                ORDER BY scan_source_provider.id DESC
                LIMIT 1";

        return Yii::app()->db
            ->createCommand($sql)
            ->bindValues([
                ":listing_type_id" => $listingType->id,
                ":hash" => $hash
            ])
            ->queryRow();
    }

    /**
     * Search for a provider's scan source previous results
     *
     * @param string $hash
     * @param string $scanTemplateId
     *
     * @return array
     * @throws Exception
     */
    public function findByHash($hash, $scanTemplateId)
    {
        $sql = "SELECT
                    scan_request_provider.provider_id,
                    scan_request_provider.provider_name,
                    listing_type.code,
                    listing_type.engine,
                    listing_type.name,
                    scan_source_provider.input_hash,
                    scan_source_provider.source_data,
                    scan_result_provider.listing_type_id
                FROM
                    scan_source_provider
                    INNER JOIN scan_result_provider
                        ON (scan_source_provider.scan_result_provider_id = scan_result_provider.id)
                    INNER JOIN scan_request_provider
                        ON (scan_result_provider.scan_request_provider_id = scan_request_provider.id)
                    INNER JOIN scan_request_practice
                        ON (scan_request_provider.scan_request_practice_id = scan_request_practice.id)
                    INNER JOIN scan_request ON (scan_request_practice.scan_request_id = scan_request.id)
                    INNER JOIN scan_template_listing_type ON (
                        scan_request.scan_template_id = scan_template_listing_type.scan_template_id
                        AND scan_template_listing_type.listing_type_id = scan_result_provider.listing_type_id
                    )
                    INNER JOIN listing_type ON (listing_type.id = scan_result_provider.listing_type_id)
                WHERE
                   scan_template_listing_type.scan_template_id = :scan_template_id
                   AND scan_request.date_added > DATE_SUB(NOW(), INTERVAL scan_template_listing_type.max_age MINUTE)
                   AND scan_source_provider.input_hash = :hash
                GROUP BY listing_type.id, scan_request_provider.provider_name";

        return Yii::app()->db
            ->createCommand($sql)
            ->bindValues([
                ":scan_template_id" => $scanTemplateId,
                ":hash" => $hash
            ])
            ->queryAll();
    }

}
