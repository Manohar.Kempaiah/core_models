<?php

Yii::import('application.modules.core_models.models._base.BaseCommunicationLogStatusHistory');

class CommunicationLogStatusHistory extends BaseCommunicationLogStatusHistory
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }
    
    /**
     * Set dates automatically if needed
     */
    public function beforeSave()
    {
        if (empty($this->date_added) || $this->date_added == '0000-00-00 00:00:00') {
            $this->date_added = date('Y-m-d H:i:s');
        }

        return parent::beforeSave();
    }

}
