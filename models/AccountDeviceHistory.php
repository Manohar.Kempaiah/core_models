<?php

Yii::import('application.modules.core_models.models._base.BaseAccountDeviceHistory');

class AccountDeviceHistory extends BaseAccountDeviceHistory
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return 'date_added';
    }

    /**
     * Pivot models
     * @return array
     */
    public function pivotModels()
    {
        return array(
        );
    }

    /**
     * Label name
     * @param int $n
     * @return string
     */
    public static function label($n = 1)
    {
        return Yii::t('app', 'Account-Device History|Account-Device Histories', $n);
    }

    /**
     * Create an entry in account_device_history when a device changes
     * @param int $account_device_id
     * @param string $prev_status
     * @param string $new_status
     * @param string $change_type
     * @return mixed
     */
    public static function log($account_device_id, $prev_status, $new_status, $change_type)
    {

        $accountDeviceHistory = new AccountDeviceHistory();
        $accountDeviceHistory->account_device_id = $account_device_id;
        $accountDeviceHistory->prev_status = $prev_status;
        $accountDeviceHistory->new_status = $new_status;
        $accountDeviceHistory->change_type = $change_type;
        if (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
            $accountDeviceHistory->account_id = Yii::app()->user->id;
        }
        $accountDeviceHistory->date_added = date('Y-m-d H:i:s');
        return $accountDeviceHistory->save();
    }

}
