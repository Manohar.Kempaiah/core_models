<?php

Yii::import('application.modules.core_models.models._base.BaseCountry');

class Country extends BaseCountry
{

    /**
     * Additional relations
     * @return array
     */
    public function relations()
    {
        return array(
            'accounts' => array(self::HAS_MANY, 'Account', 'country_id'),
            'defaultLocalization' => array(self::BELONGS_TO, 'Localization', 'default_localization_id'),
            'practices' => array(self::HAS_MANY, 'Practice', 'country_id'),
            'states' => array(self::HAS_MANY, 'State', 'country_id'),
        );
    }

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Return country's phone prefix of localization
     * @param int $localizationId
     * @return string
     */
    public static function getCountryPhonePrefix($localizationId)
    {

        // If $localizationId doesn't exist return country prefix = 1 from USA
        if ($localizationId == '') {
            return '1';
        }

        // Look for country prefix
        $sql = sprintf(
            "SELECT co.phone_prefix
            FROM location lo
            INNER JOIN city ci ON ci.id = lo.city_id
            INNER JOIN state st ON st.id = ci.state_id
            INNER JOIN country co ON co.id = st.country_id
            WHERE lo.id = %d;",
            $localizationId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

}
