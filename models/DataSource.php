<?php

Yii::import('application.modules.core_models.models._base.BaseDataSource');

class DataSource extends BaseDataSource
{
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
