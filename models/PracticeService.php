<?php

Yii::import('application.modules.core_models.models._base.BasePracticeService');

class PracticeService extends BasePracticeService
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * get PracticeServices
     * @param integer $practiceId
     * @param integer $brief
     * @return array()
     */
    public static function getPracticeServices($practiceId = 0, $brief = 0)
    {

        if (empty($practiceId)) {
            return array();
        }

        $fields = ($brief == 1 ? 'id,description' : '*');

        $sql = sprintf(
            "SELECT %s FROM practice_service WHERE practice_id = %d",
            $fields,
            $practiceId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Save Data
     * @param array $postData
     * @param int $practiceId
     * @param int $accountId
     * @return array
     */
    public static function saveData($postData = '', $practiceId = 0, $accountId = 0)
    {

        $results['success'] = true;
        $results['error'] = "";

        if (empty($postData)) {
            $results['success'] = false;
            $results['error'] = "EMPTY_POST";
            return $results;
        }

        if ($practiceId == 0 || empty($accountId)) {
            $results['success'] = false;
            $results['error'] = "PRACTICE_ID_ZERO";
            return $results;
        }

        // validate access
        ApiComponent::checkPermissions('Practice', $accountId, $practiceId);

        foreach ($postData as $data) {
            $model = null;
            $backendAction = !empty($data['backend_action']) ? trim($data['backend_action']) : '';
            $data['id'] = isset($data['id']) ? $data['id'] : 0;
            $description = trim($data['description']);

            if ($data['id'] > 0) {
                $model = PracticeService::model()->findByPk($data['id']);
            }

            if ($backendAction == 'delete' || empty($description)) {
                // delete the row
                if (!empty($model) && !$model->delete()) {
                    $results['success'] = false;
                    $results['error'] = "ERROR_DELETING_PRACTICE_SERVICE";
                    $results['data'] = $model->getErrors();
                    return $results;
                }
            } else {

                $auditAction = 'U';
                if (empty($model)) {
                    $model = new PracticeService;
                    $model->added_by_account_id = $accountId;
                    $model->date_added = TimeComponent::getNow();
                    $auditAction = 'C';
                } else {
                    $model->updated_by_account_id = $accountId;
                    $model->date_updated = TimeComponent::getNow();
                }
                $model->description = $description;
                $model->practice_id = $practiceId;

                if ($model->save()) {
                    $auditSection = 'Practice Service practice_id #' . $practiceId;
                    AuditLog::create($auditAction, $auditSection, 'practice_service', null, false);
                    $results['success'] = true;
                    $results['error'] = "";
                    if ($auditAction == 'C') {
                        $data['id'] = $model->id;
                        unset($data['backend_action']);
                        $results['data'][] = $data;
                    }
                } else {
                    $results['success'] = false;
                    $results['error'] = "ERROR_SAVING_PRACTICE_SERVICE";
                    $results['data'] = $model->getErrors();
                    return $results;
                }
            }
        }
        return $results;
    }

    /**
     * Get practice services by partner site.
     *
     * @param int $partnerSiteId
     * @return array
     */
    public function findByPartnerSite($partnerSiteId)
    {
        $sql = "SELECT DISTINCT(practice_service.description)
            FROM practice_service
            INNER JOIN account_practice ON account_practice.practice_id = practice_service.practice_id
            INNER JOIN organization_account ON organization_account.account_id = account_practice.account_id
            INNER JOIN organization ON organization.id = organization_account.organization_id
            WHERE organization.partner_site_id = :partner_site_id
            ORDER BY practice_service.description";

        return Yii::app()->db->createCommand($sql)->queryAll(true, [':partner_site_id' => $partnerSiteId]);
    }
}
