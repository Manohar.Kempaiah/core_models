<?php

Yii::import('application.modules.core_models.models._base.BaseAccountPractice');

class AccountPractice extends BaseAccountPractice
{

    /**
     * Model
     * @param str $className
     * @return object
     */
    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Columns representing the schema
     * @return string
     */
    public static function representingColumn()
    {
        return array('account_id', 'practice_id');
    }

    /**
     * Override rules defined by the base model
     * @return array
     */
    public function rules()
    {
        return array(
            array('account_id, practice_id', 'required'),
            array('account_id, practice_id', 'numerical', 'integerOnly' => true),
            array('research_status', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, account_id, account_full_name, practice_id, research_status', 'safe', 'on' => 'search'),
        );
    }

    /**
     * Specify the display labels for attributes (name=>label)
     * @return array
     */
    public function attributeLabels()
    {
        return array(
            'id' => Yii::t('app', 'ID'),
            'account_id' => Yii::t('app', 'Account'),
            'practice_id' => Yii::t('app', 'Practice'),
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria = new CDbCriteria;

        // this is to enable eager loading
        $criteria->with = array('account', 'practice');

        // $criteria->compare('id', $this->id);
        // it will search by account_id or account_full_name
        if (!intval($this->account_id) && is_string($this->account_id) && strlen($this->account_id) > 0) {
            $criteria->addSearchCondition('concat(account.first_name, " ", account.last_name)', $this->account_id);
        } else {
            $criteria->compare('account_id', $this->account_id);
        }

        // it will search by practice_id or practice.name
        if (!intval($this->practice_id) && is_string($this->practice_id) && strlen($this->practice_id) > 0) {
            $criteria->compare('practice.name', $this->practice_id, true);
        } else {
            $criteria->compare('practice_id', $this->practice_id);
        }

        return new CActiveDataProvider(get_class($this), array(
            'criteria' => $criteria, 'pagination' => array('pageSize' => 100)
        ));
    }

    /**
     * This account has access to this practice?
     * @param integer $accountId
     * @param integer $practiceId
     *
     * @return bool
     */
    public static function canAccess($accountId, $practiceId)
    {
        $exists = AccountPractice::model()->cache(Yii::app()->params['cache_medium'])->find(
            "account_id=:account_id AND practice_id=:practice_id",
            [
                ":account_id" => $accountId,
                ":practice_id" => $practiceId
            ]
        );

        if (!empty($exists)) {
            return true;
        }
        return false;
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeDelete()
    {
        $practiceId = $this->practice_id;
        $accountId = $this->account_id;

        // CPN Validation
        $existsCpn = CpnInstallation::model()->exists(
            'practice_id=:practice_id AND account_id=:account_id AND enabled_flag=1 AND del_flag=0 ',
            array(':practice_id' => $practiceId, ':account_id' => $accountId)
        );
        if ($existsCpn) {
            $error = "This practice has an active Companion installation. "
                . "Please deactivate it before unlinking the practice.";
            $this->addError('practice_id', $error);
            if (Yii::app()->request->IsAjaxRequest) {
                header("HTTP/1.0 409 Conflict");
            } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                Yii::app()->user->setFlash('error', $error);
            }
            return false;
        }

        $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId);
        $isOwner = ($accountId == $ownerAccountId) ? true : false;

        if ($isOwner) {

            $device = AccountDevice::model()->find(
                'practice_id=:practiceId AND account_id=:accountId AND status IN ("P", "T", "A")',
                array(':practiceId' => $practiceId, ':accountId' => $accountId)
            );
            if ($device) {
                if ($device->isReviewRequest()) {
                    $deviceType = "ReviewRequest";
                } else {
                    $deviceType = "ReviewHub";
                }

                $error = "This practice has " . $deviceType . " associations. Please remove the practice from the "
                    . "" . $deviceType . " and try again.";

                $this->addError('practice_id', $error);
                if (Yii::app()->request->IsAjaxRequest) {
                    header("HTTP/1.0 409 Conflict");
                } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                    Yii::app()->user->setFlash(
                        'error',
                        "<strong>ERROR: </strong>This account and practice can't be"
                            . " unlinked: <a data-who='practice' data-id='$practiceId'"
                            . " id='showErrorProviderPracticeUnlink' href='#'> See details</a>"
                    );
                }
                return false;
            }

            // get the providers at this practice for this account
            $arrProviders = ProviderPractice::getProvidersAtPractices($practiceId, $ownerAccountId);
            if (!empty($arrProviders)) {
                foreach ($arrProviders as $eachProvider) {
                    $providerId = $eachProvider['provider_id'];

                    // This relations has any widget?
                    Widget::clearDraftWidgets($accountId, $providerId, $practiceId);
                    $arrProviderPracticeWidget = ProviderPracticeWidget::getWidgetFromProviderPracticeAccount(
                        $providerId,
                        $practiceId,
                        $accountId
                    );
                    if (count($arrProviderPracticeWidget) > 0) {
                        $error = "This practice is associated with an appointment widget. "
                            . "Please remove the widget and try again.";

                        $this->addError('practice_id', $error);
                        if (Yii::app()->request->IsAjaxRequest) {
                            header("HTTP/1.0 409 Conflict");
                        } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                            Yii::app()->user->setFlash(
                                'error',
                                "<strong>ERROR: </strong>This account and practice "
                                    . "can't be unlinked: <a data-who='practice' data-id='$practiceId'"
                                    . " id='showErrorProviderPracticeUnlink' href='#'>See details</a>"
                            );
                        }
                        return false;
                    }

                    // AccountPractice count
                    $practiceCount = AccountPractice::model()->count(
                        'practice_id=:practiceId',
                        array(':practiceId' => $practiceId)
                    );

                    if ($practiceCount == 1) {
                        // Has Pending appointments for this practice<->provider ?
                        $hasAppointments = SchedulingMail::model()->exists(
                            'practice_id=:practiceId AND provider_id=:providerId AND status=:status',
                            array(':practiceId' => $practiceId, ':providerId' => $providerId, ':status' => 'P')
                        );
                        if ($hasAppointments) {
                            $error = "Providers within this practice have pending appointment requests. "
                                . "Please either approve or reject them before proceeding.";

                            $this->addError('practice_id', $error);
                            if (Yii::app()->request->IsAjaxRequest) {
                                header("HTTP/1.0 409 Conflict");
                            } elseif (php_sapi_name() != "cli" && isset(Yii::app()->user->id)) {
                                Yii::app()->user->setFlash(
                                    'error',
                                    "Providers within this practice have pending "
                                        . "appointment requests. "
                                        . "Please either approve or reject them before proceeding."
                                );
                            }
                            return false;
                        }
                    }
                }
            }
        }

        // delete from EmrScheduleException
        $arrESE = EmrScheduleException::model()->findAll(
            'practice_id = :practice_id',
            array(':practice_id' => $this->practice_id)
        );
        if (!empty($arrESE)) {
            foreach ($arrESE as $eachRecord) {
                $eachRecord->delete();
            }
            // delete from EmrScheduleException
            $esExcep = EmrScheduleException::model()->findAll(
                'practice_id = :practice_id',
                array(':practice_id' => $this['practice_id'])
            );
            if (!empty($esExcep)) {
                foreach ($esExcep as $eachExcepRecord) {
                    $eachExcepRecord->delete();
                }
            }
        }

        // delete from AccountPracticeEMR
        $apEmr = AccountPracticeEmr::model()->findAll(
            'practice_id = :practice_id AND account_id = :account_id',
            array(':practice_id' => $this->practice_id, ':account_id' => $accountId)
        );
        if (!empty($apEmr)) {
            foreach ($apEmr as $eachRecord) {
                if (!$eachRecord->delete()) {
                    throw new Exception(
                        'Failed to delete AccountPracticeEmr '
                            . $apEmr->id . ' DB errors: ' . json_encode($eachRecord->getErrors())
                    );
                }
            }
        }

        // delete from provider_practice_gmb
        $arrGmb = ProviderPracticeGmb::model()->findAll(
            'practice_id = :practiceId AND account_id = :accountId',
            array(':practiceId' => $practiceId, ':accountId' => $accountId)
        );
        if (!empty($arrGmb)) {
            foreach ($arrGmb as $gmb) {
                $gmb->delete();
            }
        }

        // delete from provider_practice_social_enhance
        $arrPpse = ProviderPracticeSocialEnhance::model()->findAll(
            'practice_id = :practiceId AND account_id = :accountId',
            array(':practiceId' => $practiceId, ':accountId' => $accountId)
        );
        if (!empty($arrPpse)) {
            foreach ($arrPpse as $ppse) {
                $ppse->delete();
            }
        }

        // Delete from ProviderPractice
        return parent::beforeDelete();
    }

    /**
     * Remove features provided by account's organization if necesary
     * Run the importation of account EMR data into the ingestion server.
     * Update Salesforce
     */
    public function afterDelete()
    {
        // save event to the audit log
        AuditLog::create(
            'D',
            'Delete record',
            'account_practice',
            'ID: ' . $this->id . ', Practice ID: ' . $this->practice_id . ', Account ID: ' . $this->account_id,
            false,
            $this->account_id
        );

        // It is owner or manager account delete AccountPractice for all
        $currentAccountOrg = OrganizationAccount::model()->find(
            'account_id=:account_id',
            array(':account_id' => $this->account_id)
        );

        if (!empty($currentAccountOrg)) {
            if ($currentAccountOrg->connection == 'O' || $currentAccountOrg->connection == 'M') {
                $allAccounts = OrganizationAccount::model()->findAll(
                    'organization_id = :organization_id AND account_id != :account_id',
                    array(':organization_id' => $currentAccountOrg->organization_id, ':account_id' => $this->account_id)
                );

                if (!empty($allAccounts)) {
                    foreach ($allAccounts as $allAccount) {
                        $allAccountPractice = AccountPractice::model()->find(
                            'practice_id = :practice_id AND account_id = :account_id',
                            array(':practice_id' => $this->practice_id, ':account_id' => $allAccount->account_id)
                        );
                        if (!empty($allAccountPractice)) {
                            $allAccountPractice->delete();
                        }
                    }
                }
            }
        }

        $accountOrg = OrganizationAccount::model()->find(
            'account_id = :accountId AND connection = :connection',
            array(':accountId' => $this->account_id, ':connection' => "O")
        );
        if ($accountOrg) {
            $organizationFeatures = OrganizationFeature::model()->findAll(
                'organization_id = :organizationId AND practice_id = :practiceId',
                array(':organizationId' => $accountOrg->organization_id, ':practiceId' => $this->practice_id)
            );
            foreach ($organizationFeatures as $orgFeature) {
                $orgFeature->deleteFeature();
            }
        }

        if ($this->practice_id) {
            // update practice claimed value
            self::updateClaimed($this->practice_id);
        }

        // queue updating Number_of_Locations__c in salesforce if necessary
        $organizationId = Account::belongsToOrganization($this->account_id);
        if ($organizationId) {
            SqsComponent::sendMessage('updateSFOrganizationFeatures', $organizationId);
        }

        SqsComponent::sendMessage('importAccountEmr', $this->account_id);
        parent::afterDelete();
    }

    /**
     * Validate account-practice is unique
     * Update Salesforce
     * @return boolean
     */
    public function beforeSave()
    {
        $this->date_added = TimeComponent::validateDateAdded($this->date_added);

        $associationChanged = false;
        if (!$this->isNewRecord) {
            // on update, check if account_id or practice_id changed
            if (!empty($this->oldRecord->accountId) && !empty($this->oldRecord->practice_id)) {
                if (
                    $this->oldRecord->accountId != $this->account_id
                    ||
                    $this->oldRecord->practice_id != $this->practice_id
                ) {
                    $associationChanged = true;
                }
            }
        }

        if ($this->isNewRecord) {
            $this->research_status = 'Published';
        }

        $practiceId = isset($this->practice_id) ? $this->practice_id : 0;
        $accountId = isset($this->account_id) ? $this->account_id : 0;

        if ($practiceId > 0 && $accountId > 0 && ($this->isNewRecord || $associationChanged)) {

            // check if association already exists
            $exists = AccountPractice::model()->exists(
                'practice_id=:practice_id AND account_id=:account_id',
                array(':practice_id' => $practiceId, ':account_id' => $accountId)
            );
            if ($exists) {
                // it does, reject
                $this->addError('practice_id_lookup', 'This practice is already associated to this account.');
                return false;
            }

            // queue updating Number_of_Locations__c in salesforce if necessary
            $organizationId = Account::belongsToOrganization($this->account_id);
            if ($organizationId) {
                SqsComponent::sendMessage('updateSFOrganizationFeatures', $organizationId);
            }
        }

        return parent::beforeSave();
    }

    /**
     * Create relation between account->practice
     * @param int $accountId
     * @param int $practiceId
     * @throws Exception
     */
    public static function associateAccountPractice($accountId = 0, $practiceId = 0)
    {
        $arrManagers = OrganizationAccount::getManagers($accountId);
        foreach ($arrManagers as $manager) {
            // Add the practice to the owner and to all managers
            if ($manager['connection_type'] == 'O' || $manager['connection_type'] == 'M') {
                $accountPractice = new AccountPractice();
                $accountPractice->account_id = $manager['id'];
                $accountPractice->practice_id = $practiceId;
                // The beforeSave() check if exists or not
                $accountPractice->save();
            }
        }
    }

    /**
     * Import EMR settings and set claimed value
     * @return bool
     */
    public function afterSave()
    {
        // save insert event to the audit log
        if ($this->isNewRecord) {
            AuditLog::create(
                'C',
                'Create record',
                'account_practice',
                'ID: ' . $this->id . ', Practice ID: ' . $this->practice_id . ', Account ID: ' . $this->account_id,
                false,
                $this->account_id
            );
        } else {
            AuditLog::create(
                'U',
                'Update record',
                'account_practice',
                'ID: ' . $this->id . ', Practice ID: ' . $this->practice_id . ', Account ID: ' . $this->account_id,
                false,
                $this->account_id
            );
        }

        // update claimed value, this is not neccesary on updates
        if ($this->isNewRecord) {
            self::updateClaimed($this->practice_id);
        }

        // Create account-practice for the managers if not exists
        AccountPractice::associateAccountPractice($this->account_id, $this->practice_id);

        // emr_importation_disabled is set by ExternalDataImporter import process
        if (empty(Yii::app()->params['emr_importation_disabled'])) {
            SqsComponent::sendMessage('importAccountEmr', $this->account_id);
        }

        return parent::afterSave();
    }

    /**
     * Called on rendering the column for each row
     * @return string
     */
    public function getPracticeTooltip()
    {
        return '<a class="practiceTooltip" href="/administration/practiceTooltip?id=' . $this->practice_id . '" '
            . 'rel="/administration/practiceTooltip?id=' . $this->practice_id . '" title="' . $this->practice . '">'
            . $this->practice . '</a>';
    }

    /**
     * Get others practices associated to a list of providers that are not managed by a given account
     *
     * @param string $providerList providers list
     * @param int $accountId account id
     * @param string $filterPractice
     * @param bool $fullData
     * @return mixed
     */
    public static function getOtherPractices(
        $providerList = '',
        $accountId = null,
        $filterPractice = '',
        $fullData = true
    ) {
        if (empty($providerList) || empty($accountId)) {
            return '';
        }

        $sql = sprintf(
            "SELECT DISTINCT t.*, t.id AS practice_id, pp.id AS provider_practice_id,
            pp.phone_number AS pp_phone_number, pp.fax_number AS pp_fax_number, pp.email AS pp_email
            FROM practice AS t
            INNER JOIN provider_practice AS pp
                ON t.id = pp.practice_id
            LEFT JOIN account_practice AS ap
                ON ap.practice_id = pp.practice_id
            WHERE pp.provider_id IN (%s)
                AND (ap.account_id IS NULL OR ap.account_id <> %d)
                %s
                AND t.id NOT IN
                    (SELECT practice_id
                    FROM account_practice
                    WHERE account_id = %d)
            GROUP BY t.id
            ORDER BY t.name;",
            $providerList,
            $accountId,
            $filterPractice,
            $accountId
        );
        $tmpOtherPractices = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();

        if (!empty($tmpOtherPractices) && $fullData) {
            foreach ($tmpOtherPractices as $tmp) {
                $practiceId = $tmp['id'];
                $arrOtherPractices[$practiceId] = $tmp;

                if (!empty($tmp['logo_path'])) {
                    $arrOtherPractices[$practiceId]['logo_path'] = AssetsComponent::generateImgSrc(
                        $tmp['logo_path'],
                        'practices',
                        'M',
                        true,
                        true
                    );
                }

                $location = Location::model()->cache(Yii::app()->params['cache_short'])->getLocationDetails(
                    $tmp['location_id']
                );
                $cityName = $abbreviation = $zipCode = '';
                if (isset($location[0])) {
                    $location = $location[0];
                    $cityName = str_replace(' ', '&nbsp;', trim($location['city_name']));
                    $abbreviation = str_replace(' ', '&nbsp;', trim($location['abbreviation']));
                    $zipCode = str_replace(' ', '&nbsp;', trim($location['zipcode']));
                }
                $arrOtherPractices[$practiceId]['city_name'] = $cityName;
                $arrOtherPractices[$practiceId]['abbreviation'] = $abbreviation;
                $arrOtherPractices[$practiceId]['zipcode'] = $zipCode;
            }
        } else {
            $arrOtherPractices = $tmpOtherPractices;
        }

        return $arrOtherPractices;
    }

    /**
     * get unclaimed practice from provider
     * @param int $providerId
     * @return array
     */
    public static function getUnclaimedPractices($providerId = 0)
    {
        $sql = sprintf(
            "SELECT p.id AS practice_id
            FROM practice AS p
            LEFT JOIN provider_practice AS pp
                ON pp.practice_id = p.id
            WHERE p.id NOT IN
                (SELECT practice_id
                FROM account_practice
                GROUP BY practice_id)
            AND pp.provider_id = %d;",
            $providerId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Associate to account, not claimed pactices,  for a given provider
     * @param int $accountId
     * @param int $providerId
     */
    public static function autoAssociateUnclaimProviderPractice($accountId = 0, $providerId = 0)
    {
        $arrPractices = AccountPractice::getUnclaimedPractices($providerId);
        $result = true;
        if (!empty($arrPractices)) {
            foreach ($arrPractices as $eachPractice) {
                $practiceId = $eachPractice['practice_id'];
                // My account is linked with this practice ?
                $haveThisPractice = AccountPractice::model()->find(
                    'practice_id=:practiceId AND account_id=:accountId',
                    array(':practiceId' => $practiceId, ':accountId' => $accountId)
                );
                if (!$haveThisPractice) {
                    $addAccountPractice = new AccountPractice;
                    $addAccountPractice->practice_id = $practiceId;
                    $addAccountPractice->account_id = $accountId;
                    $addAccountPractice->save();
                    $result = true;
                } else {
                    $result = false;
                }
            }
        }
        return $result;
    }

    /**
     * Get names of accounts other than $accountId that own $practiceId
     * @param int $practiceId
     * @param int $accountId
     * @return string
     */
    public static function getOtherOwners($practiceId, $accountId)
    {
        $sql = sprintf(
            "SELECT GROUP_CONCAT(CONCAT(account.first_name, ' ', account.last_name) SEPARATOR ', ')
            FROM account_practice
            INNER JOIN account
                ON account_practice.account_id = account.id
            WHERE practice_id = %d
                AND account_id != %d;",
            $practiceId,
            $accountId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get email of accounts related to given practice
     * @param int $practiceId
     * @param bool $useCache
     * @return arr
     */
    public static function getPracticeAccountEmail($practiceId = 0, $useCache = true)
    {
        if ($practiceId == 0) {
            return false;
        }

        $cache = ($useCache) ? Yii::app()->params['cache_medium'] : 0;

        $sql = sprintf(
            "SELECT ap.account_id, a.email
            FROM account_practice AS ap
            INNER JOIN account a
                ON ap.account_id = a.id
            WHERE ap.practice_id = %d
                AND a.email != ''; ",
            $practiceId
        );

        return Yii::app()->db->cache($cache)->createCommand($sql)->queryAll();
    }

    /**
     * Get an account's practices research status
     * @param int $accountId
     * @param int $practiceId
     * @param bool $useCache
     * @return array
     */
    public static function getPracticeResearchStatus($accountId = 0, $practiceId = 0, $useCache = false)
    {
        if ($accountId == 0 || $practiceId == 0) {
            return false;
        }

        $sql = sprintf(
            "SELECT research_status
            FROM account_practice
            WHERE account_id = %d
            AND practice_id = %d
            LIMIT 1;",
            $accountId,
            $practiceId
        );

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_medium'] : 0;
        $result = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryRow();
        return !empty($result['research_status']) ? $result['research_status'] : '';
    }

    /**
     * Get an account's practices
     * @param int $accountId
     * @param int $research
     * @return array
     */
    public static function getAccountPracticesResearch($accountId, $research)
    {
        $researchStr = "";

        if ($research == 1) {
            $researchStr = " AND (
                ap.research_status IN ('Pending', 'Rejected')
            ) ";
        }

        $sql = sprintf(
            "SELECT ap.practice_id AS id, ap.practice_id, pra.name AS name,
            pra.name AS practice_name, pra.address AS practice_address, ap.research_status
            FROM account_practice AS ap
            INNER JOIN practice AS pra
                ON pra.id = ap.practice_id
            WHERE ap.account_id = %d
                %s
            ORDER BY practice_name;",
            $accountId,
            $researchStr
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Return the list of practices for this account
     * @param int $accountId
     * @return array
     */
    public static function getList($accountId = 0)
    {
        $sql = sprintf("SELECT practice_id FROM account_practice WHERE account_id = %d;", $accountId);
        $arrPractices = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();
        $practicesList = '';
        if (!empty($arrPractices)) {
            foreach ($arrPractices as $practice) {
                $practicesList .= $practice['practice_id'] . ',';
            }
        }
        $practicesList = rtrim($practicesList, ',');
        return $practicesList;
    }

    /**
     * Get an account's practices basic data
     * @param array $data
     * [
     *    'account_id' => 0,
     *    'use_cache' => 1,
     *    'page' => 1,
     *    'page_size' => 50
     * ]
     * @return array
     */
    public static function getPractices($data)
    {

        // Process params
        $accountId = !empty($data['account_id']) ? $data['account_id'] : 0;
        $useCache = !empty($data['use_cache']) ? $data['use_cache'] : false;
        $page = !empty($data['page']) ? $data['page'] : 1;

        // page has a negative number?
        $page = $page < 1 ? 1 : $page;

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;

        // count the providers related to this account
        $totalPractices = AccountPractice::model()
            ->cache($cacheTime)
            ->count("account_id=:account_id", [":account_id" => $accountId]);

        // Prepare the offset
        $pageSize = !empty($data['page_size']) ? $data['page_size'] : $totalPractices;
        $offset = ($page - 1) * $pageSize;

        $sql = sprintf(
            "SELECT p.id, p.localization_id, p.country_id, p.name, p.address_full, p.address,
            p.phone_number, p.fax_number, p.email, p.website, p.friendly_url, p.logo_path, p.description,
            p.location_id, practice_type.name AS practice_type, account_practice.research_status,
            GROUP_CONCAT(DISTINCT pp.provider_id ORDER BY pp.provider_id SEPARATOR ',') AS providers_at_practice,
            account_practice.all_providers, p.score
            FROM practice AS p
            LEFT JOIN practice_details
                ON p.id = practice_details.practice_id
            LEFT JOIN practice_type
                ON practice_details.practice_type_id = practice_type.id
            LEFT JOIN account_practice
                ON account_practice.practice_id = p.id
            LEFT JOIN provider_practice AS pp
                ON p.id = pp.practice_id
            WHERE account_practice.account_id = %d
            AND practice_details.practice_type_id != 5
            GROUP BY p.id
            ORDER BY `name` ASC
            LIMIT %d, %d;",
            $accountId,
            $offset,
            $pageSize
        );

        $cacheTime = Common::isTrue($useCache) ? Yii::app()->params['cache_short'] : 0;
        $accountPractices = array();
        $tmpPractices = Yii::app()->db->cache($cacheTime)->createCommand($sql)->queryAll();

        $accessGranted = 'custom';
        if (!empty($tmpPractices)) {
            foreach ($tmpPractices as $value) {

                $value['practice_id'] = $value['id'];

                if (Common::isTrue($value['all_providers'])) {
                    $accessGranted = 'practices';
                }

                // Adding practice location
                $practiceLocation = Location::getLocationDetails($value['location_id']);
                $value['practice_location'] = array();
                if (!empty($practiceLocation[0])) {
                    $value['practice_location'] = $practiceLocation[0];
                    $value['city_state'] = $practiceLocation[0]['city_name'] . ', ' .
                        $practiceLocation[0]['abbreviation'];
                }

                // Adding Practice profile pic
                $value['real_logo_path'] = AssetsComponent::generateImgSrc(
                    $value['logo_path'],
                    'practices',
                    'L',
                    true
                );

                $accountPractices[$value['id']] = $value;
            }
        }

        return [
            'access_granted' => $accessGranted,
            'accountPractices' => $accountPractices,
            'practice_current_page' => $page,
            'practice_page_size' => $pageSize,
            'practice_total_records' => $totalPractices

        ];
    }

    /**
     * Get practices of given account
     * @param int $accountId
     * @param string $dataReturn - format of data return values: array(default) | list
     * @param bool $cache - use cache or not..
     * @return array
     */
    public static function getAccountPractices($accountId = 0, $dataReturn = 'array', $cache = true)
    {
        if ($accountId == 0) {
            return false;
        }

        // account management values
        $cacheTime = ($cache) ? Yii::app()->params['cache_medium'] : 0;
        $currentAccountOrg = OrganizationAccount::model()->cache($cacheTime)->find(
            'account_id=:account_id',
            array(':account_id' => $accountId)
        );

        $accountPracticeTable = "account_practice AS ap";

        if (!empty($currentAccountOrg) && $currentAccountOrg->connection == 'S') {
            $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId, true);

            $accountPracticeTable = sprintf(
                "(SELECT account_practice.practice_id AS id, all_providers,
                account_practice.practice_id, account_practice.account_id, account_practice.research_status
                FROM account_practice
                LEFT JOIN practice_details
                ON account_practice.practice_id = practice_details.practice_id
                WHERE account_id  = %d
                AND practice_type_id != 5
                    UNION
                SELECT provider_practice.practice_id AS id, all_providers,
                provider_practice.practice_id, account_provider.account_id, account_practice.research_status
                FROM provider_practice
                INNER JOIN account_provider ON provider_practice.provider_id = account_provider.provider_id
                INNER JOIN account_practice ON provider_practice.practice_id = account_practice.practice_id
                AND account_practice.account_id = %d
                LEFT JOIN practice_details
                ON account_practice.practice_id = practice_details.practice_id
                WHERE account_provider.account_id = %d
                AND account_provider.all_practices = 1
                AND practice_type_id != 5
                ) ap ",
                $accountId,
                $ownerAccountId,
                $accountId
            );
        }

        $sql = sprintf(
            "SELECT
              ap.practice_id AS id, ap.practice_id,
              pra.name AS name,
              pra.name AS practice_name,
              pra.address_full AS practice_address,
              pra.phone_number,
              pra.operatories,
              ap.research_status,
              all_providers,
              ap.id AS account_practice_id
            FROM %s
            LEFT JOIN practice AS pra
                ON ap.practice_id = pra.id
            LEFT JOIN practice_details
                ON pra.id = practice_details.practice_id
            WHERE ap.account_id = %d
            AND practice_type_id != 5
            ORDER BY practice_name;",
            $accountPracticeTable,
            $accountId
        );

        $arrAccountPractices = Yii::app()->db->createCommand($sql)->queryAll();

        if ($dataReturn == 'array') {
            return $arrAccountPractices;
        } elseif ($dataReturn == 'list') {

            $lstOwnedPractice = '';
            foreach ($arrAccountPractices as $value) {
                $lstOwnedPractice .= '"' . $value['practice_id'] . '",';
            }
            $lstOwnedPractice = substr($lstOwnedPractice, 0, -1);
            return $lstOwnedPractice;
        }
    }

    /**
     * Get practices of given account in a single field
     * @param int $accountId
     * @return string
     */
    public static function getGroupedAccountPractices($accountId)
    {
        $sql = sprintf(
            "SELECT SQL_NO_CACHE GROUP_CONCAT(practice_id) AS practices
            FROM account_practice
            WHERE account_id = %d;",
            $accountId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryScalar();
    }

    /**
     * Get all practices that belong to a given account and are linked to providers
     * @param int $accountId
     * @param bool $mustHaveProviders
     * @return boolean
     */
    public static function getAccountPracticesWithProviders($accountId = 0, $mustHaveProviders = true)
    {
        if ($accountId > 0) {
            $sql = sprintf(
                "SELECT DISTINCT practice.id AS practice_id, practice.name AS practice_name,
                practice.address AS practice_address, practice.phone_number,
                practice.address_full AS address_full
                FROM account_practice
                INNER JOIN practice
                    ON account_practice.practice_id = practice.id
                %s JOIN provider_practice
                    ON practice.id = provider_practice.practice_id
                WHERE account_practice.account_id = '%s'
                ORDER BY practice_name;",
                ($mustHaveProviders ? 'INNER' : 'LEFT'),
                $accountId
            );
            return Yii::app()->db->createCommand($sql)->queryAll();
        }
        return false;
    }

    /**
     * PADM: Get an account's practices as long as they have ratings
     * @param int $accountId
     * @return array
     */
    public static function getAccountPracticesWithRatings($accountId = 0)
    {

        if (empty($accountId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT DISTINCT(ap.practice_id)
            FROM account_practice AS ap
            INNER JOIN provider_rating AS pr
                ON ap.practice_id = pr.practice_id
            WHERE ap.account_id = %d
                AND pr.status IN ('A', 'P')
                AND pr.practice_id > 0
            ORDER BY ap.practice_id;",
            $accountId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * PADM: Get an account's practices with their RH (Google mobile, Yelp) if they have them
     * Used in the ReviewRequest screen, this is the list of practices to choose from
     * @param int $accountId
     * @return array
     */
    public static function getAccountPracticesWithRHLinks($accountId)
    {
        $currentAccountOrg = OrganizationAccount::model()->cache(Yii::app()->params['cache_medium'])->find(
            'account_id=:account_id',
            array(':account_id' => $accountId)
        );

        $accountPracticeTable = 'account_practice';

        if (!empty($currentAccountOrg)) {
            if ($currentAccountOrg->connection == 'S') {
                $ownerAccountId = OrganizationAccount::switchOwnerAccountID($accountId, true);
                $accountPracticeTable = sprintf(
                    '(SELECT account_practice.practice_id, account_practice.account_id
                    FROM account_practice
                    WHERE account_id = %d
                    UNION
                    SELECT provider_practice.practice_id, account_provider.account_id
                    FROM provider_practice
                    INNER JOIN account_provider
                        ON provider_practice.provider_id = account_provider.provider_id
                    INNER JOIN account_practice
                        ON provider_practice.practice_id = account_practice.practice_id
                        AND account_practice.account_id = %d
                    WHERE account_provider.account_id = %d AND account_provider.all_practices = 1
                    ) AS account_practice',
                    $accountId,
                    $ownerAccountId,
                    $accountId
                );
            }
        }

        $sql = sprintf(
            "SELECT account_practice.account_id, practice.id AS practice_id, practice.name AS name,
            practice.address AS practice_address, practice.address_2 AS practice_address_2,
            practice.phone_number AS phone_number, google.url AS google_url,
            yelp.url AS yelp_url, account.country_id
            FROM account
            INNER JOIN %s
                ON account.id = account_practice.account_id
            INNER JOIN practice
                ON account_practice.practice_id = practice.id
            LEFT JOIN practice_details
                ON practice.id = practice_details.practice_id
            LEFT JOIN provider_practice_listing AS google
                ON practice.id = google.practice_id
                AND google.provider_id IS NULL
                AND google.listing_type_id = %d
            LEFT JOIN provider_practice_listing AS yelp
                ON practice.id = yelp.practice_id
                AND yelp.provider_id IS NULL
                AND yelp.listing_type_id = %d
            WHERE account_practice.account_id = %d
            AND practice_details.practice_type_id != 5
            ORDER BY practice.name;",
            $accountPracticeTable,
            ListingType::GOOGLE_PROFILE_ID,
            ListingType::YELP_PROFILE_ID,
            $accountId
        );

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Get basic practice data for all account practices apart from the ID specified and circular references
     * @param int $accountId
     * @param int $practiceId
     * @return array
     */
    public static function getOtherAccountPractices($accountId, $practiceId)
    {
        $sql = sprintf(
            "SELECT practice.id AS practice_id, practice.name AS name, practice.address AS address
            FROM account_practice
            INNER JOIN practice
                ON account_practice.practice_id = practice.id
            WHERE account_practice.account_id = %d
                AND practice_id != %d
                AND (parent_practice_id != %d OR parent_practice_id IS NULL)
            ORDER BY name;",
            $accountId,
            $practiceId,
            $practiceId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get basic practice data for all account-linked practices apart from the ID specified and circular references
     * @param int $practiceId
     * @return array
     */
    public static function getAccountLinkedPractices($practiceId)
    {
        $sql = sprintf(
            "SELECT DISTINCT derived.id AS practice_id, derived.name AS name, derived.address AS address
            FROM practice base
            INNER JOIN account_practice base_ap
                ON base.id = base_ap.practice_id
            INNER JOIN account_practice derived_ap
                ON derived_ap.account_id = base_ap.account_id
            INNER JOIN practice derived
                ON derived_ap.practice_id = derived.id
            WHERE base.id = %d
                AND derived.id != base.id
                AND (derived.parent_practice_id != base.id OR derived.parent_practice_id IS NULL)
            ORDER BY name;",
            $practiceId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all account linked to a practice
     * @param int $practiceId
     * @return array
     */
    public static function getAccountsByPractice($practiceId = 0)
    {
        if ($practiceId == 0) {
            return array();
        }
        $sql = sprintf(
            "SELECT DISTINCT account_id
            FROM account_practice
            WHERE practice_id = %d;",
            $practiceId
        );

        return Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
    }

    /**
     * Get all practices without Installation
     * @param int $accountId
     * @param int $cpnInstallationId
     * @return array
     */
    public static function getAccountPracticesWithoutInstallation($accountId, $cpnInstallationId)
    {
        if (empty($cpnInstallationId)) {
            $sql = sprintf(
                "SELECT practice.id, practice.name, practice.address AS practice_address,
                practice.phone_number, practice.address_full AS address_full,
                practice.operatories,
                cpn_installation_location.id AS cpn_location_id,
                cpn_installation_location.ehr_location_data AS cpn_location_name
                FROM practice
                INNER JOIN account_practice
                    ON practice.id = account_practice.practice_id
                LEFT JOIN cpn_installation_practice
                    ON cpn_installation_practice.practice_id = account_practice.practice_id
                LEFT JOIN cpn_installation_location
                    ON cpn_installation_practice.cpn_installation_location_id = cpn_installation_location.id
                WHERE account_practice.account_id= %d;",
                $accountId
            );
        } else {
            $sql = sprintf(
                "SELECT practice.id, practice.name, practice.address AS practice_address,
                practice.phone_number, practice.address_full AS address_full,
                practice.operatories,
                cpn_installation_location.id AS cpn_location_id,
                cpn_installation_location.ehr_location_data AS cpn_location_name
                FROM practice
                INNER JOIN account_practice
                    ON practice.id = account_practice.practice_id
                INNER JOIN cpn_installation
                    ON cpn_installation.account_id= account_practice.account_id
                LEFT JOIN cpn_installation_practice
                    ON cpn_installation_practice.practice_id = account_practice.practice_id
                    AND cpn_installation_practice.cpn_installation_id = cpn_installation.id
                LEFT JOIN cpn_installation_location
                    ON cpn_installation_practice.cpn_installation_location_id = cpn_installation_location.id
                WHERE account_practice.account_id= %d
                    AND cpn_installation.id = %d;",
                $accountId,
                $cpnInstallationId
            );
        }

        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * get all Practices for this account
     * @param int $accountId
     * @return object
     */
    public static function getAllByAccountId($accountId = 0)
    {
        return AccountPractice::model()->findAll("account_id = :account_id", array(":account_id" => $accountId));
    }

    /**
     * Check if the practice or related account are in research mode
     * @param int $practiceId
     * @return bool
     */
    public static function isInResearchMode($practiceId = null)
    {
        if (is_null($practiceId)) {
            return false;
        }

        $sql = sprintf(
            "SELECT account_id, research_mode, research_status
            FROM account_practice
            LEFT JOIN account
                ON account_practice.account_id = account.id
            WHERE practice_id = %d",
            $practiceId
        );
        $data = Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryAll();

        $return = false;
        if (!empty($data)) {
            foreach ($data as $v) {
                if ($v['research_mode'] != 0 || $v['research_status'] != 'Published') {
                    $return = true;
                    break;
                }
            }
        }

        return $return;
    }

    /**
     * Unlink a practice from an account (account_practice)
     * @param int $ownerAccountId
     * @param int $practiceId
     * @return bool
     * @throws Exception
     */
    public static function unlinkPractice($ownerAccountId = 0, $practiceId = 0, $force = false)
    {
        $result['success'] = true;
        $result['error'] = array();

        ETLComponent::startTransaction();

        if ($force) {
            ProviderPracticeWidget::deleteWidget($ownerAccountId, 0, $practiceId);
            AccountDevice::deleteReviewHub($ownerAccountId, $practiceId, false);
        } else {
            $arrWidget = ProviderPracticeWidget::getWidgetFromProviderAccount(
                $ownerAccountId,
                $practiceId,
                false,
                'ALL',
                true,
                $practiceId
            );

            if ($arrWidget) {
                $error = "This practice has associated widgets. Please remove the widgets and try again.";
                $result['success'] = false;
                $result['error'][]['practice_id'][] = $error;
                return $result;
            }
        }

        // get PracticeInfo
        $objPractice = Practice::model()->findByPk($practiceId);

        // get all managers && staff for this account
        $arrManagers = OrganizationAccount::getManagers($ownerAccountId);

        foreach ($arrManagers as $manager) {
            $accountId = $manager['id'];

            $accountPractices = AccountPractice::model()->findAll(
                'practice_id = :practice_id AND account_id = :account_id',
                array(':practice_id' => $practiceId, ':account_id' => $accountId)
            );
            foreach ($accountPractices as $accountPractice) {

                // delete account_practice
                $settingsSaved = $result['success'] = $accountPractice->delete();
                if ($settingsSaved) {

                    // save to audit log and notify changes to phabricator
                    AuditLog::create(
                        'D',
                        'Unlinked practice #' . $accountPractice->practice_id
                            . ' (' . $objPractice->name . ') from account',
                        true
                    );

                    SqsComponent::sendMessage('importAccountEmr', $ownerAccountId);
                } else {
                    $result['error'][] = $accountPractice->getErrors();
                }
            }
        }

        ETLComponent::commitTransaction();

        return $result;
    }

    /**
     * Update profile population status
     *
     * @params integer $practiceId
     * @params string $newStatus
     * @params integer $accountId
     * @params integer $adminAccountId
     */
    public static function updateProfilePopulationStatus($practiceId, $newStatus, $accountId, $adminAccountId)
    {
        $accountPractice = AccountPractice::model()->find(
            'account_id=:account_id AND practice_id=:practiceId',
            array(':account_id' => $accountId, ':practiceId' => $practiceId)
        );

        $prevStatus = $accountPractice->research_status;
        $accountPractice->research_status = $newStatus;
        $result = $accountPractice->save();

        // save in researh mode log
        AuditLog::saveResearchModeLog(
            $accountId,
            'practice',
            $practiceId,
            $newStatus,
            $prevStatus,
            'Profile Population Mode',
            $adminAccountId
        );

        return $result;
    }

    /**
     * Get practices for Reputation scans by account ID
     *
     * @params int $providerId
     *
     * @return array
     */
    public function getPracticesForRepinsights($accountId, $practiceId = null)
    {
        if ($practiceId) {
            $accountPractices = AccountPractice::model()->findAll(
                'account_id = :account_id AND practice_id = :practice_id',
                [
                    ':account_id' => $accountId,
                    ':practice_id' => $practiceId
                ]
            );
        } else {
            $accountPractices = AccountPractice::model()->findAll(
                'account_id = :account_id',
                [':account_id' => $accountId]
            );
        }

        $result = [];
        foreach ($accountPractices as $accountpractice) {
            $account = $accountpractice->account;

            $item = [];
            if ($account) {
                $item['PDI_PartnerID'] = $account->partner_site_id;
            }

            $practiceId = $accountpractice->practice_id;

            if (!empty($practiceId)) {
                $practice = Practice::model()->findByPk($practiceId);

                $item['PDI_PracticeID'] = $practice->id;

                // Get the Client GUID
                $sql = "SELECT
                    t.match_column_value
                FROM
                    prd01.idp_entity_match t
                    INNER JOIN prd01.idp_entity_match idp_organization ON
                        idp_organization.idp_provision_id = 7
                        AND idp_organization.match_column_name = 'client_guid'
                        AND idp_organization.internal_entity_type = 'organization'
                        AND idp_organization.internal_column_name = 'id'
                        AND idp_organization.retrieved_entity_type = 'enterprise'
                        AND idp_organization.retrieved_column_name = 'enterprise_guid'
                        AND t.match_column_value = idp_organization.match_column_value
                    INNER JOIN prd01.organization_account ON
                        organization_account.organization_id = idp_organization.internal_column_value
                        AND CONNECTION = 'O'
                WHERE
                    t.match_column_name = 'client_guid'
                    AND t.internal_entity_type = 'practice'
                    AND t.internal_column_name = 'id'
                    AND t.retrieved_entity_type = 'practice'
                    AND t.retrieved_column_name = 'practice_external_id'
                    AND t.internal_column_value = :practice_id
                    AND organization_account.account_id = :account_id";
                $iem = IdpEntityMatch::model()->findBySql(
                    $sql,
                    [
                        ':account_id' => $accountId,
                        ':practice_id' => $practiceId
                    ]
                );
                if ($iem) {
                    $item['SmbGuid'] = $iem->match_column_value;
                }

                $item['Addresses'] = [];
                $address = [];

                $address['AddressLabel'] = $practice->name;
                $address['Line1'] = $practice->address;
                $address['Line2'] = $practice->address_2;
                $address['Line3'] = '';
                $address['Line4'] = '';

                $location = $practice->location;
                if ($location) {
                    $address['City'] = $location->city->name;
                    $address['State'] = $location->city->state->abbreviation;
                    $address['Zip'] = $location->zipcode;
                    $address['LocationID'] = $practice->id;
                }

                $item['Addresses'][] = $address;
            }

            $result[] = $item;
        }

        return $result;
    }

    /**
     * This will update the claimed value on the specified practice, also will set the nasp_date to NULL if claimed
     * is different than Practice::CLAIMED_PAYING_ACCOUNT
     *
     * Return the new claimed value or false if the practice was not found.
     *
     * @param int $practiceId
     * @return int
     * @throws Exception
     */
    public static function updateClaimed($practiceId)
    {
        // try to find the practice
        $practice = Practice::model()->findByPk($practiceId);

        // if the practice wasn't found return false
        if (empty($practice)) {
            return false;
        }

        // default
        $claimed = null;

        // find if the practice is related at least to one account
        $hasAccount = AccountPractice::model()->exists('practice_id = :practice_id', [':practice_id' => $practiceId]);

        if (empty($hasAccount)) {
            // the practice is not related to any account
            $claimed = Practice::CLAIMED_NO_ACCOUNT;
        } else {
            // check if the practice is related at least to an active or paused organization
            $query = Yii::app()->db->createCommand(
                'SELECT ap.id
                FROM account_practice ap
                JOIN organization_account oa ON oa.account_id = ap.account_id
                JOIN organization o ON o.id = oa.organization_id AND oa.connection = "O"
                WHERE
                    ap.practice_id = :practice_id
                    AND (o.status = "A" OR o.status = "P")
                LIMIT 1'
            );
            $query->bindValue(':practice_id', $practiceId);
            $row = $query->queryRow();

            // set claimed
            if ($row) {
                $claimed = Practice::CLAIMED_PAYING_ACCOUNT;
            } else {
                $claimed = Practice::CLAIMED_FREEMIUM_ACCOUNT;
            }
        }

        // flag to determine if must save the practice
        $mustSave = false;

        // if claimed is different update it
        if ($practice->claimed != $claimed) {
            $practice->claimed = $claimed;
            $mustSave = true;
        }

        // nasp date should be null if claimed is other than CLAIMED_PAYING_ACCOUNT
        if ($claimed != Practice::CLAIMED_PAYING_ACCOUNT && $practice->nasp_date != null) {
            $practice->nasp_date = null;
            $mustSave = true;
        }

        // update the record only if were changes for performance
        if ($mustSave) {
            $practice->save();
        }

        // returns the new claimed value
        return $claimed;
    }

    /**
     * Get all practices linked to an account
     * @param int $accountId
     * @return array
     */
    public static function getPracticesConcatByAccount($accountId = 0)
    {
        if ($accountId == 0) {
            return array();
        }

        // extend the maximum of characters in group_concat
        Yii::app()->db->createCommand('SET SESSION group_concat_max_len = 1000000;')->execute();
        Yii::app()->db->createCommand('SET @@group_concat_max_len = 1000000;')->execute();

        $sql = sprintf(
            "SELECT GROUP_CONCAT(practice_id) AS practices
            FROM account_practice
            WHERE account_id = %d;",
            $accountId
        );
        return Yii::app()->db->cache(Yii::app()->params['cache_short'])->createCommand($sql)->queryScalar();
    }
}
