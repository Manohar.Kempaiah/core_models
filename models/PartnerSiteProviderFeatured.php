<?php

Yii::import('application.modules.core_models.models._base.BasePartnerSiteProviderFeatured');

class PartnerSiteProviderFeatured extends BasePartnerSiteProviderFeatured
{

    /**
     * This array is filled in beforeSave and iterate in the afterSave
     * @var array
     */
    public $arrayTwilioNumbers;
    public $organization;
    public $featured_partner_site;
    public $featured_active;
    public $featured;
    public $npi_id;
    public $credential_id;
    public $prefix;
    public $first_name;
    public $last_name;

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    public function afterSave()
    {
        if ($this->isNewRecord) {

            // Look for previous records for the same partner site and provider
            $previousRecords = PartnerSiteProviderFeatured::model()->exists(
                'partner_site_id = :partner_site_id
                AND provider_id = :provider_id
                AND id != :id
                AND active = 1',
                array(
                    ':id' => $this->id,
                    ':partner_site_id' => $this->partner_site_id,
                    ':provider_id' => $this->provider_id
                )
            );

            // If there's no previous records, generate twilio number
            if (!$previousRecords) {
                // Send twilio numbers generation to a queue
                $queueBody = array(
                    'partner_site_provider_featured_id' => $this->id
                );
                SqsComponent::sendMessage('partnerSiteProviderFeaturedGenerateTwilioNumbers', $queueBody);
            }
        }

        return parent::afterSave();
    }

    /**
     * Feature a provider in a partner site
     * @param integer $partnerSiteId
     * @param integer $providerId
     */
    public static function featureProvider($partnerSiteId, $providerId)
    {

        //magic account_id = 1: default to tech@corp.doctor.com
        if (php_sapi_name() != 'cli') {
            $accountId = (!empty(Yii::app()->user->id) ? Yii::app()->user->id : 1);
        } else {
            $accountId = 1;
        }

        $pspf = PartnerSiteProviderFeatured::model()->find(
            'partner_site_id=:partnerSiteId AND provider_id=:providerId',
            array(':partnerSiteId' => $partnerSiteId, ':providerId' => $providerId)
        );
        if (!$pspf) {
            $pspf = new PartnerSiteProviderFeatured;
            $pspf->partner_site_id = $partnerSiteId;
            $pspf->provider_id = $providerId;
            $pspf->date_added = date('Y-m-d H:i:s');
            $pspf->added_by_account_id = $accountId;
        } else {
            $pspf->date_updated = date('Y-m-d H:i:s');
            $pspf->updated_by_account_id = $accountId;
        }
        $pspf->active = 1;
        $pspf->save();

        return $pspf;
    }

    /**
     * Get tracked numbers for all the practices associated with the featured provider
     * @param int $numbersAvailable
     * @param int $practiceId
     *
     * @return boolean
     */
    public function getTrackedNumbers($numbersAvailable = 0, $practiceId = 0)
    {
        ini_set('max_execution_time', 0);

        $this->arrayTwilioNumbers = array();

        $twilioPartner = TwilioPartner::model()->find(
            "partner_site_id = :partner_site_id",
            array(":partner_site_id" => $this->partner_site_id)
        );

        if (!$twilioPartner) {
            // return if we don't have a twilio partner for this partner_site
            return false;
        }

        $twilioPartnerId = $twilioPartner->id;

        /**
         * Search twilio number
         */
        if (empty($practiceId)) {
            $sql = sprintf(
                "SELECT practice_id, `location`.zipcode, practice.phone_number
                FROM `provider`
                LEFT JOIN provider_practice ON provider_practice.provider_id = `provider`.id
                LEFT JOIN practice ON practice.id = provider_practice.practice_id
                LEFT JOIN `location` ON practice.location_id = `location`.id
                WHERE provider_id = %d;",
                $this->provider_id
            );
        } else {
            $sql = sprintf(
                "SELECT practice.id AS practice_id, location.zipcode, practice.phone_number
                FROM practice
                LEFT JOIN location ON practice.location_id = `location`.id
                WHERE practice.id = %d;",
                $practiceId
            );
        }
        $practices = Yii::app()->db->createCommand($sql)->queryAll();

        foreach ($practices as $practice) {

            $sql = sprintf(
                "SELECT COUNT(1) AS total
                FROM twilio_number
                WHERE provider_id IS NULL
                    AND twilio_partner_id = %d
                    AND practice_id = %d ",
                $twilioPartnerId,
                $practice['practice_id']
            );
            $twNumbers = Yii::app()->db->createCommand($sql)->queryRow();

            if ($twNumbers['total'] == 0) {

                $jsonTwilioNumber = self::actionTwilioSearchByZipCode(null, $practice['phone_number']);
                $arrayTwilioNumber = json_decode($jsonTwilioNumber, true);

                if ($arrayTwilioNumber['success']) {

                    $this->arrayTwilioNumbers['number'][] = $arrayTwilioNumber['number'];
                    $this->arrayTwilioNumbers['practice'][] = $practice['practice_id'];
                } else {

                    $jsonTwilioNumber = self::actionTwilioSearchByZipCode($practice['zipcode'], null);
                    $arrayTwilioNumber = json_decode($jsonTwilioNumber, true);

                    if ($arrayTwilioNumber['success']) {

                        $this->arrayTwilioNumbers['number'][] = $arrayTwilioNumber['number'];
                        $this->arrayTwilioNumbers['practice'][] = $practice['practice_id'];
                    } elseif ($arrayTwilioNumber['errorType'] == 1) {

                        $sql = sprintf(
                            "SELECT city.state_id
                            FROM `location`
                            INNER JOIN city on city.id = `location`.city_id
                            WHERE zipcode = %d; ",
                            $practice['zipcode']
                        );
                        $state = Yii::app()->db->createCommand($sql)->queryRow();

                        $sql = sprintf(
                            "SELECT DISTINCT `location`.zipcode
                            FROM city
                            INNER JOIN `location` ON `location`.city_id = city.id
                            WHERE state_id = %d; ",
                            $state['state_id']
                        );
                        $nearZipcodes = Yii::app()->db->createCommand($sql)->queryAll();

                        $nearTwilioNumbers = null;

                        foreach ($nearZipcodes as $zipcode) {

                            $jsonNearTwilioNumber = self::actionTwilioSearchByZipCode($zipcode['zipcode'], null);

                            $arrayNearTwilioNumber = json_decode($jsonNearTwilioNumber, true);

                            if ($arrayNearTwilioNumber['success']) {

                                $nearTwilioNumbers = $arrayNearTwilioNumber['number'];
                                break;
                            }
                        }

                        if ($nearTwilioNumbers > 0) {
                            $this->arrayTwilioNumbers['number'][] = $nearTwilioNumbers;
                            $this->arrayTwilioNumbers['practice'][] = $practice['practice_id'];
                        } else {

                            // No numbers available for this city
                            echo $jsonTwilioNumber;
                            if ($numbersAvailable == 0) {
                                return false;
                            }
                        }
                    } else {

                        // Error processing search
                        echo $jsonTwilioNumber;
                        if ($numbersAvailable == 0) {
                            return false;
                        }
                    }
                }
            }
        }

        return true;
    }

    /**
     * Creates a twilio number for the featured provider
     * @param integer $practiceId
     * @param bool $addProvider
     * @return bool
     */
    public function saveTrackedNumbers($practiceId = 0, $addProvider = false)
    {
        $twilioLogServerAddress = Yii::app()->params->servers['dr_pho_hn'];

        $sql = sprintf(
            "SELECT p.prefix, p.first_name, p.last_name, o.tracked_number_status
                FROM provider p
                INNER JOIN account_provider ap ON p.id = ap.provider_id
                INNER JOIN organization_account oa ON ap.account_id = oa.account_id
                INNER JOIN organization o ON oa.organization_id = o.id
            WHERE p.id = %d",
            $this->provider_id
        );

        $provider = Yii::app()->db->createCommand($sql)->queryRow();

        if (empty($provider)) {
            echo $this->provider_id . " - Provider does not exist \n";
            return false;
        } elseif ($provider['tracked_number_status'] == "Inactive") {
            echo " The organization of this provider " . $this->provider_id . " - has disabled tracking numbers.\n ";
            return false;
        }

        $targetName = $provider['prefix'] . ' ' . $provider['first_name'] . ' ' . $provider['last_name'];

        $twilioPartner = TwilioPartner::model()->cache(Yii::app()->params['cache_short'])->find(
            "partner_site_id = :partner_site_id",
            array(":partner_site_id" => $this->partner_site_id)
        );

        if (!$twilioPartner) {
            // return if we don't have a twilio partner for this partner_site
            $this->addError('id', 'Twilio Partner does not exist for this partner site');
            return false;
        }

        $twilioPartnerId = $twilioPartner->id;

        /**
         * Insert the Twilio Numbers
         */
        if (is_array($this->arrayTwilioNumbers) && isset($this->arrayTwilioNumbers['number'])) {

            foreach ($this->arrayTwilioNumbers['number'] as $key => $purchasedNumber) {

                // Do we send a practice_id through the params?
                if (!empty($practiceId) && ($practiceId != $this->arrayTwilioNumbers['practice'][$key])) {
                    // Yes, only create twilio_number for this practice
                    continue;
                }

                // output message to the console
                if (php_sapi_name() == 'cli') {
                    echo "------ TwilioNumber - " . $purchasedNumber . " -  Practice - " .
                        $this->arrayTwilioNumbers['practice'][$key] . " - Provider - " . $this->provider_id . " - " .
                        PHP_EOL;
                }

                $newTwilioNumber = new TwilioNumber;
                $newTwilioNumber->id = null;
                $newTwilioNumber->twilio_number = $purchasedNumber;
                $newTwilioNumber->enabled = 0;
                $newTwilioNumber->is_dynamic = 0;
                $newTwilioNumber->practice_id = $this->arrayTwilioNumbers['practice'][$key];
                if (Common::isTrue($addProvider)) {
                    $newTwilioNumber->provider_id = $this->provider_id;
                }
                $newTwilioNumber->twilio_partner_id = $twilioPartnerId;
                $newTwilioNumber->enabled_recording = 0;

                $sql = sprintf(
                    "SELECT organization.status
                    FROM organization
                    INNER JOIN organization_account ON organization_account.organization_id = organization.id
                    INNER JOIN account_provider ON account_provider.account_id = organization_account.account_id
                    INNER JOIN provider ON provider.id = account_provider.provider_id
                    WHERE provider.id = %d
                    ORDER BY `status` = 'A' DESC;",
                    $this->provider_id
                );
                $organizationActive = Yii::app()->db->createCommand($sql)->queryRow();

                if (isset($organizationActive['status']) && $organizationActive['status'] == "A") {
                    $newTwilioNumber->enabled_recording = 1;
                }

                //$client = new Yii_Services_Twilio();

                if ($newTwilioNumber->save()) {

                    $newTwilioNumber->refresh();
                    $flag="";
                    try {
                        $flag=1;
                        //purchase the selected PhoneNumber
                        // $client->account->incoming_phone_numbers->create(array(
                        //     'PhoneNumber' => $purchasedNumber,
                        //     'VoiceUrl' => 'https://' . $twilioLogServerAddress .
                        //         '/twilioLog/start?twilio_number_id=' .
                        //         $newTwilioNumber->id,
                        //     'VoiceFallbackUrl' => 'https://' .
                        //         $twilioLogServerAddress . '/twilioLog/error',
                        //     'StatusCallback' => 'https://' .
                        //         $twilioLogServerAddress . '/twilioLog/end',
                        //     'FriendlyName' => 'FWD (DNI): ' . urlencode($targetName)
                        // ));
                    } catch (Exception $e) {

                        return CJavaScript::JsonEncode(array(
                            'success' => false,
                            'errorType' => 2,
                            'error' => "Error processing search: {$e->getMessage()}"
                        ));
                    }

                    // enable dynamic number and save it
                    $newTwilioNumber->enabled = 1;
                    if (!$newTwilioNumber->save()) {
                        $this->addError("id", "Couldn't save phone number.");
                        return false;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Generates tracked numbers for the featured providers of a given partner site
     * @param int $partnerSiteId Id of the partner site.
     */
    public static function generateTrackedNumbers($partnerSiteId)
    {
        $twilioPartner = TwilioPartner::model()->find(
            "partner_site_id = :partner_site_id",
            array(
                ":partner_site_id" => $partnerSiteId
            )
        );

        // Get featured providers without twilio numbers
        $sql = sprintf(
            "SELECT DISTINCT fp.id
            FROM partner_site_provider_featured fp
            INNER JOIN provider_practice pp
            ON fp.provider_id = pp.provider_id
            LEFT JOIN twilio_number tn
            ON tn.practice_id = pp.practice_id AND tn.twilio_partner_id = %d
            WHERE tn.id IS NULL AND fp.partner_site_id = %d
            AND fp.active=1 AND tn.provider_id IS NULL",
            $twilioPartner->id,
            $partnerSiteId
        );
        $featuredProviders = Yii::app()->db->createCommand($sql)->query();

        foreach ($featuredProviders as $fp) {
            // Generate the twilio number for each of them
            $model = PartnerSiteProviderFeatured::model()->findByPk($fp["id"]);
            $model->getTrackedNumbers(0);
            $model->saveTrackedNumbers();
        }
    }

    /**
     * Generates tracked numbers for the featured providers of all partner sites
     */
    public static function cronTrackedNumbers()
    {

        $sql = "SELECT DISTINCT partner_site.partner_id,
        partner_site.id AS partner_site_id, partner_site.name, base_network, partner_site.custom_provider
        FROM partner_site
        INNER JOIN twilio_partner ON twilio_partner.partner_site_id= partner_site.id
        WHERE custom_provider_featured IS NOT NULL
        AND for_showcase=0
        AND partner_site.active=1
        AND set_providers_featured=1
        ORDER BY partner_site.name; ";
        $partnerSite = Yii::app()->db->createCommand($sql)->query();

        foreach ($partnerSite as $ps) {

            self::generateTrackedNumbers($ps['partner_site_id']);
        }
    }

    /**
     * Search for Twilio phone numbers near a zipcode or another phone number
     *
     * @todo move this function to another model since it's not actually related to partner_site_provider_featured
     *
     * @param int $zipcode
     * @param int $phoneNumber
     * @return string
     */
    public function actionTwilioSearchByZipCode($zipcode, $phoneNumber)
    {

        $rnumber = rand(1000000000, 9999999999);

        return CJavaScript::JsonEncode(array(
            'success' => true,
            'number' => $rnumber,
        ));

        $numbers = null;
        $searchParams = array();

        $client = new Yii_Services_Twilio();

        if ($phoneNumber != "") {
            $searchParams['NearNumber'] = $phoneNumber;
        } else {
            $searchParams['InPostalCode'] = $zipcode;
        }

        try {

            // Initiate US Local PhoneNumber search with $searchParams list
            $numbers = $client->account->available_phone_numbers->getList('US', 'Local', $searchParams);

            // If we did not find any phone numbers let the user know
            if (sizeof($numbers->available_phone_numbers) == 0) {

                return CJavaScript::JsonEncode(array(
                    'success' => false,
                    'errorType' => 1,
                    'error' => 'No numbers available for purchase'
                ));
            } else {
                foreach ($numbers->available_phone_numbers as $availableNumber) {
                    $purchasedNumber = StringComponent::normalizePhoneNumber($availableNumber->phone_number, true);

                    // check that the number isn't already bought
                    $cnt = (int)TwilioNumber::model()->count(
                        "twilio_number = :number",
                        array(":number" => $purchasedNumber)
                    );
                    $notReserved = (empty($this->arrayTwilioNumbers['number'])
                        || !in_array($purchasedNumber, $this->arrayTwilioNumbers['number']));
                    if ($cnt == 0 && $notReserved) {
                        return CJavaScript::JsonEncode(array(
                            'success' => true,
                            'number' => $purchasedNumber,
                        ));
                    }
                }
                // if all numbers were bought, return an error
                return CJavaScript::JsonEncode(array(
                    'success' => false,
                    'errorType' => 1,
                    'error' => 'No numbers available for purchase'
                ));
            }
        } catch (Exception $e) {

            return CJavaScript::JsonEncode(array(
                'success' => false,
                'errorType' => 2,
                'error' => "Error processing search: {$e->getMessage()}"
            ));
        }
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @param string $partnerSiteId
     * @param string $providerId
     * @param string $practiceId
     * @return boolean
     */
    public function beforeDelete()
    {
        $deleteOk = self::deleteTwilioNumbers();

        if ($deleteOk) {
            return parent::beforeDelete();
        } else {
            $this->addError("id", "Couldn't delete partner site's featured provider.");
            return false;
        }
    }

    /**
     * Delete Twilio phone numbers
     *
     * @param int $partnerSiteId
     * @param int $providerId
     * @param int $practiceId
     * @return bool
     */
    public function deleteTwilioNumbers($partnerSiteId = '', $providerId = '', $practiceId = '')
    {

        /**
         * When beforeDelete is called then inactive a provider
         */
        if (empty($partnerSiteId) && !empty($this->partner_site_id)) {
            $partnerSiteId = $this->partner_site_id;
        }
        if (empty($providerId)) {
            $providerId = $this->provider_id;
        }


        /**
         * Lookup for the Twilio Partner
         */
        $twilioPartner = TwilioPartner::model()->find(
            "partner_site_id = :partner_site_id",
            array(":partner_site_id" => $partnerSiteId)
        );
        $twilioPartnerId = !empty($twilioPartner->id) ? $twilioPartner->id : null;

        // Do we have the 3 needed params?
        if ($twilioPartnerId > 0 && $providerId > 0 && $practiceId > 0) {

            // Yes, so search & delete
            $twilioNumber = TwilioNumber::model()->find(
                'twilio_partner_id=:twilio_partner_id AND provider_id=:provider_id AND practice_id=:practice_id',
                array(
                    ':twilio_partner_id' => $twilioPartnerId,
                    ':provider_id' => $providerId,
                    ':practice_id' => $practiceId
                )
            );

            // Delete Twilio Number
            if (!empty($twilioNumber) && !$twilioNumber->delete()) {
                return false;
            }
        } else {
            // Nop, we need a practice

            // Get practices linked to this provider
            $sql = sprintf(
                "SELECT practice_id, location.zipcode
            FROM `provider`
            INNER JOIN provider_practice ON provider_practice.provider_id = provider.id
            INNER JOIN practice ON practice.id = provider_practice.practice_id
            INNER JOIN location ON practice.location_id = location.id
            WHERE provider_id = %d",
                $providerId
            );
            if (!empty($practiceId)) {
                // limit the query only to the selected practice
                $sql .= sprintf(" AND practice_id = %d", $practiceId);
            }
            $practices = Yii::app()->db->createCommand($sql)->queryAll();

            if (!empty($practices)) {

                foreach ($practices as $practice) {

                    $practiceId = $practice['practice_id'];

                    // How many providers have this practice?
                    $sql = sprintf(
                        "SELECT COUNT(*) AS total
                    FROM `provider`
                    INNER JOIN provider_practice
                        ON provider_practice.provider_id = provider.id
                    INNER JOIN partner_site_provider_featured
                        ON partner_site_provider_featured.provider_id = provider.id
                    WHERE practice_id = %d
                        AND provider.id <> %d
                    GROUP BY practice_id;",
                        $practiceId,
                        $providerId
                    );
                    $providers = Yii::app()->db->createCommand($sql)->queryRow();

                    if (is_array($providers) && $providers['total'] == 0 && $twilioPartnerId) {

                        $sql = sprintf(
                            "SELECT id, twilio_number
                        FROM twilio_number
                        WHERE provider_id IS NULL
                            AND twilio_partner_id = %d
                            AND practice_id = %d ",
                            $twilioPartnerId,
                            $practiceId
                        );
                        $twNumbers = Yii::app()->db->createCommand($sql)->queryRow();

                        if ($twNumbers["id"] > 0) {

                            $twilioNumber = TwilioNumber::model()->findByPk($twNumbers["id"]);
                            // Delete Twilio Number
                            if (!$twilioNumber->delete()) {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        return true;
    }

    /**
     * Export Partner Site
     *
     * @param int $partnerSiteId
     */
    public static function exportPartnerSite($partnerSiteId)
    {

        ini_set('max_execution_time', 0);

        $fp = fopen('php://temp', 'w');

        $sql = sprintf(
            "SELECT custom_provider_export, display_name
            FROM partner_site
            WHERE id=%d; ",
            $partnerSiteId
        );
        $partnerSite = Yii::app()->db->createCommand($sql)->queryRow();

        $sqlExport = $partnerSite['custom_provider_export'];
        $displayName = $partnerSite['display_name'];

        if ($sqlExport != "") {

            $rawData = Yii::app()->db->createCommand($sqlExport)->queryAll();
            if (!empty($rawData)) {
                $arrKeys = array_keys($rawData[0]);
            } else {
                $arrKeys = null;
            }
            if (!empty($arrKeys)) {
                $arrExport = array();

                foreach ($arrKeys as $key => $value) {

                    if ($key > 0) {

                        $arrExport[$key] = str_replace("_", " ", $value);
                    }
                }

                fputcsv($fp, $arrExport);

                $arrExport = array();

                $providerDuplicate = 0;
                $providerTriplicate = 0;

                foreach ($rawData as $key => $arrProvider) {

                    if (@$rawData[($key - 1)]["Provider_ID"] != $arrProvider["Provider_ID"]) {

                        $providerDuplicate = 0;
                        $providerTriplicate = 0;
                    } elseif (
                        @$rawData[($key - 1)]["Provider_ID"] == $arrProvider["Provider_ID"]
                        && $providerDuplicate == 1
                    ) {

                        $providerTriplicate = 1;
                    } elseif (@$rawData[($key - 1)]["Provider_ID"] == $arrProvider["Provider_ID"]) {

                        $providerDuplicate = 1;

                        if (array_key_exists("Primary_Specialty", $arrProvider)) {

                            $arrExport[($key - 1)]["Secondary_Specialty"] = ($arrProvider["Primary_Specialty"] == ""
                                ? " "
                                : $arrProvider["Primary_Specialty"]
                            );
                            $arrExport[($key - 1)]["Secondary_Sub_Specialty"] =
                                ($arrProvider["Primary_Sub_Specialty"] == "" ? " " :
                                $arrProvider["Primary_Sub_Specialty"]
                            );
                        }
                    }

                    if ($providerDuplicate == 0 && $providerTriplicate == 0) {

                        $providerDuplicate = 0;
                        $providerTriplicate = 0;

                        foreach ($arrKeys as $key2 => $value) {

                            if ($key2 > 0) {

                                $arrExport[$key][$value] = ($arrProvider[$value] == "" ? " " : $arrProvider[$value]);
                            }
                        }
                    }
                }

                foreach ($arrExport as $key => $arrProvider) {

                    fputcsv($fp, $arrProvider);
                }
            }
        }

        rewind($fp);
        Yii::app()->request->sendFile(
            $displayName . '_FeaturedProvider_' . date("m_d_Y H_i") . '.csv',
            stream_get_contents($fp)
        );
        fclose($fp);
    }

    /**
     * Export Practices
     *
     * @param int $partnerSiteId
     * @return csv
     */
    public static function exportPractices($partnerSiteId)
    {

        ini_set('max_execution_time', 0);

        $fp = fopen('php://temp', 'w');

        $sql = sprintf(
            "SELECT custom_practice_export, display_name
            FROM partner_site
            WHERE id=%d; ",
            $partnerSiteId
        );
        $partnerSite = Yii::app()->db->createCommand($sql)->queryRow();

        $sqlExport = $partnerSite['custom_practice_export'];
        $displayName = $partnerSite['display_name'];

        if ($sqlExport != "") {

            $totalRows = Yii::app()->db->createCommand($sqlExport)->query()->count();

            $sqlExport = trim($sqlExport);
            $sqlReal = (substr($sqlExport, -1) == ";" ? substr($sqlExport, 0, -1) : $sqlExport);
            $rowsCeil = ceil($totalRows / 1000) * 1000;
            $countRows = 0;

            while ($rowsCeil > $countRows) {

                $sqlLimit = " LIMIT " . $countRows . ",1000;";

                $rawData = Yii::app()->db->createCommand($sqlReal . $sqlLimit)->queryAll();
                $arrKeys = is_array($rawData) ? array_keys($rawData[0]) : null;
                if (!empty($arrKeys)) {

                    if ($countRows == 0) {
                        $arrExport = array();

                        foreach ($arrKeys as $key => $value) {

                            $arrExport[$key] = str_replace("_", " ", $value);
                        }

                        fputcsv($fp, $arrExport);
                    }

                    $arrExport = array();

                    foreach ($rawData as $key => $arrPractice) {

                        foreach ($arrKeys as $value) {

                            $arrExport[$key][$value] = ($arrPractice[$value] == "" ? " " : $arrPractice[$value]);
                        }
                    }

                    foreach ($arrExport as $key => $arrPractice) {

                        fputcsv($fp, $arrPractice);
                    }
                }
                $countRows += 1000;
            }
        }

        rewind($fp);
        Yii::app()->request->sendFile(
            $displayName . '_Practice_' . date("m_d_Y H_i") . '.csv',
            stream_get_contents($fp)
        );
        fclose($fp);
    }

    /**
     * Remove Provider int partner_site_provider_featured
     *
     * @param integer $partnerSiteId
     * @param integer $providerId
     * @return void
     */
    public function removeProvider($partnerSiteId, $providerId)
    {
        if ($this->id > 0 && $this->provider_id > 0 && $this->partner_site_id > 0) {
            $partnerSiteProviderFeatureId = $this->id;
        } else {
            $sql = sprintf(
                "SELECT id, active
            FROM partner_site_provider_featured
            WHERE partner_site_id=%d
            AND provider_id=%d; ",
                $partnerSiteId,
                $providerId
            );
            $partnerSiteProviderFeature = Yii::app()->db->createCommand($sql)->queryRow();
            $partnerSiteProviderFeatureId = $partnerSiteProviderFeature['id'];
        }

        // Have the provider any other features?
        $pspf = PartnerSiteProviderFeatured::model()->exists(
            'partner_site_id != :psId AND provider_id = :providerId',
            array(':psId' => $partnerSiteId, ':providerId' => $providerId)
        );
        if (!$pspf) {
            // Nop, setting provider->feature as zero
            $provider = Provider::model()->findByPk($providerId);
            $provider->featured = '0';
            $provider->save();
        }

        $model = PartnerSiteProviderFeatured::model()->findByPk($partnerSiteProviderFeatureId);
        if ($model->delete()) {
            echo CJavaScript::JsonEncode(array(
                'success' => true,
                'error' => 'All numbers deleted'
            ));
        }

        return true;
    }

    /**
     * Activate Provider
     *
     * @param int $partnerSiteId
     * @param int $providerId
     * @param int $activeValue
     * @return bool
     */
    public function activeProvider($partnerSiteId, $providerId, $activeValue)
    {

        $sql = sprintf(
            "SELECT id, active
            FROM partner_site_provider_featured
            WHERE partner_site_id=%d
            AND provider_id=%d; ",
            $partnerSiteId,
            $providerId
        );
        $partnerSite = Yii::app()->db->createCommand($sql)->queryRow();

        /**
         * If inactive a provider, have to delete twilio
         */
        if ($activeValue == 0) {
            self::deleteTwilioNumbers($partnerSiteId, $providerId);
        }

        /**
         * Update - PartnerSiteProviderFeatured
         */
        $model = PartnerSiteProviderFeatured::model()->findByPk($partnerSite["id"]);
        $model->active = $activeValue;
        $model->date_updated = date('Y-m-d H:i:s');
        if (php_sapi_name() != 'cli') {
            $model->updated_by_account_id = Yii::app()->user->id;
        } else {
            $model->updated_by_account_id = 1;
        }
        if ($model->update()) {

            $strActive = "Provider inactive";
            if ($activeValue == 1) {
                $strActive = "Provider active";
            }

            echo CJavaScript::JsonEncode(array(
                'success' => true,
                'error' => $strActive,
            ));
        }

        return true;
    }

    /**
     * Activate Partner Site
     *
     * @param int $partnerSiteId
     * @param int $providerId
     * @return bool
     */
    public function addProvider($partnerSiteId, $providerId)
    {
        /**
         * Validation - Provider already inserted
         */
        $sql = sprintf(
            "SELECT id
            FROM partner_site_provider_featured
            WHERE partner_site_id=%d
            AND provider_id=%d; ",
            $partnerSiteId,
            $providerId
        );
        $partnerSite = Yii::app()->db->createCommand($sql)->queryRow();

        if (is_array($partnerSite) && $partnerSite["id"] > 0) {
            echo CJavaScript::JsonEncode(array(
                'success' => false,
                'error' => 'Provider already inserted.'
            ));
            return false;
        }

        /**
         * Validation - provider can't be selected
         */
        $sql = sprintf("SELECT custom_provider FROM partner_site WHERE id=%d", $partnerSiteId);
        $partnerSite = Yii::app()->db->createCommand($sql)->queryRow();

        $availableProvider["id"] = "";

        if ($partnerSite['custom_provider'] != "") {

            $partnerSite['custom_provider'] = str_replace(
                '$$provider_id$$',
                $providerId,
                $partnerSite['custom_provider']
            );
            $availableProvider = Yii::app()->db->createCommand($partnerSite['custom_provider'])->queryRow();
        }

        if ($availableProvider["id"] == "") {

            echo CJavaScript::JsonEncode(array(
                'success' => false,
                'error' => "The provider selected isn't shared to the PartnerSite selected."
            ));
            return false;
        }

        /**
         * Set Featured
         */
        $provider = Provider::model()->findByPk($providerId);
        $provider->featured = '1';
        $provider->save();

        /**
         * Insert the Provider
         */
        $model = new PartnerSiteProviderFeatured;
        $model->partner_site_id = $partnerSiteId;
        $model->provider_id = $providerId;
        $model->is_automatic = 0;
        if (php_sapi_name() != 'cli') {
            $model->added_by_account_id = Yii::app()->user->id;
        } else {
            $model->added_by_account_id = 1;
        }
        if ($model->save()) {

            echo CJavaScript::JsonEncode(array(
                'success' => true,
                'error' => 'New number allocated'
            ));
        }

        return true;
    }

    /**
     * Search Partner Site
     *
     * @param type $partnerSiteId
     * @param type $model
     * @return type
     */
    public static function searchPartnerSite($partnerSiteId, $model)
    {

        ini_set('max_execution_time', 0);

        $arrayDataProvider = null;
        $searchProvider = null;
        $exportProvider = null;

        $sql = "SELECT name, display_name, id, custom_provider_featured
            FROM partner_site
            WHERE custom_provider IS NOT NULL
                AND set_providers_featured = '1'
                AND active = '1'
            ORDER BY display_name;";
        $partnerSite = Yii::app()->db->createCommand($sql)->queryAll();

        if ($partnerSiteId > 0) {

            $searchProvider = Yii::app()->request->getParam("PartnerSiteProviderFeatured");

            $sql = "SELECT custom_provider_featured, custom_provider_export
                FROM partner_site
                WHERE id = :partnerSiteId;";

            $command = Yii::app()->db->createCommand($sql);
            $command->bindParam(":partnerSiteId", $partnerSiteId, PDO::PARAM_INT);
            $providerFeatured = $command->queryRow();

            $sqlProvider = $providerFeatured["custom_provider_featured"];
            $exportProvider = ($providerFeatured["custom_provider_export"] != "" ? true : false);

            if (is_array($searchProvider)) {

                $arrSql = explode("WHERE", $sqlProvider, 2);
                $sqlCriteria = "";

                if (isset($searchProvider['npi_id']) && $searchProvider['npi_id'] != "") {

                    $model->npi_id = $searchProvider['npi_id'];

                    $sqlCriteria .= " provider.npi_id ='" . $searchProvider['npi_id'] . "' AND ";
                }

                if (isset($searchProvider['first_name']) && $searchProvider['first_name'] != "") {

                    $model->first_name = $searchProvider['first_name'];

                    $sqlCriteria .= " provider.first_name ='" . stripslashes($searchProvider['first_name']) . "' AND ";
                }

                if (isset($searchProvider['last_name']) && $searchProvider['last_name'] != "") {

                    $model->last_name = $searchProvider['last_name'];

                    $sqlCriteria .= " provider.last_name ='" . stripslashes($searchProvider['last_name']) . "' AND ";
                }

                if (isset($searchProvider['featured']) && $searchProvider['featured'] != "") {

                    $model->featured = $searchProvider['featured'];

                    $sqlCriteria .= " provider.featured ='" . $searchProvider['featured'] . "' AND ";
                }


                if (isset($searchProvider['featured_active']) && $searchProvider['featured_active'] != "") {

                    $model->featured_active = $searchProvider['featured_active'];

                    $sqlCriteria .= " partner_site_provider_featured.active = '" . ($searchProvider['featured_active'])
                        . "'  AND ";
                }

                if (isset($searchProvider['organization']) && $searchProvider['organization'] != "") {

                    $arrSql2 = explode("ORDER BY", $arrSql[1], 2);

                    $arrSql[1] = $arrSql2[0] . " HAVING organization" . ($searchProvider['organization'] == 0 ? '=' :
                        '<>') . "'' ORDER BY " . $arrSql2[1];

                    $model->organization = $searchProvider['organization'];
                }

                // if custom provider featured is set
                if (isset($arrSql[1])) {
                    $sqlProvider = $arrSql[0] . ' WHERE ' . $sqlCriteria . ' ' . $arrSql[1];
                } else {
                    $sqlProvider = $arrSql[0] . ' WHERE ' . $sqlCriteria;
                }
            }

            $rawData = array();
            if (!empty($sqlProvider)) {
                $rawData = Yii::app()->db->createCommand($sqlProvider)->queryAll();
            }

            $arrayDataProvider = new CArrayDataProvider($rawData, array(
                'id' => 'id',
                'sort' => array(
                    'attributes' => array(
                        'npi_id' => array(
                            'asc' => 'npi_id ASC',
                            'desc' => 'npi_id DESC',
                        ),
                        'credential_id' => array(
                            'asc' => 'credential_title ASC',
                            'desc' => 'credential_title DESC',
                        ),
                        'prefix' => array(
                            'asc' => 'prefix ASC',
                            'desc' => 'prefix DESC',
                        ),
                        'first_name' => array(
                            'asc' => 'first_name ASC',
                            'desc' => 'first_name DESC',
                        ),
                        'last_name' => array(
                            'asc' => 'last_name ASC',
                            'desc' => 'last_name DESC',
                        ),
                    ),
                ),
                'pagination' => array(
                    'pageSize' => 100,
                ),
            ));
        }

        return array(
            "partnerSite" => $partnerSite,
            "arrayDataProvider" => $arrayDataProvider,
            "searchProvider" => $searchProvider,
            "exportProvider" => $exportProvider,
        );
    }

    /**
     * Generates and save twilio numbers
     * @param integer $practiceId
     * @param bool $addProvider
     */
    public function generateTwilioNumbers($practiceId = 0, $addProvider = false)
    {
        $this->getTrackedNumbers(0, $practiceId);
        $this->saveTrackedNumbers($practiceId, $addProvider);
    }
}
