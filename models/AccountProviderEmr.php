<?php

Yii::import('application.modules.core_models.models._base.BaseAccountProviderEmr');

class AccountProviderEmr extends BaseAccountProviderEmr
{

    public static function model($className = __class__)
    {
        return parent::model($className);
    }

    /**
     * Save AuditLog after delete
     */
    public function afterDelete()
    {
        AuditLog::create(
            'D',
            'Delete record',
            'account_provider_emr',
            'Provider: ' . $this->emr_user_data . ' (' . $this->provider_id . ')',
            false,
            $this->account_id,
            true
        );

        // update scheduling if necessary
        if (!empty($this->emr_user_id) && !empty($this->provider_id)) {
            // async update
            SqsComponent::sendMessage('updateProviderBooking', $this->provider_id);
        }

        return parent::afterDelete();
    }

    /**
     * Checks that there are not active appointment widgets before disabling the
     * emr_data for a provider.
     * @return boolean
     */
    public function beforeSave()
    {
        $accountId = $this->account_id;
        $cnt = 0;
        // are we un-setting the provider? (disabling)
        if (!empty($this->oldRecord->provider_id) && empty($this->provider_id)) {
            // yes
            // check for active widgets for that provider
            $sql = sprintf(
                "SELECT COUNT(1)
                FROM provider_practice_widget
                INNER JOIN widget
                ON provider_practice_widget.widget_id = widget.id
                WHERE
                    (
                        widget.widget_type = 'GET_APPOINTMENT'
                        OR
                        (
                            widget.widget_type = 'MOBILE_ENHANCE'
                            AND
                            widget.options NOT LIKE '%%\"hide_appointment_button\":\"1\"%%'
                        )
                    )
                    AND widget.draft = 0
                    AND widget.hidden = 0
                    AND provider_practice_widget.provider_id = %d
                    AND widget.account_id = %d;",
                $this->oldRecord->provider_id,
                $accountId
            );

            $cnt = Yii::app()->db->createCommand($sql)->queryScalar();
            if ($cnt > 0) {
                $this->addError(
                    'provider_id',
                    'Please disable the appointment widgets for this provider before turning this feature off.'
                );
                return false;
            }
        }

        return parent::beforeSave();
    }

    /**
     * Save changes to audit log
     *
     * @return void
     */
    public function afterSave()
    {

        if ($this->isNewRecord) {
            AuditLog::create(
                'C',
                'Account Provider EMR',
                'account_provider_emr',
                'Provider #' . $this->provider_id,
                false,
                $this->account_id,
                true
            );
        } else {
            AuditLog::create(
                'U',
                'Account Provider EMR',
                'account_provider_emr',
                'Provider #' . $this->provider_id,
                false,
                $this->account_id,
                true
            );
        }

        // update scheduling if necessary
        if ($this->isNewRecord
            ||
            (!empty($this->oldRecord) && $this->oldRecord->emr_user_id != $this->emr_user_id)
            ||
            (!empty($this->oldRecord) && $this->oldRecord->provider_id != $this->provider_id)
        ) {
            if (!empty($this->provider_id)) {
                // async update
                SqsComponent::sendMessage('updateProviderBooking', $this->provider_id);
            }
            if (!empty($this->oldRecord) && $this->oldRecord->provider_id != $this->provider_id) {
                // async update the previous provider_id
                SqsComponent::sendMessage('updateProviderBooking', $this->oldRecord->provider_id);
            }
        }

        return parent::afterSave();
    }

    /**
     * Get all the providers associated with the given account and their EMR parameters if defined
     * Also check whether they have telemedicine enabled
     * @param int $accountId
     * @param bool $emrUserId
     * @return array
     */
    public static function getProviders($accountId, $emrUserId = false)
    {
        $aggregate = "";
        if ($emrUserId) {
            $aggregate = "AND emr_user_id IS NOT NULL AND emr_user_data IS NOT NULL";
        }
        $sql = sprintf(
            "SELECT ape.id, ape.account_id, ape.emr_user_id, ape.emr_user_data, ape.provider_id, ape.mi7_user_id,
            ape.fb_page_id , ppl.url AS third_party_booking_url,orf.active AS third_party_url_feature,
            IF (SUM(practice_details_tm.id) > 0 , 1, 0) AS has_telemedicine
            FROM account_provider_emr ape
            LEFT JOIN (SELECT DISTINCT provider_practice_listing.provider_id,provider_practice_listing.url
                FROM provider_practice_listing
                WHERE listing_type_id IN (156,157)
            ) ppl
            ON (ape.provider_id = ppl.provider_id OR ape.emr_user_id = ppl.provider_id)
            LEFT JOIN (SELECT * FROM organization_feature WHERE feature_id = 26 AND active = 1) orf
            ON ape.emr_user_id = orf.provider_id
            LEFT JOIN provider_practice AS provider_practice_tm
            ON ape.provider_id = provider_practice_tm.provider_id
            LEFT JOIN practice_details AS practice_details_tm
            ON provider_practice_tm.practice_id = practice_details_tm.practice_id
            AND practice_details_tm.practice_type_id = 5
            WHERE ape.account_id = %d
            %s
            GROUP BY ape.emr_user_id;",
            $accountId,
            $aggregate
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Force to delete AccountProviderEmr record
     * @param int $accountId
     * @param int $providerId
     * @return array
     */
    public static function deleteRecord($accountId = 0, $providerId = 0)
    {
        // delete from AccountProviderEMR
        $apEmr = AccountProviderEmr::model()->findAll(
            'provider_id = :provider_id AND account_id = :account_id',
            array(':provider_id' => $providerId, ':account_id' => $accountId)
        );
        if (!empty($apEmr)) {
            foreach ($apEmr as $eachRecord) {
                $eachRecord->delete();
            }
        }
    }

    /**
     * Get all the providers associated with the given account and their EMR parameters if defined
     * @param int $accountId
     * @return array
     */
    public static function getByAccountId($accountId)
    {
        $sql = sprintf(
            "SELECT p.id, localization_id, country_id, first_last, first_m_last, first_middle_last,
            npi_id, gender, credential_id, prefix, first_name, middle_name, last_name, suffix, docscore,
            accepts_new_patients, year_began_practicing, vanity_specialty, additional_specialty_information,
            brief_bio, bio, fees, avg_rating, friendly_url, photo_path, p.date_added, p.date_updated, umd_id,
            date_umd_updated, featured, verified, claimed, suspended, partner_site_id, is_test,
            fb_page_id  AS profile_name,
            MAX(ape.id) AS ape_id, account_id, emr_user_id, emr_user_data, provider_id, mi7_user_id, fb_page_id
            FROM provider p
            INNER JOIN account_provider_emr ape
                ON p.id = ape.provider_id
            WHERE ape.account_id = %d
            GROUP BY p.id",
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Find the providers associated with this facebook page id
     * @param str $fbPageId
     * @return int $providerId
     */
    public static function getByFacebookPageId($fbPageId = '')
    {
        $providerId = 0;
        $fbPageId = trim($fbPageId);
        if (!empty($fbPageId)) {
            $sql = sprintf(
                "SELECT provider_id
                FROM account_provider_emr ape
                WHERE ape.fb_page_id = '%s'",
                $fbPageId
            );
            $providerId = Yii::app()->db->createCommand($sql)->queryScalar();
        }
        return $providerId;
    }

    /**
     * Count providers to export to md.com
     * @return int
     */
    public static function countMdDataExport()
    {
        $sql = "SELECT COUNT(DISTINCT provider_practice.provider_id,
            provider_practice.practice_id) "
            . self::getMdDataExportCondition() . ";";

        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Find the providers to export to md.com
     * @param int $limitFrom
     * @param int $limitTo
     * @return array
     */
    public static function getMdDataExport($limitFrom, $limitTo)
    {
        $sql = sprintf(
            "SELECT DISTINCT provider_practice.provider_id,
            provider_practice.practice_id
            %s
            LIMIT %d, %d;",
            self::getMdDataExportCondition(),
            $limitFrom,
            $limitTo
        );
        return Yii::app()->db->createCommand($sql)->queryAll();
    }

    /**
     * Unified function to retrieve the FROM and WHERE sections of the query from for the MD.com data export
     * @return string
     */
    private static function getMdDataExportCondition()
    {
        return "FROM account_provider_emr
            INNER JOIN provider_practice
                ON  account_provider_emr.provider_id= provider_practice.provider_id
            INNER JOIN account_practice_emr
                ON  (
                    account_practice_emr.practice_id =  provider_practice.practice_id
                    AND
                    account_practice_emr.account_id=  account_provider_emr.account_id
                    )
            INNER JOIN organization_account
                ON organization_account.account_id=account_provider_emr.account_id
            INNER JOIN organization
                ON organization.id = organization_account.organization_id
            INNER JOIN provider_details
                ON provider_details.provider_id= account_provider_emr.provider_id
            INNER JOIN provider
                ON  account_provider_emr.provider_id= provider.id
            INNER JOIN credential
                ON provider.credential_id = credential.id
            WHERE schedule_mail_enabled='1'
                AND (title like '%,MD %'
                OR title like '% ,MD'
                OR title like '%, MD'
                OR title like 'MD,%'
                OR title like '%,DO %'
                OR title like '% ,DO'
                OR title like '%, DO'
                OR title like 'DO,%'
                OR title = 'MD'
                OR title = 'DO'
                OR title like '%,M.D. %'
                OR title like '% ,M.D.'
                OR title like '%, M.D.'
                OR title like 'M.D.,%'
                OR title = 'M.D.'
                OR title like '%,M.D %'
                OR title like '% ,M.D'
                OR title like '%, M.D'
                OR title like 'M.D,%'
                OR title = 'M.D'
                OR title=' M.D.'
                OR title like '%,D.O. %'
                OR title like '% ,D.O.'
                OR title like '%, D.O.'
                OR title like 'D.O.,%'
                OR title = 'D.O.'
                OR title like '%,DO %'
                OR title like '% ,D.O'
                OR title like '%, D.O'
                OR title like 'D.O,%'
                OR title = 'D.O') ";
    }

    /**
     * Find Account Provider Emr
     * @param array $mi7
     * @param int $accountId
     * @param int $aEmrId
     * @return array
     */
    public static function mi7FindProviderEmr($mi7, $accountId, $aEmrId)
    {

        $matchId = !empty($mi7['PersonnelID']) ? $aEmrId . $mi7['PersonnelID'] : $aEmrId . $mi7['AttendingID'];
        $aProviderEmr = AccountProviderEmr::model()->find(
            "mi7_user_id = :mi7_user_id AND account_id = :account_id",
            array(":mi7_user_id" => $matchId, ":account_id" => $accountId)
        );

        if (empty($aProviderEmr)) {
            $matchFName = !empty($mi7['PersonnelFirstName']) ? $mi7['PersonnelFirstName'] : $mi7['AttendingFirstName'];
            $matchLName = !empty($mi7['PersonnelLastName']) ? $mi7['PersonnelLastName'] : $mi7['AttendingLastName'];
            if (!empty($matchFName)) {

                $sql = "SELECT provider.id as provider_id
                    FROM provider
                    INNER JOIN account_provider_emr
                    ON provider.id = account_provider_emr.provider_id
                    WHERE mi7_user_id IS NULL
                    AND provider.first_name = :first_name
                    AND provider.last_name = :last_name
                    AND account_id = :account_id;";

                $command = Yii::app()->db->createCommand($sql);
                $command->bindParam(":first_name", $matchFName, PDO::PARAM_STR);
                $command->bindParam(":last_name", $matchLName, PDO::PARAM_STR);
                $command->bindParam(":account_id", $accountId, PDO::PARAM_INT);
                $providerId = $command->queryScalar();

                if (empty($providerId)) {

                    //@TODO: Create - If the provider doesn't exist
                    //$aProviderEmr = AccountProviderEmr::model()->find("emr_user_data = :emr_user_data",
                    // array(":emr_user_data" => $patient['PersonnelFirstName'].' '.$patient['PersonnelLastName']));
                    //if (empty($aProviderEmr)) {
                    // If the provider doesn't exist
                    //$aProviderEmr = new AccountProviderEmr();
                    //$aProviderEmr->account_id = $aPracticeEmr->account_id;
                    //$aProviderEmr->emr_user_id = $patient['PersonnelID'];
                    //$aProviderEmr->emr_user_data = $patient['PersonnelFirstName'].' '.$patient['PersonnelLastName'];
                    //$aProviderEmr->provider_id = $providerId;
                    //}

                    $result['result'] = false;
                    $result['results'] = "The provider doesn't exist";

                    return $result;
                } else {

                    $aProviderEmr = AccountProviderEmr::model()->find(
                        "provider_id = :provider_id AND account_id = :account_id",
                        array(":provider_id" => $providerId, ":account_id" => $accountId)
                    );

                    $aProviderEmr->mi7_user_id = $matchId;
                    if (!$aProviderEmr->save()) {
                        $result['result'] = false;
                        $result['results'] = "mi7_user_id: " . $aProviderEmr->mi7_user_id
                            . " " . json_encode($aProviderEmr->errors);

                        return $result;
                    }
                }
            } else {
                $result['result'] = false;
                $result['results'] = "Ignored";

                return $result;
            }
        } else {
            $providerId = $aProviderEmr->provider_id;
        }

        $result['result'] = true;
        $result['providerId'] = $providerId;
        $result['results'] = "success";

        return $result;
    }

    /**
     * Return Mi7User
     * @param int $providerId
     * @param int $accountId
     * @return bool
     */
    public static function isMi7User($providerId = 0, $accountId = 0)
    {
        return AccountProviderEmr::model()->exists(
            'provider_id=:provider_id AND account_id=:account_id AND mi7_user_id IS NOT NULL ',
            array(":provider_id" => $providerId, ':account_id' => $accountId)
        );
    }

    /**
     * Return EmrUser
     * @param int $providerId
     * @param int $accountId
     * @return bool
     */
    public static function isEmrUser($providerId = 0, $accountId = 0)
    {
        $sql = sprintf(
            "SELECT COUNT(ape.id)
            FROM account_provider_emr ape
            LEFT JOIN organization_account oa
                ON oa.account_id = ape.account_id
            LEFT JOIN organization_account oa2
                ON oa.organization_id = oa2.organization_id
            WHERE ape.provider_id = %d
                AND ape.emr_user_id IS NOT NULL
                AND (ape.account_id = %d OR oa2.account_id = %d)",
            $providerId,
            $accountId,
            $accountId
        );
        return Yii::app()->db->createCommand($sql)->queryScalar();
    }

    /**
     * Count How Many Providers has this account
     * @param int $accountId
     * @param int $emrId
     * @return int
     */
    public static function countProviders($accountId = 0, $emrId = 0)
    {
        $cnt = 0;
        if ($accountId > 0 && $emrId > 0) {
            $sql = sprintf(
                "SELECT COUNT(*) AS cnt
                FROM account_provider_emr
                WHERE account_id = '%d'
                    AND emr_user_id != ''
                    AND emr_user_id IS NOT NULL",
                $accountId,
                $emrId
            );
            $cnt = (int)Yii::app()->db->createCommand($sql)->queryScalar();
        }
        return $cnt;
    }

    /**
     * get all Providers for this account
     * @param int $accountId
     * @return object
     */
    public static function getAllByAccountId($accountId = 0)
    {
        return AccountProviderEmr::model()->findAll(
            "account_id = :account_id ORDER BY emr_user_data ASC",
            array(":account_id" => $accountId)
        );
    }
}
