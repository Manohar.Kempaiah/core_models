<?php

Yii::import('application.modules.core_models.models._base.BaseOrganizationGroup');

class OrganizationGroup extends BaseOrganizationGroup
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Validate data beforeSave()
     * @return boolean
     */
    public function beforeSave()
    {
        $this->contact_phone_number = preg_replace('/[^0-9]/', '', $this->contact_phone_number);
        $this->contact_fax_number = preg_replace('/[^0-9]/', '', $this->contact_fax_number);
        return parent::beforeSave();
    }

}
