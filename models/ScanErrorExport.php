<?php

Yii::import('application.modules.core_models.models._base.BaseScanErrorExport');

class ScanErrorExport extends BaseScanErrorExport
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Export the Scan Error Report
     * @param string $yearMonth
     *
     * @return file
     */
    public static function donwloadHealthgradeCsv($yearMonth)
    {

        $sql = sprintf(
            "SELECT *
            FROM scan_error_export
            WHERE DATE_FORMAT( date_added, '%%Y-%%c') = '%s'
            ORDER BY id DESC;",
            $yearMonth
        );
        $records = Yii::app()->db->createCommand($sql)->queryAll();
        $report = array();

        if (!empty($records)) {
            foreach ($records as $v) {
                $scanErrorId = $v['scan_error_id'];

                if (empty($report[$scanErrorId])) {
                    // Is the first record
                    $v['remarks'] = 'Please ' . $v['remarks'];
                    if ($v['scan_result_provider_id'] > 0) {
                        $scanResult = ScanResultProvider::model()->findByPk($v['scan_result_provider_id']);
                    } else {
                        $scanResult = ScanResultPractice::model()->findByPk($v['scan_result_practice_id']);
                    }

                    $arrScanResult['match_name'] = $scanResult->match_name;
                    $arrScanResult['match_address'] = $scanResult->match_address;
                    $arrScanResult['match_phone'] = $scanResult->match_phone;
                    $arrScanResult['match_specialty'] = $scanResult->match_specialty;

                    // Adding providers details
                    $provider = [
                        'provider_name' => 'N/A',
                        'credential' => 'N/A',
                        'npi' => 'N/A',
                        'primary_specialty' => 'N/A',
                    ];
                    if (!empty($v['provider_id'])) {
                        $providerId = $v['provider_id'];
                        $arrProvider = Provider::model()->findByPk($providerId);

                        $specialty = ProviderSpecialty::getPrimarySpecialty($providerId);

                        if (!empty($arrProvider)) {
                            $provider = [
                                'provider_name' => $arrProvider->first_last,
                                'credential' => Credential::model()->findByPk($arrProvider->credential_id)->title,
                                'npi' => $arrProvider->npi_id,
                                'primary_specialty' => !empty($specialty[0]['name']) ? $specialty[0]['name'] : 'N/A',
                            ];
                        }
                    }

                    // Adding practice details
                    $practice = [
                        'location' => 'N/A',
                        'address' => 'N/A',
                        'city' => 'N/A',
                        'state' => 'N/A',
                        'zip' => 'N/A',
                        'phone' => 'N/A',
                    ];

                    if (!empty($v['practice_id'])) {
                        $arrPractice = Practice:: getDetails($v['practice_id']);
                        $arrPractice = $arrPractice[0];
                        $practice = [
                            'location' => $arrPractice['name'],
                            'address' => $arrPractice['address_full'],
                            'city' => $arrPractice['city'],
                            'state' => $arrPractice['state_abbreviation'],
                            'zip' => $arrPractice['zipcode'],
                            'phone' => $arrPractice['phone_number'],
                        ];

                    }

                    $report[$scanErrorId] = array_merge($v, $arrScanResult, $provider, $practice);

                } else {
                    // we found another record for the same scan_error->id

                    if ($v['issue_name'] == 1) {
                        $report[$scanErrorId]['issue_name'] = 1;
                    }
                    if ($v['issue_address'] == 1) {
                        $report[$scanErrorId]['issue_address'] = 1;
                    }
                    if ($v['issue_phone'] == 1) {
                        $report[$scanErrorId]['issue_phone'] = 1;
                    }
                    if ($v['issue_specialty'] == 1) {
                        $report[$scanErrorId]['issue_specialty'] = 1;
                    }
                    if ($v['issue_photo'] == 1) {
                        $report[$scanErrorId]['issue_photo'] = 1;
                    }
                    if ($v['issue_credentials'] == 1) {
                        $report[$scanErrorId]['issue_credentials'] = 1;
                    }
                    if ($v['issue_office_hours'] == 1) {
                        $report[$scanErrorId]['issue_office_hours'] = 1;
                    }

                    $report[$scanErrorId]['remarks'] .= $v['remarks'];

                }

            }

        }

        $fp = fopen('php://temp', 'w');

        /*
         * Write a header of csv file
         */
        $headers = array();
        $headers[] = 'organization_id';
        $headers[] = 'URL';
        $headers[] = 'Remarks';

        $headers[] = 'Name';
        $headers[] = 'Credentials';
        $headers[] = 'Address';
        $headers[] = 'Phone';
        $headers[] = 'Specialty';
        $headers[] = 'Photo';
        $headers[] = 'Ofic.Hours';

        $headers[] = 'npi';
        $headers[] = 'provider_name';
        $headers[] = 'credential';
        $headers[] = 'provider_status';
        $headers[] = 'primary_specialty';

        $headers[] = 'location';
        $headers[] = 'address';
        $headers[] = 'city';
        $headers[] = 'state';
        $headers[] = 'zip';
        $headers[] = 'phone';

        $headers[] = 'listing_site';
        $headers[] = 'name_on_listing';
        $headers[] = 'match_name';
        $headers[] = 'address_on_listing';
        $headers[] = 'match_address';
        $headers[] = 'phone_on_listing';
        $headers[] = 'match_phone';

        fputcsv($fp, $headers);

        if ($report) {
            foreach ($report as $value) {
                $row = array();
                $row[] = $value['organization_id'];
                $row[] = $value['url_on_listing'];
                $row[] = $value['remarks'];

                $row[] = Common::isTrue($value['issue_name']) ? 'Issue' : 'No issue';
                $row[] = Common::isTrue($value['issue_credentials']) ? 'Issue' : 'No issue';
                $row[] = Common::isTrue($value['issue_address']) ? 'Issue' : 'No issue';
                $row[] = Common::isTrue($value['issue_phone']) ? 'Issue' : 'No issue';
                $row[] = Common::isTrue($value['issue_specialty']) ? 'Issue' : 'No issue';
                $row[] = Common::isTrue($value['issue_photo']) ? 'Issue' : 'No issue';
                $row[] = Common::isTrue($value['issue_office_hours']) ? 'Issue' : 'No issue';

                $row[] = $value['npi'];
                $row[] = $value['provider_name'];
                $row[] = $value['credential'];
                $row[] = $value['provider_status'];
                $row[] = $value['primary_specialty'];

                $row[] = $value['location'];
                $row[] = $value['address'];
                $row[] = $value['city'];
                $row[] = $value['state'];
                $row[] = $value['zip'];
                $row[] = $value['phone'];

                $row[] = $value['listing_site'];
                $row[] = $value['name_on_listing'];
                $row[] = $value['match_name'];
                $row[] = $value['address_on_listing'];
                $row[] = $value['match_address'];
                $row[] = $value['phone_on_listing'];
                $row[] = $value['match_phone'];

                fputcsv($fp, $row);
            }
        }

        // save csv content to a Session
        rewind($fp);
        Yii::app()->user->setState('export', stream_get_contents($fp));
        Yii::app()->request->sendFile(
            'Scan_Error_Report_For_Healthgrade_' . $yearMonth . '_' . date("Ymd") . '.csv',
            Yii::app()->user->getState('export')
        );
        Yii::app()->user->clearState('export');
        fclose($fp);
    }

}
