<?php

Yii::import('application.modules.core_models.models._base.BaseWaitingRoomQueue');

class WaitingRoomQueue extends BaseWaitingRoomQueue
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * handle logic for setting the queue
     */
    public function afterSave()
    {

        if (!$this->waiting_room_invite_id) {
            return parent::afterSave();
        }
        // Every change in the queue clear the waiting Room telemedicine cache for this setting->id
        // params: name, value, time (in seconds)
        $waitingRoomInvite = $this->waitingRoomInvite;
        Yii::app()->cache->set("telemedicine_" . $waitingRoomInvite->waiting_room_setting_id, json_encode(''), 300);

        if (!empty($this->room_ended) || !empty($this->provider_disconnected)) {
            // Check if exists
            $chatHistory = WaitingRoomChatHistory::model()->find(
                "waiting_room_invite_id=:waiting_room_invite_id",
                array(":waiting_room_invite_id" => $waitingRoomInvite->id)
            );

            if (empty($chatHistory->messages_count)) {
                // update the chat
                WaitingRoomChatHistory::saveHistory($waitingRoomInvite->guid);
            }
        }

        return parent::afterSave();
    }

    /**
     * Fix the orphans queue records
     * @param integer $wrsId -> waiting_room_setting_id
     */
    public static function fixQueue($wrsId = null)
    {
        if (empty($wrsId)) {
            return;
        }

        // Fill uncomplete call with Twilio room data
        $sql = sprintf(
            "SELECT wrq.*
            FROM waiting_room_queue AS wrq
            LEFT JOIN waiting_room_invite as wri ON wrq.waiting_room_invite_id = wri.id
            LEFT JOIN waiting_room_setting as wrs ON wri.waiting_room_setting_id = wrs.id
            WHERE
            wrs.id = %d
            AND (
                (patient_connected IS NULL AND patient_disconnected IS NULL) -- never connected
                OR
                (patient_connected IS NOT NULL AND patient_disconnected IS NOT NULL) -- disconnected
            )
            AND provider_disconnected IS NOT null
            AND room_ended IS NULL
            AND room_sid IS NOT NULL
            AND room_duration IS NULL
            AND DATE_ADD(wrq.date_updated, INTERVAL 10 HOUR) > NOW()",
            $wrsId
        );

        $arrQueue = Yii::app()->db->cache(Yii::app()->params['cache_medium'])->createCommand($sql)->queryAll();
        if (empty($arrQueue)) {
            return;
        }

        foreach ($arrQueue as $k => $v) {

            $data = TwilioApi::getRoom($v['room_sid']);

            if (!empty($data['data']['room']['status'])) {
                $room = $data['data']['room'];

                if ($room['status'] == 'completed') {
                    // update the room_ended and room_duration fields
                    $wrq = WaitingRoomQueue::model()->findByPk($v['id']);
                    $timeStamp = strtotime($room['end_time']);
                    $wrq->room_ended = date('Y-m-d H:i:s', $timeStamp);
                    $wrq->room_duration =  $room['duration'];
                    $wrq->save();

                    $arrQueue[$k]['room_ended'] = $wrq->room_ended;
                    $arrQueue[$k]['room_duration'] = $wrq->room_duration;
                }

            } else {
                // Is an old call and we havent in twilio
                if (!empty($v['provider_connected']) && !empty(['patient_disconnected'])) {
                    $timeConnect  = strtotime($v['provider_connected']);
                    $timeDisconnect = strtotime($v['patient_disconnected']);
                    $roomDuration = $timeDisconnect - $timeConnect;

                    $wrq = WaitingRoomQueue::model()->findByPk($v['id']);
                    $wrq->room_ended = $v['patient_disconnected'];
                    $wrq->room_duration =  $roomDuration;
                    $wrq->save();

                }
            }

        }

        return [
            'success' => true,
            'error' => false,
            'data' => $arrQueue
        ];
    }


    /**
     * Analyze the queue for specific id, and remove it if never was used
     * @param int $wrqId waitingRoomQueue->id
     *
     */
    public static function removeUnused($wrqId)
    {

        $wrq = WaitingRoomQueue::model()->findByPk($wrqId);

        if ($wrq) {

            $now = strtotime(date('Y-m-d H:i:s'));
            $heartbeatWRI = Yii::app()->cache->get('heartbeat_wri_' . $wrq->waiting_room_invite_id);
            $lastPulse = TimeComponent::getDiff($now, $heartbeatWRI);

            if ($lastPulse < 5) {
                // Less than 5 seconds, still considered alive
                return;
            }

            if (empty($wrq->patient_connected) && empty($wrq->patient_disconnected)) {
                // Maybe was a guest participant
                $wrgq = WaitingRoomGuestQueue::model()->findAll(
                    "waiting_room_queue_id=:waiting_room_queue_id",
                    array(":waiting_room_queue_id" => $wrq->id)
                );
                if (!empty($wrgq)) {
                    foreach ($wrgq as $v) {
                        $v->delete();
                    }
                }

                $wrq->delete();

            } elseif (!empty($wrq->patient_connected) && empty($wrq->patient_disconnected)) {
                // The patient was connected
                if (empty($wrq->provider_connected)) {
                    // The provider never connected
                    $wrq->delete();
                }
            }

        }

    }
}
