<?php

Yii::import('application.modules.core_models.models._base.BaseEhrCommunicationQueue');

class EhrCommunicationQueue extends BaseEhrCommunicationQueue
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Returns the value of the next id to be inserted in ehr_communication_queue
     * @return int
     */
    public static function getNextId()
    {
        $sql = "SHOW TABLE STATUS WHERE `Name` = 'ehr_communication_queue'";
        $r = Yii::app()->db->createCommand($sql)->queryRow();
        return (int)$r['Auto_increment'];
    }

    /**
     * Save a message in ehr_communication_queue
     * @param array $params
     */
    public static function queueMessage($params)
    {
        $eCommunicationQueue = new EhrCommunicationQueue;
        $eCommunicationQueue->emr_id = $params['emr_id'];
        $eCommunicationQueue->emr_location_id = !empty($params['emr_location_id']) ? $params['emr_location_id'] : '';
        $eCommunicationQueue->endpoint = $params['endpoint'];
        $eCommunicationQueue->message_type = $params['message_type'];
        $eCommunicationQueue->message = $params['message'];
        $eCommunicationQueue->processed = !empty($params['processed']) ? $params['processed'] : null;
        $eCommunicationQueue->success = !empty($params['success']) ? $params['success'] : null;
        $eCommunicationQueue->entity_type = !empty($params['entity_type']) ? $params['entity_type'] : null;
        $eCommunicationQueue->entity_value = !empty($params['entity_value']) ? $params['entity_value'] : null;
        $eCommunicationQueue->save();
    }

    /**
     * waiting for provider list
     * @param array $accountEmrId
     * @return int
     */
    public static function waitingProviderList($accountEmrId = 0)
    {
        $cnt = 0;
        if ($accountEmrId > 0) {
            $sql = sprintf(
                "SELECT COUNT(*) AS cnt"
                . " FROM ehr_communication_queue"
                . " WHERE emr_id = '%d' AND message_type = %d",
                $accountEmrId,
                Mi7Importer::$GET_PROVIDER_LIST
            );
            $cnt = (int) Yii::app()->db->createCommand($sql)->queryScalar();
        }
        return $cnt;
    }

}
