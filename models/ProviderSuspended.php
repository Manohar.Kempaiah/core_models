<?php

Yii::import('application.modules.core_models.models._base.BaseProviderSuspended');

class ProviderSuspended extends BaseProviderSuspended
{

    /**
     * Suspended reason ENUM values.
     */
    const REASON_NPI_DEACTIVATED = 'NPI Deactivated';
    const REASON_RETIRED = 'Retired';
    const REASON_DECEASED = 'Deceased';
    const REASON_UNEMPLOYED = 'Unemployed';
    const REASON_LEGAL_ACTION = 'LegalAction';
    const REASON_DELETED = 'Deleted';
    const REASON_REMOVAL_REQUESTED = 'Removal Requested';
    const REASON_OTHER = 'Other';

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Check for or automatically delete data from secondary tables before deleting
     * @return boolean
     */
    public function beforeSave()
    {
        if ($this->isNewRecord) {
            // avoid duplicate from provider_suspended if it has been created
            $exists = ProviderSuspended::exists('provider_id=:provider_id', array(':provider_id' => $this->provider_id));
            if ($exists) {
                $this->addError("id", "Provider was already suspended.");
                return false;
            }
        }
        return parent::beforeSave();
    }

    /**
     * Check if this provider no longer is suspended
     * @return boolean
     */
    public function afterDelete()
    {

        // Sets the value of Provider.claimed
        if (ProviderSuspended::model()->countByAttributes(array("provider_id" => $this->provider_id)) == 0) {
            $provider = Provider::model()->findByPk($this->provider_id);
            if ($provider) {
                $provider->suspended = 0;
                $provider->save();
            }
        }

        return parent::afterDelete();
    }

    /**
     * called on rendering the column for each row in the administration
     * @return string
     */
    public function getProviderTooltip()
    {
        return ('<a class="providerTooltip" href="/administration/providerTooltip?id=' . $this->provider_id .
            '" rel="/administration/providerTooltip?id=' . $this->provider_id . '" title="' . $this->provider . '">' .
            $this->provider . '</a>');
    }

    /**
     * Update provider_suspended data
     * @return boolean
     */
    public function afterSave()
    {

        //Sets the value of Provider.suspended
        $provider = Provider::model()->findByPk($this->provider_id);
        if ($provider) {
            $provider->suspended = 1;
            $provider->save();
        }

        return parent::afterSave();
    }

}
