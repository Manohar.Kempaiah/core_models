<?php

Yii::import('application.modules.core_models.models._base.BaseWaitingRoomTwilioCallbacks');

class WaitingRoomTwilioCallbacks extends BaseWaitingRoomTwilioCallbacks
{
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Save twilio callback log
     * @param integer $waitingRoomInviteId
     * @param json $statusObj
     *
     *
     */
    public static function auditLog($waitingRoomInviteId = 0, $statusObj = '')
    {

        if (empty($statusObj) || $waitingRoomInviteId == 0) {
            return null;
        }

        $log = new WaitingRoomTwilioCallbacks();
        $log->waiting_room_invite_id = $waitingRoomInviteId;
        $log->callback_event = $statusObj['StatusCallbackEvent'];
        $log->callback_timestamp = date('Y-m-d H:i:s', strtotime($statusObj['Timestamp']));
        $log->participant_identity = !empty($statusObj['ParticipantIdentity']) ? $statusObj['ParticipantIdentity'] : 'N/A';
        $log->log = json_encode($statusObj);
        $log->date_added = TimeComponent::getNow();
        $log->save();
    }
}
