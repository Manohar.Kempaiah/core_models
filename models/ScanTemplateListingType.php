<?php

Yii::import('application.modules.core_models.models._base.BaseScanTemplateListingType');

class ScanTemplateListingType extends BaseScanTemplateListingType
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Rules
     *
     * @return array
     */
    public function rules()
    {
        return array(
            array('listing_type_id, scan_template_id, weight, section, provider_primary_only, provider_secondary_only, date_added, added_by_account_id, date_updated', 'required'),
            array('listing_type_id, scan_template_id, max_age, provider_primary_only, provider_secondary_only, added_by_account_id', 'numerical', 'integerOnly'=>true),
            array('type', 'length', 'max' => 100),
            array('weight', 'length', 'max' => 5),
            array('section', 'length', 'max' => 12),
            array('type, max_age', 'default', 'setOnEmpty' => true, 'value' => null),
            array('id, listing_type_id, scan_template_id, type, max_age, weight, section, provider_primary_only, provider_secondary_only, date_added, added_by_account_id, date_updated', 'safe', 'on'=>'search'),
        );
    }


    /**
     * Get the max age of a listing type by template
     *
     * @param integer $listingTypeId
     * @param integer $scanTemplateId
     *
     * @return integer
     * @throws Exception
     */
    public function getListingTypeMaxAge($listingTypeId, $scanTemplateId)
    {
        $maxAge = 0;

        $sql = "SELECT scan_template_listing_type.max_age
                FROM scan_template_listing_type
                WHERE scan_template_listing_type.listing_type_id = :listing_type_id
                  AND scan_template_listing_type.scan_template_id = :scan_template_id
                ORDER BY scan_template_listing_type.id DESC
                LIMIT 1";

        $result = Yii::app()->db
            ->cache(Yii::app()->params['cache_short'])
            ->createCommand($sql)
            ->bindValues([
                ":listing_type_id" => $listingTypeId,
                ":scan_template_id" => $scanTemplateId
            ])
            ->queryScalar();

        if ($result) {
            $maxAge = $result;
        }

        return $maxAge;
    }

}
