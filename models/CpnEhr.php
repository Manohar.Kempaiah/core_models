<?php

Yii::import('application.modules.core_models.models._base.BaseCpnEhr');

class CpnEhr extends BaseCpnEhr
{

    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    /**
     * Ehr Configuration
     *
     * @param int $installationIdentifier
     * @return array
     */
    public function getEhrConfiguration($installationId)
    {
        // Query to cpn_ehr
        $sql = sprintf(
            "SELECT
                e.guid AS ehrIdentifier,
                ei.guid AS identifier,
                v.`name` AS hl7Version,
                e.sockets_flag AS socketsFlag,
                e.folder_flag AS folderFlag,
                e.sftp_flag AS sftpFlag,
                e.custom_flag AS customFlag,
                e.mllp_flag AS mllpFlag,
                e.keep_alive_flag AS keepAliveFlag
            FROM cpn_ehr e
                INNER JOIN cpn_hl7_version v on e.hl7_version_id = v.id
                INNER JOIN cpn_ehr_installation ei on e.id = ei.cpn_ehr_id
                INNER JOIN cpn_installation i on ei.cpn_installation_id = i.id
            WHERE i.id = '%s'
                AND e.del_flag = 0
                AND ei.del_flag = 0
                AND i.del_flag = 0;",
            $installationId
        );

        // Return all the results
        return Yii::app()->db->createCommand($sql)->queryAll();
    }
}
